﻿#include <iostream>
#include <vector>
#include <mutex>
#include <condition_variable>
#include <random>
#include <cmath>
#include "../../appkit/tables/mt_fifo.h"
#include "../../appkit/tables/ringbuf_ctr.h"
#include "./exequeue.h"


using namespace crm;


std::atomic<size_t> okCounter{0};
std::atomic<size_t> failCouner{0};

template<typename TValue,
	typename std::enable_if_t<std::is_integral_v<TValue>, int> = 0 >
void test_integral(size_t syncSize, size_t sequenceSize) {
	using base_vector_type = crm::detail::ringbuf_ctr_t<crm::detail::ringbuf_handler<std::vector<uint8_t>>>;
	using sync_vector_type = crm::detail::mt_fifo_queue_t<std::mutex, std::unique_lock<std::mutex>, std::condition_variable, base_vector_type>;

	struct valgen_t {
		std::random_device rd;
		std::mt19937 gen{rd()};
		std::normal_distribution<> d;

		valgen_t(size_t range)
			: d{range} {
			}

		TValue operator()() {
			return  (size_t)d(gen) % std::numeric_limits<TValue>::max();
			}
		};

	std::cout << "================================================================================================" << std::endl;
	std::cout << "test(" << typeid(TValue).name() << ") read|write, sync=" << syncSize << ", elements" << std::endl;

	auto syncstream = std::make_shared<sync_vector_type>(base_vector_type(__FILE_LINE__, syncSize));
	auto r = testsuite::test_mtqueue_onewrite_oneread<sync_vector_type>::execute(sequenceSize, syncstream, valgen_t{sequenceSize * 100});

	if (r) {
		okCounter++;
		}
	else {
		failCouner++;
		}

	std::cout << "test result is ..... " << std::boolalpha << r << std::endl;
	}

template<typename TValue,
	typename std::enable_if_t<std::is_integral_v<TValue>, int> = 0 >
void test_integral() {
	test_integral<TValue>(30, 1024 * 1400);
	test_integral<TValue>(128, 16 * 1024 * 1024);
	test_integral<TValue>(1024, 128 * 1024 * 1024);
	test_integral<TValue>(512, 256 * 1024 * 1024);
	test_integral<TValue>(1024, 1024 * 1024 * 1024);
	test_integral<TValue>(2, 1024 * 1024);
	test_integral<TValue>(1, 1024);
	}

int main(){

	test_integral<uint8_t>();
	test_integral<uint64_t>();

	std::cout << "all batches completed..." << std::endl;
	std::cout << "ok: " << okCounter << ", fail: " << failCouner << std::endl;

	getchar();
	}

