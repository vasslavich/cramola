#pragma once


#include <future>
#include <thread>
#include "../../appkit/exceptions.h"
#include "../../appkit/tables/bufdef.h"


namespace crm{
namespace testsuite{


template<typename Tsyncbuf,
	typename ValueType = typename Tsyncbuf::value_type>
	class test_mtqueue_onewrite_oneread {
	public:
		using sync_buffer_type = typename Tsyncbuf;
		using value_type = typename ValueType;

	private:
		std::atomic<bool> _stopf{false};

		std::future<std::vector<value_type>> ReadThread(std::weak_ptr<sync_buffer_type> sbuf) {
			std::packaged_task<std::vector<value_type>()> task([this, sbuf] {

				std::vector<value_type> r;

				std::array<value_type, 10> block;
				while (!_stopf.load(std::memory_order::memory_order_relaxed)) {
					if (auto pbuf = sbuf.lock()) {

						size_t blockSize = (std::chrono::system_clock::now().time_since_epoch().count() % (block.size() - 1)) + 1;

						decltype(block.begin()) next;
						auto readyval = pbuf->pop(block.begin(), block.begin() + blockSize, next, std::chrono::seconds(5));

						if (bufusage_prec_t::codes_t::ERR_SUCCESS != readyval.errc) {
							if (bufusage_prec_t::codes_t::ERR_EMPTY == readyval.errc) {
								continue;
								}
							else if (bufusage_prec_t::codes_t::ERR_TIME_OUT == readyval.errc) {
								continue;
								}
							else {
								FATAL_ERROR_FWD(nullptr);
								}
							}
						else {
							r.insert(r.end(), block.cbegin(), block.cbegin() + readyval.usedElements);
							}
						}
					}

				return r;
				});

			auto wait = task.get_future();
			std::thread th(std::move(task));
			th.detach();

			return wait;
			}

		std::future<void> WriteThread(std::weak_ptr<sync_buffer_type> wbuf, const std::vector<value_type> & source) {

			std::packaged_task<void()> task([this, wbuf, &source] {

				const size_t blockSizeModule = 10;
				size_t ids = 0;
				while (ids < source.size()) {

					size_t blockSize = (std::chrono::system_clock::now().time_since_epoch().count() % blockSizeModule) + 1;
					blockSize = std::min(blockSize, source.size() - ids);

					const auto timeout = std::chrono::seconds(blockSize % 10);

					if (auto sbuf = wbuf.lock()) {
						const auto r = sbuf->push(source.cbegin() + ids, source.cbegin() + ids + blockSize, timeout);
						if (bufusage_prec_t::codes_t::ERR_SUCCESS != r.errc) {

							//if no wait and fulfilled
							if (bufusage_prec_t::codes_t::ERR_OVERFLOW == r.errc) {
								continue;
								}
							else if (bufusage_prec_t::codes_t::ERR_TIME_OUT == r.errc) {
								continue;
								}
							else {
								FATAL_ERROR_FWD(nullptr);
								}
							}
						else {
							ids += r.usedElements;
							}
						}
					else {
						return;
						}
					}
				});

			auto r = task.get_future();
			std::thread t{std::move(task)};
			t.detach();

			return r;
			}

		template<typename TGenerator>
		bool invoke(size_t count, std::shared_ptr<sync_buffer_type> ss, TGenerator && valgen) {

			std::cout << "generate source sequence{" << count << "}" << std::endl;

			std::vector<value_type> source(count);
			for (auto & v : source) {
				v = valgen();
				}

			std::cout << "run write(1)/read(1)" << std::endl;

			auto rwait = ReadThread(ss);
			auto wwait = WriteThread(ss, source);

			std::cout << "wait write completed..." << std::endl;
			wwait.wait();

			std::cout << "wait sync stream stay empty..." << std::endl;
			while (std::future_status::ready != rwait.wait_for(std::chrono::seconds(2))) {
				if (!_stopf) {
					if (ss->empty()) {
						_stopf = true;
						}
					}
				}

			std::cout << "get result from read stream" << std::endl;
			auto result = rwait.get();

			std::cout << "comparision: " << source.size() << std::endl;
			if (result.size() != source.size())
				return false;

			for (size_t i = 0; i < result.size(); ++i) {
				if (result.at(i) != source.at(i)) {
					return false;
					}
				}

			return true;
			}

	public:
		template<typename TGenerator>
		static bool execute(size_t count, std::shared_ptr<sync_buffer_type> ss, TGenerator && valgen) {
			test_mtqueue_onewrite_oneread t;
			return t.invoke(count, ss, std::forward<TGenerator>(valgen));
			}
	};
}
}

