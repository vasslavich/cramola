﻿#include <iostream>
#include <fstream>
#include <iterator>
#include <filesystem>
#include <appkit/appkit.h>
#include "datatypes.h"
#include "../../../usecase/ex_utility/rand_values.h"


namespace filesystem {
using namespace std::filesystem;
}

std::atomic<size_t> failCount{ 0 };
void result_add(bool r) {
	if(!r) {
		++failCount;

		throw std::exception();
		}
	}

template<typename T, typename S, typename O>
void deserialize_and_cmp_0(const std::list<T>& baseValues, S&& s, O = {}) {
	for(auto & bv : baseValues) {
		T m1;
		crm::srlz::deserialize(s, m1, O{});
		result_add(bv == m1);
		}
	}

template<typename TValue, typename O, typename ... TRStreamArgs>
void test_read(const std::list<TValue> & baseValues, O, TRStreamArgs && ... streamArgs) {
	deserialize_and_cmp_0(baseValues, crm::srlz::rstream_constructor_t::make(std::forward<TRStreamArgs>(streamArgs)...), O{});
	}

template<typename S, typename TContainer>
void test_read_to_end_2_container(S && r, const TContainer & bin1) {

	auto rbin1 = r.read<TContainer>();
	result_add(bin1 == rbin1);
	}

template<typename S, typename TContainer>
void test_read_to_end_2_iterator(S && r, const TContainer & bin1) {

	TContainer rbin2(bin1.size());
	if constexpr(crm::srlz::detail::is_contiguous_iterator_v<decltype(std::declval<TContainer>().cbegin())>) {
		result_add(rbin2.size() == r.read(rbin2));
		}
	else {
		auto[itlast, ic] = r.pop(rbin2.begin(), rbin2.end());
		result_add(rbin2.cend() == itlast);
		}

	result_add(rbin2 == bin1);
	}


template<typename TContainer>
void test_read_to_end(const TContainer & bin1) {
	test_read_to_end_2_container(crm::srlz::rstream_constructor_t::make(bin1), bin1);
	test_read_to_end_2_iterator(crm::srlz::rstream_constructor_t::make(bin1), bin1);

	test_read_to_end_2_container(crm::srlz::rstream_constructor_t::make(bin1.cbegin(), bin1.cend()), bin1);
	test_read_to_end_2_iterator(crm::srlz::rstream_constructor_t::make(bin1.cbegin(), bin1.cend()), bin1);

	test_read_to_end_2_container(crm::srlz::rstream_constructor_t::make(bin1.cbegin(), bin1.cend()), bin1);
	test_read_to_end_2_iterator(crm::srlz::rstream_constructor_t::make(bin1.cbegin(), bin1.cend()), bin1);

	test_read_to_end_2_container(crm::srlz::rstream_constructor_t::make(bin1), bin1);
	test_read_to_end_2_iterator(crm::srlz::rstream_constructor_t::make(bin1), bin1);
	}

template<typename T, typename O = crm::none_options>
T recycle(const T & v, O = {}) {
	auto ws0 = crm::srlz::wstream_constructor_t::make();
	crm::srlz::serialize(ws0, v, O{});

	T o;
	auto rs0 = crm::srlz::rstream_constructor_t::make(ws0.release());
	crm::srlz::deserialize(rs0, o, O{});

	return o;
	}

template<typename T, typename O>
void test_rw_base() {
	T v;
	fill(v);

	auto o = recycle(v, O{});

	if(!(o == v)) {
		throw std::exception{};
		}
	}

template<typename T, typename TWStream, typename O>
void test_1(const T& m1, TWStream ws1, O = {}) {
	static std::wstring fn = L"tmp1.bin";

	std::error_code errc;
	filesystem::remove(fn, errc);

	std::list<T> tl;

	crm::srlz::serialize(ws1, m1, O{});
	tl.push_back(m1);

	TWStream ws2;
	std::swap(ws1, ws2);

	crm::srlz::serialize(ws2, m1, O{});
	tl.push_back(m1);


	std::vector<uint8_t> bin1(ws2.sequence().cbegin(), ws2.sequence().cend());
	std::ofstream of(fn, std::ios::binary);
	of.write((const char*)bin1.data(), bin1.size());
	of.flush();
	of.close();

	std::cout << "flat data size(" << typeid(bin1).name() << ") " << bin1.size() << ", bytes" << std::endl;

	test_read(tl, O{}, bin1.cbegin(), bin1.cend());
	test_read(tl, O{}, bin1);
	test_read_to_end(bin1);

	std::ifstream inFile(fn, std::ios::binary);
	inFile.unsetf(std::ios::skipws);

	if(inFile) {
		test_read(tl, O{}, std::istream_iterator<std::uint8_t >(inFile), std::istream_iterator<std::uint8_t >{});
		}
	else {
		result_add(false);
		}
	}

static_assert(crm::srlz::write_size_v<crm::sx_uuid_t> < 32, __FILE_LINE__);
static_assert(crm::srlz::detail::is_container_of_isrlzd_v<std::vector<my_data_0_t>>, __FILE_LINE__);

template<typename TValue, typename O>
void test_x100() {
	for(size_t itc = 0; itc < 10; ++itc) {
		std::vector<TValue> m0(10);
		for(auto & m : m0)
			fill(m);

		auto ws0 = crm::srlz::wstream_constructor_t::make<std::vector<uint8_t>>();
		crm::srlz::serialize(ws0, m0, O{});
		auto ws0Bin = ws0.release();

		auto ws1 = crm::srlz::wstream_constructor_t::make<std::list<uint8_t>>();
		crm::srlz::serialize(ws1, m0, O{});
		auto ws1Bin = ws1.release();

		if(ws0Bin.size() != ws1Bin.size())
			throw std::exception();

		auto it1 = ws1Bin.cbegin();
		for(size_t i = 0; i < ws0Bin.size(); ++i, ++it1) {
			if(ws0Bin[i] != (*it1))
				throw std::exception();
			}

		std::vector<TValue> mX1, mX2, mX3;
		static_assert(crm::srlz::detail::is_resize_support_v<decltype(mX1)>, __FILE__);

		auto rs0 = crm::srlz::rstream_constructor_t::make(ws0Bin);
		crm::srlz::deserialize(rs0, mX1, O{});

		auto rs1 = crm::srlz::rstream_constructor_t::make(ws1Bin);
		crm::srlz::deserialize(rs1, mX2, O{});

		if(!(mX1 == mX2)) {
			throw std::exception();
			}

		auto rs2 = crm::srlz::rstream_constructor_t::make(ws1Bin.cbegin(), ws1Bin.cend());
		crm::srlz::deserialize(rs2, mX3, O{});

		if(!(mX1 == mX3)) {
			throw std::exception();
			}
		}
	}

template<typename T, typename TWstream, typename O>
void test_0_x(const T& t, TWstream s, O = {}) {
	test_1(t, std::move(s), O{});
	}

template<typename TValue, typename TContainer, typename O>
void test_0() {
	TValue m0;
	fill(m0);

	test_0_x(m0, crm::srlz::wstream_constructor_t::make<TContainer>(), O{});
	test_0_2(m0, O{});
	}

template<typename T, typename O>
void test_0_2(const T& v, O = {}) {
	size_t bcnt{ 0 };
	test_0_x(v, crm::srlz::wstream_constructor_t::make([&bcnt](uint8_t) {
		++bcnt;
		}), O{});
	}

template<typename T, typename O>
void test_main() {
	T m0;
	fill(m0);

	test_0<T, std::vector<uint8_t>, O>();
	test_0<T, std::list<uint8_t>, O>();
	}

template<typename TValue, typename TContainer, typename O>
void test_x20(const TValue & v) {

	auto rw = crm::srlz::wstream_constructor_t::make<TContainer>();

	TValue wm = v;
	crm::srlz::serialize(rw, wm, O{});

	auto rs = crm::srlz::rstream_constructor_t::make(rw.release());
	TValue rm;
	crm::srlz::deserialize(rs, rm, O{});

	if(wm != rm) {
		throw std::exception();
		}
	}

template<typename TValue, typename O>
void test_x20_1(const TValue& v, O = {}) {
	test_x20<TValue, std::vector<uint8_t>, O>(v);
	test_x20<TValue, std::list<uint8_t>, O>(v);
	}

template<typename O>
void test_x21() {

	std::list<uint8_t> lbuf;
	lbuf.resize(1024 * 1024);

	std::wstring is(L"wide string");

	auto iw = crm::srlz::wstream_constructor_t::make(lbuf.begin(), lbuf.end());
	crm::srlz::serialize(iw, is, O{});

	auto ir = iw.get_reader<std::vector<uint8_t>>();
	std::wstring os;
	crm::srlz::deserialize(ir, os, O{});

	if(is != os) {
		throw std::exception();
		}

	auto vb = iw.copy2 < std::vector<uint8_t>>();
	std::cout << "serialized size=" << vb.size() << std::endl;

	auto ir2 = crm::srlz::rstream_constructor_t::make(std::move(vb));
	crm::srlz::deserialize(ir2, os, O{});

	if(is != os) {
		throw std::exception();
		}
	}


/*! test of a struct with a member std::array of values T's type */
template<typename T,
	typename O = crm::none_options,
	typename _T = typename std::decay_t<T>,
	typename = std::void_t<decltype(make_rand(std::declval<_T&>()))>>
void test_0x(O = {}) {
	T in;
	make_rand(in);
	auto srlzLenOin = crm::srlz::object_trait_serialized_size(in, O{});

	auto ws = crm::srlz::wstream_constructor_t::make();
	crm::srlz::serialize(ws, in, O{});

	T outv;
	auto binary = ws.release();
	if(binary.size() != srlzLenOin) {
		throw std::exception{};
		}

	auto rs = crm::srlz::rstream_constructor_t::make(binary);
	crm::srlz::deserialize(rs, outv, O{});

	auto srlzLenOex = crm::srlz::object_trait_serialized_size(outv, O{});
	if(srlzLenOex != srlzLenOin) {
		throw std::exception();
		}

	if(in != outv) {
		throw std::exception();
		}
	}


template<typename TInt, typename Opt>
void main_test_nbytes_prefix() {
	using type = typename crm::srlz::detail::serializable_properties_prefix_trait_nbytes<TInt>;

	type v;
	static size_t fixsize = crm::srlz::write_size_v<type>;

	auto ws = crm::srlz::wstream_constructor_t::make();
	crm::srlz::serialize(ws, v, Opt{});

	if(fixsize != ws.size()) {
		throw std::exception{};
		}

	type o;
	auto rs = crm::srlz::rstream_constructor_t::make(ws.release());
	crm::srlz::deserialize(rs, o, Opt{});

	if(v.length != o.length || v.type != o.type)
		throw std::exception{};
	}

template<typename O>
void main_test_1() {
	test_rw_base<crm::sx_uuid_t, O>();
	test_rw_base<crm::sx_glob_64, O>();
	test_rw_base<crm::identity_descriptor_t, O>();
	test_rw_base<crm::address_descriptor_t, O>();

	test_0x<std::array<char, 10>, O>();
	test_0x<crm::sx_uuid_t, O>();
	test_0x<std::array<crm::sx_uuid_t, 2>, O>();

	test_x100<crm::address_hash_t, O>();
	test_x100<crm::identity_value_t, O>();
	test_x100<crm::address_descriptor_t, O>();
	test_x100<my_data_0_t, O>();
	test_x100<my_data_1_t, O>();

	test_x20_1(my_data_0_t{}, O{});
	test_x20_1(my_data_1_t{}, O{});

	test_x20_1(enum_class_1::bad, O{});
	test_x20_1(enum_1::enum_1_bad, O{});

	auto b16 = crm::sx_uuid_t::rand();
	test_x20_1(b16.u8_16[0], O{});
	test_x20_1(b16.u16_8[0], O{});
	test_x20_1(b16.u32_4[0], O{});
	test_x20_1(b16.u64_2[0], O{});
	test_x20_1(b16.to_wstr(), O{});
	test_x20_1(b16.to_str(), O{});
	test_x20_1(my_data_1_t{}, O{});

	test_main<crm::address_hash_t, O>();
	test_main<crm::identity_value_t, O>();
	test_main<crm::address_descriptor_t, O>();
	test_x21<O>();
	test_main<my_data_0_t, O>();
	test_main<my_data_1_t, O>();
	}

template<typename O>
void main_test_2() {

	crm::datagram_t din;
	din.add_value("0", crm::sx_uuid_t::rand());
	din.add_value("1", crm::sx_locid_t::rand());
	din.add_value("2", crm::sx_uuid_t::rand().to_str());
	din.add_value("3", crm::sx_glob_64::rand());
	din.add_value("4", crm::sx_loc_64::make());

	auto dout{ recycle(din, O{}) };
	}


template<typename T, typename O,
	typename = std::enable_if_t<crm::srlz::detail::is_strob_fixed_serialized_size_v<T>>>
void main_test_3(const T& v, O = {}) {
	constexpr static size_t fixsize = crm::srlz::write_size_v<T>;

	auto ws = crm::srlz::wstream_constructor_t::make();
	crm::srlz::serialize(ws, v, O{});

	if(fixsize != ws.size()) {
		throw std::exception{};
		}

	T o;
	auto rs = crm::srlz::rstream_constructor_t::make(ws.release());
	crm::srlz::deserialize(rs, o, O{});
	}

template<typename Input, typename Output, typename O>
void main_test_4(const Input& in, Output& out, O = {}) {
	auto ws = crm::srlz::wstream_constructor_t::make();
	crm::srlz::serialize(ws, in, O{});

	Output o;
	auto rs = crm::srlz::rstream_constructor_t::make(ws.release());
	crm::srlz::deserialize(rs, o, O{});
	}

struct Fixsized1 {
	int i;
	double d;
	char c;
	};

struct Fixsized2 {
	std::array<char, 2> ca;
	size_t l;
	};

struct Fixsized3 {
	Fixsized1 s1;
	Fixsized2 s2;
	};

struct Fixsized4 {
	std::array<Fixsized1, 2> as1;
	std::array<Fixsized2, 3> as2;
	Fixsized3 s3;
	};



int main() {

	crm::irequest_t r;
	main_test_4(usecase::rand_request(), r, crm::none_options{});
	main_test_4(usecase::rand_request(), r, crm::without_prefix{});

	main_test_nbytes_prefix<uint8_t, crm::without_prefix>();
	main_test_nbytes_prefix<uint8_t, crm::none_options>();

	main_test_1<crm::none_options>();
	main_test_1<crm::without_prefix>();

	main_test_2<crm::none_options>();
	main_test_2<crm::without_prefix>();

	main_test_3(Fixsized1{}, crm::none_options{});
	main_test_3(Fixsized1{}, crm::without_prefix{});

	main_test_3(Fixsized2{}, crm::none_options{});
	main_test_3(Fixsized2{}, crm::without_prefix{});

	main_test_3(Fixsized3{}, crm::none_options{});
	main_test_3(Fixsized3{}, crm::without_prefix{});

	main_test_3(Fixsized4{}, crm::none_options{});
	main_test_3(Fixsized4{}, crm::without_prefix{});

	std::cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << std::endl;
	std::cout << "                       press <enter>                      " << std::endl;
	std::cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << std::endl;

	getchar();
	}


