#pragma once


#include <appkit/appkit.h>


enum class enum_class_1 : uint8_t {
	lower_bound, bad, ok, upper_bound
	};

enum enum_1 : uint8_t {
	lower_bound, enum_1_bad, enum_1_ok, upper_bound
	};


struct my_data_0_t final {
	std::vector<wchar_t> a;
	std::array<uint64_t,10> b;
	std::array<crm::sx_uuid_t, 10> c;

	template<typename S>
	void srlz(S && dest)const {
		auto ax = a;
		ax.resize(a.size() * 2);
		crm::srlz::serialize(dest, ax);
		crm::srlz::serialize(dest, b);

		static_assert(crm::srlz::detail::is_container_of_isrlzd_v<decltype(c)>, __FILE__);
		crm::srlz::serialize(dest, c);
		}

	template<typename S>
	void dsrlz(S && src ) {
		crm::srlz::deserialize(src, a);
		crm::srlz::deserialize(src, b);
		crm::srlz::deserialize(src, c);
		}

	size_t get_serialized_size()const noexcept {
		return crm::srlz::object_trait_serialized_size(a)
			+ crm::srlz::object_trait_serialized_size(b)
			+ crm::srlz::object_trait_serialized_size(c);
		}
	};

bool operator ==(const my_data_0_t & l, const my_data_0_t & r) {
	return l.a == r.a && l.b == r.b && l.c == r.c;
	}

bool operator!=(const my_data_0_t &l, const my_data_0_t & r) {
	return !(l == r);
	}

void fill(my_data_0_t & d) {
	for(auto & v : d.a) {
		v = crm::sx_uuid_t::rand().u8_16[0];
		}

	for(auto ib : d.b) {
		ib = crm::sx_uuid_t::rand().u64_2[1];
		}

	for(auto ic : d.c) {
		ic = crm::sx_uuid_t::rand();
		}
	}

struct my_data_1_t final {
	std::string s;
	std::vector<uint8_t> v1;
	std::vector<uint16_t> v2;
	std::vector<uint32_t> v4;
	std::vector<uint64_t> v8;
	std::wstring ws;
	double d;
	size_t us;
	int i;
	std::uint8_t u1;
	std::uint16_t u2;
	std::uint32_t u4;
	std::uint64_t u8;

	my_data_0_t md0;

	template<typename S>
	void srlz(S && dest)const {
		crm::srlz::serialize(dest, s);
		crm::srlz::serialize(dest, v1);
		crm::srlz::serialize(dest, v2);
		crm::srlz::serialize(dest, v4);
		crm::srlz::serialize(dest, v8);
		crm::srlz::serialize(dest, ws);
		crm::srlz::serialize(dest, d);
		crm::srlz::serialize(dest, us);
		crm::srlz::serialize(dest, i);
		crm::srlz::serialize(dest, u1);
		crm::srlz::serialize(dest, u2);
		crm::srlz::serialize(dest, u4);
		crm::srlz::serialize(dest, u8);
		crm::srlz::serialize(dest, md0);
		}

	template<typename S>
	void dsrlz(S && src) {
		crm::srlz::deserialize(src, s);
		crm::srlz::deserialize(src, v1);
		crm::srlz::deserialize(src, v2);
		crm::srlz::deserialize(src, v4);
		crm::srlz::deserialize(src, v8);
		crm::srlz::deserialize(src, ws);
		crm::srlz::deserialize(src, d);
		crm::srlz::deserialize(src, us);
		crm::srlz::deserialize(src, i);
		crm::srlz::deserialize(src, u1);
		crm::srlz::deserialize(src, u2);
		crm::srlz::deserialize(src, u4);
		crm::srlz::deserialize(src, u8);
		crm::srlz::deserialize(src, md0);
		}

	size_t get_serialized_size()const noexcept {
		return crm::srlz::object_trait_serialized_size(s)
			+ crm::srlz::object_trait_serialized_size(v1)
			+ crm::srlz::object_trait_serialized_size(v2)
			+ crm::srlz::object_trait_serialized_size(v4)
			+ crm::srlz::object_trait_serialized_size(v8)
			+ crm::srlz::object_trait_serialized_size(ws)
			+ crm::srlz::object_trait_serialized_size(d)
			+ crm::srlz::object_trait_serialized_size(us)
			+ crm::srlz::object_trait_serialized_size(i)
			+ crm::srlz::object_trait_serialized_size(u1)
			+ crm::srlz::object_trait_serialized_size(u2)
			+ crm::srlz::object_trait_serialized_size(u4)
			+ crm::srlz::object_trait_serialized_size(u8)
			+ crm::srlz::object_trait_serialized_size(md0);
		}
	};

bool operator ==(const my_data_1_t & l, const my_data_1_t & r) {
	return l.s == r.s
		&& l.v1 == r.v1
		&& l.v2 == r.v2
		&& l.v4 == r.v4
		&& l.v8 == r.v8
		&& l.ws == r.ws
		&& l.d == r.d
		&& l.us == r.us
		&& l.i == r.i
		&& l.u1 == r.u1
		&& l.u2 == r.u2
		&& l.u4 == r.u4
		&& l.u8 == r.u8
		&& l.md0 == r.md0;
	}

bool operator!=(const my_data_1_t &l, const my_data_1_t & r) {
	return !(l == r);
	}

void fill(my_data_1_t & d) {
	d.s = crm::sx_uuid_t::rand().to_str();

	d.v1.resize(crm::sx_uuid_t::rand().u8_16[0]);
	for(auto & v : d.v1) {
		v = crm::sx_uuid_t::rand().u8_16[1];
		}

	d.v2.resize(crm::sx_uuid_t::rand().u8_16[0]);
	for(auto & v : d.v2) {
		v = crm::sx_uuid_t::rand().u16_8[1];
		}

	d.v4.resize(crm::sx_uuid_t::rand().u8_16[0]);
	for(auto & v : d.v4) {
		v = crm::sx_uuid_t::rand().u32_4[1];
		}

	d.v8.resize(crm::sx_uuid_t::rand().u8_16[0]);
	for(auto & v : d.v8) {
		v = crm::sx_uuid_t::rand().u64_2[1];
		}

	d.ws = crm::sx_uuid_t::rand().to_wstr();
	d.d = 0.0001111;
	d.us = crm::sx_uuid_t::rand().u64_2[0];
	d.i = crm::sx_uuid_t::rand().u32_4[1];

	d.u1 = crm::sx_uuid_t::rand().u8_16[0];
	d.u2 = crm::sx_uuid_t::rand().u16_8[0];
	d.u4 = crm::sx_uuid_t::rand().u32_4[0];
	d.u8 = crm::sx_uuid_t::rand().u64_2[0];

	fill(d.md0);
	}

void fill(std::string& s) {
	s = crm::sx_uuid_t::rand().to_str();
	}

void fill(std::vector<uint8_t>& b) {
	auto cb = crm::sx_uuid_t::rand().to_bytes<uint8_t, 16>();
	b = std::vector<uint8_t>(cb.cbegin(), cb.cend());
	}

void fill(crm::identity_payload_entry_t& ipe) {
	fill(ipe.key);
	fill(ipe.data);
	}

void fill(crm::identity_payload_t& ip) {
	ip.collection.resize(1);
	for(auto& item : ip.collection) {
		fill(item);
		}
	}

void fill(crm::identity_descriptor_t& id) {
	id = crm::identity_descriptor_t(crm::global_object_identity_t::rand(),
		crm::sx_uuid_t::rand().to_str());
	}

void fill(crm::identity_value_t& iv) {
	fill(iv.id_value);
	fill(iv.properties);
	}

void fill(crm::session_description_t& sd) {
	sd.set_id(crm::session_id_t::make());
	sd.set_name(crm::global_object_identity_t::rand().to_str());
	}

void fill(crm::address_descriptor_t& iv) {
	iv.set_id(crm::global_object_identity_t::rand());

	crm::identity_descriptor_t did;
	fill(did);
	iv.set_destination_id(did);

	fill(did);
	iv.set_source_id(did);

	crm::session_description_t sd;
	fill(sd);
	iv.set_session(sd);
	}


void fill(crm::address_hash_t& h) {
	h = crm::address_hash_t::rand();
	}

void fill(crm::sx_uuid_t& u) {
	u = crm::sx_uuid_t::rand();
	}

/*! random initialize std::array of integrals */
template<typename T,
	typename std::enable_if_t<std::is_integral_v<std::decay_t<T>>, int> = 0>
	void make_rand(T& a) {
	a = crm::sx_uuid_t::rand().at<T>(0);
	}

/*! random initialize std::array of std::string's initialized values */
template<typename T,
	typename std::enable_if_t<std::is_convertible_v<std::string, std::decay_t<T>>, int> = 0>
	void make_rand(T& a) {
	a = crm::sx_uuid_t::rand().to_str();
	}

void make_rand(crm::sx_uuid_t& a) {
	a = crm::sx_uuid_t::rand();
	}

/*! random initialize std::array of make_rand's initialized */
template<typename T, const size_t N = 8,
	typename _T = typename std::decay_t<T>,
	typename = std::void_t<decltype(make_rand(std::declval<_T&>()))>>
	void make_rand(std::array<T, N>& a) {
	for(size_t i = 0; i < N; ++i) {
		make_rand(a[i]);
		}
	}

namespace crm {
bool operator==(const identity_value_t& l, const identity_value_t& r)noexcept {
	if(l.id_value != r.id_value)
		return false;

	if(l.properties.collection.size() != r.properties.collection.size())
		return false;

	for(size_t i = 0; i < l.properties.collection.size(); ++i)
		if(l.properties.collection[i].data != r.properties.collection[i].data ||
			l.properties.collection[i].key != r.properties.collection[i].key)
			return false;

	return true;
	}
}
