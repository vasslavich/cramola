project(tst_ipc_channel_0_hub CXX)

################################################################################
# Source groups
################################################################################
set(no_group_source_files
    "tst_ipc_channel_0_hub.cpp"
)
source_group("" FILES ${no_group_source_files})

set(ALL_FILES
    ${no_group_source_files}
)

################################################################################
# Target
################################################################################
add_executable(${PROJECT_NAME} ${ALL_FILES})
set_target_properties(${PROJECT_NAME} PROPERTIES FOLDER "testsuite/tst_ipc_channels")

add_precompiled_header(${PROJECT_NAME} "pch.h" ".")

use_props(${PROJECT_NAME} "${CMAKE_CONFIGURATION_TYPES}" "${DEFAULT_CXX_PROPS}")
################################################################################
# Includes for CMake from *.props
################################################################################
if("${CMAKE_VS_PLATFORM_NAME}" STREQUAL "x64")
    use_props(${PROJECT_NAME} Debug             "../../../msvs/properties/general.cmake")
    use_props(${PROJECT_NAME} Debug             "../../../msvs/properties/debug.cmake")
    use_props(${PROJECT_NAME} Release_optimized "../../../msvs/properties/general.cmake")
    use_props(${PROJECT_NAME} Release_optimized "../../../msvs/properties/release_x64_optimized.cmake")
    use_props(${PROJECT_NAME} Release_with_dbg  "../../../msvs/properties/general.cmake")
    use_props(${PROJECT_NAME} Release_with_dbg  "../../../msvs/properties/release_x64_with_dbg.cmake")
    use_props(${PROJECT_NAME} Release           "../../../msvs/properties/general.cmake")
elseif("${CMAKE_VS_PLATFORM_NAME}" STREQUAL "x86")
    use_props(${PROJECT_NAME} Debug             "../../../msvs/properties/general.cmake")
    use_props(${PROJECT_NAME} Release_optimized "../../../msvs/properties/general.cmake")
    use_props(${PROJECT_NAME} Release_with_dbg  "../../../msvs/properties/general.cmake")
    use_props(${PROJECT_NAME} Release           "../../../msvs/properties/general.cmake")
endif()

set(ROOT_NAMESPACE tstipcchannel0hub)

set_target_properties(${PROJECT_NAME} PROPERTIES
    VS_GLOBAL_KEYWORD "Win32Proj"
)
if("${CMAKE_VS_PLATFORM_NAME}" STREQUAL "x64")
    set_target_properties(${PROJECT_NAME} PROPERTIES
        INTERPROCEDURAL_OPTIMIZATION_RELEASE_OPTIMIZED "TRUE"
        INTERPROCEDURAL_OPTIMIZATION_RELEASE_WITH_DBG  "TRUE"
        INTERPROCEDURAL_OPTIMIZATION_RELEASE           "TRUE"
    )
elseif("${CMAKE_VS_PLATFORM_NAME}" STREQUAL "x86")
    set_target_properties(${PROJECT_NAME} PROPERTIES
        INTERPROCEDURAL_OPTIMIZATION_RELEASE_OPTIMIZED "TRUE"
        INTERPROCEDURAL_OPTIMIZATION_RELEASE_WITH_DBG  "TRUE"
        INTERPROCEDURAL_OPTIMIZATION_RELEASE           "TRUE"
    )
endif()
################################################################################
# Include directories
################################################################################
if("${CMAKE_VS_PLATFORM_NAME}" STREQUAL "x64")
    target_include_directories(${PROJECT_NAME} PUBLIC
        "$<$<CONFIG:Debug>:"
            "$ENV{BOOST_DBG_X64_INC}"
        ">"
        "$<$<CONFIG:Release_optimized>:"
            "$ENV{BOOST_RLZ_OPTZD_X64_INC}"
        ">"
        "$<$<CONFIG:Release_with_dbg>:"
            "$ENV{BOOST_RLZ_OPTZD_X64_INC}"
        ">"
    )
endif()

################################################################################
# Compile definitions
################################################################################
if("${CMAKE_VS_PLATFORM_NAME}" STREQUAL "x64")
    target_compile_definitions(${PROJECT_NAME} PRIVATE
        "$<$<CONFIG:Release>:"
            "NDEBUG"
        ">"
        "_CONSOLE;"
        "UNICODE;"
        "_UNICODE"
    )
elseif("${CMAKE_VS_PLATFORM_NAME}" STREQUAL "x86")
    target_compile_definitions(${PROJECT_NAME} PRIVATE
        "$<$<CONFIG:Debug>:"
            "_DEBUG"
        ">"
        "$<$<CONFIG:Release_optimized>:"
            "NDEBUG"
        ">"
        "$<$<CONFIG:Release_with_dbg>:"
            "NDEBUG"
        ">"
        "$<$<CONFIG:Release>:"
            "NDEBUG"
        ">"
        "WIN32;"
        "_CONSOLE;"
        "UNICODE;"
        "_UNICODE"
    )
endif()

################################################################################
# Compile and link options
################################################################################
if(MSVC)
    if("${CMAKE_VS_PLATFORM_NAME}" STREQUAL "x64")
        target_compile_options(${PROJECT_NAME} PRIVATE
            $<$<CONFIG:Debug>:
                ${DEFAULT_CXX_DEBUG_RUNTIME_LIBRARY}
            >
            $<$<CONFIG:Release_optimized>:
                ${DEFAULT_CXX_RUNTIME_LIBRARY}
            >
            $<$<CONFIG:Release_with_dbg>:
                ${DEFAULT_CXX_RUNTIME_LIBRARY}
            >
            $<$<CONFIG:Release>:
                /permissive-;
                /O2;
                /Oi;
                /sdl;
                ${DEFAULT_CXX_RUNTIME_LIBRARY};
                /Gy;
                /W3
            >
            ${DEFAULT_CXX_DEBUG_INFORMATION_FORMAT};
            ${DEFAULT_CXX_EXCEPTION_HANDLING}
        )
    elseif("${CMAKE_VS_PLATFORM_NAME}" STREQUAL "x86")
        target_compile_options(${PROJECT_NAME} PRIVATE
            $<$<CONFIG:Debug>:
                /Od;
                ${DEFAULT_CXX_DEBUG_RUNTIME_LIBRARY}
            >
            $<$<CONFIG:Release_optimized>:
                /O2;
                /Oi;
                ${DEFAULT_CXX_RUNTIME_LIBRARY};
                /Gy
            >
            $<$<CONFIG:Release_with_dbg>:
                /O2;
                /Oi;
                ${DEFAULT_CXX_RUNTIME_LIBRARY};
                /Gy
            >
            $<$<CONFIG:Release>:
                /O2;
                /Oi;
                ${DEFAULT_CXX_RUNTIME_LIBRARY};
                /Gy
            >
            /permissive-;
            /sdl;
            /W3;
            ${DEFAULT_CXX_DEBUG_INFORMATION_FORMAT};
            ${DEFAULT_CXX_EXCEPTION_HANDLING}
        )
    endif()
    if("${CMAKE_VS_PLATFORM_NAME}" STREQUAL "x64")
        target_link_options(${PROJECT_NAME} PRIVATE
            $<$<CONFIG:Debug>:
                /DEBUG:FULL
            >
            $<$<CONFIG:Release_optimized>:
                /DEBUG:FULL
            >
            $<$<CONFIG:Release_with_dbg>:
                /DEBUG:FULL
            >
            $<$<CONFIG:Release>:
                /DEBUG;
                /OPT:REF;
                /OPT:ICF;
                /INCREMENTAL:NO
            >
            /SUBSYSTEM:CONSOLE
        )
    elseif("${CMAKE_VS_PLATFORM_NAME}" STREQUAL "x86")
        target_link_options(${PROJECT_NAME} PRIVATE
            $<$<CONFIG:Debug>:
                /INCREMENTAL
            >
            $<$<CONFIG:Release_optimized>:
                /OPT:REF;
                /OPT:ICF;
                /INCREMENTAL:NO
            >
            $<$<CONFIG:Release_with_dbg>:
                /OPT:REF;
                /OPT:ICF;
                /INCREMENTAL:NO
            >
            $<$<CONFIG:Release>:
                /OPT:REF;
                /OPT:ICF;
                /INCREMENTAL:NO
            >
            /DEBUG;
            /SUBSYSTEM:CONSOLE
        )
    endif()
endif()

################################################################################
# Dependencies
################################################################################
add_dependencies(${PROJECT_NAME}
    appkit
    hublon
    ex_specialization
    ex_utility
)

# Link with other targets.
target_link_libraries(${PROJECT_NAME} PUBLIC
    appkit
    hublon
    ex_specialization
    ex_utility
)

