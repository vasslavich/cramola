﻿#include <hublon/internal_terms.h>
#include <hublon/r0.h>
#include <iostream>
#include <chrono>
#include "../../../usecase/ex_specialization/classes.h"
#include "../../../usecase/ex_utility/test_struct_1.h"


using namespace std::chrono;


std::shared_ptr<crm::distributed_ctx_t> make_context( const crm::identity_descriptor_t & hostId,
	bool ping ){

	crm::distributed_ctx_policies_t policies;
	policies.set_threadpool_maxsize( 16 );
	policies.set_threadpool_user_handlers_maxsize( 16 );
	policies.set_io_buffer_maxsize( 1024 );
	policies.set_io_threads_count( 1 );

	policies.set_ping_timeline( crm::sndrcv_timeline_periodicity_t::make_cyclic_timeouted( 60s, 10s ) );
	policies.set_rt_ping( ping );

	return crm::distributed_ctx_t::make(crm::context_bindings(hostId), std::move(policies));
	}

using break_token = crm::break_cancel;
break_token mk_cancel_token(){
	return break_token([]{return false; });
	}

void test_2(){
	auto ctx = make_context({crm::global_object_identity_t::rand(), "hub"}, false);

	crm::detail::xchannel_shmem_host_args hubargs;
	hubargs.xchannelName.mainNodeName = "host-channel";

	crm::detail::xchannel_shmem_host_first hub( ctx, hubargs );
	hub.open( 4 );

	size_t num{ 0 };
	while( true ){
		num++;

		usecase::test_struct_1 s;
		static_assert(crm::srlz::is_serializable_v<decltype(s)>, __FILE__);

		if (hub.pop(s, mk_cancel_token())) {
			if( s.name.empty() || s.name != std::to_string( s.bin.size() ) ){
				THROW_EXC_FWD(nullptr);
				}

			CBL_VERIFY(num == s.counter);

			std::cout << "read, count:" << num << std::endl;
			}
		}

	std::cout << "press any key..." << std::endl;
	std::getchar();

	hub.close();
	}

int main(){
	test_2();
	}

