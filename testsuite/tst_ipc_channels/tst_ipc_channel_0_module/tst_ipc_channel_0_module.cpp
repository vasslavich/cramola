﻿#include <hublon/internal_terms.h>
#include <hublon/r1.h>
#include <iostream>
#include <chrono>
#include "../../../usecase/ex_specialization/classes.h"
#include "../../../usecase/ex_utility/test_struct_1.h"


using namespace std::chrono;

std::shared_ptr<crm::distributed_ctx_t> make_context( const crm::identity_descriptor_t & hostId, 
	bool ping ){

	crm::distributed_ctx_policies_t policies;
	policies.set_threadpool_maxsize( 16 );
	policies.set_threadpool_user_handlers_maxsize( 16 );
	policies.set_io_buffer_maxsize( 1024 );
	policies.set_io_threads_count( 1 );

	policies.set_ping_timeline( crm::sndrcv_timeline_periodicity_t::make_cyclic_timeouted( 60s, 10s ) );
	policies.set_rt_ping( ping );

	return crm::distributed_ctx_t::make(crm::context_bindings(hostId), std::move(policies));
	}

constexpr size_t MaxDataSize = 1024 * 10;
size_t dataSize{1};

size_t data_size() {
	if (auto nd = ((dataSize + 1) % MaxDataSize)) {
		dataSize = nd;
		}
	else {
		dataSize = 1;
		}

	return dataSize;
	}

using break_token = crm::break_cancel;
break_token mk_cancel_token(){
	return break_token( []{return false; } );
	}

void test_2(){
	auto ctx = make_context( { crm::global_object_identity_t::rand(), "module" }, false );

	crm::detail::xchannel_shmem_host_args hubargs;
	hubargs.xchannelName.mainNodeName = "host-channel";

	crm::detail::xchannel_shmem_host_second hub( ctx, hubargs );
	hub.open();

	size_t num{ 0 };
	while( true ){
		usecase::test_struct_1 s;
		s.counter = ++num;
		static_assert(crm::srlz::detail::is_strob_serializable_v<decltype(s)>, __FILE_LINE__);
		static_assert(crm::srlz::is_serializable_v<decltype(s)>, __FILE_LINE__);

		auto rnd = crm::sx_uuid_t::rand();
		std::vector<uint8_t> buf;
		const size_t ld = data_size();
		while (buf.size() < ld) {
			for (size_t i = 0; i < crm::sx_uuid_t::plane_size; ++i) {
				auto b = rnd.at<uint8_t>(i);
				buf.push_back(b);

				auto br = rnd.at<uint8_t>(crm::sx_uuid_t::plane_size - 1 - i);
				buf.push_back(br);
				}
			}

		s.bin = std::move( buf );
		s.name = std::to_string( s.bin.size() );

		if (hub.push(std::move(s), mk_cancel_token() )) {
			std::cout << "write, count:" << num << std::endl;
			}
		}

	hub.close();

	std::cout << "press any key..." << std::endl;
	std::getchar();
	}

int main(){
	test_2();
	}
