﻿#include <iostream>
#include <appkit/utilities/utilities.h>


using namespace crm;
using namespace crm::detail;

char cbuf[10];
char text[] = "mailto";

int main(){
	cbuf[1] = '\0';

	auto s = stringcat("1,", std::string("2,"), "3,", 4);
	std::cout << s << std::endl;
	std::cout << stringcat( 0.6, 1, "d", std::string{} );
	std::cout << stringcat(0.6, 1, "d", cbuf, text);

    std::cout << "Hello World!\n"; 

	std::getchar();
	return 0;
}

