#include <appkit/appkit.h>
#include <appkit/uniactors.h>
#include "../../../usecase/ex_specialization/classes.h"
#include "../../../usecase/ex_utility/rand_values.h"


using namespace std::chrono;

crm::identity_descriptor_t localhostId_RT0 = crm::identity_descriptor_t{ crm::global_object_identity_t::as_hash("FAFA0000-0A0A-F1F1-AAAA-100000000001"), "host:0" };


template<typename O>
bool test(std::shared_ptr<crm::distributed_ctx_t> & c, O&& o) {
	crm::detail::message2stream m2s(c);
	crm::detail::stream2message s2m(c);

	m2s.generate_session_state();

	o.set_source_subscriber_id(usecase::rand_subscriber_id());

	auto s = m2s.push(std::forward<O>(o));

	std::vector < std::unique_ptr < crm:: i_iol_ihandler_t >> vm;
	bool readyf = false;
	size_t parts = s.size() > 10 ? 10 : s.size();
	size_t offset = 0;
	size_t partsize = s.size() / parts;

	while (!readyf) {
		auto offb = s.cbegin() + offset;
		auto count = std::min(partsize, s.size() - offset);
		auto offe = s.cbegin() + offset + count;
		offset += count;

		std::vector<uint8_t> tmp(offb,offe );

		vm = s2m.decode(tmp, tmp.size());
		if (!vm.empty() || (offset == s.size())) {
			readyf = true;
			}
		}

	if (vm.empty()) {
		return false;
		}

	return true;
	}

int main() {
	auto c = make_context(localhostId_RT0, true);

	if (!test(c, usecase::rand_command_message())) {
		throw std::exception();
		}
	if (!test(c, usecase::rand_datagram_message())) {
		throw std::exception();
		}

	return 0;
	}


