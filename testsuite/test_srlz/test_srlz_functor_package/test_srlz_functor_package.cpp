#include <appkit/appkit.h>
#include <appkit/functorapp/handlers_map.h>


using namespace crm;


struct pod_struct {
	std::vector<uint8_t> v;
	std::string s;
	};

auto mkpod_struct(){
	return pod_struct{ crm::sx_uuid_t::rand().to_bytes<std::vector<uint8_t>>(), crm::sx_uuid_t::rand().to_str() };
	}

void test0() {
	crm::detail::functor_tuple tplpack((int)9, (double)0.11, std::string{ "axea" });
	auto [i, d, s] = tplpack.load_to_tuple<int, double, std::string>();

	if (i != 9 || d != 0.11 || s != "axea") {
		throw std::exception();
		}
	}

void test1() {
	auto p = detail::make_ofunctor_package(function_descriptor{ L"test" }, (int)10, std::string{ "test 1string" }, std::wstring{ L"test 2string" }, mkpod_struct());

	auto ws = crm::srlz::wstream_constructor_t::make();
	srlz::serialize(ws, p);
	auto wbin = ws.release();

	decltype(p) p2;
	auto rs = srlz::rstream_constructor_t::make(std::move(wbin));
	srlz::deserialize(rs, p2);

	auto p3 = detail::transform(std::move(p2));

	//auto p3tup = std::move(p3).to_handler().load_to_tuple<int, std::string, std::wstring, pod_struct>();
	std::move(p3).to_handler().apply([](int i, const std::string& s, std::wstring&& ws, pod_struct&& pd) {
		std::wcout << i << L":" << crm::cvt(s) << L":" << ws << L":" << pd.v.size() << L":" << crm::cvt(pd.s) << std::endl;
		});
	}

void test2() {
	crm::detail::handlers_map hmap;
	hmap.add_handler(function_descriptor{ L"test function" }, [](int i, const std::string& s, std::wstring&& ws, pod_struct&& pd) {
		std::wcout << i << L":" << crm::cvt(s) << L":" << ws << L":" << pd.v.size() << L":" << crm::cvt(pd.s) << std::endl;
		return pod_struct{ crm::sx_uuid_t::rand().to_bytes<std::vector<uint8_t>>(), "result of invoke[" + std::to_string(i) + "]" };
		});

	auto p = detail::make_ofunctor_package(function_descriptor{ L"test function" }, (int)10, std::string{ "test 1string" }, std::wstring{ L"test 2string" }, mkpod_struct());

	auto ws = crm::srlz::wstream_constructor_t::make();
	srlz::serialize(ws, p);
	auto wbin = ws.release();

	decltype(p) p2;
	auto rs = srlz::rstream_constructor_t::make(std::move(wbin));
	srlz::deserialize(rs, p2);

	auto p3 = detail::transform(std::move(p2));
	auto r = hmap.invoke(std::move(p3).to_handler()).as<pod_struct>();

	std::cout << "result invoke:" << r.s << std::endl;
	}

int main() {
	test0();
	test1();
	test2();

	return 0;
	}


