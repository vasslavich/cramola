#include <appkit/appkit.h>


using namespace crm;


constexpr size_t f33 = (uint32_t)(std::numeric_limits<uint32_t>::max())
& (((uint32_t)(-1)) >> 1);

constexpr bool f33bool = f33 > (std::numeric_limits<uint32_t>::max() - 100000);

int main() {
	constexpr auto lu1 = srlz::detail::serializable_properties_prefix_bytes_maxsize<uint8_t>();
	constexpr auto lu2 = srlz::detail::serializable_properties_prefix_bytes_maxsize<uint16_t>();
	constexpr auto lu4 = srlz::detail::serializable_properties_prefix_bytes_maxsize<uint32_t>();
	constexpr auto lu8 = srlz::detail::serializable_properties_prefix_bytes_maxsize<uint64_t>();

	constexpr auto lim1 = std::numeric_limits<uint8_t>::max();
	constexpr auto lim2 = std::numeric_limits<uint16_t>::max();
	constexpr auto lim4 = std::numeric_limits<uint32_t>::max();
	constexpr auto lim8 = std::numeric_limits<uint64_t>::max();


	std::cout << std::setw(10) << "u1 " << std::hex << lu1 << std::endl;
	std::cout << std::setw(10) << "lim1 " << std::hex << lim1 << std::endl;

	std::cout << std::setw(10) << "u2 " << std::hex << lu2 << std::endl;
	std::cout << std::setw(10) << "lim2 " << std::hex << lim2 << std::endl;

	std::cout << std::setw(10) << "u4 " << std::hex << lu4 << std::endl;
	std::cout << std::setw(10) << "lim4 " << std::hex << lim4 << std::endl;

	std::cout << std::setw(10) << "u8 " << std::hex << lu8 << std::endl;
	std::cout << std::setw(10) << "lim8 " << std::hex << lim8 << std::endl;

	std::cout << std::setw(10) << "what="
		<< std::dec << std::endl
		<< std::setw(10) << "prefix:" << srlz::detail::serializable_properties_prefix_bytes_maxsize<uint32_t>() << std::endl
		<< std::setw(10) << "std lim:" << (std::numeric_limits<uint32_t>::max() - 100000) << std::endl;

	constexpr bool f1 = srlz::detail::serializable_properties_prefix_bytes_maxsize<uint32_t>() < std::numeric_limits<uint32_t>::max();
	bool f2 = srlz::detail::serializable_properties_prefix_bytes_maxsize<uint32_t>() < (std::numeric_limits<uint32_t>::max() - 100000);


	std::cout << f1			<< "(constexpr:     false)" << std::endl;
	std::cout << f2			<< "(not constexpr: true)" << std::endl;
	std::cout << f33bool	<< "(constexpr:     false)" << std::endl;

	getchar();
	return 0;
	}

