#include <appkit/appkit.h>




bool test1() {
	std::string s1{ "test string" };
	std::wstring ws1{ L"test wide string" };
	int i = 10;

	auto srlzs1 = crm::srlz::object_trait_serialized_size(s1)
		+ crm::srlz::object_trait_serialized_size(ws1)
		+ crm::srlz::object_trait_serialized_size(i);

	auto srlzs2 = crm::srlz::object_trait_serialized_size_all(s1, ws1, i);
	auto srlzs3 = crm::srlz::object_trait_serialized_size(std::make_tuple(s1, ws1, i));

	return srlzs1 == srlzs2 && srlzs1 == srlzs3;
	}

bool test2() {
	std::string s1{ "test string" };
	std::wstring ws1{ L"test wide string" };
	int i = 10;

	auto tpl1 = std::make_tuple(s1, ws1, i);

	auto ws = crm::srlz::wstream_constructor_t::make();
	crm::srlz::serialize(ws, tpl1);
	
	decltype(tpl1) tpl2;
	auto rs = crm::srlz::rstream_constructor_t::make(ws.release());

	crm::srlz::deserialize(rs, tpl2);

	return
		((std::get<0>(tpl1) == std::get<0>(tpl2)) && (std::get<0>(tpl1) == s1)) &&
		((std::get<1>(tpl1) == std::get<1>(tpl2)) && (std::get<1>(tpl1) == ws1)) &&
		((std::get<2>(tpl1) == std::get<2>(tpl2)) && (std::get<2>(tpl1) == i));
	}

bool test3() {
	std::string s1{ "test string" }, s2;
	std::wstring ws1{ L"test wide string" }, ws2;
	int i = 10, i2;

	auto ws = crm::srlz::wstream_constructor_t::make();
	crm::srlz::serialize_all(ws, s1, ws1, i);

	auto rs = crm::srlz::rstream_constructor_t::make(ws.release());
	crm::srlz::deserialize_all(rs, s2, ws2, i2);

	return
		(s2 == s1) &&
		(ws2 == ws1) &&
		(i2 == i);
	}

template<typename Tm, std::size_t ... I>
void print_vals(const Tm & m, std::index_sequence<I...> ) {
	auto f_ = [](size_t idx, auto& i) {
		std::cout << "index=" << idx << ", typeid=" << typeid(i).name() << ":" << i << std::endl;
		};


	(f_(I, m.value<I>()), ...);
	}

template<typename tuple_type, std::size_t ... I>
bool test4_unroll(tuple_type &&, std::index_sequence<I...>) {

	using imessage_type = crm::detail::istrob_message_envelop_t<std::tuple_element_t<I, std::decay_t<tuple_type>>...>;
	using omessage_type = crm::detail::ostrob_message_envelop_t<std::tuple_element_t<I, std::decay_t<tuple_type>>...>;

	print_vals(imessage_type{}, std::make_index_sequence<std::tuple_size_v<tuple_type>>{});

	return true;
	}

template<typename THandler>
bool test4_unroll_2() {

	using mtype = typename crm::detail::handler_message_trait<THandler>;
	using im = typename mtype::imessage_type;
	using om = typename mtype::omessage_type;

	print_vals(im{}, std::make_index_sequence<mtype::tuple_size_v>{});
	print_vals(im{}, std::make_index_sequence<mtype::tuple_size_v>{});

	return true;
	}

template<typename THandler>
bool test4(THandler && h) {

	using meta_type = crm::detail::function_meta2<THandler>;
	using tuple_type = meta_type::params_tuple;

	test4_unroll(tuple_type{}, std::make_index_sequence<std::tuple_size_v<tuple_type>>{});
	test4_unroll_2<THandler>();

	return true;
	}

template<typename T>
bool test_state_srlzdsrlz(T&& in, std::type_identity_t<T> & out) {
	auto ws = crm::srlz::wstream_constructor_t::make();
	crm::srlz::serialize(ws, std::forward<T>(in));

	auto wsbin = ws.release();	
	if(crm::srlz::object_trait_serialized_size(in) != wsbin.size())
		return false;

	auto rs = crm::srlz::rstream_constructor_t::make(std::move(wsbin));
	crm::srlz::deserialize(rs, out);

	return true;
	}

template<typename T, typename Options>
bool test_state_srlzdsrlz(T&& in, std::type_identity_t<T>& out, Options) {
	auto ws = crm::srlz::wstream_constructor_t::make();
	crm::srlz::serialize(ws, std::forward<T>(in), Options{});

	auto wsbin = ws.release();
	if (crm::srlz::object_trait_serialized_size(in, Options{}) != wsbin.size())
		return false;

	auto rs = crm::srlz::rstream_constructor_t::make(std::move(wsbin));
	crm::srlz::deserialize(rs, out, Options{});

	return true;
	}

template<typename T, typename Tcmp>
bool test(T&& v, Tcmp&& fcmp) {
	std::decay_t<T> d{};
	return test_state_srlzdsrlz(std::forward<T>(v), d) && fcmp(v, d);
	}

template<typename T>
bool test(T&& v) {
	std::decay_t<T> d{};
	return test_state_srlzdsrlz(std::forward<T>(v), d) &&  v == d;
	}

template<typename T, typename Options>
bool test_with_opt(T&& v, Options) {
	std::decay_t<T> d{};
	return test_state_srlzdsrlz(std::forward<T>(v), d, crm::srlz::serialized_options_cond<T, Options>{}) && v == d;
	}

bool test5() {
	using prefix = crm::srlz::detail::serializable_properties_prefix_trait_nbytes<uint8_t>;
	prefix prf;
	prf.length = 17;
	prf.type = crm::srlz::srlz_prefix_type_t::prefixed;

	return test(prf, [](const auto& l, const auto& r)noexcept {
		return l.length == r.length && l.type == r.type;
		});
	}

bool test6() {
	using prefix = crm::srlz::detail::serializable_properties_prefix_trait_nbytes<uint16_t>;
	prefix prf;
	prf.length = 17;
	prf.type = crm::srlz::srlz_prefix_type_t::prefixed;

	return test(prf, [](const auto& l, const auto& r)noexcept {
		return l.length == r.length && l.type == r.type;
		});
	}

bool test7() {
	using prefix_trait = typename crm::srlz::detail::serialize_prefix_t<crm::address_descriptor_t>;
	auto prf = prefix_trait::make_prefix(crm::address_descriptor_t{});

	return test(prf, [](const auto& l, const auto& r)noexcept {
		return l.length == r.length && l.type == r.type;
		});
	}

bool test8() {
	crm::address_descriptor_t ad{};
	return test(ad, [](const auto& l, const auto& r)noexcept {
		return l == r;
		});
	}

bool test9() {
	std::array<int, 10> ia;
	static_assert(crm::srlz::detail::is_fixed_size_array_v<decltype(ia)>, __FILE_LINE__);
	for(size_t i = 0; i < ia.size(); ++i)ia[i] = (int)i;

	return test(ia);
	}

bool test10() {
	std::vector<long> vl(1000);
	for(size_t i = 0; i < vl.size(); ++i)vl[i] = i;

	return test_with_opt(vl, crm::srlz::detail::option_max_serialized_size<16000>{});
	}

bool test11() {
	std::array<long, 100> v;
	for (size_t i = 0; i < 100; ++i)v[i] = i;

	std::array<long, 100> vou = { 0 };
	if (!test_state_srlzdsrlz(v, vou)) {
		return false;
		}

	for (size_t i = 0; i < 100; ++i) {
		if (v[i] != vou[i])
			return false;
		}

	return true;
	}

int main() {
	if (!test1()) {
		throw std::exception();
		}

	if (!test2()) {
		throw std::exception();
		}

	if (!test3()) {
		throw std::exception();
		}

	if (!test4([](const std::string&, int, long, std::string&&) {})) {
		throw std::exception();
		}

	if(!test5()) {
		throw std::exception();
		}

	if(!test6()) {
		throw std::exception();
		}

	if(!test7()) {
		throw std::exception();
		}
	if(!test8()) {
		throw std::exception();
		}
	if(!test9()) {
		throw std::exception();
		}
	if(!test10()) {
		throw std::exception();
		}
	if (!test11()) {
		throw std::exception();
		}

	getchar();
	return 0;
	}

