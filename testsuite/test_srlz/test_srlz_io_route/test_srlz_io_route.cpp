#include <iostream>
#include <appkit/appkit.h>
#include <usecase/ex_utility/mkctx.h>
#include <usecase/ex_utility/rand_values.h>


template<typename O>
void normalizex(O&& obj, std::shared_ptr<crm::distributed_ctx_t> c) {

	/* ������ */
	if (crm::is_null(obj.session())) {
		obj.set_session(usecase::rand_session_descriptor());
		}

	/* ������������� ��������� */
	if (crm::is_null(obj.id())) {
		obj.set_id(c->generate_message_id());
		}

	/* ������� */
	if (crm::is_null(obj.destination_id()))
		obj.set_destination_id(usecase::rand_identity_descriptor());

	/* �������� */
	if (crm::is_null(obj.source_id()))
		obj.set_source_id(usecase::rand_identity_descriptor());
	}

template<typename O, typename E, typename A, typename C>
auto dom(O&& o, E & pe, A &pa, C& pc) {
	normalizex(o, pc);

	auto binm = pe->push(std::move(o));

	pa->push(binm, binm.size());
	auto pckl = pa->capture_packets();
	for (auto& p : pckl) {
		p.mark_as_used();
		}

	return pckl;
	}

struct strob_example_type {
	std::string s;
	int i;
	double d;
	};

int main() {
	auto c = usecase::make_serialization_example_ctx();
	auto protocol = std::make_unique<crm::default_binary_protocol_handler_t>(c);
	auto em = protocol->create_emitter();
	auto acc = protocol->create_acceptor();

	c->register_message_type<strob_example_type>();

	for (int i = 0; i < 100; ++i) {
		//dom(usecase::rand_datagram_message(), em, acc, c);
		//dom(usecase::rand_ping(), em, acc, c);
		dom(usecase::rand_request(), em, acc, c);
		//dom(usecase::rand_notify(), em, acc, c);
		//dom(usecase::rand_connection(), em, acc, c);
		//dom(usecase::rand_identity(), em, acc, c);
		//dom(usecase::rand_strob(strob_example_type{}), em, acc, c);
		}

	std::cout << "press <enter>...";

	getchar();

	return 0;
	}



