#include <appkit/appkit.h>


struct pod1 {
	int i = 1;
	char c = 3;
	std::array<int, 10> ia{ 0 };
	};

bool operator==(const pod1& l, const pod1& r)noexcept {
	return l.i == r.i && l.c == r.c && l.ia == r.ia;
	}

bool test2() {
	std::string s1{ "test string" };
	std::wstring ws1{ L"test wide string" };
	int i = 10;
	pod1 p;

	auto srlzs1 = crm::srlz::object_trait_serialized_size(s1)
		+ crm::srlz::object_trait_serialized_size(ws1)
		+ crm::srlz::object_trait_serialized_size(i)
		+ crm::srlz::object_trait_serialized_size(p);


	//auto env{ crm::detail::make_message_envelop( s1, ws1, i) };
	crm::detail::ostrob_message_envelop_t env(s1, ws1, i, pod1{ p });

	auto srlzs2 = crm::srlz::object_trait_serialized_size(env);

	std::cout << "test2: serialized by values " << srlzs1 << std::endl;
	std::cout << "       serialized by tuple  " << srlzs2 << std::endl;

	auto ws = crm::srlz::wstream_constructor_t::make();
	crm::srlz::serialize(ws, env);

	decltype(env) env2;
	auto rs = crm::srlz::rstream_constructor_t::make(ws.release());

	crm::srlz::deserialize(rs, env2);

	return (env.value<0>() == env2.value<0>() && env.value<0>() == s1) &&
		(env.value<1>() == env2.value<1>() && env.value<1>() == ws1) &&
		(env.value<2>() == env2.value<2>() && env.value<2>() == i) && 
		(env.value<3>() == env2.value<3>() && env.value<3>() == p);
	}

bool test1() {
	std::string s1{ "test string" };
	std::wstring ws1{ L"test wide string" };
	int i = 10;

	auto srlzs1 = crm::srlz::object_trait_serialized_size(s1)
		+ crm::srlz::object_trait_serialized_size(ws1)
		+ crm::srlz::object_trait_serialized_size(i);


	//auto env{ crm::detail::make_message_envelop( s1, ws1, i) };
	crm::detail::istrob_message_envelop_t env(s1, ws1, i);

	auto srlzs2 = crm::srlz::object_trait_serialized_size(env);

	std::cout << "test1: serialized by values " << srlzs1 << std::endl;
	std::cout << "       serialized by tuple  " << srlzs2 << std::endl;

	auto ws = crm::srlz::wstream_constructor_t::make();
	crm::srlz::serialize(ws, env);

	decltype(env) env2;
	auto rs = crm::srlz::rstream_constructor_t::make(ws.release());

	crm::srlz::deserialize(rs, env2);

	return (env.value<0>() == env2.value<0>() && env.value<0>() == s1) &&
		(env.value<1>() == env2.value<1>() && env.value<1>() == ws1) &&
		(env.value<2>() == env2.value<2>() && env.value<2>() == i);
	}

int main() {
	if (!test1()) {
		throw std::exception();
		}

	if (!test2()) {
		throw std::exception();
		}

	getchar();
	return 0;
	}


