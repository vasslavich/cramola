#include <appkit/appkit.h>
#include "../../../usecase/ex_utility/rand_values.h"


using namespace crm;
using namespace crm::srlz;


template<typename T, typename Tcmp>
bool test(T&& v, Tcmp&& fcmp) {
	auto ws = wstream_constructor_t::make();
	serialize(ws, std::forward<T>(v));
	auto rs = rstream_constructor_t::make(ws.release());
	
	std::decay_t<T> d{};
	deserialize(rs, d);

	return fcmp(v, d);
	}

bool test_command_rdm() {
	bool f1 = test(usecase::rand_command_rdm(), [](const auto &l, const auto &r) {
		return usecase::eql(l, r);
		});

	return f1;
	}

bool test_identity_payload_entry() {
	bool f1 = test(usecase::rand_identity_payload_entry(), [](const auto& l, const auto& r) {
		return usecase::eql(l, r);
		});

	if (f1) {
		f1 = test(usecase::rand_identity_payload_entry_container<std::vector>(10), [](const auto& l, const auto& r) {
			return usecase::eql<std::vector>(l, r);
			});
		}

	if (f1) {
		f1 = test(usecase::rand_identity_payload(), [](const auto& l, const auto& r) {
			return usecase::eql(l, r);
			});
		}

	if (f1) {
		f1 = test(usecase::rand_identity_value(), [](const auto& l, const auto& r) {
			return usecase::eql(l, r);
			});
		}

	return f1;
	}

bool test_datagram_1() {
	datagram_t dt;
	dt.add_value("ck", usecase::rand_connection_key());
	dt.add_value("id", usecase::rand_identity_descriptor());
	dt.add_value("session_id", usecase::rand_session_descriptor());
	dt.add_value("rt0", usecase::rand_rt0_rdm());
	dt.add_value("int", 111111);
	dt.add_value("mrdm", usecase::rand_mrdm());
	dt.add_value("id-payload-entry", usecase::rand_identity_payload_entry());

	return test(dt, [](const datagram_t& l, const datagram_t& r) {
		decltype(usecase::rand_connection_key())
			ckl = l.get_value<decltype(ckl)>("ck"),
			ckr = r.get_value<decltype(ckl)>("ck");


		return ckl == ckr;
		});
	}

int main() {
	if (test_identity_payload_entry() && 
		test_datagram_1() &&
		test_command_rdm()) {

		std::cout << "all test has been ok" << std::endl;
		}
	else {
		std::cout << "++++++++++++++++++++++ FAULT +++++++++++++++++++++++++++" << std::endl;
		}

	getchar();

	return 0;
	}

