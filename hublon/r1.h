#pragma once


#include <appkit/appkit.h>
#include "./router_1/rt1_main.h"
#include "./xchannel/smp/shmem_module.h"
#include "./xchannel/smp/shmem_host_second.h"
#include "./xchannel/chmk.h"
#include "./terms/internal_terms.h"
#include "./router_1/ipc_unit.h"


namespace crm{

using router_1 = detail::rt1_cntbl_t;
}
