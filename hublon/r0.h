#pragma once

#include "../appkit/appkit.h"
#include "./router_0/rt0_main.h"
#include "./xchannel/smp/shmem_hub.h"
#include "./xchannel/smp/shmem_host_first.h"
#include "./router_0/ipc_host.h"


namespace crm{
using router_0_t = detail::rt0_cntbl_t;
}
