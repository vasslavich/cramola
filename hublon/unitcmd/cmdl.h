#pragma once


#include <appkit/appkit.h>


namespace crm::detail{


enum class channel_strob_type{
	lower_bound,
	undefined,
	unit_connect,
	unit_disconnect,
	upper_bound
	};

struct unit_connect{
	std::string chName_RT0_x_m;
	std::string chName_RT0_x_c;
	std::string chName_x_RT1_m;
	std::string chName_x_RT1_c;

	channel_identity_t id;
	std::string name;
	};

struct unit_disconnect{
	channel_identity_t id;
	};

template<typename TValue,
	typename _TxValue = std::decay_t<TValue>>
constexpr channel_strob_type channel_strob_type_at(){
	if constexpr( std::is_same_v<unit_connect, _TxValue> ){
		return channel_strob_type::unit_connect;
		}
	else{
		if constexpr( std::is_same_v<unit_disconnect, _TxValue> ){
			return channel_strob_type::unit_disconnect;
			}
		else{
			FATAL_ERROR_FWD(nullptr);
			}
		}
	}

struct channel_strob_header{
	channel_strob_type type;
	};

class channel_strob_processor{
private:
	using rstream_type = std::decay_t<decltype(srlz::rstream_constructor_t::make(std::declval<std::vector<uint8_t>>()))>;

	rstream_type _rstream;

public:
	channel_strob_processor( std::vector<uint8_t> && data )noexcept;

	channel_strob_header header();
	
	template<typename TValue,
		typename std::enable_if_t<srlz::is_serializable_v<TValue>, int> = 0>
	typename TValue read(){
		std::decay_t<TValue> val;
		srlz::deserialize( _rstream, val );

		return val;
		}
	};


class channel_strob_compressor{
private:
	using wstream_type = std::decay_t<decltype(srlz::wstream_constructor_t::make())>;

public:
	template<typename TValue>
	static std::vector<uint8_t> compress( TValue && v ){

		channel_strob_header h;
		h.type = channel_strob_type_at<TValue>();

		auto wstream = srlz::wstream_constructor_t::make();
		srlz::serialize( wstream, h );
		srlz::serialize( wstream, v );

		return wstream.release();
		}
	};
}
