#include "../cmdl.h"

using namespace crm;
using namespace crm::detail;


channel_strob_processor::channel_strob_processor( std::vector<uint8_t> && data )noexcept
	: _rstream( std::move( data ) ){}

channel_strob_header channel_strob_processor::header(){
	channel_strob_header h;
	srlz::deserialize( _rstream, h );

	return h;
	}


