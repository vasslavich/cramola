#pragma once


#include <appkit/channel_terms/terms.h>


namespace crm {


std::shared_ptr<detail::io_x_rt1_channel_t> make_channel_ipc_module(std::shared_ptr<distributed_ctx_t> ctx,
	const channel_config_base&);

std::shared_ptr<detail::io_rt0_x_channel_t> make_channel_ipc_hub(std::shared_ptr<distributed_ctx_t> ctx, 
	const channel_config_base&);

std::shared_ptr<detail::unique_channel_manager> make_channel_cm_queue(std::shared_ptr<distributed_ctx_t> ctx,
	const channel_config_base&);
}

