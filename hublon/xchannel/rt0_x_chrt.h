#pragma once


#include <map>
#include <memory>
#include <functional>
#include <appkit/export_internal_terms.h>
#include "../router_0/rt0_main.h"


namespace crm::detail{


class __rt0_router_handler_t : public i_rt0_peer_handler_t {
private:

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	crm::utility::__xobject_counter_logger_t<__rt0_router_handler_t, 1> _xDbgCounter;
#endif

	void push( rt1_x_message_t &&  )final {
		FATAL_ERROR_FWD(nullptr);
		}

	bool is_opened()const noexcept final {
		return  true;
		}

	rtl_table_t direction()const noexcept {
		return rtl_table_t::rt1_router;
		}

public:
	__rt0_router_handler_t() {}
	};


class rt0_x_channel_links_rtmessage_t {
private:
	typedef std::mutex lck_t;
	typedef std::unique_lock<lck_t> lck_scp_t;
	typedef std::map<rtl_roadmap_obj_t, std::unique_ptr<rtmessate_entry_t>> rt0_channel_t;
	typedef std::map<rt0_node_rdm_t, rt0_channel_t> route_message_table_t;
	
	std::weak_ptr<i_rt0_main_router_registrator_t> _rt0Reg;
	route_message_table_t _tbl;
	mutable lck_t _rtLck;
	std::atomic<bool> _closef{false};

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	mutable crm::utility::__xvalue_counter_logger_t<1> __sizeTraceer = __FILE_LINE__;
	crm::utility::__xobject_counter_logger_t<rt0_x_channel_links_rtmessage_t> _xDbgCounter;

	size_t __size_full()const noexcept;
	void __size_trace()const noexcept;
#endif

	const std::unique_ptr<rtmessate_entry_t>& _find_entry(const rtl_roadmap_obj_t &id)const;
	const std::unique_ptr<rtmessate_entry_t>& _find_entry_RZi_LX(const rtl_roadmap_line_t & rdl, const address_descriptor_t &id)const;
	std::shared_ptr<io_rt0_x_channel_t> _find_channel2x_RZi_LX(const rtl_roadmap_line_t & rdl, const address_descriptor_t & destId)const;
	void _insert( const rtl_roadmap_obj_t &id, rtmessate_entry_t && ch);
	rtl_rdm_keypair_t _erase( const rtl_roadmap_obj_t & id )noexcept;
	std::list<rtl_rdm_keypair_t> _erase( const rt0_node_rdm_t & rt0_ID )noexcept;
	std::list<rtl_rdm_keypair_t> _erase( const channel_info_t & chId )noexcept;
	std::shared_ptr<io_rt0_x_channel_t> _find_channel2x( const rtl_roadmap_obj_t & destId )const noexcept;
	std::shared_ptr<i_rt0_peer_handler_t> _find_peer( const rtl_roadmap_obj_t & destId )const noexcept;
	std::list<std::shared_ptr<io_rt0_x_channel_t>> _find_channel2x_list( const rt0_node_rdm_t & rt0,
		const identity_descriptor_t & targetId,
		const identity_descriptor_t & sourceId,
		const rtl_table_t & ttbl )const noexcept;
	void __add_rt0( const rt0_node_rdm_t& rt0_ID );

	void _close()noexcept;
	size_t _size()const noexcept;

public:
	rt0_x_channel_links_rtmessage_t( std::weak_ptr<i_rt0_main_router_registrator_t> rt0reg );
	~rt0_x_channel_links_rtmessage_t();

	void add_rt0( const rt0_node_rdm_t& rt0_ID );
	void subscribe_messages( const rtl_roadmap_obj_t &id, rtmessate_entry_t && ch );

	rtl_rdm_keypair_t unsubscribe_messages( const rtl_roadmap_obj_t & id )noexcept;
	std::list<rtl_rdm_keypair_t> unsubscribe_by_rt0( const rt0_node_rdm_t& rt0_ID )noexcept;
	std::list<rtl_rdm_keypair_t> unsubscribe_by_channel( const channel_info_t & chId )noexcept;

	std::list<std::string> channel_list_descriptions()const;
	std::shared_ptr<io_rt0_x_channel_t> find_channel( const rtl_roadmap_obj_t & destId )const noexcept;
	std::shared_ptr<i_rt0_peer_handler_t> find_peer( const rtl_roadmap_obj_t & destId )const noexcept;
	std::shared_ptr<io_rt0_x_channel_t> find_channel_RZi_LX(const rtl_roadmap_line_t & rdl, const address_descriptor_t & a)const;
	std::list<std::shared_ptr<io_rt0_x_channel_t>> find_channel_list( const rt0_node_rdm_t & rt0,
		const identity_descriptor_t & targetId,
		const identity_descriptor_t & sourceId,
		const rtl_table_t & ttbl )const noexcept;

	void close()noexcept;
	size_t size()const noexcept;
	};

class rt0_x_channel_links_table_t {
private:
	typedef std::mutex lck_t;
	typedef std::unique_lock<lck_t> lck_scp_t;
	typedef std::map<rtl_roadmap_event_t, std::weak_ptr<io_rt0_x_channel_t>> route_events_table_t;

	std::weak_ptr<distributed_ctx_t> _ctx;
	std::weak_ptr<i_rt0_main_router_registrator_t> _rt0reg;
	/*! ������� ������������� ��������� : ������������� ������ �� �������������� ��������� */
	rt0_x_channel_links_rtmessage_t _rtMessages;
	/*! ������� ������������� ������� : ������������� ������ �� �������������� ������� */
	route_events_table_t _routeEv;
	/*! ������� ������� */
	std::map< channel_identity_t, std::weak_ptr<io_rt0_x_channel_t>> _channelLst;
	mutable lck_t _rtLck;
	std::atomic<bool> _closef{false};

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	mutable crm::utility::__xvalue_counter_logger_t<1> __sizeTraceer = __FILE_LINE__;
	crm::utility::__xobject_counter_logger_t<rt0_x_channel_links_table_t> _xDbgCounter;

	size_t __size_route_ev()const noexcept;
	void __size_trace()const noexcept;
#endif

	std::shared_ptr< distributed_ctx_t> ctx()const noexcept;

	std::shared_ptr<io_rt0_x_channel_t> _find_channel( const channel_identity_t& chId )const;
	std::shared_ptr<io_rt0_x_channel_t> _find_channel( const rtl_roadmap_event_t & destId )const;

	template<typename _TxCommand,
		typename C = typename std::decay_t<_TxCommand>,
		typename std::enable_if_t<std::is_base_of_v<i_command_from_rt02rt1_t, C>&& std::is_final_v<C>, int> = 0>
	void _notify( rtx_command_type_t ev, _TxCommand && cmd )const noexcept {
		CBL_NO_EXCEPTION_BEGIN

		/* first N-1 channels: copy command */
		auto it = _channelLst.begin();
		for (size_t i = 0; (i + 1) < _channelLst.size(); ++it, ++i) {
			auto ch = (*it).second.lock();
			if (ch) {
				auto pCpy = cmd.copy();
				ch->notify_to_rt1(ev, std::move(*(dynamic_cast<C*>(pCpy.get()))));
				}
			}

		/* last channel */
		auto ch = (*it).second.lock();
		if (ch) {
			ch->notify_to_rt1(ev, std::forward< _TxCommand>(cmd));
			}

		CBL_NO_EXCEPTION_END(ctx())
		}

	template<typename _TxCommand,
		typename C = typename std::decay_t<_TxCommand>,
		typename std::enable_if_t<std::is_base_of_v<i_command_from_rt02rt1_t, C>&& std::is_final_v<C>, int> = 0>
	bool _notify( const channel_identity_t & chid, _TxCommand && cmd )const noexcept {
		CHECK_NO_EXCEPTION_BEGIN

		auto channelIt = _channelLst.find(chid);
		if (channelIt != _channelLst.end()) {
			if (auto ch = channelIt->second.lock()) {
				return ch->push_command_to_rt1(std::forward<_TxCommand>(cmd));
				}
			else {
				return false;
				}
			}
		else {
			return false;
			}

		CHECK_NO_EXCEPTION_END
		}

	void ntf_hndl(std::unique_ptr<dcf_exception_t> && n)const noexcept;
	void _unregister_rt1_link_messages( const channel_info_t & chId,
		std::function<void( std::vector<source_object_key_t> && srcKeys )> && unregOutRT0 )noexcept;
	void _unregister_rt1_link_events( const channel_info_t & chId )noexcept;
	void _unregister_rt1_link_channels( const channel_info_t & chId )noexcept;

	std::chrono::milliseconds timeout_xchannel()const noexcept;

public:
	rt0_x_channel_links_table_t( std::weak_ptr<distributed_ctx_t> ctx,
		std::weak_ptr<i_rt0_main_router_registrator_t> rt0reg );
	~rt0_x_channel_links_table_t();

	void register_rt1_link( std::weak_ptr<io_rt0_x_channel_t> channel,
		std::weak_ptr<rt0_x_channels_table_t > root);

	void unregister_rt1_link( const channel_info_t & chId,
		std::function<void( std::vector<source_object_key_t> && srcKeys )> && unregOutRT0 )noexcept;

	std::shared_ptr<i_rt0_peer_handler_t> find_peer( const rtl_roadmap_obj_t & destId )const;
	std::shared_ptr<io_rt0_x_channel_t> find_channel( const channel_identity_t& chId )const;
	std::shared_ptr<io_rt0_x_channel_t> find_channel( const rtl_roadmap_obj_t & destId )const;
	std::shared_ptr<io_rt0_x_channel_t> find_channel( const rtl_roadmap_event_t & destId )const;
	std::list<std::shared_ptr<io_rt0_x_channel_t>> find_channel_list( const rt0_node_rdm_t& rt0,
		const identity_descriptor_t & targetId,
		const identity_descriptor_t & sourceId,
		const rtl_table_t & ttbl )const;

	std::shared_ptr<io_rt0_x_channel_t> find_channel_RZi_LX(const rtl_roadmap_line_t & rdl, const address_descriptor_t & destId)const;

	std::list<std::string> channel_list_descriptions()const;

	template<typename _TxCommand,
		typename C = typename std::decay_t<_TxCommand>,
		typename std::enable_if_t<std::is_base_of_v<i_command_from_rt02rt1_t, C>&& std::is_final_v<C>, int> = 0>
	void notify_broadcast(rtx_command_type_t ev, _TxCommand && cmd)const noexcept {
		lck_scp_t lck(_rtLck);
		_notify(ev, std::forward<_TxCommand>(cmd));
		}

	template<typename _TxCommand,
		typename C = typename std::decay_t<_TxCommand>,
		typename std::enable_if_t<std::is_base_of_v<i_command_from_rt02rt1_t, C>&& std::is_final_v<C>, int> = 0>
	bool notify_target(const channel_identity_t & chid, _TxCommand && cmd)const noexcept {
		lck_scp_t lck(_rtLck);
		return _notify(chid, std::forward<_TxCommand>(cmd));
		}

	void add_rt0( const rt0_node_rdm_t& rt0_ID );

	void subscribe_events( const channel_identity_t& channelId,
		const rtl_roadmap_event_t & id, const std::list<rtx_command_type_t> & evlst );
	void subscribe_messages( const channel_identity_t& channelId,
		const rtl_roadmap_obj_t &id,
		std::shared_ptr<i_rt0_peer_handler_t> && peer,
		rtmessate_entry_t::unregister_f && unsubscriber);

	void unsubscribe_events( const channel_identity_t& channelId,
		const rtl_roadmap_event_t & id )noexcept;

	rtl_rdm_keypair_t unsubscribe_messages( const channel_identity_t& channelId,
		const rtl_roadmap_obj_t & id )noexcept;

	void unsubscribe_by_node( const rt0_node_rdm_t & nodeId )noexcept;

	void close()noexcept;
	bool closed()const;
	};

class rt0_cntbl_t;

class rt0_x_channels_table_t :
	public i_rt0_channel_route_t,
	public std::enable_shared_from_this<rt0_x_channels_table_t> {

	typedef rt0_x_channels_table_t self_t;

private:
	typedef std::mutex lck_t;
	typedef std::unique_lock<lck_t> lck_scp_t;

	std::weak_ptr<distributed_ctx_t> _ctx;
	rt0_x_channel_links_table_t _lnkTbl;
	std::weak_ptr<rt0_cntbl_t> _rt0Registrator;
	std::atomic<bool> _closef{false};

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	crm::utility::__xobject_counter_logger_t<self_t> _xDbgCounter;
#endif

	void ntf_hndl( std::unique_ptr<dcf_exception_t> && ntf )noexcept;

	rt1_x_message_t processing(rt1_x_message_data_t && object)noexcept;

	void __push2peer( rtl_roadmap_obj_t && desc, rt1_x_message_t && );

	template<typename O,
		typename std::enable_if_t < is_out_message_requirements_v<O>, int> = 0>
	void __push2peer(i_rt0_peer_handler_t* p, O && o) {
		if (auto pout = dynamic_cast<rt0_opeer_handler_t*>(p)) {
			pout->push(std::forward<O>(o));
			}
		else if (auto pin = dynamic_cast<__rt0_ipeer_table_t::peer_handler*>(p)) {
			pin->push(std::forward<O>(o));
			}
		else {
			FATAL_ERROR_FWD(nullptr);
			}
		}

	template<typename O,
		typename std::enable_if_t < is_out_message_requirements_v<O>, int> = 0>
	void __push2peer( rtl_roadmap_obj_t && desc, O && o) {
		if (auto ph = _lnkTbl.find_peer(desc)) {
			__push2peer(ph.get(), std::forward<O>(o));
			}
		else {
			if (auto rt0 = _rt0Registrator.lock()) {
				if (auto p2 = rt0->find_peer(desc.rt0())) {
					__push2peer(p2.get(), std::forward<O>(o));
					}
				else {
					notify_rt1(rtx_command_type_t::invalid_xpeer, i_rt0_invalid_xpeer_t(std::move(desc)));
					THROW_MPF_EXC_FWD(nullptr);
					}
				}
			else {
				notify_rt1(rtx_command_type_t::invalid_xpeer, i_rt0_invalid_xpeer_t(std::move(desc)));
				THROW_MPF_EXC_FWD(nullptr);
				}
			}
		}

	template<typename _TxCommand,
		typename C = typename std::decay_t<_TxCommand>,
		typename std::enable_if_t<std::is_base_of_v<i_command_from_rt02rt1_t, C>&& std::is_final_v<C>, int> = 0>
	void __notify_rt1_invoke(rtx_command_type_t ev, _TxCommand && cmd)const noexcept {
		CBL_VERIFY((!is_null(cmd.command_target_receiver_id()) || is_broadcast_command(cmd)));
		_lnkTbl.notify_broadcast(ev, std::forward<_TxCommand>(cmd));
		}

	template<typename _TxCommand,
		typename C = typename std::decay_t<_TxCommand>,
		typename std::enable_if_t<std::is_base_of_v<i_command_from_rt02rt1_t, C>&& std::is_final_v<C>, int> = 0>
		[[nodiscard]]
	bool __notify_rt1_invoke( const channel_identity_t & chId, _TxCommand && cmd )const noexcept {
		CBL_VERIFY((!is_null(cmd.command_target_receiver_id()) || is_broadcast_command(cmd)));
		return _lnkTbl.notify_target(chId, std::forward<_TxCommand>(cmd));
		}

	rt0_x_channels_table_t( std::weak_ptr<distributed_ctx_t> ctx,
		std::weak_ptr<rt0_cntbl_t> );
	void ctr();

	std::chrono::milliseconds timeout_xchannel()const noexcept;
public:
	static std::shared_ptr<rt0_x_channels_table_t> make( std::weak_ptr<distributed_ctx_t> ctx,
		std::weak_ptr<rt0_cntbl_t> );

	~rt0_x_channels_table_t();
	void close()noexcept;
	bool closed()const noexcept;

	/*! �������� ��������� � ������ 1 � ����� X �� ������� 0 */
	[[nodiscard]]
	addon_push_event_result_t push_to_rt0_xpeer_XBN(rt1_x_message_data_t && object )noexcept final;

	/*! �������� ������� � ������ 1 � ����� X �� ������� 0 */
	[[nodiscard]]
	addon_push_event_result_t push_command_pack_to_rt0( rt0_x_command_pack_t && cmd)noexcept final;

	/*! ����������� ������ � ������� ������������� */
	void register_rt1_channel( std::weak_ptr<io_rt0_x_channel_t> channel )final;
	/*! �������� ������ */
	void unregister_rt1_channel( const channel_info_t& chId )noexcept final;

	void do_found(std::shared_ptr<io_rt0_x_channel_t> && link, rt0_x_message_unit_t && obj)const;
	void do_not_found(rtl_roadmap_obj_t && trdm, rt0_x_message_unit_t && obj)const;

	void push_to_rt1(rt0_x_message_unit_t && object ) ;

	template<typename _TxCommand,
		typename C = typename std::decay_t<_TxCommand>,
		typename std::enable_if_t<std::is_base_of_v<i_command_from_rt02rt1_t, C>&& std::is_final_v<C>, int> = 0>
	void push_to_rt1(_TxCommand && cmd) {
		auto ct = cmd.type();
		notify_rt1(ct, std::forward<_TxCommand>(cmd));
		}

	void add_rt0( const rt0_node_rdm_t& rt0_ID );

	void subscribe_events( const channel_identity_t& channelId,
		const rtl_roadmap_event_t & id, const std::list<rtx_command_type_t> & evlst );

	void subscribe_messages( const channel_identity_t& channelId,
		const rtl_roadmap_obj_t &id,
		std::shared_ptr<i_rt0_peer_handler_t> && peer,
		rtmessate_entry_t::unregister_f && unsubscriber );

	void unsubscribe_events( const channel_identity_t& channelId,
		const rtl_roadmap_event_t & id )noexcept;

	rtl_rdm_keypair_t unsubscribe_messages( const channel_identity_t& channelId,
		const rtl_roadmap_obj_t & id )noexcept;

	template<typename _TxCommand,
		typename C = typename std::decay_t<_TxCommand>,
		typename std::enable_if_t<std::is_base_of_v<i_command_from_rt02rt1_t, C>&& std::is_final_v<C>, int> = 0>
	void notify_rt1(rtx_command_type_t ev, _TxCommand && cmd)const noexcept {
		__notify_rt1_invoke(ev, std::forward<_TxCommand>(cmd));
		}

	template<typename _TxCommand,
		typename C = typename std::decay_t<_TxCommand>,
		typename std::enable_if_t<std::is_base_of_v<i_command_from_rt02rt1_t, C>&& std::is_final_v<C>, int> = 0>
		[[nodiscard]]
	bool notify_rt1(const channel_identity_t& chId, _TxCommand && cmd)const noexcept {
		return __notify_rt1_invoke(chId, std::forward<_TxCommand>(cmd));
		}

	void unsubscribe_by_node( const rt0_node_rdm_t & nodeId )noexcept;
	void channel_command_fault( const channel_identity_t & chid )noexcept;
	};
}

