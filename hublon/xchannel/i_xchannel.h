#pragma once

#include <type_traits>
#include <appkit/exceptions.h>
#include <appkit/streams/i_streams.h>


namespace crm {
//
//template<typename T, typename = std::void_t<>>
//struct is_ipc_object_dispatcher : std::false_type {};
//
//template<typename T>
//struct is_ipc_object_dispatcher<T, std::void_t<
//	decltype(std::declval<T>().input_stream_handler(std::declval<stream_source_descriptor_t>(),
//		std::declval<std::shared_ptr<i_stream_whandler_t>>())),
//	std::enable_if_t<std::is_nothrow_invocable_v<decltype(&T::exception_handler), T, std::unique_ptr<dcf_ntf_base_t>>>
//	>> : std::true_type{};
//
//template<typename T>
//constexpr bool is_ipc_object_dispatcher_v = is_ipc_object_dispatcher<std::decay_t<T>>::value;
}
