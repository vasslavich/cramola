#include <appkit/appkit.h>
#include "../chmk.h"
#include "../smm/rt0_x_chrt_linkq.h"
#include "../../terms/internal_terms.h"
#include "../../xchannel/smp/shmem_reg.h"
#include "../../xchannel/smp/shmem_module.h"
#include "../../xchannel/smp/shmem_hub.h"


using namespace crm;
using namespace crm::detail;


std::shared_ptr<unique_channel_manager> crm::make_channel_cm_queue(std::shared_ptr<distributed_ctx_t> ctx_,
	const channel_config_base& opt) {

	if (opt.type() == xchannel_type_t::shared_objects_queue) {
		const auto & umOpt = dynamic_cast<const channel_common_memory_config&>(opt);
		return rt0_x_channel_link_t::create(opt.info(),
			ctx_,
			umOpt.rt0());
		}
	else {
		THROW_EXC_FWD(nullptr);
		}
	}

std::shared_ptr<io_x_rt1_channel_t> crm::make_channel_ipc_module(std::shared_ptr<distributed_ctx_t> ctx_, 
	const channel_config_base& opt) {
		
	if( opt.type() == xchannel_type_t::ipc_shmem ){
		const auto & umOpt = dynamic_cast<const channel_shared_memory_config&>(opt);
		return std::make_shared< xchannel_shmem_module>( ctx_, umOpt );
		}
	else{
		THROW_EXC_FWD(nullptr);
		}
	}

std::shared_ptr<io_rt0_x_channel_t> crm::make_channel_ipc_hub(std::shared_ptr<distributed_ctx_t> ctx_, 
	const channel_config_base& opt) {

	if( opt.type() == xchannel_type_t::ipc_shmem ){
		const auto & umOpt = dynamic_cast<const channel_shared_memory_config&>(opt);
		return xchannel_shmem_hub::make( ctx_, umOpt );
		}
	else{
		THROW_EXC_FWD(nullptr);
		}
	}

