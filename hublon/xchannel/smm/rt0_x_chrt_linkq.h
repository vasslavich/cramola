#pragma once


#include <map>
#include <memory>
#include <mutex>
#include <appkit/export_internal_terms.h>


namespace crm::detail{


class rt0_x_channel_link_t :
	public unique_channel_manager,
	public std::enable_shared_from_this<rt0_x_channel_link_t> {

	friend class rt0_x_channels_table_t;
	typedef rt0_x_channel_link_t self_t;

private:
	typedef std::mutex lck_t;
	typedef std::unique_lock<lck_t> lck_scp_t;

	/*! ������� ������� */
	std::multimap<rtx_command_type_t, rtl_roadmap_event_t> _evTbl;
	mutable lck_t _tblLck;

	/*! ������� ��������� � ������ 0 � ����� X*/
	std::shared_ptr<i_message_queue_t<rt0_x_message_t>> _obj_RT0_X;
	/*! ������� ������ � ������ 0 � ����� X */
	std::shared_ptr<i_message_queue_t<command_rdm_t>> _ev_RT0_X;
	channel_info_t _chInf;
	std::weak_ptr<i_rt0_channel_route_t> _root;
	std::weak_ptr<distributed_ctx_t> _ctx;
	std::atomic<channel_state> _chst{ channel_state::ready };
	std::atomic<bool> _closef{false};

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	mutable crm::utility::__xvalue_counter_logger_t<> __sizeTraceer = __FILE_LINE__;
	crm::utility::__xobject_counter_logger_t<self_t> _xDbgCounter;

	void __size_trace()const noexcept;
#endif

	std::shared_ptr<distributed_ctx_t> ctx()const noexcept;
	std::chrono::milliseconds timeout_xchannel()const noexcept;
	std::list<rtl_roadmap_event_t> find_event_targets( rtx_command_type_t ev )const noexcept final;

	bool pop_from_rt0( command_rdm_t & ev, const std::chrono::milliseconds & wait )final;
	bool pop_from_rt0( rt0_x_message_t & pck, const std::chrono::milliseconds & wait )final;
	size_t pop_from_rt0( std::list<rt0_x_message_t> & lst,
		const std::chrono::milliseconds & wait )final;

	[[nodiscard]]
	addon_push_event_result_t push_to_rt0_message(rt1_x_message_data_t && object, const std::chrono::milliseconds & wait )noexcept final;

	[[nodiscard]]
	addon_push_event_result_t push_command_pack_to_rt0(rt0_x_command_pack_t&& cmd )noexcept final;

	const channel_info_t& get_channel_info()const final;

	bool push_to_rt1( rt0_x_message_t && object, const std::chrono::milliseconds & wait)noexcept final;
	bool push_to_rt1( command_rdm_t && cmd)noexcept final;
	void subscribe( const rtl_roadmap_event_t & id, const std::list<rtx_command_type_t> & evlst )final;
	void unsubscribe( const rtl_roadmap_event_t & id )noexcept final;
	//void notify( rtx_command_type_t ev, rt0_x_command_pack_t&& cmd )noexcept final;
	//bool push_command(rt0_x_command_pack_t&& cmd)noexcept final;

	rt0_x_channel_link_t( const channel_info_t & chInf,
		std::weak_ptr<distributed_ctx_t> ctx,
		std::weak_ptr<i_rt0_channel_route_t> out );

	void _subscribe( const rtl_roadmap_event_t & id, const std::list<rtx_command_type_t> & evlst );
	void _unsubscribe( const rtl_roadmap_event_t & id )noexcept;

	void ntf_hndl(std::unique_ptr<dcf_exception_t> && e)const noexcept;
	void set_channel_state( channel_state st )noexcept;
	void close(bool dctr)noexcept;

public:
	~rt0_x_channel_link_t();

	static std::shared_ptr<rt0_x_channel_link_t> create( const channel_info_t & chInf,
		std::weak_ptr<distributed_ctx_t> ctx,
		std::weak_ptr<i_rt0_channel_route_t> out );

	void close()noexcept;
	bool closed()const noexcept;
	};
}
