#include <appkit/appkit.h>
#include "../rt0_x_chrt_linkq.h"


using namespace crm;
using namespace crm::detail;


#ifdef CBL_OBJECTS_COUNTER_CHECKER
void rt0_x_channel_link_t::__size_trace()const noexcept{
	CHECK_NO_EXCEPTION(__sizeTraceer.CounterCheck( _evTbl.size() ));
	}
#endif

const channel_info_t& rt0_x_channel_link_t::get_channel_info()const {
	return _chInf;
	}

/* ���������� � �������������� ������ 1, ��� ���������� �������� ������� ����������� �� �������������� ������ 0 */
bool rt0_x_channel_link_t::pop_from_rt0( command_rdm_t & ev, const std::chrono::milliseconds & wait ) {
	if( !closed() ) {
		return _ev_RT0_X->pop( ev, wait );
		}
	else
		return false;
	}

/* ���������� � �������������� ������ 1, ��� ���������� �������� ��������� ����������� �� �������������� ������ 0 */
bool rt0_x_channel_link_t::pop_from_rt0( rt0_x_message_t & pck, const std::chrono::milliseconds & wait ) {
	if(!closed()) {
#pragma message( CBL_VAR_NAME_VALUE(CBL_BUILD_VAR_SNDRCV_REVERSE_GUARD, 77))
		return _obj_RT0_X->pop(pck, wait);
		}
	else
		return false;
	}

/* ���������� � �������������� ������ 1, ��� ���������� �������� ��������� ����������� �� �������������� ������ 0 */
size_t rt0_x_channel_link_t::pop_from_rt0( std::list<rt0_x_message_t> & lst, const std::chrono::milliseconds & wait ) {
	if(!closed()) {
#pragma message( CBL_VAR_NAME_VALUE(CBL_BUILD_VAR_SNDRCV_REVERSE_GUARD, 78))
		return _obj_RT0_X->pop(lst, wait);
		}
	else
		return 0;
	}

/* ���������� � �������������� ������ 1, ��� �������� ��������� � ������������� ������ 0 */
addon_push_event_result_t rt0_x_channel_link_t::push_to_rt0_message(rt1_x_message_data_t && object, const std::chrono::milliseconds & /*wait*/)noexcept {
	CBL_MPF_KEYTRACE_SET(object.sndrcv_handler().spitTag, 18);

	if( _chst.load( std::memory_order::memory_order_relaxed ) == channel_state::ready ){
		if( auto root = _root.lock() ){
			return root->push_to_rt0_xpeer_XBN( std::move( object ) );
			}
		else{
			return addon_push_event_result_t::enum_type::exception;
			}
		}
	else{
		return addon_push_event_result_t::enum_type::exception;
		}
	}

/* ���������� � �������������� ������ 1, ��� �������� ������ � ������������� ������ 0 */
addon_push_event_result_t rt0_x_channel_link_t::push_command_pack_to_rt0(rt0_x_command_pack_t&& cmd ) noexcept {

	if( _chst.load( std::memory_order::memory_order_relaxed ) == channel_state::ready ){
		if( auto root = _root.lock() ){
			return root->push_command_pack_to_rt0( std::move(cmd) );
			}
		else{
			return addon_push_event_result_t::enum_type::link_unregistered;
			}
		}
	else{
		return addon_push_event_result_t::enum_type::exception;
		}
	}

//void rt0_x_channel_link_t::notify( rtx_command_type_t ev, rt0_x_command_pack_t&& cmd )noexcept {
//	if( !closed() ) {
//		CHECK_NO_EXCEPTION_BEGIN
//
//		auto evTargets( find_event_targets( ev ) );
//		all_send( evTargets, std::move( cmd ) );
//
//		CHECK_NO_EXCEPTION_END
//		}
//	}

//bool rt0_x_channel_link_t::push_command(rt0_x_command_pack_t&& cmd)noexcept {
//	if( !closed() ) {
//
//		command_rdm_t rdm;
//		rdm.target = cmd.command_target_receiver_id();
//		rdm.cmd = std::move(cmd);
//		
//		return push_to_rt1( std::move( rdm ) );
//		}
//	else {
//		return false;
//		}
//	}

void rt0_x_channel_link_t::set_channel_state( channel_state st )noexcept{
	_chst.store( st, std::memory_order::memory_order_relaxed );
	}

std::shared_ptr<distributed_ctx_t> rt0_x_channel_link_t::ctx()const noexcept {
	return _ctx.lock();
	}

std::chrono::milliseconds rt0_x_channel_link_t::timeout_xchannel()const noexcept {
	if (auto c = _ctx.lock()) {
		return c->policies().timeout_wait_io();
		}
	else {
		return {};
		}
	}

std::list<rtl_roadmap_event_t> rt0_x_channel_link_t::find_event_targets( rtx_command_type_t ev )const noexcept {
	CHECK_NO_EXCEPTION_BEGIN

	lck_scp_t lck( _tblLck );
	std::list<rtl_roadmap_event_t> lst;

	for( auto it = _evTbl.begin(); it != _evTbl.end(); ++it )
		if( (*it).first == ev )
			lst.push_back( it->second );

	return std::move( lst );

	CHECK_NO_EXCEPTION_END
	}

void rt0_x_channel_link_t::ntf_hndl(std::unique_ptr<dcf_exception_t> && e)const noexcept {
	if (auto c = _ctx.lock()) {
		c->exc_hndl(std::move(e));
		}
	}

rt0_x_channel_link_t::rt0_x_channel_link_t( const channel_info_t & chInf,
	std::weak_ptr<distributed_ctx_t> ctx,
	std::weak_ptr<i_rt0_channel_route_t> out )
	: _chInf( chInf )
	, _root( out ) 
	, _ctx(ctx){

	using m_queue_type = std::decay_t<decltype(CHECK_PTR(_ctx)->queue_manager().
		create_rt0_x_message_queue(__FILE_LINE__, queue_options_t::message_stack_module_channel))>;
	using e_queue_type = std::decay_t<decltype(CHECK_PTR(_ctx)->queue_manager().
		tcreate_queue<command_rdm_t>(__FILE_LINE__, queue_options_t::message_stack_module_channel))>;

	if( is_null( _chInf.id() ) ) {
		THROW_MPF_EXC_FWD(nullptr);
		}

	_obj_RT0_X = std::make_shared<m_queue_type>(CHECK_PTR(_ctx)->queue_manager().
		create_rt0_x_message_queue(
			__FILE_LINE__,
			queue_options_t::message_stack_module_channel));

	_ev_RT0_X = std::make_shared<e_queue_type>(CHECK_PTR(_ctx)->queue_manager().
		tcreate_queue<command_rdm_t>(
			__FILE_LINE__,
			queue_options_t::message_stack_module_channel));
	}

std::shared_ptr<rt0_x_channel_link_t> rt0_x_channel_link_t::create( const channel_info_t & chInf,
	std::weak_ptr<distributed_ctx_t> ctx,
	std::weak_ptr<i_rt0_channel_route_t> out ) {

	if (auto rtTbl = out.lock()) {
		return std::shared_ptr<rt0_x_channel_link_t>(new rt0_x_channel_link_t(chInf, ctx, out));
		}
	else {
		THROW_EXC_FWD(nullptr);
		}
	}

void rt0_x_channel_link_t::_subscribe( const rtl_roadmap_event_t & id,
	const std::list<rtx_command_type_t> & evlst ) {

	_unsubscribe( id );
	for( auto iev : evlst ) {
		_evTbl.insert( {iev, id} );
		}

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	__size_trace();
#endif
	}

void rt0_x_channel_link_t::subscribe( const rtl_roadmap_event_t & id,
	const std::list<rtx_command_type_t> & evlst ) {

	lck_scp_t lck( _tblLck );
	_subscribe( id, evlst );
	}

void rt0_x_channel_link_t::_unsubscribe( const rtl_roadmap_event_t & id ) noexcept{
	bool has = true;
	while( _evTbl.size() && has ) {

		has = false;
		for( auto it = _evTbl.begin(); it != _evTbl.end(); ++it ) {
			if( id == it->second ) {
				_evTbl.erase( it );
				has = true;
				break;
				}
			}
		}

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	__size_trace();
#endif
	}

void rt0_x_channel_link_t::unsubscribe( const rtl_roadmap_event_t & id ) noexcept{
	lck_scp_t lck( _tblLck );
	CHECK_NO_EXCEPTION( _unsubscribe( id ) );
	}

bool rt0_x_channel_link_t::push_to_rt1( rt0_x_message_t && object, const std::chrono::milliseconds & /*wait*/ ) noexcept{
	if( !closed() ) {
		/* ������������� ������ � ������ 0 �� ������� 1 */
#ifdef CBL_MPF_MESSAGE_TRACEWAY_KEYS_BY_ID
		const auto tid = trait_at_keytrace_set(object.package());
#endif


#pragma message( CBL_VAR_NAME_VALUE(CBL_BUILD_VAR_SNDRCV_REVERSE_GUARD, 61))
		_obj_RT0_X->push( std::move( object ) );

		return true;
		}
	else {
		return false;
		}
	}

bool rt0_x_channel_link_t::push_to_rt1( command_rdm_t && object )noexcept {

	if( !closed() ) {

		if( !object.cmd.empty() ) {

			/* ������������� ������� ���� � ������ 0 �� ������� 1 */
			_ev_RT0_X->push( std::move( object ) );

			return true;
			}
		else {
			return false;
			}
		}
	else {
		return false;
		}
	}

void rt0_x_channel_link_t::close()noexcept {
	close(false);
	}

void rt0_x_channel_link_t::close(bool dctr)noexcept {

	bool closedf = false;
	if( _closef.compare_exchange_strong( closedf, true ) ) {

#ifdef CBL_OBJECTS_COUNTER_CHECKER
		__size_trace();
#endif

		/* �������� ����������� � �������������� ������� */
		if(dctr) {
			if(auto hub = _root.lock()) {
				hub->unregister_rt1_channel(_chInf);
				}
			}
		}
	}

bool rt0_x_channel_link_t::closed()const noexcept{
	return _closef.load( std::memory_order::memory_order_acquire );
	}

rt0_x_channel_link_t::~rt0_x_channel_link_t() {
	close(true);
	}

