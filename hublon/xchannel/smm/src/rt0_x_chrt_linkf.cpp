#include <appkit/appkit.h>
#include "../rt0_x_chrt_linkf.h"


using namespace crm;
using namespace crm::detail;


#ifdef CBL_OBJECTS_COUNTER_CHECKER
void rt0_x_channel_link_functors_table_rt0_x_t::__size_trace()const noexcept{
	CHECK_NO_EXCEPTION(__sizeTraceer.CounterCheck( _rt0_x_links.size() ));
	}
#endif


void rt0_x_channel_link_functors_table_rt0_x_t::reg( std::shared_ptr<i_rt0_x_channel_link_functors_rt0_x_t> obj ) {

	if( obj ) {
		lckscp_t lck( _lck );
		auto it = _rt0_x_links.find( obj->locid() );
		if( it != _rt0_x_links.end() ) {
			FATAL_ERROR_FWD(nullptr);
			}
		else {
			if( !_rt0_x_links.insert( {obj->locid(), obj} ).second ) {
				FATAL_ERROR_FWD(nullptr);
				}

#ifdef CBL_OBJECTS_COUNTER_CHECKER
			__size_trace();
#endif
			}
		}
	}

void rt0_x_channel_link_functors_table_rt0_x_t::unreg( const local_object_id_t&locid )noexcept {
	lckscp_t lck( _lck );
	_rt0_x_links.erase( locid );

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	__size_trace();
#endif
	}

addon_push_event_result_t rt0_x_channel_link_functors_table_rt0_x_t::push_to_x(
	const local_object_id_t & locid, rt0_x_command_pack_t && obj)noexcept {

	CBL_VERIFY(!obj.empty());

	lckscp_t lck(_lck);

	auto it = _rt0_x_links.find(locid);
	if(it == _rt0_x_links.end()) {
		return addon_push_event_result_t::enum_type::link_unregistered;
		}
	else {
		auto link = it->second.lock();
		if(link)
			return link->push_from_rt1_to_rt0(std::move(obj));
		else {
			return addon_push_event_result_t::enum_type::link_unregistered;
			}
		}
	}

bool rt0_x_channel_link_functors_table_rt0_x_t::push_to_x(
	const local_object_id_t& locid, rt1_x_message_data_t && obj ) noexcept{

	lckscp_t lck( _lck );

	auto it = _rt0_x_links.find( locid );
	if( it == _rt0_x_links.end() ) {
		return false;
		}
	else {
		auto link = it->second.lock();
		if( link )
			return link->push_from_rt1_to_rt0( std::move(obj) );
		else {
			return false;
			}
		}
	}






#ifdef CBL_OBJECTS_COUNTER_CHECKER
void rt0_x_channel_link_functors_rt0_x_t::__size_trace()const noexcept{
	CHECK_NO_EXCEPTION(__sizeTraceer.CounterCheck( _evTbl.size() ));
	}
#endif

const channel_info_t& rt0_x_channel_link_functors_x_rt1_t::get_channel_info()const {
	return _chInf;
	}

/* ���������� � �������������� ������ 1, ��� ���������� �������� ������� ����������� �� �������������� ������ 0 */
bool rt0_x_channel_link_functors_x_rt1_t::pop_from_rt0( command_rdm_t & ev, const std::chrono::milliseconds & wait ) {
	if( !closed() ) {
		return _ev_RT0_X->pop( ev, wait );
		}
	else
		return false;
	}

/* ���������� � �������������� ������ 1, ��� ���������� �������� ��������� ����������� �� �������������� ������ 0 */
bool rt0_x_channel_link_functors_x_rt1_t::pop_from_rt0( 
	rt0_x_message_t & pck, const std::chrono::milliseconds & wait ) {

	if(!closed()) {
#pragma message( CBL_VAR_NAME_VALUE(CBL_BUILD_VAR_SNDRCV_REVERSE_GUARD, 75))
		return _obj_RT0_X->pop(pck, wait);
		}
	else
		return false;
	}

/* ���������� � �������������� ������ 1, ��� ���������� �������� ��������� ����������� �� �������������� ������ 0 */
size_t rt0_x_channel_link_functors_x_rt1_t::pop_from_rt0(
	std::list<rt0_x_message_t> & lst, const std::chrono::milliseconds & wait ) {

	if(!closed()) {
#pragma message( CBL_VAR_NAME_VALUE(CBL_BUILD_VAR_SNDRCV_REVERSE_GUARD, 76))
		return _obj_RT0_X->pop(lst, wait);
		}
	else
		return 0;
	}

/* ���������� � �������������� ������ 1, ��� �������� ��������� � ������������� ������ 0 */
addon_push_event_result_t rt0_x_channel_link_functors_x_rt1_t::push_to_rt0_message(rt1_x_message_data_t && object,
	const std::chrono::milliseconds & waitTimeout )noexcept {

	CBL_MPF_KEYTRACE_SET(object.sndrcv_handler().spitTag, 18);
	if( _state.load(std::memory_order::memory_order_relaxed ) == channel_state::ready ){
		if (_rt1_to_x.fmsg(_rt1_to_x.locid, object, waitTimeout)) {
			return addon_push_event_result_t::enum_type::success;
			}
		else {
			return addon_push_event_result_t::enum_type::exception;
			}
		}
	else{
		return addon_push_event_result_t::enum_type::exception;
		}
	}

/* ���������� � �������������� ������ 1, ��� �������� ������ � ������������� ������ 0 */
addon_push_event_result_t rt0_x_channel_link_functors_x_rt1_t::push_command_pack_to_rt0(rt0_x_command_pack_t&& cmd )noexcept {
	if( _state.load( std::memory_order::memory_order_relaxed ) == channel_state::ready ){
		const auto wt = cmd.wait_timeout();
		return _rt1_to_x.fcmd( _rt1_to_x.locid, cmd, wt );
		}
	else{
		return addon_push_event_result_t::enum_type::exception;
		}
	}

bool rt0_x_channel_link_functors_x_rt1_t::push_to_rt1( rt0_x_message_t && obj ) noexcept{

	if(!closed()) {

#pragma message( CBL_VAR_NAME_VALUE(CBL_BUILD_VAR_SNDRCV_REVERSE_GUARD, 61))
		_obj_RT0_X->push(std::move(obj));
		return true;
		}
	else {
		return false;
		}
	}

void rt0_x_channel_link_functors_x_rt1_t::set_channel_state( channel_state st )noexcept{
	_state.store( st, std::memory_order::memory_order_relaxed );
	}

bool rt0_x_channel_link_functors_x_rt1_t::push_to_rt1( command_rdm_t && obj ) noexcept{
	if( !closed() ) {

		if( !obj.cmd.empty() ) {
			_ev_RT0_X->push( std::move( obj ) );
			return true;
			}
		else {
			return false;
			}
		}
	else {
		return false;
		}
	}

rt0_x_channel_link_functors_x_rt1_t::rt0_x_channel_link_functors_x_rt1_t( const channel_info_t & chInf,
	std::weak_ptr<distributed_ctx_t> ctx,
	const rt0_x_channel_link_functors_table_rt0_x_desc_t  & rt1BackLink )
	: _chInf( chInf )
	, _rt1_to_x( rt1BackLink)
	, _ctx(ctx){

	using m_queue_type = std::decay_t<decltype(CHECK_PTR(_ctx)->queue_manager().create_rt0_x_message_queue(
		__FILE_LINE__, queue_options_t::message_stack_module_channel))>;
	using e_queue_type = std::decay_t<decltype(CHECK_PTR(_ctx)->queue_manager().tcreate_queue<command_rdm_t>(
		__FILE_LINE__, queue_options_t::message_stack_module_channel))>;

	if( is_null( _chInf.id() ) ) {
		THROW_MPF_EXC_FWD(nullptr);
		}
	
	if( !_rt1_to_x.fcmd || !_rt1_to_x.fmsg )
		THROW_MPF_EXC_FWD(nullptr);

	_obj_RT0_X = std::make_shared<m_queue_type>(CHECK_PTR(_ctx)->queue_manager().
		create_rt0_x_message_queue(__FILE_LINE__, queue_options_t::message_stack_module_channel));

	_ev_RT0_X = std::make_shared<e_queue_type>(CHECK_PTR(_ctx)->queue_manager().
		tcreate_queue<command_rdm_t>(__FILE_LINE__, queue_options_t::message_stack_module_channel));

#ifdef CBL_MPF_TRACELEVEL_EVENTS_RT0RT1
	std::string log;
	log += "rt0_x_channel_link_functors_x_rt1_t:ctr=" + chInf.id().to_str();
	crm::file_logger_t::logging( log, CBL_MPF_TRACELEVEL_EVENT_RT0_RT1_TAG, __CBL_FILEW__, __LINE__ );
#endif
	}

std::shared_ptr<rt0_x_channel_link_functors_x_rt1_t> rt0_x_channel_link_functors_x_rt1_t::create( 
	const channel_info_t & chInf,
	std::weak_ptr<distributed_ctx_t> ctx,
	const rt0_x_channel_link_functors_table_rt0_x_desc_t & rt1BackLink ) {

	return std::shared_ptr<rt0_x_channel_link_functors_x_rt1_t>( 
		new rt0_x_channel_link_functors_x_rt1_t( chInf, 
		ctx, 
		rt1BackLink ) );
	}

void rt0_x_channel_link_functors_x_rt1_t::close() noexcept {

	bool closedf = false;
	if( _closef.compare_exchange_strong( closedf, true ) ) {

#ifdef CBL_MPF_TRACELEVEL_EVENTS_RT0RT1
		std::string log;
		log += "rt0_x_channel_link_functors_x_rt1_t::close=" + get_channel_info().id().to_str();
		crm::file_logger_t::logging( log, 0, __CBL_FILEW__, __LINE__ );
#endif
		}
	}

bool rt0_x_channel_link_functors_x_rt1_t::closed()const noexcept {
	return _closef.load( std::memory_order::memory_order_acquire );
	}

rt0_x_channel_link_functors_x_rt1_t::~rt0_x_channel_link_functors_x_rt1_t() {
	close();
	}

void rt0_x_channel_link_functors_x_rt1_t::ntf_hndl(std::unique_ptr<dcf_exception_t> && e)const noexcept {
	if(auto c = _ctx.lock()) {
		c->exc_hndl(std::move(e));
		}
	}



/*============================================================================================
rt0_x_channel_link_functors_rt0_x_t
==============================================================================================*/

rt0_x_channel_link_functors_table_rt0_x_t rt0_x_channel_link_functors_rt0_x_t::_clbtbl;
const channel_info_t& rt0_x_channel_link_functors_rt0_x_t::get_channel_info()const {
	return _chInf;
	}

std::shared_ptr<distributed_ctx_t> rt0_x_channel_link_functors_rt0_x_t::ctx()const noexcept {
	return _ctx.lock();
	}

bool rt0_x_channel_link_functors_rt0_x_t::push_to_rt1( rt0_x_message_t && object, const std::chrono::milliseconds & wait) noexcept{
	if(!closed()) {

#ifdef CBL_MPF_MESSAGE_TRACEWAY_KEYS_BY_ID
		const auto tid = trait_at_keytrace_set(object.package());
#endif

#pragma message( CBL_VAR_NAME_VALUE(CBL_BUILD_VAR_SNDRCV_REVERSE_GUARD, 61))
		return _push_to_rt1_msg(std::move(object), wait);
		}
	else {
		return false;
		}
	}

void rt0_x_channel_link_functors_rt0_x_t::set_channel_state( channel_state st )noexcept{
	_set_channel_state( st );
	}

std::chrono::milliseconds rt0_x_channel_link_functors_rt0_x_t::timeout_xchannel()const noexcept {
	if (auto c = _ctx.lock()) {
		return c->policies().timeout_wait_io();
		}
	else {
		return {};
		}
	}

bool rt0_x_channel_link_functors_rt0_x_t::push_to_rt1( command_rdm_t && object)noexcept {
	if( !closed() ) {
		if( !object.cmd.empty() ) {
			return _push_to_rt1_ev( std::move( object ) );
			}
		else {
			return false;
			}
		}
	else {
		return false;
		}
	}

//void rt0_x_channel_link_functors_rt0_x_t::notify( rtx_command_type_t ev, rt0_x_command_pack_t&& cmd )noexcept {
//	if( !closed() ) {
//		CHECK_NO_EXCEPTION_BEGIN
//
//		auto evTargets( find_event_targets( ev ) );
//		all_send( evTargets, std::move( cmd ) );
//
//		CHECK_NO_EXCEPTION_END
//		}
//	}
//
//bool rt0_x_channel_link_functors_rt0_x_t::push_command(rt0_x_command_pack_t&& cmd )noexcept {
//	if( !closed() ) {
//
//		const auto wt = cmd.wait_timeout();
//
//		}
//	else {
//		return false;
//		}
//	}

std::list<rtl_roadmap_event_t> rt0_x_channel_link_functors_rt0_x_t::find_event_targets( rtx_command_type_t ev )const noexcept {
	CHECK_NO_EXCEPTION_BEGIN

	lck_scp_t lck( _tblLck );
	std::list<rtl_roadmap_event_t> lst;

	for( auto it = _evTbl.begin(); it != _evTbl.end(); ++it )
		if( (*it).first == ev )
			lst.push_back( it->second );

	return std::move( lst );

	CHECK_NO_EXCEPTION_END
	}


rt0_x_channel_link_functors_rt0_x_t::rt0_x_channel_link_functors_rt0_x_t( const channel_info_t & chInf,
	std::weak_ptr<i_rt0_channel_route_t> out,
	std::weak_ptr<distributed_ctx_t> ctx_,
	push_to_rt1_msg_f && push_to_rt1_msg,
	push_to_rt1_ev_f && push_to_rt1_ev,
	set_channel_state_f && setChannelSt )
	: _ctx( ctx_ )
	, _locid( CHECK_PTR( ctx_ )->make_local_object_identity() )
	, _chInf( chInf )
	, _root( out )

	, _push_to_rt1_msg( std::move( push_to_rt1_msg ) )
	, _push_to_rt1_ev( std::move( push_to_rt1_ev ) )
	, _set_channel_state(std::move(setChannelSt)){
	
#ifdef CBL_MPF_TRACELEVEL_EVENTS_RT0RT1
	std::string log;
	log += "rt0_x_channel_link_functors_rt0_x_t:ctr=" + chInf.id().to_str();
	crm::file_logger_t::logging( log, CBL_MPF_TRACELEVEL_EVENT_RT0_RT1_TAG, __CBL_FILEW__, __LINE__ );
#endif
	}

std::shared_ptr<rt0_x_channel_link_functors_rt0_x_t> rt0_x_channel_link_functors_rt0_x_t::create(
	const channel_info_t & chInf,
	std::weak_ptr<i_rt0_channel_route_t> out,
	std::weak_ptr<distributed_ctx_t> ctx_,
	push_to_rt1_msg_f && push_to_rt1_msg,
	push_to_rt1_ev_f && push_to_rt1_ev,
	set_channel_state_f && setST ) {

	auto obj( std::shared_ptr<rt0_x_channel_link_functors_rt0_x_t>( new rt0_x_channel_link_functors_rt0_x_t( chInf,
		out,
		ctx_,
		std::move( push_to_rt1_msg ),
		std::move( push_to_rt1_ev ),
		std::move(setST) ) ));
	_clbtbl.reg( obj );

	return std::move( obj );
	}

void rt0_x_channel_link_functors_rt0_x_t::_subscribe( const rtl_roadmap_event_t & id, 
	const std::list<rtx_command_type_t> & evlst ) {

	_unsubscribe( id );
	for( auto iev : evlst ) {
		_evTbl.insert( {iev, id} );
		}

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	__size_trace();
#endif
	}
void rt0_x_channel_link_functors_rt0_x_t::subscribe( const rtl_roadmap_event_t & id,
	const std::list<rtx_command_type_t> & evlst ) {

	lck_scp_t lck( _tblLck );
	_subscribe( id, evlst );
	}

void rt0_x_channel_link_functors_rt0_x_t::_unsubscribe( const rtl_roadmap_event_t & id )noexcept {
	bool has = true;
	while( _evTbl.size() && has ) {

		has = false;
		for( auto it = _evTbl.begin(); it != _evTbl.end(); ++it ) {
			if( id == it->second ) {
				_evTbl.erase( it );
				has = true;
				break;
				}
			}
		}

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	__size_trace();
#endif
	}

void rt0_x_channel_link_functors_rt0_x_t::unsubscribe( const rtl_roadmap_event_t & id )noexcept{
	lck_scp_t lck( _tblLck );
	_unsubscribe( id );
	}

void rt0_x_channel_link_functors_rt0_x_t::close()noexcept {
	close(false);
	}

void rt0_x_channel_link_functors_rt0_x_t::close(bool dctr)noexcept {

	bool closedf = false;
	if( _closef.compare_exchange_strong( closedf, true ) ) {

		CHECK_NO_EXCEPTION( _clbtbl.unreg( locid() ) );

		_evTbl.clear();

#ifdef CBL_OBJECTS_COUNTER_CHECKER
		__size_trace();
#endif

		if(dctr) {
			if(auto hub = _root.lock())
				hub->unregister_rt1_channel(_chInf);
			}
		}
	}

bool rt0_x_channel_link_functors_rt0_x_t::closed()const noexcept {
	return _closef.load( std::memory_order::memory_order_acquire );
	}

rt0_x_channel_link_functors_rt0_x_t::~rt0_x_channel_link_functors_rt0_x_t() {
	close(true);
	}

const local_object_id_t& rt0_x_channel_link_functors_rt0_x_t::locid()const {
	CBL_VERIFY( !_locid.is_null() );
	return _locid;
	}

addon_push_event_result_t rt0_x_channel_link_functors_rt0_x_t::push_from_rt1_to_rt0(rt1_x_message_data_t&& obj)noexcept {
	if (auto rt = _root.lock()) {
		return rt->push_to_rt0_xpeer_XBN(std::move(obj));
		}
	else {
		return addon_push_event_result_t::enum_type::exception;
		}
	}

addon_push_event_result_t rt0_x_channel_link_functors_rt0_x_t::push_from_rt1_to_rt0(rt0_x_command_pack_t&& obj)noexcept {
	if (auto rt = _root.lock()) {
		return rt->push_command_pack_to_rt0(std::move(obj));
		}
	else {
		return addon_push_event_result_t::enum_type::link_unregistered;
		}
	}

rt0_x_channel_link_functors_table_rt0_x_desc_t 
rt0_x_channel_link_functors_rt0_x_t::get_rt1_to_rt0()const {

	rt0_x_channel_link_functors_table_rt0_x_desc_t desc;
	desc.locid = locid();
	desc.fmsg = [](const local_object_id_t& target, const rt1_x_message_data_t &obj, const std::chrono::milliseconds & waitTimeout) {
		return push_message_to_rt0(target, &obj, waitTimeout.count());
		};

	desc.fcmd = [](const local_object_id_t& target, const rt0_x_command_pack_t &obj, const std::chrono::milliseconds & waitTimeout) {
		return push_event_to_rt0(target, &obj, waitTimeout.count());
		};

	return std::move( desc );
	}

bool DCF_ADDON_CALL rt0_x_channel_link_functors_rt0_x_t::push_message_to_rt0(
	const local_object_id_t& target, const rt1_x_message_data_t *obj, long long /*waitTimeout*/) {

	CBL_VERIFY(!obj->rdm().points.rt0().connection_key().address.empty());
	return _clbtbl.push_to_x( target, obj->copy() );
	}

addon_push_event_result_t DCF_ADDON_CALL rt0_x_channel_link_functors_rt0_x_t::push_event_to_rt0(
	const local_object_id_t& target, const rt0_x_command_pack_t *obj, long long /*waitTimeout*/) {

	return _clbtbl.push_to_x( target, obj->cpy_mem() );
	}

void rt0_x_channel_link_functors_rt0_x_t::ntf_hndl(std::unique_ptr<dcf_exception_t> && e)const noexcept {
	if(auto c = _ctx.lock()) {
		c->exc_hndl(std::move(e));
		}
	}


