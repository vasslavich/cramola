#pragma once


#include <map>
#include <memory>
#include <appkit/export_internal_terms.h>


namespace crm::detail{


class i_rt0_x_channel_link_functors_rt0_x_t{
public:
	virtual const local_object_id_t& locid()const = 0;

	[[nodiscard]]
	virtual addon_push_event_result_t push_from_rt1_to_rt0( rt1_x_message_data_t && obj )noexcept = 0;
	
	[[nodiscard]]
	virtual addon_push_event_result_t push_from_rt1_to_rt0( rt0_x_command_pack_t && obj )noexcept = 0;
	
	virtual rt0_x_channel_link_functors_table_rt0_x_desc_t get_rt1_to_rt0()const = 0;
	};


class rt0_x_channel_link_functors_rt0_x_t;
class rt0_x_channel_link_functors_table_rt0_x_t{
private:
	typedef std::mutex lck_t;
	typedef std::lock_guard<lck_t> lckscp_t;

	std::map<local_object_id_t, std::weak_ptr<i_rt0_x_channel_link_functors_rt0_x_t>> _rt0_x_links;
	lck_t _lck;

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	mutable crm::utility::__xvalue_counter_logger_t<> __sizeTraceer = __FILE_LINE__;
	crm::utility::__xobject_counter_logger_t<rt0_x_channel_link_functors_table_rt0_x_t> _xDbgCounter;
	void __size_trace()const noexcept;
#endif

public:
	void reg( std::shared_ptr<i_rt0_x_channel_link_functors_rt0_x_t> );
	void unreg( const local_object_id_t& locid )noexcept;
	bool push_to_x( const local_object_id_t& locid, rt1_x_message_data_t && )noexcept;
	addon_push_event_result_t push_to_x( const local_object_id_t& locid, rt0_x_command_pack_t && )noexcept;
	};

class rt0_x_channel_link_functors_x_rt1_t :
	public io_x_rt1_channel_t,
	public std::enable_shared_from_this<rt0_x_channel_link_functors_x_rt1_t>{

	typedef rt0_x_channel_link_functors_x_rt1_t self_t;

private:
	channel_info_t _chInf;
	/*! ������� �������� �� X �� ������� 0 */
	rt0_x_channel_link_functors_table_rt0_x_desc_t _rt1_to_x;
	/*! ������� ��������� � ������ 0 � ����� X*/
	std::shared_ptr<i_message_queue_t<rt0_x_message_t>> _obj_RT0_X;
	/*! ������� ������ � ������ 0 � ����� X */
	std::shared_ptr<i_message_queue_t<command_rdm_t>> _ev_RT0_X;
	std::weak_ptr<distributed_ctx_t> _ctx;
	std::atomic<channel_state> _state{ channel_state::ready };
	std::atomic<bool> _closef{ false };

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	crm::utility::__xobject_counter_logger_t<self_t> _xDbgCounter;
#endif

	bool pop_from_rt0( command_rdm_t & ev, const std::chrono::milliseconds & wait )final;
	bool pop_from_rt0( rt0_x_message_t & pck, const std::chrono::milliseconds & wait )final;
	size_t pop_from_rt0( std::list<rt0_x_message_t> & lst, const std::chrono::milliseconds & wait )final;
	
	[[nodiscard]]
	addon_push_event_result_t push_to_rt0_message( rt1_x_message_data_t && object, const std::chrono::milliseconds & waitTimeout )noexcept final;

	[[nodiscard]]
	addon_push_event_result_t push_command_pack_to_rt0(rt0_x_command_pack_t&& cmd )noexcept final;

	const channel_info_t& get_channel_info()const final;

	rt0_x_channel_link_functors_x_rt1_t( const channel_info_t & chInf,
		std::weak_ptr<distributed_ctx_t> ctx,
		const rt0_x_channel_link_functors_table_rt0_x_desc_t & rt1BackLink );

	void ntf_hndl( std::unique_ptr<dcf_exception_t> && e )const noexcept;

public:
	~rt0_x_channel_link_functors_x_rt1_t();

	static std::shared_ptr<rt0_x_channel_link_functors_x_rt1_t> create( const channel_info_t & chInf,
		std::weak_ptr<distributed_ctx_t> ctx,
		const rt0_x_channel_link_functors_table_rt0_x_desc_t & rt1BackLink );

	bool push_to_rt1( rt0_x_message_t && )noexcept;
	bool push_to_rt1( command_rdm_t && )noexcept;
	void set_channel_state( channel_state )noexcept;

	void close()noexcept;
	bool closed()const noexcept;
	};


class rt0_x_channel_link_functors_rt0_x_t :
	public io_rt0_x_channel_t,
	public i_rt0_x_channel_link_functors_rt0_x_t,
	public std::enable_shared_from_this<rt0_x_channel_link_functors_rt0_x_t>{

	typedef rt0_x_channel_link_functors_rt0_x_t self_t;

public:
	typedef std::function<bool( rt0_x_message_t &&, const std::chrono::milliseconds & wait)> push_to_rt1_msg_f;
	typedef std::function<bool( command_rdm_t &&)> push_to_rt1_ev_f;
	using set_channel_state_f = std::function<void( channel_state st )>;

private:
	typedef std::mutex lck_t;
	typedef std::unique_lock<lck_t> lck_scp_t;

	static rt0_x_channel_link_functors_table_rt0_x_t _clbtbl;

	/*! �������� ����� */
	std::weak_ptr<distributed_ctx_t> _ctx;
	/*! ������� ����� */
	local_object_id_t _locid;
	channel_info_t _chInf;
	std::weak_ptr<i_rt0_channel_route_t> _root;

	push_to_rt1_msg_f _push_to_rt1_msg;
	push_to_rt1_ev_f _push_to_rt1_ev;
	set_channel_state_f _set_channel_state;

	/*! ������� ������� */
	std::multimap<rtx_command_type_t, rtl_roadmap_event_t> _evTbl;
	mutable lck_t _tblLck;

	std::atomic<bool> _closef{ false };

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	mutable crm::utility::__xvalue_counter_logger_t<> __sizeTraceer = __FILE_LINE__;
	crm::utility::__xobject_counter_logger_t<self_t> _xDbgCounter;
	void __size_trace()const noexcept;
#endif

	std::shared_ptr<distributed_ctx_t> ctx()const noexcept;
	std::list<rtl_roadmap_event_t> find_event_targets( rtx_command_type_t ev )const noexcept final;
	std::chrono::milliseconds timeout_xchannel()const noexcept;

	void set_channel_state(  channel_state st )noexcept;

	[[nodiscard]]
	bool push_to_rt1( rt0_x_message_t && object, const std::chrono::milliseconds & wait)noexcept final;

	[[nodiscard]]
	bool push_to_rt1( command_rdm_t && cmd)noexcept final;

	const channel_info_t& get_channel_info()const final;

	void subscribe( const rtl_roadmap_event_t & id, const std::list<rtx_command_type_t> & evlst )final;
	void unsubscribe( const rtl_roadmap_event_t & id )noexcept final;
	//void notify( rtx_command_type_t ev, rt0_x_command_pack_t&& cmd )noexcept final;
	//bool push_command(rt0_x_command_pack_t && c)noexcept;

	rt0_x_channel_link_functors_rt0_x_t( const channel_info_t & chInf,
		std::weak_ptr<i_rt0_channel_route_t> out,
		std::weak_ptr<distributed_ctx_t> ctx,
		push_to_rt1_msg_f && push_to_rt0_msg,
		push_to_rt1_ev_f && push_to_rt0_ev,
		set_channel_state_f && set_channel_state );

	void _subscribe( const rtl_roadmap_event_t & id, const std::list<rtx_command_type_t> & evlst );
	void _unsubscribe( const rtl_roadmap_event_t & id )noexcept;

	const local_object_id_t& locid()const final;

	[[nodiscard]]
	addon_push_event_result_t push_from_rt1_to_rt0( rt1_x_message_data_t && obj )noexcept final;

	[[nodiscard]]
	addon_push_event_result_t push_from_rt1_to_rt0( rt0_x_command_pack_t && obj )noexcept final;

	rt0_x_channel_link_functors_table_rt0_x_desc_t get_rt1_to_rt0()const final;

	void ntf_hndl( std::unique_ptr<dcf_exception_t> && e )const noexcept;

	void close(bool dctr)noexcept;

public:
	static bool DCF_ADDON_CALL push_message_to_rt0(
		const local_object_id_t& target, const rt1_x_message_data_t *obj, long long waitTimeout );
	static addon_push_event_result_t DCF_ADDON_CALL push_event_to_rt0(
		const local_object_id_t& target, const rt0_x_command_pack_t *obj, long long waitTimeout);

	~rt0_x_channel_link_functors_rt0_x_t();

	static std::shared_ptr<rt0_x_channel_link_functors_rt0_x_t> create( const channel_info_t & chInf,
		std::weak_ptr<i_rt0_channel_route_t> out,
		std::weak_ptr<distributed_ctx_t> ctx,
		push_to_rt1_msg_f && push_to_rt0_msg,
		push_to_rt1_ev_f && push_to_rt0_ev,
		set_channel_state_f && setCST );

	void close()noexcept;
	bool closed()const noexcept;
	};
}
