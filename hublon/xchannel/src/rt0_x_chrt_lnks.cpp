#include <appkit/appkit.h>
#include "../rt0_x_chrt.h"


using namespace crm;
using namespace crm::detail;


#ifdef CBL_OBJECTS_COUNTER_CHECKER
size_t rt0_x_channel_links_table_t::__size_route_ev()const noexcept{
	return _routeEv.size();
	}

void rt0_x_channel_links_table_t::__size_trace()const noexcept{
	CHECK_NO_EXCEPTION(__sizeTraceer.CounterCheck( __size_route_ev() ));
	}
#endif

rt0_x_channel_links_table_t::rt0_x_channel_links_table_t( std::weak_ptr<distributed_ctx_t> ctx,
	std::weak_ptr<i_rt0_main_router_registrator_t> reg0 )
	: _ctx( ctx )
	, _rt0reg( reg0 )
	, _rtMessages( reg0 ){}

rt0_x_channel_links_table_t::~rt0_x_channel_links_table_t() {
	close();
	}

bool rt0_x_channel_links_table_t::closed()const {
	return _closef.load( std::memory_order::memory_order_acquire );
	}

void rt0_x_channel_links_table_t::close() noexcept{
	bool closedf = false;
	if( _closef.compare_exchange_strong( closedf, true ) ) {

		lck_scp_t lck( _rtLck );

		/* �������� ���� ������� ������ �� ������ */
		auto rtCh = _channelLst.begin();
		for( ; rtCh != _channelLst.end(); ++rtCh ) {
			auto obj = rtCh->second.lock();
			if( obj ) {
				CHECK_NO_EXCEPTION( obj->close() );
				}
			}
		_channelLst.clear();

		/* �������� ���� ������� �� ������������� ��������� */
		_rtMessages.close();

		/* �������� ���� ������� �� ������������� ������� */
		auto evIt = _routeEv.begin();
		for( ; evIt != _routeEv.end(); ++evIt ) {
			auto obj = evIt->second.lock();
			if( obj ) {
				CHECK_NO_EXCEPTION( obj->close() );
				}
			}
		_routeEv.clear();
		}
	}

std::list<std::string> rt0_x_channel_links_table_t::channel_list_descriptions()const {
	return _rtMessages.channel_list_descriptions();
	}

std::shared_ptr<io_rt0_x_channel_t>
rt0_x_channel_links_table_t::find_channel( const rtl_roadmap_obj_t & destId )const {

	if( destId.target_id().is_null() && destId.table() == rtl_table_t::undefined )
		FATAL_ERROR_FWD(nullptr);

	return _rtMessages.find_channel( destId );
	}

std::shared_ptr<io_rt0_x_channel_t>
rt0_x_channel_links_table_t::find_channel_RZi_LX(const rtl_roadmap_line_t & rdl, const address_descriptor_t & destId)const {

	CBL_VERIFY(!is_null(destId.destination_id()));
	CBL_VERIFY(!is_null(destId.destination_subscriber_id()));

	return _rtMessages.find_channel_RZi_LX(rdl, destId);
	}

std::shared_ptr< distributed_ctx_t> rt0_x_channel_links_table_t::ctx()const noexcept {
	return _ctx.lock();
	}

std::shared_ptr<io_rt0_x_channel_t>
rt0_x_channel_links_table_t::_find_channel( const rtl_roadmap_event_t & destId )const {

	auto it = _routeEv.find( destId );
	if( it != _routeEv.end() ) {
		CBL_VERIFY( it->first == destId );
		return it->second.lock();
		}
	else
		return std::shared_ptr<io_rt0_x_channel_t>();
	}
std::shared_ptr<io_rt0_x_channel_t>
rt0_x_channel_links_table_t::find_channel( const rtl_roadmap_event_t & destId )const {

	lck_scp_t lck( _rtLck );
	return _find_channel( destId );
	}

std::list< std::shared_ptr<io_rt0_x_channel_t>>
rt0_x_channel_links_table_t::find_channel_list( const rt0_node_rdm_t & rt0,
const identity_descriptor_t & targetId,
const identity_descriptor_t & sourceId,
const rtl_table_t & ttbl )const {

	lck_scp_t lck( _rtLck );
	return _rtMessages.find_channel_list( rt0, targetId, sourceId, ttbl );
	}

std::shared_ptr<io_rt0_x_channel_t>
rt0_x_channel_links_table_t::_find_channel( const channel_identity_t& destId )const {

	if( is_null( destId ) ) {
		THROW_MPF_EXC_FWD(nullptr);
		}

	auto it = _channelLst.find( destId );
	if( it != _channelLst.end() ) {
		CBL_VERIFY( it->first == destId );
		return it->second.lock();
		}
	else
		return std::shared_ptr<io_rt0_x_channel_t>();
	}
std::shared_ptr<io_rt0_x_channel_t>
rt0_x_channel_links_table_t::find_channel( const channel_identity_t& destId )const {

	lck_scp_t lck( _rtLck );
	return _find_channel( destId );
	}

std::shared_ptr<i_rt0_peer_handler_t> rt0_x_channel_links_table_t::find_peer( const rtl_roadmap_obj_t & destId )const {
	return _rtMessages.find_peer( destId );
	}

void rt0_x_channel_links_table_t::ntf_hndl(std::unique_ptr<dcf_exception_t> && n)const noexcept {
	if (auto c = _ctx.lock()) {
		c->exc_hndl(std::move(n));
		}
	}

std::chrono::milliseconds rt0_x_channel_links_table_t::timeout_xchannel()const noexcept {
	if (auto c = _ctx.lock()) {
		return c->policies().timeout_wait_io();
		}
	else {
		return {};
		}
	}


void rt0_x_channel_links_table_t::register_rt1_link( std::weak_ptr<io_rt0_x_channel_t> wchannel,
	std::weak_ptr<rt0_x_channels_table_t > root ) {

	if( !closed() ) {
		auto sch( wchannel.lock() );
		if( !sch )
			THROW_MPF_EXC_FWD(nullptr);

		auto chId = sch->get_channel_info().id();
		if( chId == channel_identity_t::null )
			THROW_MPF_EXC_FWD(nullptr);

		lck_scp_t lck( _rtLck );
		auto it = _channelLst.find( chId );
		if( it != _channelLst.end() ) {

			std::string log;
			log += "rt0_x_channel_links_table_t::register_rt1_link:exists=" + chId.to_str();
			THROW_MPF_EXC_FWD( std::move( log ) );
			}
		else {
			if( !_channelLst.insert( {chId, std::move( wchannel )} ).second )
				THROW_MPF_EXC_FWD(nullptr);
			}
		}
	else {
		THROW_MPF_EXC_FWD(nullptr);
		}
	}

void rt0_x_channel_links_table_t::_unregister_rt1_link_messages(const channel_info_t & chId,
	std::function<void(std::vector<source_object_key_t> && srcKeys)> && unregOutRT0) noexcept{

	CBL_VERIFY(unregOutRT0);

	auto erasedLinsk = _rtMessages.unsubscribe_by_channel(chId);

	/* ������ ��������������� ��������� ��������� ����������� ������ 1 */
	std::vector<source_object_key_t> outboundUnregl;
	for(auto & rt0Id : erasedLinsk) {
		if(rt0Id.rdm.rt0().is_output()) {
			outboundUnregl.push_back( rt0Id.rdm.source_object_key() );
			}

		CHECK_NO_EXCEPTION(rt0Id.invoke_remote_unsubscribe());
		}

	if(auto ctx_ = _ctx.lock() ) {
		ctx_->launch_async(__FILE_LINE__, std::move(unregOutRT0), std::move(outboundUnregl));
		}
	}

void rt0_x_channel_links_table_t::_unregister_rt1_link_events( const channel_info_t & chId )noexcept{

	std::list<rtl_roadmap_event_t> erasedKeys;

	auto it = _routeEv.begin();
	while( it != _routeEv.end() ) {

		auto obj = it->second.lock();
		if( !obj )
			erasedKeys.push_back( it->first );
		else {
			if( obj->get_channel_info() == chId )
				erasedKeys.push_back( it->first );
			}

		++it;
		}

	for( auto & k : erasedKeys )
		_routeEv.erase( k );

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	if( !erasedKeys.empty() ) {
		__size_trace();
		}
#endif
	}

void rt0_x_channel_links_table_t::_unregister_rt1_link_channels( const channel_info_t & chId )noexcept{

	auto it = _channelLst.find( chId.id() );
	if( it != _channelLst.end() ) {

		auto old = it->second.lock();
		if( old ) {
			CHECK_NO_EXCEPTION( old->close() );
			}

		/* ������� ����� �� ������ */
		_channelLst.erase( it );

#ifdef CBL_OBJECTS_COUNTER_CHECKER
		std::string log;
		log += "rt0_x_channel_links_table_t::_unregister_rt1_link(channels)=" + chId.id().to_str();
		log += ":channels list=" + std::to_string( _channelLst.size() );
		crm::file_logger_t::logging( log, NOTIFICATION_TAG_TRACE_OBJCNT, __CBL_FILEW__, __LINE__ );
#endif
		}
	}

void rt0_x_channel_links_table_t::unregister_rt1_link( const channel_info_t & chId,
	std::function<void( std::vector<source_object_key_t> && srcKeys )> && unregOutRT0 ) noexcept{

	if( !closed() ) {

		lck_scp_t lck( _rtLck );

		CHECK_NO_EXCEPTION( 
			_unregister_rt1_link_messages( chId, std::move( unregOutRT0 ) ) );
		CHECK_NO_EXCEPTION(
			_unregister_rt1_link_events( chId ) );
		CHECK_NO_EXCEPTION(
			_unregister_rt1_link_channels( chId ) );
		}
	}

void rt0_x_channel_links_table_t::subscribe_events( const channel_identity_t& channelId,
	const rtl_roadmap_event_t & id,
	const std::list<rtx_command_type_t> & evlst ) {

	if( !closed() ) {

		lck_scp_t lck( _rtLck );
		auto x_channel = _find_channel( channelId );
		if( x_channel ) {
			auto itLnk = _routeEv.find( id );
			if( itLnk != _routeEv.end() ) {
				CBL_VERIFY( itLnk->first == id );

				std::string log;
				log += "rt0_x_channel_links_table_t::subscribe_events:exists, rdm=";
				log += id.glid().to_str() + ":" + id.lcid().to_str() + ":" + cvt2string( id.rt() );
				DUPLICATE_OBJECT_THROW_EXC_FWD( std::move( log ) );
				}
			else {
				if( !_routeEv.insert( {id, x_channel} ).second )
					THROW_MPF_EXC_FWD(nullptr);
				x_channel->subscribe( id, evlst );

#ifdef CBL_OBJECTS_COUNTER_CHECKER
				__size_trace();
#endif
				}
			}
		else {
			THROW_MPF_EXC_FWD( "RT0:rt0_x_channel_links_table_t: link[" + channelId.to_str() + "] not found(!)" );
			}
		}
	else {
		THROW_MPF_EXC_FWD( L"rt0_x_channel_links_table_t::subscribe_events:failure(closed)" );
		}
	}

void rt0_x_channel_links_table_t::add_rt0( const rt0_node_rdm_t & id ) {
	if( !closed() ) {
		_rtMessages.add_rt0( id );
		}
	}

void rt0_x_channel_links_table_t::subscribe_messages( const channel_identity_t& channelId,
	const rtl_roadmap_obj_t &id,
	std::shared_ptr<i_rt0_peer_handler_t> && peer,
	rtmessate_entry_t::unregister_f && unsubscriber ) {

	if( !closed() ) {

		if( id.table() != rtl_table_t::rt1_router ) {
			if(is_null(id.rt0()) ) {
				THROW_MPF_EXC_FWD(nullptr);
				}
			}

		lck_scp_t lck( _rtLck );
		auto lnk = _find_channel( channelId );
		if( lnk ) {
			_rtMessages.subscribe_messages(id, rtmessate_entry_t::make(std::move(lnk), std::move(peer), std::move(unsubscriber)));
			}
		else {
			THROW_MPF_EXC_FWD( "RT0:rt0_x_channel_links_table_t: link[" + channelId.to_str() + "] not found(!)" );
			}
		}
	else {
		THROW_MPF_EXC_FWD( L"rt0_x_channel_links_table_t::subscribe_messages:failure(closed)" );
		}
	}

void rt0_x_channel_links_table_t::unsubscribe_events( const channel_identity_t& channelId,
	const rtl_roadmap_event_t & id ) noexcept{

	if( !closed() ) {

		lck_scp_t lck( _rtLck );
		auto lnk = _find_channel( channelId );
		if( lnk ) {
			CHECK_NO_EXCEPTION( lnk->unsubscribe( id ) );
			}

		_routeEv.erase( id );

#ifdef CBL_OBJECTS_COUNTER_CHECKER
		__size_trace();
#endif
		}
	}

rtl_rdm_keypair_t rt0_x_channel_links_table_t::unsubscribe_messages( const channel_identity_t& /*channelId*/,
	const rtl_roadmap_obj_t & id )noexcept{

	rtl_rdm_keypair_t result;
	if( !closed() ) {

		auto link = _rtMessages.unsubscribe_messages( id );
		if( !is_null( link.rdm ) )
			result = std::move( link );
		}

	return std::move( result );
	}

void rt0_x_channel_links_table_t::unsubscribe_by_node( const rt0_node_rdm_t & nodeId )noexcept{

	if( !closed() ) {
		lck_scp_t lck( _rtLck );

		/* ������� �� ������� ������������� ��������� */
		auto removeKeys( _rtMessages.unsubscribe_by_rt0( nodeId ) );

		auto itMss = removeKeys.begin();
		for( ; itMss != removeKeys.end(); ++itMss ) {

			auto xch(CHECK_PTR_STRONG((*itMss).channel)->channel2x_.lock());

			/* ������� � ������ ������ 1 ����������� � �������� ���������� �� ������ 0 */
			if( xch ) {

				if( nodeId.direction() == rtl_table_t::in ) {
					CHECK_NO_EXCEPTION( xch->notify_to_rt1( rtx_command_type_t::in_connectoin_closed, 
						i_rt0_cmd_incoming_closed_t(nodeId, rtl_roadmap_event_t::null)) );
					}
				else if( nodeId.direction() == rtl_table_t::out ) {
					CHECK_NO_EXCEPTION( xch->notify_to_rt1( rtx_command_type_t::out_connection_closed, 
						i_rt0_cmd_outcoming_closed_t(nodeId, rtl_roadmap_event_t::null)) );
					}
				else {
					FATAL_ERROR_FWD(nullptr);
					}
				}
			}
		}
	}

