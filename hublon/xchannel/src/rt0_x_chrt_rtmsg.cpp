#include <appkit/appkit.h>
#include "../rt0_x_chrt.h"


using namespace crm;
using namespace crm::detail;

static std::unique_ptr< rtmessate_entry_t> nullRTMessageEntry;

#ifdef CBL_OBJECTS_COUNTER_CHECKER
size_t rt0_x_channel_links_rtmessage_t::__size_full()const noexcept{
	size_t s0 = 0;
	size_t s1 = 0;

	for( const auto & i0 : _tbl ) {
		++s0;
		s1 += i0.second.size();
		}

	return (s0 + s1);
	}

void rt0_x_channel_links_rtmessage_t::__size_trace()const noexcept{
	CHECK_NO_EXCEPTION( __sizeTraceer.CounterCheck( __size_full() ) );
	}
#endif


rt0_x_channel_links_rtmessage_t::rt0_x_channel_links_rtmessage_t( std::weak_ptr<i_rt0_main_router_registrator_t> rt0reg )
	: _rt0Reg( rt0reg ) {}

rt0_x_channel_links_rtmessage_t::~rt0_x_channel_links_rtmessage_t() {
	_close();
	}

void rt0_x_channel_links_rtmessage_t::close() noexcept{
	lck_scp_t lck( _rtLck );
	_close();
	}

void rt0_x_channel_links_rtmessage_t::_close() noexcept{
	bool closedf = false;
	if( _closef.compare_exchange_strong( closedf, true ) ) {

		CHECK_NO_EXCEPTION_BEGIN

			/* �������� ���� ������� �� ������������� ��������� */

			/* rt 0 */
			auto rt0 = _tbl.begin();
		for (; rt0 != _tbl.end(); ++rt0) {

			/* rt 1 */
			auto rtObj = rt0->second.begin();
			for (; rtObj != rt0->second.end(); ++rtObj) {
				auto obj = CHECK_PTR_STRONG(rtObj->second)->channel2x_.lock();
				if (obj) {
					CHECK_NO_EXCEPTION(obj->close());
					}
				}
			}
		_tbl.clear();

#ifdef CBL_OBJECTS_COUNTER_CHECKER
		CHECK_NO_EXCEPTION(__size_trace());
#endif

		CHECK_NO_EXCEPTION_END
		}
	}

std::list<std::string> rt0_x_channel_links_rtmessage_t::channel_list_descriptions()const {
	std::list<std::string> lst;

	lck_scp_t lck( _rtLck );

	/* rt 0 */
	auto it_RT0 = _tbl.begin();
	for( ; it_RT0 != _tbl.end(); ++it_RT0 ) {

		/* rt 1 */
		auto it_RT1 = it_RT0->second.begin();
		for( ; it_RT1 != it_RT0->second.end(); ++it_RT1 ) {
			lst.push_back( it_RT1->first.to_str() );
			}
		}

	return std::move( lst );
	}

const std::unique_ptr<rtmessate_entry_t>& rt0_x_channel_links_rtmessage_t::_find_entry(const rtl_roadmap_obj_t &id) const {

	/* rt0 */
	auto it_RT0 = _tbl.find(id.rt0());
	if (it_RT0 == _tbl.end()) {
		return nullRTMessageEntry;
		}
	else {

		/* rt 1 */
		auto it = it_RT0->second.find(id);
		if (it != it_RT0->second.end()) {
			if (it->first != id)
				FATAL_ERROR_FWD(nullptr);

			return it->second;
			}
		else {
			return nullRTMessageEntry;
			}
		}
	}

const std::unique_ptr< rtmessate_entry_t>& rt0_x_channel_links_rtmessage_t::_find_entry_RZi_LX(const rtl_roadmap_line_t & rdl, const address_descriptor_t &id)const {
	CBL_VERIFY(!is_null(rdl.points.rt0()));
	CBL_VERIFY(!is_null(id.destination_subscriber_id()));
	CBL_VERIFY(!is_null(id.destination_id()));

	crm::file_logger_t::logging("RZi_LX not found:" + id.to_string(), 112);

	/* rt0 */
	auto it_RT0 = _tbl.find(rdl.points.rt0());
	if (it_RT0 == _tbl.end()) {
		return nullRTMessageEntry;
		}
	else {
		return nullRTMessageEntry;
		}
	}

std::shared_ptr<io_rt0_x_channel_t>
rt0_x_channel_links_rtmessage_t::_find_channel2x(const rtl_roadmap_obj_t & destId)const noexcept{

	if (auto & pE = _find_entry(destId)) {
		return pE->channel2x_.lock();
		}
	else {
		return {};
		}
	}

std::shared_ptr<io_rt0_x_channel_t>
rt0_x_channel_links_rtmessage_t::_find_channel2x_RZi_LX(const rtl_roadmap_line_t & rdl, const address_descriptor_t & destId)const {

	if (auto & pE = _find_entry_RZi_LX(rdl, destId)) {
		return pE->channel2x_.lock();
		}
	else {
		return {};
		}
	}

std::shared_ptr<i_rt0_peer_handler_t> rt0_x_channel_links_rtmessage_t::_find_peer(const rtl_roadmap_obj_t & destId)const noexcept{
	if (auto & pE = _find_entry(destId)) {
		return pE->xpeer_;
		}
	else {
		return {};
		}
	}

std::list<std::shared_ptr<io_rt0_x_channel_t>>
rt0_x_channel_links_rtmessage_t::_find_channel2x_list( const rt0_node_rdm_t & rt0,
const identity_descriptor_t & targetId,
const identity_descriptor_t & sourceId,
const rtl_table_t & ttbl )const noexcept{

	std::list<std::shared_ptr<io_rt0_x_channel_t>>result;

	/* rt 0 */
	auto it_RT0 = _tbl.find( rt0 );
	if( it_RT0 != _tbl.end() ) {

		/* rt 1 */
		auto it = it_RT0->second.begin();
		for( ; it != it_RT0->second.end(); ++it ) {

			CBL_VERIFY( it->first.rt0() == rt0 );

			if( it->first.target_id() == targetId && it->first.source_id() == sourceId && it->first.table() == ttbl ) {

				if (it->second) {
					if (auto obj = it->second->channel2x_.lock()) {
						result.push_back(std::move(obj));
						}
					}
				}
			}
		}

	return std::move( result );
	}

void rt0_x_channel_links_rtmessage_t::__add_rt0( const rt0_node_rdm_t& rt0_ID ) {

	auto itRT0 = _tbl.find( rt0_ID );
	if( itRT0 == _tbl.end() ) {
		if( !_tbl.insert( {rt0_ID, rt0_channel_t()} ).second ) {
			THROW_MPF_EXC_FWD(nullptr);
			}
		}
	}

void rt0_x_channel_links_rtmessage_t::_insert( const rtl_roadmap_obj_t &id,
	rtmessate_entry_t && ch) {

	CBL_VERIFY(!is_null(id.target_id()));

	/* ����������� ��������������� ���������� ������ �� ����� */
	CBL_VERIFY(id.table() != rtl_table_t::rt1_router
		|| (
			id.rt0() == rt0_node_rdm_t::create_rt1_router_rdm(id.target_id().to_str()) &&
			is_null(id.source_id()) && is_null(id.session())
			)
	);

	if( !is_null( id.rt0() ) ) {
		if(  ch.xpeer_->is_opened() ) {

			/* rt 0 */
			auto itRT0 = _tbl.find( id.rt0() );
			if( itRT0 == _tbl.end() ) {
				auto rt0KP = _tbl.insert( {id.rt0(), rt0_channel_t()} );
				if( !rt0KP.second ) {
					THROW_MPF_EXC_FWD(nullptr);
					}
				else
					itRT0 = rt0KP.first;
				}

			/* rt 1 */
			auto itLnk = itRT0->second.find( id );
			if( itLnk != itRT0->second.end() ) {
				CBL_VERIFY( itLnk->first == id );

				std::string log;
				log += "rt0_x_channel_links_rtmessage_t::insert:exists, rdm(exists)=" + id.to_str();
				log += ":rdm(target)=" + id.to_str();
				log += ":" + std::to_string( _tbl.size() ) + " / " + std::to_string( _size() );
				DUPLICATE_OBJECT_THROW_EXC_FWD( std::move( log ) );
				}
			else {
				if (!itRT0->second.insert({id, std::make_unique<rtmessate_entry_t>(std::move(ch))}).second) {
					THROW_MPF_EXC_FWD(nullptr);
					}


#ifdef CBL_OBJECTS_COUNTER_CHECKER
				CHECK_NO_EXCEPTION( __size_trace() );
#endif
				}
			}
		else {
			THROW_MPF_EXC_FWD(nullptr);
			}
		}
	else {
		THROW_MPF_EXC_FWD(nullptr);
		}
	}

rtl_rdm_keypair_t rt0_x_channel_links_rtmessage_t::_erase( const rtl_roadmap_obj_t & id )noexcept{

	rtl_rdm_keypair_t keyp;

	/* rt 0 */
	auto itRT0 = _tbl.find( id.rt0() );
	if( itRT0 != _tbl.end() ) {

		/* rt 1 */
		auto itRT1 = itRT0->second.find( id );
		if( itRT1 != itRT0->second.end() ) {
			
			keyp.rdm = id;
			keyp.channel = std::move(itRT1->second);

			itRT0->second.erase( itRT1 );
			if( itRT0->second.empty() ) {
				CHECK_NO_EXCEPTION( _tbl.erase( itRT0 ) );
				}

#ifdef CBL_OBJECTS_COUNTER_CHECKER
			CHECK_NO_EXCEPTION( __size_trace() );
#endif
			}
		}

	return keyp;
	}

std::list<rtl_rdm_keypair_t> rt0_x_channel_links_rtmessage_t::_erase( const rt0_node_rdm_t & rt0_ID )noexcept{

	std::list<rtl_rdm_keypair_t> lst;

	CHECK_NO_EXCEPTION_BEGIN

	auto rt0reg( _rt0Reg.lock() );
	if( rt0reg ) {
		CHECK_NO_EXCEPTION( rt0reg->unbind_out_connection( rt0_ID.connection_key() ) );
		}

	/* rt 0 */
	auto it_RT0 = _tbl.find( rt0_ID );
	if( it_RT0 != _tbl.end() ) {
		
		/* rt 1 */
		auto it_RT1 = it_RT0->second.begin();
		while( it_RT1 != it_RT0->second.end() ) {
			
			if( !(it_RT1->first.rt0() == rt0_ID) )
				FATAL_ERROR_FWD(nullptr);

			lst.emplace_back(it_RT1->first, std::move(it_RT1->second));

			++it_RT1;
			}

		_tbl.erase( it_RT0 );

#ifdef CBL_OBJECTS_COUNTER_CHECKER
		CHECK_NO_EXCEPTION( __size_trace() );
#endif
		}

	CHECK_NO_EXCEPTION_END

	return lst;
	}

std::list<rtl_rdm_keypair_t> rt0_x_channel_links_rtmessage_t::_erase( const channel_info_t & chId ) noexcept{

	std::list<rtl_rdm_keypair_t> lst;

	CHECK_NO_EXCEPTION_BEGIN

		/* rt 0 */
		auto it_RT0 = _tbl.begin();
	while (it_RT0 != _tbl.end()) {

		/* rt 1 */
		auto it_RT1 = it_RT0->second.begin();
		while (it_RT1 != it_RT0->second.end()) {

			rtl_rdm_keypair_t keyp;

			auto obj = CHECK_PTR_STRONG(it_RT1->second)->channel2x_.lock();
			if (obj) {
				if (obj->get_channel_info() == chId) {

					keyp.rdm = it_RT1->first;
					keyp.channel = std::move(it_RT1->second);

					it_RT1 = it_RT0->second.erase(it_RT1);
					}
				else
					++it_RT1;
				}
			else {
				keyp.rdm = it_RT1->first;
				keyp.channel = std::move(it_RT1->second);

				it_RT1 = it_RT0->second.erase(it_RT1);
				}

			if (!is_null(keyp.rdm))
				lst.emplace_back(std::move(keyp));
			}

		if (it_RT0->second.empty()) {
			it_RT0 = _tbl.erase(it_RT0);
			}
		else {
			++it_RT0;
			}
		}

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	__size_trace();
#endif

	CHECK_NO_EXCEPTION_END

		return lst;
	}

void rt0_x_channel_links_rtmessage_t::add_rt0( const rt0_node_rdm_t& rt0_ID ) {
	lck_scp_t lck( _rtLck );
	__add_rt0( rt0_ID );
	}

void rt0_x_channel_links_rtmessage_t::subscribe_messages( const rtl_roadmap_obj_t &id, 
	rtmessate_entry_t && ch) {

	lck_scp_t lck( _rtLck );
	_insert(id, std::move(ch));
	}

rtl_rdm_keypair_t rt0_x_channel_links_rtmessage_t::unsubscribe_messages( const rtl_roadmap_obj_t & id ) noexcept{

	lck_scp_t lck( _rtLck );
	return _erase( id );
	}

std::list<rtl_rdm_keypair_t> rt0_x_channel_links_rtmessage_t::unsubscribe_by_rt0( const rt0_node_rdm_t & rt0_ID ) noexcept{

	lck_scp_t lck( _rtLck );
	return _erase( rt0_ID );
	}

std::list<rtl_rdm_keypair_t> rt0_x_channel_links_rtmessage_t::unsubscribe_by_channel( const channel_info_t & chID )noexcept{

	lck_scp_t lck( _rtLck );
	return _erase( chID );
	}

std::shared_ptr<io_rt0_x_channel_t> rt0_x_channel_links_rtmessage_t::find_channel(
	const rtl_roadmap_obj_t & destId )const noexcept{

	lck_scp_t lck( _rtLck );
	return _find_channel2x( destId );
	}

std::shared_ptr<io_rt0_x_channel_t> rt0_x_channel_links_rtmessage_t::find_channel_RZi_LX(
	const rtl_roadmap_line_t & rdl, const address_descriptor_t & a)const {

	lck_scp_t lck(_rtLck);
	return _find_channel2x_RZi_LX(rdl, a);
	}


std::list<std::shared_ptr<io_rt0_x_channel_t>> rt0_x_channel_links_rtmessage_t::find_channel_list( const rt0_node_rdm_t & rt0,
	const identity_descriptor_t & targetId,
	const identity_descriptor_t & sourceId,
	const rtl_table_t & ttbl )const noexcept{

	lck_scp_t lck( _rtLck );
	return _find_channel2x_list( rt0, targetId, sourceId, ttbl );
	}

std::shared_ptr<i_rt0_peer_handler_t> rt0_x_channel_links_rtmessage_t::find_peer( const rtl_roadmap_obj_t & destId )const noexcept{
	lck_scp_t lck( _rtLck );
	return _find_peer( destId );
	}

size_t rt0_x_channel_links_rtmessage_t::_size()const noexcept{

	size_t cnt = 0;

	/* rt 0 */
	auto it_RT0 = _tbl.begin();
	for( ; it_RT0 != _tbl.end(); ++it_RT0 ) {
		cnt += it_RT0->second.size();
		}

	return cnt;
	}


size_t rt0_x_channel_links_rtmessage_t::size()const noexcept{
	lck_scp_t lck( _rtLck );
	return _size();
	}
