#include <appkit/appkit.h>
#include "../rt0_x_chrt.h"
#include "../../router_0/rt0_main.h"


using namespace crm;
using namespace crm::detail;



rt0_x_channels_table_t::~rt0_x_channels_table_t() {
	close();
	}

bool rt0_x_channels_table_t::closed()const noexcept {
	return _closef.load(std::memory_order::memory_order_acquire);
	}

void rt0_x_channels_table_t::close() noexcept {
	bool closedf = false;
	if (_closef.compare_exchange_strong(closedf, true)) {

		CHECK_NO_EXCEPTION(_lnkTbl.close());

#ifdef CBL_MPF_TRACELEVEL_EVENTS_RT0RT1
		std::string log;
		log += "rt0_x_channel_links_table_t::close";
		crm::file_logger_t::logging(log, 0, __CBL_FILEW__, __LINE__);
#endif
		}
	}

void rt0_x_channels_table_t::register_rt1_channel(std::weak_ptr<io_rt0_x_channel_t> channel) {
	if (!closed())
		_lnkTbl.register_rt1_link(channel, shared_from_this());
	}

void rt0_x_channels_table_t::unregister_rt1_channel(const channel_info_t& chId)noexcept {
	if (!closed()) {

		auto unregRT0_Link = _rt0Registrator;
		auto unregRT0 = [unregRT0_Link](std::vector<source_object_key_t>&& srcKeys) noexcept {
			auto sptr(unregRT0_Link.lock());
			if (sptr) {
				sptr->unbind_out_connection(std::move(srcKeys));
				}
			};

		CHECK_NO_EXCEPTION(_lnkTbl.unregister_rt1_link(chId, std::move(unregRT0)));
		}
	}

void rt0_x_channels_table_t::do_not_found(rtl_roadmap_obj_t && targetRdm_, rt0_x_message_unit_t && obj)const {

	std::string merr = "target not found="
		+ std::to_string(obj.unit().package().type().utype())
		+ ":" + obj.unit().package().address().to_string()
		+ ":" + obj.unit().rdm().points.rt0().to_str()
		+ ":" + obj.unit().rdm().points.rt1_stream_table_key().to_string()
		+ ":" + targetRdm_.to_str();

	if (obj.unit().package().type().type() == iol_types_t::st_RZi_LX_error) {
		address_descriptor_t destAddr;
		std::unique_ptr<dcf_exception_t> e;

		exception_RZi_LX_extract(*CHECK_PTR(_ctx), std::move(obj).capture_unit().capture_package(), destAddr, e);

		merr += "\nrzi error descriptor:\n";
		merr += "\naddr=" + destAddr.to_string();

		if (e)
			merr += "\nerror=" + cvt(e->msg());
		}

#if(CBL_MPF_TRACELEVEL_ST >= CBL_MPF_TRACELEVEL_ST_4)

	merr += '\n';
	auto lst(_lnkTbl.channel_list_descriptions());
	for (auto & o : lst) {
		merr += o + "\n";
		}

	crm::file_logger_t::logging(merr, 0, __CBL_FILEW__, __LINE__);
#endif

	NOT_FOUND_THROW_EXC_FWD(merr);
	}

void rt0_x_channels_table_t::do_found(std::shared_ptr<io_rt0_x_channel_t> && link, rt0_x_message_unit_t && obj)const {

#ifdef CBL_MPF_TEST_EMULATION_REMOTE_HOST_CONNECTION_NOT_FOUND
	auto ctx_(_ctx.lock());
	if (ctx_->test_emulation_error() && obj.unit().package().type().type() == iol_types_t::st_connection) {
		CBL_MPF_KEYTRACE_SET(obj, 143);
		NOT_FOUND_THROW_EXC_FWD(L"test exception");
		}
#endif

#pragma message( CBL_VAR_NAME_VALUE(CBL_BUILD_VAR_SNDRCV_REVERSE_GUARD, 51))
	if (link->push_to_rt1(obj.capture_unit(), timeout_xchannel())) {
		CBL_MPF_KEYTRACE_SET(obj, 144);
		obj.reset_invoke_exception_guard().reset();
		}
	else {
		CBL_MPF_KEYTRACE_SET(obj, 145);
		obj.reset_invoke_exception_guard().invoke_async(CREATE_MPF_PTR_EXC_FWD(nullptr));
		}
	}

void rt0_x_channels_table_t::push_to_rt1(rt0_x_message_unit_t && obj) {

	if (!closed()) {

#ifdef CBL_MPF_TRACELEVEL_EVENT_CONNECTION
		if (obj.unit().package().type().type() == iol_types_t::st_disconnection) {
			std::string log;
			log += "rt0_x_channels_table_t::2rt1: disconnection";
			log += ":rdm=" + obj.unit().rdm().points.to_str();
			crm::file_logger_t::logging(CBL_MPF_PREFIX_RT_0_1(log), CBL_MPF_TRACELEVEL_EVENT_RT0_RT1_TAG);
			}
		else if (obj.unit().package().type().type() == iol_types_t::st_connection) {
			std::string log;
			log += "rt0_x_channels_table_t::2rt1: connection";
			log += ":rdm=" + obj.unit().rdm().points.to_str();
			crm::file_logger_t::logging(CBL_MPF_PREFIX_RT_0_1(log), CBL_MPF_TRACELEVEL_EVENT_RT0_RT1_TAG);
			}
#endif

		/* �������� ������������ �������� ���������� � ����� */
		CBL_VERIFY(obj.unit().rdm().points.target_id() == obj.unit().package().address().destination_id()
			&& obj.unit().rdm().points.source_id() == obj.unit().package().address().source_id());

		auto tableKey = obj.unit().rdm().points.table();

		/* ��������� - "����� ����������� �� ������ 1" - ������ ���� ���������� ��������������� �������������� ������ 1 */
		if (obj.unit().package().type().type() == iol_types_t::st_connection)
			tableKey = rtl_table_t::rt1_router;

		if (tableKey == rtl_table_t::undefined)
			FATAL_ERROR_FWD(nullptr);

		/* ���� �������� ��������� */
		rtl_roadmap_obj_t targetRdm_;
		/* ����� �������� ��������� */
		std::shared_ptr<io_rt0_x_channel_t> link;

		MPF_TEST_EMULATION_IN_CONTEXT_COND_TRCX(_ctx.lock(), 101,
			is_trait_at_emulation_context(obj.unit().package()),
			trait_at_keytrace_set(obj.unit().package()));

		/* ��������� ������������ �������������� */
		if (rtl_table_t::rt1_router == tableKey) {
			targetRdm_ = rtl_roadmap_obj_t::make(rt0_node_rdm_t::create_rt1_router_rdm(
				obj.unit().rdm().points.target_id().to_str()),
				obj.unit().rdm().points.target_id(),
				identity_descriptor_t::null,
				session_description_t(),
				tableKey);
			}

		/* ��������� ������������ ������ �� ��������, ������������ � �������������� */
		else {
			targetRdm_ = rtl_roadmap_obj_t::make(obj.unit().rdm().points.rt0(),
				obj.unit().rdm().points.target_id(),
				obj.unit().rdm().points.source_id(),
				obj.unit().rdm().points.session(),
				tableKey);
			}

		link = _lnkTbl.find_channel(targetRdm_);

		/* �������� ��������� � ������������ ���� */
		if (link) {
			do_found(std::move(link), std::move(obj));
			}
		else {
			if (obj.unit().package().type().utype() == (int)iol_types_t::st_RZi_LX_error) {
				if (link = _lnkTbl.find_channel_RZi_LX(obj.unit().rdm(), obj.unit().package().address())) {
					do_found(std::move(link), std::move(obj));
					}
				else {
					do_not_found(std::move(targetRdm_), std::move(obj));
					}
				}
			else if (obj.unit().package().type().utype() != (int)iol_types_t::st_disconnection &&
				obj.unit().package().type().utype() != (int)iol_types_t::st_notification) {

				do_not_found(std::move(targetRdm_), std::move(obj));
				}
			}
		}
	else {

#ifdef CBL_MPF_TRACELEVEL_FAULTED_RT0RT1
		std::string log;
		log += "rt0_x_channels_table_t::push_to_rt1:closed";
		log += ":target id(rdm)=" + obj.unit().rdm().points.target_id().to_str() + ":" + obj.unit().rdm().points.source_id().to_str() + ":";
		log += cvt2string(obj.unit().package().type().type());
		crm::file_logger_t::logging(log, 0, __CBL_FILEW__, __LINE__);
#endif
		}
	}

void rt0_x_channels_table_t::subscribe_events(const channel_identity_t& channelId,
	const rtl_roadmap_event_t & id,
	const std::list<rtx_command_type_t> & evlst) {

	_lnkTbl.subscribe_events(channelId, id, evlst);
	}

void rt0_x_channels_table_t::subscribe_messages(const channel_identity_t& channelId,
	const rtl_roadmap_obj_t & id,
	std::shared_ptr<i_rt0_peer_handler_t> && peer,
	rtmessate_entry_t::unregister_f && unsubscriber) {

	_lnkTbl.subscribe_messages(channelId, id, std::move(peer), std::move(unsubscriber));
	}

void rt0_x_channels_table_t::add_rt0(const rt0_node_rdm_t& rt0_ID) {
	_lnkTbl.add_rt0(rt0_ID);
	}

void rt0_x_channels_table_t::unsubscribe_events(const channel_identity_t& channelId,
	const rtl_roadmap_event_t & id)noexcept {
	_lnkTbl.unsubscribe_events(channelId, id);
	}

rtl_rdm_keypair_t rt0_x_channels_table_t::unsubscribe_messages(const channel_identity_t& channelId,
	const rtl_roadmap_obj_t & id)noexcept {
	return _lnkTbl.unsubscribe_messages(channelId, id);
	}

void rt0_x_channels_table_t::unsubscribe_by_node(const rt0_node_rdm_t & nodeId)noexcept {
	_lnkTbl.unsubscribe_by_node(nodeId);
	}

void rt0_x_channels_table_t::__push2peer(rtl_roadmap_obj_t && desc, rt1_x_message_t && mx) {
	CBL_MPF_KEYTRACE_SET(mx.package().desc().spitTag, 20);

	if (auto ph = _lnkTbl.find_peer(desc)) {
		CBL_MPF_KEYTRACE_SET(mx.package().desc().spitTag, 21);
		ph->push(std::move(mx));
		}
	else {
		if (auto rt0 = _rt0Registrator.lock()) {
			if (auto p2 = rt0->find_peer(desc.rt0())) {
				CBL_MPF_KEYTRACE_SET(mx.package().desc().spitTag, 22);
				p2->push(std::move(mx));
				}
			else {
				notify_rt1(rtx_command_type_t::invalid_xpeer, i_rt0_invalid_xpeer_t(std::move(desc)));
				THROW_MPF_EXC_FWD(nullptr);
				}
			}
		else {
			notify_rt1(rtx_command_type_t::invalid_xpeer, i_rt0_invalid_xpeer_t(std::move(desc)));
			THROW_MPF_EXC_FWD(nullptr);
			}
		}
	}


rt1_x_message_t rt0_x_channels_table_t::processing(rt1_x_message_data_t && m_)noexcept {
	CHECK_NO_EXCEPTION_BEGIN

#ifdef CBL_IS_KEEP_OUTCOME_MESSAGE_RESPONSE_GUARD_L0

	CBL_VERIFY(!m_.rdm().points.rt0().connection_key().address.empty());

	if (m_.sndrcv_handler().is_null()) {
		return rt1_x_message_t::make(std::move(m_).rdm(), outbound_message_unit_t::make("rt0_x_channels_table_t::processing-1", std::move(m_).capture_package()));
		}
	else {
		auto sdrcvh = std::move(m_).sndrcv_handler();
		CBL_VERIFY_SNDRCV_KEY_SPIN_TAG(sdrcvh);
		CBL_VERIFY(sdrcvh.type.fl_report() || !is_null(sdrcvh.requestEmitter) || !is_null(sdrcvh.responseTarget));
		CBL_VERIFY(sdrcvh.type.type() != iol_types_t::st_notification);

		auto guarder = inbound_message_with_tail_exception_guard(
			[wptr = weak_from_this(), sdrcvh, rdm = m_.rdm()](std::unique_ptr<dcf_exception_t> && exc)mutable noexcept {

			CBL_VERIFY_SNDRCV_KEY_SPIN_TAG(sdrcvh);
			CBL_VERIFY(!is_null(sdrcvh.requestEmitter) || sdrcvh.type.fl_report());
			CBL_VERIFY(!is_null(sdrcvh.level));
			CBL_VERIFY(!is_null(sdrcvh.chid));


			CHECK_NO_EXCEPTION_BEGIN

				CBL_MPF_KEYTRACE_SET(sdrcvh.spitTag, 504);

				if (auto s = wptr.lock()) {
					if (!is_null(sdrcvh.requestEmitter)) {
						std::wostringstream what;
						what << L"sndrcv-guard(request:r0tbl)";
						what << L":addr=" << rdm.points.rt0().connection_key().to_wstring();
						what << L":s=" << rdm.points.source_id();
						what << L":t=" << rdm.points.target_id();
						what << L":st=" << sdrcvh.spitTag;

						if (!exc) {
							exc = CREATE_MPF_PTR_EXC_FWD(what.str());
							}
						else {
							exc->add_message_line(what.str());
							}

						exc->set_code(RQo_L0);

#ifdef CBL_MPF_TRACELEVEL_EVENT_SNDRCV_GUARD
						GLOBAL_STOUT_FWDRITE_STR(L"sndrcv-guard(request:r0tbl):" + exc->msg());
#endif

						CBL_MPF_KEYTRACE_SET(sdrcvh.spitTag, 511);

						CBL_VERIFY(sdrcvh.level == rtl_level_t::l1 || sdrcvh.level == rtl_level_t::l0);
						if (!s->notify_rt1(sdrcvh.chid, i_rt0_cmd_sndrcv_fault_t(*CHECK_PTR(s->_ctx), std::move(sdrcvh), std::move(exc)))) {
							if (auto c = s->_ctx.lock()) {
								c->ntf_hndl(exc->dcpy());
								}
							}
						}
					else if (sdrcvh.type.fl_report()) {
						CBL_VERIFY(!is_null(sdrcvh.responseTarget));

						std::wostringstream what;
						what << L"sndrcv-guard(response:r0tbl)";
						what << L":addr=" << rdm.points.rt0().connection_key().to_wstring();
						what << L":s=" << rdm.points.source_id();
						what << L":t=" << rdm.points.target_id();
						what << L":st=" << sdrcvh.spitTag;

						if (!exc) {
							exc = CREATE_MPF_PTR_EXC_FWD(what.str());
							}
						else {
							exc->add_message_line(what.str());
							}

						exc->set_code(RZo_L0);

#ifdef CBL_MPF_TRACELEVEL_EVENT_SNDRCV_GUARD
						GLOBAL_STOUT_FWDRITE_STR(L"sndrcv-guard(response:r0tbl):" + exc->msg());
#endif

						onotification_t msg{};
						msg.set_destination_id(rdm.points.target_id());
						msg.set_source_id(rdm.points.source_id());
						msg.set_session(rdm.points.session());
						msg.set_destination_subscriber_id(sdrcvh.responseTarget);
						msg.set_level(sdrcvh.level);
						msg.set_ntf(std::move(exc));
						msg.setf_report();

						CBL_VERIFY(is_sendrecv_response(msg));

						try {
							CBL_MPF_KEYTRACE_SET(sdrcvh.spitTag, 513);

							s->__push2peer(std::move(rdm.points), std::move(msg));
							}
						catch (const dcf_exception_t & e0) {
							if (!s->notify_rt1(sdrcvh.chid,
								i_rt0_cmd_sndrcv_fault_t(*CHECK_PTR(s->_ctx), std::move(sdrcvh), e0.dcpy()))) {

								if (auto c = s->_ctx.lock()) {
									c->exc_hndl(e0);
									}
								}
							}
						catch (const std::exception & e1) {
							if (!s->notify_rt1(sdrcvh.chid,
								i_rt0_cmd_sndrcv_fault_t(*CHECK_PTR(s->_ctx), std::move(sdrcvh), CREATE_MPF_PTR_EXC_FWD(e1)))) {

								if (auto c = s->_ctx.lock()) {
									c->exc_hndl(e1);
									}
								}
							}
						catch (...) {
							if (!s->notify_rt1(sdrcvh.chid,
								i_rt0_cmd_sndrcv_fault_t(*CHECK_PTR(s->_ctx), std::move(sdrcvh), CREATE_MPF_PTR_EXC_FWD(nullptr)))) {

								if (auto c = s->_ctx.lock()) {
									c->exc_hndl(CREATE_PTR_EXC_FWD(nullptr));
									}
								}
							}
						}
					}

			CHECK_NO_EXCEPTION_END
			}, _ctx, "rt0_x_channels_table_t::processing", async_space_t::sys);

		return rt1_x_message_t::make(std::move(m_).rdm(), 
			outbound_message_unit_t::make("rt0_x_channels_table_t::processing-2", std::move(m_).capture_package(), std::move(sdrcvh), std::move(guarder)));
		}
#else
	return rt1_x_message_t::make(std::move(m_).rdm(), 
		outbound_message_unit_t::make("rt0_x_channels_table_t::processing-3", std::move(m_).capture_package()));
#endif

	CHECK_NO_EXCEPTION_END
	}

addon_push_event_result_t rt0_x_channels_table_t::push_to_rt0_xpeer_XBN(rt1_x_message_data_t && m0_ )noexcept {
	try {
		CBL_VERIFY_SNDRCV_KEY_SPIN_TAG(m0_);
		CBL_MPF_KEYTRACE_SET(m0_.sndrcv_handler().spitTag, 19);

		auto mx = processing(std::move(m0_));
		auto rdmSourceObject = mx.rdm().points;

		CBL_VERIFY(!is_null(rdmSourceObject));
		CBL_VERIFY(!rdmSourceObject.rt0().connection_key().address.empty());

#ifdef CBL_IS_KEEP_OUTCOME_MESSAGE_RESPONSE_GUARD_L0
		MPF_TEST_EMULATION_IN_CONTEXT_COND_TRCX(_ctx.lock(), 102,
			is_trait_at_emulation_context(mx.package()) && mx.is_sendrecv_cycle(),
			trait_at_keytrace_set(mx.package()));
#endif

		__push2peer(std::move(rdmSourceObject), std::move(mx));

		return addon_push_event_result_t::enum_type::success;
		}
	catch (const dcf_exception_t & e0) {
		if (auto c = _ctx.lock()) {
			c->exc_hndl(e0);
			}

		return addon_push_event_result_t::enum_type::exception;
		}
	catch (const std::exception & e1) {
		if (auto c = _ctx.lock()) {
			c->exc_hndl(e1);
			}

		return addon_push_event_result_t::enum_type::exception;
		}
	catch (...) {
		if (auto c = _ctx.lock()) {
			c->exc_hndl(CREATE_PTR_EXC_FWD(nullptr));
			}

		return addon_push_event_result_t::enum_type::exception;
		}
	}

addon_push_event_result_t rt0_x_channels_table_t::push_command_pack_to_rt0(rt0_x_command_pack_t && cmd )noexcept {

	if (auto rt0Reg = _rt0Registrator.lock()) {
		if (rt0Reg->push_command_pack_to_rt0(std::move(cmd))) {
			return addon_push_event_result_t::enum_type::success;
			}
		else {
			return addon_push_event_result_t::enum_type::exception;
			}
		}
	else {
		return addon_push_event_result_t::enum_type::link_unregistered;
		}
	}

rt0_x_channels_table_t::rt0_x_channels_table_t(std::weak_ptr<distributed_ctx_t> ctx,
	std::weak_ptr<crm::detail::rt0_cntbl_t> reg2rt0)
	: _ctx(ctx)
	, _lnkTbl(ctx, reg2rt0)
	, _rt0Registrator(reg2rt0) {
	}

std::chrono::milliseconds rt0_x_channels_table_t::timeout_xchannel()const noexcept {
	if (auto c = _ctx.lock()) {
		return c->policies().timeout_wait_io();
		}
	else {
		return {};
		}
	}

void rt0_x_channels_table_t::ctr() {}

std::shared_ptr<rt0_x_channels_table_t> rt0_x_channels_table_t::make(std::weak_ptr<distributed_ctx_t> ctx,
	std::weak_ptr<rt0_cntbl_t> rt) {

	auto obj = std::shared_ptr<self_t>(new self_t(std::move(ctx), std::move(rt)));
	obj->ctr();

	return obj;
	}

void rt0_x_channels_table_t::ntf_hndl(std::unique_ptr<dcf_exception_t> && ntf)noexcept {
	auto ctx_(_ctx.lock());
	if (ctx_) {
		ctx_->exc_hndl(std::move(ntf));
		}
	}

void rt0_x_channels_table_t::channel_command_fault( const channel_identity_t & chid )noexcept{
	if( auto ch = _lnkTbl.find_channel( chid ) ){
		ch->set_channel_state( channel_state::fault );
		}
	}
