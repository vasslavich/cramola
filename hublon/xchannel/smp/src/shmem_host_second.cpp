#include <memory>
#include <appkit/appkit.h>
#include "../shmem_host_second.h"
#include "../shmem_vector.h"

//
//namespace crm::detail{
//
//
//class xchannel_shmem_host_second_impl{
//private:
//	shmem_vector_open _qshmem;
//
//public:
//	xchannel_shmem_host_second_impl( std::weak_ptr<distributed_ctx_t> ctx,
//		const xchannel_shmem_hub_options & connectionOptionsMessages )
//		: _qshmem{ ctx, connectionOptionsMessages.mainNodeName }{}
//
//	void open(){
//		_qshmem.open();
//		}
//
//	void close()noexcept{
//		_qshmem.close();
//		}
//
//	template<typename TBinary>
//	bool push( TBinary && bin, xchannel_shmem_host_second::break_token && brTkn ){
//		return _qshmem.push( std::forward<TBinary>(bin), std::move(brTkn) );
//		}
//
//	size_t free_space()const noexcept{
//		return _qshmem.free_space();
//		}
//	};
//}


using namespace crm;
using namespace crm::detail;


xchannel_shmem_host_second::xchannel_shmem_host_second( std::weak_ptr<distributed_ctx_t> ctx,
	const xchannel_shmem_host_args & connectionOptions )
	: _pimpl( std::make_unique<shmem_vector_open>( ctx, connectionOptions.xchannelName.mainNodeName ) )
	, _chid{ CHECK_PTR( ctx )->make_channel_identity() }{}

xchannel_shmem_host_second::~xchannel_shmem_host_second(){
	close();
	}

void xchannel_shmem_host_second::open(){
	if( _pimpl )
		_pimpl->open();
	else
		THROW_EXC_FWD(nullptr);
	}

void xchannel_shmem_host_second::close()noexcept{
	if( _pimpl )
		_pimpl->close();
	}

bool xchannel_shmem_host_second::closed()const noexcept {
	if (_pimpl)
		return _pimpl->closed();
	else {
		return true;
		}
	}

size_t xchannel_shmem_host_second::free_space()const noexcept{
	if( _pimpl ){
		return _pimpl->free_space();
		}
	else{
		return 0;
		}
	}
//
//bool xchannel_shmem_host_second::push__( std::vector<uint8_t> && object, break_token && brTkn){
//
//	if( _pimpl ){
//		return _pimpl->push( std::move( object ), std::move(brTkn) );
//		}
//	else{
//		return false;
//		}
//	}



