#include <appkit/appkit.h>
#include "../shmem_vector.h"


using namespace crm;
using namespace crm::detail;


static_assert(std::atomic_llong::is_always_lock_free, __FILE_LINE__);




shmem_vector_type_traits::~shmem_vector_type_traits(){}

shmem_vector_range::shmem_vector_range()noexcept{}

shmem_vector_range::shmem_vector_range(std::weak_ptr<distributed_ctx_t> wctx, std::string_view name)
	: _ctx(wctx)
	, _shmemName(name) {}

void shmem_vector_range::open(){
	_smpVector = std::make_unique<sync_vector_type>(
		byte_ring( name(), shmem_ringbuf_handler( bvector(), ready(), tail(), head() ) ),
		*condvar_inc(),
		*condvar_dec(),
		*lock() );
	}

shmem_vector_range::shmem_channel_state_state
shmem_vector_range::adjust( channel_state st )noexcept{

	return (shmem_channel_state_state)st;
	}

channel_state shmem_vector_range::state_flag( shmem_channel_state_state * st )noexcept{
	return (channel_state)(*st);
	}

std::shared_ptr<distributed_ctx_t> shmem_vector_range::ctx()noexcept {
	return _ctx.lock();
	}

const std::string & shmem_vector_range::name()const noexcept {
	return _shmemName;
	}

void shmem_vector_range::close()noexcept {
	return _closedf.store( true, std::memory_order::memory_order_release );
	}

bool shmem_vector_range::closed()const noexcept{
	return _closedf.load( std::memory_order::memory_order_acquire );
	}

shmem_vector_range::sync_vector_type* shmem_vector_range::smp_vector()const noexcept {
	return _smpVector.get();
	}

size_t shmem_vector_range::free_space()const noexcept{
	return smp_vector()->free_space_count();
	};

shmem_vector_range::~shmem_vector_range(){
	close();
	}



shmem_vector_ctr::shmem_vector_ctr()noexcept{}

shmem_vector_ctr::shmem_vector_ctr(std::weak_ptr<distributed_ctx_t> ctx_, std::string_view name_)
	: base_type(ctx_, name_){}

shmem_vector_ctr::~shmem_vector_ctr() {
	close();
	}

shmem_vector_ctr::segment_manager_t* shmem_vector_ctr::segment_manager()const noexcept {
	return _shmem.get_segment_manager();
	}

const shmem_vector_ctr::byte_vector* shmem_vector_ctr::bvector()const noexcept {
	return _bvector;
	}

shmem_vector_ctr::byte_vector* shmem_vector_ctr::bvector()noexcept {
	return _bvector;
	}

shmem_vector_ctr::shared_mytex* shmem_vector_ctr::lock()noexcept {
	return _lck;
	}

shmem_vector_ctr::shared_condvar* shmem_vector_ctr::condvar_inc()noexcept {
	return _cndvInc;
	}

shmem_vector_ctr::shared_condvar* shmem_vector_ctr::condvar_dec()noexcept {
	return _cndvDec;
	}

std::atomic_llong *shmem_vector_ctr::ready()noexcept{
	return _nReady;
	}

std::atomic_llong *shmem_vector_ctr::tail()noexcept{
	return _nTail;
	}

std::atomic_llong *shmem_vector_ctr::head()noexcept{
	return _nHead;
	}

void shmem_vector_ctr::set( channel_state st )noexcept{
	(*_state) = adjust( st );
	}

channel_state shmem_vector_ctr::state()const noexcept{
	return state_flag( _state );
	}

void shmem_vector_ctr::construct_do(size_type vSize ){
	_lck = _shmem.construct<shared_mytex>( mytex_object_name )();
	_cndvInc = _shmem.construct< shared_condvar>( condvar_inc_object_name )();
	_cndvDec = _shmem.construct< shared_condvar>( condvar_dec_object_name )();
	_nReady = _shmem.construct<std::atomic_llong>( ready_counter_name )(0);
	_nHead = _shmem.construct<std::atomic_llong>( head_counter_name )(0);
	_nTail = _shmem.construct<std::atomic_llong>( tail_counter_name )(0);

	void_allocator alloc_inst( segment_manager() );
	_bvector = _shmem.construct<byte_vector>( vector_object_name )(vSize, alloc_inst);

	std::cout << "make byte vector, size=" << _bvector->size() << std::endl;

	_state = _shmem.construct< shmem_channel_state_state>( stateflag_name )(0);

	base_type::open();
	}

void shmem_vector_ctr::construct( shmem_vector_ctr *this_, size_type vSize ){
	this_->construct_do( vSize );
	}

void shmem_vector_ctr::open(size_t bsize_){
	bool openedf{false};
	try{
		detail::post_scope_action_t faultDo{[&]{
			if(!openedf){
				shared_memory_object::remove(_shmemName.c_str());
				}
			}};

		/* remove first( by boost programming guide and exapmles */
		shared_memory_object::remove(_shmemName.c_str());

		_shmem = managed_shared_memory(
			create_only,
			_shmemName.c_str(),
			bsize_ + internals_size);

		auto atomic_construct = std::bind(&construct, this, bsize_);
		_shmem.atomic_func(atomic_construct);

		openedf = true;

	} catch(const boost::interprocess::interprocess_exception& shmemErr){
		THROW_EXC_FWD(std::string("fault open host channel:") + shmemErr.what());
	} catch(const std::exception& e1){
		THROW_EXC_FWD(std::string("fault open host channel:") + e1.what());
	} catch(...){
		THROW_EXC_FWD("fault open channel with host");
	}
}

void shmem_vector_ctr::close()noexcept{
	bool clf = false;
	if( _clf.compare_exchange_strong( clf, true ) ){

		CHECK_NO_EXCEPTION_BEGIN

		_shmem.destroy< byte_vector>( vector_object_name );
		_shmem.destroy<shared_condvar>( condvar_inc_object_name );
		_shmem.destroy<shared_condvar>( condvar_dec_object_name );
		_shmem.destroy<shared_mytex>( mytex_object_name );
		_shmem.destroy<std::atomic_llong>( ready_counter_name );
		_shmem.destroy<std::atomic_llong>( head_counter_name );
		_shmem.destroy<std::atomic_llong>( tail_counter_name );
		_shmem.destroy<shmem_channel_state_state>( stateflag_name );

		shared_memory_object::remove( _shmemName.c_str() );

		base_type::close();

		CHECK_NO_EXCEPTION_END
		}
	}







shmem_vector_open::shmem_vector_open()noexcept{}

shmem_vector_open::shmem_vector_open(std::weak_ptr<distributed_ctx_t> ctx_, std::string_view name_)
	: base_type(ctx_, name_){}

shmem_vector_open::~shmem_vector_open() {
	close();
	}

const shmem_vector_open::byte_vector* shmem_vector_open::bvector()const noexcept  {
	return _bvector;
	}

shmem_vector_open::byte_vector* shmem_vector_open::bvector()noexcept  {
	return _bvector;
	}

shmem_vector_open::shared_mytex * shmem_vector_open::lock()noexcept {
	return _lck;
	}

shmem_vector_open::shared_condvar * shmem_vector_open::condvar_inc()noexcept {
	return _cndvInc;
	}

shmem_vector_open::shared_condvar* shmem_vector_open::condvar_dec()noexcept {
	return _cndvDec;
	}

shmem_vector_open::segment_manager_t* shmem_vector_open::segment_manager()const noexcept {
	return _shmem.get_segment_manager();
	}

std::atomic_llong *shmem_vector_open::ready()noexcept{
	return _nReady;
	}

std::atomic_llong *shmem_vector_open::tail()noexcept{
	return _nTail;
	}

std::atomic_llong *shmem_vector_open::head()noexcept{
	return _nHead;
	}

void shmem_vector_open::set( channel_state st )noexcept{
	(*_state) = adjust( st );
	}

channel_state shmem_vector_open::state()const noexcept{
	return state_flag( _state );
	}

void shmem_vector_open::construct_do(){
	_bvector = _shmem.find<byte_vector>( vector_object_name ).first;
	if( !_bvector ){
		THROW_EXC_FWD(nullptr);
		}

	std::cout << "bytes vector opened, size=" << _bvector->size() << std::endl;

	_cndvInc = _shmem.find< shared_condvar>( condvar_inc_object_name ).first;
	if( !_cndvInc ){
		THROW_EXC_FWD(nullptr);
		}

	_cndvDec = _shmem.find< shared_condvar>( condvar_dec_object_name ).first;
	if( !_cndvDec ){
		THROW_EXC_FWD(nullptr);
		}

	_lck = _shmem.find<shared_mytex>( mytex_object_name ).first;
	if( !_lck ){
		THROW_EXC_FWD(nullptr);
		}

	_nReady = _shmem.find<std::atomic_llong>( ready_counter_name ).first;
	if( !_nReady ){
		THROW_EXC_FWD(nullptr);
		}
	std::cout << name() << ":nready=" << (*_nReady) << std::endl;

	_nHead = _shmem.find<std::atomic_llong>( head_counter_name ).first;
	if( !_nHead ){
		THROW_EXC_FWD(nullptr);
		}

	_nTail = _shmem.find<std::atomic_llong>( tail_counter_name ).first;
	if( !_nTail ){
		THROW_EXC_FWD(nullptr);
		}

	_state = _shmem.find<shmem_channel_state_state>( stateflag_name ).first;
	if( !_state ){
		THROW_EXC_FWD(nullptr);
		}

	base_type::open();
	}

void shmem_vector_open::construct( shmem_vector_open *this_){
	this_->construct_do();
	}

void shmem_vector_open::open(){
	try{
		_shmem = managed_shared_memory(
			open_only,
			_shmemName.c_str());

		auto atomic_construct = std::bind(&construct, this);
		_shmem.atomic_func(atomic_construct);

	} catch(const boost::interprocess::interprocess_exception& shmemErr){
		THROW_EXC_FWD(std::string("fault open channel with host:") + shmemErr.what());
	} catch(const std::exception& e1){
		THROW_EXC_FWD(std::string("fault open host channel:") + e1.what());
	} catch(...){
		THROW_EXC_FWD("fault open channel with host");
	}
}


void shmem_vector_open::close()noexcept {
	bool clf = false;
	if (_clf.compare_exchange_strong(clf, true)) {
		
		base_type::close();
		}
	}

static_assert(is_iterator_reader_v<push_reader<std::vector<uint8_t>, 
	decltype(make_break_cancel(std::declval<std::function<bool()>>()))>>, __FILE_LINE__);

static_assert(is_iterator_reader_v<push_reader<std::vector<uint8_t>, 
	decltype(make_break_timed(std::declval<std::chrono::seconds>(), std::declval<std::chrono::seconds>()))>>, __FILE_LINE__);

static_assert(is_iterator_reader_v<push_reader<std::vector<uint8_t>, 
	decltype(make_break_cancel_timed(std::declval<std::function<bool()>>(), std::declval<std::chrono::seconds>(), std::declval<std::chrono::seconds>()))>>, __FILE_LINE__);




static_assert(is_output_buffer_v<pop_buffer<std::vector<uint8_t>,
	decltype(std::declval<break_conditional>())>>, __FILE_LINE__);

static_assert(is_output_buffer_v<pop_buffer<std::vector<uint8_t>, 
	decltype(make_break_cancel(std::declval<std::function<bool()>>()))>>, __FILE_LINE__);

static_assert(is_output_buffer_v<pop_buffer<std::vector<uint8_t>,
	decltype(make_break_timed(std::declval<std::chrono::seconds>(), std::declval<std::chrono::seconds>()))>> , __FILE_LINE__);

static_assert(is_output_buffer_v<pop_buffer<std::vector<uint8_t>,
	decltype(make_break_cancel_timed(std::declval<std::function<bool()>>(), std::declval<std::chrono::seconds>(), std::declval<std::chrono::seconds>()))>> , __FILE_LINE__);


