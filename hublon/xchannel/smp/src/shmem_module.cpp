#include <memory>
#include <appkit/appkit.h>
#include "../shmem_vector.h"
#include "../shmem_module.h"



namespace crm::detail {


class xchannel_shmem_module_impl {
private:
	using xchannel_shmem_module_typed_channel_open_rt0_x_message_t = shmem_vector_ctr;
	using xchannel_shmem_module_typed_channel_open_rt0_x_command_rdm_t = shmem_vector_ctr;
	using xchannel_shmem_module_typed_channel_ctr_rt1_x_message_data_t = shmem_vector_ctr;
	using xchannel_shmem_module_typed_channel_ctr_rt1_x_command_pack_t = shmem_vector_ctr;

	xchannel_shmem_module_typed_channel_open_rt0_x_message_t _messageRT0_x;
	xchannel_shmem_module_typed_channel_open_rt0_x_command_rdm_t _commandRT0_x;
	xchannel_shmem_module_typed_channel_ctr_rt1_x_message_data_t _messageRT1_x;
	xchannel_shmem_module_typed_channel_ctr_rt1_x_command_pack_t _commandRT1_x;

	std::atomic<bool> _clf{false};

public:
	bool closed()const noexcept {
		return _clf.load(std::memory_order::memory_order_acquire);
		}

	xchannel_shmem_module_impl(std::weak_ptr<distributed_ctx_t> ctx,
		const xchannel_shmem_hub_options & connectionOptions_RT0_x_m,
		const xchannel_shmem_hub_options & connectionOptions_RT0_x_c,
		const xchannel_shmem_hub_options & connectionOptions_x_RT1_m,
		const xchannel_shmem_hub_options & connectionOptions_x_RT1_c )
		: _messageRT0_x{ctx, connectionOptions_RT0_x_m.mainNodeName}
		, _commandRT0_x{ctx, connectionOptions_RT0_x_c.mainNodeName}
		, _messageRT1_x{ctx, connectionOptions_x_RT1_m.mainNodeName}
		, _commandRT1_x{ctx, connectionOptions_x_RT1_c.mainNodeName}{}

	void open(size_t mbSize, size_t cbSize) {
		_messageRT0_x.open(mbSize);
		_commandRT0_x.open(cbSize);
		_messageRT1_x.open(mbSize);
		_commandRT1_x.open(cbSize);
		}

	void close()noexcept {
		_clf.store(true, std::memory_order::memory_order_release);
		_messageRT0_x.close();
		_commandRT0_x.close();
		_messageRT1_x.close();
		_commandRT1_x.close();
		}

	using break_token = break_cancel_timed<std::chrono::milliseconds::rep, std::chrono::milliseconds::period>;
	break_token mk_cancel_token(const std::chrono::milliseconds & timeout )const {
		return break_token([this]{return closed(); }, timeout, timeout);
		}

	bool pop(command_rdm_t & cmd, const std::chrono::milliseconds & wait) {
		return _commandRT0_x.pop( cmd, mk_cancel_token(wait));
		}

	size_t  pop(std::list<command_rdm_t> & vl, const std::chrono::milliseconds & wait) {
		return _commandRT0_x.pop( vl, mk_cancel_token(wait));
		}

	bool pop(rt0_x_message_t & m, const std::chrono::milliseconds & wait) {
		std::vector<char> b;
		auto rw = srlz::rstream_constructor_t::make(b.begin(), b.end());
		srlz::deserialize(rw, m);

		return _messageRT0_x.pop( m, mk_cancel_token(wait));
		}

	size_t pop(std::list<rt0_x_message_t> & ml, const std::chrono::milliseconds & wait) {
		return _messageRT0_x.pop( ml, mk_cancel_token(wait));
		}

	bool push( const rt0_x_command_pack_t & cmd, const std::chrono::milliseconds & wait) {
		return _commandRT1_x.push( cmd, mk_cancel_token(wait));
		}

	bool push(const rt1_x_message_data_t & m, const std::chrono::milliseconds & wait) {
		return _messageRT1_x.push( m, mk_cancel_token(wait));
		}
	};
}


using namespace crm;
using namespace crm::detail;


bool xchannel_shmem_module::pop_from_rt0( rt0_x_message_t & pck,
	const std::chrono::milliseconds & wait ){

	return _pvector->pop(pck, wait);
	}

size_t xchannel_shmem_module::pop_from_rt0( std::list<rt0_x_message_t> & lst,
	const std::chrono::milliseconds & wait ){

	return _pvector->pop(lst, wait);
	}

addon_push_event_result_t xchannel_shmem_module::push_to_rt0_message( rt1_x_message_data_t && object,
	const std::chrono::milliseconds & wait )noexcept {

	if (_pvector->push(object, wait)) {
		return addon_push_event_result_t::enum_type::success;
		}
	else {
		return addon_push_event_result_t::enum_type::exception;
		}
	}

addon_push_event_result_t xchannel_shmem_module::push_command_pack_to_rt0(rt0_x_command_pack_t&& cmd )noexcept{

	const auto wt = cmd.wait_timeout();
	if (_pvector->push(cmd, wt)) {
		return addon_push_event_result_t::enum_type::success;
		}
	else {
		return addon_push_event_result_t::enum_type::exception;
		}
	}

const channel_info_t& xchannel_shmem_module::get_channel_info()const{
	return _chid;
	}

bool xchannel_shmem_module::pop_from_rt0( command_rdm_t & ev,
	const std::chrono::milliseconds & wait ){

	return _pvector->pop(ev, wait);
	}

xchannel_shmem_module::xchannel_shmem_module(std::weak_ptr<distributed_ctx_t> ctx, 
	const channel_shared_memory_config & args )
	: _pvector( std::make_unique< xchannel_shmem_module_impl>( ctx,
		xchannel_shmem_hub_options{args.hup_options().xchannelRT0_x_m},
		xchannel_shmem_hub_options{ args.hup_options().xchannelRT0_x_c},
		xchannel_shmem_hub_options{ args.hup_options().xchannel_x_RT1_m},
		xchannel_shmem_hub_options{ args.hup_options().xchannel_x_RT1_c}))
	, _chid{ args.info()}{}

xchannel_shmem_module::~xchannel_shmem_module(){
	close();
	}

void xchannel_shmem_module::open(size_t mSize, size_t cSize){
	if (_pvector)
		_pvector->open(mSize, cSize);
	else
		THROW_EXC_FWD(nullptr);
	}

void xchannel_shmem_module::close()noexcept{
	if (_pvector)
		_pvector->close();
	}

