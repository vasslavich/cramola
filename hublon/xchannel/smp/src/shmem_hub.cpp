#include <vector>
#include <appkit/appkit.h>
#include "../shmem_vector.h"
#include "../shmem_hub.h"


namespace crm::detail {


class xchannel_shmem_hub_impl {
private:
	using xchannel_shmem_hub_typed_channel_make_rt0_x_message_t = shmem_vector_open;
	using xchannel_shmem_hub_typed_channel_make_rt0_x_command_rdm_t = shmem_vector_open;
	using xchannel_shmem_hub_typed_channel_open_rt1_x_message_data_t = shmem_vector_open;
	using xchannel_shmem_hub_typed_channel_open_rt1_x_command_pack_t = shmem_vector_open;

	xchannel_shmem_hub_typed_channel_make_rt0_x_message_t _messageRT0_x;
	xchannel_shmem_hub_typed_channel_make_rt0_x_command_rdm_t _commandRT0_x;
	xchannel_shmem_hub_typed_channel_open_rt1_x_message_data_t _m_RT1_x;
	xchannel_shmem_hub_typed_channel_open_rt1_x_command_pack_t _c_RT1_x;
	std::atomic<bool> _clf{false};

public:
	bool closed()const noexcept {
		return _clf.load(std::memory_order::memory_order_acquire);
		}

	~xchannel_shmem_hub_impl(){
		close();
		}

	xchannel_shmem_hub_impl(std::weak_ptr<distributed_ctx_t> ctx,
		const xchannel_shmem_hub_options & co_RT0_x_m,
		const xchannel_shmem_hub_options & co_RT0_x_c,
		const xchannel_shmem_hub_options & co_x_RT1_m,
		const xchannel_shmem_hub_options & co_x_RT1_c )
		: _messageRT0_x{ctx, co_RT0_x_m.mainNodeName}
		, _commandRT0_x{ctx, co_RT0_x_c.mainNodeName}
		, _m_RT1_x{ ctx, co_x_RT1_m.mainNodeName }
		, _c_RT1_x{ ctx, co_x_RT1_c.mainNodeName }{}

	void open() {
		_messageRT0_x.open();
		_commandRT0_x.open();
		_m_RT1_x.open();
		_c_RT1_x.open();
		}

	void close() noexcept{
		_clf.store(true, std::memory_order::memory_order_release);
		_messageRT0_x.close();
		_commandRT0_x.close();
		_m_RT1_x.close();
		_c_RT1_x.close();
		}

	void set_channel_state(  channel_state st )noexcept{
		_messageRT0_x.set( st );
		_commandRT0_x.set( st );
		}

	using break_token = break_cancel_timed<std::chrono::milliseconds::rep, std::chrono::milliseconds::period>;
	break_token mk_cancel_token(const std::chrono::milliseconds & timeout)const{
		return break_token([this]{return closed(); }, timeout, timeout);
		}

	bool push(const command_rdm_t & cmd, const std::chrono::milliseconds & wait) {
		return _commandRT0_x.push(cmd, mk_cancel_token(wait));
		}

	bool push(const rt0_x_message_t & m, const std::chrono::milliseconds & wait) {
		return _messageRT0_x.push(m, mk_cancel_token(wait));
		}

	bool pop(rt0_x_command_pack_t & cmd, const std::chrono::milliseconds & wait) {
		return _c_RT1_x.pop(cmd, mk_cancel_token(wait));
		}

	size_t pop(std::list<rt0_x_command_pack_t> & l, const std::chrono::milliseconds & wait) {
		return _c_RT1_x.pop(l, mk_cancel_token(wait));
		}

	bool pop(rt1_x_message_data_t & m, const std::chrono::milliseconds & wait) {
		return _m_RT1_x.pop(m, mk_cancel_token(wait));
		}

	size_t pop( std::list<rt1_x_message_data_t> & m, const std::chrono::milliseconds & wait) {
		return _m_RT1_x.pop(m, mk_cancel_token(wait));
		}
	};
}


using namespace crm;
using namespace crm::detail;


void xchannel_shmem_hub_impl_deleter::operator()(
	xchannel_shmem_hub_impl * p)const noexcept{

	delete p;
	}

#ifdef CBL_OBJECTS_COUNTER_CHECKER
void xchannel_shmem_hub::__size_trace()const noexcept{
	CHECK_NO_EXCEPTION( __sizeTraceer.CounterCheck( _evTbl.size() ) );
	}
#endif

xchannel_shmem_hub::xchannel_shmem_hub(std::weak_ptr<distributed_ctx_t> ctx,
	const channel_shared_memory_config & cfg )
	: _pMemory( new xchannel_shmem_hub_impl( ctx,
		xchannel_shmem_hub_options{ cfg.hup_options().xchannelRT0_x_m },
		xchannel_shmem_hub_options{ cfg.hup_options().xchannelRT0_x_c },
		xchannel_shmem_hub_options{ cfg.hup_options().xchannel_x_RT1_m },
		xchannel_shmem_hub_options{ cfg.hup_options().xchannel_x_RT1_c }))
	, _chid{cfg.info()}
	, _ctx(ctx){}

std::shared_ptr< xchannel_shmem_hub> xchannel_shmem_hub::make(
	std::weak_ptr<distributed_ctx_t> ctx,
	const channel_shared_memory_config & cfg ){
	
	return std::make_shared< xchannel_shmem_hub>( ctx, cfg );
	}

bool xchannel_shmem_hub::closed()const noexcept{
	return _closef.load( std::memory_order::memory_order_acquire );
	}

void xchannel_shmem_hub::open(){
	if( _pMemory ){
		_pMemory->open();
		}
	else{
		THROW_EXC_FWD(nullptr);
		}
	}

void xchannel_shmem_hub::close()noexcept{
	bool closedf = false;
	if( _closef.compare_exchange_strong( closedf, true ) ){

	#ifdef CBL_OBJECTS_COUNTER_CHECKER
		__size_trace();
	#endif

		if( _pMemory ){
			 _pMemory->close();
			}
		}
	}

std::chrono::milliseconds xchannel_shmem_hub::timeout_xchannel()const noexcept{
	if( auto c = _ctx.lock() ){
		return c->policies().timeout_wait_io();
		}
	else{
		return {};
		}
	}

void xchannel_shmem_hub::ntf_hndl( std::unique_ptr<dcf_exception_t> && e )const noexcept{
	if( auto c = _ctx.lock() ){
		c->exc_hndl( std::move( e ) );
		}
	}

bool xchannel_shmem_hub::push_to_rt1(rt0_x_message_t && object, const std::chrono::milliseconds & wait)noexcept {
	return _pMemory->push(object, wait);
	}

bool xchannel_shmem_hub::push_to_rt1(command_rdm_t && ev )noexcept {
	const auto wt = ev.cmd.wait_timeout();
	return _pMemory->push(std::move(ev), wt);
	}

bool xchannel_shmem_hub::pop(rt0_x_command_pack_t & cmd, const std::chrono::milliseconds & wait) {
	return _pMemory->pop(cmd, wait);
	}

size_t xchannel_shmem_hub::pop(std::list<rt0_x_command_pack_t> & l, const std::chrono::milliseconds & wait) {
	return _pMemory->pop(l, wait);
	}

bool xchannel_shmem_hub::pop(rt1_x_message_data_t & m, const std::chrono::milliseconds & wait) {
	return _pMemory->pop(m, wait);
	}

size_t xchannel_shmem_hub::pop(std::list<rt1_x_message_data_t> & m, const std::chrono::milliseconds & wait) {
	return _pMemory->pop(m, wait);
	}

const channel_info_t& xchannel_shmem_hub::get_channel_info()const {
	return _chid;
	}

std::shared_ptr<distributed_ctx_t> xchannel_shmem_hub::ctx()const noexcept {
	return _ctx.lock();
	}

void xchannel_shmem_hub::_subscribe( const rtl_roadmap_event_t & id,
	const std::list<rtx_command_type_t> & evlst ){

	_unsubscribe( id );
	for( auto iev : evlst ){
		_evTbl.insert( { iev, id } );
		}

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	__size_trace();
#endif
	}

void xchannel_shmem_hub::subscribe( const rtl_roadmap_event_t & id,
	const std::list<rtx_command_type_t> & evlst ){

	lck_scp_t lck( _tblLck );
	_subscribe( id, evlst );
	}

void xchannel_shmem_hub::_unsubscribe( const rtl_roadmap_event_t & id ) noexcept{
	bool has = true;
	while( _evTbl.size() && has ){

		has = false;
		for( auto it = _evTbl.begin(); it != _evTbl.end(); ++it ){
			if( id == it->second ){
				_evTbl.erase( it );
				has = true;
				break;
				}
			}
		}

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	__size_trace();
#endif
	}

void xchannel_shmem_hub::unsubscribe( const rtl_roadmap_event_t & id ) noexcept{
	lck_scp_t lck( _tblLck );
	CHECK_NO_EXCEPTION( _unsubscribe( id ) );
	}

std::list<rtl_roadmap_event_t> xchannel_shmem_hub::find_event_targets(
	rtx_command_type_t ev )const noexcept{

	CHECK_NO_EXCEPTION_BEGIN

	lck_scp_t lck( _tblLck );
	std::list<rtl_roadmap_event_t> lst;

	for( auto it = _evTbl.begin(); it != _evTbl.end(); ++it )
		if( (*it).first == ev )
			lst.push_back( it->second );

	return std::move( lst );

	CHECK_NO_EXCEPTION_END
	}

//void xchannel_shmem_hub::notify(rtx_command_type_t ev,
//	rt0_x_command_pack_t&& cmd)noexcept {
//
//	if( !closed() ){
//		CHECK_NO_EXCEPTION_BEGIN
//
//		auto evTargets( find_event_targets( ev ) );
//		all_send( evTargets, std::move( cmd ) );
//
//		CHECK_NO_EXCEPTION_END
//		}
//	}

//bool xchannel_shmem_hub::push_command(rt0_x_command_pack_t&& cmd )noexcept {
//	command_rdm_t rdm;
//	rdm.target = cmd.command_target_receiver_id();
//	rdm.cmd = std::move(cmd);
//
//	return push_to_rt1(std::move(rdm));
//	}

void xchannel_shmem_hub::set_channel_state( channel_state st )noexcept{
	if( _pMemory ){
		_pMemory->set_channel_state( st );
		}
	}

