#include <appkit/appkit.h>
#include "../shmem_host_first.h"
#include "../shmem_vector.h"


//namespace crm::detail{
//
//class xchannel_shmem_host_impl{
//private:
//	shmem_vector_ctr _qshmem;
//
//public:
//	~xchannel_shmem_host_impl(){
//		close();
//		}
//
//	xchannel_shmem_host_impl( std::weak_ptr<distributed_ctx_t> ctx,
//		const xchannel_shmem_hub_options & connectionOptions )
//		: _qshmem{ ctx, connectionOptions.mainNodeName }{}
//
//	void open( size_t bsize ){
//		_qshmem.open( bsize );
//		}
//
//	void close()noexcept{
//		_qshmem.close();
//		}
//
//	size_t free_space()const noexcept{
//		return _qshmem.free_space();
//		}
//
//	bool pop( std::vector<uint8_t> & bin, break_conditional & cntk ){
//		return _qshmem.pop(bin, cntk );
//		}
//	};
//}


using namespace crm;
using namespace crm::detail;



//void xchannel_shmem_host_impl_deleter::operator()(
//	xchannel_shmem_host_impl * p )const noexcept{
//
//	delete p;
//	}


xchannel_shmem_host_first::xchannel_shmem_host_first( std::weak_ptr<distributed_ctx_t> ctx,
	const xchannel_shmem_host_args & options )
	: _pMemory( new shmem_vector_ctr( ctx, options.xchannelName.mainNodeName ) )
	, _chid{ CHECK_PTR( ctx )->make_channel_identity() } {}

void xchannel_shmem_host_first::open( size_t bsize ){
	if( _pMemory ){
		_pMemory->open( bsize );
		}
	else{
		THROW_EXC_FWD(nullptr);
		}
	}

void xchannel_shmem_host_first::close()noexcept{
	if( _pMemory ){
		CHECK_NO_EXCEPTION( _pMemory->close() );
		}
	}

size_t xchannel_shmem_host_first::free_space()const noexcept{
	if( _pMemory ){
		return _pMemory->free_space();
		}
	else{
		return 0;
		}
	}

//bool xchannel_shmem_host_first::pop__( std::vector<uint8_t> & bin,
//	break_conditional & tkn ){
//
//	if( _pMemory ){
//		return _pMemory->pop( bin, tkn );
//		}
//	else {
//		return false;
//		}
//	}
