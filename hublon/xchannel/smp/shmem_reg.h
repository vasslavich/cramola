#pragma once

#include <appkit/export_internal_terms.h>


namespace crm::detail{


struct xchannel_shmem_hub_options{
	std::string mainNodeName;
	};

struct xchannel_shmem_hub_args{
	xchannel_shmem_hub_options xchannelRT0_x_c;
	xchannel_shmem_hub_options xchannelRT0_x_m;
	xchannel_shmem_hub_options xchannel_x_RT1_c;
	xchannel_shmem_hub_options xchannel_x_RT1_m;
	};

struct xchannel_shmem_host_args{
	xchannel_shmem_hub_options xchannelName;
	};

struct xchannel_connect_end{
	bool result{ false };
	std::unique_ptr<dcf_exception_t> error;
	};

struct i_xchannel_shmem_host_first {
	virtual ~i_xchannel_shmem_host_first() {}
	};

struct i_xchannel_shmem_host_second {
	virtual ~i_xchannel_shmem_host_second(){}
	};
}
