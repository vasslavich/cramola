#pragma once


#include "./shmem_reg.h"
#include "../../terms/internal_terms.h"


namespace crm::detail{


class xchannel_shmem_module_impl;
class xchannel_shmem_module final : public io_x_rt1_channel_t{
private:
	std::unique_ptr<xchannel_shmem_module_impl> _pvector;
	channel_info_t _chid;

public:
	bool pop_from_rt0( rt0_x_message_t & pck,
		const std::chrono::milliseconds & wait )final;
	size_t pop_from_rt0( std::list<rt0_x_message_t> & lst,
		const std::chrono::milliseconds & wait )final;

	[[nodiscard]]
	addon_push_event_result_t push_to_rt0_message( rt1_x_message_data_t && object, const std::chrono::milliseconds & wait )noexcept final;

	[[nodiscard]]
	addon_push_event_result_t push_command_pack_to_rt0(rt0_x_command_pack_t&& cmd)noexcept final;

	const channel_info_t& get_channel_info()const final;

	bool pop_from_rt0( command_rdm_t & ev,
		const std::chrono::milliseconds & wait ) final;
public:
	xchannel_shmem_module( std::weak_ptr<distributed_ctx_t> ctx,
		const channel_shared_memory_config & args );

	~xchannel_shmem_module();

	void open( size_t bsizeMsg, size_t bsizeCmd);
	void close()noexcept;
	};
}
