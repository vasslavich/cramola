#pragma once


#include "./shmem_reg.h"
#include <appkit/tables/bufdef.h>


namespace crm::detail{


class shmem_vector_open;
class xchannel_shmem_host_second final : public i_xchannel_shmem_host_second {
public:
	using break_token = break_cancel_timed<std::chrono::milliseconds::rep, std::chrono::milliseconds::period>;

private:
	std::unique_ptr<shmem_vector_open> _pimpl;
	channel_info_t _chid;

	size_t free_space()const noexcept;

	template<typename TBinary, typename TWaitCond>
	bool push__(TBinary && bin, TWaitCond && brTkn){
		if (_pimpl){
			return _pimpl->push(std::forward<TBinary>(bin), std::forward<TWaitCond>(brTkn));
			}
		else{
			return false;
			}
		}

public:
	xchannel_shmem_host_second( std::weak_ptr<distributed_ctx_t> ctx,
		const xchannel_shmem_host_args & connectionOptions );

	~xchannel_shmem_host_second();

	void open();
	void close()noexcept;
	bool closed()const noexcept;

	template<typename TSerialized,
		typename TWaitCond,
		typename std::enable_if_t<(
			crm::is_break_conditional_v<TWaitCond> &&
			crm::srlz::is_serializable_v<TSerialized> && 
			!crm::srlz::detail::is_container_of_bytes_v<TSerialized>), int> = 0>
	bool push(TSerialized && o, TWaitCond && tk ) {
		auto ws = crm::srlz::wstream_constructor_t::make();
		crm::srlz::serialize(ws, std::forward<TSerialized>(o));

		return push(ws.release(), std::forward<TWaitCond>(tk) );
		}

	template<typename TBinary, 
		typename TWaitCond,
		typename std::enable_if_t<crm::srlz::detail::is_container_of_bytes_v<TBinary> && crm::is_break_conditional_v<TWaitCond>, int> = 0>
	bool push(TBinary && object, TWaitCond && tk ) {
		return push__(std::forward<TBinary>(object), std::forward<TWaitCond>(tk));
		}
	};
}

