#pragma once


#include <memory>
#include "./shmem_reg.h"
#include <appkit/appkit.h>


namespace crm::detail{


class shmem_vector_ctr;
class xchannel_shmem_host_first final : public i_xchannel_shmem_host_first {
private:
	std::unique_ptr<shmem_vector_ctr> _pMemory;
	channel_info_t _chid;

	size_t free_space()const noexcept;

	template<typename TBinary, typename TBreakCond>
	bool pop__(TBinary && bin, TBreakCond && tkn){
		if (_pMemory){
			return _pMemory->pop(std::forward<TBinary>(bin), std::forward<TBreakCond>(tkn));
			}
		else{
			return false;
			}
		}

public:
	xchannel_shmem_host_first( std::weak_ptr<distributed_ctx_t> ctx,
		const xchannel_shmem_host_args & options );

	template<typename TSerialized,
		typename TBreakTkn,
		typename std::enable_if_t<(
			crm::is_break_conditional_v<TBreakTkn> &&
			srlz::is_serializable_v<TSerialized> &&
			!crm::srlz::detail::is_container_of_bytes_v<TSerialized>), int> = 0>
	bool pop(TSerialized & v, TBreakTkn && tk) {
		std::vector<uint8_t> buffer;
		if (pop__(buffer, std::forward<TBreakTkn>(tk))) {
			auto rs = srlz::rstream_constructor_t::make(buffer.cbegin(), buffer.cend());
			srlz::deserialize(rs, v);

			return true;
			}
		else {
			return false;
			}
		}

	template<typename TBreakTkn,
		typename std::enable_if_t<crm::is_break_conditional_v<TBreakTkn>, int> = 0>
	bool pop(std::vector<uint8_t> & bin, TBreakTkn && tk) {
		return pop__(bin, std::forward<TBreakTkn>(tk));
		}

	void open( size_t bsize );
	void close()noexcept;
	};
}

