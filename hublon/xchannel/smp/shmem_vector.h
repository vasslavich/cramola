#pragma once


#include <appkit/appkit.h>
#include <boost/thread/thread_time.hpp>
#include <boost/container/scoped_allocator.hpp>
#include <boost/interprocess/managed_shared_memory.hpp>
#include <boost/interprocess/containers/vector.hpp>
#include <boost/interprocess/sync/interprocess_condition.hpp>
#include <boost/interprocess/sync/interprocess_mutex.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <appkit/export_internal_terms.h>
#include <appkit/tables/mt_fifo.h>
#include <appkit/tables/ringbuf_ctr.h>



using namespace boost::interprocess;


namespace crm{
namespace detail{


enum  class shmem_vector_open_mode{
	create,
	rw
	};

template<typename TContainer>
class shmem_ringbuf_functor_t {
public:
	using container_t = typename std::decay_t<TContainer>;
	using value_type = typename container_t::value_type;
	using size_type = typename container_t::size_type;
	using iterator = typename container_t::iterator;
	using const_iterator = typename container_t::const_iterator;

	static bool is_empty(const container_t & obj) {
		return obj.empty();
		}

	static void resize(container_t & obj, size_type newSize) {
		obj.resize(newSize);
		}

	static void clear(container_t & obj) {
		obj.clear();
		}

	static size_type size(const container_t & obj) {
		return obj.size();
		}

	static value_type get_move(iterator & from) {
		return boost::move((*from));
		}

	static void set_move(iterator & to, value_type && val) {
		(*to) = boost::move(val);
		}
	};


struct shmem_vector_type_traits{
	virtual ~shmem_vector_type_traits() = 0;

	using size_type = std::size_t;
	using shmem_type = managed_shared_memory;
	using segment_manager_t = managed_shared_memory::segment_manager;

	using byte_allocator = allocator<char, segment_manager_t>;
	using byte_vector = vector<char, byte_allocator>;
	using void_allocator = allocator<void, segment_manager_t>;

	using shared_mytex = interprocess_mutex;
	using shared_condvar = interprocess_condition;
	using scoped_mutex_type = scoped_lock<shared_mytex>;
	};


template<typename TIPScv, typename TIPSm>
struct TCondVarTrait <TIPScv, TIPSm, std::enable_if_t <
	std::is_same_v<typename shmem_vector_type_traits::shared_condvar, std::decay_t<TIPScv>> &&
	std::is_same_v<typename shmem_vector_type_traits::scoped_mutex_type, std::decay_t<TIPSm>>>> {

public:
	static bool wait(typename shmem_vector_type_traits::shared_condvar & c,
		typename shmem_vector_type_traits::scoped_mutex_type & m ){

		return c.wait(m);
		}

private:
	template<typename TWaitCond>
	static bool wait_timed(typename shmem_vector_type_traits::shared_condvar & c,
		typename shmem_vector_type_traits::scoped_mutex_type & m,
		TWaitCond & f) {

		const auto endTP = boost::get_system_time() + boost::posix_time::milliseconds(std::chrono::duration_cast<std::chrono::milliseconds>(f.timeout()).count());
		if( c.timed_wait( m, endTP ) ){
			std::cout << "wait:timed:ready" << std::endl;
			CBL_VERIFY( f.value() );
			return true;
			}
		else{
			return false;
			}
		}

	template<typename TWaitCond>
	static bool wait_pred( typename shmem_vector_type_traits::shared_condvar & c,
		typename shmem_vector_type_traits::scoped_mutex_type & m,
		TWaitCond && f ){

		c.wait( m, std::forward<TWaitCond>( f ) );
		CBL_VERIFY( f.value() );
		return f();
		}

	template<typename TWaitCond>
	static bool wait_timed_pred(typename shmem_vector_type_traits::shared_condvar & c,
		typename shmem_vector_type_traits::scoped_mutex_type & m,
		TWaitCond && f){

		const auto endTP = boost::get_system_time() + boost::posix_time::milliseconds(std::chrono::duration_cast<std::chrono::milliseconds>(f.timeout()).count());
		if( c.timed_wait( m, endTP, std::forward<TWaitCond>( f ) ) ){
			CBL_VERIFY( f.value() );
			return true;
			}
		else{
			return false;
			}
		}

	template<typename TWaitCondFault>
	struct wait_wrong_type : std::false_type{};

public:
	template<typename TWaitCond>
	static bool wait(typename shmem_vector_type_traits::shared_condvar & c,
		typename shmem_vector_type_traits::scoped_mutex_type & m,
		TWaitCond && f){

		if constexpr (is_wait_cond_timed_and_pred_v<TWaitCond>){
			return wait_timed_pred(c, m, std::forward<TWaitCond>(f));
			}
		else{
			if constexpr (is_wait_cond_timed_v<TWaitCond>){
				return wait_timed(c, m, std::forward<TWaitCond>(f));
				}
			else{
				if constexpr (is_wait_cond_pred_v<TWaitCond>){
					return wait_pred(c, m, std::forward<TWaitCond>(f));
					}
				else{
					static_assert(wait_wrong_type<TWaitCond>::value, __FILE_LINE__);
					}
				}
			}
		}
	};


struct shmem_ringbuf_handler{
	using container_t = shmem_vector_type_traits::byte_vector;
	using size_type = container_t::size_type;
	using value_type = container_t::value_type;

	/*! ��������� �������� */
	container_t * const _pbuf{ nullptr };
	/// count of a ready elements for reading
	std::atomic_llong * const _nReady{ nullptr };
	/// tail position(first position of ready elements)
	std::atomic_llong * const _nTail{ nullptr };
	/// head position(first position for insert of a next elements)
	std::atomic_llong * const _nHead{ nullptr };

	shmem_ringbuf_handler( container_t *pc, 
		std::atomic_llong *nReady, 
		std::atomic_llong *nTail, 
		std::atomic_llong *nHead )
		: _pbuf( pc )
		, _nReady(nReady)
		, _nTail(nTail)
		, _nHead(nHead){}

	shmem_ringbuf_handler( const shmem_ringbuf_handler& src ) = delete;
	shmem_ringbuf_handler( shmem_ringbuf_handler && src )noexcept = default;
	shmem_ringbuf_handler& operator=( const shmem_ringbuf_handler & o ) = delete;
	shmem_ringbuf_handler& operator=( shmem_ringbuf_handler  && o )noexcept = default;

	void clear()noexcept{
		set_state( {} );
		}

	ringbuf_curr_state_t state()const noexcept{
		ringbuf_curr_state_t st;
		st.Ready = (*_nReady);
		st.Tail = (*_nTail);
		st.Head = (*_nHead);
		return std::move( st );
		}

	void set_state( const ringbuf_curr_state_t & st )noexcept{
		(*_nReady) = st.Ready;
		(*_nTail) = st.Tail;
		(*_nHead) = st.Head;
		}

	size_type size()const noexcept{
		return (*_pbuf).size();
		}

	size_type ready()const noexcept{
		return (*_nReady);
		}

	void ready_set( size_type c )noexcept{
		CBL_VERIFY( size() >= c );
		(*_nReady) = c;
		}

	void ready_dec( size_type c )noexcept{
		CBL_VERIFY( ready() >= c );
		(*_nReady) -= c;
		}

	void ready_inc( size_type c )noexcept{
		CBL_VERIFY( size() >= (ready() + c) );
		(*_nReady) += c;
		}

	size_type tail()const noexcept{
		return (*_nTail);
		}

	void tail_set( size_type c )noexcept{
		CBL_VERIFY( size() >= c );
		(*_nTail) = c;
		}

	void tail_dec( size_type c )noexcept{
		CBL_VERIFY( tail() >= c );
		(*_nTail) -= c;
		}

	void tail_inc( size_type c )noexcept{
		CBL_VERIFY( size() >= (tail() + c) );
		(*_nTail) += c;
		}

	size_type head()const noexcept{
		return (*_nHead);
		}

	void head_set( size_type c )noexcept{
		CBL_VERIFY( size() >= c );
		(*_nHead) = c;
		}

	void head_dec( size_type c )noexcept{
		CBL_VERIFY( head() >= c );
		(*_nHead) -= c;
		}

	void head_inc( size_type c )noexcept{
		CBL_VERIFY( size() >= (head() + c) );
		(*_nHead) += c;
		}

	template<typename XValue>
	void write( XValue && v, size_type off ){
		CBL_VERIFY( size() > off );
		(*_pbuf)[off] = std::forward< XValue>(v);
		}

	template< typename TInIt >
	typename TInIt write( TInIt first_, TInIt last_, size_type off ){
		CBL_VERIFY( size() > off );

		const auto sourceCount = std::distance( first_, last_ );
		if( sourceCount >= 0 ){

			auto inIt = first_;
			const size_type vcnt = (std::min<size_type>)(sourceCount, size() - off);
			for( size_type idx = 0; idx < vcnt; ++idx, ++inIt ){
				(*_pbuf)[idx + off] = ( *inIt );
				}

			return inIt;
			}
		else{
			THROW_EXC_FWD(nullptr);
			}
		}

	template<typename TOutIt>
	typename TOutIt read( TOutIt first_, TOutIt last_, size_type off ){
		CBL_VERIFY( size() > off );

		const auto outSize = std::distance( first_, last_ );
		if( outSize > 0 ){
			const size_type ecount = (std::min<size_type>)(outSize, size() - off);
			auto outIt = first_;

			for( size_type idx = 0; idx < ecount; ++idx, ++outIt ){
				(*outIt) = (*_pbuf)[idx + off];
				}

			return outIt;
			}
		else{
			THROW_EXC_FWD(nullptr);
			}
		}

	template<typename XValue>
	void read( value_type & out, size_type off ){
		CBL_VERIFY( size() > off );
		(out) = (*_pbuf)[off];
		}

	value_type& front(){
		if( ready() == 0 )
			FATAL_ERROR_FWD(nullptr);

		return (*_pbuf)[tail()];
		}

	const value_type& front()const{
		if( ready() == 0 )
			FATAL_ERROR_FWD(nullptr);

		return (*_pbuf)[tail()];
		}
	};


	template<typename TBinary, 
		typename TBreakTkn,
		typename std::enable_if_t<srlz::detail::is_container_contiguous_of_bytes_v<TBinary> && is_break_conditional_v<TBreakTkn>, int> = 0>
	class push_reader : public binary_readers_collection<const uint8_t*, TBreakTkn>{
	private:
		using base_type = binary_readers_collection<const uint8_t*, TBreakTkn>;

		void constructor_body(TBinary && data_){
			add(std::make_unique<mono_reader<shmem_vector_range::block_prefix, decltype(token_break())>>(data_.size(), token_break()));
			add(std::make_unique<binary_reader<decltype(data_), decltype(token_break())>>(std::forward<TBinary>(data_), token_break()));
			}

	public:
		template<typename TBinary, typename TBreakToken>
		push_reader(TBinary && data_, TBreakToken && tk_)
			: base_type(std::forward<TBreakToken>(tk_)){

			constructor_body(std::forward<TBinary>(data_));
			}
		};


	template<typename TBinary,
		typename TBreakTkn,
		typename std::enable_if_t<srlz::detail::is_container_contiguous_of_bytes_v<TBinary> && is_break_conditional_v<TBreakTkn>, int> = 0>
	class pop_buffer final : public output_container_writer<TBinary, TBreakTkn>{
	private:
		using base_type = output_container_writer<TBinary, TBreakTkn>;

	public:
		template<typename TBreakToken>
		pop_buffer(TBreakToken && tk_)
			: base_type(std::forward<TBreakToken>(tk_)){}
		};




struct  shmem_vector_range : shmem_vector_type_traits {

	static constexpr char * vector_object_name = "typed_vector";
	static constexpr char* mytex_object_name = "sync_object";
	static constexpr char* condvar_inc_object_name = "conditional_object_inc";
	static constexpr char* condvar_dec_object_name = "conditional_object_dec";
	static constexpr char* ready_counter_name = "ready_counter";
	static constexpr char* head_counter_name = "head_counter";
	static constexpr char* tail_counter_name = "tail_counter";
	static constexpr char* stateflag_name = "state_flag";

	static const size_type internals_size = 2048;
	
	using shmem_channel_state_state = uint8_t;

	using byte_ring = ringbuf_ctr_t<shmem_ringbuf_handler>;
	using sync_vector_type = mt_fifo_queue_t<shared_mytex&,
		scoped_mutex_type,
		shared_condvar&,
		byte_ring>;

	std::weak_ptr<distributed_ctx_t> _ctx;
	std::string _shmemName;
	std::unique_ptr<sync_vector_type> _smpVector;
	std::atomic<bool> _closedf{ false };

	using block_prefix = srlz::format_traits::length_header;
	static const size_t block_prefix_size = sizeof( block_prefix );
	static const size_t block_max_size = 1024 * 1024 * 1024;

	template<typename TValue,
		typename std::enable_if_t < crm::srlz::is_serializable_v<TValue>, int> = 0>
	std::vector<uint8_t> encode(TValue && v) {
		auto ws = crm::srlz::wstream_constructor_t::make();
		crm::srlz::serialize(ws, v);
		return ws.release();
		}

	template<typename TValue,
		typename std::enable_if_t < crm::srlz::is_serializable_v<TValue>, int> = 0>
	size_t encoded_size( TValue && v )const noexcept{
		return crm::srlz::object_trait_serialized_size( v );
		}

	template<typename TValue,
		typename std::enable_if_t < crm::srlz::is_serializable_v<TValue>, int> = 0>
	void decode(const std::vector<uint8_t> & m, TValue & to)noexcept {
		CHECK_NO_EXCEPTION_BEGIN

		auto rs = crm::srlz::rstream_constructor_t::make(m.cbegin(), m.cend());
		crm::srlz::deserialize(rs, to);

		CHECK_NO_EXCEPTION_END
		}

	void open();

protected:
	shmem_vector_range()noexcept;
	shmem_vector_range(std::weak_ptr<distributed_ctx_t> wctx, std::string_view name);

	shmem_channel_state_state adjust( channel_state st )noexcept;
	static channel_state state_flag( shmem_channel_state_state * st )noexcept;

public:
	virtual ~shmem_vector_range();

	sync_vector_type* smp_vector()const noexcept;

	virtual segment_manager_t* segment_manager()const noexcept = 0;
	virtual const byte_vector* bvector()const noexcept = 0;
	virtual byte_vector* bvector()noexcept = 0;
	std::shared_ptr<distributed_ctx_t> ctx()noexcept;
	virtual shared_mytex * lock()noexcept = 0;
	virtual shared_condvar *condvar_inc()noexcept = 0;
	virtual shared_condvar* condvar_dec()noexcept = 0;
	virtual std::atomic_llong *ready()noexcept =0;
	virtual std::atomic_llong *tail ()noexcept = 0;
	virtual std::atomic_llong *head()noexcept = 0;
	virtual void set( channel_state st )noexcept = 0;
	virtual channel_state state()const noexcept = 0;

public:
	const std::string & name()const noexcept;
	void close()noexcept;
	bool closed()const noexcept;
	size_t free_space()const noexcept;

private:
	std::vector<char> _readBuffer;

public:
	template<typename TBinary, 
		typename TBreakTkn, 
		typename std::enable_if_t<crm::srlz::detail::is_container_of_bytes_v<TBinary> && is_break_conditional_v<TBreakTkn>, int> = 0>
	bool pop( TBinary && v, TBreakTkn && clf ){

		using buffer = pop_buffer<TBinary, TBreakTkn>;
		static_assert(is_output_buffer_v<buffer>, __FILE__);

		buffer wb(std::forward< TBreakTkn>(clf));
		smp_vector()->pop(wb);

		if (wb.completed()) {
			auto r{std::move(wb).release()};
			std::swap(std::forward<TBinary>(v), r);

			return true;
			}
		else {
			return false;
			}
		}

	template<typename TValue, 
		typename TBreakTkn,
		typename std::enable_if_t<!crm::srlz::detail::is_container_of_bytes_v<TValue> &&  is_break_conditional_v<TBreakTkn>, int> = 0>
	bool pop(TValue && v, TBreakTkn && clf) {
		std::vector<uint8_t> buffer;
		if (pop(buffer, std::forward< TBreakTkn>(clf))) {
			decode(buffer, v);
			return true;
			}
		else {
			return false;
			}
		}

	template<typename TValue,
		typename TBreakTkn,
		typename std::enable_if_t<!crm::srlz::detail::is_container_of_bytes_v<TValue> &&  is_break_conditional_v<TBreakTkn>, int> = 0>
	typename std::optional<TValue> pop(TBreakTkn && clf){
		std::vector<uint8_t> buffer;
		if (pop(buffer, std::forward< TBreakTkn>(clf))){

			TValue v;
			decode(buffer, v);
			return std::make_optional(std::move(v));
			}
		else{
			return {};
			}
		}

	template<typename TValue, 
		typename TBreakTkn,
		typename std::enable_if_t< is_break_conditional_v<TBreakTkn>, int> = 0>
	size_t pop(std::list<TValue> & vl, TBreakTkn && clf, size_t maxCount = 10) {
		maxCount;

		vl.clear();

		if (auto pVal = pop<TValue, TBreakTkn>(std::forward< TBreakTkn>(clf))) {
			CBL_VERIFY(pVal.has_value());
			vl.push_back(std::move(pVal).value());
			}

		return vl.size();
		}

	template<typename XValue, 
		typename TBreakTkn,
		typename std::enable_if_t<(
			srlz::is_serializable_v<XValue> &&
			!srlz::detail::is_container_of_bytes_v<XValue> &&
			is_break_conditional_v<TBreakTkn>), int> = 0>
	bool push(XValue && o, TBreakTkn && clf ) {
		return push( encode( std::forward<XValue>( o ) ), std::forward< TBreakTkn>( clf ));
		}

	template<typename TBinary,
		typename TBreakTkn,
		typename std::enable_if_t<crm::srlz::detail::is_container_of_bytes_v<TBinary> &&  is_break_conditional_v<TBreakTkn>, int> = 0>
	bool push( TBinary && o, TBreakTkn && clf ){
		using reader = push_reader<TBinary, TBreakTkn>;

		reader rd( std::forward<TBinary>( o ), std::forward< TBreakTkn>( clf ) );
		smp_vector()->push( rd );

		return rd.completed();
		}
	};


class shmem_vector_ctr final : public shmem_vector_range{
	using base_type = shmem_vector_range;

	shmem_type _shmem;
	byte_vector *_bvector = nullptr;
	shared_mytex *_lck = nullptr;
	shared_condvar *_cndvInc = nullptr;
	shared_condvar *_cndvDec = nullptr;
	std::atomic_llong *_nReady = nullptr;
	std::atomic_llong *_nTail = nullptr;
	std::atomic_llong *_nHead = nullptr;
	shmem_channel_state_state *_state = nullptr;
	std::atomic<bool> _clf{ false };

	void construct_do(size_type vSize);
	static void construct( shmem_vector_ctr *this_, size_type vSize );

public:
	shmem_vector_ctr()noexcept;
	shmem_vector_ctr(std::weak_ptr<distributed_ctx_t> ctx_, std::string_view name_);
	~shmem_vector_ctr();

	segment_manager_t* segment_manager()const noexcept;
	const byte_vector* bvector()const noexcept final;
	byte_vector* bvector()noexcept final;
	shared_mytex * lock()noexcept;
	shared_condvar *condvar_inc()noexcept;
	shared_condvar* condvar_dec()noexcept;
	std::atomic_llong *ready()noexcept;
	std::atomic_llong *tail()noexcept;
	std::atomic_llong *head()noexcept;
	void set( channel_state st )noexcept;
	channel_state state()const noexcept;

	void open(size_t bsize_);
	void close()noexcept;
	};


class shmem_vector_open final : public shmem_vector_range{
	using base_type = shmem_vector_range;

	shmem_type _shmem;
	byte_vector *_bvector = nullptr;
	shared_mytex *_lck = nullptr;
	shared_condvar *_cndvInc = nullptr;
	shared_condvar *_cndvDec = nullptr;
	std::atomic_llong *_nReady = nullptr;
	std::atomic_llong *_nTail = nullptr;
	std::atomic_llong *_nHead = nullptr;
	shmem_channel_state_state *_state = nullptr;
	std::atomic<bool> _clf{false};

	void construct_do();
	static void construct( shmem_vector_open *this_ );

public:
	shmem_vector_open()noexcept;
	shmem_vector_open(std::weak_ptr<distributed_ctx_t> ctx_, std::string_view name_);
	~shmem_vector_open();

	segment_manager_t* segment_manager()const noexcept;
	const byte_vector* bvector()const noexcept final;
	byte_vector* bvector()noexcept final;
	shared_mytex * lock()noexcept;
	shared_condvar *condvar_inc()noexcept;
	shared_condvar* condvar_dec()noexcept;
	std::atomic_llong *ready()noexcept;
	std::atomic_llong *tail()noexcept;
	std::atomic_llong *head()noexcept;
	void set( channel_state st )noexcept;
	channel_state state()const noexcept;

	void open();
	void close()noexcept;
	};


}
}

