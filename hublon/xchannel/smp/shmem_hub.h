#pragma once


#include <memory>
#include "./shmem_reg.h"
#include "../../terms/internal_terms.h"


namespace crm::detail{


class xchannel_shmem_hub_impl;
struct xchannel_shmem_hub_impl_deleter{
	void operator()( xchannel_shmem_hub_impl *p )const noexcept;
	};

class xchannel_shmem_hub final : public io_rt0_x_channel_t {
private:
	typedef std::mutex lck_t;
	typedef std::unique_lock<lck_t> lck_scp_t;

	std::unique_ptr<xchannel_shmem_hub_impl, xchannel_shmem_hub_impl_deleter> _pMemory;
	channel_info_t _chid;
	std::weak_ptr<distributed_ctx_t> _ctx;

	/*! ������� ������� */
	std::multimap<rtx_command_type_t, rtl_roadmap_event_t> _evTbl;
	mutable lck_t _tblLck;

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	mutable crm::utility::__xvalue_counter_logger_t<> __sizeTraceer = __FILE_LINE__;
	crm::utility::__xobject_counter_logger_t<xchannel_shmem_hub> _xDbgCounter;

	void __size_trace()const noexcept;
#endif

	std::atomic<bool> _closef{ false };

	std::list<rtl_roadmap_event_t> find_event_targets( rtx_command_type_t ev )const noexcept final;
	
	void subscribe(const rtl_roadmap_event_t & id, const std::list<rtx_command_type_t> & evlst)final;
	void unsubscribe(const rtl_roadmap_event_t & id)noexcept final;
	//void notify(rtx_command_type_t ev, rt0_x_command_pack_t&& cmd)noexcept final;
	//bool push_command(rt0_x_command_pack_t&& cmd)noexcept final;
	void set_channel_state( channel_state st )noexcept;

	void _subscribe( const rtl_roadmap_event_t & id,
		const std::list<rtx_command_type_t> & evlst );
	void _unsubscribe( const rtl_roadmap_event_t & id )noexcept;

	std::chrono::milliseconds timeout_xchannel()const noexcept;

	void ntf_hndl( std::unique_ptr<dcf_exception_t> && e )const noexcept;

public:
	xchannel_shmem_hub(std::weak_ptr<distributed_ctx_t> ctx, 
		const channel_shared_memory_config & cfg );

	std::shared_ptr<distributed_ctx_t> ctx()const noexcept;
	const channel_info_t& get_channel_info()const final;

	bool push_to_rt1( rt0_x_message_t && object, const std::chrono::milliseconds & wait )noexcept final;
	bool push_to_rt1( command_rdm_t && ev )noexcept final;

	bool pop(rt0_x_command_pack_t & cmd, const std::chrono::milliseconds & wait);
	size_t pop(std::list<rt0_x_command_pack_t> & l, const std::chrono::milliseconds & wait);
	bool pop(rt1_x_message_data_t & m, const std::chrono::milliseconds & wait);
	size_t pop(std::list<rt1_x_message_data_t> & m, const std::chrono::milliseconds & wait);

	static std::shared_ptr< xchannel_shmem_hub> make( std::weak_ptr<distributed_ctx_t> ctx,
		const channel_shared_memory_config & cfg );

	void open();
	void close()noexcept;
	bool closed()const noexcept;
	};
}
