#include "../channel_cm.h"

using namespace crm;
using namespace crm::detail;


channel_common_memory_config::channel_common_memory_config(const channel_info_t & chinf,
	std::shared_ptr<i_rt0_channel_route_t> rt0)
	: _locid(chinf)
	, _rt0(rt0) {}

std::shared_ptr<detail::i_rt0_channel_route_t> channel_common_memory_config::rt0()const noexcept {
	return _rt0;
	}

const channel_info_t & channel_common_memory_config::info()const noexcept{
	return _locid;
	}

xchannel_type_t channel_common_memory_config::type()const noexcept{
	return xchannel_type_t::shared_objects_queue;
	}