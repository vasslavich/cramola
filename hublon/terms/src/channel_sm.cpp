#include "../channel_sm.h"

using namespace crm;
using namespace crm::detail;


void channel_shared_memory_config::set_shmem_options( const detail::xchannel_shmem_hub_args & shmma ){
	_hubOpt = shmma;
	}

void channel_shared_memory_config::set_channel_info( const channel_info_t & chinf ){
	_chinf = chinf;
	}

const channel_info_t& channel_shared_memory_config::info()const noexcept{
	return _chinf;
	}

const xchannel_shmem_hub_args & channel_shared_memory_config::hup_options()const noexcept{
	return _hubOpt;
	}

xchannel_type_t channel_shared_memory_config::type()const noexcept{
	return xchannel_type_t::ipc_shmem;
	}
