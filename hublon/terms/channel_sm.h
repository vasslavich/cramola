#pragma once


#include <appkit/appkit.h>
#include <appkit/export_internal_terms.h>
#include "../xchannel/smp/shmem_reg.h"


namespace crm{


struct channel_shared_memory_config : public channel_config_base{
private:
	detail::xchannel_shmem_hub_args _hubOpt;
	channel_info_t _chinf;

public:
	void set_shmem_options( const detail::xchannel_shmem_hub_args & shmma );
	void set_channel_info( const channel_info_t & chinf );

	const channel_info_t& info()const noexcept;
	const detail::xchannel_shmem_hub_args & hup_options()const noexcept;
	xchannel_type_t type()const noexcept;
	};
}




