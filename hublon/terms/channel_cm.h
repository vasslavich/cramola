#pragma once


#include <appkit/appkit.h>
#include <appkit/export_internal_terms.h>


namespace crm{


class channel_common_memory_config : public channel_config_base{
	channel_info_t _locid;
	std::shared_ptr<detail::i_rt0_channel_route_t> _rt0;

public:
	channel_common_memory_config(const channel_info_t & chinf, std::shared_ptr<detail::i_rt0_channel_route_t> rt0);

	std::shared_ptr<detail::i_rt0_channel_route_t> rt0()const noexcept;
	const channel_info_t & info()const noexcept;
	xchannel_type_t type()const noexcept;
	};
}


