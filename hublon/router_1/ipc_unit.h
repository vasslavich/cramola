#pragma once


#include <appkit/appkit.h>
#include <appkit/notifications/exceptions/excdrv.h>
#include "./ipc_unit_utility.h"
#include "./rt1_main.h"
#include "../xchannel/smp/shmem_host_second.h"
#include "../terms/internal_terms.h"
#include "../unitcmd/cmdl.h"
#include "../xchannel/smp/shmem_module.h"


namespace crm::detail{


class xchannel_shmem_host_second;
class ipc_unit final :
	public ipc_unit_trait,
	public std::enable_shared_from_this<ipc_unit>{

private:
	channel_shared_memory_config _unitChannelOpt;
	std::unique_ptr<i_xchannel_shmem_host_second> _hostChannel;
	std::shared_ptr<distributed_ctx_t> _ctx;
	std::shared_ptr<xchannel_shmem_module> _channel;
	std::shared_ptr<rt1_cntbl_t> _router;
	std::atomic<bool> _closedf{ false };
	//std::shared_ptr<crm::default_binary_protocol_handler_t> _protocol;

	void construct(std::shared_ptr<ipc_unit_options> opt,
		exception_handler && eh );

	xchannel_shmem_host_second* second_channel()const noexcept;
	unit_connect make_unit_connection_options()const;
	unit_disconnect make_unit_disconnection_options()const noexcept;

	void handle(std::unique_ptr<dcf_exception_t> && n)const noexcept;

	using io_break_token = break_cancel_timed<std::chrono::milliseconds::rep, std::chrono::milliseconds::period>;
	io_break_token mk_cancel_token(const std::chrono::milliseconds & timeout)const;

	void connect_to_host(const std::chrono::milliseconds & timeout);
	void disconnect_from_host()noexcept;

	std::shared_ptr<distributed_ctx_t> ctx()const noexcept;
	
	static std::unique_ptr<i_xchannel_shmem_host_second> mkchannel(std::shared_ptr<distributed_ctx_t> c,
		const xchannel_shmem_host_args& ca);

	template<typename Options>
	void mkstate(std::shared_ptr<distributed_ctx_t> c, Options&& opt) {
		_ctx = c;
		_hostChannel = mkchannel(c, opt.host_channel());

		if(!opt.protocol()) {
			opt.set_protocol(std::make_unique<default_binary_protocol_handler_t>(_ctx));
			}

		_unitChannelOpt.set_channel_info(channel_info_t(_ctx->make_channel_identity()));

		xchannel_shmem_hub_args shma;
		shma.xchannel_x_RT1_m.mainNodeName = opt.channel_options().mainNodeName + "_x-rt1-m";
		shma.xchannel_x_RT1_c.mainNodeName = opt.channel_options().mainNodeName + "_x-rt1-c";
		shma.xchannelRT0_x_m.mainNodeName = opt.channel_options().mainNodeName + "_rt0-x-m";
		shma.xchannelRT0_x_c.mainNodeName = opt.channel_options().mainNodeName + "_rt0-x-c";

		_unitChannelOpt.set_shmem_options(shma);

		if(auto pch = std::dynamic_pointer_cast<xchannel_shmem_module>(make_channel_ipc_module(_ctx, _unitChannelOpt))) {
			_channel = pch;
			}
		else {
			FATAL_ERROR_FWD(nullptr);
			}

		opt.set_context(_ctx);

		_router = rt1_cntbl_t::create(_channel, std::forward<Options>(opt));
		}

	template<typename Options>
	void constructor(std::shared_ptr<distributed_ctx_t> c, Options && options) {
		mkstate(c, std::forward<Options>(options));
		}

public:
	~ipc_unit();

	ipc_unit()noexcept;
	ipc_unit(const ipc_unit&) = delete;
	ipc_unit& operator=(const ipc_unit&) = delete;

	template<typename Options, 
		typename std::enable_if_t<is_ipc_unit_options_v<Options>, int> = 0>
	static std::shared_ptr<ipc_unit> make(std::shared_ptr<distributed_ctx_t> c, Options && opt) {
		auto obj = std::make_shared<ipc_unit>();
		obj->constructor(c, std::forward<Options>(opt));
		return obj;
		}

	void close()noexcept;
	void start(const ipc_unit_start_options  & opt);
	void stop()noexcept;
	bool stopped()const noexcept;
	//std::shared_ptr<default_binary_protocol_handler_t> protocol()const noexcept;

	template<typename Settings,
		typename std::enable_if_t< is_outbound_connection_settings_rt1_v<Settings>, int> = 0>
	opeer_t make_outbound(Settings && opt) {
		if(_router)
			return _router->create_out_link(std::forward<Settings>(opt));
		else {
			THROW_EXC_FWD(nullptr);
			}
		}

	template<typename ... TItem>
	auto register_message_type()-> 
		typename std::enable_if_t < (
			srlz::detail::is_all_srlzd_v<TItem...> &&
			!is_any_message_handler_based_v<TItem...> &&
			(sizeof...(TItem)) >= 1
			), void> {

		if (auto c = ctx()) {
			c->register_message_type<TItem...>();
			}
		else {
			THROW_EXC_FWD(nullptr);
			}
		}
	};
}
