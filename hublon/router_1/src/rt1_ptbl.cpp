#include <appkit/appkit.h>
#include "../rt1_ptbl.h"


using namespace crm;
using namespace crm:: detail;



rt1_xpeer_router_t::rt1_xpeer_router_t( std::weak_ptr<distributed_ctx_t> ctx_,
	const identity_descriptor_t & thisId,
	std::shared_ptr<io_x_rt1_channel_messages_t> xchannel,
	std::shared_ptr<rt1_command_router_t> cmdrt )
	: _ctx( ctx_ )
	, _commandRt(std::move(cmdrt))
	, _xChannel( xchannel )
	, _lcid( CHECK_PTR( ctx_ )->make_local_object_identity() )
	, _thisEvRdm( rtl_roadmap_event_t::make( thisId, CHECK_PTR(ctx_)->make_event_identity(), rtl_table_t::rt1_router, rtl_level_t::l1) )
	, _thisMsgRdm( rtl_roadmap_obj_t::make_router_rt1( thisId ) ){}

void rt1_xpeer_router_t::ctr( connection_hndl_t && xhndl_Connection,
	disconnection_hndl_t && xhndl_Disconnection,
	local_object_create_connection_t && localIdCtr,
	remote_object_check_identity_t && remoteIdValidator,
	create_remote_object_initialize_list_t && remoteInitializer,
	opeer_connection_exception_t && opeerFail,
	canceled_f && cancelf,
	std::shared_ptr<default_binary_protocol_handler_t> && gtwCtr,
	std::shared_ptr<i_input_messages_stock_t> && instackCtr,
	object_initialize_handler_t && outXIniHndl,
	state_value_ctr_t && stvalCtr,
	__event_handler_point_f && eh ) {

	_xhndl_Connection = std::move( xhndl_Connection );
	_xhndl_Disconnection = std::move( xhndl_Disconnection );
	_localIdCtr = std::move( localIdCtr );
	_remoteIdValidator = std::move( remoteIdValidator );
	_remoteInitializer = std::move( remoteInitializer );
	_cancelExt = std::move( cancelf );
	_gtwCtr = std::move( gtwCtr );
	_instackCtr = std::move( instackCtr );
	_outXIniHndl = std::move( outXIniHndl );
	_stvalueCtr = std::move( stvalCtr );
	_evHndl = std::move( eh );
	_opeerConnectionFail = std::move( opeerFail );

	_ipTbl = std::make_unique<rt1_streams_table_t>( _ctx );
	_opTbl = std::make_unique<rt1_streams_table_t>( _ctx );

	if( !_xhndl_Connection
		|| !_xhndl_Disconnection
		|| !_localIdCtr
		|| !_remoteIdValidator
		|| !_cancelExt
		|| !_evHndl
		|| !_instackCtr) {

		FATAL_ERROR_FWD(nullptr);
		}
	}

rt1_xpeer_router_t::~rt1_xpeer_router_t() {
	close();

	_ipTbl.reset();
	_opTbl.reset();
	}

void rt1_xpeer_router_t::open() {
	bool stf = false;
	if(_startf.compare_exchange_strong(stf, true)) {

		/* ����������� � �������������� 0-�� ������ */
		register_rt0_slot();

		/* ������ ����������� �������� */
		start_x();
		}
	else {
		THROW_MPF_EXC_FWD(nullptr);
		}
	}

std::shared_ptr<distributed_ctx_t> rt1_xpeer_router_t::ctx()const noexcept {
	return _ctx.lock();
	}

std::shared_ptr<const rt1_command_router_t> rt1_xpeer_router_t::command_router()const noexcept {
	return _commandRt;
	}

std::shared_ptr<rt1_command_router_t> rt1_xpeer_router_t::command_router() noexcept {
	return _commandRt;
	}

std::shared_ptr<io_x_rt1_channel_messages_t> rt1_xpeer_router_t::x_channel()const noexcept {
	return std::atomic_load_explicit( &_xChannel, std::memory_order::memory_order_acquire );
	}

bool rt1_xpeer_router_t::canceled()const noexcept {
	if( _cancelExt && _cancelExt() )
		return true;
	else
		return _cancelf.load( std::memory_order::memory_order_acquire );
	}

void rt1_xpeer_router_t::close()noexcept {
	bool cnlf = false;
	if( _cancelf.compare_exchange_strong( cnlf, true ) ) {

		const auto ilst = _ipTbl->close();
		for( auto & rdm : ilst ) {
			CHECK_NO_EXCEPTION( unregister_rt0( rdm, unregister_message_track_reason_t::closed_by_self ) );
			}

		const auto olst = _opTbl->close();
		for( auto & rdm : olst ) {
			CHECK_NO_EXCEPTION( unregister_rt0( rdm, unregister_message_track_reason_t::closed_by_self ) );
			}

		CHECK_NO_EXCEPTION( unregister_rt0_slot() );
		CHECK_NO_EXCEPTION( join_x_read_packets() );
		}
	}

void rt1_xpeer_router_t::start_x() {
	launch_x_read_packets();
	}

void rt1_xpeer_router_t::ntf_hndl( std::unique_ptr<dcf_exception_t> && ntf ) noexcept {
	try {
		auto ctx_( ctx() );
		if( ctx_ ) {
			ctx_->exc_hndl( std::move( ntf ) );
			}
		}
	catch( const dcf_exception_t &e1 ) {
		file_logger_t::logging(e1);
		}
	catch (const std::exception & e2) {
		file_logger_t::logging(e2);
		}
	catch( ... ) {
		file_logger_t::logging(L"undefined exception", 0, __CBL_FILEW__, __LINE__ );
		}
	}

void rt1_xpeer_router_t::ntf_hndl( dcf_exception_t && exc ) noexcept {
	ntf_hndl( exc.dcpy() );
	}

void rt1_xpeer_router_t::ntf_hndl(const dcf_exception_t & exc ) noexcept {
	ntf_hndl( exc.dcpy() );
	}

const std::unique_ptr<rt1_streams_table_t >& rt1_xpeer_router_t::iptbl()const noexcept{
	return _ipTbl;
	}

const std::unique_ptr<rt1_streams_table_t>& rt1_xpeer_router_t::optbl()const noexcept {
	return _opTbl;
	}

std::shared_ptr<rt1_xpeer_router_t> rt1_xpeer_router_t::create(std::shared_ptr<io_x_rt1_channel_messages_t> xchannel,
	std::shared_ptr<rt1_command_router_t> cmdrt,
	std::shared_ptr<rt1_hub_settings_t>&& ph,
	bool startNow ) {

	auto obj = std::make_shared<rt1_xpeer_router_t>(ph->ctx(),
		ph->this_id(),
		xchannel,
		cmdrt);

	auto localIdCtr = [ph] {
		return ph->local_object_identity();
		};

	auto remoteObjInzr = [ph](syncdata_list_t&& sl) {
		return ph->remote_object_initialize(std::move(sl));
		};

	auto remoteIdValidator = [ph](const identity_value_t& id,
		std::unique_ptr<i_peer_remote_properties_t>& remoteProp) {

		return ph->remote_object_identity_validate(id, remoteProp);
		};

	/* ���������� �������� ����������� */
	auto incomingHndl = [ph, obj]
	(ipeer_t&& p, datagram_t&& iniResult) {

		ph->connection_handler(std::move(p), std::move(iniResult));
		};

	/* ���������� ���������� ���� */
	auto discHndl = [ph]
	(const rt1_xpeer_router_t::in_peer_lnk_t::xpeer_desc_t& xdesc, std::shared_ptr<i_peer_statevalue_t>&& stval) {

		ph->disconnection_handler(xdesc, std::move(stval));
		};

	/* ���������� ��������� ������������� */
	auto initializeHndl = [ph](datagram_t&& dt, object_initialize_result_guard_t&& resultHndl) {
		ph->local_object_initialize(std::move(dt), std::move(resultHndl));
		};

	auto opeerFail = [ph](const remote_connection_desc_t& opeer, std::unique_ptr<dcf_exception_t>&& exc) {
		ph->opeer_fail(opeer, std::move(exc));
		};

	auto stvalCtr = [ph] {
		return ph->state_value();
		};

	auto eh = [ph](std::unique_ptr<i_event_t>&& ev) {
		ph->event_handler(std::move(ev));
		};

	/* �������� �������� */
	obj->ctr(std::move(incomingHndl),
		std::move(discHndl),
		std::move(localIdCtr),
		std::move(remoteIdValidator),
		std::move(remoteObjInzr),
		std::move(opeerFail),
		[] { return false; },
		ph->protocol()->copy(),
		ph->input_stack(),
		std::move(initializeHndl),
		std::move(stvalCtr),
		__event_handler_point_f(std::move(eh), obj->ctx(), async_space_t::ext));

	/* ������ �������� �����/������ */
	if(startNow)
		obj->open();

	return std::move(obj);
	}

void rt1_xpeer_router_t::event_handler( std::unique_ptr<i_event_t> && ev )noexcept {

	if( is_userspace_event( ev ) ) {

		/* ����� ����������� � ��������� ������ */
		auto hndl( _evHndl );
		if( hndl ) {

			auto ctx_( _ctx.lock() );
			if( ctx_ ) {
				ctx_->launch_async_exthndl( __FILE_LINE__, std::move( hndl ), std::move( ev ) );
				}
			}
		}
	}

const rtl_roadmap_event_t& rt1_xpeer_router_t::ev_rdm()const noexcept {
	return _thisEvRdm;
	}

const local_command_identity_t& rt1_xpeer_router_t::local_object_command_id()const noexcept {
	static_assert(std::is_lvalue_reference_v<decltype(_thisEvRdm.lcid())>, __CBL_FILEW__);
	return _thisEvRdm.lcid();
	}

const rtl_roadmap_obj_t& rt1_xpeer_router_t::msg_rdm()const noexcept {
	return _thisMsgRdm;
	}

void rt1_xpeer_router_t::unroute_command_link() noexcept {
	CHECK_NO_EXCEPTION( close() );
	}

bool rt1_xpeer_router_t::is_once()const noexcept {
	return false;
	}

std::shared_ptr<default_binary_protocol_handler_t> rt1_xpeer_router_t::protocol()const noexcept {
	return _gtwCtr;
	}
