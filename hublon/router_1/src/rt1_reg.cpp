#include <appkit/appkit.h>
#include "../rt1_ptbl.h"


using namespace crm;
using namespace crm::detail;


void rt1_xpeer_router_t::register_rt0_slot() {

	if( _thisEvRdm.rt() != rtl_table_t::rt1_router || _thisMsgRdm.table() != rtl_table_t::rt1_router )
		FATAL_ERROR_FWD(nullptr);

	/* ������ �������, �� ������� ������������� ������������� ������ 1 */
	std::list<rtx_command_type_t> evlst( {rtx_command_type_t::in_connectoin_closed,
		rtx_command_type_t::out_connection_closed,
		rtx_command_type_t::exception,
		rtx_command_type_t::invalid_xpeer,
		rtx_command_type_t::sndrcv_fault} );

	auto cmdrt = command_router();
	if( cmdrt ) {
		cmdrt->register2channel(
			/* ������ ������� */
			std::move( evlst ),
			/* ����������� �� ��������� ������� ������������ ������� ������ 1 */
			_thisEvRdm,
			/* ����������� �� ��������� ��������� ������������ ������� ������ 1 */
			_thisMsgRdm,
			std::make_unique<command_link_wrapper_weak_t>( shared_from_this() ) );
		}
	}

void rt1_xpeer_router_t::__unregister_rt0_slot() noexcept {

	if( _thisEvRdm.rt() != rtl_table_t::rt1_router || _thisMsgRdm.table() != rtl_table_t::rt1_router )
		FATAL_ERROR_FWD(nullptr);

	auto cmdrt = command_router();
	if( cmdrt ) {
		CHECK_NO_EXCEPTION( cmdrt->unregister_from_channel( _thisEvRdm,
			_thisMsgRdm,
			unregister_message_track_reason_t::closed_by_self ) );
		}
	}

void rt1_xpeer_router_t::unregister_rt0_slot()noexcept {

	if(auto c = ctx()) {
		c->launch_async( __FILE_LINE__, [sptr= shared_from_this()]{
			CHECK_NO_EXCEPTION( sptr->__unregister_rt0_slot() );
			} );
		}
	}

void rt1_xpeer_router_t::register_events_slot( std::list<rtx_command_type_t> && evlst,
	std::weak_ptr<i_rt1_router_targetlink_t> hndl,
	__error_handler_point_f && rh ) {

	auto shp( hndl.lock() );
	if( shp ) {

		/* �������� � ������� ����������� �������������� ������ 0 */
		register_rt0( std::move(evlst), shp->ev_rdm(), std::move(hndl), std::move( rh ) );
		}
	else {
		THROW_MPF_EXC_FWD(nullptr);
		}
	}

void rt1_xpeer_router_t::__register_message_slot_there( std::weak_ptr<base_stream_x_rt1_t> && hndl  ) {

	auto shndl = hndl.lock();
	if( shndl ) {

		/* ����������� � �������������� ������ 1 */
		const auto & tbl = table( shndl->msg_rdm().table() );
		if( tbl ) {
			if( rt1_register_result_t::success != tbl->insert( std::move( shndl ) ) ) {
				THROW_MPF_EXC_FWD(nullptr);
				}
			}
		else {
			THROW_MPF_EXC_FWD(nullptr);
			}
		}
	else {
		THROW_EXC_FWD(nullptr);
		}
	}

void rt1_xpeer_router_t::register_messages_slot(const rtl_roadmap_obj_t & rdmMsg,
	std::weak_ptr<i_rt1_router_targetlink_t> hndl,
	__error_handler_point_f && rh_) {

	if(rdmMsg.target_id().is_null() || !(rdmMsg.table() == rtl_table_t::in || rdmMsg.table() == rtl_table_t::out))
		FATAL_ERROR_FWD(nullptr);

	auto shndl = hndl.lock();
	if(shndl) {

		auto istream = std::dynamic_pointer_cast<base_stream_x_rt1_t>(std::move(shndl));
		if(istream) {

			/* ����������� � ������ ������� �������������� */
			__register_message_slot_there(std::move(istream));

			/* ����������� � �������������� ������ 0 */
			register_rt0( rdmMsg, std::move( rh_ ) );
			}
		else {
			FATAL_ERROR_FWD(nullptr);
			}
		}
	else {
		THROW_MPF_EXC_FWD(nullptr);
		}
	}

void rt1_xpeer_router_t::__unregister_events_slot_invoke( const rtl_roadmap_event_t & id )noexcept {

	/* ������� �� ������� ����������� */
	auto cmdrt( command_router() );
	if( cmdrt ) {
		CHECK_NO_EXCEPTION( cmdrt->remove_by_id( id ) );
		}

	/* ������� � �������������� 0 */
	CHECK_NO_EXCEPTION( unregister_rt0( id ) );
	}

void rt1_xpeer_router_t::unregister_events_slot( const rtl_roadmap_event_t & id ) noexcept {

	auto ctx_( ctx() );
	if( ctx_ ) {

		std::weak_ptr<self_t> wptr( shared_from_this() );
		ctx_->launch_async( __FILE_LINE__, 

			[wptr]( const rtl_roadmap_event_t & id ) {
			if(auto sptr = wptr.lock()) {
				CHECK_NO_EXCEPTION( sptr->__unregister_events_slot_invoke( id ) );
				}

			}, id );
		}
	}

void rt1_xpeer_router_t::__unregister_messages_slot_there( const rtl_roadmap_obj_t & rdmMsg,
	std::function<void( rtl_roadmap_obj_t )> && rh ) noexcept {

	/* ������� */
	const auto & tbl = table( rdmMsg.table() );
	if( tbl ) {

		/* ����������� ����������� � �������������� ������ 1 */
		auto rt1Unreg = tbl->remove( rdmMsg.rt1_stream_table_key() );
		if( rh ) {
			if(auto c = ctx()) {
				c->launch_async( __FILE_LINE__, std::move( rh ), std::move( rt1Unreg ) );
				}
			}
		}
	else {
		ntf_hndl( CREATE_MPF_PTR_EXC_FWD(nullptr) );
		}
	}

void rt1_xpeer_router_t::__unregister_messages_slot_invoke( const rtl_roadmap_obj_t & rdmMsg,
	detail::unregister_message_track_reason_t r ) noexcept {
	
	auto sptr( shared_from_this() );
	CHECK_NO_EXCEPTION( __unregister_messages_slot_there( rdmMsg, [rdmMsg, r, sptr]( const rtl_roadmap_obj_t & rt1Unreg ) {

		if( !(rt1Unreg == rtl_roadmap_obj_t::null || rt1Unreg == rdmMsg) )
			FATAL_ERROR_FWD(nullptr);

		/* ������ �� ������� ������� ������ �������������� ������ ����� ��� ������������ � �� ������ 0 */
		if( rt1Unreg != rtl_roadmap_obj_t::null ) {
			CHECK_NO_EXCEPTION( sptr->unregister_rt0( rdmMsg, r ) );
			}
		} ) );
	}

void rt1_xpeer_router_t::unregister_messages_slot( const rtl_roadmap_obj_t & rdmMsg, 
	unregister_message_track_reason_t r ) noexcept {

	if( rdmMsg.target_id().is_null() || !(rdmMsg.table() == rtl_table_t::in || rdmMsg.table() == rtl_table_t::out) )
		FATAL_ERROR_FWD(nullptr);

	if(auto ctx_ = ctx()) {

		std::weak_ptr<self_t> wptr( shared_from_this() );
		ctx_->launch_async( __FILE_LINE__,

			[wptr]( const rtl_roadmap_obj_t & rdmMsg, unregister_message_track_reason_t r ) {
			auto sptr( wptr.lock() );
			if( sptr ) {
				CHECK_NO_EXCEPTION( sptr->__unregister_messages_slot_invoke( rdmMsg, r ) );
				}

			}, rdmMsg, r );
		}
	}

void rt1_xpeer_router_t::unregister_remote_object_slot( const rt1_endpoint_t & targetEP,
	const identity_descriptor_t & targetId,
	const source_object_key_t & srcKey )noexcept {
	
	/* �������� �������� � �������������� ������ 0 */
	CHECK_NO_EXCEPTION( unregister_rt0( targetEP, targetId, srcKey ) );
	}

void rt1_xpeer_router_t::unregister_rt0( const rtl_roadmap_obj_t & rdm,
	unregister_message_track_reason_t r )noexcept {

	/* �������� �������� � �������������� ������ 0 */
	auto cmdrt = command_router();
	if( cmdrt ) {
		CHECK_NO_EXCEPTION( cmdrt->unregister_from_channel( rdm, r ) );
		}
	}

void rt1_xpeer_router_t::unregister_rt0( const rtl_roadmap_event_t & evrdm ) noexcept {

	/* �������� �������� � �������������� ������ 0 */
	auto cmdrt = command_router();
	if( cmdrt ) {
		CHECK_NO_EXCEPTION( cmdrt->unregister_from_channel( evrdm ) );
		}
	}

void rt1_xpeer_router_t::unregister_rt0( const rt1_endpoint_t & targetEP,
	const identity_descriptor_t & targetId,
	const source_object_key_t & srcKey )noexcept {
	
	auto cmdrt = command_router();
	if( cmdrt ) {
		CHECK_NO_EXCEPTION( cmdrt->unregister_from_channel( targetEP, targetId, srcKey ) );
		}
	}

void rt1_xpeer_router_t::register_rt0(std::list<rtx_command_type_t> && evlst,
	const rtl_roadmap_event_t  & evrdm,
	std::weak_ptr<i_rt1_router_targetlink_t> hndl,
	__error_handler_point_f&& rh) {

	/* ����������� � �������������� ������ 0 */
	if(rh) {
		auto cmdrt = command_router();
		if(cmdrt) {

			auto scx = rh.space();
			auto errh2 = __command_error_handler_point_f( crm::utility::bind_once_call({ "rt1_xpeer_router_t::register_rt0-1" }, 
				[]( std::unique_ptr<i_command_from_rt02rt1_t> &&, std::unique_ptr<dcf_exception_t> && exc, __error_handler_point_f  && rh ){
				rh( std::move( exc ) );
				}, std::placeholders::_1, std::placeholders::_2, std::move( rh ) ),
					_ctx,
					scx );

			cmdrt->register2channel( std::move( evlst ),
				evrdm,
				std::make_unique<command_link_wrapper_weak_t>( hndl ),
				std::move(errh2) );
			}
		}
	else {
		THROW_MPF_EXC_FWD(nullptr);
		}
	}

void rt1_xpeer_router_t::register_rt0(const rtl_roadmap_obj_t & rdm, __error_handler_point_f && rh) {

	/* ����������� � �������������� ������ 0 */
	if(rh) {
		auto cmdrt = command_router();
		if(cmdrt) {
			auto scx = rh.space();
			auto errh2 = __command_error_handler_point_f( crm::utility::bind_once_call({ "rt1_xpeer_router_t::register_rt0-2" },
				[]( std::unique_ptr<i_command_from_rt02rt1_t> &&, std::unique_ptr<dcf_exception_t> && exc, __error_handler_point_f  && rh ){
				rh( std::move( exc ) );
				}, std::placeholders::_1, std::placeholders::_2, std::move( rh ) ),
				_ctx,
					scx );

			cmdrt->register2channel(rdm, std::move(errh2));
			}
		}
	else {
		THROW_MPF_EXC_FWD(nullptr);
		}
	}

const std::unique_ptr<detail::rt1_streams_table_t>& rt1_xpeer_router_t::table( rtl_table_t t )const noexcept {
	static const std::unique_ptr<detail::rt1_streams_table_t> null_table;

	if( t == rtl_table_t::in ) {
		return _ipTbl;
		}
	else if( t == rtl_table_t::out ) {
		return _opTbl;
		}
	else {
		return null_table;
		}
	}
