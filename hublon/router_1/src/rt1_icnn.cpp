#include <appkit/appkit.h>
#include "../rt1_ptbl.h"


using namespace crm;
using namespace crm::detail;


template<typename TPeerLink>
class io_connection_events_t {
private:
	typename TPeerLink _lnk;

public:
	typedef typename TPeerLink::input_stack_t::value_type input_value_t;
	typedef typename TPeerLink::output_stack_t::value_type output_value_t;

	io_connection_events_t(typename TPeerLink lnk)
		: _lnk(lnk) {}

	template<typename Rep, typename Period, typename TObject>
	bool push(std::unique_ptr<TObject> obj, const std::chrono::duration<Rep, Period> & /*timeout*/) {
		if(!_lnk.closed()) {
			_lnk.push(std::move(obj));
			return true;
			}
		else
			return false;
		}

	template<typename Rep, typename Period>
	bool pop(typename TPeerLink::input_stack_t::value_type & obj,
		const std::chrono::duration<Rep, Period> & timeout) {

		if(!_lnk.closed()) {
			if(_lnk.pop(obj, timeout) && obj.object)
				return true;
			}

		return false;
		}

	bool closed()const {
		return _lnk.closed();
		}
	};


#ifdef CBL_ENABLE_TEST_ERROR_EMULATION_LEVEL_1
#ifdef CBL_ENABLE_TEST_ERROR_EMULATION_LATENCY
bool rt1_xpeer_router_t::check_latency_connection(const address_descriptor_t & address) {

	std::unique_lock<std::mutex> lck(_ltEmLck);

	auto it = _ltEmTbl.find(address);
	if(it == _ltEmTbl.end()) {

		_ltEmTbl.insert({ address, 0 });
		return true;
		}
	else {
		if(++it->second > 2) {
			_ltEmTbl.erase(address);
			return false;
			}
		else
			return true;
		}
	}
#endif
#endif


void rt1_xpeer_router_t::incoming_connection_response_begin(const rtl_roadmap_line_t & rdml,
	const address_descriptor_t & addr) {

	CBL_MPF_KEYTRACE_SET(trait_at_keytrace_set(addr), 117);

	orequest_t msg{};
	msg.set_echo_params(addr);
	msg.set_code(irequest_t::what_t::identity);
	
	CBL_VERIFY(!is_trait_at_emulation_context(msg));
	CBL_VERIFY_SNDRCV_KEY_SPIN_TAG(msg);

	forward_to_rt0(rdml.points.rt0(), std::move(msg));

	CBL_MPF_KEYTRACE_SET(trait_at_keytrace_set(addr), 118);
	}

void rt1_xpeer_router_t::incoming_connection_start(inbound_connection_state_t && st)noexcept {
	CBL_MPF_KEYTRACE_SET(st.desc(), 105);

	if(_thisMsgRdm.target_id() != st.desc().package().address().destination_id())
		FATAL_ERROR_FWD(nullptr);

	auto ctx_(ctx());
	if(ctx_) {

		CBL_MPF_TRACELEVEL_EVENT_RT1_CONNECTION_TRACE("router:create connection:"
			+ st.desc().base_peer_id().to_string()
			+ ":s=" + st.desc().package().address().source_id().to_str()
			+ ":t=" + st.desc().package().address().destination_id().to_str()
			+ ":session=" + st.desc().package().address().session().object_hash().to_str());

		const rtl_roadmap_link_t rdmLink(_thisMsgRdm.target_id(),
			st.desc().package().address().source_id(),
			st.desc().package().address().session());

		/* ��������������� � ����� ����� */
		auto waitKey = rt1_streams_table_wait_key_t(st.desc().rdm().points.rt0(), rdmLink, rtl_table_t::in);
		if(!CHECK_PTR(iptbl())->get_waiting_link(waitKey)) {

			std::weak_ptr<self_t> wptr(shared_from_this());
			auto stream2 = ix_rt1_stream_t::create(ctx(),
				wptr,
				st.desc().rdm().points.rt0(),
				_thisMsgRdm.target_id(),
				st.desc().package().address().source_id(),
				st.desc().package().address().session());

			st.bind_refuse_handler([ws = std::weak_ptr<ix_rt1_stream_t>(stream2)]{
				if(auto s = ws.lock()) {
					s->close();
					}
				});

			stream2->register_routing(__error_handler_point_f(utility::bind_once_call({ "rt1_xpeer_router_t::incoming_connection_start" }, 
				[wptr, waitKey](std::unique_ptr<dcf_exception_t> && exc,
				inbound_connection_state_t && st,
				std::shared_ptr<ix_rt1_stream_t> && is) {

				auto sptr(wptr.lock());
				if(sptr) {
					CBL_MPF_KEYTRACE_SET(st.desc(), 106);

					if(is) {
						if(exc) {

							/* ���� ��� ������� �������� ���������������� ������ � ����������� ���������� ����� �������������� -
							��� ����� ���� ���������� ����, ��� ������� �� �������� ���������� ������� �� ����������� ��������� ��
							������ � ���� �� ��������� �������
							-------------------------------------------------------------------------------------*/
							if(dynamic_cast<const crm::duplicate_object_exception_t*>(exc.get())) {

								MPF_TEST_EMULATION_IN_CONTEXT_I_PEER_MESSAGE_COND(sptr->ctx(), 300, is_trait_at_emulation_context(st.desc().package()));

								/* ������ ��������� ����� */
								CHECK_NO_EXCEPTION(sptr->ntf_hndl(std::move(exc)));

								/* �������� ���-��������� � ��������� ������ � ����� ������������� */
								st.invoke_response_accept();
								}

							/* ����� - �������� ���������� */
							else {
								CHECK_PTR(sptr->optbl())->remove(waitKey);

								CBL_MPF_TRACELEVEL_EVENT_RT1_CONNECTION_TRACE("router:route connection fail:"
									+ st.desc().rdm().points.rt0().connection_key().to_string()
									+ ":s=" + st.desc().package().address().source_id().to_str()
									+ ":t=" + st.desc().package().address().destination_id().to_str()
									+ ":session=" + st.desc().package().address().session().object_hash().to_string());

								/* ������������������ ���������� */
								st.invoke_exception(std::move(exc));
								}
							}
						else {

							MPF_TEST_EMULATION_IN_CONTEXT_I_PEER_MESSAGE_COND(sptr->ctx(), 301, is_trait_at_emulation_context(st.desc().package()));

							auto ctx_(sptr->ctx());
							if(ctx_) {
								CBL_MPF_KEYTRACE_SET(st.desc(), 107);

								ctx_->launch_async(__FILE_LINE__,
									[sptr](inbound_connection_state_t && st, std::shared_ptr<ix_rt1_stream_t> && is) {

									sptr->istream_register_there(std::move(st), std::move(is));
									}, std::move(st), std::move(is));
								}
							}
						}
					else {
						CHECK_PTR(sptr->optbl())->remove(waitKey);

						CBL_MPF_TRACELEVEL_EVENT_RT1_CONNECTION_TRACE("router:route connection fail:"
							+ st.desc().rdm().points.rt0().connection_key().to_string()
							+ ":s=" + st.desc().package().address().source_id().to_str()
							+ ":t=" + st.desc().package().address().destination_id().to_str()
							+ ":session=" + st.desc().package().address().session().object_hash().to_string());

						/* ������������������ ���������� */
						st.invoke_exception(CREATE_MPF_PTR_EXC_FWD(nullptr));
						}
					}
				}, std::placeholders::_1, std::move(st), stream2), ctx(), async_space_t::sys));

			CHECK_PTR(iptbl())->insert_waiting_link(std::move(waitKey), stream2);
			}
		else {
			/* �������� ���-��������� � ��������� ������ � ����� ������������� */
			st.invoke_response_accept();
			}
		}
	}

void rt1_xpeer_router_t::istream_register_there(inbound_connection_state_t && st,
	std::shared_ptr<ix_rt1_stream_t> && is) {

#ifdef CBL_MPF_MESSAGE_TRACEWAY_KEYS_BY_ID
	auto tid = trait_at_keytrace_set(st.desc());
	CBL_VERIFY_SNDRCV_KEY_SPIN_TAG(st.desc());
#endif

	CBL_MPF_TRACELEVEL_EVENT_RT1_CONNECTION_TRACE("router:route connection register a stream:"
		+ st.desc().base_peer_id().to_string()
		+ ":s=" + st.desc().package().address().source_id().to_str()
		+ ":t=" + st.desc().package().address().destination_id().to_str()
		+ ":session=" + st.desc().package().address().session().object_hash().to_string());

	if(is) {
		CBL_MPF_KEYTRACE_SET(tid, 108);

#ifdef CBL_MPF_MESSAGE_TRACEWAY_KEYS_BY_ID
		auto isGuard = std::make_shared< post_scope_action_t<std::function<void()>>>([tid] {
			CBL_MPF_KEYTRACE_SET(tid, 120);
			});
#endif

		MPF_TEST_EMULATION_IN_CONTEXT_I_PEER_MESSAGE_COND(ctx(), 302, is_trait_at_emulation_context(st.desc().package()));

		std::weak_ptr<self_t> wthis(shared_from_this());
		rtl_roadmap_link_t rdmLink(_thisMsgRdm.target_id(),
			st.desc().package().address().source_id(),
			st.desc().package().address().session());
		auto rdml = st.desc().rdm();

		/* ���������� ������� �������� ���� */
		auto closeHndl = [wthis, rdml, rdmLink
		
#ifdef CBL_MPF_MESSAGE_TRACEWAY_KEYS_BY_ID
			, tid, isGuard
#endif
		]
		(const in_peer_t::rdm_local_identity_t & id, closing_reason_t /*reason*/, std::shared_ptr<i_peer_statevalue_t> && stvl )noexcept {

			CBL_MPF_KEYTRACE_SET(tid, 116);

			CBL_VERIFY(!is_null(id));
			CBL_VERIFY(id.rt0() == rdml.points.rt0());

			/* ����� ����������������� ����������� ��������� ���������� */
			if(auto s = wthis.lock()) {
				s->execute_exthndl_disconnection(id, std::move(stvl));
				}
			};

		/* ������� ��������� ������ �� ���������� ��������� start_handshake */
		auto handshakeOk = [wthis, rdmLink
#ifdef CBL_MPF_MESSAGE_TRACEWAY_KEYS_BY_ID
			, tid
#endif
		
		]
		(std::unique_ptr<dcf_exception_t> && exc,
			in_peer_lnk_t && peer_,
			syncdata_list_t && sl,
			std::unique_ptr<execution_bool_tail> && registerCompleted) {

			auto sthis(wthis.lock());
			if(sthis) {
				CBL_MPF_KEYTRACE_SET(tid, 111);

				if(peer_.is_opened()) {

					MPF_TEST_EMULATION_IN_CONTEXT_EXPR_COND(CHECK_PTR(wthis)->ctx(), 303, is_trait_at_emulation_context(CHECK_PTR(wthis)->level()));

					if(rdmLink != peer_.this_rdm().link())
						THROW_MPF_EXC_FWD(nullptr);


					if(!exc) {
						CBL_MPF_KEYTRACE_SET(tid, 112);

						sthis->incoming_connection_handshake_ok(
							std::move(peer_),
							std::move(sl),
							std::move(registerCompleted)
						
#ifdef CBL_MPF_MESSAGE_TRACEWAY_KEYS_BY_ID
							, tid
#endif
						);
						}
					else {
						CHECK_NO_EXCEPTION(sthis->ntf_hndl(std::move(exc)));
						}
					}
				}
			};

		auto eh = [wthis](std::unique_ptr<i_event_t> && ev) {
			auto sptr(wthis.lock());
			if(sptr)
				sptr->event_handler(std::move(ev));
			};

		/* �������� ������� �������������� */
		if(_gtwCtr) {

			/* ������� ������� */
			auto dcd = std::make_unique<binary_packet_cvt_t>(_ctx, _gtwCtr->create_emitter());

			/* ����������� ��������� ����������������� ������ */
			auto localIdCtr = _localIdCtr;

			/* ����������� ����������������� ������ ��������� ������� */
			auto remoteIdValidator = _remoteIdValidator;

			/* �������� ������� ������ */
			in_peer_t::accept2i(_ctx,
				std::move(is),
				std::move(dcd),
				std::move(closeHndl),
				std::move(localIdCtr),
				std::move(remoteIdValidator),
				_instackCtr->get(),
				std::move(handshakeOk),
				_stvalueCtr(),
				std::move(eh),
				async_space_t::sys);

			/* �������� ���-��������� � ��������� ������ � ����� ������������� */
			st.invoke_response_accept();

			CBL_MPF_KEYTRACE_SET(st.desc(), 109);
			}
		}
	else {
		CBL_MPF_KEYTRACE_SET(st.desc(), 110);

		/* ������������������ ���������� */
		st.invoke_exception(CREATE_MPF_PTR_EXC_FWD(nullptr));

		CBL_MPF_TRACELEVEL_EVENT_RT1_CONNECTION_TRACE("router:route connection fail of stream registration:"
			+ st.desc().base_peer_id().to_string()
			+ ":s=" + st.desc().package().address().source_id().to_str()
			+ ":t=" + st.desc().package().address().destination_id().to_str()
			+ ":session=" + st.desc().package().address().session().object_hash().to_string());
		}
	}

void rt1_xpeer_router_t::event_incoming_connection(rt0_x_message_unit_t && cpck) {
	CBL_MPF_KEYTRACE_SET(cpck, 104);

	MPF_TEST_EMULATION_IN_CONTEXT_I_PEER_MESSAGE_COND(ctx(),304, is_trait_at_emulation_context(cpck.unit().package()));

	auto nodeId = cpck.unit().rdm().points.rt0();

	if(is_null(nodeId))
		THROW_MPF_EXC_FWD(nullptr);
	if(cpck.unit().package().address().source_id().is_null())
		THROW_MPF_EXC_FWD(nullptr);
	if(cpck.unit().package().address().session().is_null())
		THROW_MPF_EXC_FWD(nullptr);

#if defined( CBL_ENABLE_TEST_ERROR_EMULATION_LEVEL_1 ) && defined(CBL_ENABLE_TEST_ERROR_EMULATION_LATENCY)
	auto ctx_(ctx());
	if(!ctx_)
		THROW_MPF_EXC_FWD(nullptr);

	/* �������� ����������� ������� ����������� ������� �� ������� ����������� �
	���������� ������� ������ 1 */
	if(check_latency_connection(address)) {
		const auto latencyMs = ctx_->policies().connection_timepoints().connectionRequestInterval * 2;
		std::weak_ptr<rt1_xpeer_router_t> wthis(shared_from_this());
		deffered_action_timer_t(ctx_, [wthis, rdml, address](async_operation_result_t /*rslt*/) {

			auto sthis(wthis.lock());
			if(sthis)
				sthis->incoming_connection_start(rdml, address);
			}, latencyMs);

		CBL_MPF_TRACELEVEL_EVENT_RT1_CONNECTION_TRACE("router:rt1_xpeer_router_t::event_incoming_connection:latency emulator:"
			+ cnKey.to_string()
			+ ":s=" + address.source_id().to_str()
			+ ":t=" + address.destination_id().to_str()
			+ ":session=" + address.session().object_hash().to_string());
		}
	/* ��� ��� �������� ����������� */
	else {
		incoming_connection_start(rdml, address, cnKey);
		}
#else

	auto wptr = std::weak_ptr<self_t>(shared_from_this());
	auto rdm = cpck.unit().rdm();
	auto address = cpck.unit().package().address();
	CBL_VERIFY(!is_null(address.source_subscriber_id()));

#pragma message( CBL_VAR_NAME_VALUE(CBL_BUILD_VAR_SNDRCV_REVERSE_GUARD, 84))
	auto exch = [rdm, address, wptr](std::unique_ptr<dcf_exception_t> && e)noexcept {
		if(auto s = wptr.lock()) {
			s->response_exception(rdm, address, std::move(e));
			}
		};

	incoming_connection_start(inbound_connection_state_t(std::move(cpck), [wptr, rdm, address]{
		if(auto sptr = wptr.lock()) {

			/* �������� ���-��������� � ��������� ������ � ����� ������������� */
			sptr->incoming_connection_response_begin(rdm, address);
			}
		}, std::move(exch)));
#endif
	}

void rt1_xpeer_router_t::incoming_connection_handshake_ok(in_peer_lnk_t && peer,
	syncdata_list_t && sl,
	std::unique_ptr<execution_bool_tail> && registerCompleted

#ifdef CBL_MPF_MESSAGE_TRACEWAY_KEYS_BY_ID
	, keycounter_trace::key_t ktid
#endif
) {

	CBL_MPF_KEYTRACE_SET(ktid, 113);

	MPF_TEST_EMULATION_IN_CONTEXT_EXPR_COND(ctx(), 305, is_trait_at_emulation_context(level()));

	/* ������� ���������������� ���������� ������� ����������� */
	incoming_connection_initialize(std::move(peer),
		std::move(sl),
		std::move(registerCompleted)

#ifdef CBL_MPF_MESSAGE_TRACEWAY_KEYS_BY_ID
		, ktid
#endif
	);
	}

#ifdef CBL_MPF_GLOBAL_TRACEWAY_COUNTERS
std::atomic<std::intmax_t> IncomingConnectionsInitializeStage{ 0 };

size_t get_stat_counter_incoming_connections_initialize_stage()noexcept {
	return IncomingConnectionsInitializeStage.load(std::memory_order::memory_order_acquire);
	}
#else
size_t get_stat_counter_incoming_connections_initialize_stage()noexcept { return 0; }
#endif

void rt1_xpeer_router_t::incoming_connection_initialize(in_peer_lnk_t&& lnk,
	syncdata_list_t&& sl,
	std::unique_ptr<execution_bool_tail>&& registerCompleted

#ifdef CBL_MPF_MESSAGE_TRACEWAY_KEYS_BY_ID
	, keycounter_trace::key_t ktid
#endif
) {

	CBL_MPF_KEYTRACE_SET(ktid, 114);

#ifdef CBL_MPF_GLOBAL_TRACEWAY_COUNTERS
	post_scope_action_t postDecCnt([] {
		--IncomingConnectionsInitializeStage;
		});
	++IncomingConnectionsInitializeStage;
#endif

	lnk.set_state(remote_object_state_t::initialize_stage);

	CBL_MPF_TRACELEVEL_EVENT_RT1_CONNECTION_TRACE("router:route initialize remote object:"
		+ lnk.connection_key().to_string()
		+ ":s=" + lnk.desc().source_id().to_str()
		+ ":t=" + lnk.desc().target_id().to_str()
		+ ":session=" + lnk.desc().session().object_hash().to_str()
		+ ":h=" + lnk.desc().object_hash().to_str());

	auto ctx_(ctx());
	if (ctx_) {

		/* ����������� ������� */
		auto reqCtr = crm::utility::bind_once_call({ "rt1_xpeer_router_t::incoming_connection_initialize:reqctr" },
			[wthis = weak_from_this()](syncdata_list_t&& sl) {

			MPF_TEST_EMULATION_IN_CONTEXT_EXPR_COND(CHECK_PTR(wthis)->ctx(), 306, is_trait_at_emulation_context(CHECK_PTR(wthis)->level()));

			/* ��������� ����������������� ���������� ������� ������������� */
			if (auto sthis = wthis.lock()) {

				/* ��������� � ����������������� ������� */
				osendrecv_datagram_t msg;
				if (sthis->_remoteInitializer) {
					msg = osendrecv_datagram_t(sthis->_remoteInitializer(std::move(sl)));
					}
				else {
					msg = osendrecv_datagram_t(syncdata_list_t{});
					}

				msg.set_key(in_peer_t::command_name_initialize);
				msg.set_destination_subscriber_id(in_peer_t::subscribe_address_datagram_for_this);

				return std::move(msg);
				}
			else {
				THROW_MPF_EXC_FWD(nullptr);
				}
			}, std::move(sl));

		/* ���������� ������ */
		auto resHndl = crm::utility::bind_once_call_no_throw("rt1_xpeer_router_t::incoming_connection_initialize:resh",
			[wthis = weak_from_this()](
				message_handler && m,
				in_peer_lnk_t&& peer,
				std::unique_ptr<execution_bool_tail>&& registerCompleted

#ifdef CBL_MPF_GLOBAL_TRACEWAY_COUNTERS
				, decltype(postDecCnt) && /*postDec*/
#endif

#ifdef CBL_MPF_MESSAGE_TRACEWAY_KEYS_BY_ID
				, decltype(ktid) ktid
#endif
				)noexcept {

			if (auto sthis = wthis.lock()) {
				CBL_MPF_KEYTRACE_SET(ktid, 115);

				post_scope_action_t xguard([&peer] {
					CHECK_NO_EXCEPTION(peer.close());
					});

				if (m.has_result()) {
					auto iDtg = std::move(m).as<isendrecv_datagram_t>();
					MPF_TEST_EMULATION_IN_CONTEXT_I_PEER_MESSAGE_COND(CHECK_PTR(wthis)->ctx(), 307, is_trait_at_emulation_context(iDtg));

					if (iDtg.key() != in_peer_t::command_name_initialize) {
						FATAL_ERROR_FWD(nullptr);
						}

					peer.set_state(remote_object_state_t::impersonated);

					sthis->incoming_connection_tail_register(std::move(peer),
						std::move(iDtg),
						std::move(registerCompleted));

					CHECK_NO_EXCEPTION(xguard.reset());
					}
				else if (m.has_exception()) {
					CHECK_NO_EXCEPTION(sthis->ntf_hndl(std::move(m).exception()));
					}
				else {
					CHECK_NO_EXCEPTION(sthis->ntf_hndl(CREATE_MPF_PTR_EXC_FWD(nullptr)));
					}
				}
			}, 
				std::placeholders::_1, 
				lnk,
				std::move(registerCompleted)

#ifdef CBL_MPF_GLOBAL_TRACEWAY_COUNTERS
				, std::move(postDecCnt)
#endif

#ifdef CBL_MPF_MESSAGE_TRACEWAY_KEYS_BY_ID
				, ktid
#endif
				);

		make_async_sndrcv_once_sys(ctx_,
			std::move(lnk),
			std::move(reqCtr),
			std::move(resHndl),
			std::string("rt1_xpeer_router_t::incoming_connection_initialize:") + typeid(in_peer_lnk_t).name(),
			ctx_->policies().connection_timepoints(),
			launch_as::async,
			inovked_as::async);
		}
	}

void rt1_xpeer_router_t::incoming_connection_tail_register(in_peer_lnk_t && peer,
	isendrecv_datagram_t&& initializeResult,
	std::unique_ptr<execution_bool_tail> && registerCompleted) {

	peer.invoke_event(event_type_t::st_connected);

	/* ����� ����������������� ����������� ��������� ����������� ����  */
	execute_exthndl_connection(std::move(peer),
		std::move(initializeResult),
		std::move(registerCompleted));
	}

