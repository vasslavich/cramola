#include <appkit/appkit.h>
#include "../rt1_ptbl.h"


using namespace crm;
using namespace crm::detail;


void rt1_xpeer_router_t::join_x_read_packets()noexcept{
	if( _thChannelRead.joinable() ){
		_thChannelRead.join();
		}
	}


void rt1_xpeer_router_t::launch_x_read_packets() {
	if (!_thChannelRead.joinable()) {
	#pragma message("�����!")
		_thChannelRead = std::thread([this, timeoutWait = CHECK_PTR(_ctx)->policies().timeout_wait_io()]{

#ifdef CBL_TRACE_THREAD_CREATE_POINT
			CHECK_NO_EXCEPTION(crm::file_logger_t::logging("rt1_xpeer_router_t::launch_x_read_packets",
			CBL_TRACE_THREAD_CREATE_POINT_TAG, __CBL_FILEW__, __LINE__));
#endif

		auto ichannel(x_channel());
		if (ichannel) {
			while (!canceled()) {

#pragma message( CBL_VAR_NAME_VALUE(CBL_BUILD_VAR_SNDRCV_REVERSE_GUARD, 81))

				std::list<rt0_x_message_t> lst;
				if (ichannel->pop_from_rt0(lst, timeoutWait)) {

					for (auto && item : lst) {
						try {
							CBL_MPF_KEYTRACE_SET(item, 100);

							routing_message(std::move(item), invoke_context_trait::can_be_used_as_async);
							}
						catch (const dcf_exception_t & exc0) {
							ntf_hndl(exc0.dcpy());
							}
						catch (const std::exception & exc1) {
							ntf_hndl(CREATE_MPF_PTR_EXC_FWD(exc1));
							}
						catch (...) {
							ntf_hndl(CREATE_MPF_PTR_EXC_FWD(nullptr));
							}
						}
					}
				}
			}
			});
		}
	}

static std::string routing_trace_tag(const rt0_x_message_unit_t & obj, std::string_view tag_)noexcept {
	std::ostringstream result;
	result << tag_;

	result << ":" << obj.unit().rdm().points.rt0().connection_key().to_string();
	result << ":" << obj.unit().package().address().to_string();

	return std::move(result).str();
	}

void rt1_xpeer_router_t::routing_message(rt0_x_message_unit_t && obj, invoke_context_trait ic) {
	CBL_MPF_KEYTRACE_SET(obj, 101);

	/* ��������� ��������� ���������� �� response-��������� */
	//if(obj.unit().package().type().type() > iol_types_t::st_Internal) {

	CHECK_NO_EXCEPTION_BEGIN

		/* ��������� ��������� request-��������� */
		if (is_sendrecv_cycle(obj)) {
			if (is_sendrecv_request(obj.unit().package().address())) {
				CBL_VERIFY(obj.unit().package().type().type() != iol_types_t::st_notification||
					!is_sendrecv_request(obj.unit().package().address()));

#pragma message(CBL_MESSAGE_ROADMAP_FOOTMARK(CBL_MESSAGE_ROADMAP,RQi_L1))
#pragma message( CBL_VAR_NAME_VALUE(CBL_BUILD_VAR_SNDRCV_REVERSE_SETUP_FUNCTOR, 1))
#pragma message( CBL_VAR_NAME_VALUE(CBL_BUILD_VAR_SNDRCV_REVERSE_GUARD, 82))

				if (obj.reset_invoke_exception_guard(inbound_message_with_tail_exception_guard(
					[wptr = std::weak_ptr<self_t>(shared_from_this()),
					inRDM0 = obj.unit().rdm().points.rt0(),
					inAddr = obj.unit().package().address(),
					type_ = obj.unit().package().type()](std::unique_ptr<dcf_exception_t> && exc)noexcept {

					CHECK_NO_EXCEPTION_BEGIN

						CBL_MPF_KEYTRACE_SET(inAddr.spin_tag(), 506);

					if (auto p = wptr.lock()) {

						std::wostringstream what;
						what << L":addr=" << inRDM0.connection_key().to_wstring();
						what << L":s=" << inAddr.source_id();
						what << L":t=" << inAddr.destination_id();
						what << L":st=" << inAddr.spin_tag();
						what << L":type=" << type_.utype();

						if (!exc) {
							exc = CREATE_MPF_PTR_EXC_FWD(what.str());
							}
						else {
							exc->add_message_line(what.str());
							}

						exc->set_code(RQi_L1);

#ifdef CBL_MPF_TRACELEVEL_EVENT_SNDRCV_GUARD
						GLOBAL_STOUT_FWDRITE_STR(L"sndrcv-guard(request:r1):" + exc->msg());
#endif

						if (auto c = p->ctx()) {
							c->launch_async(__FILE_LINE__,
								[wptr](std::unique_ptr<dcf_exception_t> && exc, rt0_node_rdm_t && rdm0, address_descriptor_t && addr) {

								if (auto sptr = wptr.lock()) {
									sptr->response_exception(rdm0, addr, std::move(exc));
									}
								}, std::move(exc), std::move(inRDM0), std::move(inAddr));
							}
						}

					CHECK_NO_EXCEPTION_END
					},
					ctx(),

#ifdef CBL_MPF_TRACELEVEL_REVERSE_EXC
						routing_trace_tag(obj, __FILE_LINE__),
#else
						__FILE_LINE__,
#endif

						async_space_t::sys))) {

					FATAL_ERROR_FWD(nullptr);
					}
				}

			/* ��������� ��������� response-��������� */
			else if (obj.unit().package().type().fl_report()) {
#pragma message(CBL_MESSAGE_ROADMAP_FOOTMARK(CBL_MESSAGE_ROADMAP,RZi_L1))

				if (obj.reset_invoke_exception_guard(inbound_message_with_tail_exception_guard(
					[wptr = weak_from_this(),
					inRDM = obj.unit().rdm().points,
					inAddr = obj.unit().package().address(),
					type_ = obj.unit().package().type()](std::unique_ptr<dcf_exception_t> && exc)noexcept {

					CHECK_NO_EXCEPTION_BEGIN

						CBL_MPF_KEYTRACE_SET(inAddr.spin_tag(), 505);

					if (auto p = wptr.lock()) {
						std::wostringstream what;
						what << L"sndrcv-guard(response:r1)";
						what << L":addr=" << inRDM.rt0().connection_key().to_wstring();
						what << L":s=" << inAddr.source_id();
						what << L":t=" << inAddr.destination_id();
						what << L":st=" << inAddr.spin_tag();
						what << L":type=" << type_.utype();

						if (!exc) {
							exc = CREATE_MPF_PTR_EXC_FWD(what.str());
							}
						else {
							exc->add_message_line(what.str());
							}
						exc->set_code(RZi_L1);

#ifdef CBL_MPF_TRACELEVEL_EVENT_SNDRCV_GUARD
						GLOBAL_STOUT_FWDRITE_STR(L"sndrcv-guard(response:r1):" + exc->msg());
#endif

						p->handler_exception_RZi(inRDM, inAddr, std::move(exc));
						}

					CHECK_NO_EXCEPTION_END
					}, ctx(),

#ifdef CBL_MPF_TRACELEVEL_REVERSE_EXC
						routing_trace_tag(obj, __FILE_LINE__),
#else
						__FILE_LINE__,
#endif

						async_space_t::sys))) {

					FATAL_ERROR_FWD(nullptr);
					}
				}
			else {
				FATAL_ERROR_FWD(nullptr);
				}
			}

	CHECK_NO_EXCEPTION_END

		MPF_TEST_EMULATION_IN_CONTEXT_L_COND_TRCX(ctx(), obj.unit().package().address().level(), 527,
			is_trait_at_emulation_context(obj.unit().package()),
			trait_at_keytrace_set(obj.unit().package()));

	routing_message_processing(std::move(obj), ic);
	}


void rt1_xpeer_router_t::routing_message_processing(rt0_x_message_unit_t && obj, invoke_context_trait ic) {
	CBL_MPF_KEYTRACE_SET(obj, 102);

	if (!execute_this(std::move(obj))) {
		CBL_MPF_KEYTRACE_SET(obj, 130);

		std::shared_ptr<detail::base_stream_x_rt1_t> stream;
		if (obj.unit().rdm().points.table() == rtl_table_t::in) {
			CBL_MPF_KEYTRACE_SET(obj, 131);

			stream = _ipTbl->find(obj.unit().rdm().points.rt1_stream_table_key());
			}
		else if (obj.unit().rdm().points.table() == rtl_table_t::out) {
			CBL_MPF_KEYTRACE_SET(obj, 132);

			stream = _opTbl->find(obj.unit().rdm().points.rt1_stream_table_key());
			}
		else {
			CBL_MPF_KEYTRACE_SET(obj, 133);

			THROW_MPF_EXC_FWD(nullptr);
			}

		if (stream) {
			CBL_MPF_KEYTRACE_SET(obj, 134);

			auto targetLink = std::dynamic_pointer_cast<i_rt1_router_targetlink_t>(stream);
			if (targetLink) {

#ifdef CBL_MPF_TEST_EMULATION_REMOTE_HOST_CONNECTION_NOT_FOUND
				auto ctx_(_ctx.lock());
				if (ctx_->test_emulation_error() && obj.unit().package().type().type() == iol_types_t::st_connection) {

					if (std::chrono::system_clock::now().time_since_epoch().count() % 2) {

						CBL_MPF_KEYTRACE_SET(obj, 141);

						/* ��������� �������� ������� */
						CHECK_NO_EXCEPTION(obj.invoke_exception(
							CREATE_TEST_PTR_EXC_FWD((std::string("Not founded:") + routing_trace_tag(obj, __FILE_LINE__)))));

						return;
						}
					else {
						CBL_MPF_KEYTRACE_SET(obj, 142);

						THROW_TEST_EXC_FWD((std::string("Not founded:") + routing_trace_tag(obj, __FILE_LINE__)), __FILEW__, __LINE__, 12314);
						}
					}
#endif

#pragma message( CBL_VAR_NAME_VALUE(CBL_BUILD_VAR_SNDRCV_REVERSE_GUARD, 83))
				targetLink->rt1_router_hndl(std::move(obj), ic);
				}
			else {
				FATAL_ERROR_FWD(nullptr);
				}
			}
		else {
			CBL_MPF_KEYTRACE_SET(obj, 135);

			/* ������� � �������������� 0-�� ������ */
			CHECK_NO_EXCEPTION(unregister_rt0(obj.unit().rdm().points, unregister_message_track_reason_t::not_founded));

			/* ��������� �������� ������� */
			CHECK_NO_EXCEPTION(obj.invoke_exception(
				CREATE_MPF_PTR_EXC_FWD((std::string("Not founded:") + routing_trace_tag(obj, __FILE_LINE__)))));
			}
		}
	}

bool rt1_xpeer_router_t::execute_this(rt0_x_message_unit_t && obj) {
	CBL_MPF_KEYTRACE_SET(obj, 103);

	/* ��������� ��������� - ����� ����������� �� ������ 1 */
	if (obj.unit().package().type().type() == iol_types_t::st_connection
		&& obj.unit().package().address().destination_id() == _thisMsgRdm.target_id()) {

#pragma message( CBL_VAR_NAME_VALUE(CBL_BUILD_VAR_SNDRCV_REVERSE_GUARD, 83))
		event_incoming_connection(std::move(obj));

		return true;
		}
	else
		return false;
	}

void rt1_xpeer_router_t::rt1_router_hndl(std::unique_ptr<i_command_from_rt02rt1_t> && response) {

	if (!canceled() && response) {
		switch (response->type()) {
				case rtx_command_type_t::in_connectoin_closed:
					event_from_rt0_incoming_closed(std::move(response));
					break;

				case rtx_command_type_t::out_connection_stated_ready:
					event_from_rt0_outcoming_opened(std::move(response));
					break;

				case rtx_command_type_t::exception:
					break;

				case rtx_command_type_t::out_connection_closed:
					event_from_rt0_outcoming_closed(std::move(response));
					break;

				case rtx_command_type_t::invalid_xpeer:
					event_from_rt0_invalid_xpeer(std::move(response));
					break;

				case rtx_command_type_t::sndrcv_fault:
					event_from_rt0_outmessage_fault(std::move(response));
					break;

				default:
					FATAL_ERROR_FWD(nullptr);
			}
		}
	}

void rt1_xpeer_router_t::event_from_rt0_incoming_closed(std::unique_ptr<i_rt0_command_t> && ev_) {
	auto cmd(dynamic_cast<const i_rt0_cmd_incoming_closed_t*>(ev_.get()));
	if (cmd) {

		/* ������� ��� ������� ������ 1 */
		CHECK_NO_EXCEPTION(_ipTbl->remove_by_rt0(cmd->node_id()));
		}
	else {
		FATAL_ERROR_FWD(nullptr);
		}
	}

void rt1_xpeer_router_t::event_from_rt0_outcoming_opened(std::unique_ptr<i_rt0_command_t> && ev_) {
	auto cmd(dynamic_cast<const i_rt0_cmd_outcoming_oppened_t*>(ev_.get()));
	if (cmd) {}
	else {
		FATAL_ERROR_FWD(nullptr);
		}
	}

void rt1_xpeer_router_t::event_from_rt0_outcoming_closed(std::unique_ptr<i_rt0_command_t> && ev_) {
	auto cmd(dynamic_cast<const i_rt0_cmd_outcoming_closed_t*>(ev_.get()));
	if (cmd) {

		/* ������� ��� ������� ������ 1 */
		CHECK_NO_EXCEPTION(_opTbl->remove_by_rt0(cmd->node_id()));
		}
	else {
		FATAL_ERROR_FWD(nullptr);
		}
	}

void rt1_xpeer_router_t::event_from_rt0_invalid_xpeer(std::unique_ptr<i_rt0_command_t> && ev_) {
	if (auto cmd = dynamic_cast<const i_rt0_invalid_xpeer_t*>(ev_.get())) {

		/* ������� ������ ������ 1 */
		if (cmd->target_rdm().table() == rtl_table_t::in) {
			CHECK_NO_EXCEPTION(_ipTbl->remove(cmd->target_rdm().rt1_stream_table_key()));
			}
		else if (cmd->target_rdm().table() == rtl_table_t::out) {
			CHECK_NO_EXCEPTION(_opTbl->remove(cmd->target_rdm().rt1_stream_table_key()));
			}
		else {
			FATAL_ERROR_FWD(nullptr);
			}
		}
	else {
		FATAL_ERROR_FWD(nullptr);
		}
	}

void rt1_xpeer_router_t::event_from_rt0_outmessage_fault(std::unique_ptr<i_rt0_command_t> && ev_) {
	if (auto cmd = dynamic_cast<i_rt0_cmd_sndrcv_fault_t*>(ev_.get())) {
		cmd->unpack(*CHECK_PTR(ctx()));
		handler_exception_RZi_L0(cmd->hndl(), std::move((*cmd)).ntf());
		}
	else {
		FATAL_ERROR_FWD(nullptr);
		}
	}


