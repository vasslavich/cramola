#include <appkit/appkit.h>
#include "../rt1_ptbl.h"


using namespace crm;
using namespace crm::detail;


void rt1_xpeer_router_t::async_connect_tr1(std::shared_ptr<rt1_connection_settings_t> && ps) {

	if(ps) {
		std::weak_ptr<self_t> wthis(shared_from_this());

		auto identityCtr = [ps] {
			return ps->local_object_identity();
			};

		auto identityChecker = [ps](const identity_value_t & idSet,
			std::unique_ptr<i_peer_remote_properties_t> & prop) {

			return ps->remote_object_identity_validate(idSet, prop);
			};

		/* ���������� ��������� ������������� */
		auto initializeHndl = [ps]
		(crm::datagram_t && dt, object_initialize_result_guard_t && resultHndl) {

			ps->local_object_initialize(std::move(dt), std::move(resultHndl));
			};

		/* ���������� ������� ����������� */
		auto cnHndl = [ps](std::unique_ptr<dcf_exception_t> && exc, rt1_xpeer_router_t::in_peer_lnk_t && lnk) {
			ps->connection_handler(std::move(exc), std::move(lnk));
			};

		auto ev_hndl = [ps](std::unique_ptr<i_event_t> && ev ) {
			ps->event_handler(std::move(ev));
			};

		auto connectionExcChecker = [ps]
		(detail::connection_stage_t stage, const std::unique_ptr<dcf_exception_t> & exc)noexcept{

			return ps->connnection_exception_suppress_checker(stage, exc);
			};

		/* ���������� �������� ������� ���������� �����������. */
		auto closeHndl = [ps](const out_peer_t::rdm_local_identity_t & desc,
			closing_reason_t,
			std::shared_ptr<i_peer_statevalue_t> && stv) {

			ps->disconnection_handler(desc, std::move(stv));
			};

		auto streamCtr = [wthis, ps]
		(detail::xpeer_out_connector_rt1_t::stream_connection_handler_f && hndl) {

			auto sptr(wthis.lock());
			if(sptr) {
				detail::ox_rt1_stream_t::create(sptr,
					sptr->ctx(),
					ps->this_id(),
					ps->remote_endpoint()->endpoint_id(),
					(*ps->remote_endpoint()),
					std::move(hndl));
				}
			};
		

		auto ep(std::move(*CHECK_PTR(ps->remote_endpoint())));
		auto remoteId = ep.endpoint_id();

		auto stvalueCtr = _stvalueCtr;
		auto hndl(std::make_unique<detail::xpeer_out_connector_rt1_t>(ctx(),
			shared_from_this(),
			ps->this_id(),
			ps->remote_endpoint()->endpoint_id(),
			rt1_endpoint_t(*ps->remote_endpoint()),
			_gtwCtr,
			ps->input_stack(),
			std::move(initializeHndl),
			std::move(cnHndl),
			std::move(stvalueCtr),
			__event_handler_point_f( std::move(ev_hndl), ctx(), async_space_t::ext),
			std::move(connectionExcChecker),
			std::move(streamCtr)));

		auto obj(rt1_connecto_t::launch(ctx(),
			std::move(hndl),
			std::move(identityCtr),
			std::move(identityChecker),
			std::move(closeHndl),
			async_space_t::ext));
		}
	else {
		THROW_EXC_FWD(nullptr);
		}
	}
