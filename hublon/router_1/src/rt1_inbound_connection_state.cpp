#include <appkit/appkit.h>
#include "../rt1_ptbl.h"


using namespace crm;
using namespace crm::detail;


inbound_connection_state_t::inbound_connection_state_t()noexcept {}

inbound_connection_state_t::inbound_connection_state_t(rt0_x_message_unit_t && desc,
	accept_handler_t && acceptHndl,
	std::function<void(std::unique_ptr<dcf_exception_t> && e)> && eh )noexcept
	: _desc(std::move(desc))
	, _acceptHndl(std::move(acceptHndl)) 
	, _eh(std::move(eh)){}

inbound_connection_state_t::inbound_connection_state_t(inbound_connection_state_t && o)noexcept
	: _desc(std::move(o._desc))
	, _acceptHndl(std::move(o._acceptHndl))
	, _refuseHndl(std::move(o._refuseHndl))
	, _eh(std::move(o._eh)){}

inbound_connection_state_t& inbound_connection_state_t::operator=(inbound_connection_state_t && o)noexcept {
	if(this != std::addressof(o)) {
		_desc = std::move(o._desc);
		_acceptHndl = std::move(o._acceptHndl);
		_refuseHndl = std::move(o._refuseHndl);
		_eh = std::move(o._eh);
		}

	return (*this);
	}

void inbound_connection_state_t::invoke_response_accept() {
	if(_acceptHndl) {
		_acceptHndl();
		_desc.reset_invoke_exception_guard().reset();
		_refuseHndl = nullptr;
		}
	}

void inbound_connection_state_t::bind_refuse_handler( refuse_handler_t && f )noexcept{
	CBL_VERIFY( !_refuseHndl );
	_refuseHndl = std::move( f );
	}

void inbound_connection_state_t::invoke_exception(std::unique_ptr<dcf_exception_t> && exc)noexcept {
	CHECK_NO_EXCEPTION(_eh(exc->dcpy()));

	_desc.invoke_exception(std::move(exc));
	}

const rt0_x_message_t& inbound_connection_state_t::desc()const noexcept {
	return _desc.unit();
	}

inbound_message_with_tail_exception_guard inbound_connection_state_t::reset_invoke_exception_guard()noexcept {
	return _desc.reset_invoke_exception_guard();
	}

inbound_message_with_tail_exception_guard inbound_connection_state_t::reset_invoke_exception_guard(inbound_message_with_tail_exception_guard && g)noexcept {
	return _desc.reset_invoke_exception_guard(std::move(g));
	}

inbound_connection_state_t::~inbound_connection_state_t(){
	CHECK_NO_EXCEPTION( _desc.invoke_exception( nullptr ) );

	if( _refuseHndl ){
		CHECK_NO_EXCEPTION( _refuseHndl() );
		CHECK_NO_EXCEPTION(invoke_exception(CREATE_MPF_PTR_EXC_FWD(nullptr)));
		}
	}

