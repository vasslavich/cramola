#include "../ipc_unit_utility.h"

using namespace crm;
using namespace crm::detail;


ipc_unit_options::ipc_unit_options(const xchannel_shmem_hub_options & channelOpt)
	: _channelOpt(channelOpt){}

void ipc_unit_options::set_host_channel( const detail::xchannel_shmem_host_args & hostChA ){
	_hostChannelA = hostChA;
	}

const detail::xchannel_shmem_host_args& ipc_unit_options::host_channel()const noexcept{
	return _hostChannelA;
	}

const xchannel_shmem_hub_options& ipc_unit_options::channel_options()const noexcept{
	return _channelOpt;
	}
