#include <appkit/appkit.h>
#include "../rt1_ptbl.h"


using namespace crm;
using namespace crm::detail;


void rt1_xpeer_router_t::response_exception(const rtl_roadmap_line_t & rdml,
	const address_descriptor_t & address,
	std::unique_ptr<dcf_exception_t> && ntf)noexcept {

	response_exception(rdml.points.rt0(), address, std::move(ntf));
	}

void rt1_xpeer_router_t::response_exception(const rt0_node_rdm_t & rt0rdm,
	const address_descriptor_t & addr,
	std::unique_ptr<dcf_exception_t> && exc) noexcept {

	CBL_MPF_KEYTRACE_SET(trait_at_keytrace_set(addr), 440);

	onotification_t msg{};
	msg.set_echo_params(addr);
	msg.set_ntf(std::move(exc));

	CBL_VERIFY(!is_null(msg.destination_subscriber_id()));
	CBL_VERIFY(msg.address().level() == rtl_level_t::l1);
	CBL_VERIFY(is_sendrecv_response(msg));
	CBL_VERIFY(!is_trait_at_emulation_context(msg));

	if(auto c = _ctx.lock()) {
		c->launch_async(__FILE_LINE__, [rt0rdm, wptr = weak_from_this()](onotification_t && e){
			if(auto sptr = wptr.lock()) {
				sptr->forward_to_rt0(rt0rdm, std::move(e));
				}
			}, std::move(msg));
		}
	}


void rt1_xpeer_router_t::handler_exception_RZi(const rtl_roadmap_obj_t & rdm,
	const address_descriptor_t & address,
	std::unique_ptr<dcf_exception_t> && e)noexcept {

	if(address.fl_destination_subscribe_id()) {
		auto hAddr = _address_hndl_t::make(address.destination_subscriber_id(), iol_types_t::st_RZi_LX_error, address.level(), async_space_t::undef);

		std::shared_ptr<detail::base_stream_x_rt1_t> stream;
		if(rdm.table() == rtl_table_t::in) {
			stream = _ipTbl->find(rdm.rt1_stream_table_key());
			}
		else if(rdm.table() == rtl_table_t::out) {
			stream = _opTbl->find(rdm.rt1_stream_table_key());
			}

		if(stream) {
			auto targetLink = std::dynamic_pointer_cast<i_rt1_router_targetlink_t>(stream);
			if(targetLink) {
				targetLink->addressed_exception(hAddr, std::move(e), invoke_context_trait::can_be_used_as_async);
				}
			}
		else {
			/*__debugbreak();*/
			}
		}
	else {
		FATAL_ERROR_FWD(nullptr);
		}
	}

void rt1_xpeer_router_t::handler_exception_RZi_L0(const outbound_message_desc_t & d,
	std::unique_ptr<dcf_exception_t> && e)noexcept {

	CBL_VERIFY(!is_null(d.streamId));
	CBL_VERIFY(d.streamId.table() == rtl_table_t::in || d.streamId.table() == rtl_table_t::out);

	auto hAddr = _address_hndl_t::make(d.requestEmitter, iol_types_t::st_RZi_LX_error, d.level, async_space_t::undef);

	std::shared_ptr<detail::base_stream_x_rt1_t> stream;
	if(d.streamId.table() == rtl_table_t::in) {
		stream = _ipTbl->find(d.streamId);
		}
	else if(d.streamId.table() == rtl_table_t::out) {
		stream = _opTbl->find(d.streamId);
		}

	if(stream) {
		CBL_MPF_KEYTRACE_SET(d.spitTag, 516);

		auto targetLink = std::dynamic_pointer_cast<i_rt1_router_targetlink_t>(stream);
		if(targetLink) {
			CBL_MPF_KEYTRACE_SET(d.spitTag, 517);

			targetLink->addressed_exception(hAddr, std::move(e), invoke_context_trait::can_be_used_as_async);
			}
		else {
			CBL_MPF_KEYTRACE_SET(d.spitTag, 518);
			}
		}
	else {
		CBL_MPF_KEYTRACE_SET(d.spitTag, 519);

		/*__debugbreak();*/
		}
	}

