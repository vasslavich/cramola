#include "../../xchannel/smp/shmem_vector.h"
#include "../ipc_unit.h"
#include "../../xchannel/chmk.h"
#include "../../terms/internal_terms.h"
#include "../../unitcmd/cmdl.h"


using namespace crm;
using namespace crm::detail;


std::unique_ptr<i_xchannel_shmem_host_second> ipc_unit::mkchannel(std::shared_ptr<distributed_ctx_t> c,
	const xchannel_shmem_host_args& ca) {
	return std::make_unique<xchannel_shmem_host_second>(c, ca);
	}

xchannel_shmem_host_second* ipc_unit::second_channel()const noexcept {
	return static_cast<xchannel_shmem_host_second*>(_hostChannel.get());
	}

std::shared_ptr<distributed_ctx_t> ipc_unit::ctx()const noexcept{
	return _ctx;
	}

unit_connect ipc_unit::make_unit_connection_options()const{
	unit_connect uco;
	uco.chName_RT0_x_c = _unitChannelOpt.hup_options().xchannelRT0_x_c.mainNodeName;
	uco.chName_RT0_x_m = _unitChannelOpt.hup_options().xchannelRT0_x_m.mainNodeName;
	uco.chName_x_RT1_c = _unitChannelOpt.hup_options().xchannel_x_RT1_c.mainNodeName;
	uco.chName_x_RT1_m = _unitChannelOpt.hup_options().xchannel_x_RT1_m.mainNodeName;
	uco.id = _unitChannelOpt.info().id();
	
	return uco;
	}

unit_disconnect ipc_unit::make_unit_disconnection_options()const noexcept{
	unit_disconnect udo;
	udo.id = _unitChannelOpt.info().id();

	return udo;
	}


ipc_unit::io_break_token ipc_unit::mk_cancel_token(const std::chrono::milliseconds & timeout)const{
	static_assert(is_break_timed_and_cancel_v<ipc_unit::io_break_token>, __FILE_LINE__);
	return io_break_token([this]{return stopped(); }, timeout, timeout);
	}

void ipc_unit::connect_to_host(const std::chrono::milliseconds& timeout){
	if( auto ch = second_channel()){
		if( !ch->push(
			channel_strob_compressor::compress( make_unit_connection_options() ),
			mk_cancel_token(timeout))) {

			THROW_EXC_FWD(nullptr);
			}
		}
	else{
		THROW_EXC_FWD(nullptr);
		}
	}

void ipc_unit::disconnect_from_host()noexcept{
	if( auto ch = second_channel()){
		if( !ch->push( channel_strob_compressor::compress( make_unit_disconnection_options() ),
			mk_cancel_token(std::chrono::milliseconds(1000)))) {

			if (!stopped()) {
				FATAL_ERROR_FWD(nullptr);
				}
			}
		}
	}

void ipc_unit::start(const ipc_unit_start_options  & opt){
	if( _ctx )
		_ctx->start();
	else{
		THROW_EXC_FWD(nullptr);
	}

	if( _channel ){
		auto pCh = std::dynamic_pointer_cast<xchannel_shmem_module>(_channel);
		pCh->open( opt.message_mem_size, opt.command_mem_size );
		}
	else{
		THROW_EXC_FWD(nullptr);
	}

	if( auto ch = second_channel())
		ch->open();
	else{
		THROW_EXC_FWD(nullptr);
	}
	   
	connect_to_host(std::chrono::seconds(10));

	if( _router )
		_router->open();
	else{
		THROW_EXC_FWD(nullptr);
	}
	}

void ipc_unit::stop()noexcept {
	_closedf.store(true, std::memory_order::release);
	}

bool ipc_unit::stopped()const noexcept {
	return _closedf.load();
	}

ipc_unit::~ipc_unit() {
	close();
	}

void ipc_unit::close()noexcept{
	disconnect_from_host();

	stop();

	if( _router ){
		_router->close();
		}
	}

//std::shared_ptr<default_binary_protocol_handler_t> ipc_unit::protocol()const noexcept {
//	return _protocol;
//	}

ipc_unit::ipc_unit()noexcept {}

void ipc_unit::handle(std::unique_ptr<dcf_exception_t> && n)const noexcept {
	if (auto c = ctx()) {
		c->exc_hndl(std::move(n));
		}
	}

