#include <appkit/appkit.h>
#include "../rt1_ptbl.h"


using namespace crm; 
using namespace crm::detail;


void rt1_xpeer_router_t::opeer_fail(const remote_connection_desc_t & opeer,
	std::unique_ptr<crm::dcf_exception_t> && exc)noexcept {

	/* ����� ����������������� ����������� � ��������� ������ */
	opeer_connection_exception_t hndl(_opeerConnectionFail);
	if (hndl) {
		if (auto c = _ctx.lock()) {
			c->launch_async_exthndl(__FILE_LINE__, std::move(hndl), opeer, std::move(exc));
			}
		}
	}

void rt1_xpeer_router_t::execute_exthndl_connection(in_peer_lnk_t && peer,
	isendrecv_datagram_t && initializeResult,
	std::unique_ptr<execution_bool_tail> && registerCompleted)noexcept {

	/* ����� ����������������� ����������� ��������� ����������� � ��������� ������ */
	if (_xhndl_Connection) {
		auto ctx_(_ctx.lock());
		if (ctx_) {

			CBL_MPF_TRACELEVEL_EVENT_RT1_CONNECTION_TRACE("router:send extern handler to stack:[event handler connection]:"
				+ peer.remote_object_endpoint().to_string()
				+ ":s=" + peer.desc().source_id().to_str()
				+ ":t=" + peer.desc().target_id().to_str()
				+ ":session=" + peer.desc().session().object_hash().to_str()
				+ ":h=" + peer.desc().object_hash().to_str());

			ctx_->launch_async_exthndl(__FILE_LINE__, [wptr = weak_from_this()]
			(connection_hndl_t hndl_,
				ipeer_t && peer_,
				decltype(initializeResult) && initializeResult,
				std::unique_ptr<execution_bool_tail> && registerCompleted) {

				MPF_TEST_EMULATION_IN_CONTEXT_EXPR_COND(CHECK_PTR(wptr)->ctx(), 309, is_trait_at_emulation_context(CHECK_PTR(wptr)->level()));

				hndl_(std::move(peer_), initializeResult.value_release());

				/* ������������� ����������� ������� */
				if (registerCompleted)
					registerCompleted->commite();

				initializeResult.reset_invoke_exception_guard().reset();

				}, _xhndl_Connection,
				ipeer_t(std::move(peer)),
					std::move(initializeResult),
					std::move(registerCompleted));
			CBL_VERIFY(_xhndl_Connection);
			}
		}
	}

void rt1_xpeer_router_t::execute_exthndl_disconnection(const in_peer_lnk_t::xpeer_desc_t & desc,
	std::shared_ptr<i_peer_statevalue_t> && stv)noexcept {

	/* ����� ����������������� ����������� ��������� ���������� � ��������� ������ */
	if (_xhndl_Disconnection) {
		auto ctx_(_ctx.lock());
		if (ctx_) {

			CBL_MPF_TRACELEVEL_EVENT_RT1_CONNECTION_TRACE("router:event handler disconnection:"
				+ desc.remote_object_endpoint().to_string()
				+ ":" + desc.object_hash().to_str());

			ctx_->launch_async_exthndl(__FILE_LINE__, [desc]
			(disconnection_hndl_t hndl_, std::shared_ptr<i_peer_statevalue_t> && stv) {

				hndl_(desc, std::move(stv));

				}, _xhndl_Disconnection, std::move(stv));
			CBL_VERIFY(_xhndl_Disconnection);
			}
		}
	}
