#include <appkit/appkit.h>
#include "../rt1_ptbl.h"


using namespace crm;
using namespace crm::detail;


const std::chrono::milliseconds rt1_xpeer_router_t::push2X_Timeout{100};

void rt1_xpeer_router_t::push2rt(rt1_x_message_t && obj) {
	forward_to_rt0(std::move(obj));
	}

void rt1_xpeer_router_t::forward_to_rt0( rt1_x_message_t && obj_ ) {
	CBL_MPF_KEYTRACE_SET(obj_.package(), 17);
	CBL_VERIFY_SNDRCV_KEY_SPIN_TAG(obj_);

	auto xch( x_channel() );
	if(xch) {
		CBL_MPF_KEYTRACE_SET(trait_at_keytrace_set(obj_.package()), 444);

		MPF_TEST_EMULATION_IN_CONTEXT_COND_TRCX(ctx(), 475, 
			is_trait_at_emulation_context(obj_.package()),
			trait_at_keytrace_set(obj_.package()));

		bool sndf = obj_.is_sendrecv_request();
		bool rcvf = obj_.is_sendrecv_response();
		auto unit = std::move(obj_).package();
		auto rdm = std::move(obj_).rdm();

		if(sndf || rcvf) {
			CBL_VERIFY_SNDRCV_KEY_SPIN_TAG(unit);
			CBL_VERIFY(CBL_IS_SNDRCV_KEY_SPIN_TAGGED_ON(unit));
			CBL_MPF_KEYTRACE_SET(trait_at_keytrace_set(unit), 445);

			CBL_VERIFY(!sndf || !is_null(unit.desc().streamId));
			CBL_VERIFY(!rdm.points.rt0().connection_key().address.empty());

			if (!xch->push_to_rt0_message({std::move(rdm),
				std::move(unit).capture_bin(),
				CHECK_PTR(_xChannel)->get_channel_info().id(),
				std::move(unit).desc()},
				push2X_Timeout)) {

				THROW_EXC_FWD(nullptr);
				}
			}
		else {
			CBL_VERIFY(CBL_IS_SNDRCV_KEY_SPIN_TAGGED_OFF(unit));
			CBL_MPF_KEYTRACE_SET(trait_at_keytrace_set(unit), 446);

			if (!xch->push_to_rt0_message(rt1_x_message_data_t{std::move(rdm),
				std::move(unit).capture_bin()},
				push2X_Timeout)) {

				THROW_EXC_FWD(nullptr);
				}
			}

		unit.reset_invoke_exception_guard().reset();
		}
	else {
		THROW_MPF_EXC_FWD(nullptr);
		}
	}


