#include <appkit/appkit.h>
#include "../rt1_streamstbl.h"


using namespace crm;
using namespace crm::detail;


#ifdef CBL_OBJECTS_COUNTER_CHECKER
size_t rt1_streams_table_t::__rt0_rt1_size()const {
	size_t accm = 0;
	for( const auto & rt0rt1 : _mapRT0_to_RT1 ) {
		accm += rt0rt1.second.size();
		}

	return accm;
	}

size_t rt1_streams_table_t::__rt1_size()const {
	return _mapRT1.size();
	}

size_t rt1_streams_table_t::__wait_links_size()const {
	return _waitImpersonateLinks.size();
	}

void rt1_streams_table_t::__size_trace()const noexcept{
	CHECK_NO_EXCEPTION(__sizeTraceer.CounterCheck( __rt0_rt1_size() ));
	CHECK_NO_EXCEPTION(__sizeTraceerWaitLinks.CounterCheck( __wait_links_size() ));
	}
#endif


void rt1_streams_table_t::close_link( std::shared_ptr<base_stream_x_rt1_t> && obj, bool lazy )noexcept {

	if( obj ){
		if( lazy ){
			if(auto c = _ctx.lock()){
				c->launch_async( __FILE_LINE__, []( std::shared_ptr<base_stream_x_rt1_t> && obj ){

					CHECK_NO_EXCEPTION( obj->close_from_router() );
					}, std::move( obj ) );
				}
			}
		else{
			CHECK_NO_EXCEPTION( obj->close_from_router() );
			}
		}
	}

bool rt1_streams_table_t::canceled()const noexcept {
	return _cancelf.load( std::memory_order::memory_order_acquire );
	}

std::list<rtl_roadmap_obj_t> rt1_streams_table_t::__remove_by_rt0( 
	const rt0_node_rdm_t &nodeId, bool getLst ) noexcept {

	std::list<rtl_roadmap_obj_t> lst;

	/* ����� ������ �������� ������ 1 */
	auto itNodesLst = _mapRT0_to_RT1.find( nodeId );
	if( itNodesLst != _mapRT0_to_RT1.end() ) {

		/* �� ���� ��������� ����� ������ 1 */
		auto itNd = itNodesLst->second.begin();
		for( ; itNd != itNodesLst->second.end(); ++itNd ) {

			/* ������� �� ������� �������� */
			auto rdm = __remove_from_rt1_map( (*itNd) );

			if( getLst ) {
				if( rdm != rtl_roadmap_obj_t::null )
					lst.push_back( std::move( rdm ) );
				}
			}

		itNodesLst->second.clear();

		/* ������� ������ */
		_mapRT0_to_RT1.erase( itNodesLst );
		}

	/* ������� � ������ �������� */
	auto itWaitList = _waitImpersonateLinks.begin();
	while( itWaitList != _waitImpersonateLinks.end() ) {
		if( itWaitList->first.rt0() == nodeId || !itWaitList->second ) {
			if( itWaitList->second )
				close_link( std::move( itWaitList->second ), true );

			itWaitList = _waitImpersonateLinks.erase( itWaitList );
			}
		else {
			++itWaitList;
			}
		}

	return std::move( lst );
	}

void rt1_streams_table_t::__add_rt1_rt0_link( const rt0_node_rdm_t &nodeId,
	const rt1_streams_table_wait_key_t & identity ) {

	auto itLst = _mapRT0_to_RT1.find( nodeId );
	if( itLst == _mapRT0_to_RT1.end() ) {
		auto itIns = _mapRT0_to_RT1.insert( {nodeId, std::set<rt1_streams_table_wait_key_t>()} );
		if( !itIns.second )
			FATAL_ERROR_FWD(nullptr);
		itLst = itIns.first;
		}

	auto itObj = itLst->second.find( identity );
	if( itObj == itLst->second.end() ) {
		if( !itLst->second.insert( identity ).second )
			FATAL_ERROR_FWD(nullptr);
		}
	else {
		if( (*itObj) != identity ) {
			THROW_MPF_EXC_FWD(nullptr);
			}
		}
	}

void rt1_streams_table_t::__remove_from_rt1_rt0_link( const rt1_streams_table_wait_key_t & identity )noexcept {
	auto itLst = _mapRT0_to_RT1.begin();
	for( ; itLst != _mapRT0_to_RT1.end(); ++itLst ) {

		auto itObj = itLst->second.find( identity );
		if( itObj != itLst->second.end() )
			itLst->second.erase( itObj );
		}
	}

rt1_register_result_t rt1_streams_table_t::__insert( std::shared_ptr<base_stream_x_rt1_t> && s ) {

	if( !s )
		THROW_MPF_EXC_FWD(nullptr);

	const auto streamRDM = s->rt1_stream_table_key();
	if( is_null( streamRDM ) )
		THROW_MPF_EXC_FWD(nullptr);

	if(is_null( streamRDM.rdml().source_id()) || is_null( streamRDM.rdml().target_id()) || is_null( streamRDM.rt0() ) )
		FATAL_ERROR_FWD(nullptr);

	auto s2 = __remove_waiting_link( s->rt1_stream_table_key() );
	if( s2 && s2 != s )
		FATAL_ERROR_FWD(nullptr);

	auto it = _mapRT1.find( streamRDM );

	/* ������ ������ 1 � ����� ��������������� �� ��������������� */
	if( it == _mapRT1.end() ) {
		if( !_mapRT1.insert( {streamRDM, std::move( s )} ).second )
			FATAL_ERROR_FWD(nullptr);

		/* �������� � ������ ������������ */
		__add_rt1_rt0_link( streamRDM.rt0(), streamRDM );

		return rt1_register_result_t::success;
		}
	else {

		if( !it->second ) {

			/* �������� ������������ ������ */
			it->second = std::move( s );

			return rt1_register_result_t::success;
			}
		else {
			/* �������� ��������, ���, ��������, ������,
			����� ��������� ��� �������-"�������� �����������"(����������� ����, ���������������
			������� �������� ��������� - ����� ��������, ������� ����� �������� ��������� ������ �����������
			�� ������� ��������� )
			��� ������ ����������� ��������� �������� "��������� �����������". */

#ifdef CBL_MPF_TRACELEVEL_FAULTED_RT1
			std::string log;
			log += "rt1_streams_table_t::_insert(fail)=" + streamRDM.rdml().to_str();
			crm::file_logger_t::logging( log, 0, __CBL_FILEW__, __LINE__ );
#endif

			return rt1_register_result_t::already_exists;
			}
		}
	}

rt1_register_result_t rt1_streams_table_t::insert( std::shared_ptr<base_stream_x_rt1_t> && n ) {

	if( !canceled() ) {
		lck_scp_t lck( _tblLck );
		const auto result = __insert( std::move( n ) );

#ifdef CBL_OBJECTS_COUNTER_CHECKER
		__size_trace();
#endif

		return result;
		}
	else
		return rt1_register_result_t::canceled;
	}


/*! ����� ����, �� ������������������ �������� �������� */
std::shared_ptr<base_stream_x_rt1_t> rt1_streams_table_t::__find(
	const rt1_streams_table_wait_key_t & id ) noexcept {

	std::shared_ptr<base_stream_x_rt1_t> obj;

	auto it( _mapRT1.find( id ) );
	if( it != _mapRT1.end() ) {
		CBL_VERIFY( it->first == id );
		obj = it->second;
		}

	return obj;
	}

rtl_roadmap_obj_t rt1_streams_table_t::__remove_from_rt1_map( const rt1_streams_table_wait_key_t &id )noexcept {

	rtl_roadmap_obj_t result;

	auto it = _mapRT1.find( id );
	if( it != _mapRT1.end() ) {
		CBL_VERIFY( it->first == id );

		/* ������� ����� */
		auto rfi = it->second;
		if( rfi ) {
			result = rfi->msg_rdm();
			close_link( std::move( rfi ) );
			}

		/* ������� � ������� */
		_mapRT1.erase( it );
		}

	return std::move( result );
	}

rtl_roadmap_obj_t rt1_streams_table_t::__remove( const rt1_streams_table_wait_key_t & id )noexcept {

	auto result = __remove_from_rt1_map( id );
	if( !is_null( result ) ) {
		__remove_from_rt1_rt0_link( id );
		}

	auto waitIt = __remove_waiting_link( id );
	if( waitIt ) {
		if(is_null( result ) )
			result = waitIt->msg_rdm();

		close_link( std::move( waitIt ) );
		}

	return std::move( result );
	}

std::list<rtl_roadmap_obj_t> rt1_streams_table_t::__remove_all()noexcept {

	std::list<rtl_roadmap_obj_t> result;

	auto itPeer = _mapRT1.begin();
	while( itPeer != _mapRT1.end() ) {

		try {
			auto rfi = itPeer->second;
			if( rfi ) {
				result.push_back( rfi->msg_rdm() );
				close_link( std::move( rfi ), false );
				}
			}
		catch( const crm::dcf_exception_t & exc ) {
			ntf_hndl( exc );
			}
		catch( ... ) {
			ntf_hndl( CREATE_MPF_PTR_EXC_FWD(nullptr) );
			}

		++itPeer;
		}
	_mapRT1.clear();
	_mapRT0_to_RT1.clear();
	_waitImpersonateLinks.clear();

	return std::move( result );
	}

void rt1_streams_table_t::__remove_all_nol()noexcept {

	auto itPeer = _mapRT1.begin();
	while( itPeer != _mapRT1.end() ) {

		try {
			auto rfi = itPeer->second;
			if( rfi ) {
				close_link( std::move( rfi ), false );
				}
			}
		catch( const crm::dcf_exception_t & exc ) {
			ntf_hndl( exc );
			}
		catch( ... ) {
			ntf_hndl( CREATE_MPF_PTR_EXC_FWD(nullptr) );
			}

		++itPeer;
		}
	_mapRT1.clear();
	_mapRT0_to_RT1.clear();
	_waitImpersonateLinks.clear();

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	__size_trace();
#endif
	}

std::shared_ptr<base_stream_x_rt1_t> rt1_streams_table_t::find( const rt1_streams_table_wait_key_t & id ) noexcept {

	lck_scp_t lck( _tblLck );
	return __find( id );
	}

rtl_roadmap_obj_t rt1_streams_table_t::remove( const rt1_streams_table_wait_key_t & id )noexcept {

	lck_scp_t lck( _tblLck );
	auto obj = __remove( id );

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	CHECK_NO_EXCEPTION( __size_trace() );
#endif

	return std::move( obj );
	}

std::list<rtl_roadmap_obj_t> rt1_streams_table_t::remove()noexcept {

	lck_scp_t lck( _tblLck );
	return __remove_all();
	}

void rt1_streams_table_t::remove_by_rt0( const rt0_node_rdm_t &nodeId )noexcept {

	lck_scp_t lck( _tblLck );
	CHECK_NO_EXCEPTION( __remove_by_rt0( nodeId, false ) );

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	CHECK_NO_EXCEPTION( __size_trace() );
#endif
	}

rt1_streams_table_t::~rt1_streams_table_t() {
	CHECK_NO_EXCEPTION( __close_nol() );
	}

void rt1_streams_table_t::__close_nol() noexcept {
	_cancelf.store( true, std::memory_order::memory_order_release );
	CHECK_NO_EXCEPTION( __remove_all_nol() );
	}

std::list<rtl_roadmap_obj_t> rt1_streams_table_t::__close()noexcept {
	_cancelf.store( true, std::memory_order::memory_order_release );
	return __remove_all();
	}

std::list<rtl_roadmap_obj_t> rt1_streams_table_t::close()noexcept {
	lck_scp_t lck( _tblLck );
	return __close();
	}

rt1_streams_table_t::rt1_streams_table_t( std::weak_ptr<distributed_ctx_t> ctx_ )
	: _ctx( ctx_ ){}

void rt1_streams_table_t::ntf_hndl( std::unique_ptr<crm::dcf_exception_t> ntf ) noexcept {
	auto ctx_( _ctx.lock() );
	if( ctx_ )
		ctx_->exc_hndl( std::move( ntf ) );
	}

void rt1_streams_table_t::ntf_hndl( const crm::dcf_exception_t & exc )noexcept {
	auto ctx_( _ctx.lock() );
	if( ctx_ )
		ctx_->exc_hndl( exc );
	}

void rt1_streams_table_t::insert_waiting_link( rt1_streams_table_wait_key_t && key,
	std::shared_ptr<base_stream_x_rt1_t> s ) {

	lck_scp_t lck( _tblLck );
	__insert_waiting_link( std::move( key ), std::move( s ) );
	}

std::shared_ptr<base_stream_x_rt1_t> rt1_streams_table_t::get_waiting_link( const rt1_streams_table_wait_key_t & key )const noexcept {

	lck_scp_t lck( _tblLck );
	return __get_waiting_link( key );
	}

void rt1_streams_table_t::__insert_waiting_link( rt1_streams_table_wait_key_t && key,
	std::shared_ptr<base_stream_x_rt1_t> && s ) {

	auto itWaitList = _waitImpersonateLinks.find( key );
	if( itWaitList == _waitImpersonateLinks.end() ) {
		if( !_waitImpersonateLinks.insert( std::make_pair( std::move( key ), std::move( s ) ) ).second ) {
			FATAL_ERROR_FWD(nullptr);
			}
		}
	else {
		THROW_MPF_EXC_FWD(nullptr);
		}
	}

std::shared_ptr<base_stream_x_rt1_t>  rt1_streams_table_t::__remove_waiting_link( const rt1_streams_table_wait_key_t & key ) noexcept {
	auto itWaitList = _waitImpersonateLinks.find( key );
	if( itWaitList != _waitImpersonateLinks.end() ) {
		auto obj = std::move( itWaitList->second );
		_waitImpersonateLinks.erase( itWaitList );

		return std::move( obj );
		}
	else {
		return std::shared_ptr<base_stream_x_rt1_t>();
		}
	}

std::shared_ptr<base_stream_x_rt1_t> rt1_streams_table_t::__get_waiting_link( const rt1_streams_table_wait_key_t & key ) const noexcept {
	auto itWaitList = _waitImpersonateLinks.find( key );
	if( itWaitList != _waitImpersonateLinks.end() ) {
		return itWaitList->second;
		}
	else {
		return std::shared_ptr<base_stream_x_rt1_t>();
		}
	}

