#include <appkit/appkit.h>
#include "../rt1_ptbl.h"


using namespace crm;
using namespace crm::detail;


opeer_t rt1_xpeer_router_t::create_out_link(
	const identity_descriptor_t & thisId,
	const identity_descriptor_t & remoteId,
	local_object_create_connection_t && identityCtr,
	remote_object_check_identity_t && identityChecker,
	std::unique_ptr<i_input_messages_stock_t> && ctrInput,
	object_initialize_handler_t && initializerHndl,
	o_connection_hndl_t && cnHndl,
	o_disconnection_hndl_t && dcnHndl,
	rt1_endpoint_t && endpoint_,
	__event_handler_point_f && shndl,
	exceptions_suppress_checker_t && connectionExcChecker,
	bool keepAlive_ ) {

	CBL_VERIFY(!(endpoint_.address().empty() || endpoint_.port() == 0));

	std::weak_ptr<self_t> wthis( shared_from_this() );

	/* ���������� �������� ������� ���������� �����������. */
	auto closeHndl = [dcnHndl]( const out_peer_t::rdm_local_identity_t & desc, 
		closing_reason_t,
		std::shared_ptr<i_peer_statevalue_t> && stv ) {

		if( dcnHndl )
			dcnHndl(desc, std::move(stv));
		};

	auto streamCtr = [wthis, thisId, remoteId, endpoint = endpoint_]
	(detail::xpeer_out_connector_rt1_t::stream_connection_handler_f && hndl) {
		static_assert(!std::is_reference_v<decltype(endpoint)>, __FILE__);

		auto sptr( wthis.lock() );
		if( sptr ) {

			detail::ox_rt1_stream_t::create( sptr,
				sptr->ctx(),
				thisId,
				remoteId,
				endpoint,
				std::move( hndl ) );
			}
		else {
			THROW_EXC_FWD(nullptr);
			}
		};

	auto stvalueCtr = _stvalueCtr;
	auto hndl(std::make_unique<detail::xpeer_out_connector_rt1_t>(ctx(),
		shared_from_this(),
		thisId,
		remoteId,
		std::move(endpoint_),
		_gtwCtr,
		std::move( ctrInput ),
		std::move( initializerHndl ),
		std::move(cnHndl),
		std::move(stvalueCtr),
		std::move( shndl ),
		std::move(connectionExcChecker),
		std::move(streamCtr)));

	auto obj( rt1_connecto_t::launch( ctx(),
		std::move( hndl ),
		std::move( identityCtr ),
		std::move( identityChecker ),
		std::move( closeHndl ),
		async_space_t::ext,
		keepAlive_ ) );
	return opeer_t( std::move( obj ) );
	}


opeer_t rt1_xpeer_router_t::create_out_link(std::shared_ptr<rt1_connection_settings_t> && pCtr) {

	CBL_VERIFY(!(pCtr->remote_endpoint()->address().empty() || pCtr->remote_endpoint()->port() == 0));

	auto identityCtr = [pCtr] {
		return pCtr->local_object_identity();
		};

	auto identityChecker = [pCtr](const identity_value_t & idSet,
		std::unique_ptr<i_peer_remote_properties_t> & prop) {

		return pCtr->remote_object_identity_validate(idSet, prop);
		};

		/* ���������� ��������� ������������� */
	auto initializeHndl = [pCtr]
		(crm::datagram_t && dt, object_initialize_result_guard_t && resultHndl) {

		pCtr->local_object_initialize(std::move(dt), std::move(resultHndl));
		};

		/* ���������� ������� ����������� � ��� */
	auto cnHndl = [pCtr](std::unique_ptr<dcf_exception_t> && exc, rt1_xpeer_router_t::in_peer_lnk_t && lnk) {
		pCtr->connection_handler(std::move(exc), std::move(lnk));
		};

		/* ���������� ������� ����������� � ��� */
	auto dcnHndl = [pCtr](const rt1_xpeer_router_t::xpeer_desc_t & desc, std::shared_ptr<i_peer_statevalue_t> && stv) {
		pCtr->disconnection_handler(desc, std::move(stv));
		};

	auto ev_hndl = [pCtr](std::unique_ptr<i_event_t> && ev) {
		pCtr->event_handler(std::move(ev));
		};

	auto connectionExcChecker = 
		[pCtr](detail::connection_stage_t stage, const std::unique_ptr<dcf_exception_t> & exc)noexcept{

		return pCtr->connnection_exception_suppress_checker(stage, exc);
		};

	CBL_VERIFY(!rt1_endpoint_t(*pCtr->remote_endpoint()).address().empty());
	return create_out_link(pCtr->this_id(),
		pCtr->remote_endpoint()->endpoint_id(),
		std::move(identityCtr),
		std::move(identityChecker),
		pCtr->input_stack(),
		std::move(initializeHndl),
		std::move(cnHndl),
		std::move(dcnHndl),
		rt1_endpoint_t(*pCtr->remote_endpoint()),
		__event_handler_point_f(std::move(ev_hndl), _ctx, async_space_t::ext),
		std::move(connectionExcChecker),
		pCtr->keep_alive());
	}





