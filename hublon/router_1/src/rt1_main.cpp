#include <appkit/appkit.h>
#include "../rt1_main.h"


using namespace crm;
using namespace crm::detail;


rt1_cntbl_t::~rt1_cntbl_t() {
	CHECK_NO_EXCEPTION( close() );
	}

void rt1_cntbl_t::close() {
	if( _cmdRouter )
		CHECK_NO_EXCEPTION( _cmdRouter->close() );
	if( _peerRouter )
		CHECK_NO_EXCEPTION( _peerRouter->close() );
	}

void rt1_cntbl_t::open() {
	if( _cmdRouter )
		_cmdRouter->start();
	if( _peerRouter )
		_peerRouter->open();
	}

//std::shared_ptr<rt1_cntbl_t> rt1_cntbl_t::__create(std::weak_ptr<distributed_ctx_t> ctx_,
//	const global_object_identity_t & thisId,
//	std::shared_ptr<detail::io_x_rt1_channel_t> channel,
//	connection_hndl_t && xhndl_Connection,
//	disconnection_hndl_t && xhndl_Disconnection,
//	local_object_create_connection_t && localIdCtr,
//	remote_object_check_identity_t && remoteIdValidator,
//	create_remote_object_initialize_list_t && remoteInitializer,
//	opeer_connection_exception_t && opeerFail,
//	canceled_f && cancelf,
//	ctr_gateway_rt1_t && gtwCtr,
//	std::unique_ptr<i_input_messages_stock_t> && instackCtr,
//	object_initialize_handler_t && outXIniHndl,
//	state_value_ctr_t && stvalCtr,
//	__event_handler_point_f && eh,
//	bool startNow) {
//
//	auto cmdrt = rt1_command_router_t::make(ctx_, channel, startNow);
//	auto prt = rt1_xpeer_router_t::create(ctx_,
//		thisId,
//		channel,
//		cmdrt,
//		std::move(xhndl_Connection),
//		std::move(xhndl_Disconnection),
//		std::move(localIdCtr),
//		std::move(remoteIdValidator),
//		std::move(remoteInitializer),
//		std::move(opeerFail),
//		std::move(cancelf),
//		std::move(gtwCtr),
//		std::move(instackCtr),
//		std::move(outXIniHndl),
//		std::move(stvalCtr),
//		std::move(eh),
//		startNow);
//
//	return std::make_shared<rt1_cntbl_t>(
//		ctx_,
//		cmdrt,
//		prt,
//		channel);
//	}

/*rt1_cntbl_t::rt1_cntbl_t(std::weak_ptr<distributed_ctx_t> wctx,
	std::shared_ptr<rt1_command_router_t> cmdrt,
	std::shared_ptr<rt1_xpeer_router_t> rtp,
	std::shared_ptr<io_x_rt1_channel_t> channel)
	: _ctx(wctx)
	, _cmdRouter(cmdrt)
	, _xchannel(channel)
	, _peerRouter(rtp) {}	*/

std::shared_ptr<default_binary_protocol_handler_t> rt1_cntbl_t::protocol()const noexcept {
	return _peerRouter->protocol();
	}

//opeer_t rt1_cntbl_t::__create_out_link(
//	const global_object_identity_t & thisId,
//	const global_object_identity_t & remoteId,
//	local_object_create_connection_t && identityCtr,
//	remote_object_check_identity_t && identityChecker,
//	std::unique_ptr<i_input_messages_stock_t> && ctrInput,
//	object_initialize_handler_t && initializerHndl,
//	o_connection_hndl_t && cnHndl,
//	o_disconnection_hndl_t && dcnHndl,
//	rt1_endpoint_t && endpoint,
//	__event_handler_point_f && shndl,
//	exceptions_suppress_checker_t && connectionExcChecker ) {
//
//	if( _peerRouter ) {
//		return _peerRouter->create_out_link(thisId,
//			remoteId,
//			std::move(identityCtr),
//			std::move(identityChecker),
//			std::move(ctrInput),
//			std::move(initializerHndl),
//			std::move(cnHndl),
//			std::move(dcnHndl),
//			std::move(endpoint),
//			std::move(shndl),
//			std::move(connectionExcChecker) );
//		}
//	else {
//		THROW_MPF_EXC_FWD(nullptr);
//		}
//	}


std::shared_ptr<distributed_ctx_t> rt1_cntbl_t::context()const noexcept {
	return _ctx.lock();
	}

