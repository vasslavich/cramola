#pragma once


#include <map>
#include <memory>
#include <appkit/export_internal_terms.h>
#include "./rt1_streamstbl.h"


namespace crm::detail {


class inbound_connection_state_t {
public:
	using accept_handler_t = std::function<void()>;
	using refuse_handler_t = std::function<void()>;

private:
	rt0_x_message_unit_t _desc;
	accept_handler_t _acceptHndl;
	refuse_handler_t _refuseHndl;
	std::function<void(std::unique_ptr<dcf_exception_t> && e)> _eh;

public:
	~inbound_connection_state_t();

	inbound_connection_state_t()noexcept;
	inbound_connection_state_t(rt0_x_message_unit_t && desc, 
		accept_handler_t && acceptHndl,
		std::function<void(std::unique_ptr<dcf_exception_t> && e)> && eh )noexcept;

	void bind_refuse_handler( refuse_handler_t && f )noexcept;

	inbound_connection_state_t(const inbound_connection_state_t &) = delete;
	inbound_connection_state_t& operator=(const inbound_connection_state_t &) = delete;

	inbound_connection_state_t(inbound_connection_state_t &&)noexcept;
	inbound_connection_state_t& operator=(inbound_connection_state_t &&)noexcept;

	void invoke_response_accept();
	void invoke_exception(std::unique_ptr<dcf_exception_t> && exc)noexcept;

	const rt0_x_message_t& desc()const noexcept;

	inbound_message_with_tail_exception_guard reset_invoke_exception_guard()noexcept;
	inbound_message_with_tail_exception_guard reset_invoke_exception_guard(
		inbound_message_with_tail_exception_guard && g )noexcept;
	};

class rt1_xpeer_router_t :
	public std::enable_shared_from_this<rt1_xpeer_router_t>,
	public i_rt1_main_router_t,
	public i_rt1_command_link_t{

private:
	typedef xpeer_out_rt1_t out_peer_t;
	typedef xpeer_in_rt1_t in_peer_t;
	typedef rt1_xpeer_router_t self_t;

public:
	typedef in_peer_t::link_t in_peer_lnk_t;
	typedef out_peer_t::link_t out_peer_lnk_t;
	typedef std::function<bool()> canceled_f;
	typedef std::function<void( const remote_connection_desc_t & opeer, std::unique_ptr<dcf_exception_t> && exc )> opeer_connection_exception_t;
	typedef std::function<void( ipeer_t && lnk, datagram_t && initializeResult )> connection_hndl_t;
	typedef std::function<void( const xpeer_desc_t & desc, std::shared_ptr<i_peer_statevalue_t> && stv )> disconnection_hndl_t;
	typedef std::function<void( std::unique_ptr<dcf_exception_t> && exc, out_peer_lnk_t && )> o_connection_hndl_t;
	typedef std::function<void( const xpeer_desc_t & desc, std::shared_ptr<i_peer_statevalue_t> && stv )> o_disconnection_hndl_t;
	typedef std::function<std::shared_ptr<i_peer_statevalue_t>()> state_value_ctr_t;
	typedef xpeer_out_connection_t<out_peer_t> ostream_rt0_x_t;
	typedef ostream_rt0_x_t::input_stack_t::value_type outcoming_connection_input_value_t;
	typedef ostream_rt0_x_t outcoming_connection_t;

private:
	static const std::chrono::milliseconds push2X_Timeout;

	std::weak_ptr<distributed_ctx_t> _ctx;
	std::shared_ptr<rt1_command_router_t> _commandRt;
	std::shared_ptr<io_x_rt1_channel_messages_t> _xChannel;

	/*! ������� ����� */
	local_object_id_t _lcid;
	rtl_roadmap_event_t _thisEvRdm;
	rtl_roadmap_obj_t _thisMsgRdm;

	std::unique_ptr<rt1_streams_table_t >_ipTbl;
	std::unique_ptr<rt1_streams_table_t> _opTbl;
	local_object_create_connection_t _localIdCtr;
	remote_object_check_identity_t _remoteIdValidator;
	create_remote_object_initialize_list_t _remoteInitializer;
	opeer_connection_exception_t _opeerConnectionFail;
	canceled_f _cancelExt;
	connection_hndl_t _xhndl_Connection;
	disconnection_hndl_t _xhndl_Disconnection;

	std::shared_ptr<default_binary_protocol_handler_t> _gtwCtr;
	std::shared_ptr<i_input_messages_stock_t> _instackCtr;
	object_initialize_handler_t _outXIniHndl;
	state_value_ctr_t _stvalueCtr;
	__event_handler_point_f _evHndl;
	std::thread _thChannelRead;
	std::atomic<bool> _cancelf{false};
	std::atomic<bool> _startf{false};

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	utility::__xobject_counter_logger_t<self_t> _xDbgCounter;
#endif

#ifdef CBL_ENABLE_TEST_ERROR_EMULATION_LEVEL_1
#ifdef CBL_ENABLE_TEST_ERROR_EMULATION_LATENCY
	std::map<address_descriptor_t, int> _ltEmTbl;
	std::mutex _ltEmLck;
	bool check_latency_connection( const address_descriptor_t & address );
#endif
#endif

	/*! �������� ����������� � ���� */
	void ntf_hndl( std::unique_ptr<dcf_exception_t> && ntf )noexcept;
	/*! �������� ���������� � ���� */
	void ntf_hndl( dcf_exception_t && exc )noexcept;
	/*! �������� ���������� � ���� */
	void ntf_hndl( const dcf_exception_t & exc )noexcept;

	const std::unique_ptr<rt1_streams_table_t >& iptbl()const noexcept;
	const std::unique_ptr<rt1_streams_table_t>& optbl()const noexcept;
	const std::unique_ptr<rt1_streams_table_t >& table( rtl_table_t t )const noexcept;

	std::shared_ptr<distributed_ctx_t> ctx()const noexcept;
	std::shared_ptr<io_x_rt1_channel_messages_t> x_channel()const noexcept;
	std::shared_ptr<const rt1_command_router_t> command_router()const noexcept final;
	std::shared_ptr<rt1_command_router_t> command_router()noexcept final;
	void start_x();
	void launch_x_read_packets();
	void join_x_read_packets()noexcept;
	void routing_message_processing(rt0_x_message_unit_t && env, invoke_context_trait ic);
	void routing_message(rt0_x_message_unit_t && env, invoke_context_trait ic);
	bool execute_this(rt0_x_message_unit_t && obj );

	void opeer_fail( const remote_connection_desc_t & opeer,
		std::unique_ptr<dcf_exception_t> && exc )noexcept final;

	void istream_register_there(inbound_connection_state_t && st,
		std::shared_ptr<ix_rt1_stream_t> && is );
	void event_incoming_connection(rt0_x_message_unit_t && cpck);
	void incoming_connection_start(inbound_connection_state_t && st )noexcept;
	void incoming_connection_response_begin( const rtl_roadmap_line_t & rdml,
		const address_descriptor_t & address );
	void response_exception( const rtl_roadmap_line_t & rdml,
		const address_descriptor_t & address,
		std::unique_ptr<dcf_exception_t> && ntf )noexcept;
	void response_exception( const rt0_node_rdm_t & rt0rdm,
		const address_descriptor_t & address,
		std::unique_ptr<dcf_exception_t> && ntf )noexcept;
	void handler_exception_RZi(const rtl_roadmap_obj_t & rdm,
		const address_descriptor_t & address,
		std::unique_ptr<dcf_exception_t> && ntf)noexcept;
	void handler_exception_RZi_L0(const outbound_message_desc_t & d,
		std::unique_ptr<dcf_exception_t> && e)noexcept;

	void incoming_connection_handshake_ok(in_peer_lnk_t && peer,
		syncdata_list_t && sl,
		std::unique_ptr<execution_bool_tail> && registerCompleted

#ifdef CBL_MPF_MESSAGE_TRACEWAY_KEYS_BY_ID
		, keycounter_trace::key_t ktid
#endif
	);

	void incoming_connection_initialize( in_peer_lnk_t && lnk,
		syncdata_list_t && sl,
		std::unique_ptr<execution_bool_tail> && registerCompleted

#ifdef CBL_MPF_MESSAGE_TRACEWAY_KEYS_BY_ID
		, keycounter_trace::key_t ktid
#endif
	);

	void incoming_connection_tail_register( in_peer_lnk_t && peer,
		isendrecv_datagram_t&& initializeResult,
		std::unique_ptr<execution_bool_tail> && registerCompleted );

	void event_from_rt0_incoming_closed( std::unique_ptr<i_rt0_command_t> && ev );
	void event_from_rt0_outcoming_opened( std::unique_ptr<i_rt0_command_t> && ev );
	void event_from_rt0_outcoming_closed( std::unique_ptr<i_rt0_command_t> && ev );
	void event_from_rt0_invalid_xpeer( std::unique_ptr<i_rt0_command_t> && ev );
	void event_from_rt0_outmessage_fault(std::unique_ptr<i_rt0_command_t> && ev);

	void execute_exthndl_connection( in_peer_lnk_t && peer_,
		isendrecv_datagram_t&& initializeResult,
		std::unique_ptr<execution_bool_tail> && registerCompleted )noexcept;
	void execute_exthndl_disconnection( const in_peer_lnk_t::xpeer_desc_t & desc,
		std::shared_ptr<i_peer_statevalue_t> && stv )noexcept;

	void register_rt0_slot();
	void register_rt0( const rtl_roadmap_obj_t  & rdm, __error_handler_point_f && rh );
	void register_rt0( std::list<rtx_command_type_t> && evlst, 
		const rtl_roadmap_event_t  & evrdm,
		std::weak_ptr<i_rt1_router_targetlink_t> hndl,
		__error_handler_point_f && rh );

	void __unregister_rt0_slot()noexcept;
	void unregister_rt0_slot()noexcept;
	void unregister_rt0( const rtl_roadmap_event_t & rdm )noexcept;
	void unregister_rt0( const rtl_roadmap_obj_t & rdm, unregister_message_track_reason_t r )noexcept;
	void unregister_rt0( const rt1_endpoint_t & targetEP,
		const identity_descriptor_t & targetId,
		const source_object_key_t & srcKey )noexcept;

	void register_events_slot( std::list<rtx_command_type_t> && evlst,
		std::weak_ptr<i_rt1_router_targetlink_t> hndl,
		__error_handler_point_f && rh ) final;
	void register_messages_slot( const rtl_roadmap_obj_t & objrdm,
		std::weak_ptr<i_rt1_router_targetlink_t> hndl,
		__error_handler_point_f && rh ) final;
	void __register_message_slot_there( std::weak_ptr<base_stream_x_rt1_t> && hndl );

	void __unregister_events_slot_invoke( const rtl_roadmap_event_t & evrdm )noexcept;
	void unregister_events_slot( const rtl_roadmap_event_t & evrdm )noexcept final;

	void __unregister_messages_slot_there( const rtl_roadmap_obj_t & rdmMsg,
		std::function<void( rtl_roadmap_obj_t )> && rh )noexcept;
	void __unregister_messages_slot_invoke( const rtl_roadmap_obj_t & rdms,
		unregister_message_track_reason_t r )noexcept;
	void unregister_messages_slot( const rtl_roadmap_obj_t & rdms, 
		unregister_message_track_reason_t r )noexcept final;

	void unregister_remote_object_slot( const rt1_endpoint_t & targetEP,
		const identity_descriptor_t & targetId,
		const source_object_key_t & srcKey )noexcept final;

	void push2rt( rt1_x_message_t && msg ) final;
	void forward_to_rt0( rt1_x_message_t && obj );

	template<typename O,
		typename std::enable_if_t < is_out_message_requirements_v<O>, int> = 0>
		void forward_to_rt0(const rt0_node_rdm_t & rt0, O && o) {

		if (_gtwCtr) {
			if (auto emitter = _gtwCtr->create_emitter()) {
				CBL_MPF_KEYTRACE_SET(trait_at_keytrace_set(o), 441);

				rtl_roadmap_line_t rtl_;
				rtl_.points = make_message(rt0, o);

				if (is_sendrecv_cycle(o)) {
					CBL_MPF_KEYTRACE_SET(trait_at_keytrace_set(o), 442);
					CBL_VERIFY_SNDRCV_KEY_SPIN_TAG(o);

					CBL_VERIFY(o.type().fl_report() || !is_null(o.address().source_subscriber_id()));

					outbound_message_desc_t mdesc(o);
					CBL_VERIFY_SNDRCV_KEY_SPIN_TAG(mdesc);

					auto mresponseGuard = o.reset_invoke_exception_guard();
					auto mbinary = emitter->push(std::forward<O>(o));
					auto munit = outbound_message_unit_t::make("forward_to_rt0-1", std::move(mbinary), std::move(mdesc), std::move(mresponseGuard));
					auto mx1 = rt1_x_message_t::make(std::move(rtl_), std::move(munit));

					CBL_VERIFY_SNDRCV_KEY_SPIN_TAG(mx1);
					CBL_VERIFY(CBL_IS_SNDRCV_KEY_SPIN_TAGGED_ON(mx1));

					forward_to_rt0(std::move(mx1));
					}
				else {
					CBL_MPF_KEYTRACE_SET(trait_at_keytrace_set(o), 443);

					CBL_VERIFY(!(o.type().fl_report() || !is_null(o.address().source_subscriber_id())));

					auto mbinary = emitter->push(std::forward<O>(o));
					auto munit = outbound_message_unit_t::make("forward_to_rt0-2", std::move(mbinary));
					auto mx1 = rt1_x_message_t::make(std::move(rtl_), std::move(munit));

					CBL_VERIFY(CBL_IS_SNDRCV_KEY_SPIN_TAGGED_OFF(mx1));

					forward_to_rt0(std::move(mx1));

					if (o.reset_invoke_exception_guard()) {
						FATAL_ERROR_FWD(nullptr);
						}
					}
				}
			else {
				THROW_EXC_FWD(nullptr);
				}
			}
		else {
			THROW_MPF_EXC_FWD(nullptr);
			}
		}

	bool canceled()const noexcept;

	void event_handler( std::unique_ptr<i_event_t> && ev )noexcept;

	void ctr( connection_hndl_t && xhndl_Connection,
		disconnection_hndl_t && xhndl_Disconnection,
		local_object_create_connection_t && localIdCtr,
		remote_object_check_identity_t && remoteIdValidator,
		create_remote_object_initialize_list_t && remoteInitializer,
		opeer_connection_exception_t && opeerFail,
		canceled_f && cancelf,
		std::shared_ptr<default_binary_protocol_handler_t> && gtwCtr,
		std::shared_ptr<i_input_messages_stock_t> && instackCtr,
		object_initialize_handler_t && outXIniHndl,
		state_value_ctr_t && stvalCtr,
		__event_handler_point_f && eh );

	void rt1_router_hndl( std::unique_ptr<i_command_from_rt02rt1_t> && response) final;
	const local_command_identity_t& local_object_command_id()const noexcept final;
	void unroute_command_link() noexcept final;
	bool is_once()const noexcept final;

	static std::shared_ptr<self_t> create(std::shared_ptr<io_x_rt1_channel_messages_t> xchannel,
		std::shared_ptr<rt1_command_router_t> cmdrt,
		std::shared_ptr<rt1_hub_settings_t> && ph,
		bool startNow = false);

	opeer_t create_out_link(std::shared_ptr<rt1_connection_settings_t>&& pCtr);
	void async_connect_tr1(std::shared_ptr<rt1_connection_settings_t>&& ps);

public:
	rt1_xpeer_router_t( std::weak_ptr<distributed_ctx_t> ctx_,
		const identity_descriptor_t & thisId,
		std::shared_ptr<io_x_rt1_channel_messages_t> xchannel,
		std::shared_ptr<rt1_command_router_t> cmdrt );

	~rt1_xpeer_router_t();
	void close()noexcept;

	std::shared_ptr<default_binary_protocol_handler_t> protocol()const noexcept;
	const rtl_roadmap_event_t& ev_rdm()const  noexcept final;
	const rtl_roadmap_obj_t& msg_rdm()const noexcept;

	template<typename Settings,
		typename std::enable_if_t<is_hub_settings_rt1_v<Settings>, int> = 0>
		static std::shared_ptr<self_t> create(std::shared_ptr<io_x_rt1_channel_messages_t> xchannel,
			std::shared_ptr<rt1_command_router_t> cmdrt,
			Settings && s,
			bool startNow = false) {

		return create(xchannel,
			cmdrt,
			std::make_shared<Settings>(std::forward<Settings>(s)),
			startNow);
		}

	void open();

private:
	typedef xpeer_out_connection_t<xpeer_out_rt1_t> rt1_connecto_t;

public:
	opeer_t create_out_link(
		const identity_descriptor_t & thisId,
		const identity_descriptor_t & remoteId,
		local_object_create_connection_t && identityCtr,
		remote_object_check_identity_t && identityChecker,
		std::unique_ptr<i_input_messages_stock_t> && ctrInput,
		object_initialize_handler_t && initializerHndl,
		o_connection_hndl_t && cnHndl,
		o_disconnection_hndl_t && dcnHndl,
		rt1_endpoint_t && endpoint,
		__event_handler_point_f && shndl,
		exceptions_suppress_checker_t && connectionExcChecker,
		bool keepAlive = false);

	template<typename Settings,
		typename std::enable_if_t< is_outbound_connection_settings_rt1_v<Settings>, int> = 0>
	opeer_t create_out_link(Settings && s) {
		return create_out_link(std::make_shared<Settings>(std::forward<Settings>(s)));
		}

	template<typename Settings,
		typename std::enable_if_t< is_outbound_connection_settings_rt1_v<Settings>, int> = 0>
	void async_connect_tr1(Settings && s) {
		async_connect_tr1(std::make_shared<Settings>(std::forward<Settings>(s)));
		}

	constexpr rtl_level_t level()const noexcept { return rtl_level_t::l1; }
	};
}
