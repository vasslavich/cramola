#pragma once


#include <appkit/appkit.h>
#include "./rt1_ptbl.h"


namespace crm::detail{


class rt1_cntbl_t {
public:
	typedef rt1_xpeer_router_t::opeer_connection_exception_t opeer_connection_exception_t;
	typedef rt1_xpeer_router_t::input_value_t input_message_ptr_t;
	typedef rt1_xpeer_router_t::xpeer_desc_t xpeer_desc_t;
	typedef rt1_xpeer_router_t::input_queue_t input_queue_t;
	typedef rt1_xpeer_router_t::message_stack_t message_stack_t;
	typedef rt1_xpeer_router_t::in_peer_lnk_t in_peer_lnk_t;
	typedef rt1_xpeer_router_t::out_peer_lnk_t out_peer_lnk_t;
	typedef rt1_xpeer_router_t::canceled_f canceled_f;
	typedef rt1_xpeer_router_t::connection_hndl_t connection_hndl_t;
	typedef rt1_xpeer_router_t::disconnection_hndl_t disconnection_hndl_t;
	typedef rt1_xpeer_router_t::o_connection_hndl_t o_connection_hndl_t;
	typedef rt1_xpeer_router_t::o_disconnection_hndl_t o_disconnection_hndl_t;
	typedef rt1_xpeer_router_t::state_value_ctr_t state_value_ctr_t;
	typedef rt1_xpeer_router_t::outcoming_connection_t::handler_t outcoming_connection_handler_t;

private:
	std::weak_ptr<distributed_ctx_t> _ctx;
	std::shared_ptr<rt1_command_router_t> _cmdRouter;
	std::shared_ptr<io_x_rt1_channel_t> _xchannel;
	std::shared_ptr<rt1_xpeer_router_t> _peerRouter;

public:
	template<typename Settings,
		typename std::enable_if_t<is_hub_settings_rt1_v<Settings>, int> = 0>
	rt1_cntbl_t(std::shared_ptr<io_x_rt1_channel_t> channel, Settings && s_ )
		: _ctx(s_.ctx())
		, _cmdRouter(rt1_command_router_t::make(s_.ctx(), channel))
		, _xchannel(channel)
		, _peerRouter(rt1_xpeer_router_t::create(channel, _cmdRouter, std::forward<Settings>(s_))) {}

	~rt1_cntbl_t();
	void close();
	void open();

	template<typename Settings,
		typename std::enable_if_t<is_hub_settings_rt1_v<Settings>, int> = 0>
	static std::shared_ptr<rt1_cntbl_t> create(std::shared_ptr<io_x_rt1_channel_t> channel,
		Settings&& s) {

		return std::make_shared<rt1_cntbl_t>(channel, std::forward<Settings>(s));
		}

	template<typename Settings,
		typename std::enable_if_t< is_outbound_connection_settings_rt1_v<Settings>, int> = 0>
	opeer_t create_out_link(Settings &&s) {
		if(_peerRouter) {
			return _peerRouter->create_out_link(std::forward<Settings>(s));
			}
		else {
			THROW_MPF_EXC_FWD(nullptr);
			}
		}

	template<typename Settings,
		typename std::enable_if_t< is_outbound_connection_settings_rt1_v<Settings>, int> = 0>
	void async_connect_tr1(std::unique_ptr<rt1_connection_settings_t>&&) {
		if(_peerRouter) {
			_peerRouter->async_connect_tr1(std::move(s));
			}
		else {
			THROW_MPF_EXC_FWD(nullptr);
			}
		}

	std::shared_ptr<distributed_ctx_t> context()const noexcept;
	std::shared_ptr<default_binary_protocol_handler_t> protocol()const noexcept;
	};
}

