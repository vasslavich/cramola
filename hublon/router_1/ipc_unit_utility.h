#pragma once

#include <appkit/appkit.h>
#include "../xchannel/smp/shmem_reg.h"
#include "../xchannel/i_xchannel.h"

namespace crm{


struct ipc_unit_trait{
	using exception_handler = std::function<void( std::unique_ptr<dcf_exception_t> && )>;
	};

struct ipc_unit_start_options{
	static constexpr size_t default_message_mem_size = CRM_MB;
	static constexpr size_t default_command_mem_size = CRM_MB;

	size_t message_mem_size{ default_message_mem_size };
	size_t command_mem_size{ default_command_mem_size };
	};

class ipc_unit_options : public rt1_hub_settings_t {
	detail::xchannel_shmem_host_args _hostChannelA;
	detail::xchannel_shmem_hub_options _channelOpt;

public:
	ipc_unit_options(const detail::xchannel_shmem_hub_options & channelOpt);

	void set_host_channel( const detail::xchannel_shmem_host_args & hostChA );
	const detail::xchannel_shmem_host_args& host_channel()const noexcept;

	const detail::xchannel_shmem_hub_options& channel_options()const noexcept;
	};

template<typename T, typename = std::void_t<>>
struct is_ipc_unit_options : std::false_type {};

template<typename T>
struct is_ipc_unit_options<T, std::void_t<
	std::enable_if_t<std::is_base_of_v<ipc_unit_options, T>>
	>> : std::true_type{};

template<typename T>
constexpr bool is_ipc_unit_options_v = is_ipc_unit_options<T>::value;
}
