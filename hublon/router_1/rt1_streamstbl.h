#pragma once


#include <map>
#include <memory>
#include <list>
#include <appkit/export_internal_terms.h>


namespace crm::detail{


class rt1_streams_table_t {
public:
	typedef std::function<void( std::unique_ptr<crm::dcf_exception_t> ntf )> notification_handler_t;

private:
	typedef std::mutex lck_t;
	typedef std::unique_lock<lck_t> lck_scp_t;

	/*! ����� ������������ �������� ������ 1 ����� ������ 0 */
	std::map<rt0_node_rdm_t/* ������������� ���� ������ 0 */,
		std::set<rt1_streams_table_wait_key_t/* ������������� ������� ������ 1 */ >> _mapRT0_to_RT1;
	/*! ����� ����� */
	std::map<rt1_streams_table_wait_key_t, std::shared_ptr<base_stream_x_rt1_t>> _mapRT1;
	std::map<rt1_streams_table_wait_key_t, std::shared_ptr<base_stream_x_rt1_t>> _waitImpersonateLinks;
	mutable lck_t _tblLck;
	std::weak_ptr<distributed_ctx_t> _ctx;
	std::atomic<bool> _cancelf{false};

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	mutable crm::utility::__xvalue_counter_logger_t<> __sizeTraceer = __FILE_LINE__;
	mutable crm::utility::__xvalue_counter_logger_t<> __sizeTraceerWaitLinks = __FILE_LINE__;
	crm::utility::__xobject_counter_logger_t<rt1_streams_table_t> _xDbgCounter;

	size_t __rt0_rt1_size()const;
	size_t __rt1_size()const;
	size_t __wait_links_size()const;

	void __size_trace()const noexcept;
#endif

	bool canceled()const noexcept;

	/*! �������� ����������� � ���� */
	void ntf_hndl( std::unique_ptr<dcf_exception_t> ntf )noexcept;
	/*! �������� ���������� � ���� */
	void ntf_hndl( const dcf_exception_t & exc )noexcept;

	void __add_rt1_rt0_link( const rt0_node_rdm_t &nodeId,
		const rt1_streams_table_wait_key_t & identity );
	void __remove_from_rt1_rt0_link( const rt1_streams_table_wait_key_t & identity )noexcept;

	/*! ������� ������ �� ����� �������� ������ 1 */
	rtl_roadmap_obj_t __remove_from_rt1_map( const rt1_streams_table_wait_key_t &id )noexcept;
	/*! ������� ��� ������� ������ 1 ��������� � ����� ������ 0, �� ������������������ �������� */
	std::list<rtl_roadmap_obj_t> __remove_by_rt0( const rt0_node_rdm_t &nodeId, bool getList )noexcept;

	void close_link( std::shared_ptr<base_stream_x_rt1_t> && obj, bool lazy = true )noexcept;
	rt1_register_result_t __insert( std::shared_ptr<base_stream_x_rt1_t> && );
	rtl_roadmap_obj_t __remove( const rt1_streams_table_wait_key_t & id )noexcept;
	std::shared_ptr<base_stream_x_rt1_t> __find( const rt1_streams_table_wait_key_t & rdm )noexcept;
	std::list<rtl_roadmap_obj_t> __remove_all()noexcept;
	void __remove_all_nol()noexcept;

	void __close_nol()noexcept;
	std::list<rtl_roadmap_obj_t> __close()noexcept;

	void __insert_waiting_link( rt1_streams_table_wait_key_t && key,
		std::shared_ptr<base_stream_x_rt1_t> && );
	std::shared_ptr<base_stream_x_rt1_t> __get_waiting_link( const rt1_streams_table_wait_key_t & key )const noexcept;
	std::shared_ptr<base_stream_x_rt1_t>  __remove_waiting_link( const rt1_streams_table_wait_key_t & key )noexcept;

public:
	rt1_streams_table_t( std::weak_ptr<distributed_ctx_t> ctx );
	~rt1_streams_table_t();
	std::list<rtl_roadmap_obj_t> close()noexcept;

	void insert_waiting_link( rt1_streams_table_wait_key_t && key,
		std::shared_ptr<base_stream_x_rt1_t> );
	std::shared_ptr<base_stream_x_rt1_t> get_waiting_link( const rt1_streams_table_wait_key_t & key )const noexcept;

	rt1_register_result_t insert( std::shared_ptr<base_stream_x_rt1_t> && );
	std::shared_ptr<base_stream_x_rt1_t> find( const rt1_streams_table_wait_key_t & rdm )noexcept;

	/*! ������� ������ ������ 1 */
	rtl_roadmap_obj_t remove( const rt1_streams_table_wait_key_t & id )noexcept;

	/*! ������� ��� ������� ������ 1 ��������� � ����� ������ 0 */
	void remove_by_rt0( const rt0_node_rdm_t &nodeId )noexcept;

	/*! ������� ��� ������� ������ 1 */
	std::list<rtl_roadmap_obj_t> remove()noexcept;
	};
}

