#pragma once


#include <map>
#include <memory>
#include <queue>
#include <appkit/appkit.h>
#include "./rt0_itbl.h"
#include "./rt0_otbl.h"
#include <appkit/xpeer/rt0_end_opeer.h>
#include <appkit/xpeer/rt0_outbound_cnsettings.h>



namespace crm::detail{


class rt0_x_channels_table_t;
class rt0_cntbl_t;


struct command_response_guard_t {
private:
	std::weak_ptr<rt0_cntbl_t> _rt;
	channel_identity_t _chid;
	rtl_roadmap_event_t _srcId;
	local_command_identity_t _sourceCmdId;
	rtx_command_type_t _srcCommandType = rtx_command_type_t::undefined;
	std::atomic<bool> _invokedf = false;
	bool _response = true;

#ifdef CBL_MPF_OBJECTS_COUNTER_CHECKER_HOST_COMMAND_REVERSE_GUARDER
	crm::utility::__xobject_counter_logger_t<command_response_guard_t, 1> _xDbgCounter;
#endif

	void send_abandoned()noexcept;

public:
	command_response_guard_t( std::weak_ptr<rt0_cntbl_t> rt,
		const channel_identity_t & chid,
		const rtl_roadmap_event_t & srcId,
		const local_command_identity_t& sourceCmdId,
		rtx_command_type_t sourceCmdType,
		bool response )noexcept;

	template<typename _TxCommand,
		typename C = typename std::decay_t<_TxCommand>,
		typename std::enable_if_t<std::is_base_of_v<i_command_from_rt02rt1_t, C>&& std::is_final_v<C>, int> = 0>
		void invoke_cmd_to_rt1(_TxCommand && cmd)noexcept {
		
		if (_response) {
			bool inf = false;
			if (_invokedf.compare_exchange_strong(inf, true)) {
				if (auto rt_ = _rt.lock()) {

					if (is_null(cmd.command_target_receiver_id()))
						cmd.set_target_receiver_id(_srcId);

					if (!rt_->send_cmd_target_to_rt1(_chid,
						is_null(cmd.source_command_id()) ? _sourceCmdId : cmd.source_command_id(),
						std::forward<_TxCommand>(cmd))) {

						rt_->channel_command_fault(_chid);
						}
					}
				}
			}
		}

	void invoke_exc( std::unique_ptr<dcf_exception_t> && exc )noexcept;

	const channel_identity_t & channel_id()const noexcept;
	const rtl_roadmap_event_t & target_object_id()const noexcept;
	const local_command_identity_t& source_cmd_id()const noexcept;
	bool with_response()const noexcept;
	~command_response_guard_t();
	void cancel()noexcept;
	};


class rt0_cntbl_t final :
	public i_rt0_main_router_input_t,
	public i_rt0_main_router_registrator_t,
	public i_rt0_xchannel_maker_t,
	public std::enable_shared_from_this<rt0_cntbl_t>{

public:
	typedef i_xpeer_rt0_t::xpeer_desc_t xpeer_desc_t;
	typedef __rt0_ipeer_table_t in_table_t;
	typedef in_table_t::peer_link_t ipeer_link_t;
	typedef ipeer_link_t::input_value_t ipeer_input_value_t;
	typedef ipeer_link_t::output_value_t ipeer_output_value_t;

	typedef __rt0_opeer_table_t out_table_t;
	typedef out_table_t::peer_link_t opeer_link_t;
	typedef opeer_link_t::output_value_t opeer_output_value_t;
	typedef opeer_link_t::input_value_t opeer_input_value_t;

	typedef i_xpeer_rt0_t::ctr_0_input_queue_t ctr_input_queue_t;
	typedef rt0_cntbl_t self_t;

	typedef in_table_t::connection_hndl_t connection_hndl_t;
	typedef in_table_t::disconnection_hndl_t disconnection_hndl_t;

	typedef std::function<std::shared_ptr<i_peer_statevalue_t>()> state_value_ctr_t;
	using event_handler_f = std::function<void(std::unique_ptr<i_event_t>&& ev)>;

private:
	std::weak_ptr<distributed_ctx_t> _ctx;
	std::shared_ptr<i_message_queue_t<rt0_x_command_pack_t>> _cmdqFromRT1;
	std::thread _cmdqFromRT1_Exe;
	std::shared_ptr<in_table_t> _ipTbl;
	std::shared_ptr<out_table_t> _opTbl;
	std::shared_ptr<rt0_x_channels_table_t> _rtX;
	identity_descriptor_t _thisId;
	std::shared_ptr<i_message_input_t<rt0_X_item_t>> _inputRt0;
	ctr_input_queue_t _inQCtr;
	event_handler_f _evHndl;
	connection_hndl_t _xhndl_Connection;
	disconnection_hndl_t _xhndl_Disconnection;
	state_value_ctr_t _stvalCtr;
	std::shared_ptr<default_binary_protocol_handler_t> _protocol;
	std::unique_ptr<i_endpoint_t> _ep;
	std::shared_ptr< rt0_hub_settings_t> _settings;
	std::atomic<bool> _closedf = false;

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	crm::utility::__xobject_counter_logger_t<self_t> _xDbgCounter;
#endif

	std::shared_ptr<i_message_input_t<rt0_X_item_t>> input_rt0()const noexcept;
	std::shared_ptr<rt0_x_channels_table_t> rt()const noexcept;
	std::shared_ptr<out_table_t> otbl()const noexcept;
	std::shared_ptr<in_table_t> itbl()const noexcept;


	void __evhndl_from_peer_invoke(rt0_x_message_unit_t && msg );
	void __event_handler(std::unique_ptr<i_event_t>&& ev);

public:
	std::shared_ptr<distributed_ctx_t> ctx()const noexcept;

	template<typename _TxCommand,
		typename C = typename std::decay_t<_TxCommand>,
		typename std::enable_if_t<std::is_base_of_v<i_command_from_rt02rt1_t, C>&& std::is_final_v<C>, int> = 0>
		[[nodiscard]]
	bool send_cmd_target_to_rt1(const channel_identity_t& chid,
		const local_command_identity_t& sourceCmdId,
		_TxCommand&& cmd2rt1)const noexcept {

		CBL_VERIFY(!is_null(cmd2rt1.command_target_receiver_id()) || is_broadcast_command(cmd2rt1));

		cmd2rt1.set_source_command_id(sourceCmdId);

		if (auto rt_ = rt()) {
			return rt_->notify_rt1(chid, std::forward<_TxCommand>(cmd2rt1));
			}
		else {
			return false;
			}
		}

	void channel_command_fault( const channel_identity_t & chid )noexcept;

private:
	struct addlink_reverse_scope_t {
		using handler_f = std::function<void( std::unique_ptr<dcf_exception_t> && exc )>;

	private:
		std::weak_ptr<distributed_ctx_t> _ctx;
		handler_f _h;
		std::atomic<bool> _if = false;

		void __invoke( bool dctr, std::unique_ptr<dcf_exception_t> && exc )noexcept;
		void exchndl( std::unique_ptr<dcf_exception_t> && )noexcept;

	public:
		~addlink_reverse_scope_t();
		addlink_reverse_scope_t( std::weak_ptr<distributed_ctx_t>, handler_f && h );
		void invoke( std::unique_ptr<dcf_exception_t> && exc )noexcept;
		};

	void execute_exthndl_connection( ipeer_link_t && peer,
		crm::datagram_t && initializeResult )noexcept;
	void execute_exthndl_disconnection( const xpeer_desc_t & desc,
		std::shared_ptr<i_peer_statevalue_t> && stv )noexcept;

	void hndl_ipeer_connection( ipeer_link_t && peer,
		crm::datagram_t && initializeResult );
	void hndl_ipeer_disconnection( const xpeer_desc_t & desc,
		std::shared_ptr<i_peer_statevalue_t> && stval )noexcept;
	void hndl_opeer_connection( std::unique_ptr<dcf_exception_t> && exc, 
		opeer_link_t && peer )noexcept;
	void hndl_opeer_disconnection( const xpeer_desc_t & desc, 
		std::shared_ptr<i_peer_statevalue_t> && stval )noexcept;

	void execute_command_rt1_x(std::unique_ptr<i_command_from_rt1_t> && event);

	/*! ������������� ��������� ��������� � ������ 1 �� ���� ������ 0 */
	template<typename O,
		typename std::enable_if_t < is_out_message_requirements_v<O>, int> = 0>
	void routing_from_x_to_ioenv(const rt0_node_rdm_t & nodeId,
		O && o) {

		/* ��� ������� */
		auto tableType = nodeId.direction();
		if(tableType == rtl_table_t::in) {
			auto itbl_(itbl());
			if(itbl_)
				itbl_->push(nodeId, std::forward<O>(o));
			}
		else if(tableType == rtl_table_t::out) {
			auto otbl_(otbl());
			if(otbl_)
				otbl_->push(nodeId, std::forward<O>(o));
			}
		else {
			std::string log;
			log += "RT0:table:routing_from_x_to_ioenv:invalid table type="
				+ o.destination_id().to_str() + ":" + o.source_id().to_str()
				+ ":" + cvt2string(tableType);
			FATAL_ERROR_FWD(std::move(log));
			}
		}

public:
	void unbind_out_connection( std::vector<source_object_key_t> && sourceKeys )noexcept final;
	void unbind_out_connection( const connection_key_t & ck )noexcept final;

	template<typename THandler,
		typename std::enable_if_t<(
			std::is_invocable_v<THandler, std::unique_ptr<i_rt0_peer_handler_t>>), int> = 0>
	void bind_subscribe2peer(const std::unique_ptr<i_rt0_cmd_addlink_t> & ev, const rtl_roadmap_obj_t & rdm, THandler && h) {
		if(ev->msg_rdm().table() == rtl_table_t::in) {
			auto itb = itbl();
			if(itb) {
				itb->subscribe2peer(rdm, std::forward<THandler>(h));
				}
			else {
				THROW_MPF_EXC_FWD(nullptr);
				}
			}
		else if(ev->msg_rdm().table() == rtl_table_t::out) {
			auto otb = otbl();
			if(otb) {
				otb->subscribe2peer(rdm, std::forward<THandler>(h));
				}
			else {
				THROW_MPF_EXC_FWD(nullptr);
				}
			}
		else if(ev->msg_rdm().table() == rtl_table_t::rt1_router) {
			h(std::make_unique<__rt0_router_handler_t>());
			}
		else {
			FATAL_ERROR_FWD(nullptr);
			}
		}

private:
	void event_addlink_messages( const std::unique_ptr <i_rt0_cmd_addlink_t> & ev_,
		std::unique_ptr<addlink_reverse_scope_t> && tailHndl );
	void event_addlink_events( const std::unique_ptr <i_rt0_cmd_addlink_t> & ev_,
		std::unique_ptr<addlink_reverse_scope_t> && tailHndl );
	void event_addlink_messages_events( const std::unique_ptr <i_rt0_cmd_addlink_t> & ev_,
		std::unique_ptr<addlink_reverse_scope_t> && tailHndl );
	/*! ����������� ������� ������������� */
	void event_addlink( std::unique_ptr <i_command_from_rt1_t> && ev_,
		std::shared_ptr<command_response_guard_t> reverseInvoker );

	/*! ���������� ������� ������ ���������� ����������� ������ 0. */
	void subscribe_for_rt0( std::unique_ptr<i_command_from_rt1_t> && ev_,
		std::shared_ptr<command_response_guard_t> reverseInvoker );

	void event_sublink_remote_disconnect( const std::unique_ptr<i_rt0_cmd_sublink_t> & ev_,
		rtl_rdm_keypair_t && disconnectPair )noexcept;
	rtmessate_entry_t::unregister_f make_remote_unregister(const std::unique_ptr<i_rt0_cmd_addlink_t> & ev_ );
	void event_sublink_unbind_connection( rtl_table_t t, const source_object_key_t & srcKey )noexcept;
	/*! ������ ����������� ������� ������������� */
	void event_sublink( std::unique_ptr<i_command_from_rt1_t> && ev_,
		std::shared_ptr<command_response_guard_t> reverseInvoker );
	/*! ������ ����������� ������� ������������� */
	void event_sublink_source_key( std::unique_ptr<i_command_from_rt1_t> && ev_,
		std::shared_ptr<command_response_guard_t> reverseInvoker );

	void excmd_unbind_connection( std::unique_ptr<i_command_from_rt1_t> && cmd,
		std::shared_ptr<command_response_guard_t> reverseInvoker );

	void xchannel_module_initialize(std::unique_ptr<i_command_from_rt1_t> && cmd,
		std::shared_ptr<command_response_guard_t> reverseInvoker);

	/*! ��������� ��������� �� ����(������� 0) - �������� �� ��������� ������� (1)*/
	void evhndl_from_peer(rt0_x_message_unit_t && msg) final;

	/*! ��������� ��������� �� ���� � ���� ������ */
	void evhndl_form_peer_rt_id(rt0_X_item_t&& obj ) final;

	rt0_cntbl_t( std::weak_ptr<distributed_ctx_t> ctx_,
		std::shared_ptr< rt0_hub_settings_t> settings,
		const identity_descriptor_t & thisId_,
		std::weak_ptr<i_message_input_t<rt0_X_item_t>> inputRt0,
		event_handler_f && eh,
		connection_hndl_t && xhndl_Connection,
		disconnection_hndl_t && xhndl_Disconnection,
		state_value_ctr_t && stvalCtr,
		std::unique_ptr<i_endpoint_t> && endp,
		std::shared_ptr<default_binary_protocol_handler_t> protocol_ );

	void ctr(std::shared_ptr <i_xpeer_source_factory_t> iFactory, 
		local_object_create_connection_t && localIdCtr,
		remote_object_check_identity_t && remoteIdChecker,	
		create_remote_object_initialize_list_t && remoteInzr,
		object_initialize_handler_t && outXInit,
		ctr_input_queue_t && inQCtr );

	bool closed()const noexcept;

	void ntf_hndl(std::unique_ptr<dcf_exception_t> && e)const noexcept;
	static std::shared_ptr<self_t> create(std::shared_ptr< rt0_hub_settings_t> s);

	opeer_l0 create_out_link(std::shared_ptr<rt0_connection_settings_t>);

	static std::shared_ptr<self_t> create(std::weak_ptr<distributed_ctx_t> ctx_,
		std::shared_ptr< rt0_hub_settings_t> settings,
		const identity_descriptor_t& thisId,
		std::shared_ptr<i_xpeer_source_factory_t> iFactory,
		local_object_create_connection_t&& localIdCtr,
		remote_object_check_identity_t&& remoteIdChecker,
		create_remote_object_initialize_list_t&& remoteInzr,
		object_initialize_handler_t&& outXInit,
		std::unique_ptr<i_endpoint_t>&& inEndPnt,
		std::shared_ptr<i_message_input_t<rt0_X_item_t>> inputRt0,
		connection_hndl_t&& xhndl_Connection,
		disconnection_hndl_t&& xhndl_Disconnection,
		state_value_ctr_t&& stvalCtr,
		std::shared_ptr<default_binary_protocol_handler_t> protocol,
		event_handler_f&& evhndl,
		ctr_input_queue_t&& inQCtr = nullptr);

public:
	[[nodiscard]]
	addon_push_event_result_t push_command_pack_to_rt0(rt0_x_command_pack_t && cmd)noexcept final;

	[[nodiscard]]
	addon_push_event_result_t push_message(rt1_x_message_data_t && m0_)noexcept;

	template<typename _SettingsBased,
		typename Settings = std::decay_t<_SettingsBased>,
		typename std::enable_if_t<std::is_base_of_v<rt0_hub_settings_t, Settings>, int> = 0>
	static auto create(_SettingsBased&& s) {
		return create(std::make_shared< Settings>(std::forward<_SettingsBased>(s)));
		}

	~rt0_cntbl_t();

	std::shared_ptr<default_binary_protocol_handler_t> protocol()const noexcept;

	const i_endpoint_t& endpoint()const noexcept;
	const identity_descriptor_t& this_id()const noexcept;

	/*! ����������� ������ � ������� ������������� */
	void register_rt1_channel(std::weak_ptr<io_rt0_x_channel_t> channel);
	void unregister_rt1_channel(const channel_info_t& chId)noexcept;

	std::shared_ptr<i_rt0_channel_route_t> rt0_channel_router()const;
	std::unique_ptr<i_rt0_peer_handler_t> find_peer( const rt0_node_rdm_t & rdm )const noexcept final;

	template<typename S,
		typename std::enable_if_t< is_outbound_connection_settings_rt0_v<S>, int> = 0>
	opeer_l0 create_out_link(S&& s) {
		return create_out_link(std::make_unique<S>(std::forward<S>(s)));
		}
	
	void close()noexcept;
	void start();
	};
}

