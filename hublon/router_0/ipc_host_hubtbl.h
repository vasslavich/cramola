#pragma once


#include <appkit/appkit.h>
#include "../xchannel/smp/shmem_hub.h"
#include "./ipc_hub_watcher.h"


namespace crm::detail{


class ipc_host_map_of_hubs{
	using lck_t = std::mutex;
	using lck_scp_t = std::unique_lock<lck_t>;

	std::map<channel_identity_t, std::unique_ptr<xchannel_shmem_hub_watcher>> _tbl;
	mutable lck_t _lck;

public:
	void insert( std::shared_ptr<xchannel_shmem_hub> channel,
		std::function<void( rt1_x_message_data_t && m )> && mh,
		std::function<void( rt0_x_command_pack_t && c )> && ch,
		std::weak_ptr<distributed_ctx_t> ctx );

	void remove( const channel_identity_t & chid )noexcept;
	std::shared_ptr<xchannel_shmem_hub> find( const channel_identity_t & chid )const noexcept;

	void close()noexcept;
	};
}

