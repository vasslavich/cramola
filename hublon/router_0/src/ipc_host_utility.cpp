#include "../ipc_host_utility.h"

using namespace crm;
using namespace crm::detail;

static_assert(std::is_base_of_v<i_endpoint_t, crm::detail::tcp::tcp_endpoint_t>, __FILE_LINE__);

ipc_host_options::ipc_host_options(std::shared_ptr<distributed_ctx_t> c,
	const identity_descriptor_t& thisId,
	crm::detail::tcp::tcp_endpoint_t&& ep)
	: rt0_hub_settings_t{ c,
		thisId,
		std::move(ep),
		c->queue_manager().tcreate_queue<message_stamp_t>(__FILE_LINE__,
			queue_options_t::message_stack_node) } {}

void ipc_host_options::set_channel_options( detail::xchannel_shmem_host_args && channelOpt ){
	_channelOpt = channelOpt;
	}

const detail::xchannel_shmem_host_args& ipc_host_options::channel_options()const noexcept{
	return _channelOpt;
	}

