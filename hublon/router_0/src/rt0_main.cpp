#include <appkit/appkit.h>
#include "../rt0_main.h"
#include "../../xchannel/smp/shmem_hub.h"
#include "../../xchannel/rt0_x_chrt.h"
#include "../../terms/internal_terms.h"


using namespace crm;
using namespace crm::detail;


void rt0_cntbl_t::ntf_hndl(std::unique_ptr<dcf_exception_t> && e)const noexcept {
	if(auto c = ctx()) {
		c->exc_hndl(std::move(e));
		}
	}

std::shared_ptr<distributed_ctx_t> rt0_cntbl_t::ctx()const noexcept {
	return _ctx.lock();
	}

std::shared_ptr<i_message_input_t<rt0_X_item_t>> rt0_cntbl_t::input_rt0()const noexcept {
	return _inputRt0;
	}

std::shared_ptr<rt0_x_channels_table_t> rt0_cntbl_t::rt()const  noexcept {
	return std::atomic_load_explicit(&_rtX, std::memory_order::memory_order_acquire);
	}

std::shared_ptr<rt0_cntbl_t::out_table_t> rt0_cntbl_t::otbl()const noexcept {
	return std::atomic_load_explicit(&_opTbl, std::memory_order::memory_order_acquire);
	}

std::shared_ptr<rt0_cntbl_t::in_table_t> rt0_cntbl_t::itbl()const noexcept {
	return std::atomic_load_explicit(&_ipTbl, std::memory_order::memory_order_acquire);
	}

addon_push_event_result_t rt0_cntbl_t::push_command_pack_to_rt0( rt0_x_command_pack_t && cmd )noexcept {
	CBL_NO_EXCEPTION_BEGIN

	_cmdqFromRT1->push(std::move(cmd));
	return addon_push_event_result_t::enum_type::success;

	CBL_NO_EXCEPTION_END(ctx())

		return addon_push_event_result_t::enum_type::exception;
	}

addon_push_event_result_t rt0_cntbl_t::push_message(rt1_x_message_data_t && m0_)noexcept {
	if (auto prx = rt()) {
		return prx->push_to_rt0_xpeer_XBN(std::move(m0_));
		}
	else {
		return addon_push_event_result_t::enum_type::exception;
		}
	}

#ifdef CBL_MAIN_VALIDATE_ON
static void check_response_address(const address_descriptor_t& address, const rtl_roadmap_obj_t& sourceObjectRDM) {
	CBL_VERIFY(address.session() == sourceObjectRDM.session());
	CBL_VERIFY(address.source_id() == sourceObjectRDM.source_id());
	CBL_VERIFY(address.destination_id() == sourceObjectRDM.target_id());
	CBL_VERIFY(is_sendrecv_request(address));
	}
#else
static void check_response_address(const address_descriptor_t&, const rtl_roadmap_obj_t&) {}
#endif

static void set_response_address( i_iol_ohandler_t & response,
	const address_descriptor_t & address, const rtl_roadmap_obj_t & sourceObjectRDM ) {

	check_response_address(address, sourceObjectRDM);
	response.set_echo_params( address );
	}

void rt0_cntbl_t::__evhndl_from_peer_invoke(rt0_x_message_unit_t && msg ) {
	auto rt_( rt() );
	if( rt_ ) {

		const auto packageType = msg.unit().package().type().type();
		const auto sourceAddress = msg.unit().package().address();
		const auto sourceRdm = msg.unit().rdm().points;
		const auto isResponseWait = is_sendrecv_request(sourceAddress);

		std::weak_ptr<self_t> wptr( shared_from_this() );
		auto disconnectHndl = [wptr, packageType, sourceAddress, sourceRdm] {

			auto sptr( wptr.lock() );
			if( sptr ) {

#ifdef CBL_MPF_TRACELEVEL_FAULTED_RT0
				std::string log;
				log += "RT0:evhndl_from_peer:rt1 target not found=";
				log += cvt2string( packageType );
				log += ":" + sourceAddress.to_string();
				log += ":" + sourceRdm.to_str();
				crm::file_logger_t::logging( log, 0, __CBL_FILEW__, __LINE__ );
#endif

				if( packageType != iol_types_t::st_disconnection ) {

					/* ��������� */
					odisconnection_t odiscn{};
					set_response_address( odiscn, sourceAddress, sourceRdm );

					/* �������� � ���� ������ 0 */
					sptr->routing_from_x_to_ioenv( sourceRdm.rt0(), std::move( odiscn ) );
					}
				}
			};

		auto disconnectSend = [this, isResponseWait, &disconnectHndl] {
			if( isResponseWait ) {
				auto ctx_( ctx() );
				if( ctx_ ) {
					ctx_->launch_async( __FILE_LINE__, std::move( disconnectHndl ) );
					}
				}
			};

		auto exceptionHndl = [wptr, packageType, sourceAddress, sourceRdm]( std::unique_ptr<dcf_exception_t> && ntf ) {
			auto sptr( wptr.lock() );
			if( sptr ) {

				/* ��� */
				sptr->ntf_hndl( ntf->dcpy() );

				/* �������� ��������� */
				if( packageType != iol_types_t::st_disconnection ) {

					/* ��������� */
					onotification_t msg{};
					msg.set_ntf( std::move( ntf ) );

					/* ����� �������� */
					set_response_address( msg, sourceAddress, sourceRdm );

					/* �������� � ���� ������ 0 */
					sptr->routing_from_x_to_ioenv( sourceRdm.rt0(), std::move( msg ) );
					}
				}
			};

		auto exceptionSend = [this, isResponseWait, &exceptionHndl]( std::unique_ptr<dcf_exception_t> && ntf ) {
			if( isResponseWait ) {
				auto ctx_( ctx() );
				if( ctx_ ) {
					ctx_->launch_async( __FILE_LINE__, std::move( exceptionHndl ), std::move( ntf ) );
					}
				}
			};

		try {

#pragma message( CBL_VAR_NAME_VALUE(CBL_BUILD_VAR_SNDRCV_REVERSE_GUARD, 41))
			rt_->push_to_rt1( std::move( msg ) );
			}
		catch( const not_found_exception_t & exc0 ) {

			/* ��������� ������� ���������� ��� ���������� ������� */
			disconnectSend();

			/* ��� */
			ntf_hndl( exc0.dcpy() );
			}
		catch( const dcf_exception_t & exc1 ) {
			exceptionSend( exc1.dcpy() );
			}
		catch( const std::exception & exc2 ) {
			exceptionSend( CREATE_PTR_EXC_FWD( exc2 ) );
			}
		catch( ... ) {
			exceptionSend( CREATE_PTR_EXC_FWD(nullptr) );
			}
		}
	else {
		THROW_MPF_EXC_FWD(nullptr);
		}
	}

void rt0_cntbl_t::evhndl_from_peer(rt0_x_message_unit_t && msg) {

	CBL_VERIFY(msg.unit().rdm().points.rt0().direction() == msg.unit().rdm().points.table());
	__evhndl_from_peer_invoke( std::move( msg ) );
	}

void rt0_cntbl_t::evhndl_form_peer_rt_id(rt0_X_item_t&& obj) {
	if(auto q = input_rt0()) {
		q->push(std::move(obj));
		}
	else {
		FATAL_ERROR_FWD(nullptr);
		}
	}

rt0_cntbl_t::rt0_cntbl_t(std::weak_ptr<distributed_ctx_t> ctx_,
	std::shared_ptr< rt0_hub_settings_t> settings,
	const identity_descriptor_t & thisId_,
	std::weak_ptr<i_message_input_t<rt0_X_item_t>> inputRt0,
	event_handler_f && eh,
	connection_hndl_t && xhndl_Connection,
	disconnection_hndl_t && xhndl_Disconnection,
	state_value_ctr_t && stvCtr,
	std::unique_ptr<i_endpoint_t> && iep,
	std::shared_ptr<default_binary_protocol_handler_t> protocol_)
	: _ctx(ctx_)
	, _thisId( thisId_ )
	, _inputRt0(inputRt0) 
	, _evHndl(std::move(eh))
	, _xhndl_Connection( std::move( xhndl_Connection ) )
	, _xhndl_Disconnection( std::move( xhndl_Disconnection))
	, _stvalCtr(std::move(stvCtr ))
	, _protocol(std::move(protocol_))
	, _ep(std::move(iep))
	, _settings(std::move(settings)){}

void rt0_cntbl_t::ctr(std::shared_ptr <i_xpeer_source_factory_t> iFactory,
	local_object_create_connection_t && localIdCtr,
	remote_object_check_identity_t && remoteIdChecker,
	create_remote_object_initialize_list_t && remoteInzr,
	object_initialize_handler_t && outXInit,
	ctr_input_queue_t && inQCtr ) {

	using cmd_queue_type = std::decay_t<decltype(ctx()->queue_manager().
		tcreate_queue<rt0_x_command_pack_t>(__FILE_LINE__, queue_options_t::message_stack_node))>;

	std::weak_ptr<self_t> wthis(shared_from_this());

	_cmdqFromRT1 = std::make_shared<cmd_queue_type>(CHECK_PTR(ctx())->queue_manager().
		tcreate_queue<rt0_x_command_pack_t>(
			__FILE_LINE__, 
			queue_options_t::message_stack_node));

	_cmdqFromRT1_Exe = std::thread([this]{

		static const std::chrono::seconds waitTimeout{ 2 };

		try {
			rt0_x_command_pack_t c;
			while(!closed()) {
				if(_cmdqFromRT1->pop(c, waitTimeout)) {
					execute_command_rt1_x(command_cast<i_command_from_rt1_t>(c.extract(ctx())));
					}
				}
			}
		catch(const dcf_exception_t & e0) {
			FATAL_ERROR_FWD(e0);
			}
		catch(const std::exception & e1) {
			FATAL_ERROR_FWD(e1);
			}
		catch(...) {
			FATAL_ERROR_FWD(nullptr);
			}
		});

	auto cancelHndl = [wthis]() {
		auto sthis(wthis.lock());
		if(sthis)
			return sthis->closed();
		else
			return true;
		};

	auto ipeerConnectHndl = [wthis]( ipeer_link_t && ipeer,
		crm::datagram_t && initializeResult ) {

		auto sthis( wthis.lock() );
		if( sthis ) {
			sthis->hndl_ipeer_connection( std::move( ipeer ), std::move( initializeResult ) );
			}
		};

	auto ipeerDisconnectHndl = [wthis]( const xpeer_desc_t & desc, std::shared_ptr<i_peer_statevalue_t> && stv ) {
		auto sthis( wthis.lock() );
		if( sthis )
			sthis->hndl_ipeer_disconnection(desc, std::move(stv));
		};

	auto stvalctr = _stvalCtr;
	__event_handler_point_f eh(_evHndl, ctx(), async_space_t::ext );

	auto ipTbl = in_table_t::create( ctx(),
		this_id(),
		std::move( cancelHndl ),
		local_object_create_connection_t( localIdCtr ),
		remote_object_check_identity_t( remoteIdChecker ),
		std::move( remoteInzr),
		shared_from_this(),
		iFactory,
		std::move(ipeerConnectHndl),
		std::move(ipeerDisconnectHndl),
		_protocol,
		std::move( inQCtr ),
		std::move(stvalctr),
		_ep,
		std::move(eh));
	auto oldIpTbl(std::atomic_exchange(&_ipTbl, std::move(ipTbl)));

	auto opTbl = out_table_t::create(ctx(),
		this_id(),
		shared_from_this(),
		std::move(localIdCtr),
		std::move(remoteIdChecker),
		_protocol,
		std::move(outXInit),
		iFactory);
	auto oldOpTbl(std::atomic_exchange(&_opTbl, std::move(opTbl)));

	auto chnMap = rt0_x_channels_table_t::make(ctx(), wthis);
	auto oldRtX = std::atomic_exchange(&_rtX, std::move(chnMap));

	if(oldIpTbl)
		oldIpTbl->close();
	if(oldOpTbl)
		oldOpTbl->close();
	if(oldRtX)
		oldRtX->close();
	}

bool rt0_cntbl_t::closed()const noexcept {
	return _closedf.load(std::memory_order::memory_order_acquire);
	}

std::shared_ptr<rt0_cntbl_t> rt0_cntbl_t::create(std::weak_ptr<distributed_ctx_t> ctx_,
	std::shared_ptr< rt0_hub_settings_t> settings,
	const identity_descriptor_t & thisId,
	std::shared_ptr<i_xpeer_source_factory_t> iFactory,
	local_object_create_connection_t && localIdCtr,
	remote_object_check_identity_t && remoteIdChecker,
	create_remote_object_initialize_list_t && remoteInzr,
	object_initialize_handler_t && outXInit,
	std::unique_ptr<i_endpoint_t> && inEndPnt,
	std::shared_ptr<i_message_input_t<rt0_X_item_t>> inputRt0,
	connection_hndl_t && xhndl_Connection,
	disconnection_hndl_t && xhndl_Disconnection,
	state_value_ctr_t && stvCtr,
	std::shared_ptr<default_binary_protocol_handler_t> protocol_,
	event_handler_f && evhndl,
	ctr_input_queue_t && inQCtr ) {

	std::shared_ptr<self_t> obj(new self_t(ctx_, 
		std::move(settings),
		thisId, 
		inputRt0,
		std::move(evhndl),
		std::move( xhndl_Connection ),
		std::move( xhndl_Disconnection ),
		std::move(stvCtr ),
		std::move(inEndPnt),
		std::move(protocol_) ) );

	obj->ctr(std::move(iFactory),
		std::move(localIdCtr),
		std::move(remoteIdChecker),
		std::move(remoteInzr),
		std::move(outXInit),
		std::move( inQCtr ) );
	return std::move(obj);
	}

std::shared_ptr<rt0_cntbl_t::self_t> rt0_cntbl_t::create(std::shared_ptr< rt0_hub_settings_t> hsPtr) {

	auto localIdCtr = [hsPtr] {
		return hsPtr->local_object_identity();
		};

	auto remoteIdValidator = [hsPtr](const identity_value_t& id,
		std::unique_ptr<i_peer_remote_properties_t>& remoteProp) {

		return hsPtr->remote_object_identity_validate(id, remoteProp);
		};

	auto remoteObjInzr = [hsPtr](syncdata_list_t&& sl) {
		return hsPtr->remote_object_initialize(std::move(sl));
		};

		/* ���������� ��������� ������������� */
	auto initializeHndl = [hsPtr](crm::datagram_t&& dt, object_initialize_result_guard_t&& resultHndl) {
		hsPtr->local_object_initialize(std::move(dt), std::move(resultHndl));
		};

		/* ����������� ��������� ���������� ������ 0 */
	auto ipeerConnect = [hsPtr]
	(rt0_cntbl_t::ipeer_link_t&& lnk, crm::datagram_t&& dt) {

		hsPtr->ipeer_connection(std::move(lnk), std::move(dt));
		};

		/* ���������� ��������� ���������� ������ 0 */
	auto ipeerDisconnect = [hsPtr]
	(const rt0_cntbl_t::xpeer_desc_t& desc, std::shared_ptr<i_peer_statevalue_t>&& stval) {

		hsPtr->ipeer_disconnection(desc, std::move(stval));
		};

		/* ������� ������ 0 */
	auto ipeerEvents = [hsPtr](std::unique_ptr<i_event_t>&& ev) {
		hsPtr->event_handler(std::move(ev));
		};

	auto stvalCtr = [hsPtr] {
		return hsPtr->state_value();
		};

		/* �������� ������� */
	auto rt0 = create(hsPtr->ctx(),
		hsPtr,
		hsPtr->this_id(),
		hsPtr->ipeer_factory(),
		std::move(localIdCtr),
		std::move(remoteIdValidator),
		std::move(remoteObjInzr),
		std::move(initializeHndl),
		hsPtr->endpoint(),
		hsPtr->input_stack(),
		std::move(ipeerConnect),
		std::move(ipeerDisconnect),
		std::move(stvalCtr),
		hsPtr->protocol(),
		std::move(ipeerEvents));

	return std::move(rt0);
	}

opeer_l0 rt0_cntbl_t::create_out_link(std::shared_ptr<rt0_connection_settings_t> pCtr) {
	if (auto otbl_ = otbl()) {
		auto stvalctr = _stvalCtr;
		return otbl_->create_out_link(std::move(stvalctr), pCtr);
		}
	else {
		THROW_EXC_FWD(nullptr);
		}

	}
rt0_cntbl_t::~rt0_cntbl_t() {
	close();
	}

const identity_descriptor_t& rt0_cntbl_t::this_id()const noexcept {
	return _thisId;
	}

std::shared_ptr<i_rt0_channel_route_t> rt0_cntbl_t::rt0_channel_router()const {
	return std::dynamic_pointer_cast<i_rt0_channel_route_t>(rt());
	}

void rt0_cntbl_t::register_rt1_channel(std::weak_ptr<io_rt0_x_channel_t> channel) {
	if (auto rtx = rt()) {
		rtx->register_rt1_channel(channel);
		}
	else {
		THROW_EXC_FWD(nullptr);
		}
	}

void rt0_cntbl_t::unregister_rt1_channel(const channel_info_t& chId)noexcept {
	if (auto rtx = rt()) {
		rtx->unregister_rt1_channel(chId);
		}
	}

void rt0_cntbl_t::close()noexcept {

	bool closedf = false;
	if(_closedf.compare_exchange_strong(closedf, true)) {

		if(_cmdqFromRT1_Exe.joinable())
			_cmdqFromRT1_Exe.join();

		auto rt_(std::atomic_exchange(&_rtX, std::shared_ptr<rt0_x_channels_table_t>()));
		if(rt_)
			rt_->close();

		auto ipTbl(std::atomic_exchange(&_ipTbl, std::shared_ptr<in_table_t>()));
		auto opTbl(std::atomic_exchange(&_opTbl, std::shared_ptr<out_table_t >()));

		if(ipTbl)
			ipTbl->close();
		if(opTbl)
			opTbl->close();
		}
	}

void rt0_cntbl_t::unbind_out_connection( std::vector<source_object_key_t> && sourceKeys )noexcept {

	std::weak_ptr<out_table_t> wotbl( otbl() );
	auto otbl_( wotbl.lock() );
	if( otbl_ ) {
		otbl_->unbind_connection( std::move( sourceKeys ) );
		}
	}

void rt0_cntbl_t::unbind_out_connection( const connection_key_t & ck )noexcept {

	std::weak_ptr<out_table_t> wotbl( otbl() );
	auto otbl_( wotbl.lock() );
	if( otbl_ ) {
		otbl_->unbind_connection( ck );
		}
	}

std::shared_ptr<default_binary_protocol_handler_t> rt0_cntbl_t::protocol()const noexcept {
	return _protocol;
	}

const i_endpoint_t& rt0_cntbl_t::endpoint()const noexcept {
	if (_ep)
		return (*_ep);
	else {
		return rt1_endpoint_t::null;
		}
	}

std::unique_ptr<i_rt0_peer_handler_t> rt0_cntbl_t::find_peer( const rt0_node_rdm_t & rdm )const noexcept {
	if( rdm.direction() == rtl_table_t::in ) {
		auto it( itbl() );
		if( it )
			return it->find_peer( rdm );
		else
			return nullptr;
		}
	else if( rdm.direction() == rtl_table_t::out ) {
		auto ot( otbl() );
		if( ot )
			return ot->find_peer( rdm );
		else
			return nullptr;
		}
	else
		return nullptr;
	}

void rt0_cntbl_t::channel_command_fault( const channel_identity_t & chid )noexcept{
	if( auto c = ctx() ){
		if( auto rtx = rt() ){
			c->launch_async( __FILE_LINE__, [rtx, chid]{
				rtx->channel_command_fault( chid );
				} );
			}
		}
	}

void rt0_cntbl_t::start() {
	if(auto c = ctx()) {
		c->start();
		}

	if (_settings) {
		_settings->run();
		}
	}

i_rt0_main_router_registrator_t::~i_rt0_main_router_registrator_t() {}
i_rt0_main_router_input_t::~i_rt0_main_router_input_t() {}
