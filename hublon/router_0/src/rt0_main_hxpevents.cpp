#include "../rt0_main.h"
#include "../../xchannel/rt0_x_chrt.h"


using namespace crm;
using namespace crm::detail;


void rt0_cntbl_t::hndl_ipeer_connection( ipeer_link_t && peer, 
	crm::datagram_t && initializeResult ) {

	CBL_VERIFY( peer.this_id() == this_id() || is_null( peer.this_id() ) );

	CBL_MPF_TRACELEVEL_EVENT_RT0_CONNECTION_TRACE("router:inbound connection:open:"
		+ peer.connection_key().to_string()
		+ ":s=" + peer.remote_id().to_str()
		+ ":t=" + peer.this_id().to_str()
		+ ":session=" + peer.desc().session().object_hash().to_str());

	auto rt_( rt() );
	if( rt_ ){
		rt_->add_rt0( peer.desc() );

		/* ����� ������������ ����������� ��������� ����������� */
		execute_exthndl_connection( std::move( peer ), std::move( initializeResult ) );
		}
	else{
		THROW_MPF_EXC_FWD(nullptr);
		}
	}

void rt0_cntbl_t::hndl_ipeer_disconnection( const xpeer_desc_t & desc, std::shared_ptr<i_peer_statevalue_t> && stv ) noexcept {

	CBL_MPF_TRACELEVEL_EVENT_RT0_CONNECTION_TRACE("router:inbound connection:closed:"
		+ desc.connection_key().to_string()
		+ ":s=" + desc.node_id().to_str()
		+ ":t=" + this_id().to_str()
		+ ":session=" + desc.session().object_hash().to_str());

	auto rt_( rt() );
	if( rt_ ){

		/* �������� ���� ��������� �������� ������ 1, ������� ���������������� ����� ������
		������ ������ 0. */
		rt_->unsubscribe_by_node( desc );

		/* ����� ������������ ����������� ��������� ���������� */
		execute_exthndl_disconnection( desc, std::move( stv ) );
		}
	}

void rt0_cntbl_t::hndl_opeer_connection( std::unique_ptr<dcf_exception_t> && exc, opeer_link_t && peer )noexcept {

	CBL_MPF_TRACELEVEL_EVENT_RT0_CONNECTION_TRACE("router:outbound connection:open:"
		+ peer.connection_key().to_string()
		+ ":s=" + peer.remote_id().to_str()
		+ ":t=" + peer.this_id().to_str()
		+ ":session=" + peer.desc().session().object_hash().to_str()
		+ ":exc=" + (exc ? cvt(exc->msg()) : ""));

	auto rt_( rt() );
	if( rt_ ){

		if( !exc && peer.is_opened() ){
			rt_->add_rt0( peer.desc() );
			}
		}
	}

void rt0_cntbl_t::hndl_opeer_disconnection( const xpeer_desc_t & desc, std::shared_ptr<i_peer_statevalue_t> && /*stv*/ )noexcept {

	CBL_MPF_TRACELEVEL_EVENT_RT0_CONNECTION_TRACE("router:outbound connection:closed:"
		+ desc.connection_key().to_string()
		+ ":s=" + desc.node_id().to_str()
		+ ":t=" + this_id().to_str()
		+ ":session=" + desc.session().object_hash().to_str());

	auto rt_( rt() );
	if( rt_ ) {

		/* �������� ���� ��������� �������� ������ 1, ������� ���������������� ����� ������
		������ ������ 0. */
		rt_->unsubscribe_by_node( desc );
		}
	}

void rt0_cntbl_t::execute_command_rt1_x( std::unique_ptr<i_command_from_rt1_t> && event ) {
	if( event ) {

		const auto channelId = event->channel_id();
		const auto sourceCmdObjId = event->command_source_emitter_rdm();
		const auto sourceCmdId = event->command_id();

		CBL_VERIFY( !is_null( channelId ) || is_with_response( event ) );

		auto reverseGuard = is_with_response( event )
			? std::make_shared<command_response_guard_t>( shared_from_this(), channelId, sourceCmdObjId, sourceCmdId, event->type(), true )
			: nullptr;
		try {

			switch( event->type() ) {
				case rtx_command_type_t::out_connection_req:
					CBL_VERIFY( reverseGuard->with_response() );
					subscribe_for_rt0( std::move( event ), reverseGuard );
					break;

				case rtx_command_type_t::reg_link_rdm:
					CBL_VERIFY( reverseGuard->with_response() );
					event_addlink( std::move( event ), reverseGuard );
					break;

				case rtx_command_type_t::unreg_link_rdm:
					event_sublink( std::move( event ), reverseGuard );
					break;

				case rtx_command_type_t::unreg_link_source_key:
					event_sublink_source_key( std::move( event ), std::move( reverseGuard ) );
					break;

				case rtx_command_type_t::unbind_connection:
					excmd_unbind_connection( std::move( event ), std::move( reverseGuard ) );
					break;

				case rtx_command_type_t::xchannel_rt1_initial:
					xchannel_module_initialize(std::move(event), std::move(reverseGuard));
					break;

				default:
					reverseGuard->invoke_exc( CREATE_MPF_PTR_EXC_FWD(nullptr) );
				}
			}
		catch( const dcf_exception_t & exc0 ) {
			if( reverseGuard )
				reverseGuard->invoke_exc( exc0.dcpy() );
			}
		catch( const std::exception & exc1 ) {
			if( reverseGuard )
				reverseGuard->invoke_exc( CREATE_MPF_PTR_EXC_FWD( exc1 ) );
			}
		catch( ... ) {
			if( reverseGuard )
				reverseGuard->invoke_exc( CREATE_MPF_PTR_EXC_FWD(nullptr) );
			}
		}
	}

std::unique_ptr<odisconnection_t> make_disconnection(const std::unique_ptr<i_rt0_cmd_sublink_t> & ev_)noexcept {

	if(ev_) {
		auto odisc(std::make_unique<odisconnection_t>());

		odisc->set_destination_id(ev_->msg_rdm().source_id());
		odisc->set_source_id(ev_->msg_rdm().target_id());
		odisc->set_session(ev_->msg_rdm().session());
		odisc->set_level(rtl_level_t::l1);

		return odisc;
		}
	else {
		return nullptr;
		}
	}


odisconnection_t make_disconnection(const rtl_roadmap_obj_t & rdm)noexcept{

	odisconnection_t odisc{};

	odisc.set_destination_id(rdm.source_id());
	odisc.set_source_id(rdm.target_id());
	odisc.set_session(rdm.session());
	odisc.set_level(rtl_level_t::l1);

	return odisc;
	}


/*! ���������� ������� ������ ���������� ����������� ������ 0. */
void rt0_cntbl_t::subscribe_for_rt0( std::unique_ptr<i_command_from_rt1_t> && ev_,
	std::shared_ptr<command_response_guard_t> reverseInvoker ) {

	auto evCnn( command_cast<i_rt0_cmd_reqconnection_t>(std::move( ev_ )) );
	if( evCnn ) {
		auto otbl_( otbl() );
		if( otbl_ ) {

			auto const responseObjId = evCnn->command_source_emitter_rdm();
			auto const sourceCmdId = evCnn->command_id();
			auto const channelId = evCnn->channel_id();
			
			if( is_null( responseObjId ) || is_null( sourceCmdId ) || is_null( channelId ) ) {
				THROW_MPF_EXC_FWD(nullptr);
				}

			std::weak_ptr<self_t> wthis( shared_from_this() );
			std::shared_ptr<__rt0_opeer_table_t::ostream_lnk_t> plnk;
			if( !( plnk = otbl_->exists( evCnn->source_key() ) ) ) {

				/* ���������� ������� - ��������� ����������� ������� */
				auto postConnHndl = [wthis]( std::unique_ptr<dcf_exception_t> && exc, opeer_link_t && olink ) {
					auto sthis( wthis.lock() );
					if( sthis ) {
						sthis->hndl_opeer_connection( std::move(exc), std::move( olink ) );
						}
					};

				/* ���������� ������� - ��������� ����������� ������� */
				auto closeHndl = [wthis]
					( const opeer_link_t::rdm_local_identity_t & id, closing_reason_t, std::shared_ptr<i_peer_statevalue_t> && stv ) {

					auto sthis( wthis.lock() );
					if( sthis )
						sthis->hndl_opeer_disconnection( id, std::move( stv ) );
					};

				auto excHndl = [reverseInvoker, timeout_ = std::chrono::system_clock::now() + evCnn->timeout()]
				(crm::detail::connection_stage_t, const std::unique_ptr<crm::dcf_exception_t>& e)noexcept {
					if (auto pTimeoutExc = dynamic_cast<const outbound_exception_t*>(e.get())) {
						if (std::chrono::system_clock::now() > timeout_) {
							if (reverseInvoker) {
								reverseInvoker->invoke_exc(e->dcpy());
								}
							return false;
							}
						else {
							return true;
							}
						}
					else {
						return true;
						}
					};

				auto stvalctr = _stvalCtr;
				auto cr = otbl_->create_connection( evCnn->source_key(),
					std::move( postConnHndl ),
					std::move( closeHndl ),
					std::move( stvalctr ),
					std::move(excHndl),
					__event_handler_point_f(_evHndl, ctx(), async_space_t::ext));

				plnk = cr.first;
				}
			
			if( plnk ) {

				auto subscriberEventConnection = _address_event_t::make( 
					responseObjId.lcid(),
					channelId,
					event_type_t::st_connected,
					evCnn->level() );

				/* ���������� ������� - ��������� ����������� ������� */
				auto postConnHndl = [wthis, reverseInvoker, subscriberEventConnection]
					( std::unique_ptr<dcf_exception_t> && exc, opeer_link_t && peer, bool subscribed ) {

					if( reverseInvoker ) {
						if( !exc && peer.is_opened() && !is_null( peer.desc() ) ) {
							CBL_VERIFY( peer.state() >= remote_object_state_t::impersonated );

							reverseInvoker->invoke_cmd_to_rt1( i_rt0_cmd_outcoming_oppened_t(
								peer.desc(),
								reverseInvoker->target_object_id(),
								rt1_endpoint_t::make_remote( peer.address(), peer.port(), peer.id() ),
								subscribed ? subscriberEventConnection : _address_event_t::null ) );
							}
						else {
							reverseInvoker->invoke_exc( exc ? std::move( exc ) : CREATE_MPF_PTR_EXC_FWD( L"invalid peer" ) );
							}
						}
					};

				auto stvalctr = _stvalCtr;
				otbl_->bind_connection( evCnn->source_key(),
					std::make_unique<__rt0_opeer_table_t::subscribe_event_connection_t>( 
						"rt0_cntbl_t::subscribe_for_rt0-1",
						ctx(), 
						subscriberEventConnection, 
						std::move( postConnHndl ) ) );
				}
			else {
				THROW_MPF_EXC_FWD(nullptr);
				}
			}
		else {
			THROW_MPF_EXC_FWD(nullptr);
			}
		}
	else {
		FATAL_ERROR_FWD(nullptr);
		}
	}


#ifdef CBL_TEST_EMULATION_KEEP_CONNECTION_RT0_ALIVED

std::list< std::shared_ptr<i_rt0_peer_handler_t>> __CBL_TEST_ERREMT_alivedPeersRT0;
void __CBL_TEST_ERREMT_add_alived_xpeer(std::shared_ptr<i_rt0_peer_handler_t> xp) {
	__CBL_TEST_ERREMT_alivedPeersRT0.push_back(xp);
	}

#else

void __CBL_TEST_ERREMT_add_alived_xpeer(std::shared_ptr<i_rt0_peer_handler_t> xp) {}

#endif


void rt0_cntbl_t::event_addlink_messages(const std::unique_ptr <i_rt0_cmd_addlink_t> & ev_,
	std::unique_ptr<addlink_reverse_scope_t> && tailHndl) {

	auto rdm = ev_->msg_rdm();
	auto channelId = ev_->channel_id();
	auto unref = make_remote_unregister(ev_);

	bind_subscribe2peer(ev_, rdm, utility::bind_once_call({ "rt0_cntbl_t::event_addlink_messages" }, 
		[wptr = weak_from_this(), channelId, rdm]
	(std::shared_ptr<i_rt0_peer_handler_t> && p, rtmessate_entry_t::unregister_f && unregf, std::unique_ptr<addlink_reverse_scope_t> && h) {

		if(auto sptr = wptr.lock()) {
			if(p && p->is_opened()) {

				__CBL_TEST_ERREMT_add_alived_xpeer(p);
				CHECK_PTR(sptr->rt())->subscribe_messages(channelId, rdm, std::move(p), std::move(unregf));

				if(h)
					h->invoke(nullptr);
				}
			else {
				if(h)
					h->invoke(CREATE_MPF_PTR_EXC_FWD(nullptr));
				else {
					THROW_MPF_EXC_FWD(nullptr);
					}
				}
			}
		}, std::placeholders::_1, std::move(unref), std::move(tailHndl)));
	}

void rt0_cntbl_t::event_addlink_events( const std::unique_ptr<i_rt0_cmd_addlink_t> & ev_,
	std::unique_ptr<addlink_reverse_scope_t> && tailHndl ) {

	auto rt_( rt() );
	if( rt_ ) {
		rt_->subscribe_events( ev_->channel_id(), ev_->ev_rdm(), ev_->events() );

		if( tailHndl )
			tailHndl->invoke(nullptr);
		}
	else {
		THROW_MPF_EXC_FWD(nullptr);
		}
	}

void rt0_cntbl_t::event_addlink_messages_events(const std::unique_ptr<i_rt0_cmd_addlink_t> & ev_,
	std::unique_ptr<addlink_reverse_scope_t> && tailHndl) {

	auto rdmMsg = ev_->msg_rdm();
	auto evRdm = ev_->ev_rdm();
	auto channelId = ev_->channel_id();
	auto evlst = ev_->events();
	auto unref = make_remote_unregister(ev_);

	bind_subscribe2peer(ev_, rdmMsg,
		crm::utility::bind_once_call({ "rt0_cntbl_t::event_addlink_messages_events" }, 
			[wptr = weak_from_this()](std::unique_ptr<i_rt0_peer_handler_t> && p,

			const decltype(rdmMsg) & msgRdm,
			const decltype(evRdm) & evRdm,
			const decltype(channelId) & channelId,
			decltype(evlst) && evlst,
			rtmessate_entry_t::unregister_f && unref_,
			std::unique_ptr<addlink_reverse_scope_t> && tailHndl) {

		if(auto sptr = wptr.lock()) {
			if(p && p->is_opened()) {
				CHECK_PTR(sptr->rt())->subscribe_messages(channelId, msgRdm, std::move(p), std::move(unref_));
				CHECK_PTR(sptr->rt())->subscribe_events(channelId, evRdm, evlst);

				if(tailHndl)
					tailHndl->invoke(nullptr);
				}
			else {
				THROW_MPF_EXC_FWD(nullptr);
				}
			}
		}, std::placeholders::_1, rdmMsg, evRdm, channelId, std::move(evlst), std::move(unref), std::move(tailHndl)));
	}

void rt0_cntbl_t::event_addlink( std::unique_ptr <i_command_from_rt1_t> && ev_,
	std::shared_ptr<command_response_guard_t> reverseInvoker ) {

	auto ev( command_cast<i_rt0_cmd_addlink_t>(std::move( ev_ )) );
	if( ev ) {
		auto rt_( rt() );
		if( rt_ ) {

			CBL_VERIFY( ev->command_source_emitter_rdm() == reverseInvoker->target_object_id() );

			CBL_MPF_TRACELEVEL_EVENT_RT1_CONNECTION_TRACE("add link:" + ev->msg_rdm().to_str());

			/* �����
			-----------------------------------------*/
			auto cmdSrcEmitRdm = ev->command_source_emitter_rdm();
			auto tailh = std::make_unique<addlink_reverse_scope_t>(ctx(), [reverseInvoker, cmdSrcEmitRdm]( std::unique_ptr<dcf_exception_t> && exc ) {
				if( !exc && reverseInvoker && !is_null( cmdSrcEmitRdm ) ) {
					reverseInvoker->invoke_cmd_to_rt1( i_rt0_cmd_sndrcv_result_t( cmdSrcEmitRdm, "" ) );
					}
				else {
					reverseInvoker->invoke_exc( exc ? std::move( exc ) : CREATE_MPF_PTR_EXC_FWD(nullptr) );
					}
				} );

			switch( ev->register_group() ) {
				case register_slot_group_t::events:
					event_addlink_events( ev, std::move( tailh ) );
					break;

				case register_slot_group_t::messages:
					event_addlink_messages( ev, std::move( tailh ) );
					break;

				case register_slot_group_t::message_events:
					event_addlink_messages_events( ev, std::move( tailh ) );
					break;

				default:
					FATAL_ERROR_FWD(nullptr);
				}
			}
		else {
			THROW_EXC_FWD(nullptr);
			}
		}
	else {
		FATAL_ERROR_FWD(nullptr);
		}
	}

void rt0_cntbl_t::event_sublink_unbind_connection( rtl_table_t t, const source_object_key_t & srcKey )noexcept {

	if( t == rtl_table_t::out && !is_null( srcKey ) ) {
		auto otbl_( otbl() );
		if( otbl_ ) {
			CHECK_NO_EXCEPTION( otbl_->unbind_connection( {std::move( srcKey )} ) );
			}
		}
	}

rtmessate_entry_t::unregister_f rt0_cntbl_t::make_remote_unregister(const std::unique_ptr<i_rt0_cmd_addlink_t> & ev_) {
	if(ev_) {
		struct _hndl_once_invoke_t {
			std::weak_ptr<rt0_cntbl_t> _wptr;
			std::atomic<bool> _infokedf = false;

			_hndl_once_invoke_t(std::weak_ptr<rt0_cntbl_t> wptr_)
				: _wptr(wptr_) {}

			void invoke(const rtl_roadmap_obj_t &id,
				const std::shared_ptr<i_rt0_peer_handler_t> & peer_) {

				bool invf = false;
				if(_infokedf.compare_exchange_strong(invf, true)) {

					if(id.table() != rtl_table_t::rt1_router) {
						if(peer_) {

							CBL_MPF_TRACELEVEL_EVENT_RT1_CONNECTION_TRACE("send remote object disconnect:" + id.to_str());

							if(auto  sptr = _wptr.lock()) {

								if(auto ctx_ = sptr->ctx()) {

									ctx_->launch_async(__FILE_LINE__,
										[peer_]( odisconnection_t && odisc) {

										if (peer_->direction() == rtl_table_t::in) {
											dynamic_cast<__rt0_ipeer_table_t::peer_handler*>(peer_.get())->push(std::move(odisc));
											}
										else if (peer_->direction() == rtl_table_t::out) {
											dynamic_cast<__rt0_opeer_table_t::peer_handler*>(peer_.get())->push(std::move(odisc));
											}
										else {
											FATAL_ERROR_FWD(nullptr);
											}

										}, make_disconnection(id));
									}
								}
							}
						}
					}
				}
			};


		auto pSt = std::make_shared<_hndl_once_invoke_t>(shared_from_this());
		return [pSt](const rtl_roadmap_obj_t &id, const std::shared_ptr<i_rt0_peer_handler_t> & peer) {
			if(pSt) {
				pSt->invoke(id, peer);
				}
			};
		}
	else {
		THROW_MPF_EXC_FWD(nullptr);
		}
	}

bool remote_disconnect_condition( const std::unique_ptr<i_rt0_cmd_sublink_t> & ev_ ){
	bool result = true;

	if( ev_->msg_rdm().table() == rtl_table_t::rt1_router )
		result = false;

	if( result && ev_->reason() == unregister_message_track_reason_t::closed_by_rtsys )
		result = false;

	if( result && ev_->reason() == unregister_message_track_reason_t::closed_by_self_from_remoted )
		result = false;

	return result;
	}

void rt0_cntbl_t::event_sublink_remote_disconnect( const std::unique_ptr<i_rt0_cmd_sublink_t> & ev_,
	rtl_rdm_keypair_t && disconnectPair )noexcept {

	if( ev_ ){
		if( remote_disconnect_condition( ev_ ) ){
			if(disconnectPair.channel && disconnectPair.channel->xpeer_){

				CBL_VERIFY( disconnectPair.rdm == ev_->msg_rdm() || is_null( ev_->msg_rdm() ) );
				disconnectPair.invoke_remote_unsubscribe();
				}
			}
		}
	}

void rt0_cntbl_t::event_sublink( std::unique_ptr<i_command_from_rt1_t> && ev_,
	std::shared_ptr<command_response_guard_t> reverseInvoker ) {
	if( ev_ ) {
		const auto ev( command_cast<i_rt0_cmd_sublink_t>(std::move( ev_ )) );
		if( ev ) {
			auto rt_( rt() );
			if( rt_ ) {

				CBL_MPF_TRACELEVEL_EVENT_RT1_CONNECTION_TRACE("remove link:" + ev->msg_rdm().to_str());

				/* ��������� ���������� �� ������� �����������
				-------------------------------------------------------------------------------*/
				post_scope_action_t scope0( [&] {
					event_sublink_unbind_connection( ev->msg_rdm().table(), ev->msg_rdm().source_object_key() );
					} );

				/* ������� �� ��������� �������
				-------------------------------------------------------------------------------*/
				post_scope_action_t scope1( [&] {
					if( ev->register_group() == register_slot_group_t::events || ev->register_group() == register_slot_group_t::message_events ) {
						rt_->unsubscribe_events( ev->channel_id(), ev->ev_rdm() );
						} } );

				/* ������� �� ��������� ���������
				-------------------------------------------------------------------------------*/
				if( ev->register_group() == register_slot_group_t::messages || ev->register_group() == register_slot_group_t::message_events ) {
					auto disconnectPair = rt_->unsubscribe_messages( ev->channel_id(), ev->msg_rdm() );

					/* �������� ��������� ��������� ������� �� ���������� ������� �� ���� �������
					-------------------------------------------------------------------------------*/
					event_sublink_remote_disconnect( ev, std::move( disconnectPair ) );
					}

				/* �����
				-----------------------------------------*/
				if( reverseInvoker && !is_null( ev->command_source_emitter_rdm() ) ) {

					CBL_VERIFY( ev->command_source_emitter_rdm() == reverseInvoker->target_object_id() );
					reverseInvoker->invoke_cmd_to_rt1( i_rt0_cmd_sndrcv_result_t( ev->command_source_emitter_rdm(), "" ) );
					}
				}
			else {
				THROW_EXC_FWD(nullptr);
				}
			}
		else {
			FATAL_ERROR_FWD(nullptr);
			}
		}
	}

void rt0_cntbl_t::event_sublink_source_key( std::unique_ptr<i_command_from_rt1_t> && ev_,
	std::shared_ptr<command_response_guard_t> reverseInvoker ) {
	if( ev_ ) {
		auto ev( command_cast<i_rt0_cmd_sublink_sourcekey_t>(std::move( ev_ )) );
		if( ev ) {
			auto rt_( rt() );
			if( rt_ ) {

				/* ��������� ���������� �� ������� �����������
				-------------------------------------------------------------------------------*/
				event_sublink_unbind_connection( rtl_table_t::out, ev->source_key() );

				/* �����
				-----------------------------------------*/
				if( reverseInvoker && !is_null( ev->command_source_emitter_rdm() ) ) {

					CBL_VERIFY( ev->command_source_emitter_rdm() == reverseInvoker->target_object_id());
					reverseInvoker->invoke_cmd_to_rt1( i_rt0_cmd_sndrcv_result_t( ev->command_source_emitter_rdm(), "" ) );
					}
				}
			}
		else {
			FATAL_ERROR_FWD(nullptr);
			}
		}
	}

void rt0_cntbl_t::excmd_unbind_connection( std::unique_ptr<i_command_from_rt1_t> && cmd_,
	std::shared_ptr<command_response_guard_t> reverseInvoker ) {

	if( cmd_ ) {
		auto cmd( command_cast<cmd_unsubscribe_connection_t>(std::move( cmd_ )) );
		if( cmd ) {
			if( cmd->table() == rtl_table_t::out ) {

				auto tbl_( otbl() );
				if( tbl_ ) {
					CHECK_NO_EXCEPTION( tbl_->unsubscribe_event_handler( cmd->connection_key(), cmd->subscriber_id() ) );
					}
				}
			else {
				THROW_MPF_EXC_FWD(nullptr);
				}
			}
		else {
			FATAL_ERROR_FWD(nullptr);
			}
		}
	}



void rt0_cntbl_t::xchannel_module_initialize(std::unique_ptr<i_command_from_rt1_t> && /*cmd*/,
	std::shared_ptr<command_response_guard_t> /*reverseInvoker*/) {
	
	}



void rt0_cntbl_t::addlink_reverse_scope_t::exchndl( std::unique_ptr<dcf_exception_t> && exc ) noexcept {
	auto ctx_( _ctx.lock() );
	if( ctx_ ) {
		ctx_->exc_hndl( std::move( exc ) );
		}
	}

rt0_cntbl_t::addlink_reverse_scope_t::~addlink_reverse_scope_t() {
	CHECK_NO_EXCEPTION( __invoke( true, nullptr ) );
	}

rt0_cntbl_t::addlink_reverse_scope_t::addlink_reverse_scope_t( std::weak_ptr<distributed_ctx_t> ctx, handler_f && h )
	: _ctx( ctx )
	, _h( std::move( h ) ) {}

void rt0_cntbl_t::addlink_reverse_scope_t::invoke( std::unique_ptr<dcf_exception_t> && exc )noexcept {
	CHECK_NO_EXCEPTION( __invoke( false, std::move( exc ) ) );
	}

void rt0_cntbl_t::addlink_reverse_scope_t::__invoke( bool dctr, std::unique_ptr<dcf_exception_t> && exc ) noexcept {
	bool inv = false;
	if( _if.compare_exchange_strong( inv, true ) ) {
		if( _h ) {
			if( dctr ) {
				_h( exc ? std::move( exc ) : CREATE_MPF_PTR_EXC_FWD(nullptr) );
				}
			else {
				_h( std::move( exc ) );
				}
			}
		}
	}

