#include "../ipc_host_hubtbl.h"

using namespace crm;
using namespace crm::detail;


void ipc_host_map_of_hubs::insert( std::shared_ptr<xchannel_shmem_hub> channel,
	std::function<void( rt1_x_message_data_t && m )> && mh,
	std::function<void( rt0_x_command_pack_t && c )> && ch,
	std::weak_ptr<distributed_ctx_t> ctx ){

	lck_scp_t lck( _lck );
	if( !_tbl.count( channel->get_channel_info().id() ) ){

		auto insr = _tbl.insert( { channel->get_channel_info().id(),
			std::make_unique<xchannel_shmem_hub_watcher>() } );

		if( insr.second ){
			insr.first->second->open( channel, std::move( mh ), std::move( ch ), ctx );
			}
		else{
			THROW_EXC_FWD(nullptr);
			}
		}
	else{
		THROW_EXC_FWD(nullptr);
		}
	}

void ipc_host_map_of_hubs::remove( const channel_identity_t & chid )noexcept{
	lck_scp_t lck( _lck );
	_tbl.erase( chid );
	}

std::shared_ptr<xchannel_shmem_hub> ipc_host_map_of_hubs::find( const channel_identity_t & chid )const noexcept{
	lck_scp_t lck( _lck );
	auto it = _tbl.find( chid );
	if( it != _tbl.end() ){
		return it->second->hub();
		}
	else{
		return nullptr;
		}
	}

void ipc_host_map_of_hubs::close()noexcept{}
