#include "../rt0_main.h"


using namespace crm;
using namespace crm::detail;


void rt0_cntbl_t::execute_exthndl_connection( ipeer_link_t && peer,
	crm::datagram_t && initializeResult ) noexcept{

	/* ����� ����������������� ����������� ��������� ����������� � ��������� ������ */
	connection_hndl_t hndl( _xhndl_Connection );
	if( hndl ) {
		auto ctx_( ctx() );
		if( ctx_ ) {

			CBL_MPF_TRACELEVEL_EVENT_RT0_CONNECTION_TRACE("i[0]-connection established:" + peer.desc().to_str());

			ctx_->launch_async_exthndl( __FILE_LINE__, []
				( connection_hndl_t && hndl_, ipeer_link_t && peer_, datagram_t && initializeResult_ ) {

				hndl_( std::move( peer_ ), std::move(initializeResult_) );

				}, std::move( hndl ), std::move( peer ), std::move( initializeResult ) );
			}
		}
	}

void rt0_cntbl_t::execute_exthndl_disconnection( const xpeer_desc_t & desc,
	std::shared_ptr<i_peer_statevalue_t> && stv )noexcept {

	/* ����� ����������������� ����������� ��������� ���������� � ��������� ������ */
	disconnection_hndl_t hndl( _xhndl_Disconnection );
	if( hndl ) {
		auto ctx_( ctx() );
		if( ctx_ ) {
			CBL_MPF_TRACELEVEL_EVENT_RT0_CONNECTION_TRACE("i[0]-connection closed:" + desc.to_str());

			ctx_->launch_async_exthndl( __FILE_LINE__, [desc]
				( disconnection_hndl_t && hndl_, std::shared_ptr<i_peer_statevalue_t> && stv ) {

				hndl_( desc, std::move(stv) );

				}, std::move( hndl ), std::move( stv ) );
			}
		}
	}
