#include <appkit/appkit.h>
#include "../../xchannel/rt0_x_chrt.h"
#include "../rt0_main.h"


using namespace crm;
using namespace crm::detail;


command_response_guard_t::command_response_guard_t( std::weak_ptr<rt0_cntbl_t> rt,
	const channel_identity_t & chid,
	const rtl_roadmap_event_t & srcId,
	const local_command_identity_t& sourceCmdId,
	rtx_command_type_t sourceCmdType,
	bool response )noexcept
	: _rt( rt )
	, _chid( chid )
	, _srcId( srcId )
	, _sourceCmdId( sourceCmdId )
	, _srcCommandType( sourceCmdType )
	, _response(response)

#ifdef CBL_MPF_OBJECTS_COUNTER_CHECKER_HOST_COMMAND_REVERSE_GUARDER
	, _xDbgCounter( cvt2string( sourceCmdType ) )
#endif
	{

	CBL_VERIFY( !is_null( _chid ));
	}

void command_response_guard_t::send_abandoned() noexcept {
	invoke_exc( CREATE_PTR_EXC_FWD(nullptr) );
	}

void command_response_guard_t::invoke_exc( std::unique_ptr<dcf_exception_t> && exc ) noexcept {
	if( _response ) {
		if( auto rt_ = _rt.lock() ) {

			if( !is_null( _srcId ) ) {
				invoke_cmd_to_rt1( i_rt0_exception_t( rtx_exception_code_t::undefined, std::move( exc ), _srcId ) );
				}
			}
		}
	}

command_response_guard_t::~command_response_guard_t() {
	if( !_invokedf.load( std::memory_order::memory_order_acquire ) ) {
		send_abandoned();
		}
	}

void command_response_guard_t::cancel()noexcept {
	_invokedf.store( true, std::memory_order::memory_order_release );
	}

const channel_identity_t &  command_response_guard_t::channel_id()const noexcept {
	return _chid;
	}

const rtl_roadmap_event_t &  command_response_guard_t::target_object_id()const noexcept {
	return _srcId;
	}

const local_command_identity_t&  command_response_guard_t::source_cmd_id()const noexcept {
	return _sourceCmdId;
	}

bool command_response_guard_t::with_response()const noexcept {
	return _response;
	}

