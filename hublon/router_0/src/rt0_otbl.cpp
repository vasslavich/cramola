#include "../rt0_otbl.h"
#include "../rt0_otbl_hndl.h"


using namespace crm;
using namespace crm::detail;


#ifdef CBL_OBJECTS_COUNTER_CHECKER
void __rt0_opeer_table_t::__size_trace()const noexcept{
	CHECK_NO_EXCEPTION( __sizeTraceer.CounterCheck( _tblCn.size() + _tblFullKey.size() ));
	}
#endif

void __rt0_opeer_table_t::close( std::shared_ptr<__rt0_opeer_table_t::ostream_lnk_t> && obj, bool lazy )noexcept {
	if( obj ) {
		if( lazy ) {
			auto ctx_( _ctx.lock() );
			if( ctx_ ) {
				ctx_->launch_async( __FILE_LINE__, []
					( std::shared_ptr<ostream_lnk_t> && obj ) {

					obj->close();
					}, std::move( obj ) );
				}
			}
		else {
			CHECK_NO_EXCEPTION( obj->close() );
			}
		}
	}

bool __rt0_opeer_table_t::closed() const noexcept {
	return _cancelf.load(std::memory_order::memory_order_acquire);
	}

std::shared_ptr<i_rt0_main_router_input_t> __rt0_opeer_table_t::target_stock() {
	return std::atomic_load_explicit(&_targetStock, std::memory_order::memory_order_acquire);
	}

std::shared_ptr<i_xpeer_source_factory_t> __rt0_opeer_table_t::peer_factory()const noexcept {
	return _xpFct;
	}

void __rt0_opeer_table_t::_remove_all()noexcept {
	_tblFullKey.clear();
	_tblCn.clear();
	}

std::shared_ptr<__rt0_opeer_table_t::ostream_lnk_t> __rt0_opeer_table_t::_find( const connection_key_t & remotedObjectKey )const noexcept {
	auto itKey = _tblCn.find( remotedObjectKey );
	if( itKey != _tblCn.end() ) {
		auto pOstream = itKey->second.lock();
		if( pOstream )
			return std::move( pOstream );
		else
			return nullptr;
		}
	else
		return nullptr;
	}

std::shared_ptr<__rt0_opeer_table_t::ostream_lnk_t> __rt0_opeer_table_t::__find_peer( const connection_key_t & cnk )const noexcept {
	return _find( cnk );
	}

void __rt0_opeer_table_t::_bind_id( const source_object_key_t & srcKey,
	std::shared_ptr<ostream_lnk_t> lnk ) {

	if( lnk && lnk->is_opened() ) {

		auto itObjectId = _tblFullKey.find( srcKey );
		if( itObjectId == _tblFullKey.end() ) {
			auto itIns = _tblFullKey.insert( {srcKey, std::move( lnk )} );
			if( !itIns.second ) {
				FATAL_ERROR_FWD(nullptr);
				}
			if( !itIns.first->second->is_opened() ) {
				THROW_MPF_EXC_FWD(nullptr);
				}

#ifdef CBL_OBJECTS_COUNTER_CHECKER
			__size_trace();
#endif
			}
		}
	else {
		THROW_MPF_EXC_FWD(nullptr);
		}
	}

void __rt0_opeer_table_t::_unbind_id( std::vector<source_object_key_t> && srcKeys )noexcept {

	for( const auto & srcKey : srcKeys ) {
		auto itKL = _tblFullKey.find( srcKey );
		if( itKL != _tblFullKey.end() )
			_tblFullKey.erase( itKL );

		auto itCk = _tblCn.find( srcKey.connection_key().to_key() );
		if( itCk != _tblCn.end() && itCk->second.expired() ) {
			_tblCn.erase( itCk );
			}
		}

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	__size_trace();
#endif
	}

void __rt0_opeer_table_t::_unbind_id( const connection_key_t &ck ) noexcept {

	auto itObjectId = _tblFullKey.begin();
	while( itObjectId != _tblFullKey.end() ) {

		if( (*itObjectId).first.connection_key().to_key() == ck ) {

			/* ������� ������ */
			itObjectId = _tblFullKey.erase( itObjectId );
			}
		else
			++itObjectId;
		}

	auto itCk = _tblCn.find( ck );
	if( itCk != _tblCn.end() && itCk->second.expired() ) {
		_tblCn.erase( itCk );
		}

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	__size_trace();
#endif
	}

void __rt0_opeer_table_t::__unsubscribe_event_handler( const connection_key_t & cnk, const _address_event_t & id )noexcept {
	auto p = __find_peer( cnk );
	if( p ) {
		CHECK_NO_EXCEPTION( p->unsubscribe_event( id ) );
		}
	}

std::shared_ptr<__rt0_opeer_table_t::ostream_lnk_t> __rt0_opeer_table_t::__exists( const source_object_key_t & srcKey )noexcept {
	return __exists( srcKey.connection_key().to_key() );
	}

std::shared_ptr<__rt0_opeer_table_t::ostream_lnk_t> __rt0_opeer_table_t::__exists(const connection_key_t& cKey)noexcept {
	auto p = _tblCn.find(cKey);
	if (_tblCn.end() != p) {
		auto obj = p->second.lock();
		if (!obj) {
			_tblCn.erase(p);
			return nullptr;
			}
		else {
			return obj;
			}
		}
	else {
		return nullptr;
		}
	}

std::shared_ptr<__rt0_opeer_table_t::ostream_lnk_t> __rt0_opeer_table_t::exists( const source_object_key_t & srcKey )noexcept {
	lck_scp_t tblck( _tblLck );
	return __exists( srcKey );
	}

std::pair<std::shared_ptr<__rt0_opeer_table_t::ostream_lnk_t>, bool> __rt0_opeer_table_t::create_connection( const source_object_key_t & srcKey,
	connection_create_hndl_t && postHndl,
	close_event_hndl_t && closeHndl,
	std::function<std::shared_ptr<i_peer_statevalue_t>()> && stvalctr,
	exceptions_suppress_checker_t && connectionExcChecker,
	__event_handler_point_f&& evhndl) {
	
	lck_scp_t tblck( _tblLck );
	return _create_connection( srcKey, std::move( postHndl ), std::move( closeHndl ), std::move( stvalctr ), std::move( connectionExcChecker ), std::move(evhndl) );
	}

std::pair<std::shared_ptr<__rt0_opeer_table_t::ostream_lnk_t>, bool> __rt0_opeer_table_t::_create_connection( const source_object_key_t & srcKey,
	connection_create_hndl_t && postHndl,
	close_event_hndl_t && closeHndl,
	std::function<std::shared_ptr<i_peer_statevalue_t>()> && stvalctr ,
	exceptions_suppress_checker_t && connectionExcChecker,
	__event_handler_point_f&& evhndl) {

	auto lnk = __exists( srcKey );
	if( !lnk ) {

		/* ������� ���� �� �����������
		-------------------------------------------------*/
		auto lnk_( ostream_rt0_x_t::create_routed( _ctx,
			_protocol,
			object_initialize_handler_t( _outXInit ),
			_xpFct,
			std::move( postHndl ),
			std::move( closeHndl ),
			_thisId,
			local_object_create_connection_t( _idCtr ),
			remote_object_check_identity_t( _idChecker ),
			std::make_unique<rt1_endpoint_t>( srcKey.connection_key() ),
			std::shared_ptr<i_rt0_main_router_input_t>( _targetStock ),
			std::move( stvalctr ),
			std::move( connectionExcChecker ),
			std::move(evhndl),
			async_space_t::sys ) );
		auto pLnk = std::make_shared<ostream_lnk_t>( std::move( lnk_ ) );

		/* �������� � ������� ����������� */
		if( !_tblCn.insert( {srcKey.connection_key().to_key(), pLnk} ).second )
			FATAL_ERROR_FWD(nullptr);

		/* ��������� ������������
		-----------------------------------*/
		_bind_id( srcKey, pLnk );

		return std::make_pair(pLnk,true);
		}
	else {
		return std::make_pair(lnk,false);
		}
	}


void __rt0_opeer_table_t::_bind_connection( const source_object_key_t & srcKey,
	std::unique_ptr<subscribe_event_connection_t> && postHndl ) {

	auto it = _tblCn.find( srcKey.connection_key().to_key() );
	if(it == _tblCn.end()) {
		THROW_MPF_EXC_FWD(nullptr);
		}
	else {
		auto sp( it->second.lock() );
		if( sp ) {
			sp->invoke_after_ready( async_space_t::sys, std::move( postHndl ) );
			}
		else {
			_tblCn.erase( it );
			THROW_MPF_EXC_FWD(nullptr);
			}
		}
	}

__rt0_opeer_table_t::__rt0_opeer_table_t( std::weak_ptr<distributed_ctx_t> ctx_,
	const identity_descriptor_t & thisId_,
	std::shared_ptr<i_rt0_main_router_input_t> && inputStock_,
	local_object_create_connection_t && idCtr,
	remote_object_check_identity_t && idChecker,
	std::shared_ptr<default_binary_protocol_handler_t> protocl_,
	object_initialize_handler_t && outXInit,
	std::shared_ptr<i_xpeer_source_factory_t> xpFct)
	: _ctx(ctx_)
	, _thisId(thisId_)
	, _targetStock(std::move(inputStock_))
	, _idCtr(std::move(idCtr))
	, _idChecker(std::move(idChecker))
	, _protocol(std::move(protocl_))
	, _outXInit(std::move(outXInit))
	, _xpFct(std::move(xpFct)) {}

std::shared_ptr<__rt0_opeer_table_t> __rt0_opeer_table_t::create(std::weak_ptr<distributed_ctx_t> ctx_,
	const identity_descriptor_t & thisId_,
	std::shared_ptr<i_rt0_main_router_input_t> && inputStock_,
	local_object_create_connection_t && idCtr_,
	remote_object_check_identity_t && idChecker_,
	std::shared_ptr<default_binary_protocol_handler_t> protocol_,
	object_initialize_handler_t && outXInit,
	std::shared_ptr<i_xpeer_source_factory_t> xpFct) {

	std::shared_ptr<self_t> obj(new self_t(ctx_,
		thisId_,
		std::move(inputStock_),
		std::move(idCtr_),
		std::move(idChecker_),
		std::move(protocol_),
		std::move(outXInit),
		std::move(xpFct)));
	return std::move(obj);
	}

__rt0_opeer_table_t::~__rt0_opeer_table_t() {
	CHECK_NO_EXCEPTION( _close() );
	}

void __rt0_opeer_table_t::close()noexcept {

	lck_scp_t tblck( _tblLck );
	CHECK_NO_EXCEPTION( _close() );
	}

void __rt0_opeer_table_t::_close()noexcept {

	_cancelf.store( true, std::memory_order::memory_order_release );
	CHECK_NO_EXCEPTION( _remove_all() );
	}

std::shared_ptr<__rt0_opeer_table_t::ostream_lnk_t> __rt0_opeer_table_t::find( const connection_key_t & id ) const noexcept {

	lck_scp_t tblck(_tblLck);
	return _find(id);
	}

std::unique_ptr<rt0_opeer_handler_t> __rt0_opeer_table_t::find_handler( const connection_key_t & ck )const noexcept {
	lck_scp_t tblck( _tblLck );
	auto p = _find(ck);
	tblck.unlock();

	if(p && p->is_opened()){
		return std::make_unique<rt0_opeer_handler_t>(std::move(p));
		}
	else {
		return {};
		}
	}

void __rt0_opeer_table_t::bind_connection( const source_object_key_t & srcKey,
	std::unique_ptr<subscribe_event_connection_t>  && postHndl ) {

	if( !closed() ) {

		lck_scp_t tblck( _tblLck );
		_bind_connection(srcKey,
			std::move(postHndl) );
		}
	else {
		THROW_MPF_EXC_FWD(nullptr);
		}
	}

void __rt0_opeer_table_t::unbind_connection( std::vector<source_object_key_t> && srcKeys ) noexcept {

	lck_scp_t tblck( _tblLck );
	CHECK_NO_EXCEPTION( _unbind_id( std::move(srcKeys) ) );
	}

void __rt0_opeer_table_t::unbind_connection( const connection_key_t& epKey  )noexcept {

	lck_scp_t tblck( _tblLck );
	CHECK_NO_EXCEPTION( _unbind_id( epKey ) );
	}

void __rt0_opeer_table_t::push(rt1_x_message_t && pck ) {
	auto h = find_handler( pck.rdm().points.rt0().connection_key() );
	if( h ) {
		h->push( std::move( pck ) );
		}
	else {
		THROW_MPF_EXC_FWD(nullptr);
		}
	}

std::shared_ptr<__rt0_opeer_table_t::ostream_lnk_t> __rt0_opeer_table_t::__capture_plink( const source_object_key_t & sourceKey )noexcept {

	std::shared_ptr<__rt0_opeer_table_t::ostream_lnk_t> link;

	/* �������� � ������� �������� ����������� */
	auto rt0It = _tblFullKey.find( sourceKey );
	if( rt0It != _tblFullKey.end() ) {

		link = rt0It->second;
		_tblFullKey.erase( rt0It );

#ifdef CBL_OBJECTS_COUNTER_CHECKER
		__size_trace();
#endif
		}

	/* ���� ��� �� �������, ����� � �������� ������� */
	if( !link || !link->is_opened() ) {

		auto rt0TblIt = _tblCn.find( sourceKey.connection_key().to_key() );
		if( rt0TblIt != _tblCn.end() ) {
			auto p_ = rt0TblIt->second.lock();
			if( p_ && p_->is_opened() ) {
				link = std::move( p_ );
				}
			}
		}

	return std::move( link );
	}

std::unique_ptr<rt0_opeer_handler_t> __rt0_opeer_table_t::__capture_link( const source_object_key_t & sourceKey )noexcept {

	std::unique_ptr<rt0_opeer_handler_t> ip;

	auto p = __capture_plink( sourceKey );
	/* ����������� ������� � ����������� */
	if( p && p->is_opened() ) {
		ip = std::make_unique<rt0_opeer_handler_t>( std::move( p ) );
		}

	return std::move( ip );
	}

std::unique_ptr<rt0_opeer_handler_t> __rt0_opeer_table_t::capture_link( const source_object_key_t & sourceKey )noexcept {
	
	lck_scp_t tblck( _tblLck );
	return __capture_link( sourceKey );
	}

void __rt0_opeer_table_t::unsubscribe_event_handler( const connection_key_t & cnk, const _address_event_t & id )noexcept {
	lck_scp_t tblck( _tblLck );
	CHECK_NO_EXCEPTION( __unsubscribe_event_handler( cnk, id ) );
	}

std::unique_ptr<rt0_opeer_handler_t>__rt0_opeer_table_t::find_peer( const rt0_node_rdm_t & rdm )const noexcept {
	return find_handler( rdm.connection_key() );
	}

std::shared_ptr<default_binary_protocol_handler_t> __rt0_opeer_table_t::protocol()const noexcept {
	return _protocol;
	}


