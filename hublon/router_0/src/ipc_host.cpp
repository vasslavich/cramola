#include "../ipc_host.h"
#include "../../xchannel/smp/shmem_host_first.h"
#include "../../xchannel/smp/shmem_vector.h"
#include "../../unitcmd/cmdl.h"
#include "../../xchannel/chmk.h"
#include <experimental/coroutine>
#include <experimental/resumable>


using namespace crm;
using namespace crm::detail;


ipc_host::~ipc_host(){
	close();
	}

ipc_host::ipc_host()noexcept{}


xchannel_shmem_host_first* ipc_host::xchannel_first()const noexcept {
	return static_cast<xchannel_shmem_host_first*>(_channel.get());
	}

std::unique_ptr<i_xchannel_shmem_host_first> ipc_host::mkchannel(std::shared_ptr<distributed_ctx_t> c, 
	const xchannel_shmem_host_args & ca) {

	return std::make_unique<xchannel_shmem_host_first>(c, ca);
	}

ipc_host::io_break_token ipc_host::mk_cancel_token(const std::chrono::milliseconds & timeout)const{
	static_assert(is_break_timed_and_cancel_v<io_break_token>, __FILE_LINE__);
	return io_break_token([this]{return stopped(); }, timeout, timeout);
	}

void ipc_host::listen_channel_body()noexcept{
	static const std::chrono::seconds waitChannelRead(1);
	std::vector<uint8_t> buf;

	while( !stopped() ){
		try{
			while( !stopped() ){
				if(xchannel_first()->pop(buf, mk_cancel_token(waitChannelRead))) {
					handle_channel_strob( std::move( buf ) );
					}
				}
			}
		catch( const dcf_exception_t & e0 ){
			handle_ntf( e0.dcpy() );
			}
		catch( const std::exception & e1 ){
			handle_ntf( CREATE_PTR_EXC_FWD( e1 ) );
			}
		catch( ... ){
			handle_ntf( CREATE_PTR_EXC_FWD(nullptr) );
			}
		}
	}

void ipc_host::start(const ipc_host_start_options & opt){
	if( _ctx ){
		_ctx->start();
		}

	if( auto c = xchannel_first()){
		c->open( opt.mem_size );
		}

	_channelListenTh = std::thread( [this]{
		listen_channel_body();
		} );
	}

void ipc_host::stop()noexcept{
	_stopf.store( true, std::memory_order::memory_order_release );

	if( _ctx ){
		_ctx->stop();
		}
	}

std::shared_ptr<distributed_ctx_t> ipc_host::ctx()const noexcept{
	return _ctx;
	}

void ipc_host::handle_ntf( std::unique_ptr<dcf_exception_t> && n )const noexcept{
	if( auto c = ctx() ){
		c->exc_hndl( std::move( n ) );
		}
	}

bool ipc_host::stopped()const noexcept{
	return _stopf.load( std::memory_order::memory_order_acquire );
	}

void ipc_host::close()noexcept{
	stop();

	if( _ctx ){
		_ctx->close();
		}

	if( _route ){
		_route->close();
		}

	if( _channelListenTh.joinable() ){
		_channelListenTh.join();
		}

	if( auto c = xchannel_first()){
		c->close();
		_channel.reset();
		}
	}

std::shared_ptr<rt0_cntbl_t> ipc_host::router()const noexcept{
	return _route;
	}

std::shared_ptr<crm::default_binary_protocol_handler_t> ipc_host::protocol()const noexcept{
	return _protocol;
	}

void ipc_host::handle_channel_strob( std::vector<uint8_t> && strob ){
	channel_strob_processor proc( std::move( strob ) );
	auto h = proc.header();

	if( h.type == channel_strob_type::unit_connect ){
		handle( proc.read<unit_connect>() );
		}
	else if( h.type == channel_strob_type::unit_disconnect ){
		handle( proc.read<unit_disconnect>() );
		}
	else{
		FATAL_ERROR_FWD(nullptr);
		}
	}
//
//
//auto async_do_read_commands( std::weak_ptr<distributed_ctx_t> ctx,
//	std::weak_ptr< xchannel_shmem_hub > hub,
//	std::shared_ptr<std::list<rt0_x_command_pack_t>> buffers ){
//
//	struct Awaiter{
//		std::weak_ptr<distributed_ctx_t> ctx;
//		std::weak_ptr< xchannel_shmem_hub > hub;
//		std::shared_ptr<std::list<rt0_x_command_pack_t>> buffers;
//
//		std::unique_ptr<dcf_exception_t> exc;
//		size_t count{ 0 };
//
//		bool await_ready(){ return false; }
//		void await_suspend( std::experimental::coroutine_handle<> coro ){
//			if( auto c = ctx.lock() ){
//				timer = c->create_timer_deffered_action(
//					s.async_read_some( buffers, [this, coro]( auto ec_, auto sz_ ) mutable{
//					ec = ec_;
//					sz = sz_;
//
//					coro.resume();
//					} );
//				}
//			}
//		auto await_resume(){ return std::make_pair( ec, sz ); }
//		};
//	return Awaiter{ s, std::move( buffers ) };
//	}
//
//std::future<void> ipc_host::do_cycle_read_commands( std::weak_ptr< xchannel_shmem_hub > hub ){
//	bool stopf = stopped();
//	auto pCommandsList = std::make_shared<std::list< rt0_x_command_pack_t>>();
//
//	while( !stopf ){
//		try{
//			while( !stopf ){
//
//				auto[errRead, countRead] = co_await async_do_read_commands( hub, pCommandsList );
//				if( !errRead ){
//
//					if( countRead && _pingWatcher ){
//						_pingWatcher->add_accept();
//						}
//
//					/* ��������� ������ � ������ */
//					_readSlot( rbuf, countRead );
//					}
//				else{
//				#ifdef CBL_MPF_TRACELEVEL_EVENT_CONNECTION
//					close( make_exception( errRead, endps ) );
//				#else
//					close( make_exception( errRead ) );
//				#endif
//					}
//				}
//			}
//		catch( const mpf_exception_t & ex ){
//			if( auto c = ctx() ){
//				c->exc_hndl( ex );
//		}
//	}
//		catch( const dcf_exception_t & e0 ){
//			if( auto c = ctx() ){
//				c->exc_hndl( e0 );
//				}
//			}
//		catch( const std::exception & e1 ){
//			close( CREATE_PTR_EXC_FWD( e1 ) );
//			}
//		catch( ... ){
//			close( CREATE_PTR_EXC_FWD(nullptr) );
//			}
//		}
//	}
//
//std::future<void> ipc_host::do_cycle_read_messages( std::weak_ptr< xchannel_shmem_hub > hub ){}

void ipc_host::start_io( std::weak_ptr< xchannel_shmem_hub > hub ){
	//auto do_cycle_read_commands(hub);
	//auto do_cycle_read_messages( hub );
	}

void ipc_host::handle( unit_connect && ucn ){
	xchannel_shmem_hub_args args;
	args.xchannelRT0_x_c.mainNodeName = ucn.chName_RT0_x_c;
	args.xchannelRT0_x_m.mainNodeName = ucn.chName_RT0_x_m;
	args.xchannel_x_RT1_c.mainNodeName = ucn.chName_x_RT1_c;
	args.xchannel_x_RT1_m.mainNodeName = ucn.chName_x_RT1_m;

	channel_info_t chinfo( ucn.id );

	channel_shared_memory_config cfg;
	cfg.set_shmem_options( args );
	cfg.set_channel_info( chinfo );

	if( auto pHub = std::dynamic_pointer_cast<xchannel_shmem_hub>(
		make_channel_ipc_hub( _ctx, cfg )) ){

		pHub->open();

		_route->register_rt1_channel( pHub );
		_hubsTable.insert( pHub,
			[wptr = weak_from_this()]( rt1_x_message_data_t && m ){
			if( auto sptr = wptr.lock() ){
				sptr->handle_channel_hub( std::move( m ) );
				}
			},
			[wptr = weak_from_this()]( rt0_x_command_pack_t && c ){
				if( auto sptr = wptr.lock() ){
					sptr->handle_channel_hub( std::move( c ) );
					}
				},
				_ctx );

		start_io( pHub );
		}
	else{
		FATAL_ERROR_FWD(nullptr);
		}
	}

void ipc_host::handle( unit_disconnect && ucn )noexcept{
	_route->unregister_rt1_channel( channel_info_t{ ucn.id } );
	_hubsTable.remove( ucn.id );
	}

void ipc_host::handle_channel_hub(rt1_x_message_data_t && m) {
	if (_route) {
		if (!_route->push_message(std::move(m))) {
			THROW_EXC_FWD(nullptr);
			}
		}
	else {
		THROW_EXC_FWD(nullptr);
		}
	}

void ipc_host::handle_channel_hub(rt0_x_command_pack_t && c) {
	if (_route) {
		if (!_route->push_command_pack_to_rt0(std::move(c))) {
			THROW_EXC_FWD(nullptr);
			}
		}
	else {
		THROW_EXC_FWD(nullptr);
		}
	}

