#include <appkit/appkit.h>
#include "../rt0_otbl_hndl.h"


using namespace crm;
using namespace crm::detail;


rt0_opeer_handler_t::rt0_opeer_handler_t( std::shared_ptr<ostream_rt0_x_t::ostream_lnk_t>  && pl )
	: _pl( std::move( pl ) ){}

void rt0_opeer_handler_t::push( rt1_x_message_t && pck ) {

	if( is_null( pck.rdm().points.rt0().connection_key() ) ||
		is_null( pck.rdm().points.target_id() ) ) {

		THROW_MPF_EXC_FWD(nullptr);
		}

	if( is_opened() ) {

		if( _pl->state() == remote_object_state_t::impersonated ) {
			if( _pl->desc() == pck.rdm().points.rt0() ) {

				/* �������� ������ � ���� ��������� ��������� ���� ������ 0 */
				auto blob( std::move(pck).package() );
				_pl->write( std::move( blob ) );
				}
			else {
				THROW_MPF_EXC_FWD(nullptr);
				}
			}
		else {
			THROW_MPF_EXC_FWD( L"!remote_object_state_t::impersonated" );
			}
		}
	else {
		THROW_MPF_EXC_FWD(nullptr);
		}
	}

bool rt0_opeer_handler_t::is_opened()const noexcept{
	return _pl && _pl->is_opened();
	}
