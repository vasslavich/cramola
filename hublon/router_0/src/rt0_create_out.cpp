#include <appkit/appkit.h>
#include "../rt0_otbl.h"


using namespace crm;
using namespace crm::detail;



opeer_l0 __rt0_opeer_table_t::create_out_link(std::function<std::shared_ptr<i_peer_statevalue_t>()> && stvctr, 
	std::shared_ptr<rt0_connection_settings_t> pCtr) {

	CBL_VERIFY(!(pCtr->remote_endpoint()->address().empty() || pCtr->remote_endpoint()->port() == 0));

	auto identityCtr = [pCtr] {
		return pCtr->local_object_identity();
		};

	auto identityChecker = [pCtr](const identity_value_t& idSet,
		std::unique_ptr<i_peer_remote_properties_t>& prop) {

		return pCtr->remote_object_identity_validate(idSet, prop);
		};

	/* ���������� ��������� ������������� */
	auto initializeHndl = [pCtr]
	(crm::datagram_t&& dt, object_initialize_result_guard_t&& resultHndl) {

		pCtr->local_object_initialize(std::move(dt), std::move(resultHndl));
		};

	/* ���������� ������� ����������� � ��� */
	auto cnHndl = [pCtr](std::unique_ptr<dcf_exception_t>&& exc, ostream_rt0_x_t::link_t && lnk) {
		pCtr->connection_handler(std::move(exc), std::move(lnk));
		};

	/* ���������� ������� ����������� � ��� */
	auto dcnHndl = [pCtr](const i_xpeer_rt0_t::xpeer_desc_t& desc, closing_reason_t, std::shared_ptr<i_peer_statevalue_t>&& stv) {
		pCtr->disconnection_handler(desc, std::move(stv));
		};

	auto ev_hndl = [pCtr](std::unique_ptr<i_event_t>&& ev) {
		pCtr->event_handler(std::move(ev));
		};

	auto connectionExcChecker =
		[pCtr](detail::connection_stage_t stage, const std::unique_ptr<dcf_exception_t>& exc)noexcept {

		return pCtr->connnection_exception_suppress_checker(stage, exc);
		};

	/* ������� ������ 0 */
	auto ipeerEvents = [pCtr](std::unique_ptr<i_event_t>&& ev) {
		pCtr->event_handler(std::move(ev));
		};

	lck_scp_t tblck(_tblLck);
	return _create_connection(std::move(cnHndl),
		std::move(dcnHndl),
		std::move(stvctr),
		std::move(connectionExcChecker),
		std::move(initializeHndl),
		pCtr->this_id(),
		std::move(identityCtr),
		std::move(identityChecker),
		pCtr->remote_endpoint(),
		__event_handler_point_f(std::move(ipeerEvents), _ctx.lock(), async_space_t::ext),
		pCtr->input_stack());
	}


__rt0_opeer_table_t::ostream_lnk_t __rt0_opeer_table_t::_create_connection(
	ostream_rt0_x_t::post_connection_t&& postHndl,
	ostream_rt0_x_t::close_event_hndl_t&& closeHndl,
	std::function<std::shared_ptr<i_peer_statevalue_t>()>&& stvalctr,
	exceptions_suppress_checker_t&& connectionExcChecker,
	object_initialize_handler_t&& inihndl,
	const identity_descriptor_t& thisId,
	local_object_create_connection_t&& locIdCtr,
	remote_object_check_identity_t&& remoteIdChecker,
	std::unique_ptr<i_endpoint_t>&& ep,
	__event_handler_point_f&& evhndl,
	std::unique_ptr<i_input_messages_stock_t> && inputStock) {

	/* ������� ���� �� �����������
	-------------------------------------------------*/
	return ostream_rt0_x_t::create_auto(_ctx,
		protocol(),
		std::move(inihndl),
		peer_factory(),
		std::move(postHndl),
		std::move(closeHndl),
		thisId,
		std::move(locIdCtr),
		std::move(remoteIdChecker),
		std::move(ep),
		std::move(inputStock),
		std::move(stvalctr),
		std::move(connectionExcChecker),
		std::move(evhndl),
		async_space_t::sys);
	}


