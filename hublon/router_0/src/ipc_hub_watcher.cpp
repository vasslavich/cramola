#include "../ipc_hub_watcher.h"


using namespace crm;
using namespace crm::detail;


xchannel_shmem_hub_watcher::~xchannel_shmem_hub_watcher(){
	close();
	}

std::shared_ptr<xchannel_shmem_hub> xchannel_shmem_hub_watcher::hub()const noexcept{
	return _hub;
	}

bool xchannel_shmem_hub_watcher::stopped()const noexcept{
	return _cancelf.load( std::memory_order::memory_order_relaxed );
	}

void xchannel_shmem_hub_watcher::handle_ntf( std::unique_ptr<dcf_exception_t> && n )const noexcept{
	if( auto c = _ctx.lock() ){
		c->exc_hndl( std::move( n ) );
		}
	}

void xchannel_shmem_hub_watcher::open( std::shared_ptr<xchannel_shmem_hub> hub,
	std::function<void( rt1_x_message_data_t && m )> && mh_,
	std::function<void( rt0_x_command_pack_t && c )> && ch_,
	std::weak_ptr<distributed_ctx_t> ctx ){

	_ctx = ctx;
	_hub = hub;

	static const std::chrono::seconds waitChannelRead1( 1 );
	static const std::chrono::seconds waitChannelRead2( 1 );

	std::vector<uint8_t> buf;

	_thC = std::thread( [this, ch = std::move(ch_)]{

		while( !stopped() ){
			try{
				while( !stopped() ){
					rt0_x_command_pack_t c;
					if( _hub->pop( c, waitChannelRead2 ) ){
						ch( std::move( c ) );
						}
					}
				}
			catch( const dcf_exception_t & e0 ){
				handle_ntf( e0.dcpy() );
				}
			catch( const std::exception & e1 ){
				handle_ntf( CREATE_PTR_EXC_FWD( e1 ) );
				}
			catch( ... ){
				handle_ntf( CREATE_PTR_EXC_FWD(nullptr) );
				}
			}
		} );


	_thM = std::thread([this, mh = std::move(mh_)] {

		while (!stopped()) {
			try {
				while (!stopped()) {

					rt1_x_message_data_t m;
					if (_hub->pop(m, waitChannelRead1)) {
						mh(std::move(m));
						}
					}
				}
			catch (const dcf_exception_t & e0) {
				handle_ntf(e0.dcpy());
				}
			catch (const std::exception & e1) {
				handle_ntf(CREATE_PTR_EXC_FWD(e1));
				}
			catch (...) {
				handle_ntf(CREATE_PTR_EXC_FWD(nullptr));
				}
			}
		});
	}

void xchannel_shmem_hub_watcher::close()noexcept{
	_cancelf.store( true, std::memory_order::memory_order_release );
	if( _thM.joinable() ){
		_thM.join();
		}

	if (_thC.joinable()) {
		_thC.join();
		}

	_hub.reset();
	}

