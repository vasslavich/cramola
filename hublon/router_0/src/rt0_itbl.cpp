#include "../rt0_itbl.h"
#include <appkit/export_internal_terms.h>


using namespace crm;
using namespace crm::detail;


#ifdef CBL_OBJECTS_COUNTER_CHECKER
void peer_table_t::__size_trace()const noexcept {
	CHECK_NO_EXCEPTION(__sizeTraceer.CounterCheck(_map.size()));
	}
#endif

void peer_table_t::close_object(istream_link_t && obj, bool lazy)noexcept {

	if(lazy) {
		auto ctx_(_ctx.lock());
		if(ctx_) {
			ctx_->launch_async(__FILE_LINE__, []
			(istream_link_t && lnk) {

				lnk.close();
				}, std::forward<istream_link_t>(obj));
			}
		}
	else {
		CHECK_NO_EXCEPTION(obj.close());
		}
	}

void peer_table_t::_insert(const rt0_node_rdm_t &identity, istream_link_t && n) {

	/* ����� ������ �������� � ������ ���������� ��������������� */
	auto itlst = _map.find(identity);
	if(itlst == _map.end()) {
		if(n.is_opened() && remote_object_state_t::impersonated == n.state()) {

			if(!_map.insert({ identity, std::move(n) }).second) {
				FATAL_ERROR_FWD(nullptr);
				}

#ifdef CBL_OBJECTS_COUNTER_CHECKER
			__size_trace();
#endif
			}
		else {
			THROW_MPF_EXC_FWD(L"!is_opened() || !remote_object_state_t::impersonated");
			}
		}
	else
		THROW_MPF_EXC_FWD(nullptr);
	}

/*! ����� ����, �� ������������������ �������� */
peer_table_t::istream_link_t  peer_table_t::_find(const rt0_node_rdm_t & id)const noexcept {

	auto it(_map.find(id));
	if(it != _map.end())
		return it->second;
	else
		return istream_link_t{};
	}

/*! ����� ����, �� ������������������ �������� */
std::unique_ptr<rt0_ipeer_handler_t<peer_table_t::istream_link_t>>  peer_table_t::_find_handler(const rt0_node_rdm_t & id)const noexcept {

	auto it(_map.find(id));
	if(it != _map.end())
		return std::make_unique <rt0_ipeer_handler_t<istream_link_t>>(it->second);
	else
		return {};
	}

/*! ������� ���� �� �������, �� ������������������ �������� */
void  peer_table_t::_remove(const rt0_node_rdm_t & id) noexcept {

	/* ����� �� ����������� �������������� */
	auto itlst = _map.find(id);
	if(itlst != _map.end()) {

		auto osr(std::move(itlst->second));
		_map.erase(itlst);
		close_object(std::move(osr));

#ifdef CBL_OBJECTS_COUNTER_CHECKER
		__size_trace();
#endif
		}
	}

void  peer_table_t::_remove_all()noexcept {

	auto itPeer = _map.begin();
	while(itPeer != _map.end()) {
		close_object(std::move(itPeer->second), false);
		++itPeer;
		}
	_map.clear();
	}

void  peer_table_t::_close()noexcept {
	_remove_all();
	}

peer_table_t::peer_table_t(std::weak_ptr<distributed_ctx_t> ctx_)noexcept
	: _ctx(ctx_) {}

peer_table_t::~peer_table_t() {
	_close();
	}

void  peer_table_t::close()noexcept {
	lck_scp_t lck(_mapLck);
	_close();
	}

void  peer_table_t::insert(const rt0_node_rdm_t & identity, istream_link_t && n) {
	if(!is_null(identity)) {

		lck_scp_t lck(_mapLck);
		_insert(identity, std::move(n));
		}
	else {
		THROW_MPF_EXC_FWD(nullptr);
		}
	}

peer_table_t::istream_link_t  peer_table_t::find(const rt0_node_rdm_t & identity)const noexcept {
	lck_scp_t lck(_mapLck);
	return _find(identity);
	}

/*! ������� ���� �� ������� */
void  peer_table_t::remove(const rt0_node_rdm_t & id)noexcept {
	lck_scp_t lck(_mapLck);
	_remove(id);
	}

std::unique_ptr<rt0_ipeer_handler_t<peer_table_t::istream_link_t>>  peer_table_t::find_handler(const rt0_node_rdm_t & id)const noexcept {
	lck_scp_t lck(_mapLck);
	return _find_handler(id);
	}



std::shared_ptr<distributed_ctx_t> __rt0_ipeer_table_t::ctx()const noexcept {
	return _ioCtx.lock();
	}

void __rt0_ipeer_table_t::ntf_hndl(std::unique_ptr<dcf_exception_t> && ntf)noexcept {
	auto ctx_(ctx());
	if(ctx_)
		ctx_->exc_hndl(std::move(ntf));
	}

void __rt0_ipeer_table_t::ntf_hndl(const dcf_exception_t & exc)noexcept {
	auto ctx_(ctx());
	if(ctx_)
		ctx_->exc_hndl(exc.dcpy());
	}

bool __rt0_ipeer_table_t::canceled()const noexcept {
	if(_cancelHndl && _cancelHndl())
		return true;

	return _cancelf.load(std::memory_order::memory_order_acquire);
	}

std::shared_ptr<i_rt0_main_router_input_t> __rt0_ipeer_table_t::input_stock()const noexcept {
	return _inputStock.lock();
	}

void __rt0_ipeer_table_t::insert(peer_link_t && peer) {
	if (_ismMap) {
		_ismMap->insert(peer.desc(), std::move(peer));
		}
	else {
		THROW_MPF_EXC_FWD(nullptr);
		}
	}

void __rt0_ipeer_table_t::insert(const peer_link_t & peer) {
	auto p = peer;
	insert(std::move(p));
	}

__rt0_ipeer_table_t::peer_link_t __rt0_ipeer_table_t::find(const rt0_node_rdm_t & id)noexcept {
	if (_ismMap) {
		return _ismMap->find(id);
		}
	else {
		return {};
		}
	}

void __rt0_ipeer_table_t::remove(const rt0_node_rdm_t & id)noexcept {
	if(_ismMap) {
		CHECK_NO_EXCEPTION(_ismMap->remove(id));
		}
	}

__rt0_ipeer_table_t::__rt0_ipeer_table_t( std::weak_ptr<distributed_ctx_t> ctx_,
	const identity_descriptor_t & thisId,
	canceled_t && cancelHndl,
	local_object_create_connection_t && localIdCtr,
	remote_object_check_identity_t && remoteIdChecker,
	create_remote_object_initialize_list_t && remoteInzr,
	std::weak_ptr<i_rt0_main_router_input_t> inputStock,
	std::shared_ptr <i_xpeer_source_factory_t> ifactory,
	connection_hndl_t && xhndl_Connection,
	disconnection_hndl_t && xhndl_Disconnection,
	std::shared_ptr<default_binary_protocol_handler_t> protocol_,
	i_xpeer_rt0_t::ctr_0_input_queue_t && inCtr,
	std::function<std::shared_ptr<i_peer_statevalue_t>()> && stvalctr,
	__event_handler_point_f && eh)
	: _ioCtx(ctx_)
	, _thisId(thisId)
	, _ismMap(std::make_unique<peer_table_t>(ctx_))
	, _cancelHndl(std::move(cancelHndl))
	, _localIdCtr(std::move(localIdCtr))
	, _remoteIdChecker(std::move(remoteIdChecker))
	, _remoteInzr(std::move(remoteInzr))
	, _inputStock(inputStock)
	, _connectionFactory(std::move(ifactory))
	, _evhndl(std::move(eh))
	, _xhndl_Connection(std::move(xhndl_Connection))
	, _xhndl_Disconnection(std::move(xhndl_Disconnection))
	, _protocol(std::move(protocol_))
	, _inQCtr(std::move(inCtr))
	, _stvalCtr(std::move(stvalctr)){}

std::shared_ptr<__rt0_ipeer_table_t> __rt0_ipeer_table_t::create(std::weak_ptr<distributed_ctx_t> ctx,
	const identity_descriptor_t & thisId,
	canceled_t && cancelHndl,
	local_object_create_connection_t && localIdCtr,
	remote_object_check_identity_t && remoteIdChecker,
	create_remote_object_initialize_list_t && remoteInzr,
	std::weak_ptr<i_rt0_main_router_input_t> inputStock,
	std::shared_ptr <i_xpeer_source_factory_t> ifactory,
	connection_hndl_t && xhndl_Connection,
	disconnection_hndl_t && xhndl_Disconnection,
	std::shared_ptr<default_binary_protocol_handler_t> protocol_,
	i_xpeer_rt0_t::ctr_0_input_queue_t && inQCtr,
	std::function<std::shared_ptr<i_peer_statevalue_t>()> && stvalctr,
	const std::unique_ptr<i_endpoint_t> & ep,
	__event_handler_point_f && eh ) {

	auto p(std::shared_ptr<self_t>(new self_t(ctx,
		thisId,
		std::move(cancelHndl),
		std::move(localIdCtr),
		std::move(remoteIdChecker),
		std::move(remoteInzr),
		std::move(inputStock),
		std::move(ifactory),
		std::move(xhndl_Connection),
		std::move(xhndl_Disconnection),
		std::move(protocol_),
		std::move(inQCtr),
		std::move(stvalctr),
		std::move(eh))));
	p->start(ep);
	return std::move(p);
	}

__rt0_ipeer_table_t::~__rt0_ipeer_table_t() {
	close();
	}

std::unique_ptr<rt0_ipeer_handler_t<peer_table_t::istream_link_t>>__rt0_ipeer_table_t::find_peer(const rt0_node_rdm_t & rdm)const noexcept {
	if (_ismMap) {
		return _ismMap->find_handler(rdm);
		}
	else {
		return {};
		}
	}

void __rt0_ipeer_table_t::push(rt1_x_message_t && env) {
	if (_ismMap) {
		auto h = _ismMap->find_handler(env.rdm().points.rt0());
		if (h) {
			h->push(std::move(env));
			}
		else {
			THROW_MPF_EXC_FWD(nullptr);
			}
		}
	else {
		THROW_MPF_EXC_FWD(nullptr);
		}
	}

void __rt0_ipeer_table_t::evhndl_connection_initialize(peer_link_t&& peer_,
	syncdata_list_t&& sl,
	std::unique_ptr<execution_bool_tail>&& registerCompleted) {

	peer_.set_state(remote_object_state_t::initialize_stage);

	auto ctx_(ctx());
	if (ctx_) {

		CBL_MPF_TRACELEVEL_EVENT_RT0_CONNECTION_TRACE("in-peer[0]:initialize:" + peer_.remote_object_endpoint().to_string());

		/* ����������� ������� */
		auto reqCtr = utility::bind_once_call({ "__rt0_ipeer_table_t::evhndl_connection_initialize-ctr" },
			[wthis = weak_from_this()](syncdata_list_t&& sl) {

			/* ��������� ����������������� ���������� ������� ������������� */
			if (auto sthis = wthis.lock()) {

				/* ��������� � ����������������� ������� */
				osendrecv_datagram_t msg;
				if (sthis->_remoteInzr) {
					msg = osendrecv_datagram_t(sthis->_remoteInzr(std::move(sl)));
					}
				else {
					msg = osendrecv_datagram_t(syncdata_list_t{});
					}

				msg.set_key(i_xpeer_rt0_t::command_name_initialize);
				msg.set_destination_subscriber_id(i_xpeer_rt0_t::subscribe_address_datagram_for_this);

				return msg;
				}
			else {
				THROW_MPF_EXC_FWD(nullptr);
				}
			}, std::move(sl));

		/* ���������� ������ */
		auto resHndl = [wthis = weak_from_this(), peer_, reg = std::move(registerCompleted)](
			message_handler && m )mutable noexcept {

			auto exchndl = [sptr = wthis.lock(), &peer_](std::unique_ptr<crm::dcf_exception_t>&& e)noexcept{
				peer_.close();

				if (sptr) {
					sptr->ntf_hndl(std::move(e));
					}
				};

			try {
				if (m.has_result()) {
					if (auto sthis = wthis.lock()) {

						auto iDtg = std::move(m).as<isendrecv_datagram_t>();
						if (iDtg.key() == i_xpeer_rt0_t::command_name_initialize) {

							MPF_TEST_EMULATION_IN_CONTEXT_COND_TRCX(sthis->ctx(), 100,
								is_trait_at_emulation_context(iDtg),
								trait_at_keytrace_set(iDtg));

							peer_.set_state(remote_object_state_t::impersonated);

							sthis->evhndl_connection_tail_register(std::move(peer_),
								iDtg.value_release(),
								std::move(reg));

							iDtg.reset_invoke_exception_guard().reset();
							}
						else {
							THROW_MPF_EXC_FWD(nullptr);
							}
						}
					}
				else if (m.has_exception()) {
					exchndl(std::move(m).exception());
					}
				else {
					exchndl(CREATE_MPF_PTR_EXC_FWD(nullptr));
					}
				}
			catch (const dcf_exception_t & e0) {
				exchndl(e0.dcpy());
				}
			catch (const std::exception & e1) {
				exchndl(CREATE_MPF_PTR_EXC_FWD(e1));
				}
			catch (...) {
				exchndl(CREATE_MPF_PTR_EXC_FWD(nullptr));
				}
			};


		make_async_sndrcv_once_sys(ctx_,
			std::move(peer_),
			std::move(reqCtr),
			std::move(resHndl),
			__FILE_LINE__,
			ctx_->policies().connection_timepoints(),
			launch_as::async);
		}
	}

void __rt0_ipeer_table_t::evhndl_connection_tail_register(peer_link_t && peer_,
	crm::datagram_t && initializeResult,
	std::unique_ptr<execution_bool_tail> && registerCompleted) {

	if(peer_.valid()) {

		CBL_MPF_TRACELEVEL_EVENT_RT0_CONNECTION_TRACE("in-peer[0]:register:" + peer_.remote_object_endpoint().to_string());

		/* �������� � ������� */
		CBL_VERIFY(!peer_.id().is_null());

		/* �������� � ������� ����������� */
		insert(peer_);

		peer_.invoke_event(event_type_t::st_connected);

		/* ����������� ������� ������������ */
		execute_exthndl_connection(std::move(peer_),
			std::move(initializeResult),
			std::move(registerCompleted));
		}
	else {
		THROW_EXC_FWD(nullptr);
		}
	}

void __rt0_ipeer_table_t::evhndl_connection(peer_link_t && peer_,
	syncdata_list_t && sl,
	std::unique_ptr<execution_bool_tail> && registerCompleted) {

	CBL_MPF_TRACELEVEL_EVENT_RT0_CONNECTION_TRACE("in-peer[0]:route register:" + peer_.remote_object_endpoint().to_string());

	/* ������� ���������� ��������� ������������� ��������� ������� */
	evhndl_connection_initialize(std::move(peer_), std::move(sl), std::move(registerCompleted));
	}

void __rt0_ipeer_table_t::evhndl_disconnection(const xpeer_desc_t & desc,
	std::shared_ptr<i_peer_statevalue_t> && stv) {

	CHECK_NO_EXCEPTION(remove(desc));
	CHECK_NO_EXCEPTION(execute_exthndl_disconnection(desc, std::move(stv)));
	}

void __rt0_ipeer_table_t::start(const std::unique_ptr<i_endpoint_t> & ep) {

	std::weak_ptr<self_t> wthis(shared_from_this());
	auto sourceConnectionHndl = [wthis](std::unique_ptr<i_xpeer_source_t> && source) {
		post_scope_action_t sourceGuard([&source] {
			if(source) {
				CHECK_NO_EXCEPTION(source->close());
				}
			});

		auto sthis(wthis.lock());
		if(sthis) {

			/* ���������� �������� ������� ���� */
			auto peerClosedHandler = [wthis]
			(const i_xpeer_rt0_t::xpeer_desc_t & id, closing_reason_t /*reason*/, std::shared_ptr<i_peer_statevalue_t> && stv) {

				auto sthis(wthis.lock());
				if(sthis)
					sthis->evhndl_disconnection(id, std::move(stv));
				};

			/* ������� �������� ��������� ����������������� ������ */
			auto localIdCtr = sthis->_localIdCtr;

			/* �������� �������� ����������������� ������ ��������� ���� */
			auto remoteChecker = sthis->_remoteIdChecker;

			/* ������� ��������� ������ �� ���������� ��������� ������������� */
			auto handshakeOk = [wthis]
			(std::unique_ptr<dcf_exception_t> && exc,
				peer_link_t && peer_,
				syncdata_list_t && sl,
				std::unique_ptr<execution_bool_tail> && registerCompleted) {

				post_scope_action_t checkPeerAlived([&peer_] {
					CHECK_NO_EXCEPTION(peer_.close());
					});

				auto sthis(wthis.lock());
				if(sthis) {
					if(!exc) {
						sthis->evhndl_connection(std::move(peer_),
							std::move(sl),
							std::move(registerCompleted));
						checkPeerAlived.reset();
						}
					else {
						sthis->ntf_hndl(std::move(exc));
						}
					}
				};

			/* �������� ������� �������������� */
			if( sthis->_protocol ) {

					auto dcd = std::make_unique<binary_object_cvt_t>(sthis->ctx(), sthis->_protocol);
					auto evhndl = sthis->_evhndl;

					/* �������� ������� ��������� ����������� */
					istream_rt0_x_t::accept(sthis->_ioCtx,
						std::move(source),
						std::move(dcd),
						std::move(peerClosedHandler),
						std::move(localIdCtr),
						std::move(remoteChecker),
						std::move(handshakeOk),
						sthis->_thisId,
						sthis->input_stock(),
						sthis->_stvalCtr(),
						std::move(evhndl));

					sourceGuard.reset();
				}
			else
				THROW_MPF_EXC_FWD(nullptr);
			}
		};

	CHECK_PTR(_connectionFactory)->open(std::move(sourceConnectionHndl), ep);
	}

void __rt0_ipeer_table_t::close()noexcept {
	_cancelf.store(true, std::memory_order::memory_order_release);

	if(_ismMap)
		_ismMap->close();

	if(_connectionFactory)
		_connectionFactory->close();
	}

void __rt0_ipeer_table_t::execute_exthndl_connection(peer_link_t && peer,
	crm::datagram_t && initializeResult,
	std::unique_ptr<execution_bool_tail> && registerCompleted)noexcept {

	/* ����� ����������������� ����������� ��������� ����������� � ��������� ������ */
	connection_hndl_t hndl(_xhndl_Connection);
	if(hndl) {
		auto ctx_(ctx());
		if(ctx_) {

			CBL_MPF_TRACELEVEL_EVENT_RT0_CONNECTION_TRACE("in-peer[0]:event handler connection:" + peer.remote_object_endpoint().to_string());

			ctx_->launch_async(__FILE_LINE__, []
			(connection_hndl_t && hndl_,
				peer_link_t && peer_,
				crm::datagram_t && initializeResult,
				std::unique_ptr<execution_bool_tail> && registerCompleted) {

				hndl_(std::move(peer_), std::move(initializeResult));

				/* ������������� ����������� ������� */
				if(registerCompleted)
					registerCompleted->commite();

				}, std::move(hndl),
					std::move(peer),
					std::move(initializeResult),
					std::move(registerCompleted));
			}
		}
	}

void __rt0_ipeer_table_t::execute_exthndl_disconnection(const xpeer_desc_t & desc,
	std::shared_ptr<i_peer_statevalue_t> && stv)noexcept {

	/* ����� ����������������� ����������� ��������� ���������� � ��������� ������ */
	disconnection_hndl_t hndl(_xhndl_Disconnection);
	if(hndl) {
		auto ctx_(ctx());
		if(ctx_) {

			CBL_MPF_TRACELEVEL_EVENT_RT0_CONNECTION_TRACE("in-peer[0]:router:event handler disconnection:" + desc.to_str());

			ctx_->launch_async(__FILE_LINE__, [desc]
			(disconnection_hndl_t && hndl_, std::shared_ptr<i_peer_statevalue_t> && stv) {

				hndl_(desc, std::move(stv));

				}, std::move(hndl), std::move(stv));
			}
		}
	}

std::shared_ptr<default_binary_protocol_handler_t> __rt0_ipeer_table_t::protocol()const noexcept {
	return _protocol;
	}
