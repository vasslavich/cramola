#pragma once


#include <map>
#include <set>
#include <memory>
#include <appkit/appkit.h>
#include "./rt0_otbl_hndl.h"
#include <appkit/xpeer/rt0_end_opeer.h>
#include <appkit/xpeer/rt0_outbound_cnsettings.h>


namespace crm::detail{


class __rt0_opeer_table_t :
	public std::enable_shared_from_this<__rt0_opeer_table_t> {

private:
	typedef std::mutex lck_t;
	typedef std::unique_lock<lck_t> lck_scp_t;
	typedef __rt0_opeer_table_t self_t;

public:
	using peer_handler = rt0_opeer_handler_t;
	typedef ostream_rt0_x_t::xpeer_desc_t xpeer_desc_t;
	typedef ostream_rt0_x_t::post_connection_t post_connection_t;
	typedef ostream_rt0_x_t::close_event_hndl_t close_event_hndl_t;
	typedef ostream_rt0_x_t::ostream_lnk_t ostream_lnk_t;
	typedef ostream_rt0_x_t::link_t peer_link_t;
	using subscribe_event_connection_t = subscribe_event_connection_t<peer_link_t>;
	using connection_create_hndl_t = std::function<void( std::unique_ptr<dcf_exception_t> && exc, peer_link_t && p )>;

private:
	std::weak_ptr<distributed_ctx_t> _ctx;
	identity_descriptor_t _thisId;
	std::shared_ptr<i_rt0_main_router_input_t> _targetStock;
	local_object_create_connection_t _idCtr;
	remote_object_check_identity_t _idChecker;
	std::shared_ptr<default_binary_protocol_handler_t> _protocol;
	object_initialize_handler_t _outXInit;
	std::shared_ptr<i_xpeer_source_factory_t> _xpFct;

	std::atomic<bool> _cancelf = false;

	/*! ������� ��������� ����������� */
	std::map<connection_key_t, std::weak_ptr<ostream_lnk_t >> _tblCn;
	/*! ������� ������ ������ ����������� */
	std::map<source_object_key_t, std::shared_ptr<ostream_lnk_t>> _tblFullKey;
	mutable lck_t _tblLck;

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	mutable crm::utility::__xvalue_counter_logger_t<1> __sizeTraceer = __FILE_LINE__;
	crm::utility::__xobject_counter_logger_t<self_t,1> _xDbgCounter;
	void __size_trace()const noexcept;
#endif

	void close( std::shared_ptr<ostream_lnk_t>  && obj, bool lazy = true )noexcept;
	bool closed()const noexcept;

	std::shared_ptr<i_xpeer_source_factory_t> peer_factory()const noexcept;
	std::shared_ptr<i_rt0_main_router_input_t> target_stock();
	void _remove_all()noexcept;
	std::shared_ptr<ostream_lnk_t>  _find( const connection_key_t & ck )const noexcept;

	void _bind_id( const source_object_key_t & srcKey,
		std::shared_ptr<ostream_lnk_t> lnk );
	void _unbind_id( std::vector<source_object_key_t> && srcKeys )noexcept;
	void _unbind_id( const connection_key_t & ck )noexcept;

	std::shared_ptr<ostream_lnk_t> __exists( const source_object_key_t & srcKey )noexcept;
	std::shared_ptr<ostream_lnk_t> __exists(const connection_key_t& cKey)noexcept;

	void _bind_connection( const source_object_key_t & srcKey,
		std::unique_ptr<subscribe_event_connection_t> && postHndl );

	std::pair<std::shared_ptr<ostream_lnk_t>, bool> _create_connection( const source_object_key_t & srcKey,
		connection_create_hndl_t && postHndl,
		close_event_hndl_t && closeHndl,
		std::function<std::shared_ptr<i_peer_statevalue_t>()> && stvalctr,
		exceptions_suppress_checker_t && connectionExcChecker,
		__event_handler_point_f&& evhndl);

	ostream_lnk_t _create_connection(
		ostream_rt0_x_t::post_connection_t && postHndl,
		ostream_rt0_x_t::close_event_hndl_t && closeHndl,
		std::function<std::shared_ptr<i_peer_statevalue_t>()>&& stvalctr,
		exceptions_suppress_checker_t&& connectionExcChecker,
		object_initialize_handler_t && inihndl,
		const identity_descriptor_t &thisId,
		local_object_create_connection_t && locIdCtr,
		remote_object_check_identity_t && remoteIdChecker,
		std::unique_ptr<i_endpoint_t> && ep,
		__event_handler_point_f&& evhndl,
		std::unique_ptr<i_input_messages_stock_t> && inputStock );

	self_t(std::weak_ptr<distributed_ctx_t> ctx_,
		const identity_descriptor_t & thisId_,
		std::shared_ptr<i_rt0_main_router_input_t> && inputStock_,
		local_object_create_connection_t && idCtr,
		remote_object_check_identity_t && idChecker,
		std::shared_ptr<default_binary_protocol_handler_t> protocol,
		object_initialize_handler_t && outXInit,
		std::shared_ptr<i_xpeer_source_factory_t> xpFct );

	std::shared_ptr<ostream_lnk_t> find( const connection_key_t & rmdKey )const noexcept;
	std::unique_ptr<rt0_opeer_handler_t> find_handler( const connection_key_t & rmdKey )const noexcept;

	void _close()noexcept;
	void __unsubscribe_event_handler( const connection_key_t & cnk, const _address_event_t & id )noexcept;
	std::shared_ptr<__rt0_opeer_table_t::ostream_lnk_t> __find_peer( const connection_key_t & cnk )const noexcept;

public:
	void push( rt1_x_message_t && pck  );

	template<typename O,
		typename std::enable_if_t < is_out_message_requirements_v<O>, int> = 0>
	void push(const rt0_node_rdm_t& nodeId, O && o) {
		auto h = find_handler(nodeId.connection_key());
		if(h) {
			h->push(std::forward<O>(o));
			}
		else {
			THROW_MPF_EXC_FWD(nullptr);
			}
		}

private:
	std::shared_ptr<ostream_lnk_t>  __capture_plink( const source_object_key_t & sourceKey )noexcept;
	std::unique_ptr<rt0_opeer_handler_t> __capture_link( const source_object_key_t & sourceKey )noexcept;

public:
	static std::shared_ptr<self_t> create(std::weak_ptr<distributed_ctx_t> ctx_,
		const identity_descriptor_t & thisId_,
		std::shared_ptr<i_rt0_main_router_input_t> && inputStock_,
		local_object_create_connection_t && idCtr_,
		remote_object_check_identity_t && idChecker_,
		std::shared_ptr<default_binary_protocol_handler_t> protocol,
		object_initialize_handler_t && outXInit,
		std::shared_ptr<i_xpeer_source_factory_t> xpFct);

	~__rt0_opeer_table_t();

	void close()noexcept;

	std::shared_ptr<__rt0_opeer_table_t::ostream_lnk_t> exists( const source_object_key_t & srcKey )noexcept;
	void bind_connection( const source_object_key_t & srcKey,
		std::unique_ptr<subscribe_event_connection_t> && postHndl );
	std::pair<std::shared_ptr<ostream_lnk_t>, bool> create_connection( const source_object_key_t & srcKey,
		connection_create_hndl_t && postHndl,
		close_event_hndl_t && closeHndl,
		std::function<std::shared_ptr<i_peer_statevalue_t>()> && stvalctr,
		exceptions_suppress_checker_t && connectionExcChecker,
		__event_handler_point_f&& evhndl);

	void unbind_connection( const connection_key_t& epKey )noexcept;
	void unbind_connection( std::vector<source_object_key_t> && srcKeys )noexcept;

	void unsubscribe_event_handler( const connection_key_t & cnk, const _address_event_t & id )noexcept;

	std::unique_ptr<rt0_opeer_handler_t> capture_link( const source_object_key_t & sourceKey )noexcept;

	template<typename THandler,
		typename std::enable_if_t<(
			std::is_invocable_v<THandler, std::unique_ptr<i_rt0_peer_handler_t>>), int> = 0>
	void subscribe2peer(const rtl_roadmap_obj_t & rdm,
		THandler && h) {

		h(capture_link(rdm.source_object_key()));
		}

	std::unique_ptr<rt0_opeer_handler_t> find_peer( const rt0_node_rdm_t & rdm )const noexcept;
	std::shared_ptr<default_binary_protocol_handler_t> protocol()const noexcept;

	opeer_l0 create_out_link(std::function<std::shared_ptr<i_peer_statevalue_t>()> &&,
		std::shared_ptr<rt0_connection_settings_t>);
	};
}


