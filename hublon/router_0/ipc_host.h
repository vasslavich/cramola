#pragma once


#include "./rt0_main.h"
#include "./ipc_host_utility.h"
#include "./ipc_host_hubtbl.h"
#include "../unitcmd/cmdl.h"


namespace crm::detail{


class xchannel_shmem_host_first;
class ipc_host final : 
	public ipc_host_trait,
	public std::enable_shared_from_this<ipc_host>{

private:
	std::shared_ptr<distributed_ctx_t> _ctx;
	std::unique_ptr<i_xchannel_shmem_host_first> _channel;
	ipc_host_map_of_hubs _hubsTable;
	std::shared_ptr<rt0_cntbl_t> _route;
	std::shared_ptr<i_message_input_t<message_stamp_t>> _inputStack;
	//std::shared_ptr< tcp::tcp_source_factory_t> _socketFactory;
	std::shared_ptr<crm::default_binary_protocol_handler_t> _protocol;
	std::thread _channelListenTh;
	std::atomic<bool> _stopf{ false };

	void listen_channel_body()noexcept;
	xchannel_shmem_host_first* xchannel_first()const noexcept;
	std::shared_ptr<rt0_cntbl_t> router()const noexcept;
	//std::shared_ptr< tcp::tcp_source_factory_t> socket_factory()const noexcept;
	std::shared_ptr<crm::default_binary_protocol_handler_t> protocol()const noexcept;

	using io_break_token = break_cancel_timed<std::chrono::milliseconds::rep, std::chrono::milliseconds::period>;
	io_break_token mk_cancel_token(const std::chrono::milliseconds & timeout)const;

	void handle_channel_hub(rt1_x_message_data_t && m);
	void handle_channel_hub(rt0_x_command_pack_t && m);
	void handle_channel_strob( std::vector<uint8_t> && strob );
	void handle( unit_connect && ucn );
	void handle( unit_disconnect && ucn )noexcept;

	void start_io( std::weak_ptr< xchannel_shmem_hub > hub );
	//std::future<void> do_cycle_read_commands( std::weak_ptr< xchannel_shmem_hub > hub );
	//std::future<void> do_cycle_read_messages( std::weak_ptr< xchannel_shmem_hub > hub );
	//void close_hub( std::weak_ptr< xchannel_shmem_hub > hub );

	void handle_ntf( std::unique_ptr<dcf_exception_t> && n )const noexcept;

	static std::unique_ptr<i_xchannel_shmem_host_first> mkchannel(std::shared_ptr<distributed_ctx_t> c,
		const xchannel_shmem_host_args & ca);
	
	template<typename Options>
	void mkstate(std::shared_ptr<distributed_ctx_t> c, Options && opt) {
		_ctx = c;
		_channel = mkchannel(_ctx, opt.channel_options());

		_protocol = opt.protocol();
		_inputStack = opt.input_stack();
		//_socketFactory = opt.ipeer_factory();
		_route = rt0_cntbl_t::create(std::forward<Options>(opt));
		}

	template<typename Options>
	void constructor(std::shared_ptr<distributed_ctx_t> c, Options && options) {
		mkstate(c, std::forward< Options>(options));
		}

	std::shared_ptr<distributed_ctx_t> ctx()const noexcept;

public:
	~ipc_host();

	ipc_host()noexcept;
	ipc_host(const ipc_host&) = delete;
	ipc_host& operator=(const ipc_host&) = delete;

	template<typename Options,
		typename std::enable_if_t<is_ipc_hosh_options_v<Options>, int> = 0>
	static std::shared_ptr<ipc_host> make(std::shared_ptr<distributed_ctx_t> c, Options && options) {
		auto pThat = std::make_shared<ipc_host>();
		pThat->constructor(c, std::forward<Options>(options));
		return pThat;
		}

	void close()noexcept;
	void start(const ipc_host_start_options  & opt);
	void stop()noexcept;
	bool stopped()const noexcept;

	template<typename _TStrobMessage,
		typename TStrobMessage = typename std::decay_t<_TStrobMessage>,
		typename std::enable_if_t<srlz::detail::is_strob_serializable_v<TStrobMessage>, int> = 0>
	void register_message_type() {
		if (auto c = ctx()) {
			c->typemap().add_drv_type<detail::istrob_message_envelop_t<TStrobMessage>>();
			}
		else {
			THROW_EXC_FWD(nullptr);
			}
		}
	};
}