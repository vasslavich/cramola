#pragma once


#include <map>
#include <memory>
#include <functional>
#include <appkit/appkit.h>
#include <appkit/export_internal_terms.h>
#include "./rt0_itbl_hndl.h"


namespace crm::detail{


class peer_table_t {
private:
	typedef std::mutex lck_t;
	typedef std::unique_lock<lck_t> lck_scp_t;

public:
	typedef xpeer_base_rt0_t::link_t istream_link_t;

private:
	/*! ������� ��������-�������� ����������� ������ 0 */
	typedef	std::map<rt0_node_rdm_t/* ��������� ���������� ������������� ������� */,
		istream_link_t/* ����� �����/������ ��������� � �������� */> nodes_map_glid_t;

	/*! �������� */
	std::weak_ptr<distributed_ctx_t> _ctx;
	/*! ����� ����� */
	nodes_map_glid_t _map;
	mutable lck_t _mapLck;

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	mutable crm::utility::__xvalue_counter_logger_t<1> __sizeTraceer = __FILE_LINE__;

	void __size_trace()const noexcept;
#endif

	void close_object(istream_link_t && obj, bool lazy = true)noexcept;
	void _insert(const rt0_node_rdm_t &identity, istream_link_t && n);

	/*! ����� ����, �� ������������������ �������� */
	istream_link_t _find(const rt0_node_rdm_t & id)const noexcept;

	/*! ����� ����, �� ������������������ �������� */
	std::unique_ptr<rt0_ipeer_handler_t<istream_link_t>> _find_handler(const rt0_node_rdm_t & id)const noexcept;

	/*! �������� � ����, �� ������������������ �������� */
	template<typename THandler>
	void _subscribe_transaction(const rt0_node_rdm_t & id, THandler && h) {

		auto it(_map.find(id));
		if(it != _map.end()) {
			h(std::make_unique<rt0_ipeer_handler_t<istream_link_t>>(it->second));
			}
		else {
			h(nullptr);
			}
		}

	/*! ������� ���� �� �������, �� ������������������ �������� */
	void _remove(const rt0_node_rdm_t & id)noexcept;

	void _remove_all()noexcept;

	void _close()noexcept;

public:
	peer_table_t(std::weak_ptr<distributed_ctx_t> ctx_)noexcept;
	~peer_table_t();

	void close()noexcept;

	void insert(const rt0_node_rdm_t & identity, istream_link_t && n);
	istream_link_t find(const rt0_node_rdm_t & identity)const noexcept;

	/*! ������� ���� �� ������� */
	void remove(const rt0_node_rdm_t & id)noexcept;

	/*! �������� � ����, �� ������������������ �������� */
	template<typename THandler>
	void subscribe_transaction(const rt0_node_rdm_t & id, THandler && h) {
		if constexpr(is_bool_cast_v<THandler>) {
			if(!h) {
				THROW_MPF_EXC_FWD(nullptr);
				}
			}

		lck_scp_t lck(_mapLck);
		_subscribe_transaction(id, std::forward<THandler>(h));
		}

	std::unique_ptr<rt0_ipeer_handler_t<istream_link_t>> find_handler(const rt0_node_rdm_t & id)const noexcept;
	};


/*! ������� �������� ����������� */
class __rt0_ipeer_table_t :
	public std::enable_shared_from_this<__rt0_ipeer_table_t> {

private:
	typedef std::mutex lck_t;
	typedef std::unique_lock<lck_t> lck_scp_t;

public:
	using peer_handler = rt0_ipeer_handler_t<peer_table_t::istream_link_t>;
	typedef __rt0_ipeer_table_t self_t;
	typedef xpeer_base_rt0_t::link_t peer_link_t;
	typedef peer_link_t::xpeer_desc_t xpeer_desc_t;
	typedef peer_link_t::input_value_t input_value_t;
	typedef peer_link_t::output_value_t output_value_t;
	typedef std::function<void(std::unique_ptr<i_xpeer_source_t> && source)> evhndl_source_connection_t;
	typedef std::function<bool(const input_value_t & obj)> check_identity_value_t;
	typedef std::function<bool()> canceled_t;
	typedef std::function<void( peer_link_t && lnk, crm::datagram_t && initializeResult )> connection_hndl_t;
	typedef std::function<void( const xpeer_desc_t &, std::shared_ptr<i_peer_statevalue_t> && )> disconnection_hndl_t;

	/*! ��������� ���� */
	enum class node_tag_state_t {
		idle,
		dowork,
		faulted
		};

private:
	/*! �������� */
	std::weak_ptr<distributed_ctx_t> _ioCtx;
	/*! identity_descriptor_t ������� ������� */
	identity_descriptor_t _thisId;
	/*! ������� �������� */
	std::unique_ptr<peer_table_t> _ismMap;
	/*! ������� ������ ��������� */
	canceled_t _cancelHndl;
	/*! ������� ������ */
	std::atomic<bool> _cancelf = false;
	local_object_create_connection_t _localIdCtr;
	remote_object_check_identity_t _remoteIdChecker;
	create_remote_object_initialize_list_t _remoteInzr;
	/*! ���� ������ ��� �������� ��������� �� ���� */
	std::weak_ptr<i_rt0_main_router_input_t> _inputStock;
	/*! ������� ����������� */
	std::shared_ptr <i_xpeer_source_factory_t> _connectionFactory;
	__event_handler_point_f _evhndl;

	connection_hndl_t _xhndl_Connection;
	disconnection_hndl_t _xhndl_Disconnection;
	std::shared_ptr<default_binary_protocol_handler_t> _protocol;
	i_xpeer_rt0_t::ctr_0_input_queue_t _inQCtr;
	std::function<std::shared_ptr<i_peer_statevalue_t>()> _stvalCtr;

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	crm::utility::__xobject_counter_logger_t<self_t,1> _xDbgCounter;
#endif

	std::shared_ptr<distributed_ctx_t> ctx()const noexcept;

	void ntf_hndl( std::unique_ptr<dcf_exception_t> && ntf )noexcept;
	void ntf_hndl( const dcf_exception_t & exc )noexcept;

	void execute_exthndl_connection( peer_link_t && peer_,
		crm::datagram_t && initializeResult,
		std::unique_ptr<execution_bool_tail> && registerCompleted )noexcept;
	void execute_exthndl_disconnection( const xpeer_desc_t & desc,
		std::shared_ptr<i_peer_statevalue_t> && stv )noexcept;

	/*! ������ ��������� */
	bool canceled()const noexcept;

	std::shared_ptr<i_rt0_main_router_input_t> input_stock()const noexcept;

	void insert( peer_link_t && peer );
	void insert( const peer_link_t & peer );

	peer_link_t find( const rt0_node_rdm_t & id )noexcept;

	void remove(const rt0_node_rdm_t & id)noexcept;

	void evhndl_connection_initialize( peer_link_t && peer_, 
		syncdata_list_t && sl,
		std::unique_ptr<execution_bool_tail> && registerCompleted );

	void evhndl_connection_tail_register( peer_link_t && peer_, 
		crm::datagram_t && initializeResult,
		std::unique_ptr<execution_bool_tail> && registerCompleted );

	void evhndl_connection(peer_link_t && peer_,
		syncdata_list_t && sl, 
		std::unique_ptr<execution_bool_tail> && registerCompleted );

	void evhndl_disconnection( const xpeer_desc_t & desc,
		std::shared_ptr<i_peer_statevalue_t> && stv );

	/*! �������� ��������� �����
	\param imId ������� ������������� ���
	\param inputStck ���� �������� ��������� �� ����� (����������� � ��������, ��� ������� �� ������ ���������� )
	\param excHndl ���������� ����������
	\param cancelHdnl ������� �������� ������ ���������
	*/
	__rt0_ipeer_table_t(std::weak_ptr<distributed_ctx_t> ctx_,
		const identity_descriptor_t & thisId,
		canceled_t && cancelHndl,
		local_object_create_connection_t && localIdCtr,
		remote_object_check_identity_t && remoteIdChecker,
		create_remote_object_initialize_list_t && removeInzr,
		std::weak_ptr<i_rt0_main_router_input_t> inputStock,
		std::shared_ptr <i_xpeer_source_factory_t> ifactory,
		connection_hndl_t && xhndl_Connection,
		disconnection_hndl_t && xhndl_Disconnection,
		std::shared_ptr<default_binary_protocol_handler_t> protocol,
		i_xpeer_rt0_t::ctr_0_input_queue_t && inCtr,
		std::function<std::shared_ptr<i_peer_statevalue_t>()> && stvalCtr ,
		__event_handler_point_f && eh);

	void start( const std::unique_ptr<i_endpoint_t> & ep );

public:
	static std::shared_ptr<__rt0_ipeer_table_t> create(std::weak_ptr<distributed_ctx_t> ctx,
		const identity_descriptor_t & thisId,
		canceled_t && cancelHndl,
		local_object_create_connection_t && localIdCtr,
		remote_object_check_identity_t && remoteIdChecker,
		create_remote_object_initialize_list_t && removeInzr,
		std::weak_ptr<i_rt0_main_router_input_t> inputStock,
		std::shared_ptr <i_xpeer_source_factory_t> ifactory,
		connection_hndl_t && xhndl_Connection,
		disconnection_hndl_t && xhndl_Disconnection,
		std::shared_ptr<default_binary_protocol_handler_t> protocol,
		i_xpeer_rt0_t::ctr_0_input_queue_t && inCtr,
		std::function<std::shared_ptr<i_peer_statevalue_t>()> && stvalCtr,
		const std::unique_ptr<i_endpoint_t> & ep,
		__event_handler_point_f && eh);

	~__rt0_ipeer_table_t();

	void push(rt1_x_message_t && env);

	template<typename O,
		typename std::enable_if_t < is_out_message_requirements_v<O>, int> = 0>
	void push(const rt0_node_rdm_t& nodeId, O && o) {
		if (_ismMap) {
			auto h = _ismMap->find_handler(nodeId);
			if (h) {
				h->push(std::forward<O>(o));
				}
			else {
				THROW_MPF_EXC_FWD(nullptr);
				}
			}
		else {
			THROW_MPF_EXC_FWD(nullptr);
			}
		}

	void close()noexcept;

	template<typename THandler,
		typename std::enable_if_t<(
			std::is_invocable_v<THandler, std::unique_ptr<i_rt0_peer_handler_t>>), int> = 0>
	void subscribe2peer(const rtl_roadmap_obj_t & rdm,
		THandler && h) {
		
		if (_ismMap) {
			_ismMap->subscribe_transaction(rdm.rt0(), std::forward<THandler>(h));
			}
		else {
			THROW_MPF_EXC_FWD(nullptr);
			}
		}

	std::unique_ptr<peer_handler> find_peer( const rt0_node_rdm_t & rdm )const noexcept;
	std::shared_ptr<default_binary_protocol_handler_t> protocol()const noexcept;
	};
}

