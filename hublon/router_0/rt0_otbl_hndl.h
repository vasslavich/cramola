#pragma once


#include <appkit/export_internal_terms.h>


namespace crm::detail{


class rt0_opeer_handler_t : public i_rt0_peer_handler_t {
private:
	std::shared_ptr<ostream_rt0_x_t::ostream_lnk_t> _pl;
	std::function<void( const rt0_node_rdm_t & desc )> _closeHndl;

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	crm::utility::__xobject_counter_logger_t<rt0_opeer_handler_t,1> _xDbgCounter;
#endif

public:
	rt0_opeer_handler_t( std::shared_ptr<ostream_rt0_x_t::ostream_lnk_t>  && pl );

	void push(rt1_x_message_t&& package)final;

	template<typename O,
		typename std::enable_if_t < is_out_message_requirements_v<O>, int> = 0>
	void push(O && o) {

		if (is_null(o.destination_id())) {
			THROW_MPF_EXC_FWD(nullptr);
			}

		if (is_opened()) {
			if (_pl->state() == remote_object_state_t::impersonated) {
				_pl->push(std::forward<O>(o));
				}
			else {
				THROW_MPF_EXC_FWD("!remote_object_state_t::impersonated");
				}
			}
		else {
			THROW_MPF_EXC_FWD(nullptr);
			}
		}

	bool is_opened()const noexcept final;

	rtl_table_t direction()const noexcept {
		return rtl_table_t::out;
		}
	};
}

