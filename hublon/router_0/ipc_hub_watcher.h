#pragma once

#include <appkit/appkit.h>
#include "../xchannel/smp/shmem_hub.h"

namespace crm::detail{


class xchannel_shmem_hub_watcher{
	std::shared_ptr<xchannel_shmem_hub> _hub;
	std::thread _thM;
	std::thread _thC;
	std::weak_ptr<distributed_ctx_t> _ctx;

	std::atomic<bool> _cancelf{ false };

	void handle_ntf( std::unique_ptr<dcf_exception_t> && n )const noexcept;
	bool stopped()const noexcept;

public:
	~xchannel_shmem_hub_watcher();

	void open( std::shared_ptr<xchannel_shmem_hub> hub,
		std::function<void( rt1_x_message_data_t && m )> && mh,
		std::function<void( rt0_x_command_pack_t && c )> && ch,
		std::weak_ptr<distributed_ctx_t> ctx );

	void close()noexcept;
	std::shared_ptr<xchannel_shmem_hub> hub()const noexcept;
	};
}