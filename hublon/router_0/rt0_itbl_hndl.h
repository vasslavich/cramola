#pragma once


#include <appkit/appkit.h>


namespace crm::detail {


template<typename _TStream>
class rt0_ipeer_handler_t : public i_rt0_peer_handler_t {
public:
	using self_t = rt0_ipeer_handler_t<_TStream>;
	using stream_t = typename std::decay_t<_TStream>;

private:
	stream_t _pl;
	std::function<void(const rt0_node_rdm_t& desc)> _closeHndl;

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	crm::utility::__xobject_counter_logger_t<self_t, 1> _xDbgCounter;
#endif

public:
	template<typename XStream>
	rt0_ipeer_handler_t(typename XStream&& pl)
		: _pl(std::forward<XStream>(pl)) {}

	rt0_ipeer_handler_t() {}

	void push(rt1_x_message_t&& messagePack) {
		if (is_opened()) {

			if (_pl.state() == remote_object_state_t::impersonated) {

				if (_pl.desc() == messagePack.rdm().points.rt0()) {
					_pl.write(std::move(messagePack).package());
					}
				else {
					THROW_MPF_EXC_FWD(nullptr);
					}
				}
			else {
				THROW_MPF_EXC_FWD(L"!remote_object_state_t::impersonated");
				}
			}
		else {
			THROW_MPF_EXC_FWD(nullptr);
			}
		}

	template<typename O,
		typename std::enable_if_t < is_out_message_requirements_v<O>, int> = 0>
		void push(O && message) {
		if (is_null(message.destination_id())) {
			THROW_MPF_EXC_FWD(nullptr);
			}

		if (is_opened()) {
			if (_pl.state() == remote_object_state_t::impersonated) {
				_pl.push(std::forward<O>(message));
				}
			else {
				THROW_MPF_EXC_FWD("!remote_object_state_t::impersonated");
				}
			}
		else {
			THROW_MPF_EXC_FWD(nullptr);
			}
		}

	bool is_opened()const noexcept {
		return _pl.is_opened();
		}

	rtl_table_t direction()const noexcept {
		return rtl_table_t::in;
		}
	};
}

