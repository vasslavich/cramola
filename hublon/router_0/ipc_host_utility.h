#pragma once


#include <appkit/appkit.h>
#include "../xchannel/smp/shmem_reg.h"
#include "../xchannel/i_xchannel.h"

namespace crm{


struct ipc_host_trait{
	virtual ~ipc_host_trait(){}

	using exception_handler = std::function<void( std::unique_ptr<dcf_exception_t> && )>;
	};


struct ipc_host_start_options{
	constexpr static size_t default_mem_size = CRM_MB;
	size_t mem_size{ default_mem_size };
	};

class ipc_host_options : public rt0_hub_settings_t {
private:
	detail::xchannel_shmem_host_args _channelOpt;

public:
	ipc_host_options(std::shared_ptr<distributed_ctx_t> c, 
		const identity_descriptor_t& thisId,
		crm::detail::tcp::tcp_endpoint_t&& ep);

	void set_channel_options( detail::xchannel_shmem_host_args && channelOpt );
	const detail::xchannel_shmem_host_args& channel_options()const noexcept;
	};

template<typename T, typename = std::void_t<>>
struct is_ipc_hosh_options : std::false_type {};

template<typename T>
struct is_ipc_hosh_options<T, std::void_t<
	std::enable_if_t<std::is_base_of_v<ipc_host_options, T>>
	>> : std::true_type{};

template<typename T>
constexpr bool is_ipc_hosh_options_v = is_ipc_hosh_options<T>::value;
}
