#include <appkit/appkit.h>
#include <hublon/r0.h>
#include <mutex>
#include <condition_variable>
#include <atomic>
#include <random>
#include "../../../demo_utilities/utilities.h"

using namespace std::chrono;


auto localhostId_RT0 = crm::identity_descriptor_t{ crm::global_object_identity_t::as_hash("FAFA0000-0A0A-F1F1-AAAA-100000000001"), "host:0" };
short port = 16789;

std::atomic<bool> streamReadySendf{ false };
std::atomic<bool> streamReadyRecvf{ false };
std::condition_variable cvStreamReadySend;
std::condition_variable cvStreamReadyRecv;
std::mutex mxLock;

/*! example of custom stream header(cover) data structure */
struct stream_report_data final/* for serialization requirements */ 
	: crm::i_stream_zyamba_cover_data_t/* for streaming cover data requirements */  {

	std::string senderName;
	std::string submissionSummary;
	size_t countBytes{ 0 };
	std::chrono::system_clock::time_point initialTime = std::chrono::system_clock::now();

	/*! i_stream_zyamba_cover_data_t implementaion */
	const crm::srlz::i_typeid_t& stream_typeid()const noexcept {
		static const auto stream_blob_typeid = crm::srlz::typemap_key_t::make<stream_report_data>();
		return stream_blob_typeid;
		}

	/*! for streaming cover data requirements */
	crm::address_hash_t object_hash()const noexcept {
		crm::address_hash_t h;
		crm::apply_at_hash(h, senderName);
		crm::apply_at_hash(h, submissionSummary);
		crm::apply_at_hash(h, countBytes);
		crm::apply_at_hash(h, initialTime);

		return h;
		}

	/* serialization support 
	========================================================== */

	/*! for serialization */
	template<typename S>
	void srlz(S&& dest)const {
		crm::srlz::serialize(dest, senderName);
		crm::srlz::serialize(dest, submissionSummary);
		crm::srlz::serialize(dest, countBytes);
		crm::srlz::serialize(dest, initialTime);
		}

	/*! for deserialization */
	template<typename S>
	void dsrlz(S&& src) {
		crm::srlz::deserialize(src, senderName);
		crm::srlz::deserialize(src, submissionSummary);
		crm::srlz::deserialize(src, countBytes);
		crm::srlz::deserialize(src, initialTime);
		}

	/*! member function if cheap length calculation of instance */
	size_t get_serialized_size()const noexcept {
		return crm::srlz::object_trait_serialized_size(senderName) +
			crm::srlz::object_trait_serialized_size(submissionSummary) +
			crm::srlz::object_trait_serialized_size(countBytes) +
			crm::srlz::object_trait_serialized_size(initialTime);
		}
	};
static_assert(crm::srlz::is_serializable_v<stream_report_data>, __FILE_LINE__);
static_assert(crm::is_stream_cover_data_trait_v<stream_report_data>, __FILE_LINE__);


using test_container = std::vector<uint8_t>;
using test_stream_trait = crm::stream_from_container<test_container>;

test_container testStreamData;

/*! send stream with a header */
template<typename TPeer>
void send_stream(TPeer&& peer) {

	/* end of streaming handler */
	auto endh = [](crm::async_operation_result_t st,
		std::unique_ptr<crm::i_stream_end_t>&& h,
		std::unique_ptr<crm::i_stream_rhandler_t>&& rh,
		std::unique_ptr<crm::dcf_exception_t>&& exc)noexcept {

		streamReadySendf = true;
		cvStreamReadySend.notify_one();

		if(st == crm::async_operation_result_t::st_ready) {
			std::cout << "streaming is ok" << std::endl;
			}
		else if(exc) {
			std::wcout << L"streaming end of with exception : " << exc->msg() << std::endl;
			}
		};

	/* simple reader from a predefined container-based reader type */
	auto outStreamReader = test_stream_trait::make_reader(testStreamData);

	/* header(cover data's) */
	stream_report_data cv;
	cv.senderName = "example of sender";
	cv.submissionSummary = "example of streaming";
	cv.countBytes = testStreamData.size();

	/* predefined out-stream handler for that reader and header objects */
	auto outStreamHandler = crm::make_rstream_handler(peer.ctx(),
		std::move(outStreamReader),
		std::move(cv),
		CRM_MB,
		std::chrono::minutes(10),
		L"example of streaming");

	/* streaming process launch */
	crm::make_ostream_launch(crm::async_space_t::ext,
		peer.ctx(),
		peer,
		std::move(outStreamHandler),
		[](const std::unique_ptr<crm::dcf_exception_t>& exc)noexcept {
		return true;
		},
		std::move(endh));
	}

/*! example of outbound connection's options */
class make_connection_options_t : public crm::rt0_connection_settings_t {
public:
	typedef crm::rt0_connection_settings_t base_t;

	make_connection_options_t()
		: crm::rt0_connection_settings_t(
			localhostId_RT0, 
			crm::rt1_endpoint_t::make_remote("127.0.0.1", port, localhostId_RT0),
			[](crm::input_message_knop_l0&&) {}){}

	/*! handler for every established connection(initial and recovered) for that target address */
	void connection_handler(std::unique_ptr<crm::dcf_exception_t>&&, xpeer_link&& lnk_)final {
		std::cout << "outbound connection established:"
			<< lnk_.address() << ":" << lnk_.port() << ":" << lnk_.remote_id().name() << std::endl;
		}
	};
static_assert(crm::is_outbound_connection_settings_rt0_v<make_connection_options_t>, __FILE_LINE__);

/*! example of custom hub options */
class example_rt0_hub : public crm::rt0_hub_settings_t {
	std::list<std::shared_ptr<crm::i_stream_whandler_t>> _wstreamAnchors;

public:
	example_rt0_hub(std::shared_ptr<crm::distributed_ctx_t> c)
		:crm::rt0_hub_settings_t(c,
			localhostId_RT0,
			crm::tcp::endpoint(port, crm::tcp::protocol::v4)/* end point */,
			[](crm::input_message_knop_l0&& m) {}) {}

	/*! multi-events handler, use for incoming streaming events */
	void event_handler(std::unique_ptr<crm::i_event_t>&& ev)final {
		/*! established a stream*/
		if (auto streamBegin = crm::event_cast<crm::i_input_stream_begin_t>(std::move(ev))) {
			std::cout << "stream begining..." << std::endl;
			_wstreamAnchors.push_back(streamBegin.value().holder());
			}
		/* wait stream eof */
		else if (auto streamEnd = crm::event_cast<crm::i_input_stream_eof_t>(std::move(ev))) {

			if (streamEnd.value().is_writer_as<test_stream_trait::writer_slot>()) {
				auto blobStream = std::move(streamEnd).value().writer_as<test_stream_trait::writer_slot>();
				std::cout << "stream input ready..." << std::endl;

				/* restored cover data */
				auto cv = dynamic_cast<const stream_report_data*>(blobStream.cover_data().get());
				std::cout << "name = " << cv->senderName << std::endl;
				std::cout << "submission summary = " << cv->submissionSummary << std::endl;
				std::cout << "bytes = " << cv->countBytes << std::endl;

				std::time_t t = system_clock::to_time_t(cv->initialTime);
				std::cout << "initial time point = " << t << std::endl;

				std::cout << "validate with comparision..." << std::endl;
				if (blobStream.container() == testStreamData) {
					std::cout << "    ...ok, content is equals" << std::endl;
					}
				else {
					std::cout << "    ...bad, content is not equals!" << std::endl;
					}

				streamReadyRecvf = true;
				cvStreamReadyRecv.notify_one();
				}
			else {
				std::cout << "    ...bad, wrong slot type!" << std::endl;
				}
			}
		else if (auto sExc = crm::event_cast<crm::i_input_stream_exception_t>(std::move(ev))) {
			std::cout << "    ...exception, " << crm::usecase::cvt2str( sExc.value().exception->msg()) << std::endl;
			}
		}
	};


int main() {
	/* make a random stream data */
	testStreamData.resize(CRM_MB * 128, 0);
	
	std::random_device rd{};
	std::mt19937 gen{ rd() };
	std::normal_distribution<> d{ 1 };

	for (auto & b : testStreamData) {
		b = std::round(d(gen));
		}

	/* context */
	auto ctx0 = crm::distributed_ctx_t::make(crm::context_bindings(localhostId_RT0));
	/* registration of stream_report_data stream cover type for custom data's */
	ctx0->register_stream_cover_data_type<stream_report_data>();

	/* hub */
	auto tbl0 = crm::router_0_t::create(example_rt0_hub(ctx0));
	tbl0->start();

	/* create new outbound connection to that hub and launch streaming out at this connection */
	std::thread([&] {
		/* create outbound connection... and ... make output stream */
		auto outboundConnection = tbl0->create_out_link(make_connection_options_t{});
		send_stream(outboundConnection);
		}).detach();

	/* wait for streaming ready at hub-site */
	std::unique_lock lx(mxLock);
	cvStreamReadySend.wait(lx, [] { return streamReadySendf.load(); });
	cvStreamReadyRecv.wait(lx, [] { return streamReadyRecvf.load(); });

	std::cout << "use case has been completed, press any key..." << std::endl;
	getchar();

	return 0;
	}


