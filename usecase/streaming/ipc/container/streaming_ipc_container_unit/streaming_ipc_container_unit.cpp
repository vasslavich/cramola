﻿#include <iostream>
#include <hublon/r1.h>
#include "../../../../demo_utilities/endpoints.h"
#include "../../../../demo_utilities/default_context.h"
#include "../../../../demo_utilities/stream_header_cv.h"
#include "../../../../demo_utilities/utilities.h"

using namespace crm;
using namespace crm::usecase;

std::shared_ptr< detail::ipc_unit> hub;
outbound_pinned_links outbound_stats;
std::atomic<bool> streamReadyf{ false };
std::atomic< async_operation_result_t> resultState{ async_operation_result_t::st_init };
std::shared_ptr<crm::dcf_exception_t> resultNotif{};
std::condition_variable cvStreamReady;
std::mutex mxLock;
constexpr auto stream_block_size = CRM_KB;
constexpr auto stream_object_size = CRM_KB * 64;

/*! send stream with a header */
template<typename TPeer>
void send_stream(TPeer&& peer) {

	/* end of streaming handler */
	auto endh = [](crm::async_operation_result_t st,
		std::unique_ptr<crm::i_stream_end_t>&& h,
		std::unique_ptr<crm::i_stream_rhandler_t>&& rh,
		std::shared_ptr<crm::dcf_exception_t> exc)noexcept {

		streamReadyf = true;
		resultState.store(st, std::memory_order::release);
		std::atomic_store_explicit(&resultNotif, std::move(exc), std::memory_order::release);

		cvStreamReady.notify_one();
		};

	/* simple reader from a predefined container-based reader type */
	auto outStreamReader = crm::make_rstream_container_adapter(std::vector<char>(stream_object_size, 0));

	/* header(cover data's) */
	stream_header_cv cv;
	cv.senderName = "example of sender";
	cv.submissionSummary = "example of streaming";
	cv.countBytes = stream_object_size;

	/* predefined out-stream handler for that reader and header objects */
	auto outStreamHandler = crm::make_rstream_handler(peer.ctx(),
		std::move(outStreamReader),
		std::move(cv),
		stream_block_size,
		std::chrono::minutes(10),
		L"example of streaming");

	/* streaming process launch */
	crm::make_ostream_launch(crm::async_space_t::ext,
		peer.ctx(),
		peer,
		std::move(outStreamHandler),
		[](const std::unique_ptr<crm::dcf_exception_t>& exc)noexcept {
		return true;
		},
		std::move(endh));
	}

class outbound_handler_t : public rt1_connection_settings_t {
public:
	outbound_handler_t(const crm::identity_descriptor_t& thisId,
		std::unique_ptr<crm::rt1_endpoint_t>&& ep,
		std::function<void(crm::input_message_knop_l1&&)>&& mh)
		: rt1_connection_settings_t(thisId, std::move(ep), std::move(mh)) {}

	/*! handler for every established connection(initial and recovered) for that target address */
	void connection_handler(std::unique_ptr<crm::dcf_exception_t>&&, xpeer_link&& lnk_)final {
		std::cout << "outbound connection established:"
			<< lnk_.address() << ":" << lnk_.port() << ":" << lnk_.remote_id().name() << std::endl;
		}
	};

std::map<crm::ipeer_t::remote_object_endpoint_t, crm::opeer_t> outboundHandls;
void start_outbound_connections_and_send_stream() {
	auto rdp = crm::ipeer_t::remote_object_endpoint_t::make_remote(localhostAddress, port, hubID);
	auto s = hub->make_outbound(outbound_handler_t(
		unitID,
		std::make_unique<crm::rt1_endpoint_t>(rdp),
		[rdp](crm::input_message_knop_l1&& ) {}));

	send_stream(s);

	outboundHandls[rdp] = std::move(s);
	}

class all2all_custom_hub_t : public crm::ipc_unit_options {
public:
	all2all_custom_hub_t(crm::detail::xchannel_shmem_hub_options&& opt)
		: crm::ipc_unit_options(std::move(opt)) {}
	};

int main() {
	auto c = make_context(unitID);

	crm::detail::xchannel_shmem_hub_options thisChannelOpt;
	thisChannelOpt.mainNodeName = unitChannelName;

	all2all_custom_hub_t opt(std::move(thisChannelOpt));

	detail::xchannel_shmem_host_args a;
	a.xchannelName.mainNodeName = hostChannelName;
	opt.set_host_channel(a);
	opt.set_this_id(unitID);
	opt.set_input_stack([](crm::ipeer_t&& lnk, crm::input_message_knop_l1&& m) {});

	hub = detail::ipc_unit::make(c, std::move(opt));

	ipc_unit_start_options startopt;
	startopt.message_mem_size = stream_block_size * 4;
	startopt.command_mem_size = stream_block_size;

	hub->start(startopt);
	start_outbound_connections_and_send_stream();

	while (true) {

		std::cout << "-------------   hub stats   ----------------------" << std::endl;
		std::cout << "--------------------------------------------------" << std::endl;

		/* wait for streaming ready at hub-site */
		std::unique_lock lx(mxLock);
		if (cvStreamReady.wait_for(lx, std::chrono::seconds(5), [] { return streamReadyf.load(); }))
			break;
		}

	std::cout << "outbound stream has been returned with code {" << crm::cvt2string(resultState) << "}" << std::endl;
	if (auto pexc = std::atomic_load_explicit(&resultNotif, std::memory_order::acquire)) {
		std::cout << "exception: " << crm::cvt( pexc->msg()) << std::endl;
		}

	std::cout << "press any key..." << std::endl;
	getchar();

	hub->close();
	}
