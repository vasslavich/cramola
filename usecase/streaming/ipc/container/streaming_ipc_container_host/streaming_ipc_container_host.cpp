﻿#include <hublon/r0.h>
#include "../../../../demo_utilities/default_context.h"
#include "../../../../demo_utilities/endpoints.h"


using namespace crm;
using namespace crm::usecase;


int main() {
	auto ctx = crm::usecase::make_context(hostID);
	ipc_host_options options(ctx, hostID, detail::tcp::tcp_endpoint_t(localhostAddress, port));
	options.set_channel_options(detail::xchannel_shmem_host_args{ hostChannelName });

	static_assert(is_ipc_hosh_options_v< ipc_host_options>, __FILE_LINE__);
	auto host = detail::ipc_host::make(ctx, std::move(options));

	ipc_host_start_options startopt;
	startopt.mem_size = CRM_MB * 32;
	host->start(startopt);
	while (true) {
		std::this_thread::sleep_for(std::chrono::seconds(5));
		}

	std::cout << "press any key..." << std::endl;
	getchar();

	host->close();
	}
