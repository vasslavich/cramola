﻿#include <iostream>
#include <hublon/r1.h>
#include "../../../../demo_utilities/endpoints.h"
#include "../../../../demo_utilities/default_context.h"
#include "../../../../demo_utilities/address_traftrace.h"
#include "../../../../demo_utilities/payload_types.h"
#include "../../../../demo_utilities/utilities.h"
#include "../../../../demo_utilities/stream_header_cv.h"
#include "../../../../demo_utilities/custom_stream_source.h"


using namespace crm;
using namespace crm::usecase;

std::shared_ptr< detail::ipc_unit> hub;
std::list<std::shared_ptr<crm::i_stream_whandler_t>> wstreamAnchors;
std::shared_ptr<dcf_exception_t> pExc{};
std::shared_ptr<i_stream_zyamba_cover_data_t> pStreamCV{};
std::atomic<bool> streamReadyf{ false };
std::condition_variable cvStreamReady;
std::mutex mxLock;

class all2all_custom_hub_t : public ipc_unit_options {
public:
	all2all_custom_hub_t(const crm::detail::xchannel_shmem_hub_options& channelOpt)
		: ipc_unit_options(channelOpt) {}

	/*! multi-events handler, use for incoming streaming events */
	void event_handler(std::unique_ptr<crm::i_event_t>&& ev)final {
		/*! established a stream*/
		if (auto streamBegin = crm::event_cast<crm::i_input_stream_begin_t>(std::move(ev))) {
			std::cout << "stream begining..." << std::endl;
			wstreamAnchors.push_back(streamBegin.value().holder());
			}
		/* wait stream eof */
		else if (auto streamEnd = crm::event_cast<crm::i_input_stream_eof_t>(std::move(ev))) {

			if (streamEnd.value().is_writer_as<custom_stream_slot>()) {
				auto blobStream = std::move(streamEnd).value().writer_as<custom_stream_slot>();
				std::cout << "stream input ready..." << std::endl;

				/* restored cover data */
				auto & cv = blobStream.cover_data();
				std::cout << "name = " << cv.senderName << std::endl;
				std::cout << "submission summary = " << cv.submissionSummary << std::endl;
				std::cout << "bytes = " << cv.countBytes << std::endl;

				std::time_t t = std::chrono::system_clock::to_time_t(cv.initialTime);
				std::cout << "initial time point = " << t << std::endl;

				std::atomic_exchange(&pStreamCV, std::dynamic_pointer_cast<i_stream_zyamba_cover_data_t>(
					std::make_shared<stream_header_cv>(cv)));

				streamReadyf.store(true);
				cvStreamReady.notify_one();
				}
			else {
				std::cout << "    ...bad, wrong slot type!" << std::endl;
				}
			}
		else if (auto sExc = crm::event_cast<crm::i_input_stream_exception_t>(std::move(ev))) {
			std::cout << "    ...exception, " << crm::usecase::cvt2str(sExc.value().exception->msg()) << std::endl;
			}
		}
	};


int main() {
	auto ctx = make_context(hubID);

	/* registration of stream_report_data stream cover type for custom data's */
	ctx->register_stream_cover_data_type<stream_header_cv>();

	/* register stream type */
	ctx->register_stream_type<custom_stream_slot>();

	crm::detail::xchannel_shmem_hub_options thisChannelOpt;
	thisChannelOpt.mainNodeName = hubChannelName;

	all2all_custom_hub_t opt(thisChannelOpt);

	detail::xchannel_shmem_host_args a;
	a.xchannelName.mainNodeName = hostChannelName;
	opt.set_host_channel(a);
	opt.set_this_id(hubID);
	opt.set_input_stack([](crm::ipeer_t&&, crm::input_message_knop_l1&&) {});

	hub = detail::ipc_unit::make(ctx, std::move(opt));

	ipc_unit_start_options startopt;
	startopt.message_mem_size = CRM_MB * 32;
	startopt.command_mem_size = CRM_MB;

	hub->start(startopt);

	while (true) {
		std::cout << "-------------   hub stats   ----------------------" << std::endl;
		std::cout << "--------------------------------------------------" << std::endl;

		/* wait for streaming ready at hub-site */
		std::unique_lock lx(mxLock);
		if (cvStreamReady.wait_for(lx, std::chrono::seconds(5), [] { return streamReadyf.load(); }))
			break;
		}

	if (streamReadyf) {
		std::cout << "input stream has been eof, with:" << std::endl;
		auto pHeader = std::dynamic_pointer_cast<stream_header_cv>(std::atomic_load(&pStreamCV));
		if (pHeader) {
			std::cout << "    count bytes:         " << pHeader->countBytes << std::endl;
			std::cout << "    timeline, ms:        " <<
				std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now() - pHeader->initialTime).count() << std::endl;
			std::cout << "    sender name:         " << pHeader->senderName << std::endl;
			std::cout << "    submission summary:  " << pHeader->submissionSummary << std::endl;
			}
		else if (auto pe = std::atomic_load(&pExc)) {
			std::cout << "exception: " << crm::cvt(pe->msg()) << std::endl;
			}
		else {
			std::cout << "    undefined error" << std::endl;
			}
		}

	std::cout << "press any key..." << std::endl;
	getchar();

	hub->close();
	}

