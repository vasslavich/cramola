#include <appkit/appkit.h>
#include "./declaration.h"


using namespace crm::examples::ex11;

short crm::examples::ex11::port = 16789;
const std::string crm::examples::ex11::localhostAddress = "127.0.0.1";
const crm::identity_descriptor_t crm::examples::ex11::unitID = { crm::global_object_identity_t::as_hash("FAFA0000-0A0A-F1F1-AAAA-100000000003"), "unit" };
const crm::identity_descriptor_t crm::examples::ex11::hubID = { crm::global_object_identity_t::as_hash("FAFA0000-0A0A-F1F1-AAAA-100000000002"), "hub" };
const crm::identity_descriptor_t crm::examples::ex11::hostID = { crm::global_object_identity_t::as_hash("FAFA0000-0A0A-F1F1-AAAA-100000000001"), "host" };

const std::string crm::examples::ex11::hostChannelName = "ex11_host_channel_memory";
const std::string crm::examples::ex11::hubChannelName = "ex11_hub_channel_memory";
const std::string crm::examples::ex11::unitChannelName = "ex11_unit_channel_memory";



std::atomic<size_t> crm::examples::ex11::outbound_handler_t::I = 0;
address_traffic_trace crm::examples::ex11::sndcntOutbound;
