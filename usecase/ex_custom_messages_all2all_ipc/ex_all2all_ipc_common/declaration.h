#pragma once

#include <mutex>
#include <appkit/appkit.h>

namespace crm::examples::ex11 {

extern short port;
extern const std::string localhostAddress;
extern const crm::identity_descriptor_t unitID;
extern const crm::identity_descriptor_t hubID;
extern const crm::identity_descriptor_t hostID;
extern const std::string hostChannelName;
extern const std::string hubChannelName;
extern const std::string unitChannelName;


struct serialized_as_aggregate {
	std::string s_value;
	std::vector<uint8_t> vb_value;
	std::array<int, 10> vi_array;
	};


class address_traffic_trace {
	mutable std::mutex _lck;
	std::unordered_map<std::string, size_t> _map;

public:
	void inc(const std::string & address) {
		std::lock_guard lock(_lck);
		++_map[address];
		}

	std::vector<std::pair<std::string, size_t>> trace()const noexcept {
		std::vector<std::pair<std::string, size_t>> r;
		r.reserve(_map.size());

		std::lock_guard lock(_lck);
		for(const auto& item : _map) {
			r.emplace_back(item.first, item.second);
			}

		return r;
		}
	};

extern address_traffic_trace sndcntOutbound;
class outbound_handler_t : public rt1_connection_settings_t {
	std::weak_ptr<crm::distributed_ctx_t> _ctx;
	static std::atomic<size_t> I;

	struct thread_state {
		int i{ 0 };
		std::mutex nextLck;
		std::condition_variable nextCnd;
		std::atomic<bool> nextF = true;
		};

public:
	static size_t InstancesCount()noexcept { return I.load(); }
	outbound_handler_t(std::weak_ptr<crm::distributed_ctx_t> ctx,
		const crm::identity_descriptor_t& thisId,
		std::unique_ptr<crm::rt1_endpoint_t>&& ep,
		std::function<void(crm::input_message_knop_l1&&)>&& mh)
		: rt1_connection_settings_t(thisId, std::move(ep), std::move(mh))
		, _ctx(ctx) {}

	void connection_handler(std::unique_ptr<crm::dcf_exception_t>&&, xpeer_link&& xpeer_)final {
		I++;

		std::thread([c = _ctx, xpeer = std::move(xpeer_)]{

		auto st = std::make_shared< thread_state>();
		while (xpeer.is_opened()) {
			std::unique_lock lck(st->nextLck);
			while (xpeer.is_opened() && !st->nextCnd.wait_for(lck, std::chrono::milliseconds(10),
				[&st] { return st->nextF.load(); }));
			lck.unlock();

			st->nextF.store(false);
			auto resetNext = crm::detail::make_postscope_action([st] {
				st->nextF.store(true);
				st->nextCnd.notify_one();
				});

			if (++st->i % 2) {
				serialized_as_aggregate m;
				auto rh = [c, xpeer, resetNext_ = std::move(resetNext)](message_handler&& m)mutable noexcept {
					if (m.has_result_as<serialized_as_aggregate>()) {
						CBL_VERIFY(!m.has_exception());
						sndcntOutbound.inc(xpeer.address());

						m.commit_receive();
						}
					};

				crm::ainvoke(c,
					xpeer,
					std::move(m),
					std::move(rh),
					__FILE_LINE__,
					crm::sndrcv_timeline_t::make_once_call());
				}
			else {
				serialized_as_aggregate m;
				auto rh = [c, xpeer, resetNext_ = std::move(resetNext)](
					serialized_as_aggregate&& m,
					std::unique_ptr<dcf_exception_t>&& e,
					std::chrono::microseconds)mutable noexcept {

					if (!e) {
						sndcntOutbound.inc(xpeer.address());
						}
					};

				crm::ainvoke(c,
					xpeer,
					std::move(m),
					std::move(rh),
					__FILE_LINE__,
					crm::sndrcv_timeline_t::make_once_call());
				}
			}
			}
		).detach();
		}
	};


class outbound_stats_t {
private:
	std::map<crm::ipeer_t::remote_object_endpoint_t, crm::opeer_t> _opx;
	mutable std::mutex _opxLock;

public:
	bool exists(const crm::ipeer_t::remote_object_endpoint_t& rdp)const noexcept {
		std::unique_lock lck(_opxLock);
		return _opx.count(rdp) > 0;
		}

	void insert(crm::ipeer_t::remote_object_endpoint_t rdp, crm::opeer_t peer) {
		std::unique_lock lck(_opxLock);
		_opx[rdp]= std::move(peer);
		}

	/*std::optional<crm::opeer_t> find(const crm::ipeer_t::remote_object_endpoint_t& rdp) {
		std::unique_lock lck(_opxLock);
		auto it = _opx.find(rdp);
		if(it != _opx.cend()) {
			return std::make_optional(it->second);
			}
		else {
			return std::nullopt;
			}
		}*/
	};
}
