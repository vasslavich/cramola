#include <iostream>
#include <hublon/r1.h>
#include "../ex_all2all_ipc_common/declaration.h"


using namespace crm;
using namespace crm::examples::ex11;


std::string remoteAddress;
address_traffic_trace rcvcnt;
address_traffic_trace sndcntHub;


void input_handler(crm::detail::i_xpeer_rt1_t::input_value_t&& message) {
	auto m2 = crm::message_forward<serialized_as_aggregate>(std::move(message).m());
	if(m2) {
		rcvcnt.inc(message.desc().rt0().connection_key().address);

		auto v = std::move(m2).value();
		serialized_as_aggregate m;

		crm::detail::ostrob_message_envelop_t<serialized_as_aggregate> response(std::move(m));
		response.capture_response_tail((*v.source()));

		message.send_back(std::move(response));
		v.commit();
		}
	}

std::shared_ptr<crm::distributed_ctx_t> make_context() {
	crm::distributed_ctx_policies_t policies;
	policies.set_rt_ping(true);

	return crm::distributed_ctx_t::make(crm::context_bindings(), std::move(policies));
	}


std::shared_ptr< detail::ipc_unit> hub;
outbound_stats_t outbound_stats;

class example_hub_options : public crm::ipc_unit_options {
	static std::atomic<size_t> I;

	void validate_reverse_connect(const crm::ipeer_t& rox) {
		auto rdp = crm::ipeer_t::remote_object_endpoint_t::make_remote(rox.address(), port, rox.id());
		auto xpeer1 = hub->make_outbound(outbound_handler_t(
			ctx(),
			hubID,
			std::make_unique<crm::rt1_endpoint_t>(rdp),
			[rdp](crm::input_message_knop_l1&& m) {

			input_handler(std::move(m));
			}));

		//std::thread([c = rox.ctx(), xpeer1]{
		//while(true) {
		//	serialized_as_aggregate m;
		//	auto rh = [c, addr = xpeer1.address()](serialized_as_aggregate&& obj,
		//			std::unique_ptr<crm::dcf_exception_t>&& exc_,
		//			std::chrono::microseconds mcs_)mutable noexcept {

		//		if(!exc_) {
		//			sndcntHub.inc(addr);
		//			}
		//		};

		//	crm::ainvoke(c,
		//		xpeer1,
		//		std::move(m),
		//		std::move(rh),
		//		__FILE_LINE__,
		//		crm::sndrcv_timeline_t::make_once_call());
		//	std::this_thread::sleep_for(std::chrono::seconds(1));

		//	} }).detach();

		outbound_stats.insert(std::move(rdp), std::move(xpeer1));
		}

	void connection_handler(crm::ipeer_t&& xpeer, crm::datagram_t&& initializeResult)final {
		//validate_reverse_connect(xpeer);
		}

	bool remote_object_identity_validate(const crm::identity_value_t& idSet,
		std::unique_ptr<crm::i_peer_remote_properties_t>& prop)final {

		return true;
		}

public:
	static size_t InstancesCount()noexcept { return I.load(); }
	example_hub_options(crm::detail::xchannel_shmem_hub_options&& opt)
		: crm::ipc_unit_options(std::move(opt)) {}
	};
std::atomic<size_t> example_hub_options::I{ 0 };

class all2all_custom_hub_t {
private:
	std::weak_ptr<crm::distributed_ctx_t> _ctx;

public:
	all2all_custom_hub_t(std::weak_ptr<crm::distributed_ctx_t> c)
		: _ctx(c) {}

	void begin_outbound_connection() {
		auto rdp = crm::rt1_endpoint_t::make_remote(remoteAddress, port, hubID);
		outbound_stats.insert(std::move(rdp), hub->make_outbound(outbound_handler_t(
			_ctx,
			unitID,
			std::make_unique<crm::rt1_endpoint_t>(rdp),
			[](crm::input_message_knop_l1&& m) {

			input_handler(std::move(m));
			})));
		}
	};


int main(int argc, const char* argv[]) {
	if(argc > 1)
		remoteAddress = argv[1];
	else
		remoteAddress = localhostAddress;

	auto c = make_context();

	crm::detail::xchannel_shmem_hub_options thisChannelOpt;
	thisChannelOpt.mainNodeName = unitChannelName;

	example_hub_options opt(std::move(thisChannelOpt));

	detail::xchannel_shmem_host_args a;
	a.xchannelName.mainNodeName = hostChannelName;
	opt.set_host_channel(a);
	opt.set_this_id(unitID);
	opt.set_input_stack([](crm::ipeer_t&&, crm::input_message_knop_l1&& m) {
		input_handler(std::move(m));
		});

	hub = detail::ipc_unit::make(c, std::move(opt));

	hub->register_message_type<serialized_as_aggregate>();

	ipc_unit_start_options startopt;
	startopt.message_mem_size = CRM_MB * 32;
	startopt.command_mem_size = CRM_MB;

	hub->start(startopt);

	all2all_custom_hub_t unit(c);
	unit.begin_outbound_connection();

	while(true) {
		std::this_thread::sleep_for(std::chrono::seconds(5));

		std::ostringstream text;
		text << "stat...." << std::endl;
		text << "    hub connection handlers launched			" << example_hub_options::InstancesCount() << std::endl;
		text << "    outbound connection handlers launched		" << outbound_handler_t::InstancesCount() << std::endl;

		auto hubInTraffic = rcvcnt.trace();
		auto hubOutTraffic = sndcntHub.trace();
		auto outboundTraffic = sndcntOutbound.trace();

		text << "    hub receive traffic:" << std::endl;
		for(auto& ir : hubInTraffic) {
			text << "      " << ir.first << ":" << ir.second << std::endl;
			}

		text << "    hub out traffic:" << std::endl;
		for(auto& ir : hubOutTraffic) {
			text << "      " << ir.first << ":" << ir.second << std::endl;
			}

		text << "    outbound traffic:" << std::endl;
		for(auto& ir : outboundTraffic) {
			text << "      " << ir.first << ":" << ir.second << std::endl;
			}

		std::cout << text.str() << std::endl;
		}

	std::cout << "press any key..." << std::endl;
	getchar();

	hub->close();
	}
