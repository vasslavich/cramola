#include <appkit/appkit.h>
#include <chrono>
#include <iostream>
#include <memory>
#include "../../ex_utility/mkctx.h"
#include "../../ex_utility/rand_values.h"


using namespace crm;
using namespace crm::srlz;




auto test_datagram_message() {
	datagram_t dt;
	dt.add_value("ck", usecase::rand_connection_key());
	dt.add_value("id", usecase::rand_identity_descriptor());
	dt.add_value("session_id", usecase::rand_session_descriptor());
	dt.add_value("rt0", usecase::rand_rt0_rdm());
	dt.add_value("int", 111111);
	dt.add_value("mrdm", usecase::rand_mrdm());

	return osendrecv_datagram_t(std::move(dt));
	}

bool test_cmp_eq(const osendrecv_datagram_t& l, const osendrecv_datagram_t& r) {
	decltype(usecase::rand_connection_key())
		ckl = l.value().get_value<decltype(ckl)>("ck"),
		ckr = r.value().get_value<decltype(ckl)>("ck");


	return ckl == ckr;
	}


template<typename _Mx,
	typename M = std::decay_t<_Mx>,
	typename std::enable_if_t<std::is_base_of_v<crm::i_iol_ohandler_t, M>&& std::is_final_v<M>, int> = 0>
bool test( _Mx && c, const std::shared_ptr<distributed_ctx_t> cx, const default_binary_protocol_handler_t & protoh) {
	auto bin = crm::detail::outbound_message_unit_t::make("example",
		protoh.create_emitter()->push(std::forward<_Mx>(c)),
		c,
		c.reset_invoke_exception_guard());
	
	return false;
	}

int main() {
	auto c = usecase::make_serialization_example_ctx();
	auto protocol = std::make_unique<crm::default_binary_protocol_handler_t>(c);

	if (test(test_datagram_message(), c, (*protocol))) {
		}
	else {
		std::cout << "++++++++++++++++++++++++++ FAIL ++++++++++++++++++++++++++++++" << std::endl;
		}

	std::getchar();

	return 0;
	}
