#include <appkit/appkit.h>
#include <appkit/streams/messages.h>
#include <chrono>
#include "../../ex_utility/mkctx.h"
#include "../../ex_utility/rand_values.h"


using namespace crm;
using namespace crm::srlz;


template<typename C>
std::vector<uint8_t> encode(C&& c) {
	crm::detail::rt0_x_command_pack_t pck(std::forward<C>(c));
	auto ws = wstream_constructor_t::make();
	srlz::serialize(ws, pck);
	return ws.release();
	}

std::unique_ptr<crm::detail::i_rt0_command_t> decode(const std::vector<uint8_t>& bin, std::shared_ptr<distributed_ctx_t> c) {
	crm::detail::rt0_x_command_pack_t pck;
	auto rs = rstream_constructor_t::make(bin.cbegin(), bin.cend());
	srlz::deserialize(rs, pck);
	return pck.extract(c);
	}

template<typename _Cx,
	typename C = std::decay_t<_Cx>,
	typename std::enable_if_t<std::is_base_of_v<crm::detail::i_rt0_command_t, C> && std::is_final_v<C>,int> = 0>
bool test(const _Cx & c, std::shared_ptr<distributed_ctx_t> cx) {
	auto bin = encode(c);
	auto pDecodedCmd = decode(bin, cx);
	if (auto pX = dynamic_cast<const C*>(pDecodedCmd.get())) {
		if (c == (*pX)) {
			return true;
			}
		else {
			return false;
			}
		}
	else {
		return false;
		}
	}


int main() {
	auto c = usecase::make_serialization_example_ctx();

	if (test(usecase::rand_cmd_addlink(), c)) {
		}
	else{
		std::cout << "++++++++++++++++++++++++++ FAIL ++++++++++++++++++++++++++++++" << std::endl;
		}

	std::getchar();

	return 0;
	}


