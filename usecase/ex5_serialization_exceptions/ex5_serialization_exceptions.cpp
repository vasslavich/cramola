﻿#include <appkit/appkit.h>



bool operator==(const crm::dcf_exception_t & l, const crm::dcf_exception_t & r) {
	return l.msg() == r.msg() && l.line() == r.line();
	}


template<typename T,
	typename _XT = std::decay_t<T>>
	void validate(T && t) {
	auto ws = crm::srlz::wstream_constructor_t::make();
	crm::srlz::serialize(ws, t);

	auto rs = crm::srlz::rstream_constructor_t::make(ws.release());
	_XT t2;
	crm::srlz::deserialize(rs, t2);

	if(!(t == t2)) {
		throw std::exception((std::string("bad encode/decode test: ") + typeid(_XT).name()).c_str());
		}
	else {
		std::cout << std::setw(60) << typeid(_XT).name() << " ok" << std::endl;
		}
	}

int main() {
	validate(crm::dcf_exception_t{});
	validate(crm::dcf_notification_t{});
	validate(crm::mpf_exception_t{});
	validate(crm::mpf_test_exception_t{});
	validate(crm::inconsistency_exchange_protocol_exception_t{});
	validate(crm::terminate_operation_exception_t{});
	validate(crm::timeout_exception_t{});
	validate(crm::duplicate_object_exception_t{});
	validate(crm::extern_code_exception_t{});
	validate(crm::not_found_exception_t{});
	validate(crm::object_closed_t{});
	validate(crm::refuse_operation_exception_t{});
	validate(crm::srlz::serialize_exception_t{});
	validate(crm::types_mismatching_exception_t{});
	validate(crm::abandoned_exception_t{});

	std::cout << "press any key" << std::endl;
	getchar();

	return 0;
	}

