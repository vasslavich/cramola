﻿#include <string>
#include <vector>
#include <iostream>
#include <array>
#include <appkit/appkit.h>



constexpr size_t max_container_size = 10;


template<typename T, typename std::enable_if_t<std::is_same_v<char, std::decay_t<T>>, int> = 0>
auto make() {
	return crm::sx_uuid_t::rand().u8_16[0];
	}
template<typename T, typename std::enable_if_t<std::is_same_v<wchar_t, std::decay_t<T>>, int> = 0>
auto make() {
	return crm::sx_uuid_t::rand().u16_8[0];
	}
template<typename T, typename std::enable_if_t<std::is_same_v<uint8_t, std::decay_t<T>>, int> = 0>
auto make() {
	return crm::sx_uuid_t::rand().u8_16[0];
	}
template<typename T, typename std::enable_if_t<std::is_same_v<uint32_t, std::decay_t<T>>, int> = 0>
auto make() {
	return crm::sx_uuid_t::rand().u32_4[0];
	}
template<typename T, typename std::enable_if_t<std::is_same_v<uint64_t, std::decay_t<T>>, int> = 0>
auto make() {
	return crm::sx_uuid_t::rand().u64_2[0];
	}


union u2 {
	std::size_t s;
	double d;
	std::uint8_t u8[sizeof(std::size_t)];
	};
bool operator==(const u2 & l, const u2 & r) {
	return l.s == r.s && l.d == r.d;
	}
bool operator!=(const u2 & l, const u2 & r) {
	return !(l == r);
	}
template<typename T, typename std::enable_if_t<std::is_same_v<u2, std::decay_t<T>>, int> = 0>
u2 make() {
	u2 v;
	v.s = crm::sx_uuid_t::rand().u64_2[0];
	return v;
	}


struct st0 {
	uint32_t i;
	double d;
	};
bool operator==(const st0 & l, const st0 & r) {
	return l.i == r.i && l.d == r.d;
	}
bool operator!=(const st0 & l, const st0 & r) {
	return !(l == r);
	}
template<typename T, typename std::enable_if_t<std::is_same_v<st0, std::decay_t<T>>, int> = 0>
st0 make() {
	st0 v;

	auto rndf = crm::sx_uuid_t::rand();
	v.i = rndf.u32_4[0];
	v.d = (double)rndf.u64_2[0];

	return v;
	}



struct st1 {
	std::string s;
	std::vector<uint16_t> u2;
	};

bool operator==(const st1 & l, const st1 & r) {
	return l.s == r.s && l.u2 == r.u2;
	}
bool operator!=(const st1 & l, const st1 & r) {
	return !(l == r);
	}
template<typename T, typename std::enable_if_t<std::is_same_v<st1, std::decay_t<T>>, int> = 0>
st1 make() {
	auto rndf = crm::sx_uuid_t::rand();

	st1 v;
	v.s = rndf.to_str();

	size_t l = (rndf.u32_4[0] + 1) % max_container_size;
	v.u2.resize((std::max)((size_t)1, l));
	for(size_t i = 0; i < v.u2.size(); ++i)
		v.u2[i] = i % rndf.u16_8[i % 4];

	return v;
	}


struct st2 {
	st0 s0;
	st1 s1;
	};
bool operator==(const st2 & l, const st2 & r) {
	return l.s0 == r.s0 && l.s1 == r.s1;
	}
bool operator!=(const st2 & l, const st2 & r) {
	return !(l == r);
	}
template<typename T, typename std::enable_if_t<std::is_same_v<st2, std::decay_t<T>>, int> = 0>
st2 make() {
	st2 v;
	v.s0 = make<decltype(v.s0)>();
	v.s1 = make<decltype(v.s1)>();

	return v;
	}



struct st3 {
	std::vector<st2> st2array;
	st2 s2;
	st1 s1;
	st0 s0;
	};
bool operator==(const st3 & l, const st3 & r) {
	return l.s0 == r.s0 && l.s1 == r.s1 && l.s2 == r.s2 && l.st2array == r.st2array;
	}
bool operator!=(const st3 & l, const st3 & r) {
	return !(l == r);
	}
template<typename T, typename std::enable_if_t<std::is_same_v<st3, std::decay_t<T>>, int> = 0>
st3 make() {
	st3 v;
	v.s0 = make<decltype(v.s0)>();
	v.s1 = make<decltype(v.s1)>();
	v.s2 = make<decltype(v.s2)>();

	auto rndf = crm::sx_uuid_t::rand();
	v.st2array.resize((rndf.u32_4[0] + 1) % max_container_size);
	for(size_t i = 0; i < v.st2array.size(); ++i)
		v.st2array[i] = make<decltype(v.st2array[i])>();

	return v;
	}




struct st4 {
	st3 a;
	u2 u;
	};
bool operator==(const st4 & l, const st4 & r) {
	return l.a == r.a && l.u == r.u;
	}
bool operator!=(const st4 & l, const st4 & r) {
	return !(l == r);
	}
template<typename T, typename std::enable_if_t<std::is_same_v<st4, std::decay_t<T>>, int> = 0>
st4 make() {
	st4 v;
	v.a = make<decltype(v.a)>();
	v.u = make<decltype(v.u)>();

	return v;
	}



struct st5 {
	std::list<st3> x3l;
	std::vector<st4> x4v;
	};
bool operator==(const st5 & l, const st5 & r) {
	return l.x3l == r.x3l && l.x4v == r.x4v;
	}
bool operator!=(const st5 & l, const st5 & r) {
	return !(l == r);
	}
template<typename T, typename std::enable_if_t<std::is_same_v<st5, std::decay_t<T>>, int> = 0>
st5 make() {
	auto rndf = crm::sx_uuid_t::rand();

	st5 v;
	auto lx = (rndf.u32_4[0] + 1) % max_container_size;
	for(size_t i = 0; i < lx; ++i)
		v.x3l.push_back(make<decltype(*(v.x3l.cbegin()))>());

	auto lv = (rndf.u32_4[1] + 1) % max_container_size;
	for(size_t i = 0; i < lv; ++i)
		v.x4v.push_back(make<decltype(*(v.x4v.cbegin()))>());

	return v;
	}





template<typename T>
void test_fields_count() {
	auto st_fc = crm::srlz::detail::fields_count_v<T>;
	std::cout << "st0_fc=" << st_fc << std::endl;
	}


template<typename T>
using list_t = typename std::list<T>;

template<typename T>
using vector_t = typename std::vector<T>;

template<typename T>
using string_t = typename std::basic_string<T>;


template<typename T>
size_t write_size_(std::bool_constant<false>) {
	return 0;
	}

template<typename T>
size_t write_size_(std::bool_constant<true>) {
	return crm::srlz::write_size_v<T>;
	}

template<typename _T>
void test_sd(_T && oin, std::decay_t<std::type_identity_t<_T>> & oex) {
	using T = std::decay_t<_T>;

	auto srlzLenOin = crm::srlz::object_trait_serialized_size(std::forward<_T>(oin));
	if(crm::srlz::detail::is_fixed_size_serialized_v<_T>) {
		if(srlzLenOin != write_size_<_T>(std::bool_constant<crm::srlz::detail::is_fixed_size_serialized_v<T>>())) {
			throw std::exception();
			}
		}

	auto ws = crm::srlz::wstream_constructor_t::make();
	crm::srlz::serialize(ws, oin);

	auto rs = crm::srlz::rstream_constructor_t::make(ws.release());
	crm::srlz::deserialize(rs, oex);

	auto srlzLenOex = crm::srlz::object_trait_serialized_size(oex);

	if(srlzLenOex != srlzLenOin) {
		throw std::exception();
		}

	if(oin != oex) {
		throw std::exception();
		}
	}

template<typename _T>
void test_sd(_T && oin) {
	typename std::decay_t<_T> oex;
	test_sd(std::forward<_T>(oin), oex);
	}

template<template<typename> typename Tcontainer, typename V>
void test_v1() {
	using v1t = typename Tcontainer<V>;

	v1t v1;
	auto rndf = crm::sx_uuid_t::rand();
	size_t l = (rndf.u32_4[0] + 1) % max_container_size;

	std::cout << "test_v1:" << std::endl;
	std::cout << "        " << typeid(v1t).name() << ": l=" << l << std::endl;

	for(size_t i = 0; i < l; ++i)
		v1.push_back(make<decltype(*v1.cbegin())>());

	test_sd(v1);

	std::cout << "        ok" << std::endl;
	}

template<template<typename> typename Tcontainer1, template<typename> typename Tcontainer2, typename V>
void test_v2() {
	using v1t = typename Tcontainer2<V>;
	using v2t = typename Tcontainer1<v1t>;

	v2t v2;
	auto rndf = crm::sx_uuid_t::rand();
	size_t l = (rndf.u32_4[0] + 1) % max_container_size;
	size_t y = (rndf.u32_4[1] + 1) % max_container_size;

	std::cout << "test_v2:" << std::endl;
	std::cout << "        " << typeid(v2t).name() << ": x,y=" << l << " x " << y << std::endl;
	for(size_t i = 0; i < l; ++i) {

		v1t v1;
		for(size_t j = 0; j < y; ++j) {
			v1.push_back(make<decltype(*v1.cbegin())>());
			}

		v2.push_back(std::move(v1));
		}

	test_sd(v2);

	std::cout << "        ok" << std::endl;
	}


struct my_struct_fixed_size_00 {

	// 8
	double s;
	// 4
	int i;
	// 8
	size_t sss;
	// 1
	char c;
	// 1
	char b;
	};
bool operator==(const my_struct_fixed_size_00 & l, const my_struct_fixed_size_00 & r) {
	return l.s == r.s && l.i == r.i && l.sss == r.sss && l.b == r.b && l.c == r.c;
	}
bool operator!=(const my_struct_fixed_size_00 & l, const my_struct_fixed_size_00 & r) {
	return !(l == r);
	}
template<typename T, typename std::enable_if_t<std::is_same_v<my_struct_fixed_size_00, std::decay_t<T>>, int> = 0>
my_struct_fixed_size_00 make() {
	my_struct_fixed_size_00 m;

	auto rnd = crm::sx_uuid_t::rand();

	m.b = rnd.u8_16[0];
	m.c = rnd.u8_16[1];
	m.sss = rnd.u64_2[0];
	m.s = (double)rnd.u64_2[1];
	m.i = rnd.u32_4[1];

	return m;
	}



struct my_struct_fixed_size_01 {

	// 8
	u2 u;
	// 4
	int i;
	};
bool operator==(const my_struct_fixed_size_01 & l, const my_struct_fixed_size_01 & r) {
	return l.u == r.u && l.i == r.i;
	}
bool operator!=(const my_struct_fixed_size_01 & l, const my_struct_fixed_size_01 & r) {
	return !(l == r);
	}
template<typename T, typename std::enable_if_t<std::is_same_v<my_struct_fixed_size_01, std::decay_t<T>>, int> = 0>
my_struct_fixed_size_01 make() {
	my_struct_fixed_size_01 m;
	m.u = make<decltype(m.u)>();
	m.i = 0;

	return m;
	}




struct my_struct_fixed_size_10 {
	my_struct_fixed_size_00 m00;
	my_struct_fixed_size_01 m01;

	u2 u;
	size_t s;
	};
bool operator==(const my_struct_fixed_size_10 & l, const my_struct_fixed_size_10 & r) {
	return l.m00 == r.m00 && l.m01 == r.m01 && l.u == r.u && l.s == r.s;
	}
bool operator!=(const my_struct_fixed_size_10 & l, const my_struct_fixed_size_10 & r) {
	return !(l == r);
	}
template<typename T, typename std::enable_if_t<std::is_same_v<my_struct_fixed_size_10, std::decay_t<T>>, int> = 0>
my_struct_fixed_size_10 make() {
	my_struct_fixed_size_10 m;
	m.m00 = make<decltype(m.m00)>();
	m.m01 = make<decltype(m.m01)>();
	m.u = make<decltype(m.u)>();
	m.s = 0;

	return m;
	}




struct my_struct_fixed_size_11 {
	my_struct_fixed_size_10 m10;
	std::array<my_struct_fixed_size_00, 2> v00;

	std::chrono::system_clock::time_point t = std::chrono::system_clock::now();
	std::chrono::system_clock::time_point::duration d = t.time_since_epoch();
	};
bool operator==(const my_struct_fixed_size_11 & l, const my_struct_fixed_size_11 & r) {
	return l.m10 == r.m10 && l.v00 == r.v00 && l.t == r.t;
	}
bool operator!=(const my_struct_fixed_size_11 & l, const my_struct_fixed_size_11 & r) {
	return !(l == r);
	}
template<typename T, typename std::enable_if_t<std::is_same_v<my_struct_fixed_size_11, std::decay_t<T>>, int> = 0>
my_struct_fixed_size_11 make() {
	my_struct_fixed_size_11 m;
	m.m10 = make<decltype(m.m10)>();

	for(size_t i = 0; i < m.v00.size(); ++i) {
		m.v00[i] = make<my_struct_fixed_size_00>();
		}

	return m;
	}
static_assert(crm::srlz::is_serializable_v<my_struct_fixed_size_11>, __FILE_LINE__);




struct my_struct_fixed_size_mixed1 {

	crm::sx_uuid_t uuid;
	int i;
	};
bool operator==(const my_struct_fixed_size_mixed1 & l, const my_struct_fixed_size_mixed1 & r) {
	return l.uuid == r.uuid && l.i == r.i;
	}
bool operator!=(const my_struct_fixed_size_mixed1 & l, const my_struct_fixed_size_mixed1 & r) {
	return !(l == r);
	}
template<typename T, typename std::enable_if_t<std::is_same_v<my_struct_fixed_size_mixed1, std::decay_t<T>>, int> = 0>
my_struct_fixed_size_mixed1 make() {
	my_struct_fixed_size_mixed1 m;
	m.uuid = crm::sx_uuid_t::rand();
	m.i = 0;

	return m;
	}


void gtest_0() {
	static_assert(std::is_fundamental_v<size_t>, "");

	static_assert(!crm::srlz::detail::is_strob_serializable_v<crm::sx_uuid_t>, "");
	static_assert(!crm::srlz::detail::is_container_v<crm::sx_uuid_t>, "");

	static_assert(crm::srlz::detail::is_container_v<std::string>, "");
	static_assert(crm::srlz::detail::is_container_v<std::string>, "");

	static_assert(crm::srlz::detail::is_strob_serializable_v<st0>, "validate st0");
	static_assert(crm::srlz::detail::is_strob_serializable_v<st1>, "validate st1");
	static_assert(crm::srlz::detail::is_strob_serializable_v<st2>, "validate st2");
	static_assert(crm::srlz::detail::is_strob_serializable_v<st3>, "validate st3");
	static_assert(!crm::srlz::detail::is_strob_serializable_v<u2>, "validate u2");
	static_assert(crm::srlz::detail::is_mono_serializable_v<u2>, "validate u2");
	static_assert(crm::srlz::detail::is_strob_serializable_v<st4>, "validate st4");
	static_assert(crm::srlz::detail::is_fixed_size_serialized_v<double>, __FILE__);
	static_assert(crm::srlz::detail::is_fixed_size_serialized_v<int>, __FILE__);
	static_assert(crm::srlz::detail::is_fixed_size_serialized_v<int&>, __FILE__);
	static_assert(crm::srlz::detail::is_fixed_size_serialized_v<int*>, __FILE__);
	static_assert(crm::srlz::detail::is_fixed_size_serialized_v<const size_t&>, __FILE__);
	static_assert(crm::srlz::detail::is_fixed_size_serialized_v<const char>, __FILE__);

	static_assert(!crm::srlz::detail::is_fixed_size_serialized_v<std::vector<int>>, __FILE__);
	static_assert(!crm::srlz::detail::is_fixed_size_serialized_v<std::list<std::vector<int>>>, __FILE__);
	static_assert(!crm::srlz::detail::is_fixed_size_serialized_v<std::list<char>>, __FILE__);
	static_assert(!crm::srlz::detail::is_fixed_size_serialized_v<std::vector<std::vector<int>>>, __FILE__);
	static_assert(!crm::srlz::detail::is_fixed_size_serialized_v<std::vector<char>&>, __FILE__);
	static_assert(!crm::srlz::detail::is_fixed_size_serialized_v<const std::vector<char>&>, __FILE__);

	static_assert(crm::srlz::detail::is_strob_fixed_serialized_size_v<my_struct_fixed_size_00>
		&& crm::srlz::detail::is_fixed_size_serialized_v<my_struct_fixed_size_00>, __FILE__);
	auto sssl = crm::srlz::write_size_v<my_struct_fixed_size_00>;

	static_assert(crm::srlz::detail::is_strob_fixed_serialized_size_v<my_struct_fixed_size_01>
		&& crm::srlz::detail::is_fixed_size_serialized_v<my_struct_fixed_size_01>, __FILE__);
	sssl = crm::srlz::write_size_v<my_struct_fixed_size_01>;

	static_assert(crm::srlz::detail::is_strob_fixed_serialized_size_v<my_struct_fixed_size_10>
		&& crm::srlz::detail::is_fixed_size_serialized_v<my_struct_fixed_size_10>, __FILE__);
	sssl = crm::srlz::write_size_v<my_struct_fixed_size_10>;

	static_assert(crm::srlz::detail::is_strob_serializable_v<my_struct_fixed_size_mixed1>, __FILE__);
	static_assert(!crm::srlz::detail::is_strob_fixed_serialized_size_v<my_struct_fixed_size_mixed1>, __FILE__);

	static_assert(crm::srlz::detail::is_strob_explicit_serialized_v<std::chrono::system_clock::time_point>, __FILE__);
	static_assert(crm::srlz::detail::is_strob_explicit_serialized_v<std::chrono::high_resolution_clock::time_point>, __FILE__);
	static_assert(crm::srlz::detail::is_strob_explicit_serialized_v<std::chrono::steady_clock::time_point>, __FILE__);
	static_assert(crm::srlz::detail::is_strob_explicit_serialized_v<std::chrono::nanoseconds>, __FILE__);
	static_assert(crm::srlz::detail::is_strob_explicit_serialized_v<std::chrono::microseconds>, __FILE__);
	static_assert(crm::srlz::detail::is_strob_explicit_serialized_v<std::chrono::milliseconds>, __FILE__);
	static_assert(crm::srlz::detail::is_strob_explicit_serialized_v<std::chrono::seconds>, __FILE__);
	static_assert(crm::srlz::detail::is_strob_explicit_serialized_v<std::chrono::minutes>, __FILE__);
	static_assert(crm::srlz::detail::is_strob_explicit_serialized_v<std::chrono::hours>, __FILE__);

	static_assert(!crm::srlz::detail::is_strob_explicit_serialized_v<my_struct_fixed_size_00>, __FILE__);
	static_assert(!crm::srlz::detail::is_strob_explicit_serialized_v<my_struct_fixed_size_01>, __FILE__);
	static_assert(!crm::srlz::detail::is_strob_explicit_serialized_v<my_struct_fixed_size_10>, __FILE__);
	static_assert(!crm::srlz::detail::is_strob_explicit_serialized_v<my_struct_fixed_size_mixed1>, __FILE__);

	}
//
//void gtest_1() {
//
//	test_fields_count<st0>();
//	test_fields_count<st1>();
//	test_fields_count<st2>();
//	test_fields_count<st3>();
//	test_fields_count<st4>();
//
//	test_fields_count<my_struct_fixed_size_mixed1>();
//
//
//	using tttxxx0 = crm::srlz::detail::contaiter_value_type_t<std::vector<std::vector<st0>>>;
//	tttxxx0 _xtt0;
//
//	using tttxxx2 = crm::srlz::detail::iterator_value_t<decltype(_xtt0.cbegin())>;
//	tttxxx2 _xtt2;
//
//	using tttxxx3 = crm::srlz::detail::contaiter_value_type_t<tttxxx0>;
//	tttxxx3 _xtt3;
//
//	using tttxxx4 = crm::srlz::detail::iterator_value_t<decltype(std::string().cbegin())>;
//	tttxxx2 _xtt4;
//
//	test_sd(make< my_struct_fixed_size_11>());
//	}
//
//void gtest_2() {
//
//	test_v1<vector_t, char>();
//	test_v2<vector_t, string_t, char>();
//	test_v2<vector_t, vector_t, uint8_t>();
//	test_v2<vector_t, vector_t, uint32_t>();
//	test_v2<vector_t, string_t, wchar_t>();
//	test_v2<vector_t, vector_t, st0>();
//	test_v2<list_t, vector_t, st1>();
//	test_v2<list_t, list_t, st2>();
//	test_v2<vector_t, vector_t, st3>();
//	test_v2<vector_t, vector_t, st4>();
//	test_v2<vector_t, list_t, st5>();
//	test_v1<vector_t, my_struct_fixed_size_11>();
//	test_v1<vector_t, my_struct_fixed_size_mixed1>();
//	}


static_assert(crm::srlz::detail::is_strob_flat_serializable_v<my_struct_fixed_size_11>, __FILE_LINE__);
static_assert(crm::srlz::detail::is_strob_prefixed_serialized_size_v<my_struct_fixed_size_11>, __FILE_LINE__);
static_assert(!crm::srlz::detail::is_i_srlzd_v<my_struct_fixed_size_11>, __FILE_LINE__);
static_assert(!crm::srlz::detail::is_serialized_trait_w_v<my_struct_fixed_size_11>, __FILE_LINE__);

int main() {

	gtest_0();

	auto m11 = make< my_struct_fixed_size_11>();
	auto l = crm::srlz::object_trait_serialized_size(m11);

	std::cout << "main done, press <enter>..." << std::endl;

	getchar();
	return 0;
	}


