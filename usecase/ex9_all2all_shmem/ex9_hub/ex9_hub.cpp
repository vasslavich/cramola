﻿#include <iostream>
#include <hublon/r1.h>
#include "../ex9_shared/utility.h"


using namespace crm;
using namespace ex9;


std::shared_ptr<crm::distributed_ctx_t> make_context() {
	crm::distributed_ctx_policies_t policies;
	policies.set_rt_ping(true);

	return crm::distributed_ctx_t::make(crm::context_bindings(), std::move(policies));
	}

class outbound_handler_t : public rt1_connection_settings_t {
	std::weak_ptr<crm::distributed_ctx_t> _ctx;

public:
	outbound_handler_t(std::weak_ptr<crm::distributed_ctx_t> ctx,
		const crm::identity_descriptor_t & thisId,
		std::unique_ptr<crm::rt1_endpoint_t> && ep,
		std::function<void(crm::input_message_knop_l1 &&)> && mh)
		: rt1_connection_settings_t(thisId, std::move(ep), std::move(mh))
		, _ctx(ctx) {}

	void connection_handler(std::unique_ptr<crm::dcf_exception_t> &&, xpeer_link && lnk)final {
		send(_ctx, std::move(lnk), crm::sndrcv_timeline_periodicity_t::make_cyclic(std::chrono::seconds(1)));
		}
	};

std::shared_ptr< detail::ipc_unit> hub;

class all2all_custom_hub_t : public ipc_unit_options {
private:
	class otable {
	public:
		std::map<crm::ipeer_t::remote_object_endpoint_t, crm::opeer_t> _opx;
		std::mutex _opxLock;
		};
	std::shared_ptr< otable> _optb{std::make_shared<otable>()};

	static void push_opeer_message(std::weak_ptr<otable> optb, const crm::ipeer_t::remote_object_endpoint_t & rdp,
		crm::opeer_t::input_value_t && m) {

		if (auto pt = optb.lock()) {
			std::unique_lock lck(pt->_opxLock);
			auto it = pt->_opx.find(rdp);
			if (it != pt->_opx.cend()) {
				input_handler((*it).second, std::move(m));
				}
			}
		}

	void validate_reverse_connect(const crm::ipeer_t & rox) {
		auto rdp = crm::ipeer_t::remote_object_endpoint_t::make_remote(localhostAddress, port, rox.id());

		std::unique_lock lck(_optb->_opxLock);
		if (_optb->_opx.count(rdp) == 0) {
			auto xpeer1 = hub->make_outbound( outbound_handler_t(
				ctx(),
				hubID,
				std::make_unique<crm::rt1_endpoint_t>(rdp),
				[rdp, wptr = std::weak_ptr<otable>(_optb)](crm::input_message_knop_l1 && m) {

				push_opeer_message(wptr, rdp, std::move(m));
				}));

			send(ctx(), detail::make_invokable_xpeer(xpeer1), crm::sndrcv_timeline_periodicity_t::make_cyclic(std::chrono::seconds(2)));

			_optb->_opx.insert({rdp, std::move(xpeer1)});
			lck.unlock();

			auto objectAddress = rox.address();
			auto objectPort = rox.port();
			auto objectId = rox.desc().source_id();
			}
		}

	void connection_handler(crm::ipeer_t && xpeer, crm::datagram_t && initializeResult)final {
		validate_reverse_connect(xpeer);
		send(ctx(), std::move(xpeer), crm::sndrcv_timeline_periodicity_t::make_cyclic(std::chrono::seconds(2)));
		}

	bool remote_object_identity_validate(const crm::identity_value_t & idSet,
		std::unique_ptr<crm::i_peer_remote_properties_t> & prop)final {

		return true;
		}

public:
	all2all_custom_hub_t(const crm::detail::xchannel_shmem_hub_options & channelOpt)
		: ipc_unit_options(channelOpt){}
	};


int main(){
	auto ctx = make_context();

	crm::detail::xchannel_shmem_hub_options thisChannelOpt;
	thisChannelOpt.mainNodeName = hubChannelName;

	all2all_custom_hub_t opt(thisChannelOpt);

	detail::xchannel_shmem_host_args a;
	a.xchannelName.mainNodeName = hostChannelName;
	opt.set_host_channel( a );
	opt.set_this_id(hubID);
	opt.set_input_stack([](crm::ipeer_t && lnk, crm::input_message_knop_l1&& m) {
		input_handler<crm::ipeer_t>(std::move(lnk), std::move(m)); 
		});

	hub = detail::ipc_unit::make( ctx, std::move(opt));
	hub->register_message_type<custom_message_strob_1>();

	ipc_unit_start_options startopt;
	startopt.message_mem_size = CRM_MB * 32;
	startopt.command_mem_size = CRM_MB;

	hub->start( startopt );

	while( true ){
		std::this_thread::sleep_for( std::chrono::seconds( 5 ) );
		}

	std::cout << "press any key..." << std::endl;
	getchar();

	hub->close();
	}

