project(ex9_shared CXX)

################################################################################
# Source groups
################################################################################
set(no_group_source_files
    "conditions.h"
    "utility.cpp"
    "utility.h"
)
source_group("" FILES ${no_group_source_files})

set(ALL_FILES
    ${no_group_source_files}
)

################################################################################
# Target
################################################################################
add_library(${PROJECT_NAME} STATIC ${ALL_FILES})
set_target_properties(${PROJECT_NAME} PROPERTIES FOLDER "ex9_all2all_sndrcv_shmem")

add_precompiled_header(${PROJECT_NAME} "stdafx.h" ".")

use_props(${PROJECT_NAME} "${CMAKE_CONFIGURATION_TYPES}" "${DEFAULT_CXX_PROPS}")
################################################################################
# Includes for CMake from *.props
################################################################################
if("${CMAKE_VS_PLATFORM_NAME}" STREQUAL "x64")
    use_props(${PROJECT_NAME} Debug             "../../../msvs/properties/general.cmake")
    use_props(${PROJECT_NAME} Debug             "../../../msvs/properties/debug.cmake")
    use_props(${PROJECT_NAME} Release_optimized "../../../msvs/properties/general.cmake")
    use_props(${PROJECT_NAME} Release_optimized "../../../msvs/properties/release_x64_optimized.cmake")
    use_props(${PROJECT_NAME} Release_with_dbg  "../../../msvs/properties/general.cmake")
    use_props(${PROJECT_NAME} Release_with_dbg  "../../../msvs/properties/release_x64_with_dbg.cmake")
    use_props(${PROJECT_NAME} Release           "../../../msvs/properties/general.cmake")
elseif("${CMAKE_VS_PLATFORM_NAME}" STREQUAL "x86")
    use_props(${PROJECT_NAME} Debug             "../../../msvs/properties/general.cmake")
    use_props(${PROJECT_NAME} Release_optimized "../../../msvs/properties/general.cmake")
    use_props(${PROJECT_NAME} Release_with_dbg  "../../../msvs/properties/general.cmake")
    use_props(${PROJECT_NAME} Release           "../../../msvs/properties/general.cmake")
endif()

set(ROOT_NAMESPACE ex9shared)

set_target_properties(${PROJECT_NAME} PROPERTIES
    VS_GLOBAL_KEYWORD "Win32Proj"
)
if("${CMAKE_VS_PLATFORM_NAME}" STREQUAL "x64")
    set_target_properties(${PROJECT_NAME} PROPERTIES
        INTERPROCEDURAL_OPTIMIZATION_RELEASE_OPTIMIZED "TRUE"
        INTERPROCEDURAL_OPTIMIZATION_RELEASE_WITH_DBG  "TRUE"
        INTERPROCEDURAL_OPTIMIZATION_RELEASE           "TRUE"
    )
elseif("${CMAKE_VS_PLATFORM_NAME}" STREQUAL "x86")
    set_target_properties(${PROJECT_NAME} PROPERTIES
        INTERPROCEDURAL_OPTIMIZATION_RELEASE_OPTIMIZED "TRUE"
        INTERPROCEDURAL_OPTIMIZATION_RELEASE_WITH_DBG  "TRUE"
        INTERPROCEDURAL_OPTIMIZATION_RELEASE           "TRUE"
    )
endif()
################################################################################
# Compile definitions
################################################################################
if("${CMAKE_VS_PLATFORM_NAME}" STREQUAL "x64")
    target_compile_definitions(${PROJECT_NAME} PRIVATE
        "$<$<CONFIG:Release>:"
            "NDEBUG"
        ">"
        "_LIB;"
        "UNICODE;"
        "_UNICODE"
    )
elseif("${CMAKE_VS_PLATFORM_NAME}" STREQUAL "x86")
    target_compile_definitions(${PROJECT_NAME} PRIVATE
        "$<$<CONFIG:Debug>:"
            "_DEBUG"
        ">"
        "$<$<CONFIG:Release_optimized>:"
            "NDEBUG"
        ">"
        "$<$<CONFIG:Release_with_dbg>:"
            "NDEBUG"
        ">"
        "$<$<CONFIG:Release>:"
            "NDEBUG"
        ">"
        "WIN32;"
        "_LIB;"
        "UNICODE;"
        "_UNICODE"
    )
endif()

################################################################################
# Compile and link options
################################################################################
if(MSVC)
    if("${CMAKE_VS_PLATFORM_NAME}" STREQUAL "x64")
        target_compile_options(${PROJECT_NAME} PRIVATE
            $<$<CONFIG:Debug>:
                ${DEFAULT_CXX_DEBUG_RUNTIME_LIBRARY}
            >
            $<$<CONFIG:Release_optimized>:
                ${DEFAULT_CXX_RUNTIME_LIBRARY}
            >
            $<$<CONFIG:Release_with_dbg>:
                ${DEFAULT_CXX_RUNTIME_LIBRARY}
            >
            $<$<CONFIG:Release>:
                /permissive-;
                /O2;
                /Oi;
                /sdl;
                ${DEFAULT_CXX_RUNTIME_LIBRARY};
                /Gy;
                /W3
            >
            ${DEFAULT_CXX_DEBUG_INFORMATION_FORMAT};
            ${DEFAULT_CXX_EXCEPTION_HANDLING}
        )
    elseif("${CMAKE_VS_PLATFORM_NAME}" STREQUAL "x86")
        target_compile_options(${PROJECT_NAME} PRIVATE
            $<$<CONFIG:Debug>:
                /Od;
                ${DEFAULT_CXX_DEBUG_RUNTIME_LIBRARY}
            >
            $<$<CONFIG:Release_optimized>:
                /O2;
                /Oi;
                ${DEFAULT_CXX_RUNTIME_LIBRARY};
                /Gy
            >
            $<$<CONFIG:Release_with_dbg>:
                /O2;
                /Oi;
                ${DEFAULT_CXX_RUNTIME_LIBRARY};
                /Gy
            >
            $<$<CONFIG:Release>:
                /O2;
                /Oi;
                ${DEFAULT_CXX_RUNTIME_LIBRARY};
                /Gy
            >
            /permissive-;
            /sdl;
            /W3;
            ${DEFAULT_CXX_DEBUG_INFORMATION_FORMAT};
            ${DEFAULT_CXX_EXCEPTION_HANDLING}
        )
    endif()
    if("${CMAKE_VS_PLATFORM_NAME}" STREQUAL "x64")
        target_link_options(${PROJECT_NAME} PRIVATE
            $<$<CONFIG:Release_optimized>:
                /OPT:REF;
                /OPT:ICF
            >
            $<$<CONFIG:Release_with_dbg>:
                /OPT:REF;
                /OPT:ICF
            >
            $<$<CONFIG:Release>:
                /OPT:REF;
                /OPT:ICF
            >
            /SUBSYSTEM:WINDOWS
        )
    elseif("${CMAKE_VS_PLATFORM_NAME}" STREQUAL "x86")
        target_link_options(${PROJECT_NAME} PRIVATE
            $<$<CONFIG:Release_optimized>:
                /OPT:REF;
                /OPT:ICF
            >
            $<$<CONFIG:Release_with_dbg>:
                /OPT:REF;
                /OPT:ICF
            >
            $<$<CONFIG:Release>:
                /OPT:REF;
                /OPT:ICF
            >
            /SUBSYSTEM:WINDOWS
        )
    endif()
endif()

