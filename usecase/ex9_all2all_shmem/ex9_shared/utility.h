#pragma once


#include <vector>
#include <atomic>
#include <string>
#include <appkit/appkit.h>
#include "./conditions.h"


namespace ex9 {

extern short port;
extern const std::string localhostAddress;
extern const crm::identity_descriptor_t unitID;
extern const crm::identity_descriptor_t hubID;
extern const crm::identity_descriptor_t hostID;
extern const std::string hostChannelName;
extern const std::string hubChannelName;
extern const std::string unitChannelName;

extern std::atomic<std::uintmax_t> _sndCounter;
extern std::atomic<std::uintmax_t> _rcvCounter;
extern std::atomic<std::uintmax_t> _excCounter;

static constexpr auto freqExcCollectPrint = 10;
extern size_t payloadSize;

#ifdef CBL_MPF_TEST_ERROR_EMULATION
constexpr size_t stat_step = 10;
constexpr size_t print_step = 20;
#else
constexpr size_t stat_step = 100;
constexpr size_t print_step = 200;
#endif

enum class message_cathegory {
	lower_bound,
	outcome_request_opeer,
	outcome_request_ipeer,
	upper_bound,
	__count
	};

const char* catname(message_cathegory c);

struct custom_message_strob_1 {
	std::vector<std::uint8_t> blob;
	std::string key;
	message_cathegory cat;
	};

struct statistics {
	size_t payload;
	size_t serialized;
	};

size_t payload_size(const custom_message_strob_1 & v)noexcept;

template<typename T>
statistics get_stat(const T & v)noexcept {
	return statistics{payload_size(v), crm::srlz::object_trait_serialized_size(v)};
	}




template< class, class = std::void_t<> >
struct is_blob_type : std::false_type {};

template< class T >
struct is_blob_type<T, std::void_t<decltype(std::declval<T>().data())>> : std::true_type {};

template<class T>
bool constexpr is_blob_type_v = is_blob_type<T>::value;




template<typename _TBlob,
	typename TBlob = typename std::decay_t<_TBlob>,
	typename std::enable_if_t<is_blob_type_v<TBlob> && !crm::srlz::detail::is_string_type_v<TBlob>, int> = 0>
typename TBlob initialize(size_t count) {

#if (CRM_EXAMPLES_DATA_USE_AS == CRM_EXAMPLES_DATA_USE_AS_STATIC)
	return TBlob( 0x1, count );

#elif (CRM_EXAMPLES_DATA_USE_AS == CRM_EXAMPLES_DATA_USE_AS_DYNAMIC)
	TBlob v;
	v.resize(count);

	size_t ib = 0;
	while (ib < count) {
		auto u16 = crm::sx_uuid_t::rand();
		for (size_t i = 0; i < crm::sx_uuid_t::count_of_as<decltype(v[0])>() && ib < count; ++i, ++ib) {
			v[ib] = u16.at<decltype(v[0])>(i);
			}
		}

	return v;
#endif
	}


template<typename _TString,
	typename TString = typename std::decay_t<_TString>,
	typename std::enable_if_t<crm::srlz::detail::is_string_type_v<TString>, int> = 0>
typename TString initialize(size_t count) {
#if (CRM_EXAMPLES_DATA_USE_AS == CRM_EXAMPLES_DATA_USE_AS_STATIC)
	return TString( (TString::value_type)0x45, count );

#elif (CRM_EXAMPLES_DATA_USE_AS == CRM_EXAMPLES_DATA_USE_AS_DYNAMIC)
	TString v;
	v.resize(count);

	size_t ib = 0;
	while (ib < count) {
		auto rs = crm::sx_uuid_t::rand().to_string<TString::value_type>();
		for (size_t i = 0; i < rs.size() && ib < count; ++i, ++ib) {
			v[ib] = rs[i];
			}
		}

	return v;
#endif
	}


void initialize(custom_message_strob_1 & v, size_t payload);
void print(std::list<crm::detail::exceptions_trace_collector::item_value> && l);



struct stat_entry {
	mutable std::mutex _lck;
	size_t count{0};
	size_t payload_count{0};
	size_t serialized_count{0};
	double mps{0};
	std::chrono::microseconds latencySysMax{0};
	std::chrono::microseconds latencySysMin{0};
	std::chrono::microseconds latencySysAver{0};
	std::chrono::microseconds latencyThisMax{0};
	std::chrono::microseconds latencyThisMin{0};
	std::chrono::microseconds latencyThisAver{0};

	using timeline_duration_t = std::chrono::seconds;
	using timeline_line_t = decltype(std::declval<timeline_duration_t>().count());
	timeline_line_t timeline{0};

	stat_entry();

	void __add(const custom_message_strob_1 & v);
	void __add(const std::chrono::high_resolution_clock::time_point & startTP,
		const std::chrono::microseconds & sysLatency);
	void add(const custom_message_strob_1 & v);
	void add(const custom_message_strob_1 & v,
		const std::chrono::high_resolution_clock::time_point & startTP,
		const std::chrono::microseconds & sysLatency);
	std::string print()const;
	};

class stat_lenta {
	std::array<stat_entry, ((int)message_cathegory::__count + 1)> _lenta;
	std::atomic<size_t> addcnt{0};

	void add_value(const custom_message_strob_1 & v);
	void add_value(const custom_message_strob_1 & v,
		const std::chrono::high_resolution_clock::time_point & startTP,
		const std::chrono::microseconds & sysLatency);
	void add_post();

public:
	void add(const custom_message_strob_1 & v);
	void add(const custom_message_strob_1 & v,
		const std::chrono::high_resolution_clock::time_point & startTP,
		const std::chrono::microseconds & sysLatency);
	};

extern stat_lenta stgl;

template<typename TRox>
void receive_stat(TRox && rox, custom_message_strob_1 && roxResponse,
	const std::chrono::high_resolution_clock::time_point & startTP,
	const std::chrono::microseconds & sysLatency) {

	stgl.add(roxResponse, startTP, sysLatency);
	}

template<typename TRox>
void receive_stat(TRox && rox, custom_message_strob_1 && roxResponse) {

	stgl.add(roxResponse);
	}

template<typename TRox>
void recv_response(std::weak_ptr<crm::distributed_ctx_t> ctx,
	TRox && xpeer,
	custom_message_strob_1 && roxResponse,
	crm::sndrcv_timeline_periodicity_t tp,
	const std::chrono::high_resolution_clock::time_point & startTP,
	const std::chrono::microseconds & sysLatency) {

	receive_stat(xpeer, std::move(roxResponse), startTP, sysLatency);
	send(ctx, crm::detail::make_invokable_xpeer(std::forward<TRox>(xpeer)), tp);
	}

template<typename TRox>
void send(std::weak_ptr<crm::distributed_ctx_t> ctx,
	TRox && xpeer,
	crm::sndrcv_timeline_periodicity_t tp) {

	if (xpeer.is_opened()) {
		custom_message_strob_1 m;
		initialize(m, payloadSize);

		if (xpeer.direction() == crm::xpeer_direction_t::in) {
			m.cat = message_cathegory::outcome_request_ipeer;
			}
		else if (xpeer.direction() == crm::xpeer_direction_t::out) {
			m.cat = message_cathegory::outcome_request_opeer;
			}
		else {
			THROW_EXC_FWD(nullptr);
			}

		auto next = crm::make_scoped_execution([xpeer, tp, ctx]() {
			if (tp.is_cyclic() && (xpeer.is_opened() || xpeer.renewable_connection())) {
				if (auto c = ctx.lock()) {
					c->register_timer_deffered_action_sys("ex9_shared::send", [xpeer, tp, ctx](crm::async_operation_result_t)mutable {

						send(ctx, std::move(xpeer), tp);
						}, std::chrono::seconds(2));
					}
				}
			});

		auto rh = crm::utility::bind_once_call({ "example-1" }, 
			[ctx, xpeer, tp, timepoint = std::chrono::high_resolution_clock::now()](crm::async_operation_result_t r,
			custom_message_strob_1 && obj,
			std::unique_ptr<crm::dcf_exception_t> && exc_,
			std::chrono::microseconds && mcs_,
			decltype(next) && n)mutable noexcept{

			++_sndCounter;

#ifdef CBL_SNDRCV_TRACE_COLLECT
			if (exc_) {
				auto excIdx = ++_excCounter;
				if (0 == (excIdx % freqExcCollectPrint)) {
					print(crm::detail::exceptions_trace_collector::instance().list());
					}
			}
#endif

			if (r == crm::async_operation_result_t::st_ready) {
				CBL_VERIFY(!exc_);
				n.reset();

				recv_response(ctx, xpeer, std::move(obj), tp, timepoint, mcs_);
				}
			else {
				n.execute();
				}
		}, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::move(next));

		crm::ainvoke(
			ctx,
			xpeer,
			std::move(m),
			std::move(rh),
			__FILE_LINE__,
			crm::sndrcv_timeline_t::make_once_call());
	}
}

template<typename XIO>
void input_handler(XIO && lnk,
	crm::detail::i_xpeer_rt1_t::input_value_t && message) {

	auto m2 = crm::message_forward<custom_message_strob_1>(std::move(message).m());
	if (m2) {
		auto v = std::move(m2).value();
		auto cat = v.value().cat;

		receive_stat(lnk, std::move(v).value());

		custom_message_strob_1 m;
		initialize(m, payloadSize);
		m.cat = cat;

		crm::detail::ostrob_message_envelop_t<custom_message_strob_1 > response(std::move(m));
		response.capture_response_tail((*v.source()));

		lnk.push(std::move(response));
		v.commit();
		}
	}
}
