#include "./utility.h"

using namespace ex9;

stat_lenta stgl;


 short ex9::port = 16789;
 const std::string ex9::localhostAddress = "127.0.0.1";
 const crm::identity_descriptor_t ex9::unitID = {crm::global_object_identity_t::as_hash("FAFA0000-0A0A-F1F1-AAAA-100000000003"), "unit"};
 const crm::identity_descriptor_t ex9::hubID = {crm::global_object_identity_t::as_hash("FAFA0000-0A0A-F1F1-AAAA-100000000002"), "hub"};
 const crm::identity_descriptor_t ex9::hostID = {crm::global_object_identity_t::as_hash("FAFA0000-0A0A-F1F1-AAAA-100000000001"), "host"};

 const std::string ex9::hostChannelName = "ex9_host_channel_memory";
 const std::string ex9::hubChannelName = "ex9_hub_channel_memory";
 const std::string ex9::unitChannelName = "ex9_unit_channel_memory";

 std::atomic<std::uintmax_t> ex9::_sndCounter{0};
 std::atomic<std::uintmax_t> ex9::_rcvCounter{0};
 std::atomic<std::uintmax_t> ex9::_excCounter{0};

 size_t ex9::payloadSize = 64;
 stat_lenta ex9::stgl;


void stat_lenta::add_value(const custom_message_strob_1 & v) {

	const auto idx = static_cast<int>(v.cat);
	_lenta.at(idx).add(v);
	}

void stat_lenta::add_value(const custom_message_strob_1 & v,
	const std::chrono::high_resolution_clock::time_point & startTP,
	const std::chrono::microseconds & sysLatency) {

	const auto idx = static_cast<int>(v.cat);
	_lenta.at(idx).add(v, startTP, sysLatency);
	}

void stat_lenta::add_post() {
	if ((++addcnt % print_step) == 1) {
		for (auto ic = (int)message_cathegory::outcome_request_opeer; ic < (int)message_cathegory::upper_bound; ++ic) {
			auto c = static_cast<message_cathegory>(ic);
			std::cout << std::setw(20) << catname(c) << " >> " << _lenta.at(ic).print() << std::endl;
			}
		}
	}

void stat_lenta::add(const custom_message_strob_1 & v) {
	add_value(v);
	add_post();
	}

void stat_lenta::add(const custom_message_strob_1 & v,
	const std::chrono::high_resolution_clock::time_point & startTP,
	const std::chrono::microseconds & sysLatency) {

	add_value(v, startTP, sysLatency);
	add_post();
	}






stat_entry::stat_entry()
	: timeline{std::chrono::duration_cast<timeline_duration_t>(std::chrono::system_clock::now().time_since_epoch()).count()} {}

void stat_entry::__add(const custom_message_strob_1 & v) {
	const auto st = get_stat(v);

	payload_count += st.payload;
	serialized_count += st.serialized;

	const auto c_ = ++count;
	if (c_ >= stat_step && (c_ % stat_step) == 0) {
		auto currTL = std::chrono::duration_cast<timeline_duration_t>(std::chrono::system_clock::now().time_since_epoch()).count();
		auto tl = currTL - timeline;

		mps = (double)c_ / (double)tl;
		}
	}

void stat_entry::__add(const std::chrono::high_resolution_clock::time_point & startTP,
	const std::chrono::microseconds & sysLatency) {

	auto thisLatency = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - startTP);

	if (!latencyThisMin.count()) {
		latencyThisMin = thisLatency;
		}
	else if (thisLatency < latencyThisMin) {
		latencyThisMin = thisLatency;
		}

	if (thisLatency > latencyThisMax) {
		latencyThisMax = thisLatency;
		}

	if (latencyThisAver.count()) {
		latencyThisAver = (latencyThisAver + thisLatency) / 2;
		}
	else {
		latencyThisAver = thisLatency;
		}


	if (!latencySysMin.count()) {
		latencySysMin = sysLatency;
		}
	else if (sysLatency < latencySysMin) {
		latencySysMin = sysLatency;
		}

	if (sysLatency > latencySysMax) {
		latencySysMax = sysLatency;
		}

	if (latencySysAver.count()) {
		latencySysAver = (latencySysAver + sysLatency) / 2;
		}
	else {
		latencySysAver = sysLatency;
		}
	}

void stat_entry::add(const custom_message_strob_1 & v) {
	std::lock_guard<std::mutex> lck(_lck);
	__add(v);
	}

void stat_entry::add(const custom_message_strob_1 & v,
	const std::chrono::high_resolution_clock::time_point & startTP,
	const std::chrono::microseconds & sysLatency) {

	std::lock_guard<std::mutex> lck(_lck);
	__add(v);
	__add(startTP, sysLatency);
	}

std::string stat_entry::print()const {
	std::lock_guard<std::mutex> lck(_lck);

	std::ostringstream ol;
	ol <<
		"count=" << std::setw(6) << count <<
		", mps=" << std::setw(6) << (size_t)std::trunc(mps) <<
		", latency(sys/there, microsec)=" << std::setw(8) << latencySysAver.count() << ":" << std::setw(8) << latencyThisAver.count() <<
		", payload(bytes)=" << std::setw(10) << payload_count <<
		", payload/serialized=" << std::setw(6) << (double)payload_count / (double)serialized_count;

	return ol.str();
	}


const char* ex9::catname(message_cathegory c) {
	switch (c) {
			case message_cathegory::lower_bound:
				return "upper bound(serialize required)";
			case message_cathegory::outcome_request_opeer:
				return "outcome_request_opeer";
			case message_cathegory::outcome_request_ipeer:
				return "outcome_request_ipeer";
			case message_cathegory::upper_bound:
				return "upper_bound";
			case message_cathegory::__count:
				return "";
			default:
				FATAL_ERROR_FWD(nullptr);
		}
	}

size_t ex9::payload_size(const custom_message_strob_1 & v)noexcept {
	return v.blob.size() * sizeof(std::decay_t<decltype(v.blob[0])>) + v.key.size() * sizeof(std::decay_t<decltype(v.key[0])>);
	}

void ex9::initialize(custom_message_strob_1 & v, size_t payload) {
	if (payload) {
		auto ksize = std::max((payload / 10), 1ull);
		auto bsize = std::max((payload - ksize), 1ull);

		v.blob = initialize<decltype(v.blob)>(bsize);
		v.key = initialize<decltype(v.key)>(ksize);
		}
	}

void ex9::print(std::list<crm::detail::exceptions_trace_collector::item_value> && l) {
	if (!l.empty()) {
		std::wostringstream os;
		for (auto && i : l) {
			os << L"code=" << i.code << L", num=" << i.count << L", m=" << i.message << L", file=" << i.file << L", line=" << i.line << std::endl;
			}

		crm::file_logger_t::logging(os.str(), 99998888);
		}
	}
