﻿#include <iostream>
#include <hublon/r0.h>
#include "../ex9_shared/utility.h"


using namespace crm;
using namespace ex9;


std::shared_ptr<crm::distributed_ctx_t> make_context() {
	crm::distributed_ctx_policies_t policies;
	policies.set_rt_ping(true);

	return crm::distributed_ctx_t::make(crm::context_bindings(), std::move(policies));
	}

int main() {
	auto ctx = make_context();
	ipc_host_options options(ctx, hostID, crm::detail::tcp::tcp_endpoint_t(localhostAddress, port));
	options.set_channel_options(detail::xchannel_shmem_host_args{ hostChannelName });

	static_assert(crm::is_ipc_hosh_options_v< ipc_host_options>, __FILE_LINE__);
	auto host = detail::ipc_host::make(ctx, std::move(options));

	ipc_host_start_options startopt;
	startopt.mem_size = CRM_MB * 32;
	host->start(startopt);
	while(true) {
		std::this_thread::sleep_for(std::chrono::seconds(5));
		}

	std::cout << "press any key..." << std::endl;
	getchar();

	host->close();
	}

