﻿#include <iostream>
#include <hublon/r1.h>
#include "../ex9_shared/utility.h"


using namespace crm;
using namespace ex9;


std::shared_ptr<crm::distributed_ctx_t> make_context() {
	crm::distributed_ctx_policies_t policies;
	policies.set_rt_ping(true);

	return crm::distributed_ctx_t::make(crm::context_bindings(), std::move(policies));
	}

class outbound_handler_t : public rt1_connection_settings_t {
	std::weak_ptr<crm::distributed_ctx_t> _ctx;

public:
	outbound_handler_t(std::weak_ptr<crm::distributed_ctx_t> ctx,
		const crm::identity_descriptor_t & thisId,
		std::unique_ptr<crm::rt1_endpoint_t> && ep,
		std::function<void(crm::input_message_knop_l1&& )> && mh)
		: rt1_connection_settings_t(thisId, std::move(ep), std::move(mh))
		, _ctx(ctx) {}

	void connection_handler(std::unique_ptr<crm::dcf_exception_t> &&, xpeer_link&& lnk)final {
		send(_ctx, std::move(lnk), crm::sndrcv_timeline_periodicity_t::make_cyclic(std::chrono::seconds(1)));
		}
	};

std::shared_ptr< detail::ipc_unit> hub;

class{
private:
	std::map<crm::ipeer_t::remote_object_endpoint_t, crm::opeer_t> _opx;
	mutable std::mutex _opxLock;

public:
	bool exists(const crm::ipeer_t::remote_object_endpoint_t& rdp)const noexcept {
		std::unique_lock lck(_opxLock);
		auto it = _opx.find(rdp);
		return it != _opx.cend();
		}

	void insert(crm::ipeer_t::remote_object_endpoint_t rdp, crm::opeer_t peer) {
		std::unique_lock lck(_opxLock);
		_opx.insert(std::make_pair(std::move(rdp), std::move(peer)));
		}

	std::optional<crm::opeer_t> find(const crm::ipeer_t::remote_object_endpoint_t& rdp) {
		std::unique_lock lck(_opxLock);
		auto it = _opx.find(rdp);
		if(it != _opx.cend()) {
			return std::make_optional(it->second);
			}
		else {
			return std::nullopt;
			}
		}
	}_optb;

void push_opeer_message(const crm::ipeer_t::remote_object_endpoint_t& rdp,
	crm::opeer_t::input_value_t&& m) {

	auto opeer = _optb.find(rdp);
	if(opeer) {
		input_handler(opeer.value(), std::move(m));
		}
	}

class example_hub_options : public crm::ipc_unit_options {
	void validate_reverse_connect(const crm::ipeer_t& rox) {
		auto rdp = crm::ipeer_t::remote_object_endpoint_t::make_remote(localhostAddress, port, rox.id());
		if(!_optb.exists(rdp)) {
			auto xpeer1 = hub->make_outbound(outbound_handler_t(
				ctx(),
				hubID,
				std::make_unique<crm::rt1_endpoint_t>(rdp),
				[rdp](crm::input_message_knop_l1&& m) {

				push_opeer_message(rdp, std::move(m));
				}));

			send(ctx(), detail::make_invokable_xpeer(xpeer1), crm::sndrcv_timeline_periodicity_t::make_cyclic(std::chrono::seconds(2)));

			_optb.insert(std::move( rdp), std::move(xpeer1));
			}
		}

	void connection_handler(crm::ipeer_t&& xpeer, crm::datagram_t&& initializeResult)final {
		validate_reverse_connect(xpeer);
		send(ctx(), std::move(xpeer), crm::sndrcv_timeline_periodicity_t::make_cyclic(std::chrono::seconds(2)));
		}

	bool remote_object_identity_validate(const crm::identity_value_t& idSet,
		std::unique_ptr<crm::i_peer_remote_properties_t>& prop)final {

		return true;
		}

public:
	example_hub_options(crm::detail::xchannel_shmem_hub_options && opt)
		: crm::ipc_unit_options(std::move(opt)){}
	};

class all2all_custom_hub_t{
private:
	std::weak_ptr<crm::distributed_ctx_t> _ctx;

public:
	all2all_custom_hub_t(std::weak_ptr<crm::distributed_ctx_t> c)
		: _ctx(c){}

	void begin_outbound_connection() {
		auto rdp = crm::rt1_endpoint_t::make_remote(localhostAddress, port, hubID);

		auto outboundH = outbound_handler_t(
			_ctx,
			unitID,
			std::make_unique<crm::rt1_endpoint_t>(rdp),
			[rdp](crm::input_message_knop_l1&& m) {

			push_opeer_message(rdp, std::move(m));
			});

		auto xpeer1 = hub->make_outbound(std::move(outboundH));

		_optb.insert(std::move(rdp), std::move(xpeer1));
		}
	};


int main() {
	auto c = make_context();

	crm::detail::xchannel_shmem_hub_options thisChannelOpt;
	thisChannelOpt.mainNodeName = unitChannelName;

	example_hub_options opt(std::move(thisChannelOpt));

	detail::xchannel_shmem_host_args a;
	a.xchannelName.mainNodeName = hostChannelName;
	opt.set_host_channel(a);
	opt.set_this_id(unitID);
	opt.set_input_stack([](crm::ipeer_t && lnk, crm::input_message_knop_l1&& m) {
		input_handler<crm::ipeer_t>(std::move(lnk), std::move(m));
		});

	hub = detail::ipc_unit::make(c, std::move(opt));

	hub->register_message_type<custom_message_strob_1>();

	ipc_unit_start_options startopt;
	startopt.message_mem_size = CRM_MB * 32;
	startopt.command_mem_size = CRM_MB;

	hub->start(startopt);

	all2all_custom_hub_t unit(c);
	unit.begin_outbound_connection();

	while (true) {
		std::this_thread::sleep_for(std::chrono::seconds(5));
		}

	std::cout << "press any key..." << std::endl;
	getchar();

	hub->close();
	}
