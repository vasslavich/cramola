#include "./streams_test_classes.h"
#include "./utilmk.h"
#include "../ex_utility/fileop.h"


using namespace crm;
using namespace crm::testmark;


static_assert(crm::is_stream_cover_data_trait_v<my_cover_data > , __FILE_LINE__);

const srlz::typemap_key_t my_cover_data::stream_blob_typeid = crm::srlz::typemap_key_t::make<my_cover_data>();

using binary_wstream_t = crm::wstream_container_t<std::vector<uint8_t>>;
using file_rstream_adapter_t = crm::rstream_file_adapter_t;
using file_wstream_t = crm::wstream_file_t;


namespace filesystem{
using namespace std::filesystem;
}


const size_t i_send_buffer_t::aggreementStep{ 1 * 128 * 1024 };
std::atomic<size_t> i_send_buffer_t::aggreement{ 1 * 1024 * 1024 };
const size_t  i_send_buffer_t::aggreementMax  { 2 * 1024 * 1024ull };

std::atomic<int> testsuite_base_t::testIdCounter;

i_send_buffer_t::i_send_buffer_t(){}

i_send_buffer_t::i_send_buffer_t( i_send_buffer_t && o )
	: outReady( o.outReady.load() )
	, inputReady( o.inputReady.load() )
	, result( std::move(o.result) ){}

i_send_buffer_t& i_send_buffer_t::operator=( i_send_buffer_t && o ){
	outReady = o.outReady.load();
	inputReady = o.inputReady.load();
	result = std::move(o.result);

	return (*this);
	}

memory_buffer_t::memory_buffer_t(){
	aggreement += aggreementStep;
	aggreement = aggreement % aggreementMax;

	size_t dataSize = aggreement;

	/*while( dataSize < (uint64_t)(1024 * 1024 ))
	dataSize = crm::sx_uuid_t::rand().u64_2[0] % (uint64_t)(1024 * 1024 * 1024);*/


	value.resize( dataSize );
	fill( value, dataSize );
	}

memory_buffer_t::memory_buffer_t( memory_buffer_t && o )
	: i_send_buffer_t( std::move( o ) )
	, value( std::move( o.value ) ){}

memory_buffer_t& memory_buffer_t::operator=( memory_buffer_t && o ){
	i_send_buffer_t::operator=( std::move( o ) );
	value = std::move( o.value );

	return (*this);
	}





file_buffer_t::file_buffer_t(){}

file_buffer_t::~file_buffer_t(){
	close();
	}

void file_buffer_t::close()noexcept{
	if( !_path.empty() ){
		filesystem::remove( _path );
		_path.clear();
		}
	}

file_buffer_t::file_buffer_t( file_buffer_t && o )
	: i_send_buffer_t( std::move( o ) )
	, _path( std::move( o._path ) )
	, _size( o._size ){

	o._path.clear();
	}

file_buffer_t& file_buffer_t::operator=( file_buffer_t && o ){
	i_send_buffer_t::operator=( std::move( o ) );

	close();

	_size = o._size;

	_path = std::move( o._path );
	o._path.clear();

	return (*this);
	}

size_t file_buffer_t::size()const noexcept{
	return _size;
	}

std::wstring file_buffer_t::path()const noexcept{
	return crm::cvt<wchar_t>( _path.string() );
	}

file_buffer_t file_buffer_t::make_rand( const std::wstring & root, const std::vector<uint8_t> & buf ){
	file_buffer_t obj;

	char fn[L_tmpnam];
	std::tmpnam( fn );
	obj._path = root / filesystem::path( fn );

	usecase::write2file_binary( buf, crm::cvt<wchar_t>( obj._path.string() ) );
	obj._size = buf.size();

	return obj;
	}

file_buffer_t file_buffer_t::make_rand( const std::wstring & root ){
	aggreement += aggreementStep;
	aggreement = aggreement % aggreementMax;

	size_t dataSize = aggreement;

	std::vector<uint8_t> value( dataSize );
	fill( value, dataSize );

	return make_rand( root, value );
	}

void file_buffer_t::read( size_t off, size_t count, std::vector<uint8_t> & buf )const{
	buf.resize( count );

	FILE* fi = std::fopen( _path.string().c_str(), "rb" );
	if( fi ){
		_fseeki64( fi, off, SEEK_SET );
		if( fread( buf.data(), 1, count, fi ) != count ){
			THROW_EXC_FWD(nullptr);
			}
		}
	else{
		THROW_EXC_FWD(nullptr);
		}
	}

bool file_buffer_t::cmp_with_file( const std::wstring & file )const noexcept{
	static const size_t buffer_size = 1024 * 1024;

	const auto rSize = std::filesystem::file_size( std::filesystem::path( file ) );
	if( rSize != _size )
		return false;

	std::vector<uint8_t> lBlock( buffer_size );
	std::vector<uint8_t> rBlock( buffer_size );

	size_t off = 0;
	while( off < _size ){

		const auto tailSize = (std::min)(_size - off, buffer_size);

		read( off, tailSize, lBlock );
		usecase::read_from_file_binary( file, rBlock, off, tailSize );

		if( !cmpv( lBlock, rBlock ) ){
			return false;
			}

		off += tailSize;
		}

	return true;
	}


std::vector<uint8_t> rt1_custom_outxproperties_t::serialize()const{
	auto ws( crm::srlz::wstream_constructor_t::make() );
	crm::srlz::serialize( ws, indexPeer );
	crm::srlz::serialize( ws, testId );
	return ws.release();
	}

void rt1_custom_outxproperties_t::deserialize( const std::vector<uint8_t> & buf ){
	auto rs( crm::srlz::rstream_constructor_t::make( buf.cbegin(), buf.cend() ) );
	crm::srlz::deserialize( rs, indexPeer );
	crm::srlz::deserialize( rs, testId );
	}


rt1_custom_outconnection_t::rt1_custom_outconnection_t( const crm::identity_descriptor_t & thisId,
	size_t index_,
	std::unique_ptr<crm::rt1_endpoint_t> && ep,
	std::function<void(crm::input_message_knop_l1 && m)>&& mh,
	std::weak_ptr<testsuite_base_t> testsuite_ )
	: crm::rt1_connection_settings_t( thisId, std::move( ep ), std::move( mh ) )
	, indexPeer( index_ )
	, testsuite( testsuite_ ){}

void rt1_custom_outconnection_t::event_handler( std::unique_ptr<crm::i_event_t> && ev ){

	if( ev ){
		if( auto s = testsuite.lock() ){
			s->event_handler( indexPeer, std::move( ev ) );
			}
		else{
			THROW_EXC_FWD(nullptr);
			}
		}
	}

crm::remote_object_connection_value_t rt1_custom_outconnection_t::local_object_identity(){
	crm::identity_value_t identity;
	identity.id_value = this_id();

	/* �������� ������� �������
	--------------------------------------------*/
	rt1_custom_outxproperties_t prop;
	prop.indexPeer = indexPeer;
	prop.testId = CHECK_PTR( testsuite )->test_id();

	identity.properties.add( crm::connection_stage_descriptions_t::identity_user_options, prop.serialize() );

	return crm::remote_object_connection_value_t( std::move( identity ), crm::syncdata_list_t() );
	}





opeer_item_t::opeer_item_t(){}

opeer_item_t::opeer_item_t( opeer_item_t && o )
	: peer( std::move( o.peer ) ), doClose( o.doClose.load() ){}

opeer_item_t& opeer_item_t::operator=( opeer_item_t && o ){
	peer = std::move( o.peer );
	doClose.store( o.doClose.load() );

	return (*this);
	}








bool testsuite_base_t::validate_count(const std::tuple<size_t, size_t, size_t> & vc) {
	return std::get<0>(vc) == std::get<2>(vc) && std::get<1>(vc) == std::get<2>(vc);
	}

bool testsuite_base_t::validate_count_2()const {
	return validate_count(validate_count());
	}

testsuite_base_t::testsuite_base_t()
	: testId(++testIdCounter) {}

testsuite_base_t::~testsuite_base_t() {
	_clear();
	}

void testsuite_base_t::set_peers(std::unique_ptr<i_peer_collection> && ips) {
	_peers = std::move(ips);
	}

const crm::opeer_t& testsuite_base_t::peer(int id)const {
	return _peers->peer(id);
	}

crm::opeer_t& testsuite_base_t::peer( int id ){
	return _peers->peer( id );
	}

bool testsuite_base_t::is_connected(int pid)const {
	return _peers->is_connected(pid);
	}

int testsuite_base_t::errc(int pid)const {
	return _peers->errc(pid);
	}

bool testsuite_base_t::set_is_connected(int pid) {
	return _peers->set_is_connected(pid);
	}

void testsuite_base_t::set_is_error(int pid) {
	_peers->set_is_errc(pid);
	}

size_t testsuite_base_t::size_2d()const {
	return _size2D;
	}

void testsuite_base_t::close_peers() {
	if(_peers)
		_peers->close();
	}

int testsuite_base_t::test_id() const {
	return testId;
	}

void testsuite_base_t::clear() {
	std::unique_lock<std::mutex> lck(siglock);
	_clear();
	}

void testsuite_base_t::clear_with_lock() {
	_clear();
	}

size_t testsuite_base_t::peers_count()const {
	if(_peers)
		return _peers->size();
	else
		return 0;
	}


size_t testsuite_base_t::objects_count(size_t peerId)const {
	return _sources.at(peerId).size();
	}

std::shared_future<bool> testsuite_base_t::resultf(size_t peerId, size_t objectIndex)const {
	return _sources.at(peerId).at(objectIndex)->result;
	}

size_t testsuite_base_t::object_size(size_t peerId, size_t objectIndex)const {
	return _sources.at(peerId).at(objectIndex)->size();
	}

std::shared_ptr<i_send_buffer_t> testsuite_base_t::at(size_t peerId, size_t objectId)const {
	return _sources.at(peerId).at(objectId);
	}

std::shared_ptr<i_send_buffer_t> testsuite_base_t::at(size_t peerId, size_t objectId) {
	return _sources.at(peerId).at(objectId);
	}

std::shared_ptr<crm::i_stream_reader_t> testsuite_base_t::get_reader(size_t peerId, size_t objectId)const {
	return _sreaders.at(peerId).at(objectId);
	}

std::shared_ptr<crm::i_stream_reader_t> testsuite_base_t::get_reader(size_t peerId, size_t objectId) {
	return _sreaders.at(peerId).at(objectId);
	}

bool testsuite_base_t::ready()const noexcept {
	return _readyf.load(std::memory_order::memory_order_acquire);
	}

bool testsuite_base_t::is_stream_started(int peerId, int objectId)const {
	return _sources.at(peerId).at(objectId)->stream_startedf;
	}

void testsuite_base_t::set_stream_started(int peerId, int objectId) {
	_sources.at(peerId).at(objectId)->stream_startedf.exchange(true);
	}





void testsuite_memory_buffer_t::start_ostream(my_cover_data && keys,
	int peerId,
	int objectId,
	size_t /*idStream*/,
	crm::opeer_t& xpeer_,
	ostream_end_f&& endh) {

	auto blocksSize = 1024ull;

	_EntryCounter++;

	auto pS = my_rstream_contract_wrapper(std::dynamic_pointer_cast<rstream_type>(get_reader(peerId, objectId)));
	auto fr = crm::make_rstream_handler(xpeer_.ctx(),
		pS,
		std::move(keys),
		blocksSize,
		std::chrono::seconds(100000),
		(std::to_wstring(peerId) + L":" + std::to_wstring(objectId) + L":mem"));

	crm::make_ostream_launch(crm::async_space_t::ext,
		xpeer_.ctx(),
		xpeer_,
		std::move(fr),
		[](const std::unique_ptr<crm::dcf_exception_t>& )noexcept {
		return true;
		},
		std::move(endh));

	++_IncCounter;
	set_stream_started(peerId, objectId);
	}

void testsuite_memory_buffer_t::make_place(const std::shared_ptr<crm::distributed_ctx_t> & ctx,
	size_t peersCount, size_t objectsCount) {

	auto ireaderPairMk = [ctx_ = ctx](size_t /*ipr*/, size_t /*iobj*/) {
		auto smem = std::make_shared<memory_buffer_t>();
		std::tuple < std::shared_ptr<i_send_buffer_t>, std::shared_ptr<crm::i_stream_reader_t>> r;
		r = std::make_tuple(std::move(smem), std::make_shared<rstream_type>(crm::make_rstream_container_adapter(smem->value)));
		return std::move(r);
		};

	testsuite_base_t::make_place(ctx, peersCount, objectsCount, std::move(ireaderPairMk));
	}

void testsuite_memory_buffer_t::in_eof(int peerId, int objectId, std::unique_ptr<crm::i_stream_writer_t> && w) {
	if(++at(peerId, objectId)->inputReady == 1) {
		auto[w0, w1, w2] = check_ready();

		if(w) {
			set_result_handler(peerId, objectId, [&] {

				return std::async([this, w0, w1, w2, peerId, objectId](std::unique_ptr<crm::i_stream_writer_t> && w) {
					auto pStream = dynamic_cast<binary_wstream_t*>(w.get());
					CBL_VERIFY(pStream);

					auto dataInput = pStream->container_release();

					auto r = cmpv(dynamic_cast<const memory_buffer_t*>(at(peerId, objectId).get())->value, dataInput);
					if(r) {
						std::cout << "================== TEST{peer=" << peerId << " object=" << objectId << " in/out/full=" << w0 << " " << w1 << " " << w2 << "}: OK =================" << std::endl;
						}
					else {
						std::cout << "================== TEST{peer=" << peerId << " object=" << objectId << " in/out/full=" << w0 << " " << w1 << " " << w2 << "}: FAIL =================" << std::endl;
						}

					return r;
					}, std::move(w));
				});
			}
		}
	}

void testsuite_memory_buffer_t::event_handler(size_t /*indexPeer*/, std::unique_ptr<crm::i_event_t> && /*ev*/) {
	if(!ready()) {
		/*if(auto streamEvent = dynamic_cast<crm::i_input_stream_base_event_t*>(ev.get())) {
			if(auto blobStream = streamEvent->handler) {
				auto coverData = dynamic_cast<const my_cover_data*>(blobStream->stream_cover_data().get());
				auto peerId_ = coverData->peer_id;
				auto objectId = coverData->object_id;
				auto testId_ = coverData->test_id;
				auto streamGID = coverData->stream_gid;

				if (test_id() == testId_) {

					if (auto streamBegin = dynamic_cast<crm::i_input_stream_begin_t*>(ev.get())) {
						add(std::move(streamBegin->handler));
						}
					else if (auto streamEnd = dynamic_cast<crm::i_input_stream_eof_t*>(ev.get())) {
						if (blobStream->eof()) {
							in_eof(peerId_, objectId, std::move(*blobStream).writer());
							}
						}
					}
				}
			}*/
		}
	}

void testsuite_files_t::start_ostream(my_cover_data&& keys,
	int peerId,
	int objectId,
	size_t /*idStream*/,
	crm::opeer_t& xpeer_,
	ostream_end_f&& endh) {

	auto blocksSize = 1024ull;

	_EntryCounter++;

	auto pS = my_rstream_contract_wrapper(std::dynamic_pointer_cast<rstream_type>(get_reader(peerId, objectId)));
	auto fr = crm::make_rstream_handler(xpeer_.ctx(),
		pS,
		std::move(keys),
		blocksSize,
		std::chrono::seconds(100000),
		(std::to_wstring(peerId) + L":" + std::to_wstring(objectId) + L":mem"));

	crm::make_ostream_launch(crm::async_space_t::ext,
		xpeer_.ctx(),
		xpeer_,
		std::move(fr),
		[](const std::unique_ptr<crm::dcf_exception_t>& /*exc*/)noexcept {
		return true;
		},
		std::move(endh));

	++_IncCounter;
	set_stream_started(peerId, objectId);
	}

void testsuite_files_t::make_place(const std::shared_ptr<crm::distributed_ctx_t> & ctx,
	size_t peerCount,
	size_t objectsCount) {

	auto ireaderPairMk = [ctx_ = ctx](size_t /*ipr*/, size_t /*iobj*/) {
		auto sfile = std::make_shared<file_buffer_t>(file_buffer_t::make_rand(filesystem::current_path().string<std::wstring::value_type>()));
		using stream_type = decltype(crm::make_rstream_file_adapter(ctx_, sfile->path()));
		std::tuple < std::shared_ptr<i_send_buffer_t>, std::shared_ptr<crm::i_stream_reader_t>> r;
		r = std::make_tuple(std::move(sfile), std::make_shared<stream_type>(crm::make_rstream_file_adapter(ctx_, sfile->path())));
		return std::move(r);
		};

	testsuite_base_t::make_place(ctx, peerCount, objectsCount, std::move(ireaderPairMk));
	}


void testsuite_files_t::in_eof(int peerId, int objectId, std::unique_ptr<crm::i_stream_writer_t> && writer) {

	++at(peerId, objectId)->inputReady;
	auto chr = check_ready();

	if(writer) {
		set_result_handler(peerId, objectId, [&] {
			return std::async([this, chr, peerId, objectId](std::unique_ptr<crm::i_stream_writer_t> && w) {

				auto pStream = dynamic_cast<file_wstream_t*>(w.get());
				CBL_VERIFY(pStream);

				auto r = dynamic_cast<const file_buffer_t*>(at(peerId, objectId).get())->cmp_with_file(pStream->path().string<std::wstring::value_type>());
				if(r) {
					std::cout << "================== TEST{peer=" << peerId << " object=" << objectId << " in/out/full=" << std::get<0>(chr) << " " << std::get<1>(chr) << " " << std::get<2>(chr) << "}: OK =================" << std::endl;
					}
				else {
					std::cout << "================== TEST{peer=" << peerId << " object=" << objectId << " in/out/full=" << std::get<0>(chr) << " " << std::get<1>(chr) << " " << std::get<2>(chr) << "}: FAIL =================" << std::endl;
					}

				return r;
				}, std::move(writer)); });
		}
	}

void testsuite_files_t::event_handler(size_t /*indexPeer*/, std::unique_ptr<i_event_t> && /*ev*/) {
	if (!ready()) {
		/*if (auto streamEvent = dynamic_cast<crm::i_input_stream_base_event_t*>(ev.get())) {
			if (auto blobStream = streamEvent->handler) {
				auto coverData = dynamic_cast<const my_cover_data*>(blobStream->stream_cover_data().get());
				auto peerId_ = coverData->peer_id;
				auto objectId = coverData->object_id;
				auto testId_ = coverData->test_id;
				auto streamGID = coverData->stream_gid;

				if (test_id() == testId_) {

					if (peerId_ != indexPeer) {
						FATAL_ERROR_FWD(nullptr);
						}

					if (auto streamBegin = dynamic_cast<crm::i_input_stream_begin_t*>(ev.get())) {
						if (auto fw = dynamic_cast<file_wstream_t*>(blobStream->writer().get())) {
							fw->set_keep_location();
							}
						}
					else if (auto streamEnd = dynamic_cast<crm::i_input_stream_eof_t*>(ev.get())) {
						if (blobStream->eof()) {
							in_eof(peerId_, objectId, std::move(*blobStream).writer());
							}
						}
					}
				}
			}*/
		}
	}


