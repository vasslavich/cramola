#pragma once


#include <atomic>
#include <filesystem>
#include <future>
#include "./classes.h"


namespace crm{
namespace testmark {


struct i_send_buffer_t {
	virtual ~i_send_buffer_t() {}

	static const size_t aggreementStep;
	static std::atomic<size_t> aggreement;
	static const size_t aggreementMax;

	std::atomic<int> outReady = 0;
	std::atomic<int> inputReady = 0;
	std::shared_future<bool> result;
	std::atomic<bool> stream_startedf{ false };

	i_send_buffer_t();

	i_send_buffer_t(const i_send_buffer_t &) = delete;
	i_send_buffer_t& operator=(const i_send_buffer_t &) = delete;

	i_send_buffer_t(i_send_buffer_t && o);
	i_send_buffer_t& operator=(i_send_buffer_t && o);

	virtual size_t size()const = 0;
	};


class memory_buffer_t : public i_send_buffer_t {
public:
	std::vector<uint8_t> value;

	memory_buffer_t();

	memory_buffer_t(const memory_buffer_t &) = delete;
	memory_buffer_t& operator=(const memory_buffer_t &) = delete;

	memory_buffer_t(memory_buffer_t && o);
	memory_buffer_t& operator=(memory_buffer_t && o);

	size_t size()const { return value.size(); }
	};


class file_buffer_t : public i_send_buffer_t {
private:
	std::filesystem::path _path;
	size_t _size = 0;

private:
	file_buffer_t();

public:
	~file_buffer_t();
	void close()noexcept;

	static file_buffer_t make_rand(const std::wstring & root, const std::vector<uint8_t> & buf);
	static file_buffer_t make_rand(const std::wstring & root);

	file_buffer_t(const file_buffer_t &) = delete;
	file_buffer_t& operator=(const file_buffer_t &) = delete;

	file_buffer_t(file_buffer_t && o);
	file_buffer_t& operator=(file_buffer_t && o);

	void read(size_t off, size_t count, std::vector<uint8_t> & buf)const;
	size_t size()const noexcept;
	std::wstring path()const noexcept;
	bool cmp_with_file(const std::wstring & file)const noexcept;
	};


struct i_peer_collection {
	virtual ~i_peer_collection() {}

	virtual void close()noexcept = 0;
	virtual void clear()noexcept = 0;
	virtual void resize(size_t size) = 0;
	virtual size_t size()const noexcept = 0;
	virtual const crm::opeer_t& peer(int peerId)const = 0;
	virtual crm::opeer_t& peer( int peerId ) = 0;
	virtual bool is_connected(int pid)const = 0;
	virtual bool set_is_connected(int pid) = 0;
	virtual void set_is_errc(int) = 0;
	virtual int errc(int)const = 0;
	};


struct my_cover_data final : crm::i_stream_zyamba_cover_data_t {
	static const srlz::typemap_key_t stream_blob_typeid;

public:
	int peer_id{ 0 };
	int object_id{ 0 };
	int test_id{ 0 };
	int stream_gid{ 0 };

	address_hash_t object_hash()const noexcept {
		address_hash_t h;
		crm::apply_at_hash(h, peer_id);
		crm::apply_at_hash(h, object_id);
		crm::apply_at_hash(h, test_id);
		crm::apply_at_hash(h, stream_gid);

		return h;
		}

	const srlz::i_typeid_t& stream_typeid()const noexcept {
		return stream_blob_typeid;
		}

	template<typename S>
	void srlz(S&& dest)const {
		crm::srlz::serialize(dest, peer_id);
		crm::srlz::serialize(dest, object_id);
		crm::srlz::serialize(dest, test_id);
		crm::srlz::serialize(dest, stream_gid);
		}

	template<typename S>
	void dsrlz(S&& src) {
		crm::srlz::deserialize(src, peer_id);
		crm::srlz::deserialize(src, object_id);
		crm::srlz::deserialize(src, test_id);
		crm::srlz::deserialize(src, stream_gid);
		}

	size_t get_serialized_size()const noexcept {
		return crm::srlz::object_trait_serialized_size(peer_id) +
			crm::srlz::object_trait_serialized_size(object_id) +
			crm::srlz::object_trait_serialized_size(test_id) +
			crm::srlz::object_trait_serialized_size(stream_gid);
		}
	};


/*! The custom stream, for example, from file */
template<typename TStreamReader>
class my_rstream_contract_wrapper{
private:
	std::shared_ptr<TStreamReader> _r;

public:
	my_rstream_contract_wrapper(std::shared_ptr<TStreamReader> r)
		: _r(r) {
		CBL_VERIFY(_r);
		}

	void read(const stream_sequence_position_t& offset, size_t blockSize, std::vector<uint8_t>& v) {
		_r->read(offset, blockSize, v);
		}

	bool eof(const stream_sequence_position_t& offset)const noexcept {
		return _r->eof(offset);
		}

	auto get_stream_params()const {
		return _r->get_stream_params();
		}

	const srlz::typemap_key_t& stream_typeid()const  noexcept {
		return _r->stream_typeid();
		}
	};


class testsuite_base_t {
public:
	virtual ~testsuite_base_t();

	std::unique_ptr< i_peer_collection> _peers;
	std::condition_variable endSignal;
	mutable std::mutex siglock;

	std::atomic<size_t> _ConnectionsCounter{ 0 };
	std::atomic<size_t> _EntryCounter{ 0 };
	std::atomic<size_t> _IncCounter{ 0 };
	std::atomic<size_t> _DecCounter{ 0 };
	std::atomic<size_t> _SuccessCounter{ 0 };
	std::atomic<size_t> _ErrorCounter{ 0 };
	std::atomic<size_t> _2DSizeCounter{ 0 };


private:
	static std::atomic<int> testIdCounter;

	std::vector<std::vector<std::shared_ptr<i_send_buffer_t>>> _sources;
	std::vector<std::vector<std::shared_ptr<crm::i_stream_reader_t>>> _sreaders;
	std::list<std::shared_ptr<crm::i_stream_whandler_t>> _swh;
	size_t _size2D = 0;
	int testId = -1;
	std::atomic<bool> _readyf{ false };

	void _clear() {
		if(_peers)
			_peers->clear();

		_sources.clear();
		_sreaders.clear();
		_swh.clear();
		}

public:
	std::string inform_conters()const {
		std::ostringstream strace;

		strace << std::setw(10) << "inc=" << std::setw(5) << std::to_string(_IncCounter.load()) << std::endl;
		strace << std::setw(10) << "dec=" << std::setw(5) << std::to_string(_DecCounter.load()) << std::endl;
		strace << std::setw(10) << "ok=" << std::setw(5) << std::to_string(_SuccessCounter.load()) << std::endl;
		strace << std::setw(10) << "err=" << std::setw(5) << std::to_string(_ErrorCounter.load()) << std::endl;
		strace << std::setw(10) << "entry=" << std::setw(5) << std::to_string(_EntryCounter.load()) << std::endl;
		strace << std::setw(10) << "con:" << std::setw(5) << std::to_string(_ConnectionsCounter.load()) << std::endl;
		strace << std::setw(10) << "2d=" << std::setw(5) << std::to_string(_2DSizeCounter.load()) << std::endl;
		strace << std::setw(10) << "peers=" << std::setw(5) << std::to_string(peers_count()) << std::endl;

		size_t connected{ 0 };
		size_t ierr{ 0 };
		for(size_t ipr = 0; ipr < peers_count(); ++ipr) {
			if(is_connected(ipr)) {
				++connected;
				}
			else {
				ierr += (errc(ipr) ? 1 : 0);
				}
			}

		strace << "connected peers=" << std::setw(5) << connected << std::endl;
		strace << "connect errc peers=" << std::setw(5) << ierr << std::endl;

		return strace.str();
		}

	std::string inform()const {
		std::ostringstream os;

		for(size_t ipr = 0; ipr < peers_count(); ++ipr) {
			os << "peer " << std::setw(4)
				<< ", cn establised? " << (is_connected(ipr) ? "+" : "-")
				<< ", opened:" << (peer(ipr).is_opened() ? "+" : "-")
				<< ", state:" << crm::cvt2string(peer(ipr).state())
				<< std::endl;
			}

		os << std::endl;
		for(size_t ipr = 0; ipr < peers_count(); ++ipr) {
			for(size_t iob = 0; iob < objects_count(ipr); ++iob) {

				os << "p:" << std::setw(4) << ipr << " o:" << std::setw(4) << iob;
				auto i2 = at(ipr, iob);

				if(i2->inputReady >= 1)
					os << " i:+";
				else {
					os << " i:-";
					}

				if(i2->outReady >= 1)
					os << " o:+";
				else {
					os << " o:-";
					}

				os << std::endl;
				}
			}

		return os.str();
		}

protected:
	virtual void in_eof(int peerId, int objectId, std::unique_ptr<crm::i_stream_writer_t> && sw) = 0;

public:
	virtual void event_handler(size_t indexPeer, std::unique_ptr<i_event_t> && ev) = 0;

private:
	void set(std::vector<std::vector<std::shared_ptr<i_send_buffer_t>>> && grid,
		std::vector<std::vector<std::shared_ptr<crm::i_stream_reader_t>>> && readers) {

		_sources = std::move(grid);
		_sreaders = std::move(readers);

		if(_peers)
			_peers->resize(_sreaders.size());

		_size2D = _sreaders.size() * _sreaders.at(0).size();
		}

public:
	void add(std::shared_ptr<crm::i_stream_whandler_t> && whr) {
		std::lock_guard<std::mutex> lck(siglock);
		_swh.push_back(std::move(whr));
		}

protected:
	void make_place(const std::shared_ptr<crm::distributed_ctx_t> & /*ctx*/,
		size_t peerCount,
		size_t objectsCount,

		std::function<std::tuple<std::shared_ptr<i_send_buffer_t>, std::shared_ptr<crm::i_stream_reader_t>>(size_t pid, size_t oid)> && readersPairMaker) {

		std::vector<std::vector<std::shared_ptr<i_send_buffer_t>>> grid;
		std::vector<std::vector<std::shared_ptr<crm::i_stream_reader_t>>> readers;

		grid.resize(peerCount);
		readers.resize(peerCount);

		auto initializeItem = [&](size_t ipr) {
			grid[ipr].resize(objectsCount);
			readers[ipr].resize(objectsCount);

			for(size_t is = 0; is < objectsCount; ++is) {
				auto[ibuf, ireader] = readersPairMaker(ipr, is);

				grid[ipr][is] = std::move(ibuf);
				readers[ipr][is] = std::move(ireader);
				}
			};

		std::vector<std::future<decltype(initializeItem(0))>> initializeItemRs(peerCount);
		for(size_t ipr = 0; ipr < peerCount; ++ipr) {
			initializeItemRs[ipr] = std::async(initializeItem, ipr);
			}

		for(size_t ipr = 0; ipr < peerCount; ++ipr) {
			initializeItemRs[ipr].wait();
			}

		set(std::move(grid), std::move(readers));
		}

public:
	virtual void make_place(const std::shared_ptr<crm::distributed_ctx_t> & ctx,
		size_t peerCount,
		size_t objectsCount) = 0;

	auto validate_count()const {
		size_t iCnt = 0;
		size_t oRcnt = 0;
		for(size_t ipr = 0; ipr < peers_count(); ++ipr) {
			for(size_t iob = 0; iob < objects_count(ipr); ++iob) {
				auto i2 = at(ipr, iob);

				if(i2->inputReady >= 1)
					iCnt++;
				if(i2->outReady >= 1)
					oRcnt++;
				}
			}

		return std::make_tuple(iCnt, oRcnt, _size2D);
		}

	auto check_ready() {
		auto vc = validate_count();
		if(validate_count(vc)) {
			endSignal.notify_all();
			_readyf.store(true);
			}

		return vc;
		}

	static bool validate_count(const std::tuple<size_t, size_t, size_t> & vc);
	bool validate_count_2()const;

protected:
	testsuite_base_t();

public:
	void set_peers(std::unique_ptr<i_peer_collection> && ips);
	const crm::opeer_t& peer(int id)const;
	crm::opeer_t& peer( int id );
	bool is_connected(int pid)const;
	int errc(int pid)const;
	bool set_is_connected(int pid);
	void set_is_error(int pid);
	size_t size_2d()const;
	void close_peers();

	int test_id() const;

	void clear();

	void clear_with_lock();

	size_t peers_count()const;


	size_t objects_count(size_t peerId)const;

	std::shared_future<bool> resultf(size_t peerId, size_t objectIndex)const;

	size_t object_size(size_t peerId, size_t objectIndex)const;

	std::shared_ptr<i_send_buffer_t> at(size_t peerId, size_t objectId)const;

	template<typename TOp>
	void set_result_handler(int peer, int objid, TOp && op) {
		//std::lock_guard<std::mutex> lck(siglock);

		if(!_sources.at(peer).at(objid)->result.valid()) {
			_sources.at(peer).at(objid)->result = op();
			}
		}

	std::shared_ptr<i_send_buffer_t> at(size_t peerId, size_t objectId);

	std::shared_ptr<crm::i_stream_reader_t> get_reader(size_t peerId, size_t objectId)const;
	std::shared_ptr<crm::i_stream_reader_t> get_reader(size_t peerId, size_t objectId);

	using ostream_end_f = std::function<void(crm::async_operation_result_t ,
		std::unique_ptr<crm::i_stream_end_t> && ,
		std::unique_ptr<crm::i_stream_rhandler_t> && ,
		std::unique_ptr<crm::dcf_exception_t> && )>;

	virtual void start_ostream(my_cover_data&& keys,
		int peerId,
		int objectId,
		size_t idStream,
		crm::opeer_t& xpeer_,
		ostream_end_f&& endh) = 0;

	bool ready()const noexcept;

	bool is_stream_started(int peerId, int objectId)const;
	void set_stream_started(int peerId, int objectId);
	};


struct stream_characters_t {
	size_t count = 0;
	size_t resultOk = 0;
	size_t resultFail = 0;
	};


struct testmark_accumulator_t {
	std::map<size_t, stream_characters_t> streams_stat;
	};


struct resolve_result_t {
	bool ready = false;
	std::list<std::tuple<bool, size_t>> vlst;
	};


class rt1_custom_outxproperties_t : public crm::i_peer_remote_properties_t {
public:
	int testId = -1;
	size_t indexPeer = (size_t)(-1);

	std::vector<uint8_t> serialize()const;
	void deserialize(const std::vector<uint8_t> & buf);
	};


class rt1_custom_outconnection_t : public crm::rt1_connection_settings_t {
public:
	size_t indexPeer = (size_t)(-1);
	std::weak_ptr<testsuite_base_t> testsuite;

	rt1_custom_outconnection_t(const crm::identity_descriptor_t & thisId,
		size_t indexPeer_,
		std::unique_ptr<crm::rt1_endpoint_t> && ep,
		std::function<void(crm::input_message_knop_l1&& m)> && mh,
		std::weak_ptr<testsuite_base_t> testsuite);

	void event_handler(std::unique_ptr<i_event_t> && ev);
	crm::remote_object_connection_value_t local_object_identity();
	};


struct opeer_item_t {
	std::atomic<int> errc{ 0 };
	std::atomic<bool> connected{ false };
	crm::opeer_t peer;
	std::atomic<bool> doClose = false;

	opeer_item_t();

	opeer_item_t(const opeer_item_t &) = delete;
	opeer_item_t& operator=(const opeer_item_t &) = delete;

	opeer_item_t(opeer_item_t && o);
	opeer_item_t& operator=(opeer_item_t && o);
	};


class out_peers_collection final : public i_peer_collection {
private:
	std::vector<opeer_item_t> _peers;

public:
	~out_peers_collection() {
		close();
		}

	void resize(size_t size)final {
		_peers.resize(size);
		}

	void clear()noexcept final {
		_peers.clear();
		}

	void close()noexcept final {
		for(auto & it : _peers) {
			it.peer.close();
			}
		_peers.clear();
		}

	out_peers_collection(std::vector<opeer_item_t> && pl) {
		_peers = std::move(pl);
		}

	size_t size()const noexcept {
		return _peers.size();
		}

	const crm::opeer_t& peer(int peerId)const {
		return _peers.at(peerId).peer;
		}

	crm::opeer_t& peer( int peerId ){
		return _peers.at( peerId ).peer;
		}

	bool is_connected(int pid)const {
		return _peers.at(pid).connected.load();
		}

	int errc(int pid)const {
		return _peers.at(pid).errc.load();
		}

	bool set_is_connected(int pid) {
		return _peers.at(pid).connected.exchange(true);
		}

	void set_is_errc(int pid) {
		_peers.at(pid).errc.fetch_add(1);
		}
	};

class testsuite_memory_buffer_t : public testsuite_base_t {
public:
	using rstream_type = decltype(crm::make_rstream_container_adapter(std::declval<decltype(memory_buffer_t::value)>()));

	void in_eof(int peerId, int objectId, std::unique_ptr<crm::i_stream_writer_t> && sw)final;
	void make_place(const std::shared_ptr<crm::distributed_ctx_t> & ctx, size_t peersCount, size_t objectsCount)final;
	void event_handler(size_t indexPeer, std::unique_ptr<i_event_t> && ev);
	void start_ostream(my_cover_data&& keys,
		int peerId,
		int objectId,
		size_t idStream,
		crm::opeer_t& xpeer_,
		ostream_end_f&& endh);
	};


class testsuite_files_t : public testsuite_base_t {
public:
	using rstream_type = decltype(crm::make_rstream_file_adapter(std::declval<std::weak_ptr<crm::distributed_ctx_t>>(), std::declval<std::string>()));

	void in_eof(int peerId, int objectId, std::unique_ptr<crm::i_stream_writer_t> && sw)final;
	void make_place(const std::shared_ptr<crm::distributed_ctx_t> & ctx, size_t peersCount, size_t objectsCount)final;
	void event_handler(size_t indexPeer, std::unique_ptr<i_event_t> && ev);
	void start_ostream(my_cover_data&& keys,
		int peerId,
		int objectId,
		size_t idStream,
		crm::opeer_t& xpeer_,
		ostream_end_f&& endh);
	};

struct my_custom_stream_descriptor {
	int peerId;
	int objectId;
	int testId;
	int streamGID;
	};
}
}

