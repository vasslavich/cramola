#pragma once


#include <appkit/appkit.h>
#include <hublon/r0.h>
#include <hublon/r1.h>


//#define CBL_MPF_TEST_PRINT_EXCEPTION_STREAM


namespace my_mpf {


/*class rt0_hub_usecase_t : public crm::rt0_hub_settings_t {
public:
	typedef crm::rt0_hub_settings_t base_t;

	rt0_hub_usecase_t( const std::shared_ptr<crm::distributed_ctx_t> & ctx,
		std::unique_ptr<crm::i_endpoint_t> && ep,
		const crm::identity_descriptor_t & thisId,
		std::function<void(crm::message_stamp_t&&)> && mh );
};

class rt1_hub_usecase_t : public crm::rt1_hub_settings_t {
public:
	typedef crm::rt1_hub_settings_t base_t;

	rt1_hub_usecase_t( const std::shared_ptr<crm::distributed_ctx_t> & ctx,
		const crm::identity_descriptor_t & thisId,
		crm::router_0_t & rt0,
		std::unique_ptr<crm::i_input_messages_stock_t> && inputStackCtr );
};

class rt1_outconnection_usecase_t : public crm::rt1_connection_settings_t {
public:
	typedef crm::rt1_connection_settings_t base_t;

	rt1_outconnection_usecase_t( const crm::identity_descriptor_t & thisId,
		std::unique_ptr<crm::rt1_endpoint_t> && ep,
		std::unique_ptr<crm::i_input_messages_stock_t> && inputStackCtr,
		bool keepAlive = false );
	};

class rt0_outconnection_usecase_t : public crm::rt0_connection_settings_t {
public:
	typedef crm::rt0_connection_settings_t base_t;

	rt0_outconnection_usecase_t(const crm::identity_descriptor_t& thisId,
		std::unique_ptr<crm::rt1_endpoint_t>&& ep,
		std::unique_ptr<crm::i_input_messages_stock_t> && inputStackCtr,
		bool keepAlive = false);
	};
*/
/*std::shared_ptr<crm::distributed_ctx_t> initialize_ctx( const std::wstring & cfg, 
	const crm::identity_descriptor_t & objectId,
	crm::distributed_ctx_t::whandler_push_f && streamHPush);*/
//
//std::shared_ptr<crm::distributed_ctx_t> initialize_ctx(crm::distributed_ctx_policies_t&& options,
//	const crm::identity_descriptor_t& thisId_);

std::shared_ptr<crm::default_binary_protocol_handler_t> initialize_protocol( std::shared_ptr<crm::distributed_ctx_t> & ctx );

template<typename TInputHandler>
std::shared_ptr<crm::router_0_t> initialize_rt0( int port,
	const crm::identity_descriptor_t & thisId,
	std::shared_ptr<crm::distributed_ctx_t> & ctx,
	TInputHandler && imq ) {

	/* �������� ����� ��� �������� ����������� */
	auto hubSettings = crm::rt0_hub_settings_t{ ctx,
		thisId,
		crm::detail::tcp::tcp_endpoint_t{ port, crm::detail::tcp::tcp_endpoint_t::version_t::v4 },
		std::forward<TInputHandler>(imq) };

	return crm::router_0_t::create(std::move(hubSettings));
	}

/*std::shared_ptr<crm::router_1> initialize_rt1( std::shared_ptr<crm::router_0_t> & rt0,
	const crm::global_object_identity_t & thisId,
	std::shared_ptr<crm::distributed_ctx_t> & ctx,
	std::unique_ptr<crm::i_input_messages_stock_t> && imq );*/

/*std::shared_ptr<crm::router_1> initialize_rt1(std::shared_ptr<crm::detail::io_x_rt1_channel_t> channel,
	std::shared_ptr<crm::rt1_hub_settings_t> s );*/

//crm::opeer_t create_connection( crm::router_1  & tbl1,
//	std::shared_ptr<crm::rt1_connection_settings_t> && s);
//
//crm::opeer_l0 create_connection(crm::router_0_t& tbl1,
//	std::shared_ptr<crm::rt0_connection_settings_t>&& s);
}
