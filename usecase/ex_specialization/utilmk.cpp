#include <random>
#include <appkit/appkit.h>
#include "./utilmk.h"


using namespace crm;
using namespace crm::testmark;


void crm::testmark::fill( std::vector<uint8_t> & data, size_t count ) {
	data.resize( count );

	size_t ib = 0;
	while( ib < count ) {
		auto u16 = crm::sx_uuid_t::rand();
		for( size_t i = 0; i < crm::sx_uuid_t::plane_size && ib < count; ++i, ++ib ) {

			data[ib] = u16.u8_16[i];
			}
		}


	/*	std::random_device rd;
	std::uniform_int_distribution<size_t> dist(0, count * 10);

	for(auto & b : data) {
		auto i = dist(rd);
		b = (i % (std::numeric_limits<uint8_t>::max)());
		}*/
	}

bool crm::testmark::cmpv( const std::vector<uint8_t> & v1, const std::vector<uint8_t> & v2 ) {
	if( v1.size() != v2.size() )
		return false;

	for( size_t i = 0; i < v1.size(); ++i ) {
		if( v1[i] != v2[i] ) {
			return false;
			}
		}

	return true;
	}

