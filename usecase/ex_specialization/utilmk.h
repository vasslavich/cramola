#pragma once


#include <vector>
#include <cstdint>


namespace crm{
namespace testmark{


void fill( std::vector<uint8_t> & data, size_t count );
bool cmpv( const std::vector<uint8_t> & v1, const std::vector<uint8_t> & v2 );
}
}
