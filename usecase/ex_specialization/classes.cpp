#include "./classes.h"

using namespace my_mpf;
using namespace crm;
using namespace crm::mpf;


/*rt0_hub_usecase_t::rt0_hub_usecase_t(const std::shared_ptr<crm::distributed_ctx_t>& ctx,
	std::unique_ptr<crm::i_endpoint_t>&& ep,
	const crm::identity_descriptor_t& thisId,
	std::function<void(crm::message_stamp_t&&)>&& mh)
	: base_t( ctx, std::move( ep ), thisId, std::move(mh) ){}


rt1_hub_usecase_t::rt1_hub_usecase_t( const std::shared_ptr<distributed_ctx_t> & ctx,
	const identity_descriptor_t & thisId,
	crm::router_0_t & rt0,
	std::unique_ptr<i_input_messages_stock_t> && inputStack )
	: base_t( ctx, thisId, rt0, std::move(inputStack) ){}


rt1_outconnection_usecase_t::rt1_outconnection_usecase_t( const crm::identity_descriptor_t & thisId,
	std::unique_ptr<crm::rt1_endpoint_t> && ep,
	std::unique_ptr<crm::i_input_messages_stock_t> && inputStackCtr,
	bool keepAlive )
	: base_t( thisId, std::move( ep ), std::move( inputStackCtr ), keepAlive ) {}


rt0_outconnection_usecase_t::rt0_outconnection_usecase_t(const crm::identity_descriptor_t& thisId,
	std::unique_ptr<crm::rt1_endpoint_t>&& ep,
	std::unique_ptr<i_input_messages_stock_t> && inputStackCtr,
	bool keepAlive )
	: base_t(thisId, std::move(ep), std::move(inputStackCtr), keepAlive) {}
*/
/*std::shared_ptr<crm::distributed_ctx_t> my_mpf::initialize_ctx( const std::wstring & xcfg,
	const crm::identity_descriptor_t & objectId,
	crm::distributed_ctx_t::whandler_push_f && streamHPush){

	crm::xml::xml_document_t doc;
	doc.file_load( crm::cvt( xcfg ) );

	auto ntfHndl = []( std::unique_ptr<crm::dcf_exception_t> && ntf )noexcept{
		std::wcout << ntf->msg() << std::endl;
		};
	std::unique_ptr < crm::distributed_ctx_policies_t> distrPl( new crm::distributed_ctx_policies_t(
		doc, crm::cvt( crm::distributed_ctx_policies_t::cfg_mpf_options_root_node() ) ) );

	auto mpfCtx( crm::distributed_ctx_t::make(
		objectId,
		crm::channel_identity_t::make(),
		std::move( distrPl ),
		[]( std::unique_ptr<crm::dcf_exception_t> && , const crm::global_object_identity_t  ){},
		std::move( streamHPush )));

	return std::move( mpfCtx );
	}*/

std::shared_ptr<crm::default_binary_protocol_handler_t> my_mpf::initialize_protocol( std::shared_ptr<crm::distributed_ctx_t> & ctx ) {
	return std::make_shared < crm::default_binary_protocol_handler_t>( ctx );
}
//
//std::shared_ptr<crm::router_0_t> my_mpf::initialize_rt0(int port,
//	const crm::identity_descriptor_t & thisId,
//	std::shared_ptr<crm::distributed_ctx_t> & ctx,
//	std::function<void(crm::input_message_knop_l0&&)> && imq ) 

/*std::shared_ptr<crm::router_1> my_mpf::initialize_rt1( std::shared_ptr<crm::router_0_t> & rt0,
	const crm::global_object_identity_t & thisId,
	std::shared_ptr<crm::distributed_ctx_t> & ctx,
	std::unique_ptr<crm::i_input_messages_stock_t> && imq ) {

	return initialize_rt1( std::make_unique<rt1_hub_usecase_t>( ctx,
		thisId,
		(*CHECK_PTR( rt0 )),
		std::move( imq ) ) );
	}*/

//
//std::shared_ptr<crm::router_1> my_mpf::initialize_rt1(std::shared_ptr<crm::detail::io_x_rt1_channel_t> channel, 
//	std::shared_ptr<crm::rt1_hub_settings_t> s ) {
//
//	return crm::router_1::create( channel, s );
//	}


//crm::opeer_t my_mpf::create_connection( crm::router_1  & tbl1,
//	std::shared_ptr<rt1_connection_settings_t> && s ){
//
//	return tbl1.create_out_link( std::move( s ) );
//}
//
//
//crm::opeer_l0 my_mpf::create_connection(crm::router_0_t& tbl0,
//	std::shared_ptr<rt0_connection_settings_t>&& s) {
//
//	return tbl0.create_out_link(std::move(s));
//	}
