#include <vector>
#include <string>
#include <chrono>
#include <array>
#include <appkit/appkit.h>
#include <hublon/r0.h>
#include <experimental/resumable>
#include "../../../ex_specialization/classes.h"


using namespace std::chrono;


const auto localhostId_RT0 = crm::identity_descriptor_t{ crm::global_object_identity_t::as_hash("there are rpc benchmark for coroutines #1 for l0"), "host:0" };
const auto localhostAddress = "127.0.0.1";
short port = 16789;

std::shared_ptr<crm::router_0_t> tbl0;

static std::atomic<size_t> a0{ 0 };

static constexpr auto freqExcCollectPrint = 10;

bool watchLatencyMin{ true };
bool watchLatencyMax{ true };
size_t concurrencySize = 2;
size_t messagingConcurrency = 1;

#ifdef CBL_MPF_TEST_ERROR_EMULATION
constexpr size_t stat_step = 1000;
constexpr size_t print_step = 2000;
#else
constexpr size_t stat_step = 5000;
constexpr size_t print_step = 10000;
#endif


struct stat_entry {
	size_t count{ 0 };
	double mps{ 0 };
	std::uintmax_t latencySysMax{ 0 };
	std::uintmax_t latencySysMin{ 0 };
	std::atomic<std::uintmax_t> latencySysAverCounter{ 0 };
	std::uintmax_t latencyThisMax{ 0 };
	std::uintmax_t latencyThisMin{ 0 };
	std::atomic<std::uintmax_t> latencyThisAverCounter{ 0 };

	using timeline_duration_t = std::chrono::seconds;
	using timeline_line_t = decltype(std::declval<timeline_duration_t>().count());
	std::chrono::high_resolution_clock::time_point _startTP;

	stat_entry()
		: _startTP{ std::chrono::high_resolution_clock::now() } {}

	void __add() {
		const auto c_ = ++count;
		if (c_ >= stat_step && (c_ % stat_step) == 0) {
			auto tl = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::high_resolution_clock::now() - _startTP).count();

			if (tl != 0) {
				mps = (double)c_ / (double)tl;
				}
			}
		}

	void __add(const std::chrono::high_resolution_clock::time_point& startTP,
		const std::chrono::microseconds& sysLatency) {

		auto thisLatency = (std::uintmax_t)std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - startTP).count();
		auto sysLatencyCount = (std::uintmax_t)sysLatency.count();

		if (watchLatencyMin) {
			if (!latencyThisMin) {
				latencyThisMin = thisLatency;
				}
			else if (thisLatency < latencyThisMin) {
				latencyThisMin = thisLatency;
				}

			if (!latencySysMin) {
				latencySysMin = sysLatencyCount;
				}
			else if (sysLatencyCount < latencySysMin) {
				latencySysMin = sysLatencyCount;
				}
			}

		if (watchLatencyMax) {
			if (thisLatency > latencyThisMax) {
				latencyThisMax = thisLatency;
				}

			if (sysLatencyCount > latencySysMax) {
				latencySysMax = sysLatencyCount;
				}
			}

		latencyThisAverCounter += thisLatency;
		latencySysAverCounter += sysLatencyCount;
		}

	void add() {
		__add();
		}

	void add(const std::chrono::high_resolution_clock::time_point& startTP,
		const std::chrono::microseconds& sysLatency) {

		__add();
		__add(startTP, sysLatency);

		add_post();
		}

	void add_post() {
		if ((count % print_step) == 1) {
			std::cout << "stat: >>" << print() << std::endl;
			}
		}

	std::string print()const {
		std::ostringstream ol;
		ol <<
			"count=" << std::setw(6) << count <<
			", mps=" << std::setw(6) << (size_t)std::trunc(mps) <<
			", latency median(sys/wrapwatch, microsec)=" <<
			std::setw(8) << ((double)latencySysAverCounter / (double)count) << '/' <<
			std::setw(8) << ((double)latencyThisAverCounter / (double)count) <<
			": min,max(wrapwatch)=" <<
			std::setw(8) << latencyThisMin << '/' << std::setw(8) << latencyThisMax;

		return ol.str();
		}
	}stgl;


[[noreturn]]
void FailExit(const crm::dcf_exception_t& e) {
	std::wcout << L"FAIL(1) >> " << e.msg() << std::endl;
	std::exit(-1);
	}

[[noreturn]]
void FailExit(const std::exception& e) {
	std::cout << "FAIL(2) >> " << e.what() << std::endl;
	std::exit(-1);
	}


class outbound_handler_t : public crm::rt0_connection_settings_t {
	static std::atomic<int> _startf;
	std::weak_ptr<crm::distributed_ctx_t> _ctx;

public:
	template<typename EP, typename H>
	outbound_handler_t(std::weak_ptr<crm::distributed_ctx_t> ctx,
		const crm::identity_descriptor_t& thisId,
		EP && ep,
		H && h)
		: crm::rt0_connection_settings_t(thisId, std::forward<EP>(ep), std::forward<H>(h))
		, _ctx(ctx) {}

	template<typename Tio>
	static std::future<void> invoke_resumable(Tio&& io) {
		do {
			auto timepoint = std::chrono::high_resolution_clock::now();
			auto a0_ = a0.fetch_add(1);

			auto st = co_await crm::rpc_coro_invoke(io,
				"test_rpc_1",
				crm::function_descriptor{ "test_rpc_1" },
				a0_);

			auto [l] = crm::rpc_get_result( std::move(st)).as<size_t>();
			if (l == a0_) {
				stgl.add(timepoint, st.timeline());
				}
			else {
				throw std::exception{};
				}
			}
		while (io.is_opened());
		}

	void connection_handler(std::unique_ptr<crm::dcf_exception_t>&&, xpeer_link&& lnk_)final {
		auto roxAddress = lnk_.address();
		auto roxPort = lnk_.port();
		auto roxId = lnk_.id();
		auto roxName = lnk_.remote_id().name();

		std::cout << "outbound connection:"
			<< roxAddress << ":"
			<< roxPort << ":"
			<< roxId << ":"
			<< roxName << std::endl;

		for (size_t i = 0; i < concurrencySize; ++i) {
			std::thread([peer_ = lnk_, thidx = i]()mutable {
				static_assert(crm::detail::is_xpeer_trait_z_v<decltype(peer_)>, __FILE_LINE__);

				auto xp = std::move(peer_);

				try {
					auto wf = invoke_resumable(xp);
					auto lastTimePoint = std::chrono::system_clock::now();
					while (!xp.closed()) {
						std::this_thread::sleep_for(10s);
						std::cout << "thread " << thidx 
							<< " execute..." 
							<< std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now() - lastTimePoint).count() << std::endl;
						}
					}
				catch (const std::exception & e1) {
					if (!xp.closed()) {
						FailExit(e1);
						}
					}
				catch (...) {
					if (!xp.closed()) {
						FailExit(CREATE_EXC_FWD(nullptr));
						}
					}

				std::cout << "thread " << thidx << " exited" << std::endl;
				}).detach();
			}
		}
	};

std::atomic<int> outbound_handler_t::_startf{ 0 };

class all2all_custom_hub_t : public crm::rt0_hub_settings_t {
public:
	template<typename EP, typename H>
	all2all_custom_hub_t(const std::shared_ptr<crm::distributed_ctx_t>& ctx,
		EP && ep,
		const crm::identity_descriptor_t& thisId,
		H && h)
		: crm::rt0_hub_settings_t(ctx, thisId, std::forward<EP>(ep), std::forward<H>(h))
		, _optb(std::make_shared< otable>()) {}

private:
	class otable {
	public:
		std::map<crm::ipeer_l0::remote_object_endpoint_t, crm::opeer_l0> _opx;
		std::mutex _opxLock;
		};
	std::shared_ptr< otable> _optb;
	std::weak_ptr<crm::distributed_ctx_t> _ctx;

public:
	std::function<void()> make_outbound() {
		return[otbl_ = std::weak_ptr(_optb), c = ctx()]{
			if(auto otbl = otbl_.lock()) {

			auto rdp = crm::rt1_endpoint_t::make_remote(localhostAddress, port, localhostId_RT0);
			auto outboundH = outbound_handler_t(
				c,
				localhostId_RT0,
				rdp,
				[](crm::input_message_knop_l0&&) {});

			auto xpeer1 = tbl0->create_out_link(std::move(outboundH));

			otbl->_opx.insert({ rdp, std::move(xpeer1) });
			}
			};
		}
	};

std::shared_ptr<crm::distributed_ctx_t> make_context( const crm::identity_descriptor_t& hostId,
	bool ping) {

	crm::distributed_ctx_policies_t policies;
	policies.set_threadpool_maxsize(concurrencySize);
	policies.set_threadpool_user_handlers_maxsize(concurrencySize);
	policies.set_io_buffer_maxsize(CRM_MB);
	policies.set_io_threads_count(2);

	policies.set_ping_timeline(crm::sndrcv_timeline_periodicity_t::make_cyclic_timeouted(60s, 5s));
	policies.set_rt_ping(ping);

	return crm::distributed_ctx_t::make(crm::context_bindings(hostId), std::move(policies));
	}


int main() {
	auto ctx0 = make_context(localhostId_RT0, true);

	auto rt0Settings = all2all_custom_hub_t(ctx0,
		crm::detail::tcp::tcp_endpoint_t(port, crm::detail::tcp::tcp_endpoint_t::version_t::v4),
		localhostId_RT0,
		[](crm::input_message_knop_l0&&) {});
	auto outconnf = rt0Settings.make_outbound();

	tbl0 = crm::router_0_t::create(std::move(rt0Settings));

	ctx0->register_handler({ "test_rpc_1" }, [](size_t l) {
		return l;
		});

	ctx0->start();
	outconnf();


	while (true) {
		std::this_thread::sleep_for(10s);

		std::cout << "in processing " << std::endl;
		}

	return 0;
	}


