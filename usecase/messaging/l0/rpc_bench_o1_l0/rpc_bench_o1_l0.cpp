#include <vector>
#include <string>
#include <chrono>
#include <array>
#include <appkit/appkit.h>
#include <hublon/r0.h>
#include "../../../ex_specialization/classes.h"


using namespace std::chrono;


crm::identity_descriptor_t localhostId_RT0 = crm::identity_descriptor_t{ crm::global_object_identity_t::from_str("FAFA0000-0A0A-F1F1-AAAA-100000000001"), "host:0" };
std::string localhostAddress = "127.0.0.1";
short port = 16789;

std::shared_ptr<crm::router_0_t> tbl0;

static std::atomic<std::intmax_t> steadyOperationsCounter{ 0 };
static std::atomic<std::intmax_t> wrongResultCounter{ 0 };
static std::atomic<std::intmax_t> excResultCounter{ 0 };
static std::atomic<std::intmax_t> undefResultCounter{ 0 };
static std::atomic<size_t> a0{ 0 };

static constexpr auto freqExcCollectPrint = 10;

bool watchLatencyMin{ false };
bool watchLatencyMax{ false };

#ifdef CBL_MPF_TEST_ERROR_EMULATION
constexpr size_t stat_step = 10;
constexpr size_t print_step = 20;
#else
constexpr size_t stat_step = 1000;
constexpr size_t print_step = 2000;
#endif


struct stat_entry {
	size_t count{ 0 };
	double mps{ 0 };
	std::chrono::microseconds latencySysMax{ 0 };
	std::chrono::microseconds latencySysMin{ 0 };
	std::chrono::microseconds latencySysAver{ 0 };
	std::chrono::microseconds latencyThisMax{ 0 };
	std::chrono::microseconds latencyThisMin{ 0 };
	std::chrono::microseconds latencyThisAver{ 0 };

	using timeline_duration_t = std::chrono::seconds;
	using timeline_line_t = decltype(std::declval<timeline_duration_t>().count());
	std::chrono::high_resolution_clock::time_point startTP;

	stat_entry()
		: startTP{ std::chrono::high_resolution_clock::now() } {}

	void __add() {
		const auto c_ = ++count;
		if (c_ >= stat_step && (c_ % stat_step) == 0) {
			auto currTL = std::chrono::high_resolution_clock::now();
			auto tl = std::chrono::duration_cast<std::chrono::seconds>( currTL - startTP).count();

			if (tl != 0) {
				mps = (double)c_ / (double)tl;
				}
			}
		}

	void __add(const std::chrono::high_resolution_clock::time_point& startTP,
		const std::chrono::microseconds& sysLatency) {

		auto thisLatency = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - startTP);

		if(watchLatencyMin) {
			if(!latencyThisMin.count()) {
				latencyThisMin = thisLatency;
				}
			else if(thisLatency < latencyThisMin) {
				latencyThisMin = thisLatency;
				}

			if(!latencySysMin.count()) {
				latencySysMin = sysLatency;
				}
			else if(sysLatency < latencySysMin) {
				latencySysMin = sysLatency;
				}
			}

		if(watchLatencyMax) {
			if(thisLatency > latencyThisMax) {
				latencyThisMax = thisLatency;
				}

			if(sysLatency > latencySysMax) {
				latencySysMax = sysLatency;
				}
			}

		if (latencyThisAver.count()) {
			latencyThisAver = (latencyThisAver + thisLatency) / 2;
			}
		else {
			latencyThisAver = thisLatency;
			}

		if (latencySysAver.count()) {
			latencySysAver = (latencySysAver + sysLatency) / 2;
			}
		else {
			latencySysAver = sysLatency;
			}
		}

	void add() {
		__add();
		}

	void add(const std::chrono::high_resolution_clock::time_point& startTP,
		const std::chrono::microseconds& sysLatency) {

		__add();
		__add(startTP, sysLatency);

		add_post();
		}

	void add_post() {
		if ((count % print_step) == 1) {
			std::cout << "stat: >>" << print() << std::endl;
			}
		}

	std::string print()const {
		std::ostringstream ol;
		ol <<
			"count=" << std::setw(6) << count <<
			", mps=" << std::setw(6) << (size_t)std::trunc(mps) <<
			", latency(sys/there, microsec)=" << std::setw(8) << latencySysAver.count() << ":" << std::setw(8) << latencyThisAver.count();

		return ol.str();
		}
	}stgl;


template<typename TPeer>
void send(TPeer&& p) {
	static_assert(crm::detail::is_xpeer_trait_z_v<TPeer>, __FILE_LINE__);

	if (p.is_opened()) {
		auto next = crm::make_scoped_execution([p_ = p]()mutable {send(std::move(p_)); });

		++steadyOperationsCounter;

		auto a0_ = a0.fetch_add(1);

		crm::rpc_ainvoke(std::move(p),
			crm::launch_as::now,
			std::string("test_rpc_1:"),
			crm::function_descriptor{ "test_rpc_1" },
			[a0_, n = std::move(next), timepoint = std::chrono::high_resolution_clock::now()](crm::message_handler&& rd) {

			--steadyOperationsCounter;
			stgl.add(timepoint, rd.timeline());

			if (rd.has_values()) {
				auto l = std::move(rd).as_values<size_t>();
				if (l != a0_) {
					++wrongResultCounter;
					}
				else {
					rd.commit_receive();
					}
				}
			else if (rd.has_error()) {
				++excResultCounter;
				std::wcout << L"out:  " << rd.error()->msg() << std::endl;
				}
			else {
				++undefResultCounter;
				std::cout << "out:  undefined result" << std::endl;
				}
			},

				a0_);
		}
	}

class outbound_handler_t : public my_mpf::rt0_outconnection_usecase_t {
	std::atomic<int> _startf{ 0 };
	std::weak_ptr<crm::distributed_ctx_t> _ctx;

public:
	outbound_handler_t(std::weak_ptr<crm::distributed_ctx_t> ctx,
		const crm::identity_descriptor_t& thisId,
		std::unique_ptr<crm::rt1_endpoint_t>&& ep,
		std::unique_ptr < crm::i_input_messages_stock_t >&& inputStackCtr)
		: my_mpf::rt0_outconnection_usecase_t(thisId, std::move(ep), std::move(inputStackCtr))
		, _ctx(ctx) {}

	void connection_handler(std::unique_ptr<crm::dcf_exception_t>&&, xpeer_link&& lnk_)final {
		auto roxAddress = lnk_.address();
		auto roxPort = lnk_.port();
		auto roxId = lnk_.id();
		auto roxName = lnk_.remote_id().name;

		std::cout << "outbound connection:"
			<< roxAddress << ":"
			<< roxPort << ":"
			<< roxId << ":"
			<< roxName << std::endl;

		send(std::move(lnk_));
		}
	};


class all2all_custom_hub_t : public my_mpf::rt0_hub_usecase_t {
public:
	all2all_custom_hub_t(const std::shared_ptr<crm::distributed_ctx_t>& ctx,
		std::unique_ptr<crm::i_endpoint_t>&& ep,
		const crm::identity_descriptor_t& thisId,
		std::shared_ptr<crm::i_xpeer_source_factory_t> ipeerFactory,
		std::shared_ptr<crm::i_message_input_t<crm::message_stamp_t>> inputStack,
		std::shared_ptr<crm::default_binary_protocol_handler_t> ph)
		: my_mpf::rt0_hub_usecase_t(ctx, std::move(ep), thisId, ipeerFactory, inputStack, ph)
		, _optb(std::make_shared< otable>()){}

private:
	class otable {
	public:
		std::map<crm::ipeer_l0::remote_object_endpoint_t, crm::opeer_l0> _opx;
		std::mutex _opxLock;
		};
	std::shared_ptr< otable> _optb;
	std::weak_ptr<crm::distributed_ctx_t> _ctx;

public:
	void make_outbound_connection() {
		auto rdp = crm::rt1_endpoint_t::make(localhostAddress, port, localhostId_RT0);

		auto outboundH = outbound_handler_t(
			ctx(),
			localhostId_RT0,
			std::make_unique<crm::rt1_endpoint_t>(rdp),
			std::make_unique<crm::opeer_l0_input2handler_t>([rdp, wptr = std::weak_ptr<otable>(_optb)](crm::opeer_l0::input_value_t&& ) {}));

		auto xpeer1 = tbl0->create_out_link(std::move(outboundH));

		_optb->_opx.insert({ rdp, std::move(xpeer1) });
		}
	};

std::shared_ptr<crm::distributed_ctx_t> make_context( const crm::identity_descriptor_t& hostId,
	bool ping) {

	crm::distributed_ctx_policies_t policies;
	policies.set_threadpool_maxsize(4);
	policies.set_threadpool_user_handlers_maxsize(2);
	policies.set_io_buffer_maxsize(CRM_MB);
	policies.set_io_threads_count(4);

	policies.set_ping_timeline(crm::sndrcv_timeline_periodicity_t::make_cyclic_timeouted(300s, 120s));
	policies.set_rt_ping(ping);

	return crm::distributed_ctx_t::make(crm::context_bindings(hostId), std::move(policies));
	}

int main() {

	auto ctx0 = make_context(localhostId_RT0, true);

	/* ������� ����������� */
	auto sourceFactory = crm::detail::tcp::tcp_source_factory_t::create(ctx0);

	/* �������� ����� ��� �������� ����������� */
	auto ep(std::make_unique<crm::detail::tcp::tcp_endpoint_t>(port, crm::detail::tcp::tcp_endpoint_t::version_t::v4));

	auto protocol = my_mpf::initialize_protocol(ctx0);

	auto rt0Settings = std::make_shared<all2all_custom_hub_t>(ctx0,
		std::move(ep),
		localhostId_RT0,
		[](crm::input_message_knop_l0&&) {});

	tbl0 = crm::router_0_t::create(rt0Settings);

	ctx0->register_handler({ "test_rpc_1" }, [](size_t l) {
		return l;
		});

	ctx0->start();
	rt0Settings->make_outbound_connection();


	while (true) {
		std::this_thread::sleep_for(10s);

		std::ostringstream log;
		log << "in processing operations  " << steadyOperationsCounter.load(std::memory_order::memory_order_acquire) << std::endl;
		log << "undefined results         " << undefResultCounter.load(std::memory_order::memory_order_acquire) << std::endl;
		log << "wrong results             " << wrongResultCounter.load(std::memory_order::memory_order_acquire) << std::endl;
		log << "exceptions                " << excResultCounter.load(std::memory_order::memory_order_acquire) << std::endl;

		std::cout << log.str();
		}

	return 0;
	}

