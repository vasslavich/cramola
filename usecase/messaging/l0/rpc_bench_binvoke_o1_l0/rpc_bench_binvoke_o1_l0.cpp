#include <vector>
#include <string>
#include <chrono>
#include <array>
#include <algorithm>
#include <appkit/appkit.h>
#include <hublon/r0.h>
#include "../../../ex_specialization/classes.h"
#include "../../../demo_utilities/utilities.h"


using namespace std::chrono;
using namespace crm::usecase;

crm::identity_descriptor_t localhostId_RT0 = crm::identity_descriptor_t{ crm::global_object_identity_t::as_hash("FAFA0000-0A0A-F1F1-AAAA-100000000001"), "host:0" };
std::string localhostAddress = "127.0.0.1";
short port = 16789;

std::shared_ptr<crm::router_0_t> tbl0;

static std::atomic<size_t> a0{ 0 };

static constexpr auto freqExcCollectPrint = 10;
//
//bool watchLatencyMin{ true };
//bool watchLatencyMax{ true };
size_t concurrencySize = 1;

//#ifdef CBL_MPF_TEST_ERROR_EMULATION
//constexpr size_t stat_step = 1000;
//constexpr size_t print_step = 2000;
//#else
//constexpr size_t stat_step = 5000;
//constexpr size_t print_step = 10000;
//#endif


/*struct stat_entry {
	size_t count{ 0 };
	double mps{ 0 };
	std::uintmax_t latencySysMax{ 0 };
	std::uintmax_t latencySysMin{ 0 };
	std::atomic<std::uintmax_t> latencySysAverCounter{ 0 };
	std::uintmax_t latencyThisMax{ 0 };
	std::uintmax_t latencyThisMin{ 0 };
	std::atomic<std::uintmax_t> latencyThisAverCounter{ 0 };

	using timeline_duration_t = std::chrono::seconds;
	using timeline_line_t = decltype(std::declval<timeline_duration_t>().count());
	std::chrono::high_resolution_clock::time_point _startTP;

	stat_entry()
		: _startTP{ std::chrono::high_resolution_clock::now() } {}

	void __add() {
		const auto c_ = ++count;
		if (c_ >= stat_step && (c_ % stat_step) == 0) {
			auto tl = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::high_resolution_clock::now() - _startTP).count();

			if (tl != 0) {
				mps = (double)c_ / (double)tl;
				}
			}
		}

	void __add(const std::chrono::high_resolution_clock::time_point& startTP,
		const std::chrono::microseconds& sysLatency) {

		auto thisLatency = (std::uintmax_t)std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - startTP).count();
		auto sysLatencyCount = (std::uintmax_t)sysLatency.count();

		if (watchLatencyMin) {
			if (!latencyThisMin) {
				latencyThisMin = thisLatency;
				}
			else if (thisLatency < latencyThisMin) {
				latencyThisMin = thisLatency;
				}

			if (!latencySysMin) {
				latencySysMin = sysLatencyCount;
				}
			else if (sysLatencyCount < latencySysMin) {
				latencySysMin = sysLatencyCount;
				}
			}

		if (watchLatencyMax) {
			if (thisLatency > latencyThisMax) {
				latencyThisMax = thisLatency;
				}

			if (sysLatencyCount > latencySysMax) {
				latencySysMax = sysLatencyCount;
				}
			}

		latencyThisAverCounter += thisLatency;
		latencySysAverCounter += sysLatencyCount;
		}

	void add() {
		__add();
		}

	void add(const std::chrono::high_resolution_clock::time_point& startTP,
		const std::chrono::microseconds& sysLatency) {

		__add();
		__add(startTP, sysLatency);

		add_post();
		}

	void add_post() {
		if ((count % print_step) == 1) {
			std::cout << "stat: >>" << print() << std::endl;
			}
		}

	std::string print()const {
		std::ostringstream ol;
		ol <<
			"count=" << std::setw(6) << count <<
			", mps=" << std::setw(6) << (size_t)std::trunc(mps) <<
			", latency median(sys/wrapwatch, microsec)=" << 
			std::setw(8) << ((double)latencySysAverCounter/(double)count) << '/' << 
			std::setw(8) << ((double)latencyThisAverCounter/(double)count) <<
			": min,max(wrapwatch)=" << 
			std::setw(8) << latencyThisMin << '/' << std::setw(8) << latencyThisMax;

		return ol.str();
		}
	}*/

stat_entry stgl;
//
//
//[[noreturn]]
//void FailExit(const crm::dcf_exception_t &e) {
//	std::wcout << L"FAIL(1) >> " << e.msg() << std::endl;
//	std::exit(-1);
//	}
//
//[[noreturn]]
//void FailExit(const std::exception& e) {
//	std::cout << "FAIL(2) >> " << e.what() << std::endl;
//	std::exit(-1);
//	}


class outbound_handler_t : public crm::rt0_connection_settings_t {
	std::weak_ptr<crm::distributed_ctx_t> _ctx;

public:
	outbound_handler_t(std::weak_ptr<crm::distributed_ctx_t> ctx,
		const crm::identity_descriptor_t& thisId,
		std::unique_ptr<crm::rt1_endpoint_t>&& ep,
		std::unique_ptr < crm::i_input_messages_stock_t >&& inputStackCtr)
		: crm::rt0_connection_settings_t(thisId, std::move(ep), std::move(inputStackCtr))
		, _ctx(ctx) {}

	void connection_handler(std::unique_ptr<crm::dcf_exception_t>&&, xpeer_link&& lnk_)final {
		auto roxAddress = lnk_.address();
		auto roxPort = lnk_.port();
		auto roxId = lnk_.id();
		auto roxName = lnk_.remote_id().name();

		std::cout << "outbound connection:"
			<< roxAddress << ":"
			<< roxPort << ":"
			<< roxId << ":"
			<< roxName << std::endl;

		for (size_t i = 0; i < concurrencySize; ++i) {
			std::thread([peer_ = lnk_, thidx = i]()mutable{
				auto peer = std::move(peer_);
				try {
					do {
						auto a0_ = a0.fetch_add(1);
						auto timepoint = std::chrono::high_resolution_clock::now();

						auto st = crm::rpc_binvoke(peer,
							"test_rpc_1",
							crm::function_descriptor{ "test_rpc_1" },
							a0_);

						auto [l] = crm::rpc_get_result(st).as<size_t>();
						if (l == a0_) {
							stgl.add(timepoint, st.timeline());
							}
						else {
							throw std::exception{};
							}
						}
					while (peer.is_opened());
					}
				catch (const crm::dcf_exception_t & e0) {
					std::wcout << std::endl << L"exception: " << e0.msg();
					}
				catch (const std::exception & e1) {
					std::wcout << std::endl << L"exception: " << e1.what();
					}
				catch (...) {
					std::wcout << std::endl << L"exception: undefined";
					}

				std::cout << "thread " << thidx << " exited" << std::endl;
				}).detach();
			}
		}
	};


class all2all_custom_hub_t : public crm::rt0_hub_settings_t {
public:
	template<typename TH>
	all2all_custom_hub_t(const std::shared_ptr<crm::distributed_ctx_t>& ctx,
		const crm::identity_descriptor_t& thisId,
		TH && inputStack)
		: crm::rt0_hub_settings_t(ctx, 
			thisId, 
			crm::detail::tcp::tcp_endpoint_t(port, crm::detail::tcp::tcp_endpoint_t::version_t::v4), 
			std::forward<TH>(inputStack)) {}

private:
	crm::opeer_l0 _opHandler;
	std::weak_ptr<crm::distributed_ctx_t> _ctx;

public:
	void run() {
		auto rdp = crm::rt1_endpoint_t::make_remote(localhostAddress, port, localhostId_RT0);

		auto outboundH = outbound_handler_t(
			ctx(),
			localhostId_RT0,
			std::make_unique<crm::rt1_endpoint_t>(rdp),
			std::make_unique<crm::opeer_l0_input2handler_t>([rdp](crm::opeer_l0::input_value_t&&) {}));
		_opHandler = tbl0->create_out_link(std::move(outboundH));
		}
	};

//std::shared_ptr<crm::distributed_ctx_t> make_context(const crm::identity_descriptor_t& hostId,
//	bool ping) {
//
//	crm::distributed_ctx_policies_t policies;
//	policies.set_threadpool_maxsize(std::min<size_t>(concurrencySize, 2));
//	policies.set_threadpool_user_handlers_maxsize(std::min<size_t>(concurrencySize,2));
//	policies.set_io_buffer_maxsize(CRM_MB);
//	policies.set_io_threads_count(4);
//
//	policies.set_ping_timeline(crm::sndrcv_timeline_periodicity_t::make_cyclic_timeouted(300s, 120s));
//	policies.set_rt_ping(ping);
//
//	return crm::distributed_ctx_t::make(crm::context_bindings(hostId), std::move(policies));
//	}

int main() {
	auto ctx0 = crm::usecase::make_context(localhostId_RT0);
	ctx0->register_handler({ "test_rpc_1" }, [](size_t l) {
		return l;
		});

	tbl0 = crm::router_0_t::create(all2all_custom_hub_t(ctx0,
		localhostId_RT0,
		[](crm::input_message_knop_l0&&) {}));

	tbl0->start();

	while (true) {
		std::this_thread::sleep_for(5s);

		std::cout << "in processing >>" << std::endl;
		std::cout << stgl.print() << std::endl;
		}

	return 0;
	}

