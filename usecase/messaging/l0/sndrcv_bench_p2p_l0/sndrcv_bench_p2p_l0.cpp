#include <vector>
#include <string>
#include <chrono>
#include <array>
#include <appkit/appkit.h>
#include <hublon/r0.h>
#include "../../../ex_specialization/classes.h"


using namespace std::chrono;


crm::identity_descriptor_t localhostId_RT0 = crm::identity_descriptor_t{ crm::global_object_identity_t::as_hash("FAFA0000-0A0A-F1F1-AAAA-100000000001"), "host:0" };
std::string localhostAddress = "127.0.0.1";
short port = 16789;


std::atomic<std::uintmax_t> _sndCounter{ 0 };
std::atomic<std::uintmax_t> _rcvCounter{ 0 };
std::atomic<std::uintmax_t> _excCounter{ 0 };

static constexpr auto freqExcCollectPrint = 100;
std::atomic<size_t> payloadSize{ 0 };
std::atomic<bool> payloadResized{ false };

#ifdef CBL_MPF_TEST_ERROR_EMULATION
constexpr size_t stat_step = 10;
constexpr size_t print_step = 20;
#else
constexpr size_t stat_step = 10000;
constexpr size_t print_step = 20000;
#endif


struct custom_message_strob_1 {
	std::vector<std::uint8_t> blob;
	std::string key;
	std::string name;
	};

custom_message_strob_1 m;

struct statistics {
	size_t payload;
	size_t serialized;
	};

size_t payload_size(const custom_message_strob_1& v)noexcept {
	return v.blob.size() * sizeof(std::decay_t<decltype(v.blob[0])>) + v.key.size() * sizeof(std::decay_t<decltype(v.key[0])>);
	}

template<typename T>
statistics get_stat(const T& v)noexcept {
	return statistics{ payload_size(v), crm::srlz::object_trait_serialized_size(v) };
	}




template< class, class = std::void_t<> >
struct is_blob_type : std::false_type {};

template< class T >
struct is_blob_type<T, std::void_t<decltype(std::declval<T>().data())>> : std::true_type {};

template<class T>
bool constexpr is_blob_type_v = is_blob_type<T>::value;




template<typename _TBlob,
	typename TBlob = typename std::decay_t<_TBlob>,
	typename std::enable_if_t<is_blob_type_v<TBlob> && !crm::srlz::detail::is_string_type_v<TBlob>, int> = 0>
	typename TBlob initialize(size_t count) {
	TBlob v;
	v.resize(count);

	size_t ib = 0;
	while (ib < count) {
		auto u16 = crm::sx_uuid_t::rand();
		for (size_t i = 0; i < crm::sx_uuid_t::count_of_as<decltype(v[0])>() && ib < count; ++i, ++ib) {
			v[ib] = u16.at<decltype(v[0])>(i);
			}
		}

	return v;
	}


template<typename _TString,
	typename TString = typename std::decay_t<_TString>,
	typename std::enable_if_t<crm::srlz::detail::is_string_type_v<TString>, int> = 0>
	typename TString initialize(size_t count) {
	TString v;
	v.resize(count);

	size_t ib = 0;
	while (ib < count) {
		auto rs = crm::sx_uuid_t::rand().to_string<TString::value_type>();
		for (size_t i = 0; i < rs.size() && ib < count; ++i, ++ib) {
			v[ib] = rs[i];
			}
		}

	return v;
	}


void initialize(custom_message_strob_1& v, size_t payload) {
	if (payload) {
		auto ksize = std::max((payload / 10), 1ull);
		auto bsize = std::max((payload - 2 * ksize), 1ull);

		v.blob = initialize<decltype(v.blob)>(bsize);
		v.key = initialize<decltype(v.key)>(ksize);
		v.name = initialize<decltype(v.name)>(ksize);
		}
	}

std::shared_ptr<crm::distributed_ctx_t> make_context(const std::string& /*ctxName*/,
	const crm::identity_descriptor_t& hostId,
	bool ping) {

	crm::distributed_ctx_policies_t policies;
	policies.set_threadpool_maxsize(2);
	policies.set_threadpool_user_handlers_maxsize(2);
	policies.set_io_buffer_maxsize(1024);
	policies.set_io_threads_count(1);

	policies.set_ping_timeline(crm::sndrcv_timeline_periodicity_t::make_cyclic_timeouted(300s, 120s));
	policies.set_rt_ping(ping);

	return crm::distributed_ctx_t::make(crm::context_bindings(hostId), std::move(policies));
	}

std::shared_ptr<crm::router_0_t> tbl0;


void print(std::list<crm::detail::exceptions_trace_collector::item_value>&& l) {
	if (!l.empty()) {
		std::wostringstream os;
		for (auto&& i : l) {
			os << L"code=" << i.code << L", num=" << i.count << L", m=" << i.message << L", file=" << i.file << L", line=" << i.line << std::endl;
			}

		crm::file_logger_t::logging(os.str(), 99998888);
		}
	}



struct stat_entry {
	size_t count{ 0 };
	size_t payload_count{ 0 };
	size_t serialized_count{ 0 };
	double mps{ 0 };
	std::chrono::microseconds latencySysMax{ 0 };
	std::chrono::microseconds latencySysMin{ 0 };
	std::chrono::microseconds latencySysAver{ 0 };
	std::chrono::microseconds latencyThisMax{ 0 };
	std::chrono::microseconds latencyThisMin{ 0 };
	std::chrono::microseconds latencyThisAver{ 0 };

	using timeline_duration_t = std::chrono::seconds;
	using timeline_line_t = decltype(std::declval<timeline_duration_t>().count());
	timeline_line_t timeline{ 0 };

	stat_entry()
		: timeline{ std::chrono::duration_cast<timeline_duration_t>(std::chrono::system_clock::now().time_since_epoch()).count() } {}

	void __add(const custom_message_strob_1& v) {
		const auto st = get_stat(v);

		payload_count += st.payload;
		serialized_count += st.serialized;

		const auto c_ = ++count;
		if (c_ >= stat_step && (c_ % stat_step) == 0) {
			auto currTL = std::chrono::duration_cast<timeline_duration_t>(std::chrono::system_clock::now().time_since_epoch()).count();
			auto tl = currTL - timeline;

			if (tl != 0) {
				mps = (double)c_ / (double)tl;
				}
			}
		}

	void __add(const std::chrono::high_resolution_clock::time_point& startTP,
		const std::chrono::microseconds& sysLatency) {

		auto thisLatency = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - startTP);

		if (!latencyThisMin.count()) {
			latencyThisMin = thisLatency;
			}
		else if (thisLatency < latencyThisMin) {
			latencyThisMin = thisLatency;
			}

		if (thisLatency > latencyThisMax) {
			latencyThisMax = thisLatency;
			}

		if (latencyThisAver.count()) {
			latencyThisAver = (latencyThisAver + thisLatency) / 2;
			}
		else {
			latencyThisAver = thisLatency;
			}


		if (!latencySysMin.count()) {
			latencySysMin = sysLatency;
			}
		else if (sysLatency < latencySysMin) {
			latencySysMin = sysLatency;
			}

		if (sysLatency > latencySysMax) {
			latencySysMax = sysLatency;
			}

		if (latencySysAver.count()) {
			latencySysAver = (latencySysAver + sysLatency) / 2;
			}
		else {
			latencySysAver = sysLatency;
			}
		}

	void add(const custom_message_strob_1& v) {
		__add(v);
		}

	void add(const custom_message_strob_1& v,
		const std::chrono::high_resolution_clock::time_point& startTP,
		const std::chrono::microseconds& sysLatency) {

		__add(v);
		__add(startTP, sysLatency);
		}

	std::string print()const {

		std::ostringstream ol;
		ol <<
			"count=" << std::setw(6) << count <<
			", mps=" << std::setw(6) << (size_t)std::trunc(mps) <<
			", latency(sys/there, microsec)=" << std::setw(8) << latencySysAver.count() << ":" << std::setw(8) << latencyThisAver.count() <<
			", payload(bytes)=" << std::setw(10) << payload_count <<
			", payload/serialized=" << std::setw(6) << (double)payload_count / (double)serialized_count;

		return ol.str();
		}
	};

class stat_lenta {
	stat_entry _lenta;
	std::atomic<size_t> addcnt{ 0 };

	void add_value(const custom_message_strob_1& v) {
		_lenta.add(v);
		}

	void add_value(const custom_message_strob_1& v,
		const std::chrono::high_resolution_clock::time_point& startTP,
		const std::chrono::microseconds& sysLatency) {

		_lenta.add(v, startTP, sysLatency);
		}

	void add_post() {
		if ((++addcnt % print_step) == 1) {
			std::cout << std::setw(20) << _lenta.print() << std::endl;
			}
		}

public:
	void add(const custom_message_strob_1& v) {
		add_value(v);
		add_post();
		}

	void add(const custom_message_strob_1& v,
		const std::chrono::high_resolution_clock::time_point& startTP,
		const std::chrono::microseconds& sysLatency) {

		add_value(v, startTP, sysLatency);
		add_post();
		}
	};

stat_lenta stgl;

template<typename TRox>
void receive_stat(TRox&& /*rox*/, custom_message_strob_1&& roxResponse,
	const std::chrono::high_resolution_clock::time_point& startTP,
	const std::chrono::microseconds& sysLatency) {

	stgl.add(roxResponse, startTP, sysLatency);
	}

template<typename TRox>
void receive_stat(TRox&& /*rox*/, custom_message_strob_1&& roxResponse) {

	stgl.add(roxResponse);
	}

template<typename TRox>
void recv_response(std::weak_ptr<crm::distributed_ctx_t> ctx,
	TRox&& xpeer,
	custom_message_strob_1&& roxResponse,
	/*crm::sndrcv_timeline_periodicity_t tp,*/
	const std::chrono::high_resolution_clock::time_point& startTP,
	const std::chrono::microseconds& sysLatency) {

	receive_stat(xpeer, std::move(roxResponse), startTP, sysLatency);
	send(ctx, crm::detail::make_invokable_xpeer(std::forward<TRox>(xpeer))/*, tp*/);
	}

std::atomic<std::intmax_t> sndrcvCollisions{ 0 };

template<typename TRox>
void send(std::weak_ptr<crm::distributed_ctx_t> ctx,
	TRox&& xpeer/*,
	crm::sndrcv_timeline_periodicity_t tp*/) {

	bool checkf = true;
	if (std::atomic_compare_exchange_strong(&payloadResized, &checkf, false) ){
		initialize(m, payloadSize);
		}

	auto next = crm::make_scoped_execution([xpeer] {
		bool ok{ false };
		do {
			if (auto ctx = tbl0->ctx()) {
				if (xpeer.is_opened()) {
					try {
						send(ctx, xpeer);
						ok = true;
						}
					catch (const std::exception & e0) {
						ctx->exc_hndl(e0);
						}
					catch (const crm::dcf_exception_t & e1) {
						ctx->exc_hndl(e1);
						}
					catch (...) {
						ctx->exc_hndl(CREATE_PTR_EXC_FWD(nullptr));
						}
					}
				else {
					return;
					}
				}
			}
		while (!ok);
		});

	auto decCtx = crm::detail::post_scope_action_t([] {
		--sndrcvCollisions;
		});
	if (++sndrcvCollisions > 1) {
		FATAL_ERROR_FWD(nullptr);
		}

	auto rh = [deci = std::move(decCtx),
		ctx,
		xpeer, /*tp, */
		n = std::move(next),
		timepoint = std::chrono::high_resolution_clock::now() ](

			crm::async_operation_result_t r,
			custom_message_strob_1&& obj,
			std::unique_ptr<crm::dcf_exception_t>&& exc_,
			std::chrono::microseconds&& mcs_ )mutable{

		++_sndCounter;
		deci.execute();

#ifdef CBL_SNDRCV_TRACE_COLLECT
		if (exc_) {
			auto excIdx = ++_excCounter;
			if (0 == (excIdx % freqExcCollectPrint)) {
				print(crm::detail::exceptions_trace_collector::instance().list());
				}
			}
#else

		/* complier only */
		exc_;
#endif

		if (r == crm::async_operation_result_t::st_ready) {
			CBL_VERIFY(!exc_);
			recv_response(ctx, xpeer, std::move(obj), /*tp,*/ timepoint, mcs_);

			n.reset();
			}
		else {
			n.execute();
			}
		};

	crm::ainvoke(
		ctx,
		std::forward<TRox>(xpeer),
		std::move(m),
		std::move(rh),
		std::string("snd-example:") + typeid(TRox).name(),
		crm::sndrcv_timeline_t::make_once_call());
	}

class outbound_handler_t : public crm::rt0_connection_settings_t {
	std::atomic<int> _startf{ 0 };
	std::weak_ptr<crm::distributed_ctx_t> _ctx;

public:
	outbound_handler_t(std::weak_ptr<crm::distributed_ctx_t> ctx,
		const crm::identity_descriptor_t& thisId,
		std::unique_ptr<crm::rt1_endpoint_t>&& ep,
		std::unique_ptr < crm::i_input_messages_stock_t >&& inputStackCtr)
		: crm::rt0_connection_settings_t(thisId, std::move(ep), std::move(inputStackCtr))
		, _ctx(ctx) {}

	void connection_handler(std::unique_ptr<crm::dcf_exception_t>&&, xpeer_link&& lnk)final {
		auto roxAddress = lnk.address();
		auto roxPort = lnk.port();
		auto roxId = lnk.id();
		auto roxName = lnk.remote_id().name();

		std::cout << "outbound connection:"
			<< roxAddress << ":"
			<< roxPort << ":"
			<< roxId << ":"
			<< roxName << std::endl;

		send(_ctx, std::move(lnk)/*, crm::sndrcv_timeline_periodicity_t::make_cyclic(2s)*/);
		}
	};

template<typename XIO, typename Tm>
void input_handler(XIO&& lnk, Tm&& message) {

	auto m2 = crm::message_forward<custom_message_strob_1>(std::move(message).m());
	if (m2) {
		auto v = std::move(m2).value();

		receive_stat(lnk, std::move(v).value());

		crm::detail::ostrob_message_envelop_t<custom_message_strob_1> response(std::move(m));
		response.capture_response_tail((*v.source()));

		lnk.push(std::move(response));
		v.commit();
		}
	}


class all2all_custom_hub_t : public crm::rt0_hub_settings_t {
public:
	all2all_custom_hub_t(const std::shared_ptr<crm::distributed_ctx_t>& ctx,
		std::unique_ptr<crm::i_endpoint_t>&& ep,
		const crm::identity_descriptor_t& thisId,
		std::shared_ptr<crm::i_xpeer_source_factory_t> ipeerFactory,
		std::shared_ptr<crm::i_message_input_t<crm::message_stamp_t>> inputStack,
		std::shared_ptr<crm::default_binary_protocol_handler_t> ph)
		: crm::rt0_hub_settings_t(ctx, std::move(ep), thisId, ipeerFactory, inputStack, ph)
		, _optb(std::make_shared< otable>())
		, _optbReversed(std::make_shared< otable>())
		, _ctx(ctx) {}

private:
	class otable {
	public:
		std::map<crm::ipeer_l0::remote_object_endpoint_t, crm::opeer_l0> _opx;
		std::mutex _opxLock;
		};
	std::shared_ptr< otable> _optb;
	std::shared_ptr< otable> _optbReversed;
	std::weak_ptr<crm::distributed_ctx_t> _ctx;


	static void push_opeer_message(std::weak_ptr<otable> optb, const crm::ipeer_l0::remote_object_endpoint_t& rdp,
		crm::ipeer_l0::input_value_t&& m_) {

		if (auto pt = optb.lock()) {
			std::unique_lock lck(pt->_opxLock);
			auto it = pt->_opx.find(rdp);
			if (it != pt->_opx.cend()) {
				input_handler((*it).second, std::move(m_));
				}
			}
		}

	crm::datagram_t remote_object_initialize(crm::syncdata_list_t&& /*dt*/)final {
		return crm::datagram_t{};
		}

	void local_object_initialize(crm::datagram_t&& /*dt*/,
		crm::detail::object_initialize_result_guard_t&& /*resultHndl*/)final {
		return;
		}

	void ipeer_connection(crm::ipeer_l0&& /*xpeer*/, crm::datagram_t&& /*initializeResult*/)final {
		//send(_ctx, std::move(xpeer)/*, crm::sndrcv_timeline_periodicity_t::make_cyclic(2s)*/);
		}

	void ipeer_disconnection(const crm::ipeer_l0::xpeer_desc_t& /*desc*/, std::shared_ptr<crm::i_peer_statevalue_t>&&)final {
		return;
		}

	bool remote_object_identity_validate(const crm::identity_value_t& /*idSet*/,
		std::unique_ptr<crm::i_peer_remote_properties_t>& /*prop*/)final {

		return true;
		}

public:
	void begin_outbound_connection() {
		auto rdp = crm::rt1_endpoint_t::make_remote(localhostAddress, port, localhostId_RT0);

		auto outboundH = outbound_handler_t(
			ctx(),
			localhostId_RT0,
			std::make_unique<crm::rt1_endpoint_t>(rdp),
			std::make_unique<crm::opeer_l0_input2handler_t>([rdp, wptr = std::weak_ptr<otable>(_optb)](crm::opeer_l0::input_value_t&& m_) {

			//push_opeer_message(wptr, rdp, std::move(m));

			auto xp = crm::get_link(m_);
			input_handler(xp, std::move(m));
			}));

		auto xpeer1 = tbl0->create_out_link(std::move(outboundH));

		_optb->_opx.insert({ rdp, std::move(xpeer1) });
		}
	};


int main() {

	payloadSize = 64;
	payloadResized = true;

	auto ctx0 = make_context(localhostId_RT0, true);

	/* ������� ����������� */
	auto sourceFactory = crm::detail::tcp::tcp_source_factory_t::create(ctx0);

	/* �������� ����� ��� �������� ����������� */
	auto ep(std::make_unique<crm::detail::tcp::tcp_endpoint_t>(port, crm::detail::tcp::tcp_endpoint_t::version_t::v4));

	auto protocol = my_mpf::initialize_protocol(ctx0);

	auto rt0Settings = all2all_custom_hub_t(ctx0,
		std::move(ep),
		localhostId_RT0,
		[](crm::input_message_knop_l0&& m) {
		input_handler(m.link(), std::move(m));
		});

	tbl0 = crm::router_0_t::create(rt0Settings);

	ctx0->register_message_type<custom_message_strob_1>();
	ctx0->start();

	rt0Settings->begin_outbound_connection();


	while (true) {
		std::this_thread::sleep_for(10s);
		
		payloadSize = ((payloadSize++) % CRM_MB) + 64;
		payloadResized = true;

		std::cout << "sndrcvCollisions= >>>> boom! = " << sndrcvCollisions << std::endl;
		}

	return 0;
	}
