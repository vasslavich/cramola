﻿#include <vector>
#include <string>
#include <chrono>
#include <array>
#include <algorithm>
#include <appkit/appkit.h>
#include <hublon/r0.h>
#include "../../../demo_utilities/utilities.h"


using namespace std::chrono;
using namespace crm::usecase;


crm::identity_descriptor_t localhostId_RT0 = crm::identity_descriptor_t{ 
	crm::global_object_identity_t::as_hash("demo ainvoke"), 
	"host" 
	};

std::string localhostAddress = "127.0.0.1";
short port = 16789;
stat_entry stgl;

std::shared_ptr<crm::router_0_t> tbl0;

template<typename TIO>
void rpc_demo_call(TIO peer, size_t arg) {
	auto timepoint = std::chrono::high_resolution_clock::now();

	crm::rpc_ainvoke(peer,
		crm::launch_as::async,
		"test_rpc_1",
		crm::function_descriptor{ "test_rpc_1" },
		[arg, timepoint, peer](size_t responseValue, std::unique_ptr<crm::dcf_exception_t>&& e, std::chrono::microseconds mcs) {

		if (responseValue == arg && !e) {
			stgl.add(timepoint, mcs);
			rpc_demo_call(peer, responseValue + 1);
			}
		else {
			stgl.add_error(e ? (*e) : crm::dcf_exception_t{});
			}
		},
		arg);
	}

class outbound_handler_t : public crm::rt0_connection_settings_t {
	std::weak_ptr<crm::distributed_ctx_t> _ctx;

public:
	outbound_handler_t(std::weak_ptr<crm::distributed_ctx_t> ctx,
		const crm::identity_descriptor_t& thisId,
		crm::rt1_endpoint_t && ep,
		std::unique_ptr < crm::i_input_messages_stock_t >&& inputStackCtr)
		: crm::rt0_connection_settings_t(thisId, std::move(ep), std::move(inputStackCtr))
		, _ctx(ctx) {}

	void connection_handler(std::unique_ptr<crm::dcf_exception_t>&& e, xpeer_link&& peer)final {
		if (!e) {
			rpc_demo_call(std::move(peer), 0);
			}
		else {
			stgl.add_error(*e);
			}
		}
	};


class all2all_custom_hub_t : public crm::rt0_hub_settings_t {
public:
	template<typename TH>
	all2all_custom_hub_t(const std::shared_ptr<crm::distributed_ctx_t>& ctx,
		crm::detail::tcp::tcp_endpoint_t&& ep,
		const crm::identity_descriptor_t& thisId,
		TH&& inputStack)
		: crm::rt0_hub_settings_t(ctx, std::move(ep), thisId, std::forward<TH>(inputStack))
		, _optb(std::make_shared< otable>()) {}

private:
	class otable {
	public:
		std::map<crm::ipeer_l0::remote_object_endpoint_t, crm::opeer_l0> _opx;
		std::mutex _opxLock;
		};
	std::shared_ptr< otable> _optb;
	std::weak_ptr<crm::distributed_ctx_t> _ctx;
	std::shared_ptr<crm::router_0_t> _tbl0;

public:
	void make_outbound_connection() {
		auto rdp = crm::rt1_endpoint_t::make_remote(localhostAddress, port, localhostId_RT0);

		auto outboundH = outbound_handler_t(
			ctx(),
			localhostId_RT0,
			crm::rt1_endpoint_t(rdp),
			std::make_unique<crm::opeer_l0_input2handler_t>([rdp, wptr = std::weak_ptr<otable>(_optb)](crm::opeer_l0::input_value_t&&) {}));

		auto xpeer1 = tbl0->create_out_link(std::move(outboundH));

		_optb->_opx.insert({ rdp, std::move(xpeer1) });
		}
	};


int main() {
	auto ctx0 = crm::usecase::make_context(localhostId_RT0);

	/* конечная точка для входящих подключений */
	crm::detail::tcp::tcp_endpoint_t ep(port, crm::detail::tcp::tcp_endpoint_t::version_t::v4);

	auto rt0Settings = all2all_custom_hub_t(ctx0,
		std::move(ep),
		localhostId_RT0,
		[](crm::input_message_knop_l0&&) {});

	tbl0 = crm::router_0_t::create(rt0Settings);

	ctx0->register_handler({ "test_rpc_1" }, [](size_t l) {
		return l;
		});

	ctx0->start();
	rt0Settings.make_outbound_connection();


	while (true) {
		std::this_thread::sleep_for(10s);

		std::cout << "============  in processing  =======================" << std::endl;
		std::cout << stgl.print() << std::endl;
		std::cout << "====================================================" << std::endl;
		}

	return 0;
	}
