﻿#include <hublon/r0.h>
#include "../../../../demo_utilities/endpoints.h"
#include "../../../../demo_utilities/default_context.h"
#include "../../../../demo_utilities/address_traftrace.h"
#include "../../../../demo_utilities/payload_types.h"
#include "../../../../demo_utilities/utilities.h"
#include "../../../../demo_utilities/inbound_sockets_hub.h"

using namespace crm;
using namespace usecase;

stat_entry statMessaging;

int main(){
    auto ctx = make_context(hostID, std::chrono::seconds(10), std::chrono::seconds(1));
    auto hub = make_inbound_sockets_hub(ctx,
        hostID,
        crm::detail::tcp::tcp_endpoint_t(port, crm::detail::tcp::tcp_endpoint_t::version_t::v4),
        [](crm::input_message_knop_l0&& ){
        
        statMessaging.add();
        });

    ctx->register_message_type<serialized_as_aggregate>();
    hub->start();

    while(true){
        std::this_thread::sleep_for(std::chrono::seconds(5));
        std::cout << statMessaging.print() << std::endl;
        }
    }
