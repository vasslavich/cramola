﻿#include <thread>
#include <appkit/appkit.h>
#include "../../../../demo_utilities/endpoints.h"
#include "../../../../demo_utilities/default_context.h"
#include "../../../../demo_utilities/address_traftrace.h"
#include "../../../../demo_utilities/payload_types.h"
#include "../../../../demo_utilities/utilities.h"
#include "../../../../demo_utilities/inbound_sockets_hub.h"

using namespace crm;
using namespace usecase;

stat_entry statMessagingInput;
stat_entry statMessagingOutput;

int main(){
    auto ctx = make_context(unitID, std::chrono::seconds(10), std::chrono::seconds(1));
    auto hub = make_inbound_sockets_hub(ctx,
        unitID,
        crm::detail::tcp::tcp_endpoint_t(port, crm::detail::tcp::tcp_endpoint_t::version_t::v4),
        [](crm::input_message_knop_l0&& ){

        statMessagingInput.add();
        });
    hub->start();

    auto socketHandler = make_outbound_socket(hub, ctx, unitID,
        crm::rt1_endpoint_t::make_remote(localhostAddress, port, hostID),
        [](crm::input_message_knop_l0&& ){},
        [](outbound_socket_handler_t::xpeer_link&& lnk){
        
        std::thread([&, lnk = std::move(lnk)]()mutable{

            try{
                while(true){
                    send( lnk, serialized_as_aggregate{});
                    statMessagingOutput.add();
                    }
                }
            catch(const std::exception& e){
                std::cout << e.what() << std::endl;
                }
            }).detach();
        });

    while(true){
        std::this_thread::sleep_for(std::chrono::seconds(5));
        std::cout << statMessagingOutput.print() << std::endl;
        }
    }


