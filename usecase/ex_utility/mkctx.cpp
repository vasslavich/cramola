#include "./mkctx.h"


using namespace usecase;
using namespace crm;


std::shared_ptr<distributed_ctx_t> usecase::make_serialization_example_ctx() {
	crm::distributed_ctx_policies_t contextOptionsRT0;
	contextOptionsRT0.set_threadpool_maxsize(2);
	contextOptionsRT0.set_threadpool_user_handlers_maxsize(2);
	contextOptionsRT0.set_io_buffer_maxsize(1024);
	contextOptionsRT0.set_io_threads_count(1);

	return crm::distributed_ctx_t::make(
		crm::context_bindings(identity_descriptor_t{ global_object_identity_t::rand(), "test" }),
		std::move(contextOptionsRT0));
	}
