#pragma once


#include <appkit/appkit.h>
#include <chrono>
#include <list>


namespace usecase {


auto rand_byte_vector() {
	auto binarr = crm::global_object_identity_t::rand().to_bytes<>();
	return std::vector<uint8_t>(binarr.cbegin(), binarr.cend());
	}

auto rand_string() {
	return crm::global_object_identity_t::rand().to_str();
	}

auto rand_subscriber_id() {
	return crm::subscriber_node_id_t::make();
	}

auto rand_identity_payload_entry() {
	return crm::identity_payload_entry_t{ rand_string(), rand_byte_vector() };
	}

bool eql(const crm::identity_payload_entry_t& l, const crm::identity_payload_entry_t& r) {
	return l.key == r.key && l.data == r.data;
	}

template<template<typename> typename Tcontainer>
auto rand_identity_payload_entry_container(size_t count) {
	Tcontainer< crm::identity_payload_entry_t> r;
	for(size_t i = 0;i < count; ++i)
		r.push_back({ rand_string(), rand_byte_vector() });

	return r;
	}

template<template<typename> typename Tcontainer>
bool eql(const Tcontainer<crm::identity_payload_entry_t>& l, const Tcontainer<crm::identity_payload_entry_t>&r) {
	if (l.size() == r.size()) {
		for (size_t i = 0; i < l.size(); ++i) {
			if (l[i].key != r[i].key || l[i].data != r[i].data)
				return false;
			}

		return true;
		}
	else
		return false;
	}

auto rand_identity_payload() {
	return crm::identity_payload_t{ rand_identity_payload_entry_container<std::vector>(10) };
	}

bool eql(const crm::identity_payload_t& l, const crm::identity_payload_t& r) {
	return eql<std::vector>(l.collection, r.collection);
	}

auto rand_connection_key() {
	return crm::connection_key_t{ "192.168.11.11", 
		crm::global_object_identity_t::rand().at<uint16_t>(0) };
	}

auto rand_identity_descriptor() {
	return crm::identity_descriptor_t{ crm::global_object_identity_t::rand(),
		crm::global_object_identity_t::rand().to_str() };
	}

auto rand_identity_value() {
	return crm::identity_value_t{ rand_identity_payload(), rand_identity_descriptor() };
	}

bool eql(const  crm::identity_value_t& l, const  crm::identity_value_t& r) {
	return eql(l.properties, r.properties) && l.id_value == r.id_value;
	}


auto rand_session_descriptor() {
	return crm::session_description_t{ crm::session_id_t::make(),
		crm::global_object_identity_t::rand().to_str() };
	}

auto rand_rt0_rdm() {
	return crm::rt0_node_rdm_t::make_output_rdm(rand_identity_descriptor(),
		rand_session_descriptor(),
		rand_connection_key(),
		rand_connection_key());
	}

auto rand_mrdm() {
	return crm::detail::rtl_roadmap_obj_t::make_rt1_ostream(rand_rt0_rdm(),
		rand_identity_descriptor(),
		rand_identity_descriptor(),
		rand_session_descriptor());
	}

auto rand_erdm() {
	return crm::detail::rtl_roadmap_event_t::make(rand_identity_descriptor(),
		crm::subscriber_event_id_t::make(),
		crm::detail::rtl_table_t::out,
		crm::detail::rtl_level_t::l1);
	}

auto rand_command_rdm() {
	return crm::detail::command_rdm_t{};
	}

auto rand_datagram() {
	crm::datagram_t dt;
	dt.add_value("ck", rand_connection_key());
	dt.add_value("id", rand_identity_descriptor());
	dt.add_value("session_id", rand_session_descriptor());
	dt.add_value("rt0", rand_rt0_rdm());
	dt.add_value("int", 111111);
	dt.add_value("mrdm", rand_mrdm());
	dt.add_value("id-payload-entry", rand_identity_payload_entry());

	return dt;
	}

auto rand_datagram_message() {
	return crm::osendrecv_datagram_t(rand_datagram());
	}

auto rand_identity_message() {
	return crm::oidentity_t(rand_identity_value(), rand_session_descriptor());
	}

auto rand_command_message() {
	crm::ocommand_t c;
	c.set_cmd(rand_string());
	c.set_tag(rand_string());

	return c;
	}

bool eql(const  crm::detail::command_rdm_t& l, const  crm::detail::command_rdm_t& r) {
	return l.cmd.at_binary() ==  r.cmd.at_binary() && l.target == r.target;
	}


auto rand_evlist() {
	return std::list<crm::detail::rtx_command_type_t>{
		crm::detail::rtx_command_type_t::invalid_xpeer,
			crm::detail::rtx_command_type_t::sndrcv_result,
			crm::detail::rtx_command_type_t::unbind_connection,
			crm::detail::rtx_command_type_t::sndrcv_fault,
			crm::detail::rtx_command_type_t::xchannel_rt1_initial };
	}

auto rand_cmd_addlink() {
	crm::detail::i_rt0_cmd_addlink_t cmd(rand_mrdm(), rand_erdm(), rand_evlist());

	cmd.set_channel_id(crm::channel_identity_t::make());
	cmd.set_command_id(crm::local_command_identity_t::make());
	cmd.set_command_evrdm(rand_erdm());
	cmd.set_wait_timeout(std::chrono::seconds(10));

	return cmd;
	}

auto rand_ping() {
	return crm::oping_id_t{};
	}

auto rand_request() {
	crm::orequest_t r;

	r.set_code(rand_string());
	return r;
	}

auto rand_notify() {
	crm::onotification_t n;
	n.set_ntf(CREATE_PTR_EXC_FWD(rand_string()));
	return n;
	}

auto rand_connection() {
	return crm::oconnection_t(rand_session_descriptor());
	}

auto rand_identity() {
	crm::oidentity_t i(rand_identity_value(), rand_session_descriptor());
	return i;
	}

template<typename T>
auto rand_strob(T&& t) {
	return crm::detail::ostrob_message_envelop_t<T>(std::forward<T>(t));
	}
}

