#pragma once

#include <appkit/appkit.h>


namespace usecase {


std::shared_ptr<crm::distributed_ctx_t> make_serialization_example_ctx();
}
