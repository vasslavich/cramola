#pragma once


#include <fstream>
#include <vector>
#include <string>
#include <exception>
#include <appkit/appkit.h>


namespace usecase {


enum class fileop_stream_mode {
	binray,
	text
	};

void read_from_file_binary(const std::wstring &filename,
	std::vector<uint8_t> & vec,
	size_t offset,
	std::intmax_t count);

std::vector<uint8_t> read_from_file_binary(const std::wstring &filename);

void write2file_binary(const std::vector<uint8_t> &src, const std::wstring &filename);
}

