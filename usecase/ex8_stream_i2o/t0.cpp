#include "./t0.h"
#include "../ex_utility/fileop.h"
#include <filesystem>
#include <thread>
#include <future>
#include "../ex_specialization/utilmk.h"


using namespace crm;
using namespace crm::testmark;


namespace filesystem {
using namespace std::experimental::filesystem;
}



void rt1_custom_outconnection_2_t::connection_handler(std::unique_ptr<dcf_exception_t> && exc,
	crm::detail::i_xpeer_rt1_t::link_t &&) {

	if(auto ptst = testsuite.lock()) {
		if(exc) {
			ptst->domain()->set_is_error(indexPeer);
			}
		}
	else {
		FATAL_ERROR_FWD((*exc));
		}
	}



rt1_custom_hub_t::rt1_custom_hub_t( const std::shared_ptr<crm::distributed_ctx_t> & ctx,
	const crm::identity_descriptor_t & thisId,
	crm::router_0_t & rt0,
	std::unique_ptr<crm::i_input_messages_stock_t> && inputStackCtr )
	: crm::rt1_hub_settings_t( ctx, thisId, rt0, std::move( inputStackCtr ) ) {}

void rt1_custom_hub_t::connection_handler( crm::ipeer_t && xpeer,
	crm::datagram_t && initializeResult ) {
	
	auto rp = std::dynamic_pointer_cast<const rt1_custom_outxproperties_t>(xpeer.remote_properties());
	const auto testId = CHECK_PTR( rp )->testId;
	const auto peerId = CHECK_PTR(rp)->indexPeer;

	if(auto ts_ = ts.lock()) {
		ts_->connection_handler_2d(testId, peerId, std::move(xpeer));
		}
	}

void rt1_custom_hub_t::set_testsuite( std::shared_ptr<recurrent_render_stream2input_base_t> testsuite_ ) {
	ts= testsuite_ ;
	}

bool rt1_custom_hub_t::remote_object_identity_validate( const crm::identity_value_t & idSet,
	std::unique_ptr<crm::i_peer_remote_properties_t> & prop ) {

	auto pProp = idSet.properties.find_by_key( crm::connection_stage_descriptions_t::identity_user_options );
	if( !pProp || pProp->data.empty() )
		FATAL_ERROR_FWD(nullptr);

	auto xprop = std::make_unique<rt1_custom_outxproperties_t>();
	xprop->deserialize( pProp->data );

	prop = std::move( xprop );
	return true;
	}

void rt1_custom_hub_t::event_handler(std::unique_ptr<crm::i_event_t> && ev) {
	if(ev) {
		if(auto s = ts.lock()) {
			s->domain()->event_handler(-1, std::move(ev));
			}
		else {
			THROW_EXC_FWD(nullptr);
			}
		}
	}


recurrent_render_stream2input_base_t::recurrent_render_stream2input_base_t( std::string ip_,
	short targetPort_,
	crm::identity_descriptor_t targetId_)
	: ip_address( ip_ )
	, targetPort( targetPort_ )
	, target( targetId_ ) {}

std::shared_ptr<testsuite_base_t> recurrent_render_stream2input_base_t::domain()const noexcept {
	return testSuite;
	}

void recurrent_render_stream2input_base_t::make(const std::shared_ptr<crm::distributed_ctx_t> & ctx,
	size_t peersCount,
	size_t count) {

	auto suitePtr = make_testsuite();
	std::lock_guard<std::mutex> lck(suitePtr->siglock);

	std::atomic_exchange(&testSuite, suitePtr);

	crm::file_logger_t::logging("start test id " + std::to_string(suitePtr->test_id()), 1212121214);

	std::cout << "make input data..." << std::endl;
	suitePtr->make_place(ctx, peersCount, count);
	std::cout << "make input data...ok" << std::endl;

	suitePtr->_2DSizeCounter += suitePtr->size_2d();
	}

void recurrent_render_stream2input_base_t::start(size_t peersCount,
	size_t count,
	std::shared_ptr<crm::router_1> tbl1) {

	auto p = domain();
	std::lock_guard<std::mutex> lck(p->siglock);

	std::cout << "make out-streams..." << std::endl;

	std::vector<opeer_item_t> pl(peersCount);
	for(size_t ip = 0; ip < pl.size(); ++ip) {

		crm::identity_descriptor_t cnid;
		cnid.id.at<decltype(ip)>(0) = ip + 1;
		cnid.name = "outbound-1-i-side-" + std::to_string( ip );

		auto xpeer1 = tbl1->create_out_link(rt1_custom_outconnection_2_t(
			cnid, 
			ip, 
			std::make_unique<crm::rt1_endpoint_t>(crm::rt1_endpoint_t::make(ip_address, targetPort, target)),
			[](crm::input_message_knop_l1&&) {},
			p,
			weak_from_this()));

		opeer_item_t item;
		item.peer = std::move(xpeer1);

		pl[ip] = std::move(item);
		}

	p->set_peers(std::make_unique<out_peers_collection>(std::move(pl)));
	std::cout << "make out-streams...ok" << std::endl;
	}

resolve_result_t recurrent_render_stream2input_base_t::wait_resolve(
	std::weak_ptr<crm::distributed_ctx_t> ctx0,
	std::weak_ptr<crm::distributed_ctx_t> ctx1) {

	resolve_result_t r;

	auto sptr = domain();
	if(sptr) {
		while(!r.ready) {

			std::unique_lock<std::mutex> cvlck(sptr->siglock);
			auto waitr = sptr->endSignal.wait_for(cvlck, std::chrono::seconds(10));
			if(waitr == std::cv_status::timeout) {
				std::cout << sptr->inform_conters() << std::endl;

#ifdef CBL_MPF_MESSAGE_TRACEWAY_KEYS_BY_ID
				crm::file_logger_t::logging(L"keytracer:>>> " + print(crm::detail::get_keytracer()), 101);
#endif

				if(auto c0 = ctx0.lock())
					crm::file_logger_t::logging(c0->get_errormod_counters_s(), 300);

				if(auto c1 = ctx1.lock())
					crm::file_logger_t::logging(c1->get_errormod_counters_s(), 301);
				}
			else {
				r.ready = sptr->validate_count_2();
				}
			}

		std::ostringstream obuf;
		obuf << "testmark wait_resolve #" << sptr->test_id() << std::endl;

		if(r.ready) {
			for(size_t pn = 0; pn < sptr->peers_count(); ++pn) {
				for(size_t on = 0; on < sptr->objects_count(pn); ++on) {
					auto rf = sptr->resultf(pn, on).get();
					if(rf) {
						r.vlst.emplace_back(true, sptr->object_size(pn, on));
						}
					else {
						r.vlst.emplace_back(false, sptr->object_size(pn, on));
						}

					obuf << "    " << sptr->object_size(pn, on) << ":" << (rf ? "ok" : "false") << std::endl;
					}
				}
			}

		crm::file_logger_t::logging(obuf.str(), 1010404030, __FILEW__, __LINE__);
		}

	return r;
	}

void recurrent_render_stream2input_base_t::close()noexcept {
	auto p = std::atomic_exchange(&testSuite, std::shared_ptr<testsuite_base_t>{});
	if(p)
		p->clear();
	}

void recurrent_render_stream2input_base_t::connection_handler_2d(int testId,
	int peerId,
	crm::ipeer_t xpeer_) {

	const auto suitePtr = domain();
	CBL_VERIFY(suitePtr);

	suitePtr->_ConnectionsCounter++;

	if(testId == suitePtr->test_id()) {

		if(!(suitePtr->set_is_connected(peerId) && xpeer_.renewable_connection())) {
			for(size_t is = 0; is < suitePtr->objects_count(peerId); ++is) {

				auto pS = suitePtr->readers(peerId, is);
				std::vector<std::string> keys = { std::to_string(peerId), std::to_string(is), std::to_string(testId), pS->source_id().to_string() };
				auto blocksSize = 1024ull;

				crm::rstream_handler_blob_t fr(pS,
					std::move(keys),
					blocksSize,
					std::to_string(peerId) + ":" + std::to_string(is) + ":mem",
					std::chrono::seconds(100000));

				const auto idStream = suitePtr->_IncCounter.load();

				crm::make_ostream_launch( xpeer_.ctx(),
					xpeer_,
					std::move(fr),
					[](const std::unique_ptr<crm::dcf_exception_t> & exc) {
					return true;
					},
					[peerId, is, suitePtr, testId](crm::async_operation_result_t st,
						std::unique_ptr<crm::i_stream_end_t> && h,
						std::unique_ptr<crm::i_stream_rhandler_t> && rh,
						std::unique_ptr<crm::dcf_exception_t> && exc) {

						++suitePtr->_DecCounter;

						std::unique_lock<std::mutex> lck(suitePtr->siglock);

						if(st == crm::async_operation_result_t::st_ready) {
							++suitePtr->_SuccessCounter;

							++suitePtr->at(peerId, is)->outReady;
							suitePtr->check_ready();
							}
						else if(exc) {
							/**/
							}
						else {
							++suitePtr->_ErrorCounter;
							FATAL_ERROR_FWD(nullptr);
							/**/
							}


						std::ostringstream strace;

						if(suitePtr->_DecCounter == suitePtr->size_2d() || suitePtr->_DecCounter == suitePtr->_IncCounter) {
							strace << ":"
								<< std::endl << "============================ test id " << testId << " ================" << std::endl
								<< std::endl << "                             ALL STREAM END UP" << std::endl
								<< std::endl << "======================================================================" << std::endl;

							strace << suitePtr->inform() << std::endl;
							}

						crm::file_logger_t::logging(strace.str(), 1212121214);
						});


				++suitePtr->_IncCounter;
				}
			}
		}
	}




recurrent_render_stream2input_t::recurrent_render_stream2input_t( std::string ip_,
	short targetPort_,
	crm::identity_descriptor_t targetId_)
	: recurrent_render_stream2input_base_t( ip_ , targetPort_ ,targetId_ ) {}

std::shared_ptr<testsuite_base_t> recurrent_render_stream2input_t::make_testsuite()const {
	return std::make_shared<testsuite_memory_buffer_t>();
	}




recurrent_render_file_stream2input_t::recurrent_render_file_stream2input_t( std::string ip_,
	short targetPort_,
	crm::identity_descriptor_t targetId_  )
	: recurrent_render_stream2input_base_t( ip_ ,targetPort_ ,targetId_ ) {}

std::shared_ptr<testsuite_base_t> recurrent_render_file_stream2input_t::make_testsuite()const {
	return std::make_shared<testsuite_files_t>();
	}


