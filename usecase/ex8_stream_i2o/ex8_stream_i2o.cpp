#include <algorithm>
#include <mutex>
#include <appkit/appkit.h>
#include <hublon/r0.h>
#include <hublon/r1.h>
#include "../ex_specialization/classes.h"
#include "./t0.h"

using namespace std::chrono;

using namespace std::chrono;

std::string ip_address = "127.0.0.1";
short port = 16789;
short targetPort = 16789;
crm::identity_descriptor_t thisIdTable_0 = { crm::global_object_identity_t::from_str( "FAFA0000-0A0A-F1F1-AAAA-100000000001" ), "host-0" };


std::shared_ptr<crm::i_message_queue_t<crm::message_stamp_t>> irt0Stack;


bool runInteractive = false;


template<typename Tstring>
static void write2file(const Tstring &strout, const std::wstring & filename) noexcept {

	typedef typename Tstring::value_type string_elem_t;
	typedef typename std::basic_ofstream<string_elem_t> ofstream_t;

	std::ofstream ofile(filename, std::ios_base::out | std::ios_base::app);
	if(ofile.is_open()) {
		ofile << strout << std::endl;
		ofile.close();
		}
	}

void print_log(const std::string & s) {
	crm::file_logger_t::logging(s, 10104040, __FILEW__, __LINE__);

	//std::wstring fname(L"e:\\Y2U\\OffDisk\\test-gggg.xtx");
	//write2file(s, fname);
	}

std::shared_ptr<crm::distributed_ctx_t> make_context(const crm::identity_descriptor_t & hostId,
	bool ping) {

	crm::distributed_ctx_policies_t policies;
	policies.set_threadpool_maxsize(16);
	policies.set_threadpool_user_handlers_maxsize(16);
	policies.set_io_buffer_maxsize(1024);
	policies.set_io_threads_count(1);

	policies.set_ping_timeline(crm::sndrcv_timeline_periodicity_t::make_cyclic_timeouted(60s, 10s));
	policies.set_rt_ping(ping);

	crm::distributed_ctx_t::make(crm::context_bindings(hostId), std::move(policies));
	}

int main(int argc, char* argv[]) {

	auto ctx0(make_context(thisIdTable_0, true));

	irt0Stack = ctx0->queue_manager().tcreate_queue<crm::message_stamp_t>(__FILE_LINE__, crm::detail::queue_options_t::message_stack_node);
	auto tbl0(my_mpf::initialize_rt0( port, thisIdTable_0, ctx0, irt0Stack));
	ctx0->start();


	crm::testmark::testmark_accumulator_t st;
	for(int mainCycle = 51; mainCycle < 1000000; mainCycle += 3) {

		crm::identity_descriptor_t thisIdTable_1 = { crm::global_object_identity_t::rand(), "host-l1-outbound" };

		auto ctx1(make_context( thisIdTable_1, false));

		crm::channel_info_t chDesc;
		chDesc.set_id( ctx1->make_local_object_identity() );

		auto chOpt = crm::channel_common_memory_config( chDesc, tbl0->rt0_channel_router() );
		auto t0_1_queue = crm::make_channel_cm_queue( ctx1, chOpt );
		tbl0->register_rt1_channel( t0_1_queue );

		auto rt1Hub = crm::testmark::rt1_custom_hub_t(ctx1, thisIdTable_1, (*tbl0),
			[](crm::ipeer_t peer, crm::input_message_knop_l1&&) {
			});

		auto tbl1 = crm::router_1::create( t0_1_queue, std::move(rt1Hub));

		const auto peersCount = (std::max)((mainCycle) % 3000, 2);
		const auto objectsOnPeer = 2;// (std::max)((mainCycle) % (4000 / peersCount), 2);

		std::ostringstream scl;

		scl << "setup" << std::endl;
		scl << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << std::endl;
		scl << "   - cycle #" + std::to_string(mainCycle) << std::endl;
		scl << "   - peers count:" << peersCount << std::endl;
		scl << "   - objects on peer count:" << objectsOnPeer << std::endl;

		print_log(scl.str());


		auto testsuite = std::make_shared<crm::testmark::recurrent_render_stream2input_t>(ip_address,
			targetPort,
			thisIdTable_1);
		rt1Hub->set_testsuite(testsuite);

		testsuite->make(ctx1, peersCount, objectsOnPeer);
		testsuite->start(peersCount, objectsOnPeer, tbl1);
		ctx1->start();

		const auto stx = testsuite->wait_resolve(ctx0, ctx1);

		size_t ifail = 0;
		if(stx.ready) {

			for(const auto & ist : stx.vlst) {
				auto sizeKey = std::get<1>(ist);
				auto resultKey = std::get<0>(ist);

				++st.streams_stat[sizeKey].count;
				if(resultKey)
					++st.streams_stat[sizeKey].resultOk;
				else {
					++st.streams_stat[sizeKey].resultFail;
					++ifail;
					}
				}
			}
		else {
			print_log(std::string("cycle #" + std::to_string(mainCycle) + ":fail"));

			++ifail;
			break;
			}

		tbl0->unregister_rt1_channel( chDesc );
		ctx1->close();
		testsuite->close();

		if(ifail) {
			print_log("fail counter:");
			getchar();
			}
		}

	if(runInteractive) {
		std::cout << "======= press any key... ==============" << std::endl;
		getchar();
		}


	if(runInteractive) {
		std::cout << "======= press <E> for exit ============" << std::endl;
		bool d = true;
		while(d) {
			std::this_thread::sleep_for(std::chrono::seconds(10));

			std::string entry;
			std::cin >> entry;

			if(entry == "E")
				d = false;
			}
		}

	std::ostringstream obuf;
	obuf << "summary=" << std::endl;
	for(const auto & itest : st.streams_stat) {
		obuf << "    " << itest.first << ":" << itest.second.count << ":" << itest.second.resultOk << ":" << itest.second.resultFail << std::endl;
		}

	print_log(obuf.str());

	if(runInteractive) {
		std::cout << "======= press any key... ==============" << std::endl;
		getchar();
		}

	print_log("========= THE OF THE TEST ==============");

	crm::terminate_list_execute();

	return 0;
	}

