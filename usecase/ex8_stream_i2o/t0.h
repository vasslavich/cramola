#pragma once


#include <appkit/appkit.h>
#include <hublon/r0.h>
#include <hublon/r1.h>
#include "../ex_specialization/streams_test_classes.h"


namespace crm{
namespace testmark{


class recurrent_render_stream2input_base_t :
	public std::enable_shared_from_this< recurrent_render_stream2input_base_t> {

public:
	std::shared_ptr<testsuite_base_t> testSuite;
	std::string ip_address;
	short targetPort;
	crm::identity_descriptor_t target;

	virtual std::shared_ptr<testsuite_base_t> make_testsuite()const = 0;
	virtual void close()noexcept;

public:
	recurrent_render_stream2input_base_t( std::string ip_,
		short targetPort_,
		crm::identity_descriptor_t targetId_);
	void make(const std::shared_ptr<crm::distributed_ctx_t> & ctx,
		size_t peersCount,
		size_t count);
	void start(size_t peersCount,
		size_t count,
		std::shared_ptr<crm::router_1> tbl1_);
	resolve_result_t wait_resolve(std::weak_ptr<crm::distributed_ctx_t> ctx0,
		std::weak_ptr<crm::distributed_ctx_t> ctx1);
	std::shared_ptr<testsuite_base_t> domain()const noexcept;
	void connection_handler_2d(int testId, int peerId, crm::ipeer_t xpeer);
	};


class rt1_custom_outconnection_2_t : public rt1_custom_outconnection_t {
public:
	std::weak_ptr<recurrent_render_stream2input_base_t> testsuite;
	rt1_custom_outconnection_2_t(const crm::identity_descriptor_t & thisId,
		size_t indexPeer_,
		std::unique_ptr<crm::rt1_endpoint_t> && ep,
		std::unique_ptr<crm::i_input_messages_stock_t> && inputStackCtr,
		std::weak_ptr<testsuite_base_t> testsuiteEH_,
		std::weak_ptr<recurrent_render_stream2input_base_t> testsetX_)
		: rt1_custom_outconnection_t(thisId, indexPeer_, std::move(ep), std::move(inputStackCtr), testsuiteEH_)
		, testsuite(testsetX_) {}

	void connection_handler(std::unique_ptr<dcf_exception_t> &&, crm::detail::i_xpeer_rt1_t::link_t &&)final;
	};


class rt1_custom_hub_t : public crm::rt1_hub_settings_t {
public:
	rt1_custom_hub_t( const std::shared_ptr<crm::distributed_ctx_t> & ctx,
		const crm::identity_descriptor_t & thisId,
		crm::router_0_t & rt0,
		std::unique_ptr<crm::i_input_messages_stock_t> && inputStackCtr );

	void set_testsuite( std::shared_ptr<recurrent_render_stream2input_base_t> testsuite );
	void event_handler(std::unique_ptr<crm::i_event_t> && ev)final;

private:
	std::atomic < std::uintmax_t> _inputConnectionNum = 0;
	std::weak_ptr<recurrent_render_stream2input_base_t> ts;

	void connection_handler(crm::ipeer_t && xpeer, crm::datagram_t && initializeResult)final;
	bool remote_object_identity_validate( const crm::identity_value_t & idSet,
		std::unique_ptr<crm::i_peer_remote_properties_t> & prop )final;
	};


class recurrent_render_stream2input_t :
	public recurrent_render_stream2input_base_t {

	std::shared_ptr<testsuite_base_t> make_testsuite()const final;

public:
	recurrent_render_stream2input_t( std::string ip_,
		short targetPort_,
		crm::identity_descriptor_t targetId_);
	};


class recurrent_render_file_stream2input_t :
	public recurrent_render_stream2input_base_t {

	std::shared_ptr<testsuite_base_t> make_testsuite()const final;

public:
	recurrent_render_file_stream2input_t( std::string ip_,
		short targetPort_,
		crm::identity_descriptor_t targetId_);
	};
}
}
