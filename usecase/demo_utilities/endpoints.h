#pragma once

#include <string>
#include <appkit/appkit.h>

namespace crm::usecase {

extern short port;
extern const std::string localhostAddress;
extern const identity_descriptor_t unitID;
extern const identity_descriptor_t hubID;
extern const identity_descriptor_t hostID;
extern const std::string hostChannelName;
extern const std::string hubChannelName;
extern const std::string unitChannelName;
}
