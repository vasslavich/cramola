#pragma once

#include "./counters_stat.h"
#include "./default_context.h"


namespace crm::usecase {

class outbound_pinned_links {
private:
	std::map<crm::ipeer_t::remote_object_endpoint_t, crm::opeer_t> _opx;
	mutable std::mutex _opxLock;

public:
	bool exists(const crm::ipeer_t::remote_object_endpoint_t& rdp)const noexcept {
		std::unique_lock lck(_opxLock);
		return _opx.count(rdp) > 0;
		}

	void insert(crm::ipeer_t::remote_object_endpoint_t rdp, crm::opeer_t peer) {
		std::unique_lock lck(_opxLock);
		_opx[rdp] = std::move(peer);
		}
	};

std::wstring cvt2wstr(const std::string& s);
std::string cvt2str(const std::wstring& s);
}


