#pragma once

#include <appkit/appkit.h>

namespace crm::usecase {

/*! example of custom stream header(cover) data structure */
struct stream_header_cv final/* for serialization requirements */
	: crm::i_stream_zyamba_cover_data_t/* for streaming cover data requirements */ {

	std::string senderName;
	std::string submissionSummary;
	size_t countBytes{ 0 };
	std::chrono::system_clock::time_point initialTime = std::chrono::system_clock::now();

	/*! i_stream_zyamba_cover_data_t implementaion */
	const crm::srlz::i_typeid_t& stream_typeid()const noexcept {
		static const auto stream_blob_typeid = crm::srlz::typemap_key_t::make<stream_header_cv>();
		return stream_blob_typeid;
		}

	/*! for streaming cover data requirements */
	crm::address_hash_t object_hash()const noexcept {
		crm::address_hash_t h;
		crm::apply_at_hash(h, senderName);
		crm::apply_at_hash(h, submissionSummary);
		crm::apply_at_hash(h, countBytes);
		crm::apply_at_hash(h, initialTime);

		return h;
		}

	/* serialization support
	========================================================== */

	/*! for serialization */
	template<typename S>
	void srlz(S&& dest)const {
		crm::srlz::serialize(dest, senderName);
		crm::srlz::serialize(dest, submissionSummary);
		crm::srlz::serialize(dest, countBytes);
		crm::srlz::serialize(dest, initialTime);
		}

	/*! for deserialization */
	template<typename S>
	void dsrlz(S&& src) {
		crm::srlz::deserialize(src, senderName);
		crm::srlz::deserialize(src, submissionSummary);
		crm::srlz::deserialize(src, countBytes);
		crm::srlz::deserialize(src, initialTime);
		}

	/*! member function if cheap length calculation of instance */
	size_t get_serialized_size()const noexcept {
		return crm::srlz::object_trait_serialized_size(senderName) +
			crm::srlz::object_trait_serialized_size(submissionSummary) +
			crm::srlz::object_trait_serialized_size(countBytes) +
			crm::srlz::object_trait_serialized_size(initialTime);
		}
	};
static_assert(crm::srlz::is_serializable_v<stream_header_cv>, __FILE_LINE__);
static_assert(crm::is_stream_cover_data_trait_v<stream_header_cv>, __FILE_LINE__);
}

