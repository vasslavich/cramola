#include "./counters_stat.h"

using namespace crm;
using namespace crm::usecase;


stat_entry::stat_entry(size_t averageStep)
	: stat_step(averageStep)
	, _startTP{ std::chrono::high_resolution_clock::now() } {}

void stat_entry::__add() {
	const auto c_ = ++count;
	if (c_ >= stat_step && (c_ % stat_step) == 0) {
		auto tl = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::high_resolution_clock::now() - _startTP).count();

		if (tl != 0) {
			mps = (double)c_ / (double)tl;
			}
		}
	}

void stat_entry::__add(const std::chrono::high_resolution_clock::time_point& startTP,
	const std::chrono::microseconds& sysLatency) {

	auto thisLatency = (std::uintmax_t)std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - startTP).count();
	auto sysLatencyCount = (std::uintmax_t)sysLatency.count();

	if (watchLatencyMin) {
		if (!latencyThisMin) {
			latencyThisMin = thisLatency;
			}
		else if (thisLatency < latencyThisMin) {
			latencyThisMin = thisLatency;
			}

		if (!latencySysMin) {
			latencySysMin = sysLatencyCount;
			}
		else if (sysLatencyCount < latencySysMin) {
			latencySysMin = sysLatencyCount;
			}
		}

	if (watchLatencyMax) {
		if (thisLatency > latencyThisMax) {
			latencyThisMax = thisLatency;
			}

		if (sysLatencyCount > latencySysMax) {
			latencySysMax = sysLatencyCount;
			}
		}

	latencyThisAverCounter += thisLatency;
	latencySysAverCounter += sysLatencyCount;
	}

void stat_entry::add() {
	__add();
	}

void stat_entry::add(const std::chrono::high_resolution_clock::time_point& startTP,
	const std::chrono::microseconds& sysLatency) {

	__add();
	__add(startTP, sysLatency);
	}

void stat_entry::add_error(const std::exception & ) {
	errorsCounter.fetch_add(1);
	}

std::string stat_entry::print()const {
	std::ostringstream ol;
	ol <<
		"count=" << std::setw(6) << count <<
		", mps=" << std::setw(6) << (size_t)std::trunc(mps) <<
		", latency median(sys/wrapwatch, microsec)=" <<
		std::setw(8) << ((double)latencySysAverCounter / (double)count) << '/' <<
		std::setw(8) << ((double)latencyThisAverCounter / (double)count) <<
		": min,max(wrapwatch)=" <<
		std::setw(8) << latencyThisMin << '/' << std::setw(8) << latencyThisMax <<
		", errors=" <<
		std::setw(8) << errorsCounter.load(std::memory_order::relaxed) << std::endl;

	return ol.str();
	}
