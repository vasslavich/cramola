#include "./endpoints.h"


using namespace crm;
using namespace crm::usecase;


short crm::usecase::port = 16789;
const std::string crm::usecase::localhostAddress = "127.0.0.1";
const crm::identity_descriptor_t crm::usecase::unitID = { crm::global_object_identity_t::as_hash("FAFA0000-0A0A-F1F1-AAAA-100000000003"), "unit" };
const crm::identity_descriptor_t crm::usecase::hubID = { crm::global_object_identity_t::as_hash("FAFA0000-0A0A-F1F1-AAAA-100000000002"), "hub" };
const crm::identity_descriptor_t crm::usecase::hostID = { crm::global_object_identity_t::as_hash("FAFA0000-0A0A-F1F1-AAAA-100000000001"), "host" };

const std::string crm::usecase::hostChannelName = "ex11_host_channel_memory";
const std::string crm::usecase::hubChannelName = "ex11_hub_channel_memory";
const std::string crm::usecase::unitChannelName = "ex11_unit_channel_memory";

