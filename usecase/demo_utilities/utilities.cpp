#include <codecvt>
#include <locale>
#include "./utilities.h"

using namespace crm::usecase;

std::wstring crm::usecase::cvt2wstr(const std::string& s) {
	using convert_type = std::codecvt_utf8<wchar_t>;
	std::wstring_convert<convert_type, wchar_t> converter;
	return converter.from_bytes(s);
	}

std::string crm::usecase::cvt2str(const std::wstring& s) {
	using convert_type = std::codecvt_utf8<wchar_t>;
	std::wstring_convert<convert_type, wchar_t> converter;
	return converter.to_bytes(s);
	}
