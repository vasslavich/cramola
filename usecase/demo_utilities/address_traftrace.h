#pragma once

#include <mutex>
#include <unordered_map>
#include <string>
#include "./counters_stat.h"

namespace crm::usecase {

class address_traffic_trace {
	mutable std::mutex _lck;
	std::unordered_map<std::string, stat_entry> _map;

public:
	void inc(const std::string& address, 
		const std::chrono::high_resolution_clock::time_point& startTP,
		const std::chrono::microseconds& sysLatency) {

		std::lock_guard lock(_lck);
		_map[address].add(startTP, sysLatency);
		}

	void inc(const std::string& address, const crm::dcf_exception_t & e) {
		std::lock_guard lock(_lck);
		_map[address].add_error(e);
		}

	std::vector<std::pair<std::string, std::string>> trace()const noexcept {
		std::vector<std::pair<std::string, std::string>> r;
		r.reserve(_map.size());

		std::lock_guard lock(_lck);
		for (const auto& item : _map) {
			r.emplace_back(item.first, item.second.print());
			}

		return r;
		}
	};
}
