#pragma once

#include <appkit/appkit.h>

namespace crm::usecase{

class outbound_socket_handler_t : public rt0_connection_settings_t{
	static std::atomic<int> _startf;
	std::weak_ptr<crm::distributed_ctx_t> _ctx;
	std::function<void(xpeer_link&&)> _connectionHandler;

public:
	template<typename ConnectionHandler, typename EP, typename H>
	outbound_socket_handler_t(std::weak_ptr<distributed_ctx_t> ctx,
		const identity_descriptor_t& thisId,
		EP&& ep,
		H&& h,
		ConnectionHandler&& ch)
		: crm::rt0_connection_settings_t(thisId, std::forward<EP>(ep), std::forward<H>(h))
		, _ctx(ctx)
		, _connectionHandler(std::forward< ConnectionHandler>(ch)){}

	void connection_handler(std::unique_ptr<crm::dcf_exception_t>&&, xpeer_link&& lnk_)final{
		auto roxAddress = lnk_.address();
		auto roxPort = lnk_.port();
		auto roxId = lnk_.id();
		auto roxName = lnk_.remote_id().name();

		if(_connectionHandler){
			if(auto c = _ctx.lock()){
				c->launch_async(__FILE_LINE__, _connectionHandler, std::move(lnk_));
				}
			}
		}
	};

template<typename EP, typename H, typename ConnectionHandler>
auto make_outbound_socket(std::shared_ptr<router_0_t>& hub,
	std::weak_ptr<distributed_ctx_t> c,
	const identity_descriptor_t& thisId,
	EP&& ep,
	H&& h,
	ConnectionHandler&& ch){

	return hub->create_out_link(outbound_socket_handler_t(
		c,
		thisId,
		std::forward<EP>(ep),
		std::forward<H>(h),
		std::forward<ConnectionHandler>(ch)));
	}
}

