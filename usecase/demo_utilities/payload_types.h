#pragma once

#include <string>
#include <vector>
#include <array>

namespace crm::usecase {

struct serialized_as_aggregate {
	std::string s_value;
	std::vector<uint8_t> vb_value;
	std::array<int, 10> vi_array;
	};
}
