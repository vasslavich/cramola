#pragma once

#include <appkit/appkit.h>
#include <hublon/r0.h>
#include "./outbound_socket_handler.h"

namespace crm::usecase{

class inbound_sockets_hub_optinos : public rt0_hub_settings_t{
public:
	template<typename EP, typename H>
	inbound_sockets_hub_optinos(std::shared_ptr<distributed_ctx_t> ctx,
		EP&& ep,
		const identity_descriptor_t& thisId,
		H&& h)
		: rt0_hub_settings_t(ctx, thisId, std::forward<EP>(ep), std::forward<H>(h)){}
	};

template<typename EP, typename H>
std::shared_ptr<crm::router_0_t> make_inbound_sockets_hub(
	std::shared_ptr<distributed_ctx_t> c,
	const identity_descriptor_t& thisHubId,
	EP&& ep,
	H&& h){

	return crm::router_0_t::create(inbound_sockets_hub_optinos(c,
		std::forward<EP>(ep),
		thisHubId,
		std::forward<H>(h)));
	}
}
