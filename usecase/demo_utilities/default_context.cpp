#include <chrono>
#include "./default_context.h"

using namespace crm;
using namespace crm::usecase;
using namespace std::chrono;

std::shared_ptr<crm::distributed_ctx_t> crm::usecase::make_context(const crm::identity_descriptor_t& hostId,
	std::chrono::seconds pingTimeout, std::chrono::seconds pingInterval) {

	if (pingInterval > pingTimeout)
		throw std::out_of_range("ping interval can't be more the ping timeout");

	crm::distributed_ctx_policies_t policies;
	policies.set_threadpool_maxsize(std::min<size_t>(conncurrency_mode, 2));
	policies.set_threadpool_user_handlers_maxsize(std::min<size_t>(conncurrency_mode, 2));
	policies.set_io_buffer_maxsize(CRM_MB);
	policies.set_io_threads_count(4);

	policies.set_ping_timeline(crm::sndrcv_timeline_periodicity_t::make_cyclic_timeouted(pingTimeout, pingInterval));
	policies.set_rt_ping(true);

	return crm::distributed_ctx_t::make(crm::context_bindings(hostId), std::move(policies));
	}

std::shared_ptr<crm::distributed_ctx_t> crm::usecase::make_context(const crm::identity_descriptor_t& hostId) {
	return make_context(hostId, 300s, 120s);
	}

