#pragma once

#include <vector>
#include <string>
#include <chrono>
#include <array>
#include <algorithm>
#include <appkit/appkit.h>

namespace crm::usecase {

constexpr size_t conncurrency_mode = 1;

std::shared_ptr<crm::distributed_ctx_t> make_context(const crm::identity_descriptor_t& hostId);
std::shared_ptr<crm::distributed_ctx_t> make_context(const crm::identity_descriptor_t& hostId,
	std::chrono::seconds pingTimeout, std::chrono::seconds pingInterval);
}
