#pragma once

#include <appkit/appkit.h>

namespace crm::usecase {

class stat_entry {
private:
	bool watchLatencyMin{ true };
	bool watchLatencyMax{ true };
	size_t stat_step{ 100 };

	size_t count{ 0 };
	double mps{ 0 };
	std::uintmax_t latencySysMax{ 0 };
	std::uintmax_t latencySysMin{ 0 };
	std::atomic<std::uintmax_t> latencySysAverCounter{ 0 };
	std::uintmax_t latencyThisMax{ 0 };
	std::uintmax_t latencyThisMin{ 0 };
	std::atomic<std::uintmax_t> latencyThisAverCounter{ 0 };
	std::atomic<std::uintmax_t> errorsCounter{ 0 };

	using timeline_duration_t = std::chrono::seconds;
	using timeline_line_t = decltype(std::declval<timeline_duration_t>().count());
	std::chrono::high_resolution_clock::time_point _startTP;

	void __add();

	void __add(const std::chrono::high_resolution_clock::time_point& startTP,
		const std::chrono::microseconds& sysLatency);

public:
	stat_entry(size_t averageStep = 100);

	void add();

	void add(const std::chrono::high_resolution_clock::time_point& startTP,
		const std::chrono::microseconds& sysLatency);

	void add_error(const std::exception  & e);

	std::string print()const;
	};
}
