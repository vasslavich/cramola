#pragma once

#include <appkit/appkit.h>
#include "./stream_header_cv.h"

namespace crm::usecase {

struct custom_stream_description : stream_class_description {
	static srlz::typemap_key_t class_type_id()noexcept;
	const  srlz::typemap_key_t& stream_typeid()const noexcept final;
	};

struct custom_stream_params final {
	std::vector<size_t> partitions;
	};

struct custom_stream_source : custom_stream_description {
	std::vector<std::string> _data;

	custom_stream_source(std::vector<std::string>&& data)noexcept;

	void read(const stream_sequence_position_t& offset, size_t blockSize, std::vector<uint8_t>& v);
	bool eof(const stream_sequence_position_t& offset)const noexcept;
	custom_stream_params get_stream_params()const;
	};


struct custom_stream_slot : custom_stream_description {
	using params_type = custom_stream_params;

	custom_stream_params _params;
	stream_header_cv _coverData;
	std::vector<std::string> _data;

public:
	custom_stream_slot(const custom_stream_slot&) = delete;
	custom_stream_slot& operator=(const custom_stream_slot&) = delete;

	custom_stream_slot(custom_stream_slot&&) = default;
	custom_stream_slot& operator=(custom_stream_slot&&) = default;

	custom_stream_slot(std::unique_ptr<i_stream_zyamba_identity_t> h,
		std::unique_ptr<i_stream_zyamba_cover_data_t> cd)noexcept;

	stream_sequence_position_t next_offset()const noexcept;
	stream_sequence_position_t write(const stream_sequence_position_t& offset, std::vector<uint8_t>&& v);
	void close(detail::stream_close_reason_t rc)noexcept;

	stream_header_cv cover_data() && noexcept;
	const stream_header_cv& cover_data()const& noexcept;

	/*
	const std::unique_ptr<i_stream_zyamba_identity_t>& stream_identity()const& noexcept;
	const std::unique_ptr<i_stream_zyamba_cover_data_t>& stream_cover_data()const& noexcept;

	std::unique_ptr<i_stream_zyamba_identity_t> stream_identity() && noexcept;
	std::unique_ptr<i_stream_zyamba_cover_data_t> stream_cover_data() && noexcept;
	*/
	};
}
