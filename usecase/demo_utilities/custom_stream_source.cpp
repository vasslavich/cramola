#include "./custom_stream_source.h"


using namespace crm::usecase;


static_assert(crm::is_stream_reader_from_members_v<custom_stream_source>, __FILE_LINE__);
static_assert(crm::is_stream_slot_based_by_members_v<custom_stream_slot>, __FILE_LINE__);
static_assert(crm::is_stream_slot_v <custom_stream_slot>, __FILE_LINE__);
static_assert(crm::has_type_declaration_stream_params_v< custom_stream_slot>, __FILE_LINE__);
static_assert(crm::srlz::is_serializable_v<crm::has_type_declaration_stream_params<custom_stream_slot>::params_type>, __FILE_LINE__);

custom_stream_slot::custom_stream_slot(
	std::unique_ptr<i_stream_zyamba_identity_t> h,
	std::unique_ptr<i_stream_zyamba_cover_data_t> cd)noexcept{

	if (auto vParams = get_stream_param<custom_stream_params>(*h)) {
		_params = std::move(vParams).value();
		}

	if (auto vCD = get_stream_cover_data<stream_header_cv>(*cd)) {
		_coverData = std::move(vCD).value();
		}
	}

stream_header_cv custom_stream_slot::cover_data() && noexcept {
	return std::move(_coverData);
	}

const stream_header_cv& custom_stream_slot::cover_data()const& noexcept {
	return _coverData;
	}




static const crm::srlz::typemap_key_t customStreamClassId = crm::srlz::typemap_key_t::make<custom_stream_description>();
crm::srlz::typemap_key_t custom_stream_description::class_type_id()noexcept {
	return customStreamClassId;
	}

const  crm::srlz::typemap_key_t& custom_stream_description::stream_typeid()const noexcept {
	return customStreamClassId;
	}




custom_stream_source::custom_stream_source(std::vector<std::string>&& data)noexcept
	: _data(std::move(data)) {}

void custom_stream_source::read(const stream_sequence_position_t& offset,
	size_t /*blockSize*/,
	std::vector<uint8_t>& v) {

	if (crm::stream_position_at<size_t>(offset) < _data.size()) {
		v.clear();
		std::copy(_data.at(crm::stream_position_at<size_t>(offset)).cbegin(),
			_data.at(crm::stream_position_at<size_t>(offset)).cend(),
			std::back_inserter(v));
		}
	}

bool custom_stream_source::eof(const stream_sequence_position_t& offset)const noexcept {
	return crm::stream_position_at<size_t>(offset) >= _data.size();
	}

custom_stream_params custom_stream_source::get_stream_params()const {
	custom_stream_params cp;
	for (const auto& line : _data)cp.partitions.push_back(line.size());
	return cp;
	}





crm::stream_sequence_position_t custom_stream_slot::next_offset()const noexcept {
	return crm::make_stream_postion(_data.size());
	}

crm::stream_sequence_position_t custom_stream_slot::write(
	const stream_sequence_position_t& offset,
	std::vector<uint8_t>&& v) {

	if (_params.partitions.size() > crm::stream_position_at<size_t>( offset)){
		if (v.size() == _params.partitions.at(crm::stream_position_at<size_t>(offset))) {
			std::string s;
			std::copy(v.cbegin(), v.cend(), std::back_inserter(s));
			_data.push_back(std::move(s));

			return next_offset();
			}
		else {
			throw std::exception{ "Wrong stream offset" };
			}
		}
	else {
		if (_params.partitions.size() != crm::stream_position_at<size_t>(offset)
			|| next_offset() != offset) {
			throw std::exception{ "Wrong stream partitioning by user spaced code" };
			}
		else {
			return next_offset();
			}
		}
	}

void custom_stream_slot::close(detail::stream_close_reason_t)noexcept{}



