#include <vector>
#include <string>
#include <chrono>
#include <array>
#include <appkit/appkit.h>
#include <hublon/r0.h>
#include "../../ex_specialization/classes.h"


using namespace std::chrono;


crm::identity_descriptor_t localhostId_RT0 = crm::identity_descriptor_t{ crm::global_object_identity_t::from_str("FAFA0000-0A0A-F1F1-AAAA-100000000001"), "host:0" };
std::string localhostAddress = "127.0.0.1";
short port = 16789;

std::shared_ptr<crm::router_0_t> tbl0;

class outbound_handler_t : public my_mpf::rt0_outconnection_usecase_t {
	std::atomic<int> _startf{ 0 };
	std::weak_ptr<crm::distributed_ctx_t> _ctx;

public:
	outbound_handler_t(std::weak_ptr<crm::distributed_ctx_t> ctx,
		const crm::identity_descriptor_t& thisId,
		std::unique_ptr<crm::rt1_endpoint_t>&& ep,
		std::unique_ptr < crm::i_input_messages_stock_t >&& inputStackCtr)
		: my_mpf::rt0_outconnection_usecase_t(thisId, std::move(ep), std::move(inputStackCtr))
		, _ctx(ctx) {}

	void connection_handler(std::unique_ptr<crm::dcf_exception_t>&&, xpeer_link&& lnk)final {
		auto roxAddress = lnk.address();
		auto roxPort = lnk.port();
		auto roxId = lnk.id();
		auto roxName = lnk.remote_id().name;

		std::cout << "outbound connection:"
			<< roxAddress << ":"
			<< roxPort << ":"
			<< roxId << ":"
			<< roxName << std::endl;

		//send(_ctx, std::move(lnk)/*, crm::sndrcv_timeline_periodicity_t::make_cyclic(2s)*/);

		crm::rpc_ainvoke(std::move(lnk),
			std::string("test_rpc_1:") + roxName,
			crm::function_descriptor{ "test_rpc_1" }, 
			[](crm::message_handler&& rd) {
			
			if (rd.has_values()) {
				auto [l, s] = std::move(rd).as_values<long, std::string>();
				std::cout << "out:  " << l << ":" << s << std::endl;

				rd.commit_receive();
				}
			else if (rd.has_error()) {
				std::wcout << L"out:  " << rd.error()->msg() << std::endl;
				}
			else {
				std::cout << "out:  undefined result" << std::endl;
				}
			},

			(long)0,
			std::string{ "s" });
		}
	};


class all2all_custom_hub_t : public my_mpf::rt0_hub_usecase_t {
public:
	all2all_custom_hub_t(const std::shared_ptr<crm::distributed_ctx_t>& ctx,
		std::unique_ptr<crm::i_endpoint_t>&& ep,
		const crm::identity_descriptor_t& thisId,
		std::shared_ptr<crm::i_xpeer_source_factory_t> ipeerFactory,
		std::shared_ptr<crm::i_message_input_t<crm::message_stamp_t>> inputStack,
		std::shared_ptr<crm::default_binary_protocol_handler_t> ph)
		: my_mpf::rt0_hub_usecase_t(ctx, std::move(ep), thisId, ipeerFactory, inputStack, ph)
		, _optb(std::make_shared< otable>())
		, _optbReversed(std::make_shared< otable>())
		, _ctx(ctx) {}

private:
	class otable {
	public:
		std::map<crm::ipeer_l0::remote_object_endpoint_t, crm::opeer_l0> _opx;
		std::mutex _opxLock;
		};
	std::shared_ptr< otable> _optb;
	std::shared_ptr< otable> _optbReversed;
	std::weak_ptr<crm::distributed_ctx_t> _ctx;

	crm::datagram_t remote_object_initialize(crm::syncdata_list_t&& /*dt*/)final {
		return crm::datagram_t{};
		}

	void local_object_initialize(crm::datagram_t&& /*dt*/,
		crm::detail::object_initialize_result_guard_t&& /*resultHndl*/)final {
		return;
		}

	void ipeer_connection(crm::ipeer_l0&& xpeer, crm::datagram_t&& /*initializeResult*/)final {
		crm::rpc_ainvoke(std::move(xpeer),
			std::string("test_rpc_2:") + xpeer.remote_id().name,
			crm::function_descriptor{ "test_rpc_2" },
			[](crm::message_handler&& rd) {
			
			if (rd.has_values()) {
				auto [l, s] = std::move(rd).as_values<int, std::wstring>();
				std::wcout << L"in:  " << l << L":" << s << std::endl;

				rd.commit_receive();
				}
			else if (rd.has_error()) {
				std::wcout << L"in:  " << rd.error()->msg() << std::endl;
				}
			else {
				std::wcout << L"in:  undefined result" << std::endl;
				}
			},

			(int)0,
			std::wstring{ L"ws" });
		}

	void ipeer_disconnection(const crm::ipeer_l0::xpeer_desc_t& /*desc*/, std::shared_ptr<crm::i_peer_statevalue_t>&&)final {
		return;
		}

	bool remote_object_identity_validate(const crm::identity_value_t& /*idSet*/,
		std::unique_ptr<crm::i_peer_remote_properties_t>& /*prop*/)final {

		return true;
		}

public:
	void begin_outbound_connection() {
		auto rdp = crm::rt1_endpoint_t::make(localhostAddress, port, localhostId_RT0);

		auto outboundH = outbound_handler_t(
			ctx(),
			localhostId_RT0,
			std::make_unique<crm::rt1_endpoint_t>(rdp),
			std::make_unique<crm::opeer_l0_input2handler_t>([rdp, wptr = std::weak_ptr<otable>(_optb)](crm::opeer_l0::input_value_t&& m) {

			//push_opeer_message(wptr, rdp, std::move(m));
			auto xp = crm::get_link(m);
			//input_handler(xp, std::move(m));
			}));

		auto xpeer1 = tbl0->create_out_link(std::move(outboundH));

		_optb->_opx.insert({ rdp, std::move(xpeer1) });
		}
	};

std::shared_ptr<crm::distributed_ctx_t> make_context( const crm::identity_descriptor_t& hostId,
	bool ping) {

	crm::distributed_ctx_policies_t policies;
	policies.set_threadpool_maxsize(2);
	policies.set_threadpool_user_handlers_maxsize(2);
	policies.set_io_buffer_maxsize(1024);
	policies.set_io_threads_count(1);

	policies.set_ping_timeline(crm::sndrcv_timeline_periodicity_t::make_cyclic_timeouted(300s, 120s));
	policies.set_rt_ping(ping);

	return crm::distributed_ctx_t::make(crm::context_bindings(hostId), std::move(policies));
	}

int main() {

	auto ctx0 = make_context(localhostId_RT0, true);

	/* ������� ����������� */
	auto sourceFactory = crm::detail::tcp::tcp_source_factory_t::create(ctx0);

	/* �������� ����� ��� �������� ����������� */
	auto ep(std::make_unique<crm::detail::tcp::tcp_endpoint_t>(port, crm::detail::tcp::tcp_endpoint_t::version_t::v4));

	auto protocol = my_mpf::initialize_protocol(ctx0);

	auto rt0Settings = all2all_custom_hub_t(ctx0,
		std::move(ep),
		localhostId_RT0,
		input_message_knop_l0[](crm::input_message_knop_l0&&) {
		std::cout << "!" << std::endl;
		//input_handler(m.link(), std::move(m));
		});

	tbl0 = crm::router_0_t::create(rt0Settings);

	ctx0->register_handler({ "test_rpc_1" }, [](long l, std::string&& s) {
		return std::make_tuple(std::move(l), std::move(s));
		});
	ctx0->register_handler({"test_rpc_2"}, [](int l, std::wstring&& s) {
		return std::make_tuple(std::move(l),std::move( s));
		});

	ctx0->start();

	rt0Settings->begin_outbound_connection();


	while (true) {
		std::this_thread::sleep_for(10s);
		}

	return 0;
}
