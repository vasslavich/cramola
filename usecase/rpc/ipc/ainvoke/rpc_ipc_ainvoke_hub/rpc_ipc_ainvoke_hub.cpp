﻿#include <iostream>
#include <hublon/r1.h>
#include "../../../../demo_utilities/endpoints.h"
#include "../../../../demo_utilities/default_context.h"
#include "../../../../demo_utilities/address_traftrace.h"
#include "../../../../demo_utilities/payload_types.h"
#include "../../../../demo_utilities/utilities.h"

using namespace crm;
using namespace crm::usecase;


std::shared_ptr< detail::ipc_unit> hub;
outbound_pinned_links outbound_stats;
address_traffic_trace stgl;

template<typename TIO>
void rpc_demo_reverse_call(TIO peer, serialized_as_aggregate arg) {
	auto timepoint = std::chrono::high_resolution_clock::now();
	arg.s_value = "request";

	crm::rpc_ainvoke(peer,
		crm::launch_as::async,
		"call_unit_rpc_1",
		crm::function_descriptor{ "call_unit_rpc_1" },
		[arg, timepoint, peer](serialized_as_aggregate responseValue, std::unique_ptr<crm::dcf_exception_t>&& e, std::chrono::microseconds mcs) {

		if (responseValue.s_value == "response" && !e) {
			stgl.inc(peer.address(), timepoint, mcs);
			rpc_demo_reverse_call(peer, arg);
			}
		else {
			stgl.inc(peer.address(), e ? (*e) : crm::dcf_exception_t{});
			}
		},
		arg);
	}

class outbound_handler_t : public rt1_connection_settings_t {
public:
	outbound_handler_t(const crm::identity_descriptor_t& thisId,
		std::unique_ptr<crm::rt1_endpoint_t>&& ep,
		std::function<void(crm::input_message_knop_l1&&)>&& mh)
		: rt1_connection_settings_t(thisId, std::move(ep), std::move(mh)){}

	void connection_handler(std::unique_ptr<crm::dcf_exception_t>&& , xpeer_link&& x )final {
		rpc_demo_reverse_call(std::move(x), {});
		}
	};

class all2all_custom_hub_t : public ipc_unit_options {
	void connection_handler(crm::ipeer_t&& rox, crm::datagram_t&& )final {
		auto rdp = crm::ipeer_t::remote_object_endpoint_t::make_remote(rox.address(), port, rox.id());
		outbound_stats.insert(std::move(rdp), hub->make_outbound(outbound_handler_t(
			hubID,
			std::make_unique<crm::rt1_endpoint_t>(rdp),
			[](crm::input_message_knop_l1&& ) {})));
		}

public:
	all2all_custom_hub_t(const crm::detail::xchannel_shmem_hub_options& channelOpt)
		: ipc_unit_options(channelOpt) {}
	};


int main(){
	auto ctx = make_context(hubID);

	crm::detail::xchannel_shmem_hub_options thisChannelOpt;
	thisChannelOpt.mainNodeName = hubChannelName;

	all2all_custom_hub_t opt(thisChannelOpt);

	detail::xchannel_shmem_host_args a;
	a.xchannelName.mainNodeName = hostChannelName;
	opt.set_host_channel(a);
	opt.set_this_id(hubID);
	opt.set_input_stack([](crm::ipeer_t&&, crm::input_message_knop_l1&&){});

	hub = detail::ipc_unit::make(ctx, std::move(opt));
	ctx->register_handler({"call_hub_rpc_1"}, [](serialized_as_aggregate a){
		if(a.s_value == "request"){
			auto r = a;
			r.s_value = "response";
			return r;
			} else{
			THROW_EXC_FWD("input argument hasn't marked as request");
			}
		});

	ipc_unit_start_options startopt;
	startopt.message_mem_size = CRM_MB * 32;
	startopt.command_mem_size = CRM_MB;

	bool fgop{false};
	while(!fgop){
		try{
			hub->start(startopt);
			fgop = true;
			}
		catch(const std::exception& e){
			std::cout << e.what() << ", wait 2 sec..." << std::endl;
			std::this_thread::sleep_for(std::chrono::seconds(2));
			}
		}

	while(true){
		std::this_thread::sleep_for(std::chrono::seconds(5));

		std::cout << "-------------   hub stats   ----------------------" << std::endl;
		for(const auto& i : stgl.trace()){
			std::cout << i.second << std::endl;
			}

		std::cout << "--------------------------------------------------" << std::endl;
		}

	std::cout << "press any key..." << std::endl;
	getchar();

	hub->close();
	}
