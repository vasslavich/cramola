﻿#include <iostream>
#include <experimental/resumable>
#include <hublon/r1.h>
#include "../../../../demo_utilities/endpoints.h"
#include "../../../../demo_utilities/default_context.h"
#include "../../../../demo_utilities/address_traftrace.h"
#include "../../../../demo_utilities/payload_types.h"
#include "../../../../demo_utilities/utilities.h"

using namespace crm;
using namespace crm::usecase;

std::shared_ptr< detail::ipc_unit> hub;
outbound_pinned_links outbound_stats;
address_traffic_trace stgl;

class outbound_handler_t : public rt1_connection_settings_t {
	template<typename Tio>
	static std::future<void> invoke_resumable(Tio io) {
		auto addr = io.address();

		do {
			auto timepoint = std::chrono::high_resolution_clock::now();
			auto st = co_await crm::rpc_coro_invoke(io,
				"call_hub_rpc_1",
				crm::function_descriptor{ "call_hub_rpc_1" },
				serialized_as_aggregate{ "request" });

			auto [l] = crm::rpc_get_result(std::move(st)).as<serialized_as_aggregate>();
			if (l.s_value == "response") {
				stgl.inc(addr, timepoint, st.timeline());
				}
			else {
				stgl.inc(addr, CREATE_PTR_EXC_FWD(nullptr));
				}
			} while (io.is_opened());
		}

public:
	outbound_handler_t(const crm::identity_descriptor_t& thisId,
		std::unique_ptr<crm::rt1_endpoint_t>&& ep,
		std::function<void(crm::input_message_knop_l1&&)>&& mh)
		: rt1_connection_settings_t(thisId, std::move(ep), std::move(mh)) {}

	void connection_handler(std::unique_ptr<crm::dcf_exception_t>&& e, xpeer_link&& xpeer_)final {
		invoke_resumable(xpeer_);
		}
	};

class all2all_custom_hub_t : public crm::ipc_unit_options {
public:
	all2all_custom_hub_t(crm::detail::xchannel_shmem_hub_options&& opt)
		: crm::ipc_unit_options(std::move(opt)) {}
	};

std::map<crm::ipeer_t::remote_object_endpoint_t, crm::opeer_t> outboundHandls;
void start_outbound_connections() {
	auto rdp = crm::ipeer_t::remote_object_endpoint_t::make_remote(localhostAddress, port, hubID);
	outboundHandls[rdp] = hub->make_outbound(outbound_handler_t(
		unitID,
		std::make_unique<crm::rt1_endpoint_t>(rdp),
		[rdp](crm::input_message_knop_l1&& m) {}));
	}

int main() {
	auto c = make_context(unitID);

	crm::detail::xchannel_shmem_hub_options thisChannelOpt;
	thisChannelOpt.mainNodeName = unitChannelName;

	all2all_custom_hub_t opt(std::move(thisChannelOpt));

	detail::xchannel_shmem_host_args a;
	a.xchannelName.mainNodeName = hostChannelName;
	opt.set_host_channel(a);
	opt.set_this_id(unitID);
	opt.set_input_stack([](crm::ipeer_t&& lnk, crm::input_message_knop_l1&& m) {});

	hub = detail::ipc_unit::make(c, std::move(opt));
	c->register_handler({ "call_unit_rpc_1" }, [](serialized_as_aggregate a) {
		if (a.s_value == "request") {
			auto r = a;
			r.s_value = "response";
			return r;
			}
		else {
			THROW_EXC_FWD("input argument hasn't marked as request");
			}
		});

	ipc_unit_start_options startopt;
	startopt.message_mem_size = CRM_MB * 32;
	startopt.command_mem_size = CRM_MB;

	hub->start(startopt);
	start_outbound_connections();

	while (true) {
		std::this_thread::sleep_for(std::chrono::seconds(5));

		std::cout << "-------------   hub stats   ----------------------" << std::endl;
		for (const auto& i : stgl.trace()) {
			std::cout << i.second << std::endl;
			}

		std::cout << "--------------------------------------------------" << std::endl;

		}

	std::cout << "press any key..." << std::endl;
	getchar();

	hub->close();
	}