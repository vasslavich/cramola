#include <vector>
#include <string>
#include <chrono>
#include <array>
#include <appkit/appkit.h>
#include <hublon/r0.h>
#include <hublon/r1.h>
#include "../ex_specialization/classes.h"


using namespace std::chrono;


const auto localhostId_RT0 = crm::identity_descriptor_t{ crm::global_object_identity_t::as_hash( "FAFA0000-0A0A-F1F1-AAAA-100000000001" ), "host:0" };
const auto localhostId_RT1 = crm::identity_descriptor_t{ crm::global_object_identity_t::as_hash( "this is rt1 host" ), "host:1" };
const auto localhostAddress = "127.0.0.1";
short port = 16789;


std::atomic<std::uintmax_t> _sndCounter{ 0 };
std::atomic<std::uintmax_t> _rcvCounter{ 0 };
std::atomic<std::uintmax_t> _excCounter{ 0 };

static constexpr auto freqExcCollectPrint = 10;
size_t payloadSize = 64;

#ifdef CBL_MPF_TEST_ERROR_EMULATION
constexpr size_t stat_step = 10;
constexpr size_t print_step = 20;
#else
constexpr size_t stat_step = 100;
constexpr size_t print_step = 200;
#endif

enum class message_cathegory {
	lower_bound,
	outcome_request_opeer,
	outcome_request_ipeer,
	upper_bound,
	__count
	};

const char* catname(message_cathegory c) {
	switch(c) {
		case message_cathegory::lower_bound:
			return "upper bound(serialize required)";
		case message_cathegory::outcome_request_opeer:
			return "outcome_request_opeer";
		case message_cathegory::outcome_request_ipeer:
			return "outcome_request_ipeer";
		case message_cathegory::upper_bound:
			return "upper_bound";
		case message_cathegory::__count:
			return "";
		default:
			FATAL_ERROR_FWD(nullptr);
		}
	}

struct custom_message_strob_1 {
	std::vector<std::uint8_t> blob;
	std::string key;
	message_cathegory cat;
	};

struct statistics {
	size_t payload;
	size_t serialized;
	};

size_t payload_size(const custom_message_strob_1 & v)noexcept {
	return v.blob.size() * sizeof(std::decay_t<decltype(v.blob[0])>) + v.key.size() * sizeof(std::decay_t<decltype(v.key[0])>);
	}

template<typename T>
statistics get_stat(const T & v)noexcept {
	return statistics{ payload_size(v) , crm::srlz::object_trait_serialized_size(v) };
	}




template< class, class = std::void_t<> >
struct is_blob_type : std::false_type {};

template< class T >
struct is_blob_type<T, std::void_t<decltype(std::declval<T>().data())>> : std::true_type {};

template<class T>
bool constexpr is_blob_type_v = is_blob_type<T>::value;




template<typename _TBlob,
	typename TBlob = typename std::decay_t<_TBlob>,
	typename std::enable_if_t<is_blob_type_v<TBlob> && !crm::srlz::detail::is_string_type_v<TBlob>, int> = 0>
typename TBlob initialize(size_t count) {
	TBlob v;
	v.resize(count);

	size_t ib = 0;
	while(ib < count) {
		auto u16 = crm::sx_uuid_t::rand();
		for(size_t i = 0; i < crm::sx_uuid_t::count_of_as<decltype(v[0])>() && ib < count; ++i, ++ib) {
			v[ib] = u16.at<decltype(v[0])>(i);
			}
		}

	return v;
	}


template<typename _TString,
	typename TString = typename std::decay_t<_TString>,
	typename std::enable_if_t<crm::srlz::detail::is_string_type_v<TString>,int> = 0>
typename TString initialize(size_t count) {
	TString v;
	v.resize(count);

	size_t ib = 0;
	while(ib < count) {
		auto rs = crm::sx_uuid_t::rand().to_string<TString::value_type>();
		for(size_t i = 0; i < rs.size() && ib < count; ++i, ++ib) {
			v[ib] = rs[i];
			}
		}

	return v;
	}


void initialize(custom_message_strob_1 & v, size_t payload) {
	if(payload) {
		auto ksize = std::max((payload / 10), 1ull);
		auto bsize = std::max((payload - ksize), 1ull);

		v.blob = initialize<decltype(v.blob)>(bsize);
		v.key = initialize<decltype(v.key)>(ksize);
		}
	}

std::shared_ptr<crm::distributed_ctx_t> make_context(const crm::identity_descriptor_t & hostId, 
	bool ping) {
	
	crm::distributed_ctx_policies_t policies;
	policies.set_threadpool_maxsize(2);
	policies.set_threadpool_user_handlers_maxsize(2);
	policies.set_io_buffer_maxsize(1024);
	policies.set_io_threads_count(1);

	policies.set_ping_timeline(crm::sndrcv_timeline_periodicity_t::make_cyclic_timeouted(60s, 10s));
	policies.set_rt_ping(ping);

	return crm::distributed_ctx_t::make(crm::context_bindings(hostId), std::move(policies));
	}

std::shared_ptr<crm::router_1> tbl1;


void print(std::list<crm::detail::exceptions_trace_collector::item_value> && l) {
	if(!l.empty()) {
		std::wostringstream os;
		for(auto && i : l) {
			os << L"code=" << i.code << L", num=" << i.count << L", m=" << i.message << L", file=" << i.file << L", line=" << i.line << std::endl;
			}

		crm::file_logger_t::logging(os.str(), 99998888);
		}
	}



struct stat_entry {
	mutable std::mutex _lck;
	size_t count{ 0 };
	size_t payload_count{ 0 };
	size_t serialized_count{ 0 };
	double mps{ 0 };
	std::chrono::microseconds latencySysMax{ 0 };
	std::chrono::microseconds latencySysMin{ 0 };
	std::chrono::microseconds latencySysAver{ 0 };
	std::chrono::microseconds latencyThisMax{ 0 };
	std::chrono::microseconds latencyThisMin{ 0 };
	std::chrono::microseconds latencyThisAver{ 0 };

	using timeline_duration_t = std::chrono::seconds;
	using timeline_line_t = decltype(std::declval<timeline_duration_t>().count());
	timeline_line_t timeline{ 0 };

	stat_entry()
		: timeline{ std::chrono::duration_cast<timeline_duration_t>(std::chrono::system_clock::now().time_since_epoch()).count() } {}

	void __add(const custom_message_strob_1 & v) {
		const auto st = get_stat(v);

		payload_count += st.payload;
		serialized_count += st.serialized;

		const auto c_ = ++count;
		if(c_ >= stat_step && (c_ % stat_step) == 0) {
			auto currTL = std::chrono::duration_cast<timeline_duration_t>(std::chrono::system_clock::now().time_since_epoch()).count();
			auto tl = currTL - timeline;

			if (tl != 0) {
				mps = (double)c_ / (double)tl;
				}
			}
		}

	void __add(const std::chrono::high_resolution_clock::time_point & startTP,
		const std::chrono::microseconds & sysLatency) {

		auto thisLatency = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - startTP);

		if(!latencyThisMin.count()) {
			latencyThisMin = thisLatency;
			}
		else if(thisLatency < latencyThisMin) {
			latencyThisMin = thisLatency;
			}
	
		if(thisLatency > latencyThisMax) {
			latencyThisMax = thisLatency;
			}

		if(latencyThisAver.count()) {
			latencyThisAver = (latencyThisAver + thisLatency) / 2;
			}
		else {
			latencyThisAver = thisLatency;
			}


		if(!latencySysMin.count()) {
			latencySysMin = sysLatency;
			}
		else if(sysLatency < latencySysMin) {
			latencySysMin = sysLatency;
			}

		if(sysLatency > latencySysMax) {
			latencySysMax = sysLatency;
			}

		if(latencySysAver.count()) {
			latencySysAver = (latencySysAver + sysLatency) / 2;
			}
		else {
			latencySysAver = sysLatency;
			}
		}

	void add(const custom_message_strob_1 & v) {
		std::lock_guard<std::mutex> lck(_lck);
		__add(v);
		}

	void add(const custom_message_strob_1 & v,
		const std::chrono::high_resolution_clock::time_point & startTP,
		const std::chrono::microseconds & sysLatency) {

		std::lock_guard<std::mutex> lck(_lck);
		__add(v);
		__add(startTP, sysLatency);
		}

	std::string print()const {
		std::lock_guard<std::mutex> lck(_lck);

		std::ostringstream ol;
		ol <<
			"count=" << std::setw(6) << count <<
			", mps=" << std::setw(6) << (size_t)std::trunc(mps) <<
			", latency(sys/there, microsec)=" << std::setw(8) << latencySysAver.count() << ":" << std::setw(8) << latencyThisAver.count() <<
			", payload(bytes)=" << std::setw(10) << payload_count <<
			", payload/serialized=" << std::setw(6) << (double)payload_count / (double)serialized_count;

		return ol.str();
		}
	};

class stat_lenta {
	std::array<stat_entry, ((int)message_cathegory::__count + 1)> _lenta;
	std::atomic<size_t> addcnt{ 0 };

	void add_value(const custom_message_strob_1 & v) {

		const auto idx = static_cast<int>(v.cat);
		_lenta.at(idx).add(v);
		}

	void add_value(const custom_message_strob_1 & v,
		const std::chrono::high_resolution_clock::time_point & startTP,
		const std::chrono::microseconds & sysLatency) {

		const auto idx = static_cast<int>(v.cat);
		_lenta.at(idx).add(v, startTP, sysLatency);
		}

	void add_post() {
		if((++addcnt % print_step) == 1) {
			for(auto ic = (int)message_cathegory::outcome_request_opeer; ic < (int)message_cathegory::upper_bound; ++ic) {
				auto c = static_cast<message_cathegory>(ic);
				std::cout << std::setw(20) << catname(c) << " >> " << _lenta.at(ic).print() << std::endl;
				}
			}
		}

public:
	void add(const custom_message_strob_1 & v) {
		add_value(v);
		add_post();
		}

	void add(const custom_message_strob_1 & v,
		const std::chrono::high_resolution_clock::time_point & startTP,
		const std::chrono::microseconds & sysLatency) {

		add_value(v, startTP, sysLatency);
		add_post();
		}
	};

stat_lenta stgl;

template<typename TRox>
void receive_stat(TRox && /*rox*/, custom_message_strob_1 && roxResponse,
	const std::chrono::high_resolution_clock::time_point & startTP,
	const std::chrono::microseconds & sysLatency) {

	stgl.add(roxResponse, startTP, sysLatency);
	}

template<typename TRox>
void receive_stat(TRox && /*rox*/, custom_message_strob_1 && roxResponse) {

	stgl.add(roxResponse);
	}

template<typename TRox>
void recv_response(std::weak_ptr<crm::distributed_ctx_t> ctx, 
	TRox && xpeer,
	custom_message_strob_1 && roxResponse,
	/*crm::sndrcv_timeline_periodicity_t tp,*/
	const std::chrono::high_resolution_clock::time_point & startTP,
	const std::chrono::microseconds & sysLatency) {

	receive_stat(xpeer, std::move(roxResponse), startTP, sysLatency);
	send(ctx, crm::detail::make_invokable_xpeer(std::forward<TRox>(xpeer))/*, tp*/);
	}

std::atomic<std::intmax_t> sndrcvCollisions{ 0 };

template<typename TRox>
void send(std::weak_ptr<crm::distributed_ctx_t> ctx,
	TRox&& xpeer/*,
	crm::sndrcv_timeline_periodicity_t tp*/) {

	custom_message_strob_1 m;
	initialize(m, payloadSize);

	if (xpeer.direction() == crm::xpeer_direction_t::in) {
		m.cat = message_cathegory::outcome_request_ipeer;
		}
	else if (xpeer.direction() == crm::xpeer_direction_t::out) {
		m.cat = message_cathegory::outcome_request_opeer;
		}
	else {
		FATAL_ERROR_FWD(nullptr);
		}

	auto next = crm::make_scoped_execution([xpeer] {
		bool ok{ false };
		do {
			if (auto ctx = tbl1->context()) {
				if (xpeer.is_opened()) {
					try {
						send(ctx, xpeer);
						ok = true;
						}
					catch (const std::exception & e0) {
						ctx->exc_hndl(e0);
						}
					catch (...) {
						ctx->exc_hndl(CREATE_PTR_EXC_FWD(nullptr));
						}
					}
				else {
					return;
					}
				}
			}
		while (!ok);
		});

	auto decCtx = crm::detail::post_scope_action_t([] {
		--sndrcvCollisions;
		});
	++sndrcvCollisions;

	auto rh = [n = std::move(next), ctx, xpeer, timepoint = std::chrono::high_resolution_clock::now()]
	(custom_message_strob_1&& obj,
		std::unique_ptr<crm::dcf_exception_t>&& exc_,
		const std::chrono::microseconds & mcs_)mutable{

		++_sndCounter;

#ifdef CBL_SNDRCV_TRACE_COLLECT
		if(exc_) {
			auto excIdx = ++_excCounter;
			if(0 == (excIdx % freqExcCollectPrint)) {
				print(crm::detail::exceptions_trace_collector::instance().list());
				}
			}
#endif

		if(!exc_) {
			recv_response(ctx, xpeer, std::move(obj), /*tp,*/ timepoint, mcs_);

			n.reset();
			}
		else {
			n.execute();
			}
		};

	crm::ainvoke(
		ctx,
		xpeer,
		std::move(m),
		std::move(rh),
		std::string("snd-example:") + typeid(TRox).name(),
		crm::sndrcv_timeline_t::make_once_call(),
		crm::launch_as::async );
	}

class outbound_handler_t : public crm::rt1_connection_settings_t {
	std::weak_ptr<crm::distributed_ctx_t> _ctx;
	std::chrono::system_clock::time_point _createTP = std::chrono::system_clock::now();

public:
	outbound_handler_t(std::weak_ptr<crm::distributed_ctx_t> ctx,
		const crm::identity_descriptor_t & thisId,
		std::unique_ptr<crm::rt1_endpoint_t> && ep,
		std::function<void(crm::input_message_knop_l1 && m)>&& mh )
		: crm::rt1_connection_settings_t(thisId, std::move(ep), std::move(mh))
		, _ctx(ctx){}

	bool connnection_exception_suppress_checker(crm::detail::connection_stage_t,
		const std::unique_ptr<crm::dcf_exception_t>&)noexcept final {

		return true;
		}

	void connection_handler(std::unique_ptr<crm::dcf_exception_t> && n, xpeer_link&& lnk)final {
		if (lnk.is_opened()) {
			auto roxAddress = lnk.address();
			auto roxPort = lnk.port();
			auto roxId = lnk.id();
			auto roxName = lnk.desc().source_id().name();

			std::cout << "outbound connection:"
				<< roxAddress << ":"
				<< roxPort << ":"
				<< roxId << ":"
				<< roxName << std::endl;

			send(_ctx, std::move(lnk)/*, crm::sndrcv_timeline_periodicity_t::make_cyclic(2s)*/);
			}
		else if (n) {
			std::cout << "outbound connection attempt with error: " << crm::cvt(n->msg()) << std::endl;
			}
		}
	};

template<typename XIO>
void input_handler(XIO && lnk,
	crm::detail::i_xpeer_rt1_t::input_value_t && message) {

	auto m2 = crm::message_forward<custom_message_strob_1>(std::move(message).m());
	if(m2) {
		auto v = std::move(m2).value();
		auto cat = v.value().cat;

		receive_stat(lnk, std::move(v).value());

		custom_message_strob_1 m;
		initialize(m, payloadSize);
		m.cat = cat;

		crm::detail::ostrob_message_envelop_t<custom_message_strob_1> response(std::move(m));
		response.capture_response_tail((*v.source()));
		
		lnk.push(std::move(response));
		v.commit();
		}
	}


class all2all_custom_hub_t : public crm::rt1_hub_settings_t {
public:
	template<typename H>
	all2all_custom_hub_t(const std::shared_ptr<crm::distributed_ctx_t>& ctx,
		const crm::identity_descriptor_t& thisId,
		H&& h)
		: crm::rt1_hub_settings_t(ctx, thisId, std::forward<H>(h))
		, _optb(std::make_shared< otable>())
		, _ctx(ctx) {
		static_assert(crm::is_rt1_hub_input_handler_v<H>, __FILE_LINE__);
		}

private:
	class otable {
	public:
		std::map<crm::ipeer_t::remote_object_endpoint_t, crm::opeer_t> _opx;
		std::mutex _opxLock;
		};
	std::shared_ptr< otable> _optb;
	std::weak_ptr<crm::distributed_ctx_t> _ctx;

	static void push_opeer_message(std::weak_ptr<otable> optb, const crm::ipeer_t::remote_object_endpoint_t& rdp,
		crm::opeer_t::input_value_t&& m) {

		if(auto pt = optb.lock()) {
			std::unique_lock lck(pt->_opxLock);
			auto it = pt->_opx.find(rdp);
			if(it != pt->_opx.cend()) {
				input_handler((*it).second, std::move(m));
				}
			}
		}

	void validate_reverse_connect(const crm::ipeer_t& rox) {
		auto rdp = crm::ipeer_t::remote_object_endpoint_t::make_remote(rox.address(), port, rox.remote_id());

		std::unique_lock lck(_optb->_opxLock);
		if (!_optb->_opx.contains(rdp)) {
			auto xpeer1 = tbl1->create_out_link(outbound_handler_t(
				_ctx,
				localhostId_RT1,
				std::make_unique<crm::rt1_endpoint_t>(rdp),
				[rdp, wptr = std::weak_ptr<otable>(_optb)](crm::opeer_t::input_value_t&& m) {

				push_opeer_message(wptr, rdp, std::move(m));
				}));

			//send(_ctx, crm::detail::make_invokable_xpeer(xpeer1)/*, crm::sndrcv_timeline_periodicity_t::make_cyclic(2s)*/);

			_optb->_opx[rdp] = std::move(xpeer1);
			lck.unlock();

			auto roxAddress = rox.address();
			auto roxPort = rox.port();
			auto roxId = rox.id();
			auto roxName = rox.desc().source_id().name();

			std::cout << "inbound connection:"
				<< roxAddress << ":"
				<< roxPort << ":"
				<< roxId << ":"
				<< roxName << std::endl;
			}
		}

	void connection_handler(crm::ipeer_t&& xpeer, crm::datagram_t&& /*initializeResult*/)final {
		validate_reverse_connect(xpeer);
		send(_ctx, std::move(xpeer)/*, crm::sndrcv_timeline_periodicity_t::make_cyclic(2s)*/);
		}

	bool remote_object_identity_validate(const crm::identity_value_t& /*idSet*/,
		std::unique_ptr<crm::i_peer_remote_properties_t>& /*prop*/)final {

		return true;
		}

	void event_handler(std::unique_ptr<crm::i_event_t>&& /*ev*/)final {}

public:
	std::function<void()> make_outbound_connection()const {
		return[wotbl = std::weak_ptr<otable>(_optb), c = ctx()]{
			auto rdp = crm::rt1_endpoint_t::make_remote(localhostAddress, port, localhostId_RT1);
			auto outboundH = outbound_handler_t(
				c,
				localhostId_RT1,
				std::make_unique<crm::rt1_endpoint_t>(rdp),
				[rdp, wotbl](crm::opeer_t::input_value_t&& m) {

				push_opeer_message(wotbl, rdp, std::move(m));
				});

			auto xpeer1 = tbl1->create_out_link(std::move(outboundH));

			if(auto t = wotbl.lock()) {
				t->_opx[ rdp ]= std::move(xpeer1);
				}
			};
		}
	};


int main() {

	auto ctx0 = make_context(localhostId_RT0, true);
	auto ctx1 = make_context(localhostId_RT1, false);

	auto tbl0 = my_mpf::initialize_rt0( port, 
		localhostId_RT0,
		ctx0, 
		[](crm::input_message_knop_l0&& m0){});

	crm::channel_info_t chDesc;
	chDesc.set_id( ctx1->make_channel_identity() );
	auto chOpt = crm::channel_common_memory_config(chDesc, tbl0->rt0_channel_router());
	auto t0_1_queue = crm::make_channel_cm_queue(ctx1, chOpt );
	tbl0->register_rt1_channel(t0_1_queue);

	auto rt1Hub = all2all_custom_hub_t(ctx1,
		localhostId_RT1,
		[](crm::ipeer_t&& lnk, crm::input_message_knop_l1&& m) {
		input_handler<crm::ipeer_t>(std::move(lnk), std::move(m));
		});
	auto outboundMakef = rt1Hub.make_outbound_connection();

	tbl1 = crm::router_1::create(t0_1_queue, std::move(rt1Hub));

	ctx1->register_message_type<custom_message_strob_1>();

	ctx0->start();
	ctx1->start();

	tbl1->open();
	outboundMakef();
	

	while(true) {
		std::this_thread::sleep_for(10s);
		std::cout << "sndrcvCollisions= >>>> boom! = " << sndrcvCollisions << std::endl;
		}

	return 0;
	}
