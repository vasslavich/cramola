#include <appkit/appkit.h>


union u2 {
	std::size_t s;
	double d;
	std::uint8_t u8[sizeof(std::size_t)];
	};
bool operator==(const u2& l, const u2& r) {
	return l.s == r.s && l.d == r.d;
	}
bool operator!=(const u2& l, const u2& r) {
	return !(l == r);
	}

/*! make random value of u2 */
void make_rand(u2 & v) {
	v.s = crm::sx_uuid_t::rand().at<size_t>(0);
	}

/*! random initialize std::array of integrals */
template<typename T, const size_t N = 8, 
	typename std::enable_if_t<std::is_integral_v<std::decay_t<T>>, int> = 0>
void make_rand(std::array<T, N> & a) {
	for(size_t i = 0; i < N; ++i) {
		a[i] = crm::sx_uuid_t::rand().at<T>(0);
		}
	}

/*! random initialize std::array of std::string's initialized values */
template<typename T, const size_t N = 8, 
	typename std::enable_if_t<std::is_convertible_v<std::string, std::decay_t<T>>, int> = 0>
void make_rand(std::array<T, N>& a) {
	for(size_t i = 0; i < N; ++i) {
		a[i] = crm::sx_uuid_t::rand().to_str();
		}
	}

/*! random initialize std::array of make_rand's initialized */
template<typename T, const size_t N = 8,
	typename _T = typename std::decay_t<T>,
	typename  = std::void_t<decltype(make_rand(std::declval<_T&>()))>>
void make_rand(std::array<T, N>& a) {
	for(size_t i = 0; i < N; ++i) {
		make_rand(a[i]);
		}
	}


/*! struct with std::array member */
template<typename T, const int N = 8>
struct s_carray_0 {
	int size()const noexcept { return N; }
	std::array<T,N> ca;
	};
template<typename T, const int N = 8>
bool operator==(const s_carray_0<T,N>& l, const s_carray_0<T,N>& r) {
	return l.ca == r.ca;
	}
template<typename T, const int N = 8>
bool operator!=(const s_carray_0<T,N>& l, const s_carray_0<T,N>& r) {
	return !(l == r);
	}
template<typename T, const size_t N = 8>
void make_rand(s_carray_0<T, N> & v) {
	make_rand(v.ca);
	}


struct my_struct_fixed_size_00 {

	// 8
	double s;
	// 4
	int i;
	// 8
	size_t sss;
	// 1
	char c;
	// 1
	char b;
	};
bool operator==(const my_struct_fixed_size_00& l, const my_struct_fixed_size_00& r) {
	return l.s == r.s && l.i == r.i && l.sss == r.sss && l.b == r.b && l.c == r.c;
	}
bool operator!=(const my_struct_fixed_size_00& l, const my_struct_fixed_size_00& r) {
	return !(l == r);
	}
/*! random initialize a my_struct_fixed_size_00's value */
 void make_rand(my_struct_fixed_size_00 & m) {
	auto rnd = crm::sx_uuid_t::rand();

	m.b = rnd.u8_16[0];
	m.c = rnd.u8_16[1];
	m.sss = rnd.u64_2[0];
	m.s = (double)rnd.u64_2[1];
	m.i = rnd.u32_4[1];
	}


struct my_struct_fixed_size_01 {

	// 8
	u2 u;
	// 4
	int i;
	};
bool operator==(const my_struct_fixed_size_01& l, const my_struct_fixed_size_01& r) {
	return l.u == r.u && l.i == r.i;
	}
bool operator!=(const my_struct_fixed_size_01& l, const my_struct_fixed_size_01& r) {
	return !(l == r);
	}
/*! random initialize a my_struct_fixed_size_01's value */
void make_rand(my_struct_fixed_size_01 & m) {
	make_rand(m.u);
	m.i = 0;
	}


struct my_struct_fixed_size_10 {
	my_struct_fixed_size_00 m00;
	my_struct_fixed_size_01 m01;

	u2 u;
	size_t s;
	};
bool operator==(const my_struct_fixed_size_10& l, const my_struct_fixed_size_10& r) {
	return l.m00 == r.m00 && l.m01 == r.m01 && l.u == r.u && l.s == r.s;
	}
bool operator!=(const my_struct_fixed_size_10& l, const my_struct_fixed_size_10& r) {
	return !(l == r);
	}
/*! random initialize a my_struct_fixed_size_10's value */
void make_rand(my_struct_fixed_size_10 & m) {
	make_rand(m.m00);
	make_rand(m.m01);
	make_rand(m.u);
	m.s = 0;
	}

struct my_struct_fixed_size_11 {
	my_struct_fixed_size_10 m10;
	std::array<my_struct_fixed_size_00, 2> v00;
	};
bool operator==(const my_struct_fixed_size_11& l, const my_struct_fixed_size_11& r) {
	return l.m10 == r.m10 && l.v00 == r.v00;
	}
bool operator!=(const my_struct_fixed_size_11& l, const my_struct_fixed_size_11& r) {
	return !(l == r);
	}
/*! random initialize a my_struct_fixed_size_11's value */
void make_rand(my_struct_fixed_size_11 & m) {
	make_rand(m.m10);

	for(size_t i = 0; i < m.v00.size(); ++i) {
		make_rand(m.v00[i]);
		}
	}

/*! test of a std::array of values T's type */
template<typename T, const size_t N>
void test_0() {
	std::array<T,N> ia;
	make_rand(ia);

	auto ls = crm::srlz::object_trait_serialized_size(ia);

	auto ws = crm::srlz::wstream_constructor_t::make();
	crm::srlz::serialize(ws, ia);

	decltype(ia) oa;
	auto binary = ws.release();
	if(binary.size() != ls) {
		throw std::exception{};
		}
	auto rs = crm::srlz::rstream_constructor_t::make(binary);
	crm::srlz::deserialize(rs, oa);

	if(ia != oa) {
		throw std::exception();
		}
	}

/*! test of a struct with a member std::array of values T's type */
template<typename T, const size_t N>
void test_1() {
	s_carray_0<T, N> in;
	make_rand(in);
	auto srlzLenOin = crm::srlz::object_trait_serialized_size(in);

	auto ws = crm::srlz::wstream_constructor_t::make();
	crm::srlz::serialize(ws, in);

	s_carray_0<T, N> outv;
	auto binary = ws.release();
	if(binary.size() != srlzLenOin) {
		throw std::exception{};
		}

	auto rs = crm::srlz::rstream_constructor_t::make(binary);
	crm::srlz::deserialize(rs, outv);

	auto srlzLenOex = crm::srlz::object_trait_serialized_size(outv);
	if(srlzLenOex != srlzLenOin) {
		throw std::exception();
		}


	if(in != outv) {
		throw std::exception();
		}
	}


static_assert(crm::srlz::detail::is_fixed_size_serialized_v< my_struct_fixed_size_11>, __FILE_LINE__);


int main() {
	test_0<int, 16>();
	test_0<std::string, 16>();
	test_0<my_struct_fixed_size_11, 16>();
	test_1<int, 16>();
	test_1<std::string, 16>();
	test_1<my_struct_fixed_size_11, 2>();

	std::cout << "ok" << std::endl;
	std::getchar();

    return 0;
    }

