#pragma once


#include "./notifications/base_terms.h"
#include "./notifications/base_exception_terms.h"
#include "./notifications/exceptions/abandoned_exception.h"
#include "./notifications/exceptions/duplicate_object_exception.h"
#include "./notifications/exceptions/extern_exception.h"
#include "./notifications/exceptions/not_found_exception.h"
#include "./notifications/exceptions/object_closed.h"
#include "./notifications/exceptions/refuse_operation_exception.h"
#include "./notifications/exceptions/serialize_exception.h"
#include "./notifications/exceptions/serialize_overflow.h"
#include "./notifications/exceptions/base_channel_terms.h"
#include "./notifications/exceptions/terminate_operation_exception.h"
#include "./notifications/exceptions/inconsistency_exchange_protocol_exception.h"
#include "./notifications/exceptions/test_exception.h"
#include "./notifications/exceptions/timeout_exception.h"
#include "./notifications/exceptions/types_mismatching_exception.h"
#include "./notifications/exceptions/outbound_exception.h"
#include "./notifications/tagged/canceletion.h"
