#pragma once


#include "../../context/context.h"


namespace crm::detail {

class stream2message {
	std::weak_ptr< distributed_ctx_t> _ctx;
	std::shared_ptr<default_binary_protocol_handler_t> _protocol;
	std::unique_ptr<iol_gtw_t> _gateway;

public:
	stream2message(std::weak_ptr< distributed_ctx_t>);

	std::vector<std::unique_ptr<i_iol_ihandler_t>> decode(const binary_vector_t& bin, size_t count);
	};
}
