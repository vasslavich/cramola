#include "../../../internal.h"
#include "../stream2message.h"

using namespace crm;
using namespace crm::detail;


stream2message::stream2message(std::weak_ptr< distributed_ctx_t> c_)
	: _ctx(c_)
	, _protocol(std::make_shared < crm::default_binary_protocol_handler_t>(CHECK_PTR(c_)))
	, _gateway(std::make_unique<iol_gtw_t>(_ctx, _protocol)) {}

std::vector<std::unique_ptr<i_iol_ihandler_t>> stream2message::decode(const binary_vector_t& bin, size_t count) {
	return _gateway->decode(bin, count);
	}