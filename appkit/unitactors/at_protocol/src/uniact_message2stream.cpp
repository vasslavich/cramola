#include "../../../internal.h"
#include "../message2stream.h"


using namespace crm;
using namespace crm::detail;


static_assert(srlz::detail::is_container_of_bytes_v<decltype(std::declval<message2stream>().push(std::declval<osendrecv_datagram_t>()))>, __FILE_LINE__);


auto rand_session_descriptor() {
	return session_description_t{ session_id_t::make(),
		global_object_identity_t::rand().to_str() };
	}

auto rand_identity_descriptor() {
	return identity_descriptor_t{ global_object_identity_t::rand(),
		global_object_identity_t::rand().to_str() };
	}

message2stream::message2stream(std::weak_ptr< distributed_ctx_t> c_)
	: _ctx(c_)
	, _protocol(std::make_shared < crm::default_binary_protocol_handler_t>(CHECK_PTR(c_)))
	, _gateway(std::make_unique<iol_gtw_t>( _ctx, _protocol)) {}

const session_description_t& message2stream::session()const noexcept {
	return _session;
	}

rtl_level_t message2stream::level()const noexcept {
	return _level;
	}

const identity_descriptor_t& message2stream::this_id()const noexcept {
	return _thisId;
	}

const identity_descriptor_t& message2stream::remote_id()const noexcept {
	return _remoteId;
	}

const identity_descriptor_t& message2stream::id()const noexcept {
	return remote_id();
	}

void message2stream::generate_session_state() {
	_session = rand_session_descriptor();
	_level = rtl_level_t::l0;
	_thisId = rand_identity_descriptor();
	_remoteId = rand_identity_descriptor();
	}
