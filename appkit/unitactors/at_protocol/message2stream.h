#pragma once


#include "../../context/context.h"


namespace crm::detail {


class message2stream {
	std::weak_ptr< distributed_ctx_t> _ctx;
	std::shared_ptr<default_binary_protocol_handler_t> _protocol;
	std::unique_ptr<iol_gtw_t> _gateway;
	session_description_t _session;
	rtl_level_t _level;
	identity_descriptor_t _thisId;
	identity_descriptor_t _remoteId;

public:
	const session_description_t& session()const noexcept;
	rtl_level_t level()const noexcept;
	const identity_descriptor_t& this_id()const noexcept;
	const identity_descriptor_t& remote_id()const noexcept;
	const identity_descriptor_t& id()const noexcept;

	void generate_session_state();

private:
	template<typename O,
		typename std::enable_if_t < is_out_message_requirements_v<O>, int> = 0>
	void normalizex(O&& obj)const {

		if (auto c = _ctx.lock()) {
			/* ������ */
			if (is_null(obj.session()))
				obj.set_session(session());

			/* ������� */
			if (is_null(obj.destination_id()))
				obj.set_destination_id(remote_id());

			/* �������� */
			if (is_null(obj.source_id()))
				obj.set_source_id(this_id());

			/* ������� ������������� */
			if (!obj.fl_level())
				obj.set_level(level());

			/* ������������� ��������� */
			if (is_null(obj.id())) {
				obj.set_id(c->generate_message_id());
				}

			/* ������������� ����������� ����� */
			if (c->policies().use_crc()) {
				obj.setf_use_crc();
				}
			}
		else {
			THROW_MPF_EXC_FWD(nullptr);
			}
		}

	template<typename O,
		typename std::enable_if_t < is_out_message_requirements_v<O>, int> = 0>
	auto push_normalizedx(O&& obj) {
		normalizex(obj);
		return CHECK_PTR(_gateway)->serialize_gateout(std::forward<O>(obj)).bin();
		}

public:
	message2stream(std::weak_ptr< distributed_ctx_t>);

	/*! ��������� ������ � ������� ��������� ��������� */
	template<typename O,
		typename std::enable_if_t < is_out_message_requirements_v<O>, int> = 0>
	auto push(O&& obj) {
		return push_normalizedx(std::forward<O>(obj));
		}
	};
}


