#pragma once

#ifdef __cplusplus
#include <cstdint>
#else
#include <stdint.h>
#endif

#include "./build_capability.h"

namespace crm {


#ifdef _MSC_VER


/* VC++ 9.0 */
#if _MSC_VER <= 1500

typedef __int8					int8_t;
typedef __int16					int16_t;
typedef __int32					int32_t;
typedef __int64					int64_t;

typedef unsigned __int8			uint8_t;
typedef unsigned __int16		uint16_t;
typedef unsigned __int32		uint32_t;
typedef unsigned __int64		uint64_t;

/* VC++ 10.0 */
#elif _MSC_VER >= 1600

#else
#error undefined VC++ version
#endif
#endif//_MSC_VER

using sys_error_code_t = uint32_t;
using max_serialized_length_type = std::uintmax_t;

#define DCF_ADDON_SPEC		__declspec(dllexport)
#define DCF_ADDON_CALL		/*__stdcall*/
}

