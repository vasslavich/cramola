#pragma once 


#include "./build_capability.h"



#ifdef CBL_MAIN_TRACE_ON
	#define CBL_TRACE_ON
	#define CBL_USE_TRACE_MODE
	#define CBL_BUILD_TRACE

	#if (defined _DEBUG) && !defined( CBL_USE_TRACE_MODE)
		#define CBL_USE_TRACE_MODE
	#endif
#endif


#define CBL_TRACE_LEVEL_OFF					0
#define CBL_TRACE_LEVEL_1					1
#define CBL_TRACE_LEVEL_2					2
#define CBL_TRACE_LEVEL_3					3
#define CBL_TRACE_LEVEL_4					4
#define CBL_TRACE_LEVEL_ALL					CBL_TRACE_LEVEL_4

#ifdef CBL_TRACE_ON
	#define CBL_TRACE_LEVEL_TRAIT				CBL_TRACE_LEVEL_2
#else
	#define CBL_TRACE_LEVEL_TRAIT				CBL_TRACE_LEVEL_OFF
#endif


#if (CBL_TRACE_LEVEL_TRAIT	>=	CBL_TRACE_LEVEL_1)
	#define CBL_TRACE_TYPEMAP_FAIL
#endif


#if (CBL_TRACE_LEVEL_TRAIT	>=	CBL_TRACE_LEVEL_2)
	#define CBL_TRACE_CTX_STATE
	#define CBL_TRACE_LAZY_EXECUTION_STACK
#endif

#if (CBL_TRACE_LEVEL_TRAIT	>=	CBL_TRACE_LEVEL_3)
	#define CBL_TRACE_THREAD_CREATE_POINT
	#define DCF_ADDON_MANAGER_TRACE
	#define CBL_USE_TRACE_SM_HOST_MEMORY
#endif

#if (CBL_TRACE_LEVEL_TRAIT	>=	CBL_TRACE_LEVEL_4)
	#define CBL_TRACE_EXCEPTION_RAISE
#endif



#define CBL_VALUE_TO_STRING(x) #x
#define CBL_VALUE(x) CBL_VALUE_TO_STRING(x)
#define CBL_VAR_NAME_VALUE(var, value)				"crm:build:" #var "="  CBL_VALUE(value) ":" __FILE__ ":" CBL_VALUE(__LINE__)
#define CBL_MESSAGE_ROADMAP_FOOTMARK(var, tag)		"crm:build:message footmark:" #var "=" #tag ":" CBL_VALUE(tag) ":" __FILE__ ":" CBL_VALUE(__LINE__)
#define CBL_MESSAGE_TYPEID(name)					"crm:build:type:" #name "=" CBL_VALUE(__LINE__)



#define DCF_ERR_LANG_ENG					0
#define DCF_ERR_LANG_RUS					1
#define DCF_ERR_LANG						DCF_ERR_LANG_ENG




#define NOTIFICATION_TAG_TRACE_OBJCNT						10203040
#define NOTIFICATION_TRACE_OBJCNT_LIVE	
#define NOTIFICATION_TRACE_OBJCNT_LIVE_INTERVAL_SECS		60
#define NOTIFICATION_TRACE_ASYNC_POOL_TAG					987654321



#define DCF_ADDON_MANAGER_TRACE_TAG							939495961
#define DCF_ADDON_MANAGER_TRACE_ANCHOR_TAG					939495962
#define DCF_ADDON_MANAGER_TRACE_SMODULE_TAG					939495963
#define DCF_ADDON_MANAGER_TRACE_HOST_MEMORY_TAG				939495964
#define DCF_ADDON_MANAGER_PREFIX(msg)						(std::string("ADM:") + (msg))
#define DCF_ADDON_MANAGER_FWDPREFIX(msg)						(std::wstring(L"ADM:") + (msg))



#define CBL_SHARED_MODULE_BASE_TAG							937459000
#define CBL_SHARED_MODULE_PREFIX(msg)						(std::string("CBL-SM:") + (msg))


#define CBL_TRACE_THREAD_BASE_TAG							940000000
#define CBL_TRACE_THREAD_CREATE_POINT_TAG					(CBL_TRACE_THREAD_BASE_TAG + 1)



#ifdef DCF_ADDON_MANAGER_TRACE
#define DCF_ADDON_MANAGER_TRACE_MANAGER(msg)				CHECK_NO_EXCEPTION( crm::logger_t::logging(DCF_ADDON_MANAGER_PREFIX((msg)),DCF_ADDON_MANAGER_TRACE_TAG));
#define DCF_ADDON_MANAGER_TRACE_MANAGER_FWD(msg)				CHECK_NO_EXCEPTION( crm::logger_t::logging(DCF_ADDON_MANAGER_FWDPREFIX((msg)),DCF_ADDON_MANAGER_TRACE_TAG));
#define DCF_ADDON_MANAGER_TRACE_ANCHOR(msg)					CHECK_NO_EXCEPTION( crm::logger_t::logging(DCF_ADDON_MANAGER_PREFIX((msg)),DCF_ADDON_MANAGER_TRACE_ANCHOR_TAG));
#define DCF_ADDON_MANAGER_TRACE_ANCHOR_FWD(msg)				CHECK_NO_EXCEPTION( crm::logger_t::logging(DCF_ADDON_MANAGER_FWDPREFIX((msg)),DCF_ADDON_MANAGER_TRACE_ANCHOR_TAG));
#define DCF_ADDON_MANAGER_TRACE_SMODULE(msg)				CHECK_NO_EXCEPTION( crm::logger_t::logging(DCF_ADDON_MANAGER_PREFIX((msg)),DCF_ADDON_MANAGER_TRACE_SMODULE_TAG));
#define DCF_ADDON_MANAGER_TRACE_HOST_MEMORY(msg)			CHECK_NO_EXCEPTION( crm::logger_t::logging(DCF_ADDON_MANAGER_PREFIX((msg)),DCF_ADDON_MANAGER_TRACE_HOST_MEMORY_TAG));
#else
#define DCF_ADDON_MANAGER_TRACE_MANAGER(msg)	
#define DCF_ADDON_MANAGER_TRACE_MANAGER_FWD(msg)
#define DCF_ADDON_MANAGER_TRACE_ANCHOR(msg)		
#define DCF_ADDON_MANAGER_TRACE_ANCHOR_FWD(msg)
#define DCF_ADDON_MANAGER_TRACE_SMODULE(msg)
#define DCF_ADDON_MANAGER_TRACE_HOST_MEMORY(msg)
#endif




#define CBL_MPF_TRACELEVEL_EVENT_TAG												1020302010
#define CBL_MPF_TRACELEVEL_EVENT_RT0_TAG											22334452
#define CBL_MPF_TRACELEVEL_EVENT_RT1_TAG											22334453
#define CBL_MPF_TRACELEVEL_EVENT_RT0_INPUT_TAG										22334454
#define CBL_MPF_TRACELEVEL_EVENT_RT1_INPUT_TAG										22334455
#define CBL_MPF_TRACELEVEL_FAULT_RT0_TAG											22334456
#define CBL_MPF_TRACELEVEL_EVENT_CONNECTION_TAG										22334457
#define CBL_MPF_TRACELEVEL_EVENT_RT0_CONNECTION_TAG									22334458
#define CBL_MPF_TRACELEVEL_EVENT_RT1_CONNECTION_TAG									22334459
#define CBL_MPF_TRACELEVEL_EVENT_RT0_OUTPUT_TAG										22334462
#define CBL_MPF_TRACELEVEL_EVENT_RT1_OUTPUT_TAG										22334463
#define CBL_MPF_TRACELEVEL_EVENT_RT0_RT1_TAG										22334465
#define CBL_MPF_TRACELEVEL_EVENT_RT0_SEND_TAG										22334466
#define CBL_MPF_TRACELEVEL_EVENT_RT1_SEND_TAG										22334467
#define CBL_MPF_TRACELEVEL_FAULT_RT1_TAG											22334468
#define CBL_MPF_TEST_EMULATION_IN_CONTEXT_TAG										148724361
#define CBL_MPF_STREAMED_OBJECTS_TRACE_TAG											23452323


/*! ����������� �������� ��������� �� ������ 0
-----------------------------------------------------------------------------------------------------------*/
#define CBL_MPF_TRACELEVEL_EVENT_RT0_RECV_OFF										0
#define CBL_MPF_TRACELEVEL_EVENT_RT0_RECV_1											1
#define CBL_MPF_TRACELEVEL_EVENT_RT0_RECV_2											2
#define CBL_MPF_TRACELEVEL_EVENT_RT0_RECV_ALL										CBL_MPF_TRACELEVEL_EVENT_RT0_RECV_2




/*! ����������� ��������� ��������� ������ 0
-----------------------------------------------------------------------------------------------------------*/
#define CBL_MPF_TRACELEVEL_EVENT_RT0_SEND_OFF										0
#define CBL_MPF_TRACELEVEL_EVENT_RT0_SEND_1											1
#define CBL_MPF_TRACELEVEL_EVENT_RT0_SEND_2											2
#define CBL_MPF_TRACELEVEL_EVENT_RT0_SEND_ALL										CBL_MPF_TRACELEVEL_EVENT_RT0_SEND_2



/*! ����������� �������� ��������� ������ 1
-----------------------------------------------------------------------------------------------------------*/
#define CBL_MPF_TRACELEVEL_EVENT_RT1_RECV_OFF										0
#define CBL_MPF_TRACELEVEL_EVENT_RT1_RECV_1											1
#define CBL_MPF_TRACELEVEL_EVENT_RT1_RECV_2											2
#define CBL_MPF_TRACELEVEL_EVENT_RT1_RECV_ALL										CBL_MPF_TRACELEVEL_EVENT_RT1_RECV_2




/*! ����������� ��������� ��������� ������ 1
-----------------------------------------------------------------------------------------------------------*/
#define CBL_MPF_TRACELEVEL_EVENT_RT1_SEND_OFF										0
#define CBL_MPF_TRACELEVEL_EVENT_RT1_SEND_1											1
#define CBL_MPF_TRACELEVEL_EVENT_RT1_SEND_2											2
#define CBL_MPF_TRACELEVEL_EVENT_RT1_SEND_ALL										CBL_MPF_TRACELEVEL_EVENT_RT1_SEND_2




/*! ����������� ����������� �������� 
------------------------------------------------------------------------------------------------------------*/
#define CBL_MPF_TRACELEVEL_ST_OFF													0
#define CBL_MPF_TRACELEVEL_ST_1														1
#define CBL_MPF_TRACELEVEL_ST_2														2
#define CBL_MPF_TRACELEVEL_ST_3														3
#define CBL_MPF_TRACELEVEL_ST_4														4
#define CBL_MPF_TRACELEVEL_ST_5														5
#define CBL_MPF_TRACELEVEL_ST_ALL													CBL_MPF_TRACELEVEL_ST_5



#ifdef CBL_TRACE_ON
	#define CBL_MPF_TRACELEVEL_EVENTS
#endif

#ifdef CBL_MPF_TRACELEVEL_EVENTS
	#define CBL_MPF_TRACELEVEL_COMMANDS_HOST
	#define CBL_MPF_TRACELEVEL_EVENTS_RT0RT1

	#define CBL_MPF_TRACELEVEL_ST										CBL_MPF_TRACELEVEL_ST_1
#else
	#define CBL_MPF_TRACELEVEL_ST										CBL_MPF_TRACELEVEL_ST_OFF
#endif


#if (CBL_MPF_TRACELEVEL_ST >= CBL_MPF_TRACELEVEL_ST_1)
	#define CBL_MPF_TRACELEVEL_FAULTED
	#define CBL_MPF_STREAMED_OBJECTS_TRACE
#endif


#if (CBL_MPF_TRACELEVEL_ST >= CBL_MPF_TRACELEVEL_ST_2)
	#define CBL_MPF_TRACELEVEL_EVENT_CONNECTION
	#define CBL_MPF_TRACELEVEL_EMULATOR_COUNTERS
	#define CBL_MPF_MESSAGE_TRACEWAY_KEYS_BY_ID
	#define CBL_MPF_TRACELEVEL_FUNCTOR_INVOKE_GUARD_NAME
	#define CBL_MPF_TRACELEVEL_EVENT_SNDRCV_GUARD
	#define CBL_SNDRCV_TRACE_COLLECT
#endif


#if (CBL_MPF_TRACELEVEL_ST >= CBL_MPF_TRACELEVEL_ST_3)
	#define CBL_MPF_TRACELEVEL_EVENT_RT1_RECV_INBOUND
	#define CBL_MPF_TRACELEVEL_EVENT_RT1_RECV_OUTBOUND
	#define CBL_MPF_TRACELEVEL_EVENT_RT1_SEND_INBOUND
	#define CBL_MPF_TRACELEVEL_EVENT_RT1_SEND_OUTBOUND


	#define CBL_MPF_TRACELEVEL_EVENT_RT1_SEND_LEVEL						CBL_MPF_TRACELEVEL_EVENT_RT1_SEND_ALL
	#define CBL_MPF_TRACELEVEL_EVENT_RT1_RECV_LEVEL						CBL_MPF_TRACELEVEL_EVENT_RT1_RECV_ALL
#else
	#define CBL_MPF_TRACELEVEL_EVENT_RT1_SEND_LEVEL						CBL_MPF_TRACELEVEL_EVENT_RT1_SEND_OFF
	#define CBL_MPF_TRACELEVEL_EVENT_RT1_RECV_LEVEL						CBL_MPF_TRACELEVEL_EVENT_RT1_RECV_OFF
#endif


#if (CBL_MPF_TRACELEVEL_ST >= CBL_MPF_TRACELEVEL_ST_4)	
	#define CBL_MPF_TRACELEVEL_EVENT_RT0_RECV_INBOUND
	#define CBL_MPF_TRACELEVEL_EVENT_RT0_RECV_OUTBOUND
	#define CBL_MPF_TRACELEVEL_EVENT_RT0_SEND_INBOUND
	#define CBL_MPF_TRACELEVEL_EVENT_RT0_SEND_OUTBOUND
	
	#define CBL_MPF_TRACELEVEL_EVENT_RT0_SEND_LEVEL						CBL_MPF_TRACELEVEL_EVENT_RT0_SEND_1
	#define CBL_MPF_TRACELEVEL_EVENT_RT0_RECV_LEVEL						CBL_MPF_TRACELEVEL_EVENT_RT0_RECV_1
#else
	#define CBL_MPF_TRACELEVEL_EVENT_RT0_SEND_LEVEL						CBL_MPF_TRACELEVEL_EVENT_RT0_SEND_OFF
	#define CBL_MPF_TRACELEVEL_EVENT_RT0_RECV_LEVEL						CBL_MPF_TRACELEVEL_EVENT_RT0_RECV_OFF
#endif


#if (CBL_MPF_TRACELEVEL_ST >= CBL_MPF_TRACELEVEL_ST_5)
	//#define CBL_MPF_TRACELEVEL_PING_RT0
	//#define CBL_MPF_TRACELEVEL_PING_RT1

	#define CBL_MPF_TRACELEVEL_REVERSE_EXC
	#define CBL_SUBSCRIBE_TABLE_TRACE
#endif



#ifdef CBL_MPF_TRACELEVEL_FAULTED
	#define CBL_MPF_TRACELEVEL_FAULTED_RT0
	#define CBL_MPF_TRACELEVEL_FAULTED_RT1
	#define CBL_MPF_TRACELEVEL_FAULTED_RT0RT1
#endif



#if defined(CBL_MPF_TRACELEVEL_FUNCTOR_INVOKE_GUARD_NAME) || defined(CBL_TRACE_LAZY_EXECUTION_STACK)
	#define CBL_TRACELEVEL_TRACE_TAG_ON
#endif


#define CBL_MPF_PREFIX(msg)												(std::string("mpf:") + (msg))
#define CBL_MPF_PREFIX_RT0(msg)											(std::string("mpf-rt0:") + (msg))
#define CBL_MPF_PREFIX_RT1(msg)											(std::string("mpf-rt1:") + (msg))
#define CBL_MPF_PREFIX_RT_0_1(msg)										(std::string("mpf-rt0-rt1:") + (msg))



#ifdef CBL_MPF_TRACELEVEL_EVENT_CONNECTION
#define CBL_MPF_TRACELEVEL_EVENT_TAG_CONNECTION_TRACE(msg, tag) CHECK_NO_EXCEPTION( crm::logger_t::logging(CBL_MPF_PREFIX((msg)),tag));
#else
#define CBL_MPF_TRACELEVEL_EVENT_TAG_CONNECTION_TRACE(msg, tag)
#endif

#ifdef CBL_MPF_TRACELEVEL_EVENT_CONNECTION
#define CBL_MPF_TRACELEVEL_EVENT_CONNECTION_TRACE(msg) CBL_MPF_TRACELEVEL_EVENT_TAG_CONNECTION_TRACE((msg),CBL_MPF_TRACELEVEL_EVENT_CONNECTION_TAG);
#else
#define CBL_MPF_TRACELEVEL_EVENT_CONNECTION_TRACE(msg)
#endif

#ifdef CBL_MPF_TRACELEVEL_EVENT_CONNECTION
#define CBL_MPF_TRACELEVEL_EVENT_RT0_CONNECTION_TRACE(msg) CBL_MPF_TRACELEVEL_EVENT_TAG_CONNECTION_TRACE((msg),CBL_MPF_TRACELEVEL_EVENT_RT0_CONNECTION_TAG);
#else
#define CBL_MPF_TRACELEVEL_EVENT_RT0_CONNECTION_TRACE(msg)
#endif

#ifdef CBL_MPF_TRACELEVEL_EVENT_CONNECTION
#define CBL_MPF_TRACELEVEL_EVENT_RT1_CONNECTION_TRACE(msg) CBL_MPF_TRACELEVEL_EVENT_TAG_CONNECTION_TRACE((msg),CBL_MPF_TRACELEVEL_EVENT_RT1_CONNECTION_TAG);
#else
#define CBL_MPF_TRACELEVEL_EVENT_RT1_CONNECTION_TRACE(msg)
#endif


#define CBL_BUILD_VAR_SNDRCV_REVERSE_GUARD
#define CBL_BUILD_VAR_SNDRCV_REVERSE_SETUP_FUNCTOR
#define CBL_BUILD_VAR_MESSAGE_ROADMAP_MARKER_OUTBOUND
#define CBL_MESSAGE_ROADMAP


constexpr auto CBL_SNDRCV_RXx_Xx_MASK = 0x800;

constexpr auto RQo_B = (CBL_SNDRCV_RXx_Xx_MASK | 0);
constexpr auto RQo_L1 = (CBL_SNDRCV_RXx_Xx_MASK | 1);
constexpr auto RQo_L0 = (CBL_SNDRCV_RXx_Xx_MASK | 2);
constexpr auto RQi_L0 = (CBL_SNDRCV_RXx_Xx_MASK | 3);
constexpr auto RQi_L1 = (CBL_SNDRCV_RXx_Xx_MASK | 4);
constexpr auto RQi_E = (CBL_SNDRCV_RXx_Xx_MASK | 5);

constexpr auto RZo_B = (CBL_SNDRCV_RXx_Xx_MASK | 10);
constexpr auto RZo_L1 = (CBL_SNDRCV_RXx_Xx_MASK | 11);
constexpr auto RZo_L0 = (CBL_SNDRCV_RXx_Xx_MASK | 12);
constexpr auto RZi_L0 = (CBL_SNDRCV_RXx_Xx_MASK | 13);
constexpr auto RZi_L1 = (CBL_SNDRCV_RXx_Xx_MASK | 14);
constexpr auto RZi_E = (CBL_SNDRCV_RXx_Xx_MASK | 15);


#ifdef CBL_BUILD_TRACE
	#define CBL_MPF_USE_REVERSE_EXCEPTION_TRACE
	#define CBL_MPF_GLOBAL_TRACEWAY_COUNTERS
#endif

#define CBL_TRACE_OBJECTS_COUNTER_STEP		20
#define SIZE_SEQUENCE_FWDARNING				"SIZE_SEQUENCE_FWDARNING"
#define OBJECTS_COUNTER_FWDARNING				"OBJECTS_COUNTER_FWDARNING"




#ifdef CBL_TRACE_CTX_STATE
#define CBL_TRACE_CTX_STATE_SNDRCV_IOPS
#endif


#ifdef CBL_TRACE_CTX_STATE
#define CBL_MPF_TRACE_ID_INC(locid, text)		{crm::detail::get_trace_collection().inc((locid), (text));}
#else
#define CBL_MPF_TRACE_ID_INC(locid, text)
#endif


#ifdef CBL_TRACE_CTX_STATE
#define CBL_MPF_TRACE_ID_DEC(locid)				{crm::detail::get_trace_collection().dec((locid));}
#else
#define CBL_MPF_TRACE_ID_DEC(locid)
#endif

#ifdef CBL_TRACE_CTX_STATE
	#define CBL_MPF_TRACE_ID_HAS(locid)			(crm::detail::get_trace_collection().count((locid)))
#else
	#define CBL_MPF_TRACE_ID_HAS(keypack)		(0)
#endif


#ifdef CBL_MPF_MESSAGE_TRACEWAY_KEYS_BY_ID
#define CBL_MPF_KEYTRACE_SET(keypack, value)	{crm::detail::get_keytracer().set(crm::trait_at_keytrace_set((keypack)), (value));}
#else
#define CBL_MPF_KEYTRACE_SET(keypack, value)	CBL_MPF_UNUSED_VARIABLE_COMPILER_MKREF(value)
#endif


#ifdef CBL_MPF_MESSAGE_TRACEWAY_KEYS_BY_ID
#define CBL_MPF_KEYTRACE_UNSET(keypack)			{crm::detail::get_keytracer().unset(crm::trait_at_keytrace_set((keypack)));}
#else
#define CBL_MPF_KEYTRACE_UNSET(keypack)
#endif


#ifdef CBL_MPF_MESSAGE_TRACEWAY_KEYS_BY_ID

namespace crm::detail {
template<typename __XObj>
bool __verify_sndrcv_attributes( const __XObj & obj)noexcept {
	auto f1 = ::is_report(obj);
	auto f2 = ::is_sendrecv_response(obj);
	auto f3 = ::is_sendrecv_request(obj);
	auto directionf_ = ::passing_direction(obj);

	auto r = (f1 && f2) ||
		(!f1 && !f2 && (directionf_ == rtl_table_t::in)) ||
		(f2 && (directionf_ == rtl_table_t::in)) ||
		(f3 && (directionf_ == rtl_table_t::out)) ||
		(!(f2 && f3) && (directionf_ == rtl_table_t::out) );

	if(!r) {
		__debugbreak();
		}

	return r;
	}
}

#define CBL_VERIFY_SNDRCV_KEY_SPIN_TAG(spintag)					CBL_VERIFY(check_sndrcv_key_spin_tag(spintag))
#define CBL_IS_SNDRCV_KEY_SPIN_TAGGED_ON(spintag)				(is_sndrcv_spin_tagged_invoke(spintag))
#define CBL_IS_SNDRCV_KEY_SPIN_TAGGED_OFF(spintag)				(!is_sndrcv_spin_tagged_invoke(spintag))
#define CBL_VERIFY_SNDRCV_ATTRIBUTES(objxxx)					CBL_VERIFY(__verify_sndrcv_attributes(objxxx))
#else
#define CBL_VERIFY_SNDRCV_KEY_SPIN_TAG(spintag)		
#define CBL_IS_SNDRCV_KEY_SPIN_TAGGED_ON(spintag)				(is_sndrcv_spin_tagged_on())
#define CBL_IS_SNDRCV_KEY_SPIN_TAGGED_OFF(spintag)				(is_sndrcv_spin_tagged_off())
#define CBL_VERIFY_SNDRCV_ATTRIBUTES(obj)			
#endif




