#pragma once


/*! a byte sizeof, of course, it is one */
constexpr size_t CRM_BYTE = 1;
constexpr size_t CRM_KB = 1024 * CRM_BYTE;
constexpr size_t CRM_MB = CRM_KB * CRM_KB;
constexpr size_t CRM_GB = CRM_KB * CRM_MB;


// использование трассировки
//#define CBL_MAIN_TRACE_ON

// использование проверок
//#define CBL_MAIN_VALIDATE_ON

/*! Keep guard object(send receive garanty of response) */
//#define CBL_IS_KEEP_OUTCOME_MESSAGE_RESPONSE_GUARD_L0

//#define CRM_INCOMING_MESSAGES_SPLIT_IO_FROM_MESSAGES_STOCK
#define CRM_INCOMING_MESSAGES_ORDERED_BY_TARGET_ID

/*! Monitoring objects validity */
//#define CBL_USE_OBJECTS_MONITOR

// генерация случайных ошибок
//#define CBL_MPF_TEST_ERROR_EMULATION


//#define CBL_MPF_ENABLE_TIMETRACE
//#define CBL_MPF_ENABLE_RT1_PING
//#define CBL_MPF_REDIRECT_NOT_FOUNDED_SUBSCRIBED_MESSAGE_TO_BASE


#define CRM_SUBSCRIBE_TBL_INVOKE_MODE_MUTABLE			1
#define CRM_SUBSCRIBE_TBL_INVOKE_MODE_INVAR				2
#define CRM_SUBSCRIBE_TBL_INVOKE_MODE					CRM_SUBSCRIBE_TBL_INVOKE_MODE_INVAR



//#define CBL_USE_BIG_LITTLE_ENDIAN


#define CBL_SRLZ_OPTIONS_DEDUCTION_PREFIX_ON			1
#define CBL_SRLZ_OPTIONS_DEDUCTION_PREFIX_OFF			0
#define CBL_SRLZ_OPTIONS_DEDUCTION_PREFIX_MODE			CBL_SRLZ_OPTIONS_DEDUCTION_PREFIX_OFF

#define CRM_SESSION_TAG_ON								1
#define CRM_SESSION_TAG_OFF								0
#define CRM_SESSION_TAG_MODE							CRM_SESSION_TAG_OFF


#define CBL_SESSION_MAKE_LONG_PREFIX		1
#define CBL_SESSION_MAKE_SHORT_PREFIX		2
#define CBL_SESSION_MAKE_TYPE				CBL_SESSION_MAKE_SHORT_PREFIX


#define CBL_MESSAGE_ADDRESS_TYPES_SHORT


#define CBL_MPF_UNUSED_VARIABLE_COMPILER_MKREF(v) { v; }



#if defined(_FWDIN32) || defined(WIN32)
#define DCF_FWDIN
#elif defined(POSIX)
#define DCF_NIX
#endif

#ifdef DCF_FWDIN
#define PATH_SLASH "\\"
#else
#ifdef DCF_NIX
#define PATH_SLASH "/"
#endif
#endif


#ifdef _MSC_VER
#define __DCF_COMIPLER_MS__
#endif


#define CMD_ARG_NAME_MAX        64




#define __CBL2STR__(x) #x
#define CBL2STR(x) __CBL2STR__(x)

#define __CBL2STRW__(x) L##x
#define CBL2STR(x) __CBL2STR__(x)

#define __CBL_FILE__ __FILE__
#define __CBL_FILEW__ __FILEW__
#define __FWDFILE__ __CBL_FILEW__

#define __FILE_LINE__ __CBL_FILE__ ":" CBL2STR(__LINE__)
#define __FILEW_LINE__ __CBL_FILEW__ ":" CBL2STR(__LINE__)



#define CBL_CAT_NUM(mess, num)			(mess)	":"		CBL2STR(num)







