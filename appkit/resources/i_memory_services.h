#pragma once


namespace crm {


struct i_memory_services_t {
	virtual ~i_memory_services_t() = 0;

	/*! ��������� ������ ��� ���������� ��������� */
	virtual void* allocate(size_t bytes, const char *traceid) = 0;

	/*! ��������� ������ ��� ���������� ��������� */
	virtual void* allocate(size_t bytes) = 0;

	/*! ������������ ������, ����� ���������� ������� #allocate */
	virtual void free(void *p)noexcept = 0;
	};
}
