#include <experimental/coroutine>
#include <experimental/resumable>
#include <boost/asio.hpp>
#include <future>
#include <atomic>
#include "../../internal.h"
#include "../../io_services/src/ios_boost_impl.h"
#include "../../context/context.h"
#include "../tcp_extensions.h"
#include "../../io_services/ios.h"
#include "../../xpeer/pingwatcher.h"
#include "../../queues/qdef.h"


using namespace crm;
using namespace crm::detail;
using namespace crm::detail::tcp;


namespace crm ::detail:: tcp {


// Class to manage the memory to be used for handler-based custom allocation.
// It contains a single block of memory which may be returned for allocation
// requests. If the memory is in use when an allocation request is made, the
// allocator delegates allocation to the global heap.
class handler_memory {
public:
	handler_memory()
		: in_use_(false) {}

	handler_memory(const handler_memory&) = delete;
	handler_memory& operator=(const handler_memory&) = delete;

	void* allocate(std::size_t size) {
		if(!in_use_ && size < sizeof(storage_)) {
			in_use_ = true;
			return &storage_;
			}
		else {
			return ::operator new(size);
			}
		}

	void deallocate(void* pointer) {
		if(pointer == &storage_) {
			in_use_ = false;
			}
		else {
			::operator delete(pointer);
			}
		}

private:
  // Storage space used for handler-based custom memory allocation.
	typename std::aligned_storage<1024>::type storage_;

	// Whether the handler-based custom allocation storage has been used.
	bool in_use_;
	};

// The allocator to be associated with the handler objects. This allocator only
// needs to satisfy the C++11 minimal allocator requirements.
template <typename T>
class handler_allocator {
public:
	using value_type = T;

	explicit handler_allocator(handler_memory& mem)
		: memory_(mem) {}

	template <typename U>
	handler_allocator(const handler_allocator<U>& other) noexcept
		: memory_(other.memory_) {}

	bool operator==(const handler_allocator& other) const noexcept {
		return &memory_ == &other.memory_;
		}

	bool operator!=(const handler_allocator& other) const noexcept {
		return &memory_ != &other.memory_;
		}

	T* allocate(std::size_t n) const {
		return static_cast<T*>(memory_.allocate(sizeof(T) * n));
		}

	void deallocate(T* p, std::size_t /*n*/) const {
		return memory_.deallocate(p);
		}

private:
	template <typename> friend class handler_allocator;

	// The underlying memory.
	handler_memory& memory_;
	};

// Wrapper class template for handler objects to allow handler memory
// allocation to be customised. The allocator_type type and get_allocator()
// member function are used by the asynchronous operations to obtain the
// allocator. Calls to operator() are forwarded to the encapsulated handler.
template <typename Handler>
class custom_alloc_handler {
public:
	using allocator_type = handler_allocator<Handler>;

	custom_alloc_handler(handler_memory& m, Handler h)
		: memory_(m),
		handler_(h) {}

	allocator_type get_allocator() const noexcept {
		return allocator_type(memory_);
		}

	template <typename ...Args>
	void operator()(Args&& ... args) {
		handler_(std::forward<Args>(args)...);
		}

private:
	handler_memory& memory_;
	Handler handler_;
	};

	// Helper function to wrap a handler object to add custom allocation.
template <typename Handler>
inline custom_alloc_handler<Handler> make_custom_alloc_handler(
	handler_memory& m, Handler h) {
	return custom_alloc_handler<Handler>(m, h);
	}


class tcp_source_impl_t : public std::enable_shared_from_this<tcp_source_impl_t> {
private:
	using self_type = tcp_source_impl_t;
	using outqueue_type= i_message_queue_await_t<outbound_message_unit_t>;

	boost::asio::ip::tcp::socket _socket;
	std::weak_ptr<distributed_ctx_t> _ctx;
	i_xpeer_source_t::end_execute_f _closeSlot;
	i_xpeer_source_t::read_ready_f _readSlot;
	i_xpeer_source_t::make_ping_sequence_f _pingOut;
	std::weak_ptr <outqueue_type> _outlist;

	// The memory to use for handler-based custom memory allocation.
	handler_memory _handlerMemW;
	handler_memory _handlerMemR;

	/*! �������� ���������� */
	std::unique_ptr<ping_watcher_t> _pingWatcher;
	/*! ������ ����� */
	std::unique_ptr<i_xtimer_t> _pingTimer;

	std::atomic<bool> _closedf{false};

#ifdef CBL_MPF_OBJECTS_COUNTER_CHECKER_XPEER_CONNECTION_STATE
	crm::utility::__xobject_counter_logger_t<tcp_source_impl_t, 1> _xDbgCounter;
#endif

	std::shared_ptr<distributed_ctx_t> ctx()const noexcept {
		return _ctx.lock();
		}

	void _close_body(std::unique_ptr<dcf_exception_t> && exc) {

		if(auto pOutStack = _outlist.lock()) {
			pOutStack->close();
			}

#ifdef CBL_MPF_TRACELEVEL_EVENT_CONNECTION
		auto endps = "socket closed=" + saddress() + ":" + std::to_string(port());
#endif
		decltype(_closeSlot) clh;
		std::swap(clh, _closeSlot);

		/* ��������� ��������� �������� ������ */
		if (_pingWatcher) {
			CHECK_NO_EXCEPTION(_pingWatcher->stop());
			}

		boost::system::error_code ec;
		CHECK_NO_EXCEPTION(_socket.shutdown(boost::asio::ip::tcp::socket::shutdown_both, ec));
		CHECK_NO_EXCEPTION(_socket.close(ec));

		if (clh) {
			if (auto ctx_ = ctx()) {
				ctx_->launch_async(__FILE_LINE__,
					std::move(clh), exc ? std::move(exc) :

#ifdef CBL_MPF_TRACELEVEL_EVENT_CONNECTION
					CREATE_ABANDONED_PTR_EXC_FWD(std::move(endps))
#else
					CREATE_ABANDONED_PTR_EXC_FWD(nullptr)
#endif
				);
				}
			}
		}

	void close(std::unique_ptr<dcf_exception_t> && exc)noexcept {

		bool clf = false;
		if (_closedf.compare_exchange_strong(clf, true)) {

			CBL_MPF_TRACELEVEL_EVENT_RT0_CONNECTION_TRACE(exc ? "socket close by exception:" : "socket close force:" + saddress());
			CHECK_NO_EXCEPTION(_close_body(std::move(exc)));
			}
		}

	static void set_options(boost::asio::ip::tcp::socket& s) {
		boost::asio::ip::tcp::no_delay option(true);
		s.set_option(option);
		}

public:
	~tcp_source_impl_t() {
		close();
		}

	tcp_source_impl_t(std::weak_ptr<distributed_ctx_t> ctx_, boost::asio::io_service & service)
		: _socket(service)
		, _ctx(ctx_) {}

	tcp_source_impl_t(std::weak_ptr<distributed_ctx_t> ctx_, boost::asio::ip::tcp::socket && socket)
		: _socket(std::move(socket))
		, _ctx(ctx_) {}

	static std::unique_ptr<dcf_exception_t> make_exception(const boost::system::error_code& err, const std::string & tag)noexcept {
		auto text = "sockerr:" + err.message() + ":code=" + std::to_string(err.value()) + ":" + tag;
		return CREATE_TERMINATE_OPERATION_PTR_EXC_FWD(std::move(text));
		}

	static std::unique_ptr<dcf_exception_t> make_exception(const boost::system::error_code& err)noexcept {
		return CREATE_TERMINATE_OPERATION_PTR_EXC_FWD("sockerr:" + err.message());
	}

	void async_connect(const std::unique_ptr<i_endpoint_t> & endPoint,
		std::function<void(std::unique_ptr<dcf_exception_t> && exc)> && onConnect) {

		if (endPoint) {
			if (endPoint->port() > std::numeric_limits<uint16_t>::max())
				THROW_MPF_EXC_FWD(nullptr);

#ifdef CBL_MPF_TRACELEVEL_EVENT_CONNECTION
			std::string endps = endPoint->address() + ":" + std::to_string(endPoint->port());
#endif

			auto port(static_cast<uint16_t>(endPoint->port()));
			auto fep = boost::asio::ip::tcp::endpoint(boost::asio::ip::address::from_string(
				endPoint->address()), port);

			_socket.async_connect(fep,

#ifdef CBL_MPF_TRACELEVEL_EVENT_CONNECTION
				[onConnect, endps]
#else
				[onConnect]
#endif

			(const boost::system::error_code& err) {
				if (err) {


#ifdef CBL_MPF_TRACELEVEL_EVENT_CONNECTION
					auto exc = make_exception(err, endps);
#else
					auto exc = make_exception(err);
#endif

#ifdef CBL_MPF_TRACELEVEL_FAULTED_RT0
					crm::file_logger_t::logging(exc->msg(), CBL_MPF_TRACELEVEL_FAULT_RT0_TAG, __CBL_FILEW__, __LINE__);
#endif

					onConnect(std::move(exc));
					}
				else {
					CBL_MPF_TRACELEVEL_EVENT_RT0_CONNECTION_TRACE("socket opened:" + endps);

					onConnect(std::unique_ptr<dcf_exception_t>());
					}
				} );
			}
		else {
			THROW_MPF_EXC_FWD(nullptr);
			}
		}

	void close() noexcept {
		close(nullptr);
		}

	std::string saddress()const noexcept {
		std::string r;

		if (_socket.is_open()) {
			boost::system::error_code ec;
			auto rep = _socket.remote_endpoint(ec);
			if (!ec) {
				auto a = rep.address();
				auto sa = a.to_string(ec);
				if (!ec) {
					r = sa;
					}
				}
			}

		return r;
		}

	int port() const noexcept {
		int r = -1;

		if (_socket.is_open()) {
			boost::system::error_code ec;
			auto rep = _socket.remote_endpoint(ec);
			if (!ec) {
				r = rep.port();
				}
			}

		return r;
		}

	bool closed()const noexcept {
		return _closedf.load(std::memory_order::memory_order_acquire);
		}

	bool is_socket_opened()const noexcept {
		return _socket.is_open();
		}

	void notify_hndl(std::unique_ptr<dcf_exception_t>&& pn)noexcept {
		if(auto c = _ctx.lock()) {
			c->exc_hndl(std::move(pn));
			}
		}

private:
	void test_alive() {
		if (_pingWatcher) {
			if (!(_pingWatcher->emit_timeout_exceeded() || _pingWatcher->accept_timeout_exceeded())) {
				_pingWatcher->check();
				}
			else {
				std::ostringstream times;
				times << "timeout:trace(sec)="
					<< std::chrono::seconds(_pingWatcher->emit_curr_delta()).count() << ":"
					<< std::chrono::seconds(_pingWatcher->accept_curr_delta()).count();

				close(CREATE_TIMEOUT_PTR_EXC_FWD(times.str()));
				}
			}
		}

	void send_ping_request() {
		if (_pingWatcher && _pingWatcher->emit_interval_exceeded()) {
			write(_pingOut());
			}
		}

	template <typename SyncReadStream, typename DynamicBuffer>
	auto async_do_read(SyncReadStream &s, DynamicBuffer && buffers) {
		struct Awaiter {
			SyncReadStream& s;
			handler_memory& handlerMemR;
			DynamicBuffer buffers;

			boost::system::error_code ec;
			size_t sz;

			bool await_ready() { return false; }
			auto await_resume() { return std::make_pair(ec, sz); }
			void await_suspend(std::experimental::coroutine_handle<> coro) {
				s.async_read_some(buffers, make_custom_alloc_handler(handlerMemR, [this, coro](auto ec_, auto sz_) mutable {

					ec = ec_;
					sz = sz_;

					coro.resume();
					}));
				}
			};

		return Awaiter{s, _handlerMemR, std::forward<DynamicBuffer>(buffers)};
		}

	auto async_do_write(const std::vector<uint8_t> & wbuf, size_t tailOffset) {
		struct Awaiter {
			boost::asio::ip::tcp::socket& socket;
			handler_memory& handlerMemW;
			const std::vector<uint8_t>& wbuf;
			size_t tailOffset = 0;
			std::size_t length = 0;
			boost::system::error_code ec;

			bool await_ready() { return tailOffset >= wbuf.size(); }
			auto await_resume() { return std::make_pair(ec, length); }
			void await_suspend(std::experimental::coroutine_handle<> coro) {
				boost::asio::async_write(socket, boost::asio::buffer(wbuf.data() + tailOffset, wbuf.size() - tailOffset),
					make_custom_alloc_handler(handlerMemW, [this, coro](auto ec_, size_t sz) {
					ec = ec_;
					length = sz;

					coro.resume();
					}));
				}
			};

		return Awaiter{_socket, _handlerMemW, wbuf, tailOffset};
		}

	static std::future<void> do_cycle_write( std::weak_ptr<self_type> wptr, std::shared_ptr<outqueue_type> outlist) {
		try {

#ifdef CBL_OBJECTS_COUNTER_CHECKER
			struct do_cycle_write_instance_counter {};
			utility::__xobject_counter_logger_t<do_cycle_write_instance_counter, 1> CoroWriteStackCounter;
#endif

			/* ��������� ����� */
			outbound_message_unit_t wrbuf;
			size_t wrOffset{ 0 };
			bool brokenf{ false };
			do {
				if(auto p = wptr.lock()) {
#ifdef CBL_MPF_TRACELEVEL_EVENT_CONNECTION
					auto endps = "do_cycle_write:" + p->saddress() + ":" + std::to_string(p->port());
#endif

					/* ���� ��������� ��������� ���� ��������� ��������� */
					if(wrOffset >= wrbuf.bin().size()) {
						auto nextBuf = co_await crm::detail::queue_async_pop(async_space_t::sys, (*outlist));
						if(!nextBuf.bin().empty()) {
							wrbuf.reset_invoke_exception_guard_reset();

							wrbuf = std::move(nextBuf);
							wrOffset = 0;

							CBL_VERIFY(!wrbuf.bin().empty());
							}
						else {
							if(outlist->closed()) {
								p->close(CREATE_PTR_EXC_FWD(nullptr));
								}
							}
						}
					else {
						auto [errcWrite, countWrite] = co_await p->async_do_write(wrbuf.bin(), wrOffset);
						if(!errcWrite) {
							wrOffset += countWrite;

							if(countWrite) {
								if(p->_pingWatcher) {
									p->_pingWatcher->add_emit();
									}
								}
							}
						else {
#ifdef CBL_MPF_TRACELEVEL_EVENT_CONNECTION
							p->close(make_exception(errcWrite, endps));
#else
							p->close(make_exception(errcWrite));
#endif
							}
						}

					brokenf = p->closed();
					}
				else {
					brokenf = true;
					}
				}
			while(!brokenf);
			}
		catch (const dcf_exception_t & e0) {
			if(auto p = wptr.lock()) {
				p->close(e0.dcpy());
				}
			}
		catch (const std::exception & e1) {
			if(auto p = wptr.lock()) {
				p->close(CREATE_PTR_EXC_FWD(e1));
				}
			}
		catch(...) {
			if(auto p = wptr.lock()) {
				p->close(CREATE_PTR_EXC_FWD(nullptr));
				}
			}
		}

	static std::future<void> do_cycle_read(std::weak_ptr<self_type> wptr) {

#ifdef CBL_OBJECTS_COUNTER_CHECKER
		struct do_cycle_read_instance_counter {};
		utility::__xobject_counter_logger_t<do_cycle_read_instance_counter, 1> CoroReadStackCounter;
#endif

		/* �������� ����� */
		std::vector<uint8_t> rbuf(CHECK_PTR(CHECK_PTR(wptr)->ctx())->policies().io_buffer_maxsize());
		bool brokenf{ false };

		do{
			try {
				do{
					if(auto p = wptr.lock()) {
#ifdef CBL_MPF_TRACELEVEL_EVENT_CONNECTION
						auto endps = "do_cycle_read:" + p->saddress() + ":" + std::to_string(p->port());
#endif

						auto [errRead, countRead] = co_await p->async_do_read(p->_socket, boost::asio::buffer(rbuf.data(), rbuf.size()));
						if(!errRead) {

							if(countRead && p->_pingWatcher) {
								p->_pingWatcher->add_accept();
								}

							/* ��������� ������ � ������ */
							p->_readSlot(rbuf, countRead);
							}
						else {
#ifdef CBL_MPF_TRACELEVEL_EVENT_CONNECTION
							p->close(make_exception(errRead, endps));
#else
							p->close(make_exception(errRead));
#endif
							}

						brokenf = p->closed();
						}
					else {
						brokenf = true;
						}
					}
				while(!brokenf);
				}
			catch (const mpf_exception_t & ex) {
				if (auto p = wptr.lock()) {
					p->notify_hndl(ex.dcpy());
					}
				}
			catch (const dcf_exception_t & e0) {
				if (auto p = wptr.lock()) {
					p->notify_hndl(e0.dcpy());
					}
				}
			catch (const std::exception & e1) {
				if(auto p = wptr.lock()) {
					p->close(CREATE_PTR_EXC_FWD(e1));
					}
				}
			catch (...) {
				if(auto p = wptr.lock()) {
					p->close(CREATE_PTR_EXC_FWD(nullptr));
					}
				}

			if(auto p = wptr.lock()) {
				brokenf = p->closed();
				}
			else {
				brokenf = true;
				}
			}while(!brokenf);
		}

public:
	void start_io(i_xpeer_source_t::read_ready_f && r,
		i_xpeer_source_t::end_execute_f && eof,
		i_xpeer_source_t::make_ping_sequence_f && pingout,
		const sndrcv_timeline_t & pingTimeline) {

		using qtype = std::decay_t<decltype(CHECK_PTR(_ctx)->queue_manager().tcreate_await_queue<outbound_message_unit_t>(
			__FILE_LINE__,
			async_space_t::sys))>;

		_readSlot = std::move(r);
		_pingOut = std::move(pingout);
		_closeSlot = std::move(eof);

		if (pingTimeline != sndrcv_timeline_t::null) {
			_pingWatcher = std::make_unique<ping_watcher_t>(pingTimeline.value(), [this] { send_ping_request(); });
			_pingTimer = CHECK_PTR(_ctx)->create_timer_sys();
			}

		auto outlist = std::make_shared< qtype>(CHECK_PTR(_ctx)->queue_manager().tcreate_await_queue<outbound_message_unit_t>(
			__FILE_LINE__,
			async_space_t::sys));

		_outlist = outlist;

		set_options(_socket);

		auto rc = do_cycle_read(weak_from_this());
		auto wc = do_cycle_write(weak_from_this(), std::move(outlist));

		/* ����� �������� ������
		----------------------------------------------------*/
		if (_pingWatcher) {
			_pingWatcher->start();

			_pingTimer->start([wptr = weak_from_this()](auto) {
				if (auto sptr = wptr.lock()) {
					sptr->test_alive();
					return true;
					}
				else {
					return false;
					}
				}, pingTimeline.interval(), true);
			}
		}

	void write(outbound_message_unit_t && buf) {
		if (!closed()) {
			if(auto pout = _outlist.lock()) {
				if(!pout->push(std::move(buf))) {
					THROW_EXC_FWD(nullptr);
					}
				}
			else {
				THROW_EXC_FWD(nullptr);
				}
			}
		else {
			THROW_EXC_FWD(nullptr);
			}
		}

	bool ping_emit_interval_exceeded()const noexcept {
		if (_pingWatcher)
			return _pingWatcher->emit_interval_exceeded();
		else
			return false;
		}

	bool ping_accept_interval_exceeded()const noexcept {
		if (_pingWatcher)
			return _pingWatcher->accepted_interval_exceeded();
		else
			return false;
		}
};


class tcp_source_factory_impl_t :
	public std::enable_shared_from_this<tcp_source_factory_impl_t> {

	typedef tcp_source_factory_impl_t self_t;

private:
	class accept_state_t {
		boost::asio::ip::tcp::socket sock;

		accept_state_t(boost::asio::io_service & service)
			: sock(service) {
			}
		};

	typedef std::mutex lck_t;
	typedef std::unique_lock<lck_t> ctx_lck_t;

	lck_t _lck;
	std::weak_ptr<distributed_ctx_t> _ioCtx;
	boost::asio::ip::tcp::acceptor _acceptor;
	tcp_source_factory_t::evhndl_source_connection_t _evhndlConnection;
	std::atomic<bool> _stopped = false;

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	crm::utility::__xobject_counter_logger_t<self_t> _xDbgCounter;
#endif

	std::shared_ptr<distributed_ctx_t> io_ctx()noexcept {
		return std::move(_ioCtx.lock());
		}

	std::shared_ptr<const distributed_ctx_t> io_ctx()const noexcept {
		return std::move(_ioCtx.lock());
		}

	void notify(const dcf_exception_t &err)noexcept {
		auto ctx(_ioCtx.lock());
		if (ctx) {
			ctx->exc_hndl(err);
			}
		}

	void notify(const boost::system::error_code& err)noexcept {
		auto ctx(_ioCtx.lock());
		if (ctx) {
			ctx->exc_hndl(CREATE_MPF_PTR_EXC_FWD(err.message()));
			}
		}

	void _stop()noexcept {

		bool stopped = false;
		if (_stopped.compare_exchange_strong(stopped, true)) {

			boost::system::error_code ec;

			// ���������� ��������� �����������
			CHECK_NO_EXCEPTION(_acceptor.cancel(ec));
			CHECK_NO_EXCEPTION(_acceptor.close(ec));
			}
		}

	struct _begin_accept_t {
		boost::asio::ip::tcp::socket socket;

		_begin_accept_t(boost::asio::io_service & service)
			: socket(service) {
			}
		};

	void connection_execute(const std::shared_ptr<_begin_accept_t> & acceptDt,
		const boost::system::error_code &err) {

		/* ����� �������� ���������� ����������� */
		post_scope_action_t nextAccept([this] {
			begin_accept();
			});

		try {
			if (err)
				notify(err);
			else {
				CBL_MPF_TRACELEVEL_EVENT_RT0_CONNECTION_TRACE(("socked accepted connection["
					+ acceptDt->socket.remote_endpoint().address().to_string() + ":"
					+ cvt_to_str(acceptDt->socket.remote_endpoint().port()) + "]"));

				/* ��� ����������� � ������ �������� ������ ����������� */
				/* ��� ������� ������������ ����������� ��������� */
				if (!stopped()) {

					auto mpfCtx(_ioCtx.lock());
					if (mpfCtx) {

						if (!acceptDt->socket.is_open())
							THROW_MPF_EXC_FWD(nullptr);

						/* �������������� ������ ����������� */
						auto objImpl(std::make_unique<tcp_source_impl_t>(mpfCtx, std::move(acceptDt->socket)));
						/* �������� ������ ������ */
						auto pSock(std::make_unique<tcp_source_t>(std::move(std::move(objImpl))));

						/* ���������� ����������� � ���� */
						auto hndl(_evhndlConnection);
						if (hndl) {
							mpfCtx->launch_async(__FILE_LINE__, []
							(tcp_source_factory_t::evhndl_source_connection_t && hndl_,
								std::unique_ptr<tcp_source_t> && pSock_) {

								hndl_(std::move(pSock_));

								}, std::move(hndl), std::move(pSock));
							}
						}
					}
				}
			}
		catch (const crm::dcf_exception_t & exc) {
			notify(exc);
			}
		catch (...) {
			notify(CREATE_MPF_EXC_FWD(nullptr));
			}
		}

	void begin_accept() {

		if (!stopped()) {
			auto ctx_(_ioCtx.lock());
			if (ctx_) {
				std::shared_ptr<_begin_accept_t> acceptDt(new _begin_accept_t(ctx_->io_services().detail()->services()));
				std::weak_ptr<tcp_source_factory_impl_t> wthis(shared_from_this());

				_acceptor.async_accept(acceptDt->socket,
					[wthis, acceptDt](const boost::system::error_code &err) {

					auto plck0(wthis.lock());
					if (plck0) {
						plck0->connection_execute(acceptDt, err);
						}
					});
				}
			else {
				stop();
				}
			}
		}

	static boost::asio::ip::tcp::acceptor create_acceptor(std::weak_ptr<distributed_ctx_t> & ctx) {
		boost::asio::ip::tcp::acceptor accptObj(CHECK_PTR(ctx)->io_services().detail()->services());
		return std::move(accptObj);
		}

public:
	tcp_source_factory_impl_t(std::weak_ptr<distributed_ctx_t> ctx,
		tcp_source_factory_t::evhndl_source_connection_t && evhndlConnection_)
		: _ioCtx(ctx)
		, _acceptor(create_acceptor(ctx))
		, _evhndlConnection(evhndlConnection_) {
		}

	void start(const std::unique_ptr<i_endpoint_t> & endpoint) {

		if (!endpoint)
			THROW_MPF_EXC_FWD(nullptr);

		auto pTcpEP = dynamic_cast<tcp_endpoint_t*>(endpoint.get());
		if (!pTcpEP)
			THROW_MPF_EXC_FWD(nullptr);

		if (pTcpEP->port() > std::numeric_limits<uint16_t>::max())
			THROW_MPF_EXC_FWD(nullptr);
		auto port(static_cast<uint16_t>(pTcpEP->port()));

		boost::asio::ip::tcp::endpoint accpEP;
		if (pTcpEP->version() == tcp_version_t::v4) {
			accpEP = boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), port);
			}
		else if (pTcpEP->version() == tcp_version_t::v6) {
			accpEP = boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v6(), port);
			}
		else
			THROW_MPF_EXC_FWD(nullptr);

		_acceptor.open(accpEP.protocol());
		_acceptor.set_option(boost::asio::ip::tcp::acceptor::reuse_address(true));
		_acceptor.bind(accpEP);
		_acceptor.listen();

		CBL_MPF_TRACELEVEL_EVENT_RT0_CONNECTION_TRACE("tcp factory:start listining:"
			+ accpEP.address().to_string()
			+ ":" + cvt_to_str(accpEP.port()));

		/* ����� ������������ �������� �������� ����������� */
		begin_accept();
		}

	static std::shared_ptr<tcp_source_factory_impl_t> create(std::weak_ptr<distributed_ctx_t> ctx,
		tcp_source_factory_t::evhndl_source_connection_t && evhndlConnection_) {

		std::shared_ptr<tcp_source_factory_impl_t> obj(new tcp_source_factory_impl_t(ctx,
			std::move(evhndlConnection_)));
		return std::move(obj);
		}

	~tcp_source_factory_impl_t() {
		CHECK_NO_EXCEPTION(stop());
		}

	tcp_source_factory_impl_t(const tcp_source_factory_impl_t &o) = delete;
	tcp_source_factory_impl_t& operator=(const tcp_source_factory_impl_t &o) = delete;

	bool stopped()const noexcept {
		return _stopped.load(std::memory_order::memory_order_acquire);
		}

	void stop()noexcept {
		ctx_lck_t ctx(_lck);
		_stop();
		}
	};
}



/*============================================================================================
tcp_endpoint_t
==============================================================================================*/

const std::string tcp_endpoint_t::localhost_address = "127.0.0.1";

tcp_endpoint_t::tcp_endpoint_t(int port_, tcp_version_t ver_)
	: tcp_endpoint_t(localhost_address, port_, ver_){}

tcp_endpoint_t::tcp_endpoint_t(std::string_view address_, int port_, tcp_version_t ver_)
	: _address(address_)
	, _port(port_)
	, _ver(ver_) {}

const std::string& tcp_endpoint_t::address()const noexcept {
	return _address;
	}

int tcp_endpoint_t::port()const noexcept {
	return _port;
	}

connection_key_t tcp_endpoint_t::to_key()const noexcept {
	connection_key_t ck;
	ck.address = _address;
	ck.port = _port;
	return std::move(ck);
	}

tcp_version_t tcp_endpoint_t::version()const noexcept {
	return _ver;
	}

std::string tcp_endpoint_t::to_string()const noexcept {
	return to_key().to_string();
	}

address_hash_t tcp_endpoint_t::object_hash()const noexcept {
	address_hash_t h;
	apply_at_hash(h, _address);
	apply_at_hash(h, _port);
	apply_at_hash(h, _ver);

	return h;
	}

std::unique_ptr<i_endpoint_t> tcp_endpoint_t::copy()const noexcept {
	return std::make_unique<tcp_endpoint_t>(tcp_endpoint_t(*this));
	}


/*============================================================================================
tcp_source_t
==============================================================================================*/


tcp_source_t::tcp_source_t(const std::weak_ptr<distributed_ctx_t> & ctx)
	: _impl(std::make_shared<tcp_source_impl_t>(ctx, CHECK_PTR(ctx.lock())->io_services().detail()->services())) {
	}

tcp_source_t::tcp_source_t(std::unique_ptr<tcp_source_impl_t> && impl)noexcept
	: _impl(std::move(impl)) {
	}

tcp_source_t::tcp_source_t(tcp_source_t && o)noexcept
	: _impl(std::move(o._impl)) {
	}

tcp_source_t& tcp_source_t::operator=(tcp_source_t && o)noexcept {
	_impl = std::move(o._impl);
	return (*this);
	}

void  tcp_source_t::start(read_ready_f && r, end_execute_f && eof, make_ping_sequence_f && pingout, const sndrcv_timeline_t & pingTimeline) {
	if (_impl)
		_impl->start_io(std::move(r), std::move(eof), std::move(pingout), pingTimeline);
	else {
		THROW_MPF_EXC_FWD(nullptr);
		}
	}

void  tcp_source_t::write(outbound_message_unit_t && buf) {
	if (_impl)
		_impl->write(std::move(buf));
	else {
		THROW_MPF_EXC_FWD(nullptr);
		}
	}

void tcp_source_t::async_connect(const std::unique_ptr<i_endpoint_t> & endPoint, end_execute_f && onConnect) {
	CHECK_PTR(_impl)->async_connect(endPoint, std::move(onConnect));
	}

void tcp_source_t::close()noexcept {
	if (_impl)
		_impl->close();
	}

std::string tcp_source_t::saddress()const noexcept {
	if (_impl)
		return _impl->saddress();
	else
		return {};
	}

int tcp_source_t::port() const noexcept {
	if (_impl)
		return _impl->port();
	else
		return -1;
	}

bool tcp_source_t::ping_emit_interval_exceeded()const noexcept {
	if (_impl)
		return _impl->ping_emit_interval_exceeded();
	else
		return true;
	}

bool tcp_source_t::ping_accept_interval_exceeded()const noexcept {
	if (_impl)
		return _impl->ping_accept_interval_exceeded();
	else
		return true;
	}

connection_key_t tcp_source_t::connecition_key()const noexcept {
	if (_impl)
		return connection_key_t{ _impl->saddress(), _impl->port() };
	else
		return {};
	}

bool tcp_source_t::is_integrity_provided()const noexcept {
	return true;
	}

tcp_source_t::~tcp_source_t() {
	close();
	}

/*============================================================================================
tcp_source_factory_t
==============================================================================================*/


tcp_source_factory_t::tcp_source_factory_t(std::weak_ptr<distributed_ctx_t> ctx)
	: _ctx(ctx) {}

tcp_source_factory_t tcp_source_factory_t::create(std::weak_ptr<distributed_ctx_t> ctx) {
	return tcp_source_factory_t(ctx);
	}

void tcp_source_factory_t::open(evhndl_source_connection_t && evhndlConnection_,
	const std::unique_ptr<i_endpoint_t> & endpoint) {

	_impl = tcp_source_factory_impl_t::create(_ctx, std::move(evhndlConnection_));

	CHECK_PTR(_impl)->start(endpoint);
	}

tcp_source_factory_t::~tcp_source_factory_t() {
	close();
	}

bool tcp_source_factory_t::closed()const noexcept {
	if (_impl)
		return _impl->stopped();
	else
		return true;
	}

void tcp_source_factory_t::close()noexcept {
	if (_impl)
		_impl->stop();
	}

std::unique_ptr<i_xpeer_source_t> tcp_source_factory_t::create() {
	return std::make_unique<tcp_source_t>(_ctx);
	}

std::unique_ptr<i_endpoint_t> crm::tcp::make_endpoint(int port) {
	return std::make_unique<tcp_endpoint_t>(port, tcp_endpoint_t::version_t::v4);
	}
