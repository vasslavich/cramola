#pragma once


#include <memory>
#include "../protocol/i_ssource.h"


namespace crm {
namespace detail {
namespace tcp {


enum class tcp_version_t : std::uint8_t {
	undefined, v4, v6
	};


class tcp_endpoint_t final : public i_endpoint_t {
public:
	static const std::string localhost_address;

private:
	std::string _address;
	int _port;
	tcp_version_t _ver;

public:
	typedef tcp_version_t version_t;
	tcp_endpoint_t(int port, tcp_version_t ver);
	tcp_endpoint_t(std::string_view address, int port, tcp_version_t ver = tcp_version_t::v4 );

	const std::string& address()const noexcept final;
	int port()const noexcept final;
	connection_key_t to_key()const noexcept final;
	tcp_version_t version()const noexcept;
	std::unique_ptr<i_endpoint_t> copy()const  noexcept final;
	std::string to_string()const  noexcept final;
	address_hash_t object_hash()const noexcept final;
	};


class tcp_source_impl_t;
class tcp_source_factory_impl_t;


class tcp_source_factory_t;
class tcp_source_t final : public i_xpeer_source_t {
	friend class tcp_source_factory_impl_t;

private:
	std::shared_ptr<tcp_source_impl_t> _impl;

public:
	typedef tcp_endpoint_t endpoint_t;

	tcp_source_t(std::unique_ptr<tcp_source_impl_t>&& impl)noexcept;

	tcp_source_t(const std::weak_ptr<distributed_ctx_t>&);
	tcp_source_t(tcp_source_t&&)noexcept;
	tcp_source_t& operator=(tcp_source_t&&)noexcept;

	~tcp_source_t();

	tcp_source_t(const tcp_source_t&) = delete;
	tcp_source_t& operator=(const tcp_source_t&) = delete;

	void async_connect(const std::unique_ptr<i_endpoint_t>& endPoint,
		end_execute_f&& onConnect)final;

	void close()noexcept final;
	std::string saddress()const noexcept final;
	int port() const noexcept final;
	connection_key_t connecition_key()const noexcept final;
	bool is_integrity_provided()const noexcept final;

	void write(outbound_message_unit_t&& buf) final;
	void start(read_ready_f&& r, end_execute_f&& eof, make_ping_sequence_f&& pingout, const sndrcv_timeline_t& pingTimeline)final;

	bool ping_emit_interval_exceeded()const noexcept final;
	bool ping_accept_interval_exceeded()const noexcept final;
	};


class tcp_source_factory_t : public i_xpeer_source_factory_t {
public:
	typedef tcp_source_t source_object_t;

private:
	std::weak_ptr<distributed_ctx_t> _ctx;
	std::shared_ptr<tcp_source_factory_impl_t> _impl;

	tcp_source_factory_t(std::weak_ptr<distributed_ctx_t> ctx);

	void start();

public:
	static tcp_source_factory_t create(std::weak_ptr<distributed_ctx_t> ctx);
	virtual ~tcp_source_factory_t();

	tcp_source_factory_t(const tcp_source_factory_t& o) = delete;
	tcp_source_factory_t& operator=(const tcp_source_factory_t& o) = delete;

	tcp_source_factory_t( tcp_source_factory_t&&)noexcept = default;
	tcp_source_factory_t& operator=(tcp_source_factory_t&&)noexcept = default;

	void open(evhndl_source_connection_t&& evhndlConnection_,
		const std::unique_ptr<i_endpoint_t>& endpoint)final;
	bool closed()const noexcept final;
	void close()noexcept final;
	std::unique_ptr<i_xpeer_source_t> create()final;
	};
}
}

namespace tcp {

using protocol = crm::detail::tcp::tcp_version_t;
using endpoint = crm::detail::tcp::tcp_endpoint_t;
std::unique_ptr<i_endpoint_t> make_endpoint(int port);
}
}

