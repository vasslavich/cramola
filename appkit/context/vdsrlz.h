#pragma once


#include <functional>
#include <atomic>
#include "../internal.h"
#include "../eval/eval_terms.h"


namespace crm::detail {


class deserialize_vector_t {
	std::vector<std::shared_ptr<lazy_action_stack_t>> _lzv;
	std::atomic<bool> _closef{ false };
	std::atomic<bool> _cancelf{ false };

public:
	deserialize_vector_t(size_t, std::function<void(std::unique_ptr<dcf_exception_t> &&)> && ntfh);

	/*! ���������� �������� � ���� ���������� ���������� */
	template<typename TAction, typename ... TArgs>
	void launch_async_ordered(size_t hash_,
		std::string_view name_,
		typename TAction && action_, typename TArgs && ... args)noexcept {

		if(!canceled()) {
			auto idx = hash_ % _lzv.size();

			CHECK_NO_EXCEPTION_BEGIN
				if(auto l = std::atomic_load_explicit(&_lzv[idx], std::memory_order::memory_order_acquire)) {
					l->launch(name_, std::forward<TAction>(action_), std::forward<TArgs>(args)...);
					}
			CHECK_NO_EXCEPTION_END
			}
		}

	void close(bool soft = false)noexcept;
	void cancel()noexcept;

	bool canceled()const noexcept;
	bool closed()const noexcept;
	};


}