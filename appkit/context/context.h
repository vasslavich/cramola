#pragma once


#include <memory>
#include <atomic>
#include <unordered_set>
#include "../internal.h"
#include "../eval/eval_terms.h"
#include "./vdsrlz.h"
#include "../streams/internal_terms.h"
#include "../io_services/ios.h"
#include "../erremlt/erremlt_context.h"
#include "../channel_terms/events_terms.h"
#include "../channel_terms/message_subscribe_terms.h"
#include "./async_sequential.h"
#include "../time/terms.h"
#include "../logging/internal_terms.h"
#include "../time/msg_timestack.h"
#include "../time/global_time.h"
#include "../functorapp/handlers_map.h"
#include "../channel_terms/context_options.h"


namespace crm{
namespace detail {


class istreams_whandlers_table_t;
using message_frame_deserialize_tail_f = crm::utility::invoke_functor<void, std::unique_ptr<i_iol_ihandler_t> &&>;
}


class distributed_ctx_t :
	public i_timers_factory_t,
	public std::enable_shared_from_this<distributed_ctx_t> {

public:
	typedef distributed_ctx_t self_t;

	using notification_handler_t = crm::utility::invoke_functor_shared<void, std::unique_ptr<dcf_exception_t> &&>;

	using whandler_push_f = detail::i_stream_type_handler_t::whandler_push_f;
	using wstream_make_f = detail::i_stream_type_handler_t::wstream_make_f;

private:
	/*! ����� */
	std::unique_ptr<context_bindings> _options;
	/*! ��������� ���������� ��������� ��������������� �������� */
	mutable std::atomic<std::uintmax_t> _locidCounter{ 0 };
	/*! ��������� ���������� ������ �������� */
	mutable std::atomic<std::uintmax_t> _locidCommandCounter{ 0 };
	/*! �������� ���������� ��������� */
	std::unique_ptr<distributed_ctx_policies_t> _policies;
	/*! ������� �����/������ ������������ ������� */
	std::shared_ptr<shared_io_services_t> _osLayer;
	/*! ���������� ����������� */
	notification_handler_t _ntfHndl;

	/*! ������� ����� */
	std::shared_ptr<i_types_driver_t> _typesDrv;
	std::shared_ptr<detail::istreams_whandlers_table_t> _streamTypeHndl;

	/*! ����� ����� */
	crm::srlz::dcf_typemap_t _typeMap;

	/*! ���� ���������� ���������� ���������� �������� */
	std::shared_ptr<detail::lazy_action_stack_t> _lzyExestack;
	/*! ���� ���������� ���������� ������� ������������ */
	std::shared_ptr<detail::lazy_action_stack_t> _lzyExestackExtHndl;
	/*! ���� �����������, ������ ����� ����������� ���������. */
	std::shared_ptr<detail::lazy_action_stack_t> _ntfstack;

	std::shared_ptr<detail::lazy_action_stack_t> _ioWriteStack;

	std::shared_ptr<detail::subscribe_multitable_t> _subscribeTable;
	std::shared_ptr<detail::events_multitable_t> _eventsTable;

	std::shared_ptr<detail::async_sequential> _sequentialStack;
	std::shared_ptr<detail::stream_route_itable_t> _streamTbl;

#ifdef CBL_MPF_TEST_ERROR_EMULATION
	tunetest::erremlt_context _errctx;
#endif

#ifdef CBL_MPF_USE_REVERSE_EXCEPTION_TRACE
	std::unique_ptr<tunetest::reverse_exception_tracer> _reverseExcTracer;
#endif

	std::unique_ptr<detail::deserialize_vector_t> _dzlv;
	std::unique_ptr<detail::deserialize_vector_t> _dzlvExt;

	std::array<std::mutex, 32> _outboundConnectionEntries;

	std::thread _traceTh;

#ifdef CBL_USE_OBJECTS_MONITOR
	detail::object_rootable_collection _rootCollection;
#endif

	/*! ������� ������������ �������� */
	std::atomic<bool> _closedf{ false };
	/*! ������� ��������� */
	std::atomic<bool> _canceledf{ false };
	/*! ������� ������� */
	std::atomic<bool> _startedf{false};

	/*! �������� �������� */
	std::unique_ptr<detail::queue_manager_t> _qMngr;

	/*! ��������� ���������� ��������� */
	std::atomic<std::uintmax_t> _messageIdCnt{ 0 };

	detail::handlers_map _handlersMap;

	void initialize_objects();
	void initialize_typemap_drv(whandler_push_f && streamWHandlerPush);

	void zero_layer_ntf_hndl(std::unique_ptr<dcf_exception_t> ntf)noexcept;
	std::shared_ptr<detail::lazy_action_stack_t> ntfstack()const;

	/*! ���������� �������� � ���� ���������� ���������� */
	template<typename TAction, typename ... TArgs>
	static void launch_async(const std::shared_ptr<detail::lazy_action_stack_t> & stck,
		std::string_view name,
		typename TAction && action_, typename TArgs && ... args)noexcept {

		CHECK_NO_EXCEPTION_BEGIN
			if(stck) {
				stck->launch(name,
					std::forward<TAction>(action_),
					std::forward<TArgs>(args)...);
				}
		CHECK_NO_EXCEPTION_END
		}

	std::atomic<std::uintmax_t> __tmpfile_id = 0;
	std::wstring make_tmpfile();


	void trace_state_start();
	void trace_state_body()noexcept;
	void trace_start_cose()noexcept;

public:
	using subscribe_line_t = detail::message_subscribe_assist_t::handler_t;
	using events_line_t = detail::event_subscribe_assist_t::event_handler_t;

	subscribe_line_t subscribe_make_line(detail::rtl_level_t l);
	events_line_t events_make_line();

	static std::string time_to_str(const std::chrono::system_clock::time_point & tp);
	
	virtual ~distributed_ctx_t();

	distributed_ctx_t(const distributed_ctx_t&) = delete;
	distributed_ctx_t& operator=(const distributed_ctx_t&) = delete;

	distributed_ctx_t(distributed_ctx_t &&) = default;
	distributed_ctx_t& operator=(distributed_ctx_t &&) = default;

	void close()noexcept;
	void stop()noexcept;
	bool stopped()const noexcept;

	void disable_errormod()noexcept;
	void enable_errormod()noexcept;

	std::chrono::system_clock::duration default_timeline()const noexcept;

	std::shared_ptr<i_stream_whandler_t> pop_stream_writer(std::unique_ptr<i_stream_zyamba_identity_t> && h,
		std::unique_ptr<i_stream_zyamba_cover_data_t> &&)const;
	void push_stream_writer(const stream_source_descriptor_t& streamIdentity)noexcept;

	std::shared_ptr<i_types_driver_t> types_driver();
	std::shared_ptr<i_types_driver_t> types_driver()const;

	std::unique_ptr<i_xtimer_oncecall_t> create_timer_oncecall_sys();
	std::unique_ptr<i_xtimer_oncecall_t> create_timer_oncecall_sys(
		i_xtimer_oncecall_t::timer_out_t && hndl,
		const std::chrono::milliseconds & timeout);
	std::unique_ptr<i_xtimer_t> create_timer_sys(std::chrono::milliseconds interval);
	std::unique_ptr<i_xtimer_t> create_timer_sys();
	std::unique_ptr<i_xdeffered_action_timer_t> create_timer_deffered_action_sys(
		trace_tag&& tag,
		i_xdeffered_action_timer_t::timer_out_t && hndl,
		const std::chrono::milliseconds & timeout);
	void register_timer_deffered_action_sys(
		trace_tag&&,
		i_xdeffered_action_timer_t::timer_out_t && hndl,
		const std::chrono::milliseconds & timeout);

	std::unique_ptr<i_xtimer_oncecall_t> create_timer_oncecall();
	std::unique_ptr<i_xtimer_oncecall_t> create_timer_oncecall(
		i_xtimer_oncecall_t::timer_out_t && hndl,
		const std::chrono::milliseconds & timeout);
	std::unique_ptr<i_xtimer_t> create_timer(std::chrono::milliseconds interval);
	std::unique_ptr<i_xtimer_t> create_timer();
	std::unique_ptr<i_xdeffered_action_timer_t> create_timer_deffered_action(
		trace_tag&& tag,
		i_xdeffered_action_timer_t::timer_out_t && hndl,
		const std::chrono::milliseconds & timeout);

	std::unique_ptr<i_xtimer_oncecall_t> create_timer_oncecall(async_space_t spc);
	std::unique_ptr<i_xtimer_oncecall_t> create_timer_oncecall(async_space_t spc,
		i_xtimer_oncecall_t::timer_out_t && hndl,
		const std::chrono::milliseconds & timeout);
	std::unique_ptr<i_xtimer_t> create_timer(async_space_t spc, std::chrono::milliseconds interval);
	std::unique_ptr<i_xtimer_t> create_timer(async_space_t spc);
	std::unique_ptr<i_xdeffered_action_timer_t> create_timer_deffered_action(
		trace_tag&& tag,
		async_space_t spc,
		i_xdeffered_action_timer_t::timer_out_t && hndl,
		const std::chrono::milliseconds & timeout);

	distributed_time_t time()const;
	double asyncpool_loading_factor()const noexcept;
	std::string curr_time_str()const;
	global_object_identity_t generate_message_id();
	crm::srlz::dcf_typemap_t& typemap();
	const crm::srlz::dcf_typemap_t& typemap()const;
	const detail::queue_manager_t& queue_manager()const;
	detail::queue_manager_t& queue_manager();
	std::shared_ptr<detail::lazy_action_stack_t> lzy_stack()const;
	std::shared_ptr<detail::lazy_action_stack_t> exthndl_stack()const;
	std::shared_ptr<detail::lazy_action_stack_t> timers_stack()const;
	std::shared_ptr<detail::lazy_action_stack_t> timers_stack_ext()const;
	const distributed_ctx_policies_t& policies()const;
	const shared_io_services_t& io_services()const;
	shared_io_services_t& io_services();
	const channel_identity_t& domain_id()const noexcept;
	const identity_descriptor_t& identity()const noexcept;

	void exc_hndl(std::unique_ptr<dcf_exception_t> && ntf)const noexcept;
	void exc_hndl(const dcf_exception_t & exc)const noexcept;
	void exc_hndl(dcf_exception_t && exc)const noexcept;

	void log_time(const global_object_identity_t & id,
		const std::string & tag,
		const std::chrono::microseconds & mcs);

	void log_time(const global_object_identity_t & id,
		const global_object_identity_t & parent,
		const std::string & tag,
		const std::chrono::microseconds & mcs);

#ifdef CBL_MPF_ENABLE_TIMETRACE
	void log_time(const global_object_identity_t & id,
		const std::string & tag,
		detail::hanlder_times_stack_t::clocks_t && clocks);
#endif

	/*! ���������� �������� � ���� ���������� ���������� */
	template<typename TAction, typename ... TArgs>
	void launch_async(std::string_view name,
		typename TAction && action_, typename TArgs && ... args)noexcept {

		if(!stopped())
			launch_async(lzy_stack(), name, std::forward<TAction>(action_), std::forward<TArgs>(args)...);
		}

	/*! ���������� �������� ����������� ���� ���������� ���������� */
	template<typename TAction, typename ... TArgs>
	void launch_async_exthndl(std::string_view name,
		typename TAction && action_, typename TArgs && ... args)noexcept {

		CHECK_NO_EXCEPTION_BEGIN
			if(!stopped())
				launch_async(exthndl_stack(), name, std::forward<TAction>(action_), std::forward<TArgs>(args)...);
		CHECK_NO_EXCEPTION_END
		}

	/*! ���������� �������� ����������� ���� ���������� ���������� */
	template<typename TAction, typename ... TArgs>
	void launch_async(async_space_t scx,
		std::string_view name,
		typename TAction && action_, typename TArgs && ... args)noexcept {

		CHECK_NO_EXCEPTION_BEGIN
			if(!stopped()) {
				if(scx == async_space_t::ext) {
					launch_async_exthndl(name, std::forward<TAction>(action_), std::forward<TArgs>(args)...);
					}
				else if(scx == async_space_t::sys) {
					launch_async(name, std::forward<TAction>(action_), std::forward<TArgs>(args)...);
					}
				else {
					FATAL_ERROR_FWD(nullptr);
					}
				}
		CHECK_NO_EXCEPTION_END
		}

	/*! ���������� �������� ����������� ���� ���������� ���������� */
	template<typename TAction, typename ... TArgs>
	void launch_async_timer_handler(async_space_t scx,
		std::string_view name,
		typename TAction && action_, typename TArgs && ... args)noexcept {

		CHECK_NO_EXCEPTION_BEGIN
			if(!stopped()) {
				if(scx == async_space_t::ext) {
					launch_async(timers_stack_ext(),
						name,
						std::forward<TAction>(action_),
						std::forward<TArgs>(args)...);
					}
				else if(scx == async_space_t::sys) {
					launch_async(timers_stack(),
						name,
						std::forward<TAction>(action_),
						std::forward<TArgs>(args)...);
					}
				else {
					FATAL_ERROR_FWD(nullptr);
					}
				}
		CHECK_NO_EXCEPTION_END
		}


	/*! ���������� �������� � ���� ���������� ���������� */
	template<typename TAction, typename ... TArgs>
	void launch_async_ordered(size_t hash_,
		std::string_view name_,
		typename TAction && action_, typename TArgs && ... args)noexcept {

		_dzlv->launch_async_ordered(hash_, name_, std::forward<TAction>(action_), std::forward<TArgs>(args)...);
		}

	template<typename TAction, typename ... TArgs>
	void launch_async_ordered(async_space_t scx, 
		size_t hash_,
		std::string_view name_,
		typename TAction && action_,
		typename TArgs && ... args)noexcept {

		if(scx == async_space_t::sys) {
			_dzlv->launch_async_ordered(hash_, name_, std::forward<TAction>(action_), std::forward<TArgs>(args)...);
			}
		else if(scx == async_space_t::ext){
			_dzlvExt->launch_async_ordered(hash_, name_, std::forward<TAction>(action_), std::forward<TArgs>(args)...);
			}
		else {
			FATAL_ERROR_FWD(nullptr);
			}
		}

	std::shared_ptr<detail::lazy_action_stack_t> io_write_lze()const noexcept;

	size_t concurrency_size( async_space_t )const noexcept;

	template<typename THandler,
		typename ... AL >
	void outbound_connection_lock_entry(size_t key, THandler && h, AL && ... al ) {
		std::lock_guard lck(_outboundConnectionEntries.at(key % _outboundConnectionEntries.size()));
		h(std::forward<AL>(al)...);
		}

protected:
	crm::srlz::dcf_typemap_t& get_typemap();
	const crm::srlz::dcf_typemap_t& get_typemap()const;

	void ctr();

/*! public for std::make_shared only */
public:
	distributed_ctx_t(std::unique_ptr< context_bindings> && options,
		std::unique_ptr<distributed_ctx_policies_t>&& pl );

private:
	std::unique_ptr<i_iol_ihandler_t> decode(std::shared_ptr<srlz::i_type_ctr_i_srlzd_t<i_iol_handler_t>> usrlz,
		detail::message_frame_unit_t && bin)const;

	std::unique_ptr<i_iol_ihandler_t> decode(std::shared_ptr<srlz::i_type_ctr_serialized_trait_t> usrlz,
		detail::message_frame_unit_t && bin)const;

	std::unique_ptr<i_iol_ihandler_t> decode(std::shared_ptr<srlz::i_type_ctr_base_t> usrlz,
		detail::message_frame_unit_t && bin)const;

	void decode_no_report(i_iol_ihandler_t & iHndl,
		srlz::serialized_base_r & pISrlz_,
		detail::_packet_t && frame_)const;

	std::unique_ptr<i_iol_ihandler_t> decode_report(i_iol_ihandler_t & iHndl,
		srlz::serialized_base_r & pISrlz,
		detail::_packet_t && frame_)const noexcept;

	void decode_no_report(i_iol_ihandler_t & iHndl,
		srlz::decouple_1<i_iol_handler_t> & ISrlz_,
		detail::_packet_t && frame_)const;

	std::unique_ptr<i_iol_ihandler_t> decode_report(i_iol_ihandler_t & iHndl,
		srlz::decouple_1<i_iol_handler_t> & ISrlz_,
		detail::_packet_t && frame_)const noexcept;

	std::unique_ptr<i_iol_ihandler_t> deserialize_internal(detail::message_frame_unit_t && bin)const;

public:
#ifdef CBL_MPF_TEST_ERROR_EMULATION
	std::string get_errormod_counters_s()const noexcept;
	bool test_emulation_error()const noexcept;
	bool test_emulation_connection_corrupt(detail::rtl_level_t)const noexcept;
	void chechthrow_emulation_error(int tag, detail::rtl_level_t l, const std::wstring & file, int line)const;
	void chechthrow_emulation_error(int tag, const std::wstring & file, int line)const;
	
	void chechthrow_emulation_error(int tag, detail::rtl_level_t l, std::string stracex, const std::wstring & file, int line)const;
	void chechthrow_emulation_error(int tag, std::string stracex, const std::wstring & file, int line)const;

	void chechthrow_emulation_error(int tag, detail::rtl_level_t l, const detail::keycounter_trace::key_t & ktracex, const std::wstring & file, int line)const;
	void chechthrow_emulation_error(int tag, const detail::keycounter_trace::key_t & ktracex, const std::wstring & file, int line)const;
#endif

#ifdef CBL_MPF_USE_REVERSE_EXCEPTION_TRACE
	void add_reverse_exception_trace(const std::wstring & key)const noexcept;
	std::wstring get_reverse_exception_trace()const noexcept;
#endif

	using sequential_handler = detail::async_sequential_push_handler;

	sequential_handler get_sequential_handler(async_space_t scx);

	local_object_id_t make_local_object_identity()const noexcept;
	local_command_identity_t make_command_identity()const noexcept;
	subscriber_event_id_t make_event_identity()const noexcept;
	global_object_identity_t make_global_object_identity()const noexcept;
	channel_identity_t make_channel_identity()const noexcept;
	sx_uuid_t make_global_object_identity_128()const noexcept;

	static size_t subscribe_address_user_range();

	std::unique_ptr<i_iol_ihandler_t> deserialize(detail::rtl_level_t l, detail::message_frame_unit_t && bin)const;

	void deserialize(detail::rtl_level_t l, detail::message_frame_unit_t && bin,
		detail::message_frame_deserialize_tail_f && tail);

	template<typename TBindings,
		typename TPolicies = crm::distributed_ctx_policies_t,
		typename std::enable_if_t<(
			is_context_bindings_v<TBindings> &&
			is_context_policies_v<TPolicies>
			), int> = 0>
	static std::shared_ptr<distributed_ctx_t> make(TBindings&& opt, TPolicies&& pl = {}) {
		using bindings_type = typename std::decay_t<TBindings>;
		using policies_type = typename std::decay_t<TPolicies>;

		auto dctx = std::make_shared<distributed_ctx_t>(
			std::make_unique<bindings_type>(std::forward<TBindings>(opt)),
			std::make_unique<policies_type>(std::forward<TPolicies>(pl)));
		dctx->ctr();

		return std::move(dctx);
		}

	//template<typename TBindings,
	//	typename std::enable_if_t<is_context_bindings_v<TBindings>, int> = 0>
	//static std::shared_ptr<distributed_ctx_t> make(TBindings  && opt) {
	//	return make(std::forward<TBindings>(opt), distributed_ctx_policies_t::get_default());
	//	}

	std::shared_ptr<detail::lazy_action_stack_t> get_async_stack( async_space_t scx_  ){
		return scx_ == async_space_t::sys ? _lzyExestack : _lzyExestackExtHndl;
		}

	template<typename ... TItem>
	auto register_message_type()->
		typename std::enable_if_t < (
			srlz::detail::is_all_srlzd_v<TItem...> &&
			!is_any_message_handler_based_v<TItem...> &&
			(sizeof...(TItem)) >= 1
			), void> {
		using icomposed_type = typename detail::istrob_message_envelop_t<TItem...>;
		using ocomposed_type = typename detail::ostrob_message_envelop_t<TItem...>;

		if(typemap().add_drv_type<icomposed_type, i_iol_handler_t>())
			THROW_EXC_FWD(nullptr);

		if(typemap().add_drv_type<ocomposed_type, icomposed_type, i_iol_handler_t>())
			THROW_EXC_FWD(nullptr);
		}

	template<typename THandler>
	void register_handler(function_descriptor&& fd, THandler&& h_) {
		_handlersMap.add_handler(std::move(fd), std::forward<THandler>(h_));
		}


public:
	template<typename _XMakerWriter,
		typename XMakerWriter = std::decay_t<_XMakerWriter>,
		typename std::enable_if_t<is_write_stream_typemaker_v<XMakerWriter>, int> = 0>
	void register_stream_type(_XMakerWriter && wstreamCtr ) {

		/*! register make object for that stream type */
		CHECK_PTR(_streamTypeHndl)->add_wstream_type_maker(std::forward<_XMakerWriter>(wstreamCtr));
		}

	template<typename _XMakerWriter,
		typename XMakerWriter = std::decay_t<_XMakerWriter>,
		typename std::enable_if_t<(
			is_stream_slot_v<XMakerWriter>
			), int> = 0>
	void register_stream_type() {
		using stream_params_type = typename has_type_declaration_stream_params<XMakerWriter>::params_type;
		using stream_identity_type = typename stream_blob_identity_t<stream_params_type>;

		if constexpr (is_stream_slot_based_by_interface_v<XMakerWriter>) {

			/* constructor for custom stream object inherited from interface */
			CHECK_PTR(_streamTypeHndl)->add_wstream_type_maker(detail::custom_stream_maker_as_interface_t<XMakerWriter>{weak_from_this()});
			}
		else if constexpr (is_stream_slot_based_by_members_v<XMakerWriter>) {

			/* constructor for custom stream object from its members */
			CHECK_PTR(_streamTypeHndl)->add_wstream_type_maker(detail::custom_stream_maker_as_members_t<XMakerWriter>{weak_from_this()});
			}
		else {
			static_assert(fault_instantiaion_detect<XMakerWriter>::value, __FILE_LINE__);
			}

		register_stream_identity_type<stream_identity_type>();
		}

	template<typename TFuncMake, typename TContainer>
	struct wstream_container_maker_t {
		TFuncMake _f;

		auto stream_typeid()const noexcept {
			return wstream_container_t<typename TContainer>::stream_blob_typeid;
			}

		auto operator()(std::unique_ptr<i_stream_zyamba_identity_t>&& h, std::unique_ptr<i_stream_zyamba_cover_data_t>&& cd) const {
			return _f(std::move(h), std::move(cd));
			}

		template<typename XFuncMake>
		wstream_container_maker_t(XFuncMake&& f)noexcept
			: _f(std::forward<XFuncMake>(f)) {}
		};

	template<typename TFuncMake, typename TContainer>
	auto get_container_make(TFuncMake&& f) {
		using type = wstream_container_maker_t<TFuncMake, TContainer>;
		return type(std::forward<TFuncMake>(f));
		}

	template<typename TContainer>
	void register_stream_container_type() {
		using container_type = typename std::decay_t<TContainer>;
		using wstream_type = wstream_container_t<container_type>;

		auto mkf = [ctx = weak_from_this()](std::unique_ptr<i_stream_zyamba_identity_t>&& h, std::unique_ptr<i_stream_zyamba_cover_data_t>&& cd) {
			return wstream_handler_blob_t::make(ctx.lock(), std::make_unique<wstream_type>(std::move(h), std::move(cd)));
			};

		register_stream_type(get_container_make<decltype(mkf), TContainer>(std::move(mkf)));
		}

	template<typename T,
		typename std::enable_if_t<is_stream_identity_trait_v<T>, int> = 0>
	void register_stream_identity_type() {
		static_assert(std::is_base_of_v<i_stream_zyamba_identity_t, std::decay_t<T>>, __FILE_LINE__);

		if(_typeMap.add_drv_type<T, i_stream_zyamba_identity_t>()) {
			SERIALIZE_THROW_EXC_FWD(nullptr);
			}
		}

	template<typename T,
		typename std::enable_if_t<is_stream_cover_data_trait_v<T>, int> = 0>
	void register_stream_cover_data_type() {
		static_assert(std::is_base_of_v < i_stream_zyamba_cover_data_t, std::decay_t<T>>, __FILE_LINE__);

		if(_typeMap.add_drv_type<T, i_stream_zyamba_cover_data_t>()) {
			SERIALIZE_THROW_EXC_FWD(nullptr);
			}
		}

	std::shared_ptr<detail::stream_route_itable_t> streams_handler() noexcept;
	std::shared_ptr<const detail::stream_route_itable_t> streams_handler()const noexcept;

private:
	void register_file_stream_type();

public:
	detail::functor_result invoke_handler(detail::functor_trait&& fd);

#ifdef CBL_USE_OBJECTS_MONITOR
	void object_monitor_set(const sx_locid_t & id_, std::weak_ptr<i_terminatable_t> wptr)noexcept;
	void object_monitor_unset(const sx_locid_t & id_)noexcept;
	void object_monitor_check()noexcept;
#endif

	void start();

	size_t streams_count()const noexcept;
	};
}

