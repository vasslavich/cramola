#pragma once


#include "../base_predecl.h"
#include "../internal_invariants.h"
#include "../utilities/check_validate/checker_seqcall.h"


namespace crm::detail{


class async_sequential;
struct async_sequential_push_handler {
	using inbound_stack_key = std::uintmax_t;

	static std::atomic<inbound_stack_key> id_counter;

	std::weak_ptr<async_sequential> wptr;
	inbound_stack_key id{ id_counter++ };
	async_space_t scx{ async_space_t::undef };

	template<typename THandler, typename ... TArgs>
	void push(std::string_view name, typename THandler&& hf, typename TArgs&& ... args) {
		if (auto p = wptr.lock()) {
			p->push((*this), name, std::forward<THandler>(hf), std::forward<TArgs>(args)...);
			}
		}

	async_sequential_push_handler()noexcept {}

	async_sequential_push_handler(const async_sequential_push_handler&) = delete;
	async_sequential_push_handler& operator=(const async_sequential_push_handler&) = delete;

	async_sequential_push_handler(async_sequential_push_handler&&)noexcept = default;
	async_sequential_push_handler& operator=(async_sequential_push_handler&&)noexcept = default;
	};


class async_sequential : public std::enable_shared_from_this<async_sequential>{
	using inbound_stack_key = async_sequential_push_handler::inbound_stack_key;
	friend struct async_sequential_push_handler;

private:
	std::weak_ptr<distributed_ctx_t> _aq;

	template<typename THandler, typename ... TArgs>
	void push( const async_sequential_push_handler & h, std::string_view name, typename THandler && hf, typename TArgs && ... args ){

		if(auto c = _aq.lock()) {
			c->launch_async_ordered(h.scx, h.id, name, std::forward<THandler>(hf), std::forward<TArgs>(args)...);
			}
		}

public:
	async_sequential( std::weak_ptr<distributed_ctx_t> aq, size_t size_ );

	async_sequential_push_handler get_handler( async_space_t scx_ );
	};
}

