#include <ctime>
#include <chrono>
#include "../context.h"


using namespace crm;
using namespace crm::detail;


const distributed_time_t::timepoint_t& distributed_time_t::timepoint()const {
	return _tp;
	}

std::string distributed_time_t::to_str()const {
	auto t = std::chrono::system_clock::to_time_t( timepoint() );
	std::string st( std::ctime( &t ) );
	return std::move( st );
	}

