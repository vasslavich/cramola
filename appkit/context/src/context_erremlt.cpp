#include "../../internal.h"


using namespace crm;
using namespace crm::detail;
using namespace crm::tunetest;


#ifdef CBL_MPF_TEST_ERROR_EMULATION

bool distributed_ctx_t::test_emulation_error()const noexcept {
	return _errctx.test_emulation_error();
	}

bool distributed_ctx_t::test_emulation_connection_corrupt(rtl_level_t l) const noexcept {
	return _errctx.test_emulation_error_connection(l);
	}

void distributed_ctx_t::disable_errormod()noexcept {
	_errctx.disable();
	}

void distributed_ctx_t::enable_errormod()noexcept{
	_errctx.enable();
	}

std::string distributed_ctx_t::get_errormod_counters_s()const noexcept {
	return _errctx.get_errormod_counters_s();
	}

void distributed_ctx_t::chechthrow_emulation_error(int tag, rtl_level_t l, const std::wstring & file, int line)const {
	_errctx.chechthrow_emulation_error(tag, l, file, line);
	}

void distributed_ctx_t::chechthrow_emulation_error(int tag, const std::wstring & file, int line )const {
	_errctx.chechthrow_emulation_error(tag, file, line);
	}

void distributed_ctx_t::chechthrow_emulation_error(int tag, rtl_level_t l, std::string stracex, const std::wstring & file, int line)const {
	_errctx.chechthrow_emulation_error(tag, l, stracex, file, line);
	}

void distributed_ctx_t::chechthrow_emulation_error(int tag, std::string stracex, const std::wstring & file, int line)const {
	_errctx.chechthrow_emulation_error(tag, stracex, file, line);
	}

void distributed_ctx_t::chechthrow_emulation_error(int tag,
	rtl_level_t l, const keycounter_trace::key_t & ktracex, const std::wstring & file, int line)const {

	_errctx.chechthrow_emulation_error(tag, l, ktracex, file, line);
	}

void distributed_ctx_t::chechthrow_emulation_error(int tag,
	const keycounter_trace::key_t & ktracex, const std::wstring & file, int line)const {

	_errctx.chechthrow_emulation_error(tag, ktracex, file, line);
	}

#endif



