#include <sstream>
#include <ctime>
#include <chrono>
#include <filesystem>
#include "../../internal.h"
#include "../../queues/qdef.h"
#include "../../channel_terms/subscr_tbl.h"
#include "../../streams/istreams_routetbl.h"


using namespace crm;
using namespace crm::detail;


void distributed_ctx_t::start() {
	bool stf{false};
	if (_startedf.compare_exchange_strong(stf, true)) {
		io_services().start(policies().io_threads_count());
		trace_state_start();
		}
	}

void distributed_ctx_t::close() noexcept {

	bool closed = false;
	if(_closedf.compare_exchange_strong(closed, true)) {

		if(_dzlv) {
			_dzlv->cancel();
			}

		if(_dzlvExt) {
			_dzlvExt->cancel();
			}

		stop();
		io_services().close();

		auto lzy1(std::atomic_exchange(&_lzyExestack, std::shared_ptr<lazy_action_stack_t>()));
		auto lzy2(std::atomic_exchange(&_lzyExestackExtHndl, std::shared_ptr<lazy_action_stack_t>()));
		auto ntf(std::atomic_exchange(&_ntfstack, std::shared_ptr<lazy_action_stack_t >()));
		auto ioW(std::atomic_exchange(&_ioWriteStack, std::shared_ptr<lazy_action_stack_t >()));

		if(_dzlv) {
			_dzlv->close(false);
			}

		if(_dzlvExt) {
			_dzlvExt->close(false);
			}

		/* ������ */
		if(lzy1) {
			lzy1->cancel();
			}
		if(lzy2) {
			lzy2->cancel();
			}
		if(ntf) {
			ntf->cancel();
			}
		if(ioW) {
			ioW->cancel();
			}

		/* �������� */
		if(lzy1) {
			lzy1->close(false);
			}
		if(lzy2) {
			lzy2->close(false);
			}
		if(ntf) {
			ntf->close(false);
			}
		if(ioW) {
			ioW->close(false);
			}

		trace_start_cose();
		}
	}

void distributed_ctx_t::stop()noexcept {
	_canceledf.store( true, std::memory_order::memory_order_release );
	io_services().stop();
	}

bool distributed_ctx_t::stopped()const noexcept {
	return _canceledf.load( std::memory_order::memory_order_acquire );
	}

distributed_ctx_t::~distributed_ctx_t() {
	close();
	}

global_object_identity_t distributed_ctx_t::generate_message_id() {

	const auto idx = ++_messageIdCnt;
	global_object_identity_t r;
	r.at<decltype(idx)>(global_object_identity_t::count_of_as<decltype(idx)>() - 1) = idx;

	return r;
	}

const srlz::dcf_typemap_t& distributed_ctx_t::typemap()const {
	return _typeMap;
	}

 srlz::dcf_typemap_t& distributed_ctx_t::typemap() {
	return _typeMap;
	}

srlz::dcf_typemap_t& distributed_ctx_t::get_typemap() {
	return _typeMap;
	}

const srlz::dcf_typemap_t& distributed_ctx_t::get_typemap()const {
	return _typeMap;
	}

std::string distributed_ctx_t::curr_time_str()const {
	return time_to_str( std::chrono::system_clock::now() );
	}

std::string distributed_ctx_t::time_to_str( const std::chrono::system_clock::time_point & tp ) {
	auto t = std::chrono::system_clock::to_time_t( tp );
	std::string sbuild;
	auto sct = std::string( std::ctime( &t ) );
	if( sct.at( sct.size() - 1 ) == '\n' )
		sct.erase( sct.begin() + sct.size() - 1 );
	sbuild += sct;
	sbuild += ":" + detail::cvt_to_str( std::chrono::duration_cast<std::chrono::nanoseconds>(tp.time_since_epoch()).count() );
	return std::move( sbuild );
	}

distributed_ctx_t::distributed_ctx_t(std::unique_ptr< context_bindings>&& options,
	std::unique_ptr<distributed_ctx_policies_t>&& pl)

	: /* ������������� ������� */
	_options(std::move(options))
	, _locidCounter(subscribe_address_user_range())
	/* �������� */
	, _policies(std::move(pl))

#ifdef CBL_MPF_TEST_ERROR_EMULATION
	, _errctx(policies())
#endif

#ifdef CBL_MPF_USE_REVERSE_EXCEPTION_TRACE
	, _reverseExcTracer(std::make_unique<tunetest::reverse_exception_tracer>())
#endif

{}

void distributed_ctx_t::ctr() {

	_typesDrv = std::make_shared<types_driver_t>();
	_osLayer = std::make_shared<crm::shared_io_services_t>(_ntfHndl);

#ifdef CBL_USE_OBJECTS_MONITOR
	_rootCollection.set_ctx(weak_from_this());
#endif

	_sequentialStack = std::make_shared<detail::async_sequential>( weak_from_this(),
		(std::min)(_policies->thread_pool_maxsize(), _policies->thread_pool_extern_handlers_maxsize()) );

	/* �������� ��������� �������� */
	_qMngr = std::make_unique<queue_manager_t>( queue_manager_t( shared_from_this() ) );

	/* ������������� ����� ������� */
	initialize_objects();

	/* ��������� ��������� ������������� ��������� ����� ������ */
	initialize_typemap_drv([wptr = weak_from_this()](const stream_source_descriptor_t& d, std::shared_ptr<i_stream_whandler_t>&& w){
		if(auto s = wptr.lock()) {
			s->_options->wstream(d, std::move(w));
			}
		});

	_subscribeTable = std::make_shared<subscribe_multitable_t>(weak_from_this(), concurrency_size(async_space_t::sys));
	_eventsTable = std::make_shared<events_multitable_t>( weak_from_this(), concurrency_size( async_space_t::sys ) );

	_streamTbl = std::make_shared<stream_route_itable_t>(weak_from_this());
	}

std::shared_ptr<stream_route_itable_t> distributed_ctx_t::streams_handler() noexcept {
	return _streamTbl;
	}

std::shared_ptr<const stream_route_itable_t> distributed_ctx_t::streams_handler()const noexcept {
	return _streamTbl;
	}

const queue_manager_t& distributed_ctx_t::queue_manager()const {
	if( !_qMngr )
		THROW_MPF_EXC_FWD(nullptr);
	return *(_qMngr);
	}

queue_manager_t& distributed_ctx_t::queue_manager() {
	if( !_qMngr )
		THROW_MPF_EXC_FWD(nullptr);
	return *(_qMngr);
	}

std::shared_ptr<lazy_action_stack_t> distributed_ctx_t::lzy_stack()const {
	return std::atomic_load_explicit( &_lzyExestack, std::memory_order::memory_order_acquire );
	}

std::shared_ptr<lazy_action_stack_t> distributed_ctx_t::exthndl_stack()const {
	return std::atomic_load_explicit( &_lzyExestackExtHndl, std::memory_order::memory_order_acquire );
	}

std::shared_ptr<lazy_action_stack_t> distributed_ctx_t::timers_stack()const {
	return std::atomic_load_explicit( &_lzyExestack, std::memory_order::memory_order_acquire );
	}

std::shared_ptr<lazy_action_stack_t> distributed_ctx_t::timers_stack_ext()const {
	return std::atomic_load_explicit( &_lzyExestackExtHndl, std::memory_order::memory_order_acquire );
	}

distributed_time_t distributed_ctx_t::time()const {
	return distributed_time_t();
	}

double distributed_ctx_t::asyncpool_loading_factor()const noexcept {
	auto lzy( lzy_stack() );
	if( lzy ) {
		const auto ldf = (double)lzy->stacked_count() / (double)lzy->conncurrency_size();
		return ldf;
		}
	else
		return 0;
	}

void distributed_ctx_t::initialize_objects() {

	/* ������� ��������� ����������� */
	std::weak_ptr<distributed_ctx_t> wthis(shared_from_this());
	auto ntfHndlPool = [wthis](std::unique_ptr<dcf_exception_t> ntf) {
		auto sthis(wthis.lock());
		if(sthis)
			sthis->exc_hndl(std::move(ntf));
		};

	/* ������� ��������� ����������� ���������� ������ */
	auto ntfHndlZero = [wthis](std::unique_ptr<dcf_exception_t> ntf) {
		auto sthis(wthis.lock());
		if(sthis)
			sthis->zero_layer_ntf_hndl(std::move(ntf));
		};

	_dzlv = std::make_unique<deserialize_vector_t>(_policies->thread_pool_maxsize(), ntfHndlPool);
	_dzlvExt = std::make_unique<deserialize_vector_t>(1, ntfHndlPool);

	/* ������������� ������� ���������� �������� */
	auto old1(std::atomic_exchange(&_lzyExestack, lazy_action_stack_t::make(identity().to_str() + ":distributed_ctx_t::exestack:sys",
		ntfHndlPool,
		2,
		_policies->threadpool_wait_item())));

	/* ������������� ����� ������� ������������ */
	auto old2(std::atomic_exchange(&_lzyExestackExtHndl, lazy_action_stack_t::make(identity().to_str() + ":distributed_ctx_t::exestack:ext",
		ntfHndlPool,
		_policies->thread_pool_extern_handlers_maxsize(),
		_policies->threadpool_wait_item())));

	/* ������������� ����� ����������� */
	auto ntfStckOld(std::atomic_exchange(&_ntfstack, lazy_action_stack_t::make(identity().to_str() + ":distributed_ctx_t::notifications",
		ntfHndlZero,
		1,
		_policies->threadpool_wait_item())));

	auto ioWOld(std::atomic_exchange(&_ioWriteStack, lazy_action_stack_t::make(identity().to_str() + ":distributed_ctx_t::io-write",
		ntfHndlZero,
		1,
		_policies->threadpool_wait_item())));

	if(old1)
		old1->close();

	if(old2)
		old2->close();

	if(ntfStckOld)
		ntfStckOld->close();

	if(ioWOld)
		ioWOld->close();
	}

std::shared_ptr<lazy_action_stack_t> distributed_ctx_t::io_write_lze()const noexcept {
	return std::atomic_load_explicit(&_ioWriteStack, std::memory_order::memory_order_acquire);
	}

std::wstring distributed_ctx_t::make_tmpfile() {
	auto tmpfid = std::to_wstring( ++__tmpfile_id );
	auto thisId = std::to_wstring((uintptr_t)this);
	auto timeId = std::to_wstring( std::chrono::system_clock::now().time_since_epoch().count() );

	std::filesystem::path path = policies().upload_folder();
	if( path.empty() )
		path /= std::filesystem::current_path();
	
	path /= (thisId + L"_" + tmpfid + L"_" + timeId);

	return path.string<std::wstring::value_type>();
	}

void distributed_ctx_t::zero_layer_ntf_hndl( std::unique_ptr<dcf_exception_t> e )noexcept {

	try {
		std::wcout << L"notification: "
			<< L":" << e->msg()
			<< L":" << e->file()
			<< L":" << e->line()
			<< std::endl;
		}
	catch( ... ) {}
	}

const distributed_ctx_policies_t& distributed_ctx_t::policies()const {
	return (*_policies);
	}

local_object_id_t distributed_ctx_t::make_local_object_identity()const noexcept {
	return local_object_id_t::make(_locidCounter.fetch_add(1));
	}

local_command_identity_t distributed_ctx_t::make_command_identity()const noexcept {
	return local_command_identity_t::make((uintptr_t)this, _locidCommandCounter.fetch_add(1));
	}

subscriber_event_id_t distributed_ctx_t::make_event_identity()const noexcept {
	return subscriber_event_id_t::make((uintptr_t)this, _locidCommandCounter.fetch_add(1));
	}

global_object_identity_t distributed_ctx_t::make_global_object_identity() const noexcept{
	return global_object_identity_t::rand();
	}

channel_identity_t distributed_ctx_t::make_channel_identity()const noexcept {
	return channel_identity_t::rand();
	}

sx_uuid_t distributed_ctx_t::make_global_object_identity_128()const noexcept {
	return sx_uuid_t::rand();
	}

size_t distributed_ctx_t::subscribe_address_user_range() {
	return 0x100;
	}

shared_io_services_t& distributed_ctx_t::io_services() {
	if( !_osLayer )
		THROW_MPF_EXC_FWD(nullptr);
	return *(_osLayer);
	}

const channel_identity_t& distributed_ctx_t::domain_id()const noexcept {
	if(_options)
		return _options->domain_id();
	else
		return channel_identity_t::null;
	}

const identity_descriptor_t& distributed_ctx_t::identity()const noexcept {
	if(_options)
		return _options->identity();
	else
		return identity_descriptor_t::null;
	}

const shared_io_services_t& distributed_ctx_t::io_services()const {
	if( !_osLayer )
		THROW_MPF_EXC_FWD(nullptr);
	return *(_osLayer);
	}

std::shared_ptr<lazy_action_stack_t> distributed_ctx_t::ntfstack()const {
	return std::atomic_load_explicit( &_ntfstack, std::memory_order::memory_order_acquire );
	}

/*void distributed_ctx_t::ntf_hndl( std::unique_ptr<dcf_exception_t> && ntf, const global_object_identity_t & srcId )const noexcept{

	if( !stopped() ) {
		auto ntfstck_( ntfstack() );
		if( ntf && ntfstck_ && _ntfHndl ) {

			ntfstck_->launch( __FILE_LINE__, _ntfHndl, std::move( ntf ), srcId );
			ERR_VERIFY( _ntfHndl, 0 );
			}
		}
	}*/

void distributed_ctx_t::exc_hndl( std::unique_ptr<dcf_exception_t> && ntf )const noexcept {
	//ntf_hndl( std::move( ntf ), global_object_identity_t::null );

	if( !stopped() ){
		auto ntfstck_( ntfstack() );
		if( ntf && ntfstck_ && _ntfHndl ){

			ntfstck_->launch( __FILE_LINE__, _ntfHndl, std::move( ntf ) );
			CBL_VERIFY( _ntfHndl );
			}
		}
	}

/*void distributed_ctx_t::exc_hndl( const dcf_exception_t & exc, const global_object_identity_t & srcId )const noexcept {
	ntf_hndl( exc.dcpy(), srcId );
	}*/

/*void distributed_ctx_t::exc_hndl( dcf_exception_t && exc, const global_object_identity_t & srcId ) const noexcept {
	ntf_hndl( exc.dcpy(), srcId );
	}*/

void distributed_ctx_t::exc_hndl( const dcf_exception_t & exc )const noexcept {
	exc_hndl( exc.dcpy() );
	}

void distributed_ctx_t::exc_hndl( dcf_exception_t && exc )const noexcept {
	exc_hndl( std::move(exc).dcpy() );
	}

void distributed_ctx_t::log_time( const global_object_identity_t & id,
	const std::string & tag,
	const std::chrono::microseconds & mcs ) {

	std::string log;
	log += "timer:" + id.to_str() + ":" + tag;
	log += "=";
	log += std::to_string( std::chrono::duration_cast<std::chrono::milliseconds>(mcs).count() );
	log += ", millisec";

	file_logger_t::logging( log, 0, __CBL_FILEW__, __LINE__ );
	}

void distributed_ctx_t::log_time( const global_object_identity_t & id,
	const global_object_identity_t & ,
	const std::string & tag,
	const std::chrono::microseconds & mcs ) {

	std::string log;
	log += "timer:" + id.to_str() + ":" + tag;
	log += "=";
	log += std::to_string( std::chrono::duration_cast<std::chrono::milliseconds>(mcs).count() );
	log += ", millisec";

	file_logger_t::logging( log, 0, __CBL_FILEW__, __LINE__ );
	}

#ifdef CBL_MPF_ENABLE_TIMETRACE
void distributed_ctx_t::log_time( const global_object_identity_t & id,
	const std::string & tag,
	hanlder_times_stack_t::clocks_t && clocks ) {

	std::string log;
	log += "timer:" + id.to_str() + ":" + tag;
	log += "=";

	for( size_t i = 0; i < clocks.clocks.size(); ++i ) {

		log += std::to_string( std::chrono::duration_cast<std::chrono::milliseconds>(clocks.clocks.at( i )).count() );
		log += "|";
		}

	log += ", millisec";

	file_logger_t::logging( log, 0, __CBL_FILEW__, __LINE__ );
	}
#endif

size_t distributed_ctx_t::concurrency_size( async_space_t sc )const noexcept{
	if( sc == async_space_t::sys ){
		return CHECK_PTR( lzy_stack() )->conncurrency_size();
		}
	else if( sc == async_space_t::ext ){
		return CHECK_PTR( exthndl_stack() )->conncurrency_size();
		}
	else{
		FATAL_ERROR_FWD(nullptr);
		}
	}

distributed_ctx_t::sequential_handler distributed_ctx_t::get_sequential_handler( async_space_t scx ){
	return _sequentialStack->get_handler(scx);
	}

distributed_ctx_t::subscribe_line_t distributed_ctx_t::subscribe_make_line(rtl_level_t l) {
	return CHECK_PTR(_subscribeTable)->make_handler(l);
	}

distributed_ctx_t::events_line_t distributed_ctx_t::events_make_line(){
	return CHECK_PTR( _eventsTable )->make_handler();
	}

functor_result distributed_ctx_t::invoke_handler(functor_trait&& fd) {
	return _handlersMap.invoke(std::move(fd));
	}


