#include "../context.h"
#include "../../channel_terms/internal_terms.h"
#include "../../messages/internal_terms.h"
#include "../../channel_terms/xtract.h"


using namespace crm;
using namespace crm::detail;


static void decode__(srlz::serialized_base_r & dcp, crm::binary_vector_t && bin) {
	auto rstream = crm::srlz::rstream_constructor_t::make(std::move(bin));
	dcp.deserialize(rstream);
	}

static void decode__(srlz::decouple_1<i_iol_handler_t> & dcp, crm::binary_vector_t && bin) {
	auto rstream = crm::srlz::rstream_constructor_t::make(std::move(bin));
	dcp.decode(rstream);
	}


std::unique_ptr<i_iol_ihandler_t> distributed_ctx_t::decode_report(i_iol_ihandler_t &iHndl,
	srlz::decouple_1<i_iol_handler_t> & pISrlz_, _packet_t && frame_)const noexcept {

	try {
		MPF_TEST_EMULATION_IN_CONTEXT_L_COND_TRCX(this, iHndl.level(), 7, is_trait_at_emulation_context(iHndl), trait_at_keytrace_set(iHndl));
		decode__(pISrlz_, frame_.data_release());

		return {};
	} catch (const crm::dcf_exception_t & e0) {
		return inotification_t::make_internal_report(iHndl.address(), e0.dcpy());
	} catch (const std::exception & e1) {
		return inotification_t::make_internal_report(iHndl.address(), CREATE_MPF_PTR_EXC_FWD(e1));
	} catch (...) {
		return inotification_t::make_internal_report(iHndl.address(), CREATE_MPF_PTR_EXC_FWD(nullptr));
	}
}

std::unique_ptr<i_iol_ihandler_t> distributed_ctx_t::decode_report(i_iol_ihandler_t &iHndl,
	srlz::serialized_base_r & pISrlz_, _packet_t && frame_)const noexcept {

	try {
		MPF_TEST_EMULATION_IN_CONTEXT_L_COND_TRCX(this, iHndl.level(), 7, is_trait_at_emulation_context(iHndl), trait_at_keytrace_set(iHndl));
		decode__( pISrlz_, frame_.data_release());

		return {};
	} catch (const crm::dcf_exception_t & e0) {
		return inotification_t::make_internal_report(iHndl.address(), e0.dcpy());
	} catch (const std::exception & e1) {
		return inotification_t::make_internal_report(iHndl.address(), CREATE_MPF_PTR_EXC_FWD(e1));
	} catch (...) {
		return inotification_t::make_internal_report(iHndl.address(), CREATE_MPF_PTR_EXC_FWD(nullptr));
	}
}

void distributed_ctx_t::decode_no_report(i_iol_ihandler_t & iHndl,
	srlz::decouple_1<i_iol_handler_t> & pISrlz_, _packet_t && frame_)const {

	MPF_TEST_EMULATION_IN_CONTEXT_L_COND_TRCX(this, iHndl.level(), 8, is_trait_at_emulation_context(iHndl), trait_at_keytrace_set(iHndl));
	decode__(pISrlz_, frame_.data_release());
}

void distributed_ctx_t::decode_no_report(i_iol_ihandler_t & iHndl,
	srlz::serialized_base_r & pISrlz_, _packet_t && frame_)const {

	MPF_TEST_EMULATION_IN_CONTEXT_L_COND_TRCX(this, iHndl.level(), 8, is_trait_at_emulation_context(iHndl), trait_at_keytrace_set(iHndl));
	decode__(pISrlz_, frame_.data_release());
}


std::unique_ptr<i_iol_ihandler_t> wrap2_uplevel_handler(message_frame_unit_t && frame_)noexcept {
	auto result = std::make_unique<ipacket_t>(std::move(frame_.capture_unit()));

#pragma message( CBL_VAR_NAME_VALUE(CBL_BUILD_VAR_SNDRCV_REVERSE_GUARD, 20))
	if(result->reset_invoke_exception_guard(frame_.reset_invoke_exception_guard())) {
		FATAL_ERROR_FWD(nullptr);
		}

	return result;
	}

std::unique_ptr<i_iol_ihandler_t> distributed_ctx_t::deserialize_internal( message_frame_unit_t && bin)const {
	if(bin.unit().type().type() == iol_types_t::st_RZi_LX_error) {

		address_descriptor_t destAddr;
		std::unique_ptr<dcf_exception_t> e;

		exception_RZi_LX_extract( (*this), bin.capture_unit(), destAddr, e);
		CBL_VERIFY(e);
		CBL_VERIFY(!is_null(destAddr));
		CBL_VERIFY(!is_null(destAddr.destination_subscriber_id()));

		auto p = std::make_unique<inotification_t>(std::move(e));
		p->set_address(destAddr);

		CBL_VERIFY((is_null(destAddr.spin_tag()) || destAddr.spin_tag() == p->spin_tag()));

		return p;
		}
	else {
		FATAL_ERROR_FWD(nullptr);
		}
	}

std::unique_ptr<i_iol_ihandler_t> distributed_ctx_t::deserialize(rtl_level_t l, message_frame_unit_t && bin)const {
	CBL_MPF_KEYTRACE_SET(bin, 40);

	/* ������� ������ */
	std::unique_ptr<i_iol_ihandler_t> iHndl;

	/* ���� ������� ������ ����� ����������� ������� ������������� */
	if(bin.unit().address().level() > l) {
		CBL_MPF_KEYTRACE_SET(bin, 41);

		/* ��������� ����� ������ � ���� */
		iHndl = wrap2_uplevel_handler( std::move( bin ) );
		}
	else if( bin.unit().address().level() == l ){
		CBL_MPF_KEYTRACE_SET(bin, 42);

		MPF_TEST_EMULATION_IN_CONTEXT_COND_TRCX(this, 1, 
			is_trait_at_emulation_context(bin),
			trait_at_keytrace_set(bin));

		/*
		����� ������� ��������������� ������� � ������� ������
		*/
		if(bin.unit().type().type() > iol_types_t::st_Internal) {
			CBL_MPF_KEYTRACE_SET(bin, 43);

			/* ���������� ������������� ���� � �������  */
			auto typemapKey = bin.unit().type().typemap_key();

			/* ���� ��� �������������� ����������� ������������� ����, ������������ ��� */
			if(bin.unit().type().fl_typeid()) {
				typemapKey = crm::srlz::typemap_key_t(bin.unit().type_id(), bin.unit().type_name());
				}

			/* ����� ��������������� �� ����� ����� */
			if(auto archv = typemap().find(typemapKey)) {
				iHndl = decode(std::move(archv), std::move(bin));
				}
			else {
				std::ostringstream em;
				em << "type ctr not found = "
					<< bin.unit().type().utype()
					<< ":" << bin.unit().type().ucode()
					<< ":" << cvt2string(bin.unit().type().type())
					<< ":" << (uint64_t)bin.unit().type().type()
					<< ":" << bin.unit().type().type_id()
					<< ":" << bin.unit().type().type_name()
					<< " + " << typemapKey.type_id() << ":" << typemapKey.type_name();

				bin.invoke_exception(CREATE_SERIALIZE_PTR_EXC_FWD(em.str()));
				}
			}
		else {
			iHndl = deserialize_internal( std::move(bin));
			}
		}
	else{
		/* ������� ������������� ����� ���� ������ ���� */
		bin.invoke_exception(CREATE_MPF_PTR_EXC_FWD(nullptr));
		}

	return iHndl;
	}

void distributed_ctx_t::deserialize(rtl_level_t l, 
	message_frame_unit_t && bin, 
	message_frame_deserialize_tail_f && tail ) {

	CBL_MPF_KEYTRACE_SET(bin, 50);

	/* ���� ������� ������ ����� ����������� ������� ������������� */
	if(bin.unit().address().level() > l) {
		CBL_MPF_KEYTRACE_SET(bin, 51);

		/* ��������� ����� ������ � ���� */
		if(tail) {

#pragma message( CBL_VAR_NAME_VALUE(CBL_BUILD_VAR_SNDRCV_REVERSE_GUARD, 9))

			const size_t hl = get_packet_idx(bin);
			launch_async_ordered( async_space_t::sys, hl, __FILE_LINE__, std::move(tail), wrap2_uplevel_handler(std::move(bin)));
			}
		else {
			FATAL_ERROR_FWD(nullptr);
			}
		}
	else if( bin.unit().address().level() == l ){
		CBL_MPF_KEYTRACE_SET(bin, 52);

		MPF_TEST_EMULATION_IN_CONTEXT_L_COND_TRCX(this, l, 5, is_trait_at_emulation_context(bin), trait_at_keytrace_set(bin));

		/*
		����� ������� ��������������� ������� � ������� ������
		*/
		if(bin.unit().type().type() > iol_types_t::st_Internal) {

			/* ���������� ������������� ���� � �������  */
			auto typemapKey = bin.unit().type().typemap_key();
			/* ���� ��� ������������� ����������� ������������� ����, ������������ ��� */
			if(bin.unit().type().fl_typeid()) {
				typemapKey = srlz::typemap_key_t(bin.unit().type_id(), bin.unit().type_name());
				}

			/* ����� ��������������� �� ����� ����� */
			if(auto archv = typemap().find(typemapKey)) {
				CBL_MPF_KEYTRACE_SET(bin, 53);

#pragma message( CBL_VAR_NAME_VALUE(CBL_BUILD_VAR_SNDRCV_REVERSE_GUARD, 9))

				const size_t hl = get_packet_idx(bin);
				launch_async_ordered( async_space_t::sys, hl, __FILE_LINE__, [wptr = weak_from_this()](
					message_frame_deserialize_tail_f && tail, decltype(archv) && archv_, message_frame_unit_t && bin){

#pragma message( CBL_VAR_NAME_VALUE(CBL_BUILD_VAR_SNDRCV_REVERSE_GUARD, 10))
					tail(CHECK_PTR(wptr)->decode(std::move(archv_), std::move(bin)));
					}, std::move(tail), std::move(archv), std::move(bin));
				}
			else {
				CBL_MPF_KEYTRACE_SET(bin, 54);

				std::ostringstream msgBuf;
				msgBuf << "type ctr not found = "
					<< bin.unit().type().utype()
					<< ":" << bin.unit().type().ucode()
					<< ":" << cvt2string(bin.unit().type().type())
					<< ":" << (uint64_t)bin.unit().type().type()
					<< ":" << bin.unit().type().type_id()
					<< ":" << bin.unit().type().type_name()
					<< " + " << typemapKey.type_id() << ":" << typemapKey.type_name() << std::endl;

				bin.invoke_exception(CREATE_MPF_PTR_EXC_FWD(msgBuf.str()));
				}
			}
		else {
			CBL_MPF_KEYTRACE_SET(bin, 55);

#pragma message( CBL_VAR_NAME_VALUE(CBL_BUILD_VAR_SNDRCV_REVERSE_GUARD, 9))
			const auto idx = get_packet_idx(bin);
			launch_async_ordered(async_space_t::sys, idx, __FILE_LINE__, [wptr = weak_from_this()](
				message_frame_deserialize_tail_f && tail, message_frame_unit_t && bin) {

#pragma message( CBL_VAR_NAME_VALUE(CBL_BUILD_VAR_SNDRCV_REVERSE_GUARD, 10))
				tail(CHECK_PTR(wptr)->deserialize_internal(std::move(bin)));
				}, std::move(tail), std::move(bin));
			}
		}
	else{
		/* ������� ������������� ����� ���� ������ ���� */
		bin.invoke_exception(CREATE_MPF_PTR_EXC_FWD(nullptr));
		}
	}

std::unique_ptr<i_iol_ihandler_t> distributed_ctx_t::decode(std::shared_ptr<srlz::i_type_ctr_i_srlzd_t<i_iol_handler_t>> usrlz,
	detail::message_frame_unit_t && bin)const {

	if (usrlz) {
		if (auto pSrlzObj = usrlz->create()) {
			if (auto pHndl = dynamic_cast<i_iol_ihandler_t*>(pSrlzObj->pObj.get())) {
				CBL_MPF_KEYTRACE_SET(bin, 45);
									
				if (auto tdrv = types_driver()) {
					pHndl->set_typedriver(tdrv);
				} else {
					THROW_MPF_EXC_FWD(nullptr);
				}

				/* ����� */
				auto frame_ = bin.capture_unit();

				pHndl->cvt_from(std::move(frame_));
				pHndl->build();

				if (pHndl->type().fl_report()) {
					if (auto errHndl = decode_report(*pHndl, *pSrlzObj, std::move(frame_))) {
						return errHndl;
					}
				} else {
					decode_no_report(*pHndl, *pSrlzObj, std::move(frame_));
				}

#pragma message( CBL_VAR_NAME_VALUE(CBL_BUILD_VAR_SNDRCV_REVERSE_GUARD, 20))
				if (pHndl->reset_invoke_exception_guard(bin.reset_invoke_exception_guard())) {
					FATAL_ERROR_FWD(nullptr);
				}

				auto ret = std::unique_ptr< i_iol_ihandler_t>(pHndl);
				pSrlzObj->pObj.release();

				return ret;

			} else {
				THROW_MPF_EXC_FWD(nullptr);
			}
		} else {
			THROW_MPF_EXC_FWD(nullptr);
		}
	} else {
		THROW_MPF_EXC_FWD(nullptr);
	}
}

std::unique_ptr<i_iol_ihandler_t> distributed_ctx_t::decode(std::shared_ptr<srlz::i_type_ctr_serialized_trait_t> usrlz,
	detail::message_frame_unit_t && bin)const {

	if (usrlz) {
		if (auto pObj = usrlz->create()) {
			if (auto pHndl = dynamic_cast<i_iol_ihandler_t*>(pObj.get())) {
				CBL_MPF_KEYTRACE_SET(bin, 49);

				if (auto tdrv = types_driver()) {
					pHndl->set_typedriver(tdrv);
					}
				else {
					THROW_MPF_EXC_FWD(nullptr);
					}

				/* ����� */
				auto frame_ = bin.capture_unit();

				pHndl->cvt_from(std::move(frame_));
				pHndl->build();

				if (pHndl->type().fl_report()) {
					if (auto errHndl = decode_report(*pHndl, *pObj, std::move(frame_))) {
						return errHndl;
						}
					}
				else {
					decode_no_report(*pHndl, *pObj, std::move(frame_));
					}

#pragma message( CBL_VAR_NAME_VALUE(CBL_BUILD_VAR_SNDRCV_REVERSE_GUARD, 20))
				if (pHndl->reset_invoke_exception_guard(bin.reset_invoke_exception_guard())) {
					FATAL_ERROR_FWD(nullptr);
					}

				auto ret = std::unique_ptr< i_iol_ihandler_t>(pHndl);
				pObj.release();

				return ret;
				}
			else {
				THROW_MPF_EXC_FWD(nullptr);
				}
			}
		else {
			THROW_MPF_EXC_FWD(nullptr);
			}
		}
	else {
		THROW_MPF_EXC_FWD(nullptr);
		}
	}

std::unique_ptr<i_iol_ihandler_t> distributed_ctx_t::decode(std::shared_ptr<srlz::i_type_ctr_base_t> isrlz_,
	message_frame_unit_t && bin)const {

	CBL_MPF_KEYTRACE_SET(bin, 44);

	if (isrlz_) {
		MPF_TEST_EMULATION_IN_CONTEXT_L_COND_TRCX(this, bin.unit().address().level(), 6, is_trait_at_emulation_context(bin), trait_at_keytrace_set(bin));

		if (auto srlzAsTrait = std::dynamic_pointer_cast<srlz::i_type_ctr_serialized_trait_t>(isrlz_)) {
			return decode(srlzAsTrait, std::move(bin));
			}
		else if (auto srlzAsI = std::dynamic_pointer_cast<srlz::i_type_ctr_i_srlzd_t<i_iol_handler_t>>(isrlz_)) {
			return decode(srlzAsI, std::move(bin));
			}
		else {
			THROW_MPF_EXC_FWD(nullptr);
			}
		}
	else {
		THROW_MPF_EXC_FWD(nullptr);
		}
	}


deserialize_vector_t::deserialize_vector_t(size_t c, std::function<void(std::unique_ptr<dcf_exception_t> &&)> && ntfh){
	for(size_t i = 0; i < c; ++i) {
		_lzv.emplace_back(lazy_action_stack_t::make(std::string( __FILE_LINE__ ) + ":" + std::to_string(i),
			ntfh,
			1));
		}
	}

void deserialize_vector_t::cancel()noexcept {
	bool cancelf = false;
	if(_cancelf.compare_exchange_strong(cancelf, true)) {

		for(size_t i = 0; i < _lzv.size(); ++i) {
			if(auto l = std::atomic_load_explicit(&_lzv[i], std::memory_order::memory_order_acquire)) {
				l->cancel();
				}
			}
		}
	}

void deserialize_vector_t::close(bool soft)noexcept {
	bool closed = false;
	if(_closef.compare_exchange_strong(closed, true)) {

		cancel();

		for(size_t i = 0; i < _lzv.size(); ++i) {
			if(auto l = std::atomic_exchange(&_lzv[i], std::shared_ptr<lazy_action_stack_t>())) {
				l->close(soft);
				}
			}
		}
	}

bool deserialize_vector_t::closed()const noexcept {
	return _closef.load(std::memory_order_acquire);
	}

bool deserialize_vector_t::canceled()const noexcept {
	return _cancelf.load(std::memory_order::memory_order_relaxed);
	}
