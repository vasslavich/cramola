#include "../context.h"
#include "../../logging/internal_terms.h"


using namespace crm;
using namespace crm::detail;
using namespace crm::mpf;
using namespace crm::mpf::detail;



#ifdef CBL_MPF_USE_REVERSE_EXCEPTION_TRACE
void distributed_ctx_t::add_reverse_exception_trace(const std::wstring & key)const noexcept {
	_reverseExcTracer->add(std::move(key));
	}

std::wstring distributed_ctx_t::get_reverse_exception_trace()const noexcept {
	return _reverseExcTracer->get_reverse_exception_trace();
	}
#endif


#ifdef CBL_USE_OBJECTS_MONITOR
void distributed_ctx_t::object_monitor_set(const sx_locid_t & id_, std::weak_ptr<i_terminatable_t> wptr)noexcept {
	_rootCollection.set(id_, std::move(wptr));
	}

void distributed_ctx_t::object_monitor_unset(const sx_locid_t & id_)noexcept {
	_rootCollection.unset(id_);
	}

void distributed_ctx_t::object_monitor_check()noexcept {
	_rootCollection.check();
	}
#endif

std::chrono::system_clock::duration distributed_ctx_t::default_timeline()const noexcept {
	return std::chrono::seconds(3600);
	}


void distributed_ctx_t::trace_state_start() {
	_traceTh = std::thread(&distributed_ctx_t::trace_state_body, this);
	}

void distributed_ctx_t::trace_start_cose()noexcept {
	if (_traceTh.joinable()) {
		CHECK_NO_EXCEPTION(_traceTh.join());
		}
	}

void distributed_ctx_t::trace_state_body() noexcept {

	while (!stopped()) {
		try {
			auto stpEnd = std::chrono::system_clock::now() + std::chrono::seconds(60);

			std::ostringstream text;

#ifdef CBL_MPF_MESSAGE_TRACEWAY_KEYS_BY_ID

			text << "key tracer state:" << std::endl;

			const auto kl = get_keytracer().view();
			for (const auto& ik : kl) {

				text << "    " << std::setw(8) << std::get<0>(ik).key() << " ";
				for (size_t i = 0; i < std::get<1>(ik).size; ++i) {
					if (std::get<1>(ik).il.at(i) != 0) {
						text << "{" << i << "," << std::get<1>(ik).il.at(i) << "}";
						}
					}

				text << std::endl;
				}

			auto keycover = get_keytracer().coverage_stat();
			text << "key coverage:" << std::get<0>(keycover) << "/" << std::get<1>(keycover) << std::endl;
#endif

#ifdef CBL_TRACE_CTX_STATE
			text << "trace id iops:" << std::endl;
			const auto idops = get_trace_collection().view();
			for (const auto id : idops) {

				const auto durationSec = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now() - std::get<3>(id));

				text << "    "
					<< std::setw(20) << std::get<0>(id)
					<< " " << std::get<1>(id)
					<< " " << std::get<2>(id)
					<< " " << durationSec.count() << " sec";
				text << std::endl;

				if (durationSec.count() > 100) {
					file_logger_t::logging("iops time long: " + std::get<1>(id) + ":" + std::get<0>(id).to_str() + ", sec=" + std::to_string(durationSec.count()));
					}
				}
#endif

			crm::file_logger_t::logging(text.str(), 920);

#ifdef CBL_USE_OBJECTS_MONITOR
			_rootCollection.check();
#endif

			while (!stopped() && stpEnd > std::chrono::system_clock::now()) {
				std::this_thread::sleep_for(std::chrono::seconds(2));
				}
			}
		catch (const dcf_exception_t & exc0) {
			crm::file_logger_t::logging(exc0);
			}
		catch (const std::exception & exc1) {
			crm::file_logger_t::logging(exc1);
			}
		catch (...) {
			crm::file_logger_t::logging("undefined", NOTIFICATION_TRACE_ASYNC_POOL_TAG, __CBL_FILEW__, __LINE__);
			}
		}
	}
