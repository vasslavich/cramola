#include "../context.h"


using namespace crm;
using namespace crm::detail;



std::unique_ptr<i_xtimer_oncecall_t> distributed_ctx_t::create_timer_oncecall_sys() {
	return std::make_unique<timer_oncecall_sys_t>(shared_from_this());
	}

std::unique_ptr<i_xtimer_oncecall_t> distributed_ctx_t::create_timer_oncecall_sys(
	i_xtimer_oncecall_t::timer_out_t && hndl,
	const std::chrono::milliseconds & timeout) {

	return std::make_unique<timer_oncecall_sys_t>(shared_from_this(), std::move(hndl), timeout);
	}

std::unique_ptr<i_xtimer_t> distributed_ctx_t::create_timer_sys(std::chrono::milliseconds interval) {
	return std::make_unique<timer_sys_t>(shared_from_this(), interval);
	}

std::unique_ptr<i_xtimer_t> distributed_ctx_t::create_timer_sys() {
	return std::make_unique<timer_sys_t>(shared_from_this());
	}

std::unique_ptr<i_xdeffered_action_timer_t> distributed_ctx_t::create_timer_deffered_action_sys(
	trace_tag&& tag,
	i_xdeffered_action_timer_t::timer_out_t && hndl,
	const std::chrono::milliseconds & timeout) {

	return std::make_unique<deffered_action_timer_sys_t>(shared_from_this(), std::move(tag), std::move(hndl), timeout);
	}

void distributed_ctx_t::register_timer_deffered_action_sys(
	trace_tag&& tag,
	i_xdeffered_action_timer_t::timer_out_t && hndl,
	const std::chrono::milliseconds & timeout) {

	deffered_action_timer_sys_t::launch(shared_from_this(), std::move(tag), std::move(hndl), timeout);
	}


std::unique_ptr<i_xtimer_oncecall_t> distributed_ctx_t::create_timer_oncecall() {
	return std::make_unique<timer_oncecall_t>(shared_from_this());
	}

std::unique_ptr<i_xtimer_oncecall_t> distributed_ctx_t::create_timer_oncecall(
	i_xtimer_oncecall_t::timer_out_t && hndl,
	const std::chrono::milliseconds & timeout) {

	return std::make_unique<timer_oncecall_t>(shared_from_this(), std::move(hndl), timeout);
	}

std::unique_ptr<i_xtimer_t> distributed_ctx_t::create_timer(std::chrono::milliseconds interval) {
	return std::make_unique<timer_t>(shared_from_this(), interval);
	}

std::unique_ptr<i_xtimer_t> distributed_ctx_t::create_timer() {
	return std::make_unique<timer_t>(shared_from_this());
	}

std::unique_ptr<i_xdeffered_action_timer_t> distributed_ctx_t::create_timer_deffered_action(
	trace_tag&& tag,
	i_xdeffered_action_timer_t::timer_out_t && hndl,
	const std::chrono::milliseconds & timeout) {

	return std::make_unique<deffered_action_timer_t>(shared_from_this(), std::move(tag), std::move(hndl), timeout);
	}

std::unique_ptr<i_xtimer_oncecall_t> distributed_ctx_t::create_timer_oncecall(async_space_t spc) {
	switch(spc) {
		case async_space_t::undef:
			FATAL_ERROR_FWD(nullptr);
		case async_space_t::sys:
			return create_timer_oncecall_sys();
		case async_space_t::ext:
			return create_timer_oncecall();
		default:
			FATAL_ERROR_FWD(nullptr);
		}
	}

std::unique_ptr<i_xtimer_oncecall_t> distributed_ctx_t::create_timer_oncecall(async_space_t spc,
	i_xtimer_oncecall_t::timer_out_t && hndl,
	const std::chrono::milliseconds & timeout) {

	switch(spc) {
		case async_space_t::undef:
			FATAL_ERROR_FWD(nullptr);
		case async_space_t::sys:
			return create_timer_oncecall_sys(std::move(hndl), timeout);
		case async_space_t::ext:
			return create_timer_oncecall(std::move(hndl), timeout);
		default:
			FATAL_ERROR_FWD(nullptr);
		}
	}

std::unique_ptr<i_xtimer_t> distributed_ctx_t::create_timer(async_space_t spc, std::chrono::milliseconds interval) {
	switch(spc) {
		case async_space_t::undef:
			FATAL_ERROR_FWD(nullptr);
		case async_space_t::sys:
			return create_timer_sys(interval);
		case async_space_t::ext:
			return create_timer(interval);
		default:
			FATAL_ERROR_FWD(nullptr);
		}
	}

std::unique_ptr<i_xtimer_t> distributed_ctx_t::create_timer(async_space_t spc) {
	switch(spc) {
		case async_space_t::undef:
			FATAL_ERROR_FWD(nullptr);
		case async_space_t::sys:
			return create_timer_sys();
		case async_space_t::ext:
			return create_timer();
		default:
			FATAL_ERROR_FWD(nullptr);
		}
	}

std::unique_ptr<i_xdeffered_action_timer_t> distributed_ctx_t::create_timer_deffered_action(
	trace_tag&& tag,
	async_space_t spc,
	i_xdeffered_action_timer_t::timer_out_t && hndl,
	const std::chrono::milliseconds & timeout) {

	switch(spc) {
		case async_space_t::undef:
			FATAL_ERROR_FWD(nullptr);
		case async_space_t::sys:
			return create_timer_deffered_action_sys(std::move(tag), std::move(hndl), timeout);
		case async_space_t::ext:
			return create_timer_deffered_action(std::move(tag), std::move(hndl), timeout);
		default:
			FATAL_ERROR_FWD(nullptr);
		}
	}



