#include "../../internal.h"
#include "../async_sequential.h"


using namespace crm;
using namespace crm::detail;


std::atomic<async_sequential_push_handler::inbound_stack_key> async_sequential_push_handler::id_counter{ 0 };


async_sequential::async_sequential( std::weak_ptr<distributed_ctx_t> aq, size_t )
	: _aq( std::move( aq ) ){}


async_sequential_push_handler async_sequential::get_handler( async_space_t scx_ ){
	if( async_space_t::undef != scx_ ){
		auto h = async_sequential_push_handler{};
		h.scx = scx_;
		h.wptr = weak_from_this();

		return h;
		}
	else{
		THROW_EXC_FWD(nullptr);
		}
	}

