#include <sstream>
#include <ctime>
#include <chrono>
#include <filesystem>
#include "../../internal.h"
#include "../../streams/whandlers_tbl.h"


using namespace crm;
using namespace crm::detail;


std::shared_ptr<i_stream_whandler_t> distributed_ctx_t::pop_stream_writer(
	std::unique_ptr<i_stream_zyamba_identity_t>&& h,
	std::unique_ptr<i_stream_zyamba_cover_data_t>&& cd)const {

	if (_streamTypeHndl)
		return _streamTypeHndl->pop_stream_writer(std::move(h), std::move(cd));
	else
		return nullptr;
	}

void distributed_ctx_t::push_stream_writer(const stream_source_descriptor_t& streamIdentity)noexcept {
	try {
		if (_streamTypeHndl) {
			launch_async(__FILE_LINE__, [wp = weak_from_this(), streamIdentity]{
				if (auto sp = wp.lock()) {
					CHECK_NO_EXCEPTION(sp->_streamTypeHndl->push_stream_writer(streamIdentity));
					}
				});
			}
		}
	catch (const dcf_exception_t & exc0) {
		exc_hndl(exc0.dcpy());
		}
	catch (const std::exception & exc1) {
		exc_hndl(CREATE_PTR_EXC_FWD(exc1));
		}
	catch (...) {
		exc_hndl(CREATE_PTR_EXC_FWD(nullptr));
		}
	}


size_t distributed_ctx_t::streams_count()const noexcept {
	return _streamTypeHndl->streams_count();
	}
