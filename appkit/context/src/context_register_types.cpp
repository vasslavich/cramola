#include <sstream>
#include <ctime>
#include <chrono>
#include <filesystem>
#include "../../internal.h"
#include "../../streams/whandlers_tbl.h"


using namespace crm;
using namespace crm::detail;



std::shared_ptr<i_types_driver_t> distributed_ctx_t::types_driver() {
	return std::atomic_load_explicit(&_typesDrv, std::memory_order::memory_order_acquire);
	}

std::shared_ptr<i_types_driver_t> distributed_ctx_t::types_driver()const {
	return std::atomic_load_explicit(&_typesDrv, std::memory_order::memory_order_acquire);
	}

template<typename TFuncMake>
struct wstream_file_maker_t {
	TFuncMake _f;

	auto stream_typeid()const noexcept {
		return wstream_file_t::stream_blob_typeid;
		}

	auto operator()(std::unique_ptr<i_stream_zyamba_identity_t>&& h, std::unique_ptr<i_stream_zyamba_cover_data_t>&& cd) const {
		return _f(std::move(h), std::move(cd));
		}

	template<typename XFuncMake>
	wstream_file_maker_t(XFuncMake&& f)noexcept
		: _f(std::forward<XFuncMake>(f)) {}
	};


template<typename TFuncMake>
auto get_file_stream_make(TFuncMake&& f) {
	using type = wstream_file_maker_t<TFuncMake>;
	return type(std::forward<TFuncMake>(f));
	}

static_assert(is_write_stream_typemaker_v<decltype(
	get_file_stream_make(std::declval<std::function<
		std::unique_ptr<i_stream_whandler_t>(std::unique_ptr<i_stream_zyamba_identity_t>&&, std::unique_ptr<i_stream_zyamba_cover_data_t>&&)>>()))>, __FILE_LINE__);

void distributed_ctx_t::initialize_typemap_drv(whandler_push_f&& streamWHandlerPush) {
	using ifunctor_result_envelop = istrob_message_envelop_t< functor_result>;
	using ofunctor_result_envelop = ostrob_message_envelop_t< functor_result>;

	auto check_result_add_type = [](auto errc) {
		if (errc) {
			THROW_EXC_FWD(nullptr);
			}
		};

	check_result_add_type(_typeMap.add_drv_type<ifunctor_result_envelop, i_iol_handler_t>());
	check_result_add_type(_typeMap.add_drv_type<ofunctor_result_envelop, ifunctor_result_envelop, i_iol_handler_t>());

	check_result_add_type(_typeMap.add_drv_type < ifunctor_package_t, i_iol_handler_t >(ifunctor_package_t::mpf_type_spcf().typemap_key()));
	check_result_add_type(_typeMap.add_drv_type<ofunctor_package_t, ifunctor_package_t, i_iol_handler_t>());
	check_result_add_type(_typeMap.add_drv_type<iping_id_t, i_iol_handler_t>(iping_id_t::mpf_type_spcf().typemap_key()));
	check_result_add_type(_typeMap.add_drv_type<oping_id_t, iping_id_t, i_iol_handler_t>());
	check_result_add_type(_typeMap.add_drv_type<iidentity_t, i_iol_handler_t>(iidentity_t::mpf_type_spcf().typemap_key()));
	check_result_add_type(_typeMap.add_drv_type<oidentity_t, iidentity_t, i_iol_handler_t>());
	check_result_add_type(_typeMap.add_drv_type<icommand_t, i_iol_handler_t>(icommand_t::mpf_type_spcf().typemap_key()));
	check_result_add_type(_typeMap.add_drv_type<ocommand_t, icommand_t, i_iol_handler_t>());
	check_result_add_type(_typeMap.add_drv_type<irequest_t, i_iol_handler_t>(irequest_t::mpf_type_spcf().typemap_key()));
	check_result_add_type(_typeMap.add_drv_type<orequest_t, irequest_t, i_iol_handler_t>());
	check_result_add_type(_typeMap.add_drv_type<idisconnection_t, i_iol_handler_t>(idisconnection_t::mpf_type_spcf().typemap_key()));
	check_result_add_type(_typeMap.add_drv_type<odisconnection_t, idisconnection_t, i_iol_handler_t>());
	check_result_add_type(_typeMap.add_drv_type<isendrecv_datagram_t, i_iol_handler_t>(isendrecv_datagram_t::mpf_type_spcf().typemap_key()));
	check_result_add_type(_typeMap.add_drv_type<osendrecv_datagram_t, isendrecv_datagram_t, i_iol_handler_t>());
	check_result_add_type(_typeMap.add_drv_type<inotification_t, i_iol_handler_t>(inotification_t::mpf_type_spcf().typemap_key()));
	check_result_add_type(_typeMap.add_drv_type<onotification_t, inotification_t, i_iol_handler_t>());
	check_result_add_type(_typeMap.add_drv_type<stream_iheader_t, i_iol_handler_t>());
	check_result_add_type(_typeMap.add_drv_type<stream_oheader_t, stream_iheader_t, i_iol_handler_t>());
	check_result_add_type(_typeMap.add_drv_type<stream_iheader_response_t, i_iol_handler_t>());
	check_result_add_type(_typeMap.add_drv_type<stream_oheader_response_t, stream_iheader_response_t, i_iol_handler_t>());
	check_result_add_type(_typeMap.add_drv_type<stream_itail_t, i_iol_handler_t>());
	check_result_add_type(_typeMap.add_drv_type<stream_otail_t, stream_itail_t, i_iol_handler_t>());
	check_result_add_type(_typeMap.add_drv_type<stream_itail_next_t, i_iol_handler_t>());
	check_result_add_type(_typeMap.add_drv_type<stream_otail_next_t, stream_itail_next_t, i_iol_handler_t>());
	check_result_add_type(_typeMap.add_drv_type<stream_iend_t, i_iol_handler_t>());
	check_result_add_type(_typeMap.add_drv_type<stream_oend_t, stream_iend_t, i_iol_handler_t>());
	check_result_add_type(_typeMap.add_drv_type<stream_idisconnection_t, i_iol_handler_t>());
	check_result_add_type(_typeMap.add_drv_type<stream_odisconnection_t, stream_idisconnection_t, i_iol_handler_t>());


	check_result_add_type(_typeMap.add_drv_type< i_rt0_cmd_addlink_t, i_rt0_command_t>());
	check_result_add_type(_typeMap.add_drv_type< i_rt0_cmd_sublink_t, i_rt0_command_t>());
	check_result_add_type(_typeMap.add_drv_type< i_rt0_cmd_sublink_sourcekey_t, i_rt0_command_t>());
	check_result_add_type(_typeMap.add_drv_type< i_rt0_cmd_reqconnection_t, i_rt0_command_t>());
	check_result_add_type(_typeMap.add_drv_type< i_rt0_cmd_outcoming_oppened_t, i_rt0_command_t>());
	check_result_add_type(_typeMap.add_drv_type< i_rt0_exception_t, i_rt0_command_t>());
	check_result_add_type(_typeMap.add_drv_type< i_rt0_cmd_outcoming_closed_t, i_rt0_command_t>());
	check_result_add_type(_typeMap.add_drv_type< i_rt0_invalid_xpeer_t, i_rt0_command_t>());
	check_result_add_type(_typeMap.add_drv_type< i_rt0_cmd_incoming_closed_t, i_rt0_command_t>());
	check_result_add_type(_typeMap.add_drv_type< i_rt0_cmd_sndrcv_result_t, i_rt0_command_t>());
	check_result_add_type(_typeMap.add_drv_type< cmd_unsubscribe_connection_t, i_rt0_command_t>());
	check_result_add_type(_typeMap.add_drv_type< i_rt0_cmd_sndrcv_fault_t, i_rt0_command_t>());
	check_result_add_type(_typeMap.add_drv_type< module_channel_connection_string, i_rt0_command_t>());

	_streamTypeHndl = std::make_unique<istreams_whandlers_table_t>(shared_from_this(), std::move(streamWHandlerPush));

	/* ������������ �������-������ ���� ������ */
	register_stream_container_type<std::vector<uint8_t>>();
	register_stream_container_type<std::vector<char>>();
	register_stream_container_type<std::string>();
	register_file_stream_type();

	/* container's and file's streams identity types */
	register_stream_identity_type<stream_blob_identity_t<stream_container_params_t>>();
	register_stream_identity_type<stream_blob_identity_t<stream_file_descriptor_t>>();
	}

void distributed_ctx_t::register_file_stream_type() {

	/* ����������� �������-������ ���� ���� */
	CHECK_PTR(_streamTypeHndl)->add_wstream_type_maker(get_file_stream_make(
		[wptr = weak_from_this()](std::unique_ptr<i_stream_zyamba_identity_t>&& h, std::unique_ptr<i_stream_zyamba_cover_data_t> && cd) {
		if (auto sptr = wptr.lock()) {

			/* ������� ��������� ���� */
			auto tmpid = std::filesystem::path(sptr->make_tmpfile());
			while (std::filesystem::exists(tmpid))
				tmpid = sptr->make_tmpfile();

			return wstream_handler_blob_t::make(sptr,
				std::make_unique<wstream_file_t>(sptr, std::move(h), std::move(cd), std::move(tmpid)));
			}
		else {
			return std::unique_ptr<i_stream_whandler_t>();
			}
		}));
	}

