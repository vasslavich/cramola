#pragma once


#include <atomic>
#include <queue>
#include <list>
#include <functional>
#include "../internal.h"
#include "./i_message_queue.h"


namespace crm{
namespace detail{


template<typename TMutex,
	typename TMutexScope,
	typename TCondvar,
	typename TContainer,
	typename TContainerFunctor>
class dynamic_queue_ctr_t : public i_message_queue_t<typename TContainer::value_type> {
public:
	typedef typename TContainer container_t;
	typedef typename container_t::value_type element_t;
	typedef typename TMutex lck_t;
	typedef typename TMutexScope lck_scp_t;
	typedef typename TCondvar cndvar_t;
	typedef typename TContainerFunctor functor_t;
	typedef typename dynamic_queue_ctr_t<lck_t, lck_scp_t, cndvar_t, container_t, functor_t> self_t;

private:
	/*! ������� ��������� */
	container_t _container;
	/*! ������� ����� ��������� ������� */
	std::atomic<std::intmax_t> _nStored = 0;
	/*! ���������� */
	typename mutable lck_t _lock;
	/*! ������ "���� ����� ��� �������" */
	typename cndvar_t _condDec;
	/*! �������� ���������� */
	std::weak_ptr<distributed_ctx_t> _ctx;
	/*! ������� ������ */
	std::atomic<bool> _cancelf = false;
	/*! ��� ������� */
	std::string _objectName;

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	utility::__xobject_counter_logger_t<self_t, 1> _xDbgCounter;
#endif

#ifdef CBL_QUEUE_SIZE_CHECKER
	/*! ������������ ������������ ����� ������� */
	std::atomic<std::intmax_t> _lastMaxStored = 0;
#endif

	void _clear() {
		functor_t::clear( _container );
		_nStored.store( 0, std::memory_order::memory_order_release );

#ifdef CBL_QUEUE_SIZE_CHECKER
		_lastMaxStored.store( 0, std::memory_order::memory_order_release );
#endif
		}

	std::shared_ptr<distributed_ctx_t> ctx()const {
		return _ctx.lock();
		}

	void copy( self_t &&o ) {
		if( this != std::addressof(o) ) {
			lck_scp_t lock_1( o._lock, std::defer_lock );
			lck_scp_t lock_2( _lock, std::defer_lock );
			std::lock( lock_1, lock_2 );

			_container = std::move( o._container );
			_nStored.store( o._nStored.exchange(0) );
			_ctx = std::move( o._ctx );
			_cancelf.exchange( o._cancelf.exchange(true) );
			_objectName = std::move( o._objectName );

#ifdef CBL_OBJECTS_COUNTER_CHECKER
			_xDbgCounter = std::move(o._xDbgCounter);
#endif

			o._clear();
			}
		}

	template<typename Rep, typename Period>
	size_t sem_wait_dec( lck_scp_t &lock,
		typename const std::chrono::duration<Rep, Period> &waitTime ) {

		CBL_VERIFY( lock.owns_lock() );

		auto ctx_( ctx() );
		if( ctx_ ) {

			if( ctx_->asyncpool_loading_factor() < ctx_->policies().pool_loading_mfactor() ) {

				size_t stValue = 0;
				if( _condDec.wait_for( lock, waitTime, [this, &stValue] {
					stValue = stored_count();
					return 0 != stValue;
					} ) ) {

					CBL_VERIFY( lock.owns_lock() );
					return stValue;
					}
				else
					return 0;
				}
			else {
				return stored_count();
				}
			}
		else
			return 0;
		}

	void sem_dec() {
		sem_dec( 1 );
		}

	void sem_dec(size_t count_) {

		CBL_VERIFY( (size_t)stored_count() >= count_ );

		stored_dec( count_ );
		CBL_VERIFY( stored_count() >= 0 );
		}

	void sem_inc() {

		stored_inc(1);
		_condDec.notify_one();
		}

	template<typename Rep, typename Period>
	bool _pop( element_t & dest,
		typename const std::chrono::duration<Rep, Period> &wait ) {

		lck_scp_t lock( _lock );
		if( sem_wait_dec( lock, wait ) ) {
			CBL_VERIFY( stored_count() == functor_t::size( _container ) );

			/* ������ �������� */
			dest = std::move( functor_t::front(_container) );
			/* �������� �� ����� */
			functor_t::pop( _container );
			/* ������ ��������� �������  */
			sem_dec();

			CBL_VERIFY( stored_count() == functor_t::size( _container ) );

			return true;
			}

		return false;
		}

	template<typename Rep, typename Period>
	size_t _pop( std::list<element_t> & dest,
		typename const std::chrono::duration<Rep, Period> &wait ) {

		dest.clear();

		lck_scp_t lock( _lock );
		const auto stCount = sem_wait_dec( lock, wait );
		if( stCount ) {

			CBL_VERIFY( stored_count() == stCount && stCount == functor_t::size( _container ) );

			for( size_t i = 0; i < stCount; ++i ) {
				/* ������ �������� */
				auto item( std::move( functor_t::front( _container ) ) );
				/* � ������� ��������� */
				dest.push_back( std::move( item ) );
				/* �������� �� ����� */
				functor_t::pop( _container );
				}
			sem_dec(stCount);

			CBL_VERIFY( stored_count() == functor_t::size( _container ) );
			}

		return stCount;
		}

	void stored_dec() {
		stored_dec( 1 );
		}

	void stored_dec(size_t count_) {
		if( _nStored.fetch_sub( count_ ) <= 0 )
			THROW_MPF_EXC_FWD(nullptr);
		}

	void stored_inc(std::intmax_t count) {

#ifdef CBL_QUEUE_SIZE_CHECKER
		const auto lastMax = _lastMaxStored.load(std::memory_order::memory_order_acquire);
#endif
		
		const auto stVal = _nStored.fetch_add( count );

		// compilier only
		stVal;

#ifdef CBL_QUEUE_SIZE_CHECKER
		if( stVal > lastMax ) {
			_lastMaxStored.store( stVal, std::memory_order::memory_order_release );
			if( lastMax && ( lastMax % CBL_TRACE_OBJECTS_COUNTER_STEP ) == 0 ) {

				std::string log( SIZE_SEQUENCE_FWDARNING );
				log += ":" + _objectName + "=" + std::to_string( stVal );
				crm::file_logger_t::logging( log, 0, __CBL_FILEW__, __LINE__ );
				}
			}
		else {
			if((lastMax - stVal) >= CBL_TRACE_OBJECTS_COUNTER_STEP) {
				_lastMaxStored.store(stVal, std::memory_order::memory_order_release);
				}
			}
#endif
		}

public:
	template<typename ... TContainerArgs>
	dynamic_queue_ctr_t( std::weak_ptr<distributed_ctx_t> ctx, std::string_view name,
		typename TContainerArgs && ... args )
		: _ctx( ctx )
		, _objectName( name )
		, _container( std::forward<TContainerArgs>( args )... ) 
#ifdef CBL_OBJECTS_COUNTER_CHECKER
		, _xDbgCounter(name)
#endif
		{
		
		if( _objectName.empty() ) {
			FATAL_ERROR_FWD(nullptr);
			}
		}

	dynamic_queue_ctr_t( std::weak_ptr<distributed_ctx_t> ctx, std::string_view name )
		: _ctx( ctx )
		, _objectName( name )
#ifdef CBL_OBJECTS_COUNTER_CHECKER 
		, _xDbgCounter(name)
#endif
		{
		
		if( _objectName.empty() ) {
			FATAL_ERROR_FWD(nullptr);
			}
		}

	dynamic_queue_ctr_t( const self_t & src ) = delete;
	self_t& operator=(const self_t &src) = delete;

	dynamic_queue_ctr_t( self_t && src )noexcept {
		copy( std::move( src ) );
		}

	self_t& operator=(self_t &&src)noexcept {
		if(this != std::addressof(src)) {
			copy(std::move(src));
			}

		return (*this);
		}

	void clear() noexcept {
		lck_scp_t lock( _lock );
		_clear();
		}

	virtual ~dynamic_queue_ctr_t() {
		close();
		}

	bool canceled()const noexcept {
		return _cancelf.load( std::memory_order::memory_order_acquire );
		}

	void cancel()noexcept {
		_cancelf.store( true, std::memory_order::memory_order_release );
		}

	void close()noexcept {
		cancel();
		clear();
		}

	void push( element_t && value ) {
		lck_scp_t lock( _lock );
		
		functor_t::push( _container, std::move( value ) );
		sem_inc();

		CBL_VERIFY( stored_count() == functor_t::size( _container ) );
		}

	template<typename Rep, typename Period>
	bool pop( element_t & dest,
		typename const std::chrono::duration<Rep, Period> &wait ) {
		return _pop( dest, wait );
		}

	template<typename Rep, typename Period>
	size_t pop( std::list<element_t> & dest,
		typename const std::chrono::duration<Rep, Period> &wait ) {
		return _pop( dest, wait );
		}

	bool pop( element_t & dest,
		const std::chrono::milliseconds &wait ) {
		return _pop( dest, wait );
		}

	size_t pop( std::list<element_t> & dest,
		const std::chrono::milliseconds &wait ) {
		return _pop( dest, wait );
		}

	std::uintmax_t stored_count()const noexcept {
		const auto value = _nStored.load( std::memory_order::memory_order_acquire );
		CBL_VERIFY(value >= 0);

		return (std::uintmax_t)value;
		}

	const std::string& name()const noexcept {
		return _objectName;
		}
	};
}
}

