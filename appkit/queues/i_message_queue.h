#pragma once


#include <chrono>
#include <type_traits>
#include <functional>
#include "../internal_invariants.h"


namespace crm {



enum class __queue_operations_t {
	success,
	exception,
	closed
	};
using queue_operations_t = detail::enum_nodiscard_value<__queue_operations_t>;



struct i_input_messages_stock_t{
	virtual ~i_input_messages_stock_t() = 0;
	virtual std::unique_ptr<i_input_messages_stock_t> get() = 0;
	};

template<typename TElement>
struct i_message_base_t {
	using value_type = typename TElement ;

	virtual ~i_message_base_t() {}

	virtual std::uintmax_t stored_count()const noexcept = 0;
	virtual void clear()noexcept = 0;
	};

template<typename TElement>
struct i_message_input_t : public virtual i_message_base_t<TElement> {
	using __i_base_t = typename i_message_base_t<TElement> ;

	virtual void push(typename __i_base_t::value_type && item) = 0;
	};

template<typename TElement>
struct i_message_output_t : public virtual i_message_base_t<TElement> {
	typedef typename i_message_base_t<TElement> __i_base_t;

	virtual bool pop(typename __i_base_t::value_type & dest, const std::chrono::milliseconds & wait) = 0;
	virtual size_t pop(typename std::list<__i_base_t::value_type> & dest, const std::chrono::milliseconds & wait) = 0;
	};

template<typename TElement>
struct i_message_queue_t :
	i_message_input_t<TElement>,
	i_message_output_t<TElement> {};


template<class TElement, class Enable = void>
struct i_message_queue_await_t;

template<class TElement>
struct i_message_queue_await_t<TElement,
	typename std::enable_if_t<(
		std::is_copy_constructible_v<std::decay_t<TElement>> &&
		std::is_copy_assignable_v<std::decay_t<TElement>> &&
		std::is_move_constructible_v<std::decay_t<TElement>> &&
		std::is_move_assignable_v<std::decay_t<TElement>>)>> {

	virtual ~i_message_queue_await_t() {}

	using value_type = typename std::decay_t<TElement>;
	using await_result_f = typename std::function<void(value_type && v)>;

	virtual queue_operations_t push(const value_type &) = 0;
	virtual queue_operations_t push(value_type &&) = 0;
	virtual queue_operations_t async_pop(async_space_t space, await_result_f && callback_f) = 0;

	virtual void clear()noexcept = 0;
	virtual void close()noexcept = 0;
	virtual bool closed()const noexcept = 0;
	};

template<class TElement>
struct i_message_queue_await_t<TElement,
	typename std::enable_if_t<(
		std::is_move_constructible_v<std::decay_t<TElement>> &&
		std::is_move_assignable_v<std::decay_t<TElement>>)>> {

	virtual ~i_message_queue_await_t() {}

	using value_type = typename std::decay_t<TElement>;
	using await_result_f = typename std::function<void(value_type && v)>;

	virtual queue_operations_t push(value_type &&) = 0;
	virtual queue_operations_t async_pop(async_space_t space, await_result_f && callback_f) = 0;

	virtual void clear()noexcept = 0;
	virtual void close()noexcept = 0;
	virtual bool closed()const noexcept = 0;
	};
}

