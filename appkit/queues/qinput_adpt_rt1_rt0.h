#pragma once


#include "./i_message_queue.h"
#include "../xpeer/i_rt1_xpeer.h"


namespace crm::detail{


class inqueue_adpt_rt1_rt0_t final : public i_message_input_t<input_item_t> {
public:
	typedef input_item_t base_item_t;
	typedef i_xpeer_rt1_t::xpeer_desc_t xpeer_desc_t;
	typedef i_xpeer_rt1_t::input_stack_t target_queue_t;
	typedef target_queue_t::value_type input_value_t;
	typedef std::function<input_value_t( base_item_t && item )> preprocessor_f;

private:
	preprocessor_f _preprocessf;
	std::shared_ptr<target_queue_t> _targetQ;

#ifdef CBL_MPF_OBJECTS_COUNTER_CHECKER_INPUT_STACK
	crm::utility::__xobject_counter_logger_t<inqueue_adpt_rt1_rt0_t, 1> _xDbgCounter;
#endif

	void push( base_item_t && item )final;
	std::uintmax_t stored_count()const  noexcept final;
	void clear()noexcept final;

public:
	inqueue_adpt_rt1_rt0_t( std::weak_ptr<distributed_ctx_t> ctx, 
		preprocessor_f && pref );
	inqueue_adpt_rt1_rt0_t( std::shared_ptr<target_queue_t> && targetQ, 
		preprocessor_f && pref );
	};
}

