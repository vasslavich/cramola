#pragma once


#include "./i_message_queue.h"
#include "../xpeer/i_rt0_xpeer.h"


namespace crm::detail{


template<typename TQElement>
class qinput_adptf_t : public  i_message_input_t<TQElement> {
public:
	typedef typename TQElement base_item_t;
	typedef std::function<void( base_item_t && item )> preprocessor_f;

private:
	preprocessor_f _preprocessf;

#ifdef CBL_MPF_OBJECTS_COUNTER_CHECKER_INPUT_STACK
	crm::utility::__xobject_counter_logger_t<qinput_adptf_t<base_item_t>, 1> _xDbgCounter;
#endif

	void push( base_item_t && item ) final {
		if( item.message() ) {
			_preprocessf( std::move( item ) );
			}
		}

	std::uintmax_t stored_count()const noexcept final {
		FATAL_ERROR_FWD(nullptr);
		}

	void clear()noexcept final {}

public:
	qinput_adptf_t( preprocessor_f && preprocessf )
		: _preprocessf( std::move( preprocessf ) ) {

		if( !_preprocessf )
			FATAL_ERROR_FWD(nullptr);
		}
	};
}
