#pragma once


#include <functional>
#include "./i_message_queue.h"


namespace crm{
namespace detail{


template<typename TBaseType>
class functor_container_priority_queue_t {
public:
	typedef typename TBaseType base_type_t;
	typedef typename base_type_t::value_type value_type_t;

	static void push( base_type_t & target, const value_type_t & item ) {
		target.push( item );
		}

	static void push( base_type_t & target, value_type_t && item ) {
		target.push( std::move( item ) );
		}

	template<typename ... TArgs>
	static void emplace( base_type_t & target, typename TArgs && ... args ) {
		target.emplace( std::forward<TArgs>( args )... );
		}

	static void pop( base_type_t & target ) {
		target.pop();
		}

	static value_type_t& front( base_type_t & target ) {
		return target.top();
		}

	static const value_type_t& front( const base_type_t & target ) {
		return target.top();
		}

	static void clear( base_type_t & obj ) {
		base_type_t tmp;
		std::swap( tmp, obj );
		}

	static bool empty( const base_type_t & obj ) {
		return obj.empty();
		}

	static size_t size( const base_type_t & obj ) {
		return obj.size();
		}
	};


template<typename TBaseType>
class functor_container_list_t {
public:
	typedef typename TBaseType base_type_t;
	typedef typename base_type_t::value_type value_type_t;

	static void push( base_type_t & target, const value_type_t & item ) {
		target.push_back( item );
		}

	static void push( base_type_t & target, value_type_t && item ) {
		target.push_back( std::move( item ) );
		}

	static void pop( base_type_t & target ) {
		target.erase( target.begin() );
		}

	static value_type_t& front( base_type_t & target ) {
		return target.front();
		}

	static const value_type_t& front( const base_type_t & target ) {
		return target.front();
		}

	static void clear( base_type_t & obj ) {
		base_type_t tmp;
		std::swap( tmp, obj );
		}

	static bool empty( const base_type_t & obj ) {
		return obj.empty();
		}

	static size_t size( const base_type_t & obj ) {
		return obj.size();
		}
	};
}
}

