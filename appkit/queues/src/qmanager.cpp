#include "../../internal.h"
#include "../../context/context.h"
#include "../qmanager.h"


using namespace crm;
using namespace crm::detail;


i_input_messages_stock_t::~i_input_messages_stock_t(){}


queue_manager_t::queue_manager_t( std::weak_ptr<distributed_ctx_t> ctx )
	: _ctx( ctx ) {}

queue_manager_t::mp_ow_queue_t<queue_manager_t::rt0_input_value_t> 
queue_manager_t::create_rt0_input_queue(
	const std::string & name,
	const std::uintmax_t maxSize, 
	queue_options_t opt ) {

	struct cmprtr_t {
		bool operator()( const rt0_input_value_t &l, const rt0_input_value_t & r ) {
			if( !l.m() || !r.m() )
				return false;

			return !i_iol_ihandler_t::priority( (*l.m()), (*r.m()) );
			}
		};

	return create_queue<rt0_input_value_t>( name, maxSize, opt );
	}

queue_manager_t::mp_ow_queue_t<queue_manager_t::rt0_input_value_t>
queue_manager_t::create_rt0_input_queue( const std::string & name,
	queue_options_t opt ) {

	return create_rt0_input_queue( name, unlimited, opt );
	}

queue_manager_t::mp_ow_queue_t<queue_manager_t::rt0_output_value_t>
queue_manager_t::create_rt0_output_queue(
	const std::string& name,
	const std::uintmax_t maxSize,
	queue_options_t opt) {

	struct cmprtr_t {
		bool operator()(const rt0_output_value_t& l, const rt0_output_value_t& r) {
			if (!l || !r)
				return false;

			return !i_iol_ohandler_t::priority((*l), (*r));
			}
		};

	return create_queue<rt0_output_value_t>(name, maxSize, opt);
	}

queue_manager_t::mp_ow_queue_t<queue_manager_t::rt0_output_value_t>
queue_manager_t::create_rt0_output_queue( const std::string & name,
	queue_options_t opt ) {

	return create_rt0_output_queue( name, unlimited, opt );
	}

queue_manager_t::mp_ow_queue_t<_packet_t> queue_manager_t::create_packet_queue(
	const std::string & name,
	const std::uintmax_t maxSize,
	queue_options_t opt ) {

	struct cmprtr_t{
		bool operator()( const _packet_t &l, const _packet_t & r ) {
			return !_packet_t::priority( l, r );
			}
		};

	//return create_priority_queue<_packet_t, cmprtr_t>( maxSize, opt );
	return create_queue<_packet_t>( name, maxSize, opt );
	}

queue_manager_t::mp_ow_queue_t<_packet_t> queue_manager_t::create_packet_queue(
	const std::string & name, 
	queue_options_t opt ) {

	return create_packet_queue( name, unlimited, opt );
	}


queue_manager_t::mp_ow_queue_t<rt0_x_message_t> queue_manager_t::create_rt0_x_message_queue(
	const std::string & name,
	const std::uintmax_t maxSize,
	queue_options_t opt ) {

	return create_queue<rt0_x_message_t>( name, maxSize, opt );
	}

queue_manager_t::mp_ow_queue_t<rt0_x_message_t> queue_manager_t::create_rt0_x_message_queue(
	const std::string & name, 
	queue_options_t opt ) {

	return create_rt0_x_message_queue( name, unlimited, opt );
	}

queue_manager_t::mp_ow_queue_t<command_rdm_t> queue_manager_t::create_rt0_x_event_queue(
	const std::string & name, 
	const std::uintmax_t maxSize,
	queue_options_t opt ) {

	return create_queue<command_rdm_t>( name, maxSize, opt );
	}

queue_manager_t::mp_ow_queue_t<command_rdm_t> queue_manager_t::create_rt0_x_event_queue(
	const std::string & name,
	queue_options_t opt ) {

	return create_rt0_x_event_queue( name, unlimited, opt );
	}

queue_manager_t::mp_ow_queue_t<rt1_x_message_data_t> queue_manager_t::create_x_rt0_message_queue(
	const std::string & name, 
	const std::uintmax_t maxSize,
	queue_options_t opt ) {

	return create_queue<rt1_x_message_data_t>( name, maxSize, opt );
	}

queue_manager_t::mp_ow_queue_t<rt1_x_message_data_t> queue_manager_t::create_x_rt0_message_queue(
	const std::string & name,
	queue_options_t opt ) {

	return create_x_rt0_message_queue( name, unlimited, opt );
	}

queue_manager_t::mp_ow_queue_t<std::unique_ptr<i_rt0_command_t >> 
queue_manager_t::create_x_rt0_event_queue(
	const std::string & name, 
	const std::uintmax_t maxSize,
	queue_options_t opt ) {

	return create_queue<std::unique_ptr<i_rt0_command_t >>( name, maxSize, opt );
	}

queue_manager_t::mp_ow_queue_t<std::unique_ptr<i_rt0_command_t>> 
queue_manager_t::create_x_rt0_event_queue(
	const std::string & name,
	queue_options_t opt ) {

	return create_x_rt0_event_queue( name, unlimited, opt );
	}
