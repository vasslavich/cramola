#include "../../internal.h"
#include "../qinput_adpt.h"


using namespace crm;
using namespace crm::detail;


void qinput_adpt_t::push( value_type && item ) {

	if( item.m() ) {

		auto o1 = _preprocessf( std::move( item ) );
		if( o1.message() ) {

			/* �������� � ������� ������� */
			CHECK_PTR( _qTarget )->push( std::move(o1) );
			}
		}
	}

std::uintmax_t qinput_adpt_t::stored_count()const noexcept {
	return CHECK_PTR( _qTarget )->stored_count();
	}

void qinput_adpt_t::clear() noexcept {
	CHECK_PTR( _qTarget )->clear();
	}

qinput_adpt_t::qinput_adpt_t( std::shared_ptr<target_queue_t> && target,
	preprocessor_f && preprocessf )
	: _qTarget( std::move( target ) )
	, _preprocessf( std::move( preprocessf ) ) {
	
	if( !_preprocessf )
		FATAL_ERROR_FWD(nullptr);
	}
