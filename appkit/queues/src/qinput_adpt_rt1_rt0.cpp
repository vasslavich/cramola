#include "../../internal.h"
#include "../../context/context.h"
#include "../qinput_adpt_rt1_rt0.h"
#include "../qmanager.h"


using namespace crm;
using namespace crm::detail;

using qtype = std::decay_t<decltype(std::declval<distributed_ctx_t>().
	queue_manager().
	tcreate_queue<inqueue_adpt_rt1_rt0_t::input_value_t>(
		__FILE_LINE__, (size_t)(-1), queue_options_t::message_stack_node)) >;

inqueue_adpt_rt1_rt0_t::inqueue_adpt_rt1_rt0_t( std::shared_ptr<target_queue_t> && targetQ,
	preprocessor_f && pref )
	: _preprocessf( std::move( pref ) )
	, _targetQ( targetQ ){

	if( !_targetQ || !_preprocessf )
		THROW_MPF_EXC_FWD(nullptr);
	}

inqueue_adpt_rt1_rt0_t::inqueue_adpt_rt1_rt0_t(std::weak_ptr<distributed_ctx_t> ctx,
	preprocessor_f&& pref)
	: _preprocessf(std::move(pref))
	, _targetQ(std::make_shared< qtype>(CHECK_PTR(ctx)->queue_manager().tcreate_queue<input_value_t>(
		__FILE_LINE__,
		(size_t)(-1), 
		queue_options_t::message_stack_node))) {

	if (!_targetQ || !_preprocessf)
		THROW_MPF_EXC_FWD(nullptr);
	}

void inqueue_adpt_rt1_rt0_t::push( base_item_t && item ) {
	if( item.m() ) {
		auto o2 = _preprocessf( std::move( item ) );
		if( o2.m() )
			_targetQ->push( std::move( o2 ) );
		}
	}

std::uintmax_t inqueue_adpt_rt1_rt0_t::stored_count()const noexcept {
	return _targetQ->stored_count();
	}

void inqueue_adpt_rt1_rt0_t::clear()noexcept {
	_targetQ->clear();
	}

