#pragma once


#include <atomic>
#include <queue>
#include <list>
#include <functional>
#include "../base_predecl.h"
#include "../internal.h"
#include "./i_message_queue.h"


namespace crm::detail {


template<typename TMutex,
	typename TMutexScope,
	typename TContainer,
	typename TContainerFunctor>
	class dynamic_queue_await_t final : public i_message_queue_await_t<typename TContainer::value_type> {
	public:
		typedef typename TContainer container_t;
		typedef typename container_t::value_type element_t;
		typedef typename TMutex lck_t;
		typedef typename TMutexScope lck_scp_t;
		typedef typename TContainerFunctor functor_t;
		typedef typename dynamic_queue_await_t<lck_t, lck_scp_t, container_t, functor_t> self_t;
		using await_f = typename self_t::await_result_f;

	private:
		struct notify_item_t {
			async_space_t space;
			await_f h;

			notify_item_t(){}
			notify_item_t(async_space_t spc, await_f && f)
				: space(spc)
				, h(std::move(f)) {}
			};

		/*! �������� ���������� */
		async_sequential_push_handler _lze;
		/*! ��� ������� */
		std::string _objectName;
		/*! ������� ��������� */
		container_t _container;
		/*! ���������� */
		typename mutable lck_t _lock;
		/*! ������� ������ */
		std::atomic<bool> _cancelf = false;
		/*! ������ ����������� */
		std::queue<notify_item_t> _notifyList;

#ifdef CBL_QUEUE_SIZE_CHECKER
		/*! ������������ ������������ ����� ������� */
		std::atomic<std::intmax_t> _lastMaxStored = 0;
#endif

#ifdef CBL_OBJECTS_COUNTER_CHECKER
		utility::__xobject_counter_logger_t<self_t, 1> _xDbgCounter;
#endif

		void _clear(bool notify_)noexcept {
			CHECK_NO_EXCEPTION(functor_t::clear(_container));

			if(notify_) {
				CHECK_NO_EXCEPTION_BEGIN

				notify_item_t slot;
				while(!_notifyList.empty()) {

					std::swap(_notifyList.front(), slot);
					_notifyList.pop();

					if(slot.h) {
						_lze.push(__FILE_LINE__, std::move(slot.h), element_t{});
						}
					}

				CHECK_NO_EXCEPTION_END
				}
			else {
				std::queue<notify_item_t> tmp;
				std::swap(_notifyList, tmp);
				}

#ifdef CBL_QUEUE_SIZE_CHECKER
			_lastMaxStored.store(0, std::memory_order::memory_order_release);
#endif
			}

		void copy(self_t &&o)noexcept {
			if(this != std::addressof(o)) {
				lck_scp_t lock_1(o._lock, std::defer_lock);
				lck_scp_t lock_2(_lock, std::defer_lock);
				std::lock(lock_1, lock_2);

				_objectName = std::move(o._objectName);
				_container = std::move(o._container);
				//_nStored = std::move(o._nStored);
				_lze = std::move(o._lze);
				_cancelf.store(o._cancelf.exchange(true));

#ifdef CBL_OBJECTS_COUNTER_CHECKER
				_xDbgCounter = std::move(o._xDbgCounter);
#endif

				std::swap(_notifyList, o._notifyList);

				CHECK_NO_EXCEPTION(o._clear(true));
				}
			}

	public:
		bool canceled()const noexcept {
			return _cancelf.load();
			}

		bool closed()const noexcept {
			return canceled();
			}

		~dynamic_queue_await_t() {
			CHECK_NO_EXCEPTION(_clear(true));
			}

		template<typename ... TContainerArgs>
		dynamic_queue_await_t(async_space_t scx,
			std::string_view name, 
			std::weak_ptr<distributed_ctx_t> ctx_,
			typename TContainerArgs && ... args)
			: _lze(CHECK_PTR(ctx_)->get_sequential_handler(scx))
			, _objectName(name)
			, _container(std::forward<TContainerArgs>(args)...) 
#ifdef CBL_OBJECTS_COUNTER_CHECKER
			, _xDbgCounter(name)
#endif
			{

			if(_objectName.empty()) {
				FATAL_ERROR_FWD(nullptr);
				}
			}

		dynamic_queue_await_t(const self_t & src) = delete;
		self_t& operator=(const self_t &src) = delete;

		dynamic_queue_await_t(self_t && src)noexcept {
			copy(std::move(src));
			}

		self_t& operator=(self_t &&src)noexcept {
			if(this != std::addressof(src)) {
				copy(std::move(src));
				}

			return (*this);
			}

		void clear() noexcept final{
			lck_scp_t lock(_lock);
			_clear(true);
			}

		queue_operations_t push(const element_t & value){
			return emplace(value);
			}

		queue_operations_t push( element_t&& value) {
			return emplace(std::move(value));
			}

		template<typename ... XVAL,
			const typename std::enable_if_t<std::is_constructible_v<element_t, XVAL...>, int> = 0>
			queue_operations_t emplace(XVAL&& ... args) {

#ifdef CBL_QUEUE_SIZE_CHECKER
			const auto lastMax = _lastMaxStored.load(std::memory_order::memory_order_acquire);
			CBL_VERIFY(lastMax >= 0);
#endif

			lck_scp_t lock(_lock);
			if (!canceled()) {
				if (!_notifyList.empty()) {

					notify_item_t awaitObject;
					std::swap(awaitObject, _notifyList.front());
					_notifyList.pop();

					CBL_VERIFY(awaitObject.h);

					CHECK_NO_EXCEPTION(_lze.push(__FILE_LINE__,
						std::move(awaitObject.h),
						std::forward<XVAL>(args)...));
					}
				else {
					functor_t::push(_container, std::forward<XVAL>(args)...);
					}

				const auto stVal = functor_t::size(_container);

#ifdef CBL_QUEUE_SIZE_CHECKER
				if (stVal > static_cast<size_t>(lastMax)) {
					_lastMaxStored.store(stVal, std::memory_order::memory_order_release);
					if (lastMax && (lastMax % CBL_TRACE_OBJECTS_COUNTER_STEP) == 0) {

						std::string log(SIZE_SEQUENCE_FWDARNING);
						log += ":" + _objectName + "=" + std::to_string(stVal);
						crm::file_logger_t::logging(log, 0, __CBL_FILEW__, __LINE__);
						}
					}
				else {
					if ((lastMax - stVal) >= CBL_TRACE_OBJECTS_COUNTER_STEP) {
						_lastMaxStored.store(stVal, std::memory_order::memory_order_release);
						}
					}
#endif

				return __queue_operations_t::success;
				}
			else {
				return __queue_operations_t::closed;
				}
			}

		void close()noexcept {
			lck_scp_t lock(_lock);

			_cancelf.store(true);
			_clear(true);
			}

		queue_operations_t async_pop(async_space_t space, await_f && callback_f)final {
			return async_pop_handler(space, std::move(callback_f));
			}

		template<typename THandler,
			typename std::enable_if_t<std::is_constructible_v<await_f, THandler>, int> = 0>
		queue_operations_t async_pop_handler(async_space_t space, THandler&& callback_f) {
			if constexpr (is_functional_check_empty_v<THandler>) {
				if (!callback_f) {
					THROW_EXC_FWD(nullptr);
					}
				}

			lck_scp_t lock(_lock);
			if (functor_t::size(_container)) {

				/* ������ �������� */
				element_t value;
				std::swap(functor_t::front(_container), value);
				/* �������� �� ����� */
				functor_t::pop(_container);

				_lze.push(__FILE_LINE__,
					std::forward<THandler>(callback_f),
					std::move(value));
				}
			else {
				_notifyList.emplace(space, std::forward<THandler>(callback_f));
				}

			return __queue_operations_t::success;
			}

		const std::string& name()const {
			return _objectName;
			}
	};


template<typename TElement>
auto queue_async_pop(async_space_t space_, i_message_queue_await_t<TElement> & q) {
	struct Awaiter {
		async_space_t space;
		i_message_queue_await_t<TElement> & q;
		typename i_message_queue_await_t<TElement>::value_type r;

		bool await_ready() { return false; }
		void await_suspend(std::experimental::coroutine_handle<> coro) {
			if (!q.async_pop(space, [this, coro](auto && r_) mutable {

				r = std::move(r_);
				coro.resume();
				})) {

				coro.resume();
				}
			}
		auto await_resume() { return std::move(r); }
		};
	return Awaiter{ space_, q };
	}
}


