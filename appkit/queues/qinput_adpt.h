#pragma once


#include "./i_message_queue.h"
#include "../xpeer/rt0_xpeer.h"


namespace crm::detail{


class qinput_adpt_t final : public  i_message_input_t<input_item_t> {
public:
	typedef input_item_t base_item_t;
	typedef xpeer_base_rt0_t::xpeer_desc_t xpeer_desc_t;
	typedef xpeer_base_rt0_t::input_stack_t target_queue_t;
	typedef target_queue_t::value_type input_value_t;
	typedef std::function<input_value_t( base_item_t && item )> preprocessor_f;

private:
	std::shared_ptr<target_queue_t> _qTarget;
	preprocessor_f _preprocessf;

#ifdef CBL_MPF_OBJECTS_COUNTER_CHECKER_INPUT_STACK
	crm::utility::__xobject_counter_logger_t<qinput_adpt_t, 1> _xDbgCounter;
#endif

	void push( base_item_t && item ) final;
	std::uintmax_t stored_count()const noexcept final;
	void clear()noexcept final;

public:
	qinput_adpt_t( std::shared_ptr<target_queue_t> && target,
		std::function<input_value_t( base_item_t && item )> && preprocessf );
	};
}

