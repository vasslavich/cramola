#pragma once


#include <mutex>
#include <condition_variable>
#include "./dynqueue_ctr.h"
#include "./dynqueue_await.h"
#include "./opfunctors.h"
#include "./i_message_queue.h"
#include "../channel_terms/internal_terms.h"


namespace crm::detail{


enum class queue_options_t {
	nodef,
	message_stack_node, 
	message_stack_module_channel
	};

class queue_manager_t {
private:
	static const std::uintmax_t unlimited = (std::uintmax_t)(-1);

	typedef std::mutex lck_t;
	typedef std::unique_lock<lck_t> lck_scp_t;
	typedef std::condition_variable condvar_t;

	template<typename TElement, typename TElementOrderer>
	using base_priority_queue_t = std::priority_queue<TElement, std::vector<TElement>, TElementOrderer>;


	template<typename TElement, typename TElementOrderer>
	using mp_ow_priority_queue_t = dynamic_queue_ctr_t<
		lck_t,
		lck_scp_t,
		condvar_t,
		base_priority_queue_t<TElement, TElementOrderer>,
		functor_container_priority_queue_t<base_priority_queue_t<TElement, TElementOrderer>>>;

	template<typename TElement>
	using mp_ow_queue_t = dynamic_queue_ctr_t<
		lck_t,
		lck_scp_t,
		condvar_t,
		std::list<TElement>,
		functor_container_list_t<std::list<TElement>>>;

	template<typename TElement>
	using mp_await_queue_t = dynamic_queue_await_t<
		lck_t,
		lck_scp_t,
		std::list<TElement>,
		functor_container_list_t<std::list<TElement>>>;

	std::weak_ptr<distributed_ctx_t> _ctx;

	template<typename TElement, typename TElementOrderer>
	mp_ow_priority_queue_t<TElement, TElementOrderer> create_priority_queue(
		const std::string& name, const std::uintmax_t, queue_options_t /*opt*/) {

		return mp_ow_priority_queue_t<TElement, TElementOrderer>{_ctx, name };
		}

	template<typename TElement>
	mp_ow_queue_t<TElement> create_queue(
		const std::string & name, const std::uintmax_t, queue_options_t /*opt*/ ) {

		return mp_ow_queue_t<TElement>{ _ctx, name };
		}

	template<typename TElement>
	mp_await_queue_t<TElement> create_await_queue(async_space_t scx,
		const std::string & name  ) {
		
		return mp_await_queue_t<TElement>{scx, name, _ctx};
		}

public:
	typedef input_item_t rt0_input_value_t;
	typedef std::unique_ptr<i_iol_ohandler_t> rt0_output_value_t;

	queue_manager_t( std::weak_ptr<distributed_ctx_t> ctx );

	mp_ow_queue_t<rt0_input_value_t> create_rt0_input_queue(
		const std::string & name, const std::uintmax_t maxSize, queue_options_t opt );
	mp_ow_queue_t<rt0_input_value_t> create_rt0_input_queue(
		const std::string & name, queue_options_t );

	mp_ow_queue_t<rt0_output_value_t> create_rt0_output_queue(
		const std::string & name, const std::uintmax_t maxSize, queue_options_t opt );
	mp_ow_queue_t<rt0_output_value_t> create_rt0_output_queue(
		const std::string & name, queue_options_t );

	mp_ow_queue_t<_packet_t> create_packet_queue(
		const std::string & name, const std::uintmax_t maxSize, queue_options_t opt );
	mp_ow_queue_t<_packet_t> create_packet_queue(
		const std::string & name, queue_options_t );


	mp_ow_queue_t<rt0_x_message_t> create_rt0_x_message_queue(
		const std::string & name, const std::uintmax_t maxSize, queue_options_t opt );
	mp_ow_queue_t<rt0_x_message_t> create_rt0_x_message_queue(
		const std::string & name, queue_options_t );

	mp_ow_queue_t<command_rdm_t> create_rt0_x_event_queue(
		const std::string & name, const std::uintmax_t maxSize, queue_options_t opt );
	mp_ow_queue_t<command_rdm_t> create_rt0_x_event_queue(
		const std::string & name, queue_options_t );



	mp_ow_queue_t<rt1_x_message_data_t> create_x_rt0_message_queue(
		const std::string & name, const std::uintmax_t maxSize, queue_options_t opt );
	mp_ow_queue_t<rt1_x_message_data_t> create_x_rt0_message_queue(
		const std::string & name, queue_options_t );

	mp_ow_queue_t<std::unique_ptr<i_rt0_command_t>>  create_x_rt0_event_queue(
		const std::string & name, const std::uintmax_t maxSize, queue_options_t opt );
	mp_ow_queue_t<std::unique_ptr<i_rt0_command_t>>  create_x_rt0_event_queue(
		const std::string & name, queue_options_t );

	template<typename TElement>
	auto tcreate_queue(
		const std::string & name, const std::uintmax_t maxSize, queue_options_t opt ) {

		return create_queue<TElement>(name, maxSize, opt);
		}

	template<typename TElement>
	auto tcreate_await_queue(const std::string & name,
		async_space_t scx) {

		return create_await_queue<TElement >(scx, name);
		}

	template<typename TElement>
	auto tcreate_queue(
		const std::string & name, queue_options_t opt ) {

		return tcreate_queue<TElement >( name, unlimited, opt );
		}
	};
}


