#pragma once


#include "../channel_terms/subscribe_terms.h"
#include "../queues/i_message_queue.h"
#include "./base_terms.h"
#include "./xwlink.h"
#include "../messages/internal_terms.h"


namespace crm::detail{


struct i_x0_xpeer_address_handler{
	virtual ~i_x0_xpeer_address_handler();

	static const std::string null_address;
	static const int null_port;

	using rdm_local_identity_t = rt0_node_rdm_t;
	using xpeer_desc_t = rdm_local_identity_t;
	using remote_object_endpoint_t = i_xpeer_t::remote_object_endpoint_t;

	virtual remote_object_state_t state()const noexcept{
		return remote_object_state_t::null;
		}

	virtual void set_state( remote_object_state_t )noexcept{}

	virtual const session_description_t& session()const noexcept{
		return session_description_t::null;
		}

	virtual const rdm_local_identity_t& rdm_local_identity()const noexcept{
		return rdm_local_identity_t::null;
		}

	virtual const local_object_id_t& local_id()const noexcept{
		return local_object_id_t::null;
		}

	virtual const rt0_node_rdm_t& rt0rdm()const noexcept{
		return rt0_node_rdm_t::null;
		}

	virtual rtl_table_t direction()const noexcept{
		return rtl_table_t::undefined;
		}

	virtual peer_direction_t peer_direction()const noexcept{
		return peer_direction_t::undefined;
		}

	virtual const identity_descriptor_t& this_id()const noexcept{
		return identity_descriptor_t::null;
		}

	virtual const identity_descriptor_t& remote_id()const noexcept{
		return identity_descriptor_t::null;
		}

	virtual const identity_descriptor_t& id()const noexcept{
		return identity_descriptor_t::null;
		}

	virtual const xpeer_desc_t& desc()const noexcept{
		return xpeer_desc_t::null;
		}

	/*! ����� ��������� ����� ����������� */
	virtual const std::string& address()const noexcept{
		return null_address;
		}

	/*! ���� �������� ����� ����������� */
	virtual int port()const noexcept{
		return null_port;
		}

	virtual connection_key_t connection_key()const noexcept{
		return null_value_t<connection_key_t>::null;
		}

	virtual remote_object_endpoint_t remote_object_endpoint()const noexcept{
		return remote_object_endpoint_t::null;
		}

	virtual const std::string & name()const noexcept{
		return null_address;
		}

	virtual rtl_level_t level()const noexcept{
		return rtl_level_t::l0;
		}
	};

struct i_xpeer_rt0_t : public i_xpeer_t{
	static const subscriber_node_id_t subscribe_address_datagram_for_this;
	static const std::string command_name_identity;
	static const std::string command_name_initialize;

	using rdm_local_identity_t = i_x0_xpeer_address_handler::rdm_local_identity_t;
	using xpeer_desc_t = i_x0_xpeer_address_handler::xpeer_desc_t;


	struct input_value_t {
	private:
		friend class xpeer_base_rt0_t;

		std::unique_ptr<i_iol_ihandler_t> m_;
		/*xpeer_desc_t d_;*/
		rtl_roadmap_obj_t d_;
		std::weak_ptr<i_xpeer_rt0_t> _xpeerWlnk;

		input_value_t()noexcept;
		input_value_t(std::unique_ptr<i_iol_ihandler_t>&& m,
			rtl_roadmap_obj_t&& d,
			std::weak_ptr<i_xpeer_rt0_t>)noexcept;

	public:
		const rtl_roadmap_obj_t& rdm()const noexcept;
		const std::unique_ptr<i_iol_ihandler_t>& message()const& noexcept;
		std::unique_ptr<i_iol_ihandler_t> message() && noexcept;

		template<typename TMessage,
			typename std::enable_if_t<is_out_message_requirements_v<TMessage>, int> = 0>
		void send_back(TMessage&& m) {
			if (auto sxp = _xpeerWlnk.lock()) {
				(static_cast<xpeer_base_rt0_t&>(*sxp)).push(std::forward<TMessage>(m));
			}
		}
	};

	typedef std::function<void(input_value_t&&)> input_handler_opeer_t;
	/*! Stack of a items input messages */
	typedef i_message_input_t<input_value_t> input_stack_t;
	/*! ������� �������� ������� �������� ��������� */
	typedef std::function<std::unique_ptr<i_message_input_t<input_value_t>>()> ctr_0_input_queue_t;

	typedef input_stack_t::value_type subscribe_hndl_value_t;

	typedef std::function<void(const input_value_t & message)> handler_f;

	typedef i_message_output_t<std::unique_ptr<i_iol_ohandler_t>> output_stack_t;
	typedef output_stack_t::value_type output_value_t;
	typedef session_description_t session_t;
	typedef remote_object_state_t state_t;

	virtual const session_description_t& session()const noexcept = 0;
	virtual const rt0_node_rdm_t& rt0rdm()const noexcept = 0;
	virtual const xpeer_desc_t& desc()const noexcept = 0;

	/*! �������� �� ��������� ��������� ��� ���������� ��������������.
	\param id ������� �������������
	\param once ���� ������� true, ����������� �������� ���� ���, ����� �� ��� ���,
	���� �� ����� ��������� ������ ����������� � ������� ������ /ref unsubscribe_hndl_id.
	*/
	virtual subscribe_invoker_result subscribe_hndl_id( async_space_t scx, 
		const _address_hndl_t & id,
		subscribe_options opt,
		subscribe_message_handler && handler ) = 0;

	/*virtual subscribe_invoker_result subscribe_hndl_id(async_space_t scx, const _address_hndl_t & id, subscribe_options opt,
		const subscribe_handler_t & handler) = 0;*/

		/*! ����� �� ��������� ����������� ��� ���������� ��������������
		\param id ������������� ����������
		*/
	virtual void unsubscribe_hndl_id( const _address_hndl_t & id )noexcept = 0;

	virtual void subscribe_event( events_list && el ) = 0;

	virtual subscribe_result subscribe_event( async_space_t scx,
		const _address_event_t & id,
		bool once,
		__event_handler_f && handler ) = 0;

	virtual void unsubscribe_event( const _address_event_t & id )noexcept = 0;

	virtual void write( outbound_message_unit_t && ) = 0;

	virtual std::shared_ptr<i_x0_xpeer_address_handler> address_handler()const noexcept = 0;
	};
}

namespace crm {
/*namespace detail {


struct rt0_X_item_t {
	std::unique_ptr<i_iol_ihandler_t> m_;
	rtl_roadmap_obj_t rdm_;

#ifdef CBL_PEER_L0_INPUT_STAMP_INCLUDE_REF2PEER
	xpeer_rt0_wlink_type plnk_;
#endif

	rt0_X_item_t()noexcept;

	rt0_X_item_t(std::unique_ptr<i_iol_ihandler_t>&& m,
		const rtl_roadmap_obj_t& rdm
#ifdef CBL_PEER_L0_INPUT_STAMP_INCLUDE_REF2PEER
		, xpeer_rt0_wlink_type&& lnk
#endif
	);

	const rtl_roadmap_obj_t& desc()const noexcept;
	const std::unique_ptr<i_iol_ihandler_t>& m()const& noexcept;
	std::unique_ptr<i_iol_ihandler_t> m() && noexcept;

#ifdef CBL_PEER_L0_INPUT_STAMP_INCLUDE_REF2PEER
	const xpeer_rt0_wlink_type& link()const noexcept;
	xpeer_rt0_wlink_type& link() noexcept;
#endif

	};

}*/

namespace detail {
using rt0_X_item_t = i_xpeer_rt0_t::input_value_t;
}

using message_stamp_t = detail::i_xpeer_rt0_t::input_value_t;//detail::rt0_X_item_t;
}

