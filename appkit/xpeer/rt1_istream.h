#pragma once


#include <memory>
#include "./i_rt1_xpeer.h"
#include "./rt1_base_xstream.h"


namespace crm::detail {


class ix_rt1_stream_t : public std::enable_shared_from_this<ix_rt1_stream_t>,

	/*! ��������� ��������� ������ �������� ������ 1 */
	public istream_x_rt1_t,
	/*! �������� �� ������������� ������ � ������ */
	public i_rt1_router_targetlink_t {

	typedef ix_rt1_stream_t self_t;

private:
	struct __address_handler final : public i_x1_stream_address_handler {
		rt0_node_rdm_t _nodeId;

		/*! ������� ����� */
		local_object_id_t _locid;
		rtl_roadmap_obj_t _remoteRdm;
		rtl_roadmap_event_t _rdmEv;
		rtl_roadmap_obj_t _rdmMsg;

		__address_handler(std::weak_ptr<distributed_ctx_t> ctx_,
			const rt0_node_rdm_t & nodeId,
			const identity_descriptor_t & thisId_,
			const identity_descriptor_t & remoteId_,
			const session_description_t & session_);

		const rtl_roadmap_event_t& ev_rdm()const  noexcept;
		const rtl_roadmap_obj_t& msg_rdm()const  noexcept;
		const rtl_roadmap_obj_t& this_rdm()const noexcept final;
		const rtl_roadmap_obj_t& remote_rdm()const noexcept final;
		const identity_descriptor_t& remote_id()const noexcept final;
		const identity_descriptor_t& this_id()const noexcept final;
		const identity_descriptor_t& id()const noexcept final;
		const xpeer_desc_t& desc()const noexcept final;
		const rt0_node_rdm_t& node_id()const noexcept final;
		const local_object_id_t& local_id()const noexcept final;
		const local_command_identity_t& local_object_command_id()const noexcept final;
		const session_description_t& session()const noexcept final;
		rtl_table_t direction()const noexcept final;
		const std::string& address()const noexcept final;
		int port()const noexcept final;
		remote_object_endpoint_t remote_object_endpoint()const noexcept final;
		connection_key_t connection_key()const noexcept final;
		};

	rt1_base_xstream_t _xbase;
	std::shared_ptr<__address_handler> _addrh;
	std::shared_ptr<i_decode_tail_stream2peer_t_x1> _tail;

	std::atomic<bool> _closef{ false };
	std::atomic<bool> _rtRegistered{ false };

#ifdef CBL_MPF_OBJECTS_COUNTER_CHECKER_RT1_PEER
	crm::utility::__xobject_counter_logger_t<self_t, 1> _xDbgCounter;
#endif

	//static size_t stack_size(const std::weak_ptr<distributed_ctx_t> ctx_)noexcept;

	/*! �������� ���������������� ��������� � ������������� ������ 1. ������������ /ref xpeer_in_rt1_t  */
	void push_to_x(outbound_message_unit_t && buf) final;

	/*! ��������� ��������� ��������� ������� */
	void set_state(remote_object_state_t st)noexcept final;
	/*! ������ ��������� ��������� ������� */
	remote_object_state_t get_state()const  noexcept final;

	/*! ���������� ��������� �� �������������� ������ 1 */
	void rt1_router_hndl(std::unique_ptr<i_command_from_rt02rt1_t> && response)noexcept final;
	/*! ���������� ������� �� �������������� ������ 1 */
	void rt1_router_hndl(rt0_x_message_unit_t && msg, invoke_context_trait ic)final;

	void addressed_exception(const _address_hndl_t & address,
		std::unique_ptr<dcf_exception_t> && ntf,
		invoke_context_trait )noexcept final;

	void close_from_router()noexcept final;
	void unroute_command_link()noexcept final;
	bool is_once()const noexcept final;

	std::shared_ptr<i_decode_tail_stream2peer_t_x1> reset_tail()noexcept;
	bool set_tail(std::shared_ptr<i_decode_tail_stream2peer_t_x1> && tail_)noexcept final;
	std::shared_ptr<i_decode_tail_stream2peer_t_x1> tail()const noexcept;

	void close_tail()noexcept;
	void close_objects()noexcept;
	void close(unregister_message_track_reason_t r)noexcept;
	void unbind_reslink()noexcept;

	std::string make_trace_line()const noexcept;

protected:
	ix_rt1_stream_t(std::weak_ptr<distributed_ctx_t> ctx_,
		std::weak_ptr < i_rt1_main_router_t> outs,
		const rt0_node_rdm_t & nodeId,
		const identity_descriptor_t & thisId_,
		const identity_descriptor_t & remoteId,
		const session_description_t & session);

public:
	void register_routing( __error_handler_point_f && rgres );
	void unregister_routing( detail::unregister_message_track_reason_t r )noexcept;

	std::shared_ptr<i_rt1_main_router_t> router()const noexcept final;
	std::shared_ptr<i_x1_stream_address_handler> address_point()const noexcept final;
	rtl_table_t direction()const noexcept final;
	const rtl_roadmap_obj_t& msg_rdm()const noexcept final;
	const rtl_roadmap_link_t& msg_rdm_link()const noexcept  final;
	const rtl_roadmap_event_t& ev_rdm()const  noexcept final;
	const rtl_roadmap_obj_t& this_rdm()const  noexcept  final;
	const rtl_roadmap_obj_t& remote_rdm()const  noexcept final;
	const rt0_node_rdm_t& node_id()const noexcept  final;
	const identity_descriptor_t& remote_id()const noexcept  final;
	const identity_descriptor_t& this_id()const  noexcept final;
	const local_object_id_t& local_id()const noexcept  final;
	const local_command_identity_t& local_object_command_id()const noexcept final;
	const session_description_t& session()const noexcept  final;
	void close()noexcept final;
	bool closed()const noexcept  final;
	bool is_integrity_provided()const noexcept;

	static std::shared_ptr<ix_rt1_stream_t> create(std::weak_ptr<distributed_ctx_t> ctx_,
		std::weak_ptr < i_rt1_main_router_t> outs,
		const rt0_node_rdm_t & nodeId,
		const identity_descriptor_t & thisId_,
		const identity_descriptor_t & remoteId,
		const session_description_t & session);
	~ix_rt1_stream_t();
	};
}

