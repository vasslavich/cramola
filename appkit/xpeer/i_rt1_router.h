#pragma once


#include "../internal_invariants.h"
#include "../queues/i_message_queue.h"
#include "../channel_terms/internal_terms.h"
#include "../utilities/handlers/errhndl.h"
#include "./base_terms.h"


namespace crm::detail{


struct i_rt1_router_targetlink_t : public i_rt1_command_link_t{
	virtual ~i_rt1_router_targetlink_t() = 0;

	/*! ���������� ������� */
	virtual void rt1_router_hndl( rt0_x_message_unit_t && msg, invoke_context_trait ic) = 0;

	virtual void addressed_exception( const _address_hndl_t & address,
		std::unique_ptr<dcf_exception_t> && ntf,
		invoke_context_trait ic )noexcept = 0;

	/*! ����� �������� ��������� */
	virtual const rtl_roadmap_obj_t& msg_rdm()const noexcept = 0;
	virtual const local_object_id_t& local_id()const noexcept = 0;
	virtual void close()noexcept = 0;
	};


struct i_rt1_main_router_t{
	virtual ~i_rt1_main_router_t() = 0;
	
	using xpeer_desc_t = i_x1_stream_address_handler::xpeer_desc_t;
	using input_value_t = i_x1_stream_address_handler::input_value_t;
	typedef i_message_input_t<input_value_t> input_queue_t;
	typedef i_message_queue_t<input_value_t> message_stack_t;
	typedef i_x1_stream_address_handler::ctr_1_input_queue_t ctr_1_input_queue_t;

	virtual void opeer_fail( const remote_connection_desc_t & opeer, std::unique_ptr<crm::dcf_exception_t> && exc )noexcept = 0;

	virtual void register_events_slot( std::list<rtx_command_type_t> && evlst,
		std::weak_ptr<i_rt1_router_targetlink_t> hndl,
		__error_handler_point_f && rh ) = 0;
	virtual void register_messages_slot( const rtl_roadmap_obj_t & objrdm,
		std::weak_ptr<i_rt1_router_targetlink_t> hndl,
		__error_handler_point_f && rh ) = 0;

	virtual void unregister_events_slot( const rtl_roadmap_event_t & evrdm )noexcept = 0;
	virtual void unregister_messages_slot( const rtl_roadmap_obj_t & rdms,
		unregister_message_track_reason_t r )noexcept = 0;

	virtual void unregister_remote_object_slot( const rt1_endpoint_t & targetEP,
		const identity_descriptor_t & targetId,
		const source_object_key_t & srcKey )noexcept = 0;

	virtual void push2rt( rt1_x_message_t && msg ) = 0;

	virtual std::shared_ptr<rt1_command_router_t> command_router()noexcept = 0;
	virtual std::shared_ptr<const rt1_command_router_t> command_router()const noexcept = 0;
	};
}
