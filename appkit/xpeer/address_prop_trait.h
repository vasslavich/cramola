#pragma once


#include <type_traits>


namespace crm {
namespace mpf {
namespace detail {


template<typename T, typename Enable = void>
struct is_peer_address_properties_trait_z_t : std::false_type {};


template<typename T>
struct is_peer_address_properties_trait_z_t<T, std::void_t<
	decltype(std::declval<T>().id()),
	decltype(std::declval<T>().local_id()),
	decltype(std::declval<T>().desc()),
	decltype(std::declval<T>().address()),
	decltype(std::declval<T>().this_id()),
	decltype(std::declval<T>().remote_id()),
	decltype(std::declval<T>().direction()),
	decltype(std::declval<T>().port()),
	decltype(std::declval<T>().remote_object_endpoint()),
	decltype(std::declval<T>().state()),
	decltype(std::declval<T>().level()),
	decltype(std::declval<T>().connection_key())>> : std::true_type{};

template<typename T>
constexpr bool is_peer_address_properties_trait_z_v = is_peer_address_properties_trait_z_t<T>::value;


template<typename T, typename Enable = void>
struct is_peer_address_properties_trait_z_2_t : std::false_type {};

template<typename T>
struct is_peer_address_properties_trait_z_2_t<T, std::void_t<
	typename std::enable_if_t<is_peer_address_properties_trait_z_v<T>>,
	decltype(std::declval<T>().this_rdm()),
	decltype(std::declval<T>().remote_rdm())>> : std::true_type{};

template<typename T>
constexpr bool is_peer_address_properties_trait_z_2_v = is_peer_address_properties_trait_z_2_t<T>::value;
}
}
}

