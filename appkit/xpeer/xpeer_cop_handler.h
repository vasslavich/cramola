#pragma once

#include "../internal_invariants.h"

namespace crm:: detail {


struct xpeer_cop_handler {
	using tail_f = std::function<void()>;

	xpeer_cop_handler(tail_f && f_, std::weak_ptr<distributed_ctx_t> wptr)noexcept;
	~xpeer_cop_handler();

	xpeer_cop_handler(const xpeer_cop_handler &) = delete;
	xpeer_cop_handler& operator=(const xpeer_cop_handler &) = delete;

	void close()noexcept;

private:
	tail_f _f;
	std::weak_ptr<distributed_ctx_t> _ctx;
	std::atomic<bool> _invf{ false };

	void invoke()noexcept;
	};
}

