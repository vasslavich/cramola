#pragma once


#include "../channel_terms/i_channel_io.h"
#include "../functorapp/functorapp.h"
#include "./i_rt0_xpeer.h"
#include "./i_rt1_xpeer.h"
#include "../channel_terms/i_stream.h"
#include "../queues/i_message_queue.h"
#include "../channel_terms/i_channel_io.h"


namespace crm{

using input_message_knop_l1 = detail::i_xpeer_rt1_t::input_value_t;
using input_message_knop_l0 = detail::i_xpeer_rt0_t::input_value_t;

template<typename T>
constexpr bool is_rt1_outbound_connection_input_handler_v = std::is_invocable_v<T, input_message_knop_l1&&>;

template<typename T>
constexpr bool is_rt0_outbound_connection_input_handler_v = std::is_invocable_v<T, input_message_knop_l0&&>;

template<typename T>
constexpr bool is_rt1_hub_input_handler_v = std::is_invocable_v<T, ipeer_t&&, input_message_knop_l1&&>;

template<typename T>
constexpr bool is_rt0_hub_input_handler_v = std::is_invocable_v<T, input_message_knop_l0&&>;



struct input2stack_t final : public i_input_messages_stock_t{
	typedef detail::i_x1_stream_address_handler::ctr_1_input_queue_t input_stack_ctr_t;

	input_stack_ctr_t ictr;

	input2stack_t( input_stack_ctr_t && ctr )
		: ictr( std::move( ctr ) ){}

	std::unique_ptr<i_input_messages_stock_t> get()final{
		return std::make_unique< input2stack_t>( *this );
		}
	};

struct hub_input2handler_rt1_t final : public i_input_messages_stock_t{
	using input_handler_f = crm::utility::invoke_functor_shared<void, std::add_rvalue_reference_t<ipeer_t>, std::add_rvalue_reference_t<input_message_knop_l1>>;

	input_handler_f ihndl;

	template<typename TH,
		typename std::enable_if_t <is_rt1_hub_input_handler_v<TH>, int> = 0>
	hub_input2handler_rt1_t( TH && hndl )
		: ihndl( std::forward<TH>( hndl ) ){}

	std::unique_ptr<i_input_messages_stock_t> get()final{
		return std::make_unique< hub_input2handler_rt1_t>( *this );
		}
	};

struct opeer_l1_input2handler_t final : public i_input_messages_stock_t{
	typedef detail::i_xpeer_rt1_t::input_handler_opeer_t input_handler_t;

	input_handler_t ihndl;

	template<typename H,
		typename std::enable_if_t<is_rt1_outbound_connection_input_handler_v<H>, int> = 0>
	opeer_l1_input2handler_t(H&& h)
		: ihndl(std::forward<H>(h)) {}

	std::unique_ptr<i_input_messages_stock_t> get()final{
		return std::make_unique< opeer_l1_input2handler_t>( *this );
		}
	};

struct opeer_l0_input2handler_t final : public i_input_messages_stock_t {
	typedef detail::i_xpeer_rt0_t::input_handler_opeer_t input_handler_t;

	input_handler_t ihndl;

	template<typename H,
		typename std::enable_if_t<is_rt0_outbound_connection_input_handler_v<H>, int> = 0>
	opeer_l0_input2handler_t(H&& h)
		: ihndl(std::forward<H>(h)) {}

	std::unique_ptr<i_input_messages_stock_t> get()final {
		return std::make_unique< opeer_l0_input2handler_t>(*this);
		}
	};


template<typename Func,
	typename = std::enable_if_t<is_rt0_hub_input_handler_v<Func>>>
struct input2handler_rt0_t final : 
	public i_message_input_t<input_message_knop_l0>{

	//using input_handler_f = crm::utility::invoke_functor<void, std::add_rvalue_reference_t<input_message_knop_l0>>;
	//input_handler_f _h;

	Func _h;

	template<typename H,
		typename std::enable_if_t<is_rt0_hub_input_handler_v<H>, int> = 0>
		input2handler_rt0_t( H && h )
		: _h( std::forward<H>( h ) ){}

	void push(input_message_knop_l0&& item )final{
		_h( std::move( item ) );
		}

	std::uintmax_t stored_count()const noexcept final{
		return 0;
		}

	void clear()noexcept final{}
	};


namespace detail{

struct rtmessate_entry_t {
	using unregister_f = std::function<void(const rtl_roadmap_obj_t &id,
		const std::shared_ptr<i_rt0_peer_handler_t> & peer)>;

	std::weak_ptr<io_rt0_x_channel_t> channel2x_;
	std::shared_ptr<i_rt0_peer_handler_t> xpeer_;
	unregister_f _hunsubscriber;

	rtmessate_entry_t();

	static rtmessate_entry_t make(std::weak_ptr<io_rt0_x_channel_t> channel2x,
		std::shared_ptr<i_rt0_peer_handler_t> peer, unregister_f && unsubscriber);

private:
	rtmessate_entry_t(std::weak_ptr<io_rt0_x_channel_t> channel2x,
		std::shared_ptr<i_rt0_peer_handler_t> peer, unregister_f && unsubscriber);
	};

struct rtl_rdm_keypair_t {
	rtl_roadmap_obj_t rdm;
	std::unique_ptr<rtmessate_entry_t> channel;

	~rtl_rdm_keypair_t();

	rtl_rdm_keypair_t(const rtl_rdm_keypair_t &) = delete;
	rtl_rdm_keypair_t& operator=(const rtl_rdm_keypair_t &) = delete;

	rtl_rdm_keypair_t(rtl_rdm_keypair_t &&) = default;
	rtl_rdm_keypair_t& operator=(rtl_rdm_keypair_t &&) = default;

	rtl_rdm_keypair_t()noexcept;
	rtl_rdm_keypair_t(const  rtl_roadmap_obj_t & rdm,
		std::unique_ptr<rtmessate_entry_t> && channel)noexcept;

	void invoke_remote_unsubscribe()noexcept;
	};
}
}

