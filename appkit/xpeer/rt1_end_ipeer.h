#pragma once


#include "./base_terms.h"
#include "./i_rt1_xpeer.h"
#include "./rt1_opeer.h"
#include "../channel_terms/events_terms.h"
#include "./mk_outbound_peer.h"


namespace crm{


class ipeer_t{
private:
	detail::xpeer_base_rt1_t::link_t _lnk;

#if defined( CBL_MPF_OBJECTS_COUNTER_CHECKER_RT1_PEER ) && ( CBL_MPF_TRACELEVEL_ST >= CBL_MPF_TRACELEVEL_ST_4)
	utility::__xobject_counter_logger_t<ipeer_t, 1> _xDbgCounter;
#endif

public:
	using input_value_t = detail::xpeer_base_rt1_t::input_value_t;
	using xpeer_desc_t = detail::xpeer_base_rt1_t::xpeer_desc_t;
	using remote_object_endpoint_t = detail::xpeer_base_rt1_t::remote_object_endpoint_t;

	ipeer_t()noexcept;
	ipeer_t( detail::xpeer_base_rt1_t::link_t && lnk )noexcept;

	ipeer_t(const ipeer_t &)noexcept;
	ipeer_t(ipeer_t &&)noexcept;
	ipeer_t& operator=(const ipeer_t &)noexcept;
	ipeer_t& operator=(ipeer_t &&)noexcept;

	[[nodiscard]]
	detail::subscribe_invoker_result subscribe_hndl_id( async_space_t scx,
		const detail::_address_hndl_t &hndl, 
		detail::subscribe_options opt,
		detail::subscribe_message_handler && f );
	void unsubscribe_hndl_id( const detail::_address_hndl_t &hndl )noexcept;

	template<typename O,
		typename std::enable_if_t < is_out_message_requirements_v<O>, int> = 0>
	void push(O && o) {
		_lnk.push(std::move(o));
		}

	void subscribe_event( detail::events_list && el );

	[[nodiscard]]
	subscribe_result subscribe_event( async_space_t scx, 
		const _address_event_t &hndl, 
		bool once,
		detail::__event_handler_f && f ) ;
	void unsubscribe_event( const _address_event_t &hndl )noexcept;

	void close()noexcept;
	bool closed()const  noexcept ;
	bool is_opened()const noexcept;
	const xpeer_desc_t& desc()const  noexcept ;
	std::string address()const  noexcept ;
	int port()const  noexcept ;
	bool renewable_connection()const  noexcept ;
	detail::rtl_table_t direction()const noexcept;

	remote_object_state_t state()const  noexcept;
	const local_object_id_t& local_id()const noexcept;
	const identity_descriptor_t& this_id()const  noexcept;
	const identity_descriptor_t& remote_id()const noexcept;
	const identity_descriptor_t& id()const noexcept;
	remote_object_endpoint_t remote_object_endpoint()const  noexcept;

	detail::rtl_level_t level()const  noexcept;
	std::shared_ptr<i_peer_statevalue_t> state_value()noexcept;
	std::shared_ptr<const i_peer_statevalue_t> state_value()const noexcept;
	void set_state_value( std::shared_ptr<i_peer_statevalue_t> && stv )noexcept;
	std::shared_ptr<const i_peer_remote_properties_t> remote_properties()const noexcept;
	connection_key_t connection_key()const noexcept;
	std::shared_ptr<distributed_ctx_t> ctx()const  noexcept;
	detail::peer_commands_result_t execute_command( peer_command_t cmd );
	};
}
