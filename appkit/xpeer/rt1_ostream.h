#pragma once


#include <memory>
#include "./base_predecl.h"
#include "./rt1_base_xstream.h"
#include "../cmdrt/i_channel_commands.h"


namespace crm::detail {


class ox_rt1_stream_t;
struct connection_handler_guard {
	std::function<void(async_operation_result_t, std::shared_ptr<ox_rt1_stream_t>, std::unique_ptr<dcf_exception_t>&& e)> _f;
	std::atomic<bool> _if{ false };

#ifdef CBL_MPF_OBJECTS_COUNTER_CHECKER_RT1_PEER
	crm::utility::__xobject_counter_logger_t<connection_handler_guard, 1> _xDbgCounter;
#endif

	~connection_handler_guard();

	template<typename THandler>
	connection_handler_guard(THandler&& h)
		: _f(std::forward<THandler>(h)) {}

	connection_handler_guard(const connection_handler_guard&) = delete;
	connection_handler_guard& operator=(const connection_handler_guard&) = delete;

	connection_handler_guard(connection_handler_guard&&)noexcept;
	connection_handler_guard& operator=(connection_handler_guard&&)noexcept;

	void invoke(async_operation_result_t r, std::shared_ptr<ox_rt1_stream_t>, std::unique_ptr<dcf_exception_t>&& e);
	void invoke(async_operation_result_t r, std::shared_ptr<ox_rt1_stream_t>);
	void invoke(std::unique_ptr<dcf_exception_t>&& e)noexcept;
	void operator()(async_operation_result_t r, std::shared_ptr<ox_rt1_stream_t>);
	void operator()(async_operation_result_t r, std::shared_ptr<ox_rt1_stream_t>, std::unique_ptr<dcf_exception_t>&& e);
	};


class ox_rt1_stream_t  final :
	public std::enable_shared_from_this<ox_rt1_stream_t>,
	/*! ��������� ���������� ������ �������� ������ 1 */
	public ostream_x_rt1_t,
	/*! �������� �� ������������� ������ � ������ */
	public i_rt1_router_targetlink_t {

	friend class rt1_xpeer_router_t;
	typedef ox_rt1_stream_t self_t;

public:
	using connection_handler = connection_handler_guard;

private:
	struct rdm_key_holder_t {
	private:
		/* ������� �� ������ !!! */
		session_id_t _sessionId;
		rtl_roadmap_obj_t _rdmMsg;
		rtl_roadmap_obj_t _remoteRdm;
		std::atomic<bool> _entryf{ false };
		std::atomic<bool> _setf{ false };

	public:
		rdm_key_holder_t(std::weak_ptr<distributed_ctx_t> ctx_,
			const identity_descriptor_t & thisId_,
			const identity_descriptor_t & remoteId_,
			const char * tag);

		bool setval()const noexcept;
		bool set_rt0(const rt0_node_rdm_t &nodeId);
		const rtl_roadmap_obj_t& get_rdm_msg()const noexcept;
		const rtl_roadmap_obj_t& get_remote_rdm()const noexcept;
		const session_description_t& session()const noexcept;
		};

	struct __address_handler final : public i_x1_stream_address_handler {
		rt1_endpoint_t _ep;
		identity_descriptor_t _thisId;
		identity_descriptor_t _remoteId;

		/* ������� ����� */
		local_object_id_t _locid;
		rtl_roadmap_event_t _rdmEv;
		rdm_key_holder_t _rdmPair;

		std::string _name;
		std::atomic<remote_object_state_t> _state{ remote_object_state_t::null };

		__address_handler(std::weak_ptr<distributed_ctx_t> ctx_,
			const identity_descriptor_t & thisId_,
			const identity_descriptor_t & remoteId_,
			const rt1_endpoint_t & ep,
			const char * tag);

		const rtl_roadmap_link_t& msg_rdm_link()const noexcept;
		const rt1_endpoint_t& endpoint()const noexcept;
		bool set_rt0(const rt0_node_rdm_t &nodeId);
		bool rdm_registered()const noexcept;
		const rtl_roadmap_event_t& ev_rdm()const  noexcept;
		const rtl_roadmap_obj_t& msg_rdm()const  noexcept;
		const rtl_roadmap_obj_t& this_rdm()const noexcept final;
		const rtl_roadmap_obj_t& remote_rdm()const noexcept final;
		const identity_descriptor_t& remote_id()const noexcept final;
		const identity_descriptor_t& this_id()const noexcept final;
		const identity_descriptor_t& id()const noexcept final;
		const xpeer_desc_t& desc()const noexcept final;
		const rt0_node_rdm_t& node_id()const noexcept final;
		const local_object_id_t& local_id()const noexcept final;
		const local_command_identity_t& local_object_command_id()const noexcept final;
		const session_description_t& session()const noexcept final;
		rtl_table_t direction()const noexcept final;
		const std::string& address()const noexcept final;
		int port()const noexcept final;
		remote_object_endpoint_t remote_object_endpoint()const noexcept final;
		connection_key_t connection_key()const noexcept final;
		};

	rt1_base_xstream_t _xbase;
	std::shared_ptr<__address_handler> _addrh;
	std::shared_ptr<i_async_expression_handler_t> _requestRT0;
	std::shared_ptr<i_decode_tail_stream2peer_t_x1> _tail;

	std::atomic<bool> _closef{ false };
	std::atomic<bool> _rtReget{ false };

#ifdef CBL_MPF_OBJECTS_COUNTER_CHECKER_RT1_PEER
	crm::utility::__xobject_counter_logger_t<self_t, 1> _xDbgCounter;
#endif

	//static size_t stack_size(const std::weak_ptr<distributed_ctx_t> ctx_)noexcept;

	void ntf_hndl(std::unique_ptr<crm::dcf_exception_t> && obj)noexcept;
	void ntf_hndl(crm::dcf_exception_t && exc)noexcept;
	void ntf_hndl(const crm::dcf_exception_t & exc)noexcept;

	std::shared_ptr<distributed_ctx_t> ctx()noexcept;
	std::shared_ptr<const distributed_ctx_t> ctx()const noexcept;

	/*! �������� ���������������� ��������� � �����. ������������ /ref xpeer_out_rt1_t */
	void push_to_x(outbound_message_unit_t && buf) final;

	/*! ��������� ��������� ��������� ������� */
	void set_state(remote_object_state_t st)noexcept final;
	/*! ������ ��������� ��������� ������� */
	remote_object_state_t get_state()const noexcept final;

	/*! ���������� ��������� �� �������������� ������ 1 */
	void rt1_router_hndl(std::unique_ptr<i_command_from_rt02rt1_t> && response)noexcept final;
	/*! ���������� ������� �� �������������� ������ 1 */
	void rt1_router_hndl(rt0_x_message_unit_t && msg, invoke_context_trait ic)final;

	void addressed_exception(const _address_hndl_t & address,
		std::unique_ptr<dcf_exception_t> && ntf,
		invoke_context_trait ic)noexcept final;

	void close_from_router()noexcept final;
	void unroute_command_link()noexcept final;
	bool is_once()const noexcept final;

	void register_routing(__error_handler_point_f && rh);
	void unregister_routing(detail::unregister_message_track_reason_t r)noexcept;

	std::shared_ptr<i_decode_tail_stream2peer_t_x1> reset_tail()noexcept;
	bool set_tail(std::shared_ptr<i_decode_tail_stream2peer_t_x1> && tail_) noexcept final;
	std::shared_ptr<i_decode_tail_stream2peer_t_x1> tail()const noexcept;
	void close_tail()noexcept;

	void close_object_connection_command()noexcept;
	void close_objects()noexcept;
	void close(unregister_message_track_reason_t r)noexcept;
	bool rdm_registered()const noexcept;

	void unbind_reslink()noexcept;
	std::string make_trace_line()const noexcept;

public:
	void connecton_on_invoke(connection_handler_guard && ch,
		std::unique_ptr<i_command_result_t> && unreg,
		async_operation_result_t rc,
		std::unique_ptr<i_command_from_rt02rt1_t> && response,
		std::unique_ptr<i_command_result_t> && r,
		std::unique_ptr<dcf_exception_t> && n_);

protected:
	ox_rt1_stream_t(std::weak_ptr<i_rt1_main_router_t> rt,
		std::weak_ptr<distributed_ctx_t> ctx_,
		const identity_descriptor_t & thisId_,
		const identity_descriptor_t & remoteId_,
		const rt1_endpoint_t & ep,
		const char* tag = 0);

	void connect(connection_handler_guard && connectHndl);

public:
	void register_rt0(const rt0_node_rdm_t &nodeId, __error_handler_point_f && rh);

	std::shared_ptr<i_rt1_main_router_t> router()const noexcept final;
	std::shared_ptr<i_x1_stream_address_handler> address_point()const noexcept final;
	rtl_table_t direction()const noexcept final;
	const rt1_endpoint_t& endpoint()const noexcept final;
	bool closed()const  noexcept final;
	void close()noexcept final;
	const rtl_roadmap_event_t&  ev_rdm()const  noexcept final;
	const rtl_roadmap_obj_t&  msg_rdm()const  noexcept  final;
	const rtl_roadmap_link_t& msg_rdm_link()const  noexcept final;
	const rtl_roadmap_obj_t&  this_rdm()const  noexcept  final;
	const rtl_roadmap_obj_t& remote_rdm()const  noexcept  final;
	const identity_descriptor_t& this_id()const  noexcept  final;
	const identity_descriptor_t& remote_id()const  noexcept  final;
	const rt0_node_rdm_t& node_id()const noexcept  final;
	const local_object_id_t& local_id()const noexcept  final;
	const local_command_identity_t& local_object_command_id()const noexcept final;
	const session_description_t& session()const noexcept  final;
	source_object_key_t source_key()const noexcept  final;
	void unregister_routing()noexcept;
	bool is_integrity_provided()const noexcept;

	static void create( std::weak_ptr<i_rt1_main_router_t> rt,
		std::weak_ptr<distributed_ctx_t> ctx_,
		const identity_descriptor_t & thisId_,
		const identity_descriptor_t & remoteId_,
		const rt1_endpoint_t & ep,
		connection_handler_guard && connectHndl,
		const char* tag = 0 );

	~ox_rt1_stream_t();
	};
}

