#pragma once


#include <chrono>
#include <memory>


namespace crm ::detail{


/*! ������ ������ �� �����������
\tparam Xpeer ���������������� ������ �� ������
*/
template<typename Xpeer>
class xpeer_strong_link_t {
	friend typename Xpeer;

public:
	typedef typename Xpeer peer_t;
	typedef typename peer_t::link_t link_t;
	typedef typename peer_t::remote_object_endpoint_t remote_object_endpoint_t;
	typedef typename peer_t::xpeer_desc_t xpeer_desc_t;
	typedef typename peer_t::state_t state_t;
	typedef typename peer_t::input_value_t input_value_t;
	typedef typename peer_t::output_value_t output_value_t;

private:
	static const std::string null_string;
	std::shared_ptr<peer_t> _pSrc;

protected:
	std::shared_ptr<const peer_t> xpr()const noexcept{
		return _pSrc;
		}

	std::shared_ptr<peer_t> xpr()noexcept{
		return _pSrc;
		}

public:
	xpeer_strong_link_t() {}
	virtual ~xpeer_strong_link_t() {}

	void assign(xpeer_strong_link_t &&o) {
		_pSrc = std::move(o._pSrc);
		}

	void assign(const xpeer_strong_link_t &o) {
		_pSrc = o._pSrc;
		}

	void assign(std::weak_ptr<peer_t> &&pSrc) {
		_pSrc = pSrc.lock();
		}

	void assign(const std::weak_ptr<peer_t> &pSrc) {
		_pSrc = pSrc.lock();
		}

	void assign(std::shared_ptr<peer_t> &&pSrc) {
		_pSrc = std::move(pSrc);
		}

	void assign(const std::shared_ptr<peer_t> &pSrc) {
		_pSrc = pSrc;
		}

	xpeer_strong_link_t(const xpeer_strong_link_t &o) {
		assign(o);
		}

	xpeer_strong_link_t& operator=(const xpeer_strong_link_t &o) {
		assign(o);
		return (*this);
		}

	xpeer_strong_link_t(xpeer_strong_link_t  &&o) {
		assign(std::move(o));
		}

	xpeer_strong_link_t& operator=(xpeer_strong_link_t &&o) {
		assign(std::move(o));
		return (*this);
		}

	xpeer_strong_link_t(const std::weak_ptr<peer_t> &o) {
		assign(o);
		}

	xpeer_strong_link_t& operator=(const std::weak_ptr<peer_t> &o) {
		assign(o);
		return (*this);
		}

	xpeer_strong_link_t(std::weak_ptr<peer_t>  &&o) {
		assign(std::move(o));
		}

	xpeer_strong_link_t& operator=(std::weak_ptr<peer_t> &&o) {
		assign(std::move(o));
		return (*this);
		}

	xpeer_strong_link_t(const std::shared_ptr<peer_t> &o) {
		assign(o);
		}

	xpeer_strong_link_t& operator=(const std::shared_ptr<peer_t> &o) {
		assign(o);
		return (*this);
		}

	xpeer_strong_link_t(std::shared_ptr<peer_t>  &&o) {
		assign(std::move(o));
		}

	xpeer_strong_link_t& operator=(std::shared_ptr<peer_t> &&o) {
		assign(std::move(o));
		return (*this);
		}

	detail::peer_commands_result_t execute_command(peer_command_t cmd) {
		if(_pSrc) {
			return _pSrc->execute_command(cmd);
			}
		else {
			THROW_MPF_EXC_FWD(nullptr);
			}
		}


	rtl_table_t direction()const noexcept {
		if(_pSrc)
			return _pSrc->direction();
		else
			return rtl_table_t::undefined;
		}

	auto id()const  noexcept ->decltype(_pSrc->id()){
		using return_type = std::decay_t<decltype(_pSrc->id())>;

		if(_pSrc)
			return _pSrc->id();
		else
			return return_type::null;
		}

	auto local_id()const  noexcept ->decltype(_pSrc->local_id()){
		if(_pSrc)
			return  _pSrc->local_id();
		else
			return local_object_id_t::null;
		}

	bool valid()const noexcept {
		return !_pSrc.expired();
		}

	auto desc()const  noexcept ->decltype(_pSrc->desc()){
		if(_pSrc)
			return _pSrc->desc();
		else
			return xpeer_desc_t::null;
		}

	std::shared_ptr<distributed_ctx_t> ctx()const  noexcept {
		if(_pSrc)
			return _pSrc->ctx();
		else
			return nullptr;
		}

	template<typename O,
		typename std::enable_if_t<is_out_message_requirements_v<O>, int> = 0>
	void push(O && o) {
		CHECK_PTR(_pSrc)->push(std::forward<O>(o));
		}

	template<typename O,
		typename std::enable_if_t <(
			srlz::is_serializable_v<O> &&
			!is_out_message_requirements_v<O>
			), int> = 0>
	void push(O&& obj){
		push(ostrob_message_envelop_t(std::forward<O>(obj)));
		}

	void write(outbound_message_unit_t && bin) {
		CHECK_PTR(_pSrc)->write(std::move(bin));
		}

	auto address()const  noexcept ->decltype(_pSrc->address()){
		if(_pSrc)
			return _pSrc->address();
		else
			return "";
		}

	auto this_id()const  noexcept ->decltype(_pSrc->this_id()){
		using return_type = std::decay_t<decltype(_pSrc->this_id())>;

		if(_pSrc)
			return _pSrc->this_id();
		else
			return return_type::null;
		}

	auto remote_id()const   noexcept ->decltype(_pSrc->remote_id()){
		using return_type = std::decay_t<decltype(_pSrc->remote_id())>;

		if (_pSrc)
			return _pSrc->remote_id();
		else
			return  return_type::null;
		}

	int port()const  noexcept {
		if(_pSrc)
			return  _pSrc->port();
		else
			return -1;
		}

	bool renewable_connection()const  noexcept {
		if(_pSrc)
			return _pSrc->renewable_connection();
		else
			return false;
		}

	std::shared_ptr<i_peer_statevalue_t> state_value() noexcept{
		if(_pSrc)
			return _pSrc->state_value();
		else
			return nullptr;
		}

	std::shared_ptr<const i_peer_statevalue_t> state_value()const noexcept{
		if(_pSrc)
			return _pSrc->state_value();
		else
			return nullptr;
		}

	auto remote_object_endpoint()const  noexcept ->decltype(_pSrc->remote_object_endpoint()){
		if(_pSrc)
			return _pSrc->remote_object_endpoint();
		else
			return remote_object_endpoint_t::null;
		}

	void set_state_value(std::shared_ptr<i_peer_statevalue_t> && stval) {
		CHECK_PTR(_pSrc)->set_state_value(std::move(stval));
		}

	void set_state(remote_object_state_t st) {
		CHECK_PTR(_pSrc)->set_state(st);
		}

	template<typename THandl>
	subscribe_invoker_result subscribe_hndl_id(async_space_t scx, 
		const _address_hndl_t &hndl, 
		subscribe_options opt, 
		typename THandl && f) {

		return CHECK_PTR(_pSrc)->subscribe_hndl_id(scx, hndl, opt, std::forward<THandl>(f));
		}

	void unsubscribe_hndl_id(const _address_hndl_t &hndl)noexcept {
		if(_pSrc)
			_pSrc->unsubscribe_hndl_id(hndl);
		}

	template<typename THandler,
		typename std::enable_if_t<std::is_constructible_v<__event_handler_f, std::decay_t<THandler>>, int> = 0>
	[[nodiscard]]
	subscribe_result subscribe_event( async_space_t scx, const _address_event_t &hndl, bool once, THandler && f) {
		return CHECK_PTR(_pSrc)->subscribe_event(scx, hndl, once, std::forward<THandler>(f));
		}

	void subscribe_event(events_list && el) {
		CHECK_PTR(_pSrc)->subscribe_event(std::move(el));
		}

	void unsubscribe_event(const _address_event_t &hndl)noexcept {
		if(_pSrc)
			_pSrc->unsubscribe_event(hndl);
		}

	int tag()const  noexcept {
		if(_pSrc)
			return _pSrc->tag();
		else
			return -1;
		}

	void set_tag(int value) {
		CHECK_PTR(_pSrc)->set_tag(value);
		}

	bool closed()const noexcept {
		if(_pSrc)
			return _pSrc->closed();
		else
			return true;
		}

	state_t state() const noexcept{
		if(_pSrc)
			return _pSrc->state();
		else
			return state_t::closed;
		}

	bool is_opened() const noexcept {
		if(_pSrc)
			return _pSrc->is_opened();
		else
			return false;
		}

	rtl_level_t level()const  noexcept {
		if(_pSrc)
			return _pSrc->level();
		else
			THROW_MPF_EXC_FWD(nullptr);
		}

	typename peer_t::link_t link()  noexcept {
		if(_pSrc)
			return _pSrc->link();
		else
			return typename peer_t::link_t{};
		}

	auto connection_key()const  noexcept ->decltype(_pSrc->connection_key()){
		if(_pSrc)
			return _pSrc->connection_key();
		else
			return connection_key_t{};
		}

	std::shared_ptr<const i_peer_remote_properties_t> remote_properties()const noexcept {
		if(_pSrc)
			return _pSrc->remote_properties();
		else
			return std::shared_ptr<const i_peer_remote_properties_t>();
		}

	void invoke_after_ready(async_space_t scx, std::unique_ptr<typename peer_t::subscribe_event_connection_t> && hndl) {
		if(_pSrc)
			_pSrc->invoke_after_ready(scx, std::move(hndl));
		else {
			THROW_MPF_EXC_FWD(nullptr);
			}
		}

	void close()noexcept{
		_pSrc.reset();
		}
	};


template<typename Xpeer>
const std::string xpeer_strong_link_t<Xpeer>::null_string{};

template<typename Xpeer>
class xpeer_strong_link_l1_t : public xpeer_strong_link_t<Xpeer>{
public:
	xpeer_strong_link_l1_t(){}

	xpeer_strong_link_l1_t( const std::weak_ptr<peer_t> &o ){
		assign( o );
		}

	xpeer_strong_link_l1_t& operator=( const std::weak_ptr<peer_t> &o ){
		assign( o );
		return (*this);
		}

	xpeer_strong_link_l1_t( std::weak_ptr<peer_t>  &&o ){
		assign( std::move( o ) );
		}

	xpeer_strong_link_l1_t& operator=( std::weak_ptr<peer_t> &&o ){
		assign( std::move( o ) );
		return (*this);
		}

	xpeer_strong_link_l1_t( const std::shared_ptr<peer_t> &o ){
		assign( o );
		}

	xpeer_strong_link_l1_t& operator=( const std::shared_ptr<peer_t> &o ){
		assign( o );
		return (*this);
		}

	xpeer_strong_link_l1_t( std::shared_ptr<peer_t>  &&o ){
		assign( std::move( o ) );
		}

	xpeer_strong_link_l1_t& operator=( std::shared_ptr<peer_t> &&o ){
		assign( std::move( o ) );
		return (*this);
		}

	auto this_rdm()const  noexcept ->decltype(xpr()->this_rdm()){
		auto xpr_( xpr() );
		if( xpr_ )
			return  xpr_->this_rdm();
		else
			return rtl_roadmap_obj_t::null;
		}

	auto remote_rdm()const  noexcept ->decltype(xpr()->remote_rdm()){
		auto xpr_( xpr() );
		if( xpr_ )
			return  xpr_->remote_rdm();
		else
			return rtl_roadmap_obj_t::null;
		}
	};
}
