#pragma once


#include "../internal_invariants.h"


namespace crm::detail{
class ix_rt1_stream_t;
class ox_rt1_stream_t;


size_t get_stat_counter_incoming_connections_initialize_stage()noexcept;
size_t get_stat_counter_outbound_connections_counter( remote_object_state_t st )noexcept;
}

