#pragma once


#include <memory>
#include "../internal_invariants.h"
#include "./outbound_xconnector.h"
#include "../channel_terms/events_terms.h"
#include "../channel_terms/events_tbl.h"


namespace crm::detail {


template<typename TPeer>
class xpeer_out_connection_t;


template<typename TPeer>
class xpeer_out_connection_t
	: public std::enable_shared_from_this<xpeer_out_connection_t<TPeer>> {

public:
	/*! ��� ���� */
	typedef typename TPeer peer_t;
	/*! ������ ������� */
	typedef typename peer_t::state_t state_t;
	/*! ��� ������ */
	typedef typename xpeer_out_connection_t<peer_t> self_t;
	/*! ��� �������� ��� ������ � ����� ���� */
	typedef typename xpeer_out_connector_t<peer_t> connection_initializer_t;
	/*! ��� ����� �������� ��������� */
	typedef typename peer_t::input_stack_t input_stack_t;
	/*! ��� ���������� ������������� ������� � �������� ���� */
	typedef typename peer_t::xpeer_desc_t xpeer_desc_t;
	/*! ������ �� ������ */
	typedef typename peer_t::link_t peer_link_t;
	typedef typename peer_t::remote_object_endpoint_t remote_object_endpoint_t;

	/*! ���������� �������� ���� */
	typedef std::function<void(const xpeer_desc_t & id,
		closing_reason_t,
		std::shared_ptr<i_peer_statevalue_t> &&)> close_event_hndl_t;

	using event_open_connection_t = event_open_connection_t<peer_link_t>;
	using event_close_connection_t = event_close_connection_t<peer_link_t>;
	using subscribe_event_connection_t = subscribe_event_connection_t<peer_link_t>;

private:
	std::weak_ptr<distributed_ctx_t> _ctx;
	/*! ������� ���������� �� ������ � ������ ����������� */
	std::unique_ptr<connection_initializer_t> _peerFunctor;
	/*! ������� �������� ����������������� ������ */
	local_object_create_connection_t _identityCtr;
	/*! ������� �������� ����������������� ������ */
	remote_object_check_identity_t _identityChecker;
	/*! ������� ������ */
	std::atomic<bool> _cancel{false};
	/*! ���������� �������� ���� */
	close_event_hndl_t _closeEventHndl;
	/*! ������ �� ���� */
	std::atomic<std::shared_ptr<peer_link_t>> _sLnk;
	/*! ��� �������� */
	events_multitable_t::handler_t _evCache;
	async_space_t _scx;

	std::atomic<remote_object_state_t> _selfState{remote_object_state_t::oxcn_maked};

	const bool _keepAlive{false};

#ifdef CBL_MPF_OBJECTS_COUNTER_CHECKER_OUTBOUND_STLINK
	crm::utility::__xobject_counter_logger_t<self_t, 1> _xDbgCounter;
#endif

	std::shared_ptr<distributed_ctx_t> ctx()const noexcept {
		return _ctx.lock();
		}

	bool canceled()const noexcept {
		return _cancel.load(std::memory_order::memory_order_acquire);
		}

	std::string this_object_sid()const noexcept {
		return std::to_string((uintptr_t)this);
		}

	std::string connection_string()const noexcept {
		if (_peerFunctor) {
			if (const auto ep = _peerFunctor->target_endpoint()) {

				std::string log(this_object_sid() + ":");
				log += "address=" + ep->address();
				log += ":port=" + crm::detail::cvt_to_str(ep->port());
				log += ":id=" + this_id().to_str();
				return log;
				}
			else {
				return {};
				}
			}
		else {
			return {};
			}
		}

	void event_close_link(bool destroy,
		const xpeer_desc_t & rdmlocal,
		closing_reason_t rs,
		std::shared_ptr<i_peer_statevalue_t> && stv)noexcept {

		if (_closeEventHndl) {
			if (auto c = ctx()) {
				if (destroy) {

					c->launch_async(_scx, __FILE_LINE__,
						[rdmlocal, rs](close_event_hndl_t && hndl, std::shared_ptr<i_peer_statevalue_t> && stv) {

						hndl(rdmlocal, rs, std::move(stv));

						}, std::move(_closeEventHndl), std::move(stv));
					}
				else {

					c->launch_async(_scx, __FILE_LINE__,
						[rdmlocal, rs](close_event_hndl_t && hndl, std::shared_ptr<i_peer_statevalue_t> && stv) {

						hndl(rdmlocal, rs, std::move(stv));

						}, _closeEventHndl, std::move(stv));

					CBL_VERIFY(_closeEventHndl);
					}
				}
			}
		}

	void __destroy_link_body()noexcept {
		if (auto oldPLnk = _sLnk.exchange(std::shared_ptr<peer_link_t>())) {
			CHECK_NO_EXCEPTION(event_close_link(true, oldPLnk->desc(), closing_reason_t::destroy, nullptr));
			CHECK_NO_EXCEPTION(oldPLnk->close());
			}
		else {
			CHECK_NO_EXCEPTION(event_close_link(true, xpeer_desc_t{}, closing_reason_t::destroy, nullptr));
			}
		}

	void __destroy_link(bool dctr_)noexcept {
		if (dctr_) {
			__destroy_link_body();
			}
		else {
			if (auto c = ctx()) {
				c->outbound_connection_lock_entry((uintptr_t)this, [this] {
					__destroy_link_body();
					});
				}
			}
		}

public:
	peer_link_t link() noexcept {
		if (auto pLnk = _sLnk.load(std::memory_order::memory_order_acquire))
			return (*pLnk);
		else
			return peer_link_t{};
		}

	peer_link_t link()const noexcept {
		if (auto pLnk = _sLnk.load( std::memory_order::memory_order_acquire))
			return (*pLnk);
		else
			return peer_link_t{};
		}

	local_object_id_t local_id()const noexcept {
		return link().local_id();
		}

	identity_descriptor_t this_id()const  noexcept {
		if (_peerFunctor) {
			return _peerFunctor->this_id();
			}
		else {
			return identity_descriptor_t::null;
			}
		}

	identity_descriptor_t id()const  noexcept {
		return remote_id();
		}

	identity_descriptor_t remote_id()const  noexcept {
		return link().remote_id();
		}

	xpeer_desc_t desc()const noexcept {
		return link().desc();
		}

	rtl_level_t level()const noexcept {
		return link().level();
		}

	state_t state()const  noexcept {
		auto l = link();
		if (l.valid()) {
			return l.state();
			}
		else {
			return _selfState.load(std::memory_order::memory_order_acquire);
			}
		}

	std::string address()const  noexcept {
		return link().address();
		}

	int port()const  noexcept {
		return link().port();
		}

	rtl_table_t direction()const noexcept {
		return link().direction();
		}

	std::shared_ptr<i_peer_statevalue_t> state_value() noexcept {
		return link().state_value();
		}

	std::shared_ptr<const i_peer_statevalue_t> state_value()const noexcept {
		return link().state_value();
		}

	void set_state_value(std::shared_ptr<i_peer_statevalue_t> && stv) {
		link().set_state_value(std::move(stv));
		}

	bool renewable_connection()const noexcept {
		return true;
		}

	remote_object_endpoint_t remote_object_endpoint()const noexcept {
		if (_peerFunctor)
			return _peerFunctor->remote_object_endpoint();
		else
			return remote_object_endpoint_t{};
		}

private:
#ifdef CBL_MPF_TRACELEVEL_EVENT_CONNECTION
	std::string make_trace_line()const noexcept {
		return connection_string();
		}
#endif

	void _on_connect(peer_link_t lnk) {
		if (!canceled()) {

			CBL_VERIFY(lnk.state() >= remote_object_state_t::impersonated);
			CBL_MPF_TRACELEVEL_EVENT_CONNECTION_TRACE("outbound connector:on_connect:" + make_trace_line());

			_selfState.store(lnk.state(), std::memory_order::memory_order_release);

			if (auto c = ctx()) {
				c->outbound_connection_lock_entry((uintptr_t)this, [&, this] {
					if (!canceled()) {
						if (auto oldPLnk = _sLnk.exchange( std::make_shared<peer_link_t>(lnk))) {
							CHECK_NO_EXCEPTION(oldPLnk->close());
							}

						post_scope_action_t pnDctr([lnk]()mutable {
							CHECK_NO_EXCEPTION(lnk.close());
							});

						lnk.subscribe_event(_evCache.release());
						if (auto c = ctx()) {

							auto postFunctor(CHECK_PTR(_peerFunctor)->get_event_post_connection());
							if (postFunctor) {
								c->launch_async(_scx, __FILE_LINE__,
									[](peer_link_t && lnk_, decltype(postFunctor) && postFunctor_, decltype(pnDctr) && lnkErr) {

									if (lnk_.is_opened()) {
										CBL_VERIFY(lnk_.state() >= remote_object_state_t::impersonated);

										postFunctor_(nullptr, std::move(lnk_));
										CHECK_NO_EXCEPTION(lnkErr.reset());
										}
									else {
										postFunctor_(CREATE_MPF_PTR_EXC_FWD(nullptr), peer_link_t{});
										}

									}, std::move(lnk), std::move(postFunctor), std::move(pnDctr));
								}
							else {
								FATAL_ERROR_FWD(nullptr);
								}
							}
						}
					});
				}
			}
		}

	void __destroy(bool dctr_)noexcept {
		_cancel.store(true);

		CBL_MPF_TRACELEVEL_EVENT_CONNECTION_TRACE((dctr_ ? "outbound connector:close by unused:" : "outbound connector:close by forced:")
			+ make_trace_line());

		CHECK_NO_EXCEPTION(__destroy_link(dctr_));

		/* ������� ����� ������������ */
		_evCache.close();
		}

	class __keep_alive_tail_t {
		std::weak_ptr<self_t> _wptr;
		std::atomic<bool> _nextf{false};

#ifdef CBL_MPF_TRACELEVEL_EVENT_CONNECTION
		std::string _tag;
#endif

		__keep_alive_tail_t(const __keep_alive_tail_t&) = delete;
		__keep_alive_tail_t& operator=(const __keep_alive_tail_t &) = delete;

		const std::string& trace_line()const noexcept {
#ifdef CBL_MPF_TRACELEVEL_EVENT_CONNECTION
			return _tag;
#else

			static const std::string _null;
			return _null;
#endif
			}

		void __next_start()noexcept {
			bool nextf{ false };
			if (_nextf.compare_exchange_strong(nextf, true)) {
				if (auto p = _wptr.lock()) {
					if (!p->canceled()) {

						CBL_MPF_TRACELEVEL_EVENT_CONNECTION_TRACE("outbound connector:recycler:next:" + trace_line());

						/* ��������� ������ ��������� ����������� ����� �������� ����� �������� �������� �������
						-----------------------------------------------------------------------------------------*/
						if (auto ctx_ = p->ctx()) {

							CBL_MPF_TRACELEVEL_EVENT_CONNECTION_TRACE("outbound connector:recycler:"
								+ p->connection_string()
								+ ":push reconnect, ms=" + std::to_string(ctx_->policies().timeout_reconnect_attempt().count()));

							bool reconnectf{ true };
							if (p->_peerFunctor) {
								std::ostringstream message;
								message << "Outbound connection timeout{ ";
								message << p->connection_string();
								message << " }. Reconnect after delay, ms=";
								message << ctx_->policies().timeout_reconnect_attempt().count();

								reconnectf = p->_peerFunctor->check_notify(CREATE_OUTBOUND_PTR_EXC_FWD(message.str()));
								}

							if (!reconnectf) {
								if (auto postFunctor = CHECK_PTR(p->_peerFunctor)->get_event_post_connection()) {
									CHECK_NO_EXCEPTION(ctx_->launch_async(async_space_t::sys, __FILE_LINE__,
										std::move(postFunctor),
										CREATE_REFUSE_OPERATION_PTR_EXC_FWD(p->connection_string()),
										peer_link_t{}));
									}
								else {
									reconnectf = true;
									}
								}

							if (reconnectf) {
								if (ctx_->policies().timeout_reconnect_attempt().count() > 0) {
									CHECK_NO_EXCEPTION(ctx_->register_timer_deffered_action_sys({ "mk-outbound-peer" }, [p](crm::async_operation_result_t) {
										p->launch_instance(std::make_unique<__keep_alive_tail_t>(p));
										}, ctx_->policies().timeout_reconnect_attempt()));
									}
								else {
									CHECK_NO_EXCEPTION(ctx_->launch_async(async_space_t::sys, __FILE_LINE__, [p] {
										p->launch_instance(std::make_unique<__keep_alive_tail_t>(p));
										}));
									}
								}
							}
						}
					}
				else {
					CBL_MPF_TRACELEVEL_EVENT_CONNECTION_TRACE("outbound connector:recycler:canceled:" + trace_line());
					}
				}
			}

	public:
		void next()noexcept {
			CHECK_NO_EXCEPTION(__next_start());
			}

		~__keep_alive_tail_t() {
			CHECK_NO_EXCEPTION(next());
			}

		__keep_alive_tail_t(const std::shared_ptr<self_t> & ptr_)noexcept
			: _wptr(ptr_)
#ifdef CBL_MPF_TRACELEVEL_EVENT_CONNECTION
			, _tag(std::to_string((uintptr_t)ptr_.get()))
#endif
			{
			CBL_VERIFY(ptr_);
			CBL_MPF_TRACELEVEL_EVENT_CONNECTION_TRACE("outbound connector:recycler:created:" + ptr_->make_trace_line());
			}
			};

	std::function<void(const xpeer_desc_t & lcrdm,
		closing_reason_t reason,
		std::shared_ptr<i_peer_statevalue_t> && stv)>
		make_close_handler(std::shared_ptr<__keep_alive_tail_t> pTail) {
		if (_keepAlive) {
			return[sthis = shared_from_this(), pTail](const xpeer_desc_t & lcrdm,
				closing_reason_t reason,
				std::shared_ptr<i_peer_statevalue_t> && stv)mutable {

				post_scope_action_t closeTail([&pTail] {
					if (pTail) {
						CHECK_NO_EXCEPTION(pTail->next());
						}
					});

				CHECK_NO_EXCEPTION(sthis->event_close_link(false, lcrdm, reason, std::move(stv)));
				};
			}
		else {
			return[wthis = weak_from_this(), pTail](const xpeer_desc_t & lcrdm,
				closing_reason_t reason,
				std::shared_ptr<i_peer_statevalue_t> && stv)mutable {

				post_scope_action_t closeTail([&pTail] {
					if (pTail) {
						CHECK_NO_EXCEPTION(pTail->next());
						}
					});

				if (auto sthis = wthis.lock()) {
					CHECK_NO_EXCEPTION(sthis->event_close_link(false, lcrdm, reason, std::move(stv)));
					}
				};
			}
		}

	void launch_instance(std::shared_ptr<__keep_alive_tail_t> && pTail_) {
		if (!canceled()) {
			if (auto ctx_ = ctx()) {
				_selfState.store(remote_object_state_t::oxcn_launch, std::memory_order::memory_order_release);

				CBL_MPF_TRACELEVEL_EVENT_CONNECTION_TRACE("outbound connector:launch_instance:begin:" + make_trace_line());

				std::weak_ptr<__keep_alive_tail_t> wTail = pTail_;

				/* ������� ��������� ������� �������� ������� */
				auto eventClosing = make_close_handler(std::move(pTail_));

				/* ������� ��������� ����������� ��������� ����������� */
				auto resultHndl = [wthis = weak_from_this(), wTail](peer_link_t && obj,
					crm::async_operation_result_t result,
					std::unique_ptr<dcf_exception_t> && exc,
					detail::reconnect_setup_t rc)mutable {

					post_scope_action_t objScp([&obj, &wTail] {
						CHECK_NO_EXCEPTION(obj.close());

						if (auto pTail = wTail.lock()) {
							CHECK_NO_EXCEPTION(pTail->next());
							}
						});

					auto sptr(wthis.lock());
					if (sptr && detail::reconnect_setup_t::yes == rc) {

						if (crm::async_operation_result_t::st_ready == result && obj.valid() && !exc) {
							CBL_MPF_TRACELEVEL_EVENT_CONNECTION_TRACE("outbound connector:launch_instance:end(ok):" + sptr->make_trace_line());

							sptr->_on_connect(std::move(obj));
							objScp.reset();
							}
						else {
							CBL_MPF_TRACELEVEL_EVENT_CONNECTION_TRACE("outbound connector:launch_instance:end(error):" + sptr->make_trace_line());
							}
						}
					};

				/*! ������� �������� ����������������� ������ */
				auto identityCtr = [wthis = weak_from_this()]() {
					auto sptr(wthis.lock());
					if (sptr)
						return sptr->_identityCtr();
					else
						THROW_MPF_EXC_FWD(nullptr);
					};

				/*! ������� ��������� ����������������� ������ ��������� ���� */
				auto identityChecker = [wthis = weak_from_this()](const identity_value_t & obj, std::unique_ptr<i_peer_remote_properties_t> & remoteProp)->bool {
					auto sptr(wthis.lock());
					if (sptr)
						return sptr->_identityChecker(obj, remoteProp);
					else
						return false;
					};

				/* ��������� ������ ����������� */
				CHECK_PTR(_peerFunctor)->connect(ctx(),
					std::move(eventClosing),
					std::move(resultHndl),
					std::move(identityCtr),
					std::move(identityChecker));
				}
			}
		}

public:
	virtual ~xpeer_out_connection_t() {
		close(true);
		}

	bool is_opened()const noexcept {
		if (auto p = _sLnk.load( std::memory_order::memory_order_acquire)) {
			return p->is_opened();
			}
		else {
			return false;
			}
		}

	void close() {
		close(false);
		}

	self_t(std::weak_ptr<distributed_ctx_t> mpfCtx,
		std::unique_ptr<connection_initializer_t> && baseFunctor,
		local_object_create_connection_t && identityCtr,
		remote_object_check_identity_t && identityChecker,
		close_event_hndl_t && closeEventHndl,
		async_space_t scx,
		bool keepAlive_ = false)
		: _ctx(mpfCtx)
		, _peerFunctor(std::move(baseFunctor))
		, _identityCtr(std::move(identityCtr))
		, _identityChecker(std::move(identityChecker))
		, _closeEventHndl(std::move(closeEventHndl))
		, _evCache(CHECK_PTR(mpfCtx)->events_make_line())
		, _scx(scx)
		, _keepAlive(keepAlive_) {

		CBL_MPF_TRACELEVEL_EVENT_CONNECTION_TRACE("outbound connector:created:" + make_trace_line());
		}

private:
	/*! ������ �� ������� �������� �������� */
	std::shared_ptr<input_stack_t> get_input_queue()noexcept {
		return link().get_input_queue();
		}

	void close(bool dctr_)noexcept {
		__destroy(dctr_);
		}

public:
	struct handler_t {
		friend class self_t;

	private:
		std::shared_ptr<self_t> _sp;

	public:
		handler_t() noexcept {}

		handler_t(std::shared_ptr<self_t> && sp)noexcept
			: _sp(std::move(sp)) {
			}

		std::shared_ptr<distributed_ctx_t> ctx()const noexcept {
			if (_sp)
				return _sp->ctx();
			else
				return nullptr;
			}

		peer_commands_result_t execute_command(peer_command_t cmd) {
			return CHECK_PTR(_sp)->execute_command(cmd);
			}

		std::string trace_line()const noexcept {
			if (_sp)
				return _sp->trace_line();
			else
				return {};
			}

		rtl_level_t level()const noexcept {
			if (_sp)
				return _sp->level();
			else
				return rtl_level_t::undefined;
			}

		identity_descriptor_t id()const  noexcept {
			if (_sp)
				return _sp->id();
			else
				return identity_descriptor_t::null;
			}

		peer_link_t link()noexcept {
			if (_sp)
				return _sp->link();
			else
				return peer_link_t{};
			}

		peer_link_t link()const noexcept {
			if (_sp)
				return _sp->link();
			else
				return peer_link_t{};
			}

		rtl_table_t direction()const noexcept {
			if (_sp)
				return _sp->direction();
			else
				return rtl_table_t::undefined;
			}

		local_object_id_t local_id()const noexcept {
			if (_sp)
				return _sp->local_id();
			else
				return local_object_id_t::null;
			}

		identity_descriptor_t this_id()const noexcept {
			if (_sp)
				return _sp->this_id();
			else
				return identity_descriptor_t::null;
			}

		identity_descriptor_t remote_id()const noexcept {
			if (_sp)
				return  _sp->remote_id();
			else
				return identity_descriptor_t::null;
			}

		xpeer_desc_t desc()const noexcept {
			if (_sp)
				return _sp->desc();
			else
				return xpeer_desc_t::null;
			}

		state_t state()const noexcept {
			if (_sp)
				return _sp->state();
			else
				return state_t::closed;
			}

		std::string address()const noexcept {
			if (_sp)
				return  _sp->address();
			else
				return "";
			}

		int port()const noexcept {
			if (_sp)
				return _sp->port();
			else
				return -1;
			}

		std::shared_ptr<i_peer_statevalue_t> state_value() noexcept {
			if (_sp)
				return _sp->state_value();
			else {
				return nullptr;
				}
			}

		std::shared_ptr<const i_peer_statevalue_t> state_value()const noexcept {
			if (_sp)
				return _sp->state_value();
			else {
				return nullptr;
				}
			}

		void set_state_value(std::shared_ptr<i_peer_statevalue_t> && stv) {
			CHECK_PTR(_sp)->set_state_value(std::move(stv));
			}

		bool renewable_connection()const noexcept {
			if (_sp)
				return _sp->renewable_connection();
			else
				return false;
			}

		remote_object_endpoint_t remote_object_endpoint()const noexcept {
			if (_sp)
				return _sp->remote_object_endpoint();
			else
				return remote_object_endpoint_t{};
			}

		bool closed()const noexcept {
			return nullptr == _sp;
			}

		bool is_opened()const noexcept {
			if (_sp)
				return _sp->is_opened();
			else
				return false;
			}

		/*! ��������� ������ � ������� ��������� ��������� */
		template<typename O,
			typename std::enable_if_t < is_out_message_requirements_v<O>, int> = 0>
		void push(O && o) {
			CHECK_PTR(_sp)->push(std::forward<O>(o));
			}

		void write(outbound_message_unit_t && blob) {
			CHECK_PTR(_sp)->write(std::move(blob));
			}

		/*! ������� ������ �� ������� �������� ��������� */
		template<typename Rep, typename Period>
		bool pop(typename input_stack_t::value_type & obj,
			const std::chrono::duration<Rep, Period> & wait) {

			if (_sp)
				return  _sp->pop(obj, wait);
			else
				return false;
			}

		/*! �������� �� ��������� ��������� ��� ���������� ��������������.
		\param id ������� �������������
		\param once ���� ������� true, ����������� �������� ���� ���, ����� �� ��� ���,
		���� �� ����� ��������� ������ ����������� � ������� ������ /ref unsubscribe_hndl_id.
		*/
		template<typename TSubscribeHndl>
		[[nodiscard]]
		subscribe_invoker_result subscribe_hndl_id(async_space_t scx, const _address_hndl_t & id, subscribe_options opt,
			typename TSubscribeHndl && handler) {

			return CHECK_PTR(_sp)->subscribe_hndl_id(scx, id, opt, std::forward<TSubscribeHndl>(handler));
			}

		/*! ����� �� ��������� ����������� ��� ���������� ��������������
		\param id ������������� ����������
		*/
		void unsubscribe_hndl_id(const _address_hndl_t & id)noexcept {
			if (_sp)
				_sp->unsubscribe_hndl_id(id);
			}

		/* �������� ��������� ������� */
		std::shared_ptr<const i_peer_remote_properties_t> remote_properties()const noexcept {
			if (_sp)
				return _sp->remote_properties();
			else
				return nullptr;
			}

		void subscribe_event(events_list && el) {
			CHECK_PTR(_sp)->subscribe_event(std::move(el));
			}

		template<typename THandler,
			typename std::enable_if_t<std::is_constructible_v<__event_handler_f, std::decay_t<THandler>>, int> = 0>
			[[nodiscard]]
		subscribe_result subscribe_event(async_space_t scx, const _address_event_t & id, bool once,
			THandler && handler) {

			return CHECK_PTR(_sp)->subscribe_event(scx, id, once, std::forward<THandler>(handler));
			}

		void unsubscribe_event(const _address_event_t & id)noexcept {
			if (_sp)
				_sp->unsubscribe_event(id);
			}

		void invoke_after_ready(async_space_t scx, std::unique_ptr<subscribe_event_connection_t> && hndl) {
			if (_sp)
				_sp->invoke_after_ready(scx, std::move(hndl));
			else {
				THROW_MPF_EXC_FWD(nullptr);
				}
			}
		};

	static handler_t launch(std::weak_ptr<distributed_ctx_t> mpfCtx,
		std::unique_ptr<connection_initializer_t> && baseFunctor,
		local_object_create_connection_t && identityCtr,
		remote_object_check_identity_t && identityChecker,
		close_event_hndl_t && closeEventHndl,
		async_space_t scx,
		bool keepAlive_ = false) {

		auto pObj = std::make_shared<self_t>(mpfCtx,
			std::move(baseFunctor),
			std::move(identityCtr),
			std::move(identityChecker),
			std::move(closeEventHndl),
			scx,
			keepAlive_);

		pObj->launch_instance(std::make_unique<__keep_alive_tail_t>(pObj));
		return std::move(pObj);
		}

	/*! ��������� ������ � ������� ��������� ��������� */
	template<typename O,
		typename std::enable_if_t < is_out_message_requirements_v<O>, int> = 0>
	void push(O && o) {
		link().push(std::forward<O>(o));
		}

	/*! ��������� ������ � ������� ��������� ��������� */
	template<typename O,
		typename std::enable_if_t <(
			srlz::is_serializable_v<O> &&
			!is_out_message_requirements_v<O>
			), int> = 0>
	void push(O&& obj){
		push(ostrob_message_envelop_t(std::forward<O>(obj)));
		}

	void write(outbound_message_unit_t && blob) {
		link().write(std::move(blob));
		}

	/*! ������� ������ �� ������� �������� ��������� */
	template<typename Rep, typename Period>
	bool pop(typename input_stack_t::value_type & obj,
		const std::chrono::duration<Rep, Period> & wait) {

		return link().pop(obj, wait);
		}

	/*! �������� �� ��������� ��������� ��� ���������� ��������������.
	\param id ������� �������������
	\param once ���� ������� true, ����������� �������� ���� ���, ����� �� ��� ���,
	���� �� ����� ��������� ������ ����������� � ������� ������ /ref unsubscribe_hndl_id.
	*/
	template<typename TSubscribeHndl>
	[[nodiscard]]
	subscribe_invoker_result subscribe_hndl_id(async_space_t scx, const _address_hndl_t & id, subscribe_options opt,
		typename TSubscribeHndl && handler) {

		if (auto c = ctx()) {
			detail::subscribe_invoker_result r;
			c->outbound_connection_lock_entry((uintptr_t)this, [&, this] {
				r = link().subscribe_hndl_id(scx, id, opt, std::forward<TSubscribeHndl>(handler));
				});

			return r;
			}
		else {
			return detail::subscribe_invoker_result::enum_type::exception;
			}
		}

	/*! ����� �� ��������� ����������� ��� ���������� ��������������
	\param id ������������� ����������
	*/
	void unsubscribe_hndl_id(const _address_hndl_t & id)noexcept {
		if (auto c = ctx()) {
			c->outbound_connection_lock_entry((uintptr_t)this, [&, this] {
				CHECK_NO_EXCEPTION(link().unsubscribe_hndl_id(id));
				});
			}
		}

	/* �������� ��������� ������� */
	std::shared_ptr<const i_peer_remote_properties_t> remote_properties()const noexcept {
		return link().remote_properties();
		}

	template<typename THandler,
		typename std::enable_if_t<std::is_constructible_v<__event_handler_f, std::decay_t<THandler>>, int> = 0>
	[[nodiscard]]
	subscribe_result subscribe_event(async_space_t spx,
		const _address_event_t & id,
		bool once,
		THandler && handler) {

		if (auto c = ctx()) {
			if (!canceled()) {
				subscribe_result result = subscribe_result::enum_type::exception;

				c->outbound_connection_lock_entry((uintptr_t)this, [&, this] {

					auto xp = link();
					if (xp.is_opened() && xp.valid()) {
						result = xp.subscribe_event(spx, id, once, std::forward<THandler>(handler));
						}
					else {
						result = _evCache.subscribe(spx, id, once, std::forward<THandler>(handler), nullptr);
						}
					});

				return result;
				}
			else {
				return subscribe_result::enum_type::exception;
				}
			}
		else {
			return subscribe_result::enum_type::exception;
			}
		}

	void subscribe_event(events_list && el) {
		if (!canceled()) {
			if (auto c = ctx()) {
				c->outbound_connection_lock_entry((uintptr_t)this, [&, this] {

					auto xp = link();
					if (xp.is_opened() && xp.valid()) {
						xp.subscribe_event(std::move(el));
						}
					else {
						_evCache.add(std::move(el));
						}
					});
				}
			else {
				THROW_MPF_EXC_FWD(nullptr);
				}
			}
		else {
			THROW_EXC_FWD(nullptr);
			}
		}

	void unsubscribe_event(const channel_identity_t & chid)noexcept {
		if (auto c = ctx()) {
			c->outbound_connection_lock_entry((uintptr_t)this, [&, this] {

				CHECK_NO_EXCEPTION(_evCache.unsubscribe(chid));
				CHECK_NO_EXCEPTION(link().unsubscribe_event(chid));
				});
			}
		}

	void unsubscribe_event(const _address_event_t & id)noexcept {
		if (auto c = ctx()) {
			c->outbound_connection_lock_entry((uintptr_t)this, [&, this] {

				CHECK_NO_EXCEPTION(_evCache.unsubscribe(id));
				CHECK_NO_EXCEPTION(link().unsubscribe_event(id));
				});
			}
		}

	peer_commands_result_t execute_command(peer_command_t cmd) {
		return link().execute_command(cmd);
		}

	void invoke_after_ready(async_space_t spx,
		std::unique_ptr<subscribe_event_connection_t> && pHndl_) {
		if (pHndl_) {
			auto subscriberId = pHndl_->subscriber_id();
			if (!subscribe_event(spx, subscriberId, true,
				[h = std::move(pHndl_)](std::unique_ptr<i_event_t>&& args, subscribe_invoke as)noexcept {

				auto pConnect = event_cast<event_open_connection_t>(std::move(args));
				if (pConnect && pConnect.value().peer.is_opened()) {
					if (h)
						(*h)(nullptr, std::move(pConnect).value().peer, subscribe_invoke::without_subscribtion != as);
					}

				})) {

				THROW_MPF_EXC_FWD(nullptr);
				}
			}
		else {
			THROW_MPF_EXC_FWD(nullptr);
			}
		}

	std::string trace_line()const noexcept {
	#ifdef CBL_MPF_TRACELEVEL_EVENT_CONNECTION
		return make_trace_line();
	#else
		return {};
	#endif
		}
	};
}

