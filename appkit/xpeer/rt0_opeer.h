#pragma once


#include <memory>
#include <atomic>
#include <chrono>
#include "./rt0_xpeer.h"
#include "./rtx_cnctx.h"
#include "./xpeer_cop_handler.h"


namespace crm::detail{


/*! �������� ����� ���������� ����������� � ��������� ���� */
class xpeer_out_rt0_t  final :
	public xpeer_base_rt0_t,

	/*! ��������� ���������� ����������� */
	public i_outcoming_peer_t {

public:
	typedef xpeer_out_rt0_t self_t;
	typedef xpeer_base_rt0_t base_t;

	typedef std::function<void(link_t && obj, 
		async_operation_result_t, 
		std::unique_ptr<dcf_exception_t> && exc,
		reconnect_setup_t rc )> connection_result_t;

private:
	std::shared_ptr<__initialize_state_t> _iniSt;
	exceptions_suppress_checker_t _remoteExceptionChecker;

#ifdef CBL_MPF_OBJECTS_COUNTER_CHECKER_RT0_PEER
	crm::utility::__xobject_counter_logger_t<self_t, 1> _xDbgCounter;
#endif


	using indentity_tail = indentity_tail<xpeer_out_rt0_t>;

	static indentity_tail make_identity_handler(std::weak_ptr<self_t> wptr,
		connection_result_t && connectionHndl)noexcept;

	void connection_result_handler(async_operation_result_t result,
		std::unique_ptr<dcf_exception_t> && exc,
		reconnect_setup_t rc,
		std::unique_ptr<__outbound_connection_result_t<link_t>> && pHndl)noexcept;


	void destroy()noexcept final;

	std::shared_ptr<__initialize_state_t> initialize_state()noexcept;
	std::shared_ptr<__initialize_state_t> initialize_state_release()noexcept;

	void handshake_state_register(std::shared_ptr< __rtx_out_connection_ctx_timered_t> pctx,
		object_initialize_handler_t && initializeHndl);

	void command_handler_dtg_identity( std::unique_ptr<isendrecv_datagram_t> && iDtg)final;
	void command_handler_dgt_initialize( std::unique_ptr<isendrecv_datagram_t> && iDtg)final;

	void _connect(std::weak_ptr<__rtx_out_connection_ctx_timered_t> connCtx,
		ctr_0_input_queue_t && destStream );
	void _on_connect( std::weak_ptr<__rtx_out_connection_ctx_timered_t> connCtx,
		ctr_0_input_queue_t && destStream );

	void handshake_exception_handler(std::weak_ptr<__rtx_out_connection_ctx_timered_t> connCtx,
		std::unique_ptr<dcf_exception_t> && ntf)noexcept;
		 
	void start_handshake( std::weak_ptr<__rtx_out_connection_ctx_timered_t> connCtx );

	/*! ��������� �������� ������������� ������ ������� */
	void on_handshake_success();

	/* ������������� ���������� �������-����������� */
	xpeer_out_rt0_t(const std::weak_ptr<distributed_ctx_t> & ctx_,
		std::unique_ptr<i_xpeer_source_t> && source_,
		std::string address,
		int port,
		std::unique_ptr<iol_gtw_t> && gtw_,
		rtx_object_closed_t && closeHndl,
		exceptions_suppress_checker_t && remoteExcCheck,
		std::shared_ptr<i_peer_statevalue_t> && stval,
		const identity_descriptor_t &  thisId,
		__event_handler_point_f&& eh);

public:
	~xpeer_out_rt0_t();

	xpeer_out_rt0_t( const xpeer_out_rt0_t & ) = delete;
	xpeer_out_rt0_t& operator=(const xpeer_out_rt0_t &) = delete;

	xpeer_out_rt0_t( xpeer_out_rt0_t && o ) = delete;
	xpeer_out_rt0_t& operator=(xpeer_out_rt0_t &&o) = delete;

public:
	static std::shared_ptr<xpeer_cop_handler> connect(const std::weak_ptr<distributed_ctx_t> & ctx_,
		std::unique_ptr<i_xpeer_source_t> && source_,
		std::unique_ptr<iol_gtw_t> && gtw_,
		std::unique_ptr<i_endpoint_t> && targetEP,
		const identity_descriptor_t & thisId,
		rtx_object_closed_t && closeHndl,
		connection_result_t && connectionHndl,
		local_object_create_connection_t && localIdentityCtr,
		remote_object_check_identity_t && remoteIdentityChecker,
		object_initialize_handler_t && objectInitialize,
		ctr_0_input_queue_t && ctrInputStack,
		std::shared_ptr<i_peer_statevalue_t> && stval ,
		__event_handler_point_f&& eh,
		exceptions_suppress_checker_t && remoteExcCheck = nullptr );
	};
}


