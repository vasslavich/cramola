#pragma once


#include <memory>
#include "./base_predecl.h"
#include "./rt0_opeer.h"
#include "./xslink.h"
#include "./rt0_cninzr.h"
#include "./outbound_xconnector.h"
#include "./mk_outbound_peer.h"
#include "./i_rt0_ostream.h"


namespace crm::detail{


class ostream_rt0_x_t  final :
	public i_rt0_ostream_t,
	public std::enable_shared_from_this<ostream_rt0_x_t> {

public:
	typedef xpeer_out_rt0_t::link_t link_t;
	typedef link_t::xpeer_desc_t xpeer_desc_t;
	typedef xpeer_out_rt0_t::state_t state_t;
	typedef link_t::remote_object_endpoint_t remote_object_endpoint_t;

private:
	typedef xpeer_out_connection_t<xpeer_out_rt0_t> remote_peer_t;

public:
	typedef xpeer_strong_link_t<ostream_rt0_x_t> ostream_lnk_t;
	typedef ostream_rt0_x_t self_t;
	typedef xpeer_out_rt0_t::input_value_t input_value_t;
	typedef xpeer_out_rt0_t::output_value_t output_value_t;
	typedef remote_peer_t::close_event_hndl_t close_event_hndl_t;
	typedef remote_peer_t::xpeer_desc_t xpeer_desc_t;
	typedef xpeer_out_connector_rt0_t::post_connection_t post_connection_t;
	using subscribe_event_connection_t = remote_peer_t::subscribe_event_connection_t;

private:
	enum class stream_route_type {
		undefined_,
		routed_,
		auto_
		};

	std::weak_ptr<distributed_ctx_t> _ctx;
	remote_peer_t::handler_t _peerLnk;
	std::weak_ptr<i_rt0_main_router_input_t> _inputStockRouted;
	std::unique_ptr<i_input_messages_stock_t> _inputStockAuto;
	const stream_route_type _rtype{ stream_route_type::undefined_ };
	std::atomic<bool>_closedf{false};

#ifdef CBL_MPF_OBJECTS_COUNTER_CHECKER_RT0_PEER
	crm::utility::__xobject_counter_logger_t<self_t> _xDbgCounter;
#endif

	std::shared_ptr<i_rt0_main_router_input_t> input_channel_routed()const noexcept;
	const std::unique_ptr<i_input_messages_stock_t>& input_channel_auto()const noexcept;

	void exc_hndl( std::unique_ptr<dcf_exception_t> && ntf )noexcept;
	void exc_hndl( const dcf_exception_t & exc )noexcept;
	void exc_hndl( dcf_exception_t && exc )noexcept;
	void input_hndl( input_value_t && item );
	void unbind_reslink()noexcept;

public:
	ostream_rt0_x_t(std::weak_ptr<distributed_ctx_t> ctx_,
		std::shared_ptr<i_rt0_main_router_input_t> && inputStock );
	ostream_rt0_x_t(std::weak_ptr<distributed_ctx_t> ctx_,
		std::unique_ptr<i_input_messages_stock_t>&& inputStock );

	void ctr(const identity_descriptor_t & thisId_,
		local_object_create_connection_t && idCtr,
		remote_object_check_identity_t && idChecker,
		post_connection_t && postConnHndl,
		close_event_hndl_t && closeHndl,
		std::unique_ptr<i_endpoint_t> && endpoint,
		std::shared_ptr<default_binary_protocol_handler_t> && ctrGateway,
		object_initialize_handler_t && objectInitializer,
		std::shared_ptr<i_xpeer_source_factory_t> xpFact,
		std::function<std::shared_ptr<i_peer_statevalue_t>()> && stvctr,
		exceptions_suppress_checker_t && connectionExcChecker,
		__event_handler_point_f&& eh,
		async_space_t scx );

	~ostream_rt0_x_t();
	void close()noexcept final;
	bool closed()const noexcept final;
	bool is_opened()const noexcept final;

	peer_commands_result_t execute_command( peer_command_t cmd );
	link_t link() noexcept;
	local_object_id_t local_id()const noexcept final;
	identity_descriptor_t this_id()const noexcept final;
	remote_object_state_t state()const noexcept final;
	identity_descriptor_t remote_id()const noexcept final;
	identity_descriptor_t id()const noexcept final;
	xpeer_desc_t desc()const noexcept final;
	rt0_node_rdm_t rt0_rdm()const noexcept final;
	connection_key_t connection_key()const noexcept final;
	std::string address()const noexcept final;
	int port()const noexcept final;
	remote_object_endpoint_t remote_object_endpoint()const noexcept final;
	std::shared_ptr<distributed_ctx_t> ctx()const noexcept;
	std::shared_ptr<const i_peer_remote_properties_t> remote_properties()const noexcept;

	std::shared_ptr<i_peer_statevalue_t> state_value() noexcept;
	std::shared_ptr<const i_peer_statevalue_t> state_value()const noexcept;
	void set_state_value( std::shared_ptr<i_peer_statevalue_t> && stval ) noexcept;

#ifdef CBL_USE_SMART_OBJECTS_COUNTING
	crm::log::counter_t subcounter(const std::string & name);
#endif//CBL_USE_SMART_OBJECTS_COUNTING

	template<typename O,
		typename std::enable_if_t<is_out_message_requirements_v<O>, int> = 0>
	void push(O && o) {
		_peerLnk.push(std::forward<O>(o));
		}

	void write(outbound_message_unit_t && blob )final;

	void invoke_after_ready( async_space_t scx, std::unique_ptr<subscribe_event_connection_t> && hndl );

	template<typename THandl>
	[[nodiscard]]
	subscribe_invoker_result subscribe_hndl_id(async_space_t scx,
		const _address_hndl_t& hndl,
		subscribe_options opt,
		THandl&& f) {

		return _peerLnk.subscribe_hndl_id(scx, hndl, opt, std::forward<THandl>(f));
		}

	template<typename THandler,
		typename std::enable_if_t<std::is_constructible_v<__event_handler_f, std::decay_t<THandler>>, int> = 0>
	[[nodiscard]]
	subscribe_result subscribe_event(async_space_t scx, 
		const _address_event_t& hndl, 
		bool once, 
		THandler&& f) {

		return _peerLnk.subscribe_event(scx, hndl, once, std::forward<THandler>(f));
		}

	void subscribe_event(events_list&& el);
	void unsubscribe_event( const _address_event_t &hndl )noexcept;
	void unsubscribe_hndl_id(const _address_hndl_t& hndl)noexcept;

	bool renewable_connection()const  noexcept;
	rtl_table_t direction()const noexcept;

	static ostream_lnk_t create_routed(std::weak_ptr<distributed_ctx_t> ctx_,
		std::shared_ptr<default_binary_protocol_handler_t> ctr_gateway_rt0_t,
		object_initialize_handler_t && objectInitializer,
		std::shared_ptr<i_xpeer_source_factory_t> xpFact,
		post_connection_t && postConnHndl,
		close_event_hndl_t && closeHndl,
		const identity_descriptor_t & thisId_,
		local_object_create_connection_t && idCtr,
		remote_object_check_identity_t && idChecker,
		std::unique_ptr<i_endpoint_t> && endpoint,
		std::shared_ptr<i_rt0_main_router_input_t>  && inputStock,
		std::function<std::shared_ptr<i_peer_statevalue_t>()> && stvctr,
		exceptions_suppress_checker_t && connectionExcChecker,
		__event_handler_point_f&& eh,
		async_space_t scx);

	static ostream_lnk_t create_auto(std::weak_ptr<distributed_ctx_t> ctx_,
		std::shared_ptr<default_binary_protocol_handler_t> ctr_gateway_rt0_t,
		object_initialize_handler_t&& objectInitializer,
		std::shared_ptr<i_xpeer_source_factory_t> xpFact,
		post_connection_t&& postConnHndl,
		close_event_hndl_t&& closeHndl,
		const identity_descriptor_t& thisId_,
		local_object_create_connection_t&& idCtr,
		remote_object_check_identity_t&& idChecker,
		std::unique_ptr<i_endpoint_t>&& endpoint,
		std::unique_ptr<i_input_messages_stock_t>&& inputStock,
		std::function<std::shared_ptr<i_peer_statevalue_t>()>&& stvctr,
		exceptions_suppress_checker_t&& connectionExcChecker,
		__event_handler_point_f&& eh,
		async_space_t scx);
	};
}


