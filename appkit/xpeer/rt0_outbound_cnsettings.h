#pragma once


#include "../tableval/tableval.h"
#include "../xpeer/rt0_xpeer.h"
#include "../xpeer/rt0_end_opeer.h"
#include "../channel_terms/terms.h"
#include "../queues/i_message_queue.h"
#include "../messages/terms.h"
#include "../xpeer/xpeer_mkprereq.h"
#include "../xpeer/i_input_handlers.h"


namespace crm {


class rt0_connection_settings_t {
public:
	using xpeer_link = opeer_l0::link_type;

private:
	identity_descriptor_t _thisId;
	std::unique_ptr<rt1_endpoint_t> _ep;
	std::unique_ptr<i_input_messages_stock_t> _inputStackCtr;
	bool _keepAlive{ false };

	void assign(rt0_connection_settings_t&& o);

public:
	virtual ~rt0_connection_settings_t() {}

	template<typename TEndPoint,
		typename TInputMessageHandler,

		typename _TEndPoint = std::decay_t<TEndPoint>,
		typename _TInputMessageHandler = std::decay_t<TInputMessageHandler>,
		typename std::enable_if_t<(
			std::is_base_of_v<rt1_endpoint_t, _TEndPoint>&&
			std::is_base_of_v<i_input_messages_stock_t, _TInputMessageHandler>
			), int> = 0>
	rt0_connection_settings_t(const identity_descriptor_t& thisId,
		TEndPoint && ep,
		TInputMessageHandler && inputStackCtr,
		bool keepAlive_ = false)
		: _thisId(thisId)
		, _ep(std::make_unique<_TEndPoint>(std::forward<TEndPoint>(ep)))
		, _inputStackCtr(std::make_unique<_TInputMessageHandler>(std::forward<TInputMessageHandler>(inputStackCtr)))
		, _keepAlive(keepAlive_) {

		CBL_VERIFY(!(_ep->address().empty() || _ep->port() == 0));
		}

	template<typename TEndPoint, 
		typename TInputMessageHandler,

		typename _TEndPoint = std::decay_t<TEndPoint>,
		typename _TInputMessageHandler = std::decay_t<TInputMessageHandler>,

		typename std::enable_if_t<(
			std::is_base_of_v<rt1_endpoint_t, _TEndPoint>&& 
			is_rt0_outbound_connection_input_handler_v<_TInputMessageHandler>
			), int> = 0>
	rt0_connection_settings_t(const identity_descriptor_t& thisId,
		TEndPoint && ep,
		TInputMessageHandler && h,
		bool keepAlive_ = false)
		: rt0_connection_settings_t(thisId,
			std::forward<TEndPoint>(ep),
			opeer_l0_input2handler_t(std::forward<TInputMessageHandler>(h)),
			keepAlive_) {}

	rt0_connection_settings_t(const rt0_connection_settings_t&) = delete;
	rt0_connection_settings_t& operator=(const rt0_connection_settings_t&) = delete;

	rt0_connection_settings_t(rt0_connection_settings_t&& o)noexcept;
	rt0_connection_settings_t& operator=(rt0_connection_settings_t&& o)noexcept;

	const identity_descriptor_t& this_id()const noexcept;
	std::unique_ptr<i_endpoint_t> remote_endpoint()const noexcept;

	virtual syncdata_list_t local_object_syncdata();
	virtual remote_object_connection_value_t local_object_identity();
	virtual bool remote_object_identity_validate(const identity_value_t&,
		std::unique_ptr<i_peer_remote_properties_t>&);
	virtual bool connnection_exception_suppress_checker(detail::connection_stage_t,
		const std::unique_ptr<dcf_exception_t>&)noexcept;
	virtual void local_object_initialize(datagram_t&& /*dt*/,
		detail::object_initialize_result_guard_t&& resultHndl);

	virtual void connection_handler(std::unique_ptr<dcf_exception_t>&&, 
		xpeer_link &&);

	virtual void disconnection_handler(const detail::i_xpeer_rt0_t::xpeer_desc_t&,
		std::shared_ptr<i_peer_statevalue_t>&&);

	virtual void event_handler(std::unique_ptr<i_event_t>&& ev);
	std::unique_ptr<i_input_messages_stock_t> input_stack();
	bool keep_alive()const noexcept;
	};


template<typename S, typename = void>
struct is_outbound_connection_settings_rt0 : std::false_type {};

template<typename S>
struct is_outbound_connection_settings_rt0 < S,
	typename std::enable_if_t<std::is_base_of_v<rt0_connection_settings_t, S>>> : std::true_type {};

template<typename S>
constexpr bool is_outbound_connection_settings_rt0_v = is_outbound_connection_settings_rt0<S>::value;
}
