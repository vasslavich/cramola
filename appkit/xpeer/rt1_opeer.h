#pragma once


#include <memory>
#include <chrono>
#include "./rt1_xpeer.h"
#include "../messages/internal_terms.h"
#include "./rtx_cnctx.h"
#include "./base_predecl.h"


namespace crm:: detail{


/*! �������� ����� ���������� ����������� � ��������� ���� */
class xpeer_out_rt1_t final :
	public xpeer_base_rt1_t {

public:
	typedef xpeer_out_rt1_t self_t;
	typedef xpeer_base_rt1_t base_t;
	typedef std::function<void(link_t && obj,
		async_operation_result_t,
		std::unique_ptr<dcf_exception_t> && exc,
		reconnect_setup_t rc)> connection_result_t;

private:
	std::weak_ptr<i_rt1_main_router_t> _rt;
	std::shared_ptr<__initialize_state_t> _iniSt;
	exceptions_suppress_checker_t _remoteExceptionChecker;

	void command_handler_dtg_identity(  std::unique_ptr<isendrecv_datagram_t> && iDtg )final;
	void command_handler_dgt_initialize(  std::unique_ptr<isendrecv_datagram_t> && iDgt )final;

	using indentity_tail = indentity_tail<xpeer_out_rt1_t>;
	static indentity_tail make_identity_handler( std::weak_ptr<self_t> ptr,
		connection_result_t && connectionHndl )noexcept;

	void connection_result_handler(async_operation_result_t result,
		std::unique_ptr<dcf_exception_t> && exc,
		reconnect_setup_t rc,
		std::unique_ptr<__outbound_connection_result_t<link_t>> && pHndl)noexcept;

public:
	remote_connection_desc_t remote_connection_descriptor()const noexcept;

private:
	std::shared_ptr<__initialize_state_t> initialize_state()noexcept;
	void handshake_state_register(std::shared_ptr<__rtx_out_connection_ctx_timered_t> ctx,
		object_initialize_handler_t && initializeHndl);

	void handshake_exception_handler(std::weak_ptr<__rtx_out_connection_ctx_timered_t> connCtx,
		std::unique_ptr<dcf_exception_t> && ntf)noexcept;

	void _session_create( std::weak_ptr<__rtx_out_connection_ctx_timered_t> connCtx,
		ctr_1_input_queue_t && qinput);
	void _on_session_create( std::weak_ptr<__rtx_out_connection_ctx_timered_t> connCtx,
		ctr_1_input_queue_t && qinput);

	void start_connection_request( std::weak_ptr<__rtx_out_connection_ctx_timered_t> connCtx );
	void on_connection_request( std::weak_ptr<__rtx_out_connection_ctx_timered_t> onnCtx );

	/*! ������� ��������� �������� ����������������� ������ �� �������, ��������� �������� */
	void start_handshake( std::weak_ptr<__rtx_out_connection_ctx_timered_t> connCtx );
	/*! ��������� �������� ������������� ������ ������� */
	void on_handshake_success();

	std::shared_ptr<ox_rt1_stream_t> osource()noexcept;

public:
	/* ������������� ���������� �������-����������� */
	xpeer_out_rt1_t( const std::weak_ptr<distributed_ctx_t> & ctx,
		std::weak_ptr<i_rt1_main_router_t> rt,
		std::shared_ptr<ox_rt1_stream_t> && source_,
		std::unique_ptr<gtw_rt1_x_t> && gtw,
		rtx_object_closed_t && closeHndl,
		std::shared_ptr<i_peer_statevalue_t> && stval,
		__event_handler_point_f && xevh,
		exceptions_suppress_checker_t && remoteExcChecker );

private:
	static void connect( std::shared_ptr<self_t> && obj,
		const std::weak_ptr<distributed_ctx_t> & ctx,
		connection_result_t && connectionHndl,
		local_object_create_connection_t && localIdentityCtr,
		remote_object_check_identity_t && remoteIdentityChecker,
		ctr_1_input_queue_t && destStream,
		object_initialize_handler_t && objectInitializer );

public:
	xpeer_out_rt1_t( const xpeer_out_rt1_t & ) = delete;
	xpeer_out_rt1_t& operator=(const xpeer_out_rt1_t &) = delete;

	xpeer_out_rt1_t( xpeer_out_rt1_t && o ) = delete;
	xpeer_out_rt1_t& operator=(xpeer_out_rt1_t &&o) = delete;

	~xpeer_out_rt1_t();

	static void connect2stack(const std::weak_ptr<distributed_ctx_t> & ctx,
		std::weak_ptr<i_rt1_main_router_t> rt,
		std::shared_ptr<ox_rt1_stream_t> && source_,
		std::unique_ptr<gtw_rt1_x_t> && gtw,
		rtx_object_closed_t && closingHndl,
		connection_result_t && connectionHndl,
		local_object_create_connection_t && localIdentityCtr,
		remote_object_check_identity_t && remoteIdentityChecker,
		object_initialize_handler_t && objectInitializer,
		ctr_1_input_queue_t && hndlStack,
		std::shared_ptr<i_peer_statevalue_t> && stval,
		__event_handler_point_f && xevh,
		exceptions_suppress_checker_t && remoteExcChecker);

	static void connect2handler( const std::weak_ptr<distributed_ctx_t> & ctx,
		std::weak_ptr<i_rt1_main_router_t> rt,
		std::shared_ptr<ox_rt1_stream_t> && source_,
		std::unique_ptr<gtw_rt1_x_t> && gtw,
		rtx_object_closed_t && closingHndl,
		connection_result_t && connectionHndl,
		local_object_create_connection_t && localIdentityCtr,
		remote_object_check_identity_t && remoteIdentityChecker,
		object_initialize_handler_t && objectInitializer,
		input_handler_opeer_t && hndlInput,
		std::shared_ptr<i_peer_statevalue_t> && stval,
		__event_handler_point_f && xevh,
		exceptions_suppress_checker_t && remoteExcChecker);

	static void connect2i( const std::weak_ptr<distributed_ctx_t> & ctx,
		std::weak_ptr<i_rt1_main_router_t> rt,
		std::shared_ptr<ox_rt1_stream_t> && source_,
		std::unique_ptr<gtw_rt1_x_t> && gtw,
		rtx_object_closed_t && closingHndl,
		connection_result_t && connectionHndl,
		local_object_create_connection_t && localIdentityCtr,
		remote_object_check_identity_t && remoteIdentityChecker,
		object_initialize_handler_t && objectInitializer,
		const i_input_messages_stock_t * hndlInput,
		std::shared_ptr<i_peer_statevalue_t> && stval,
		__event_handler_point_f && xevh,
		exceptions_suppress_checker_t && remoteExcChecker );
	};
}

