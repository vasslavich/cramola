#pragma once


#include <memory>
#include <atomic>
#include <chrono>
#include <queue>
#include <list>
#include "../internal_invariants.h"
#include "./base_predecl.h"
#include "./i_rt0_xpeer.h"
#include "./refornull_objhld.h"
#include "../protocol/i_ssource.h"
#include "../protocol/base_predecl.h"
#include "../channel_terms/subscribe_terms.h"
#include "../protocol/i_bincntr.h"
#include "../channel_terms/events_terms.h"
#include "../utilities/check_validate/checker_seqcall.h"


namespace crm ::detail{


struct x0_xpeer_address_handler final : public i_x0_xpeer_address_handler {
	/*==============================
	������� �����
	=================================*/

	/*! ���������� ������������� ������� */
	const identity_descriptor_t _thisId;
	/*! ��������� ������������� ������� */
	const local_object_id_t _thisLocid;
	/*! ���������� */
	refornull_objhld_t<rt0_node_rdm_t> _localRdm;
	/*! ������������� ��������� ����������� ������� */
	refornull_objhld_t<identity_descriptor_t> _remoteId;
	std::string _address;
	int _port{ 0 };

	/*! ����������� �������(���������/�������� �����������) */
	peer_direction_t _direction{ peer_direction_t::undefined };
	/*! ������ ����������� */
	std::atomic<remote_object_state_t> _state{ remote_object_state_t::null };

#ifdef CBL_MPF_OBJECTS_COUNTER_CHECKER_RT0_PEER
	utility::__xobject_counter_logger_t<x0_xpeer_address_handler, 1> _xDbgCounter;
#endif

	x0_xpeer_address_handler( const std::weak_ptr<distributed_ctx_t> & ctx0,
		const identity_descriptor_t &thisId,
		std::string address,
		int port,
		peer_direction_t direction);

	remote_object_state_t state()const noexcept final;
	void set_state(remote_object_state_t st)noexcept final;
	const session_description_t& session()const noexcept final;
	const rdm_local_identity_t& rdm_local_identity()const noexcept final;
	const local_object_id_t& local_id()const noexcept final;
	const rt0_node_rdm_t& rt0rdm()const noexcept final;
	rtl_table_t direction()const noexcept final;
	peer_direction_t peer_direction()const noexcept final;
	const identity_descriptor_t& this_id()const noexcept final;
	const identity_descriptor_t& remote_id()const noexcept final;
	const identity_descriptor_t& id()const noexcept final;
	const xpeer_desc_t& desc()const noexcept final;
	/*! ����� ��������� ����� ����������� */
	const std::string& address()const noexcept final;
	/*! ���� �������� ����� ����������� */
	int port()const noexcept final;
	connection_key_t connection_key()const noexcept final;
	remote_object_endpoint_t remote_object_endpoint()const noexcept final;

	void set_rdm(rt0_node_rdm_t && rdm);
	void set_remote_id(const identity_descriptor_t&);

	rtl_level_t level()const noexcept final {
		return rtl_level_t::l0;
		}
	};


class xpeer_base_rt0_t :
	/* ��������� ���� �������� ������ */
	public i_xpeer_rt0_t,
	/* ���������������� �������� ������ �� ������ */
	public std::enable_shared_from_this<xpeer_base_rt0_t>{

public:
	typedef xpeer_base_rt0_t self_t;

	/*! ����������� ������� ��������� ���������� �������� ����������� ���� � ��������� ���� � ������� */
	typedef std::function<void()> register_node_completed_t;

	/*! ������� ��������� ������� ���������� ������� */
	typedef std::function<void( const rdm_local_identity_t &,
		closing_reason_t,
		std::shared_ptr<i_peer_statevalue_t> && stv )> rtx_object_closed_t;

	typedef peer_lnk_t<xpeer_base_rt0_t> link_t;
	typedef peer_lnk_t<const xpeer_base_rt0_t> clink_t;

	using event_open_connection_t = event_open_connection_t<link_t>;
	using event_close_connection_t = event_close_connection_t<link_t>;

private:
	typedef binary_vector_t io_buffer_t;

	/*! �������� */
	std::weak_ptr<distributed_ctx_t> _ctx;
	std::shared_ptr<x0_xpeer_address_handler> _addrh;
	/*! �������� ��������� ������ */
	std::unique_ptr<i_xpeer_source_t> _source;
	/*! ������ ��������� ��������������� */
	std::unique_ptr<iol_gtw_t> _gateway;
	/*! ���������� ������� �������� ������� */
	rtx_object_closed_t _eventClosingHndl;
	/*! ���������������� ������ ���� */
	std::shared_ptr<i_peer_statevalue_t> _stateValue;
	std::shared_ptr<input_stack_t> _inQueue;
	/*! ��������� ������� ��������� ������� */
	std::shared_ptr<i_peer_remote_properties_t> _remoteProp;
	/*! ���������� �� ������� ������� */
	event_subscribe_assist_t::event_handler_t _evhLst;

	__event_handler_point_f _evHndl;

#ifdef CBL_USE_SEQUENTIAL_CALL_CHECKER
	checker_seqcall_t _seqCallCheckerIO;
#endif

	/*! �������, ��� ��������� ��������� �������� ������� */
	std::atomic<bool> _closedf = false;
	/*! ������� ������ ��������� */
	std::atomic<bool> _cancelf = false;

	std::string address_trace()const;

	void close_trace(closing_reason_t)noexcept;

	void trace_send( const i_iol_ohandler_t & msg )const noexcept;
	void trace_recv( const i_iol_ihandler_t & msg )const noexcept;

	event_subscribe_assist_t::event_handler_t& get_handler();
	sndrcv_timeline_t ping_timeline()const;

#ifdef CBL_ENABLE_TEST_ERROR_EMULATION_LEVEL_0
	std::string check_erreml_key()const noexcept;
#endif

protected:
#ifdef CBL_USE_SEQUENTIAL_CALL_CHECKER
	using seqcall_checker_holder = std::decay_t<decltype(_seqCallCheckerIO.inc_scope())>;
	seqcall_checker_holder inc_scope_seqcall();
#endif

	virtual void on_handshake_success();
	void event_invoke(event_type_t type, std::unique_ptr<event_args_t> && args)noexcept;

	void begin_connect(const std::unique_ptr<i_endpoint_t> &address,
		std::function<void(std::unique_ptr<dcf_exception_t> && exc)> && endConnect);

	void set_state(remote_object_state_t st)noexcept;

	void set_session_input(const identity_descriptor_t & nodeId_,
		const session_description_t & session_ );
	void set_session_output( const identity_descriptor_t & nodeId_,
		const session_description_t & session_,
		const i_endpoint_t & epRequested );

	void set_remote_properties( std::unique_ptr<i_peer_remote_properties_t> && prop )noexcept;

public:
	std::shared_ptr<i_x0_xpeer_address_handler> xpeer_base_rt0_t::address_handler()const noexcept final;

	std::shared_ptr<distributed_ctx_t> ctx()noexcept;
	std::shared_ptr<distributed_ctx_t> ctx()const noexcept;

	/*! ������ ������ �� ��������� ������� */
	link_t get_link()noexcept;
	/*! ������ ������ �� ��������� ������� */
	clink_t get_link()const noexcept;

	void exc_hndl( std::unique_ptr<dcf_exception_t> && exc )noexcept;
	void exc_hndl(const dcf_exception_t & exc)noexcept;
	void exc_hndl(dcf_exception_t && exc)noexcept;

#ifdef CBL_USE_SMART_OBJECTS_COUNTING
	crm::log::counter_t subcounter(const std::string &name);
#endif//CBL_USE_SMART_OBJECTS_COUNTING

	std::shared_ptr<const i_peer_remote_properties_t> remote_properties()const noexcept final;
	bool renewable_connection()const noexcept final;

private:
	void send_ping_request();
	void send_echo_ping( std::unique_ptr<i_iol_ihandler_t> && sourceObj );
	void end_read_push_ready( const std::vector<uint8_t> & buf, size_t );

	void subscribe_command_handler();
	void command_handler( std::unique_ptr<i_iol_ihandler_t> && iCmd);
	std::function<std::unique_ptr<i_event_t>(event_type_t)> event_checker() noexcept;

protected:
	void on_extern_completed( std::function<void()> && externHndl );

	/* ������������� ���������� ���������� ����������� */
	xpeer_base_rt0_t(const std::weak_ptr<distributed_ctx_t> & ctx0,
		std::unique_ptr<i_xpeer_source_t> && source_,
		std::string address,
		int port,
		std::unique_ptr<iol_gtw_t> && gtw_,
		rtx_object_closed_t && closeHndl,
		std::shared_ptr<i_peer_statevalue_t> && stval,
		const identity_descriptor_t &thisId,
		peer_direction_t direction,
		__event_handler_point_f&& eh);

	void close(closing_reason_t reason)noexcept;

	void set_id(const identity_descriptor_t  &id);

	void set_handshake_mode(std::shared_ptr<input_stack_t> qinput );
	void handshake_end( bool result );
	
	virtual void command_handler_dtg_identity( std::unique_ptr<isendrecv_datagram_t> && iDgt) = 0;
	virtual void command_handler_dgt_initialize( std::unique_ptr<isendrecv_datagram_t> && iDgt) = 0;

	void io_rw_listen();

	void reverse_functor_message(std::unique_ptr<ifunctor_package_t> && m);

public:
	xpeer_base_rt0_t( const xpeer_base_rt0_t & ) = delete;
	xpeer_base_rt0_t& operator=(const xpeer_base_rt0_t &) = delete;

	xpeer_base_rt0_t( xpeer_base_rt0_t && o ) = delete;
	xpeer_base_rt0_t& operator=(xpeer_base_rt0_t &&o) = delete;

	virtual ~xpeer_base_rt0_t();

	remote_object_state_t state()const noexcept final;
	const session_description_t& session()const noexcept final;
	const rdm_local_identity_t& rdm_local_identity()const noexcept;
	const local_object_id_t& local_id()const noexcept final;
	const rt0_node_rdm_t& rt0rdm()const noexcept final;
	rtl_table_t direction()const noexcept final;
	peer_direction_t peer_direction()const noexcept;
	rtl_level_t level()const noexcept final;
	const identity_descriptor_t& this_id()const noexcept final;
	const identity_descriptor_t& remote_id()const noexcept final;
	const identity_descriptor_t& id()const noexcept final;
	const xpeer_desc_t& desc()const noexcept final;
	/*! ����� ��������� ����� ����������� */
	const std::string& address()const noexcept final;
	/*! ���� �������� ����� ����������� */
	int port()const noexcept final;
	connection_key_t connection_key()const noexcept final;
	remote_object_endpoint_t remote_object_endpoint()const noexcept final;
	/*! ��� ��������� ���� */
	const std::string& name()const noexcept final;

	std::shared_ptr<i_peer_statevalue_t> state_value()noexcept;
	std::shared_ptr<const i_peer_statevalue_t> state_value()const noexcept;
	void set_state_value( std::shared_ptr<i_peer_statevalue_t> && stv )noexcept;

	std::shared_ptr<input_stack_t> get_input_queue()noexcept;
	std::shared_ptr<input_stack_t> get_input_queue()const noexcept;

	void close()noexcept;

	bool is_opened()const noexcept;
	bool closed() const noexcept;

	void invoke_event(event_type_t)noexcept final;

protected:
	virtual void destroy()noexcept;

private:
	template<typename O,
		typename std::enable_if_t < is_out_message_requirements_v<O>, int> = 0>
	void normalizex(O && obj)const {
		if (auto c = ctx()) {
			/* ������ */
			if (is_null(obj.session()))
				obj.set_session(session());

			/* ������� */
			if (is_null(obj.destination_id()))
				obj.set_destination_id(remote_id());

			/* �������� */
			if (is_null(obj.source_id()))
				obj.set_source_id(this_id());

			/* ������� ������������� */
			if (!obj.fl_level())
				obj.set_level(level());

			/* ������������� ��������� */
			if (is_null(obj.id())) {
				obj.set_id(c->generate_message_id());
				}

			/* ������������� ����������� ����� */
			if (!_source->is_integrity_provided() || c->policies().use_crc()) {
				obj.setf_use_crc();
				}

#ifdef CBL_MPF_ENABLE_TIMETRACE
			obj.set_timeline(detail::hanlder_times_stack_t::stage_t::rt0_sender);
#endif//CBL_MPF_ENABLE_TIMETRACE
			}
		else {
			THROW_MPF_EXC_FWD(nullptr);
			}
		}

	template<typename O,
		typename std::enable_if_t < is_out_message_requirements_v<O>, int> = 0>
	void push_normalizedx(O && obj) {
		normalizex( obj );

		CBL_VERIFY(obj.type().type() != iol_types_t::st_notification || !is_sendrecv_request(obj.address()));
		write(CHECK_PTR(_gateway)->serialize_gateout(std::forward<O>(obj)));
		}

	subscribe_invoker_result __subscribe_hndl_id(async_space_t scx, 
		const _address_hndl_t & id, 
		subscribe_options opt,
		subscribe_message_handler && h);
	void __unsubscribe_hndl_id( const _address_hndl_t & id )noexcept;

	__event_handler_point_f get_event_handler()const noexcept;

public:
	/*! ��������� ������ � ������� ��������� ��������� */
	template<typename O,
		typename std::enable_if_t < is_out_message_requirements_v<O>, int> = 0>
	void push( O && obj ) {
		push_normalizedx(std::forward<O>(obj));
		}

	/*! ��������� ������ � ������� ��������� ��������� */
	template<typename O,
		typename std::enable_if_t <(
			srlz::is_serializable_v<O>&&
			!is_out_message_requirements_v<O>
			), int> = 0>
	void push(O&& obj){
		push(ostrob_message_envelop_t(std::forward<O>(obj)));
		}

	/*! ������ � �������������� ������-��������� */
	//void write( const crm::srlz::i_rstream_t & proxy );
	/*!  ������ ������ */
	void write(outbound_message_unit_t && blob );

	/*! �������� �� ��������� ��������� ��� ���������� ��������������.
	\param id ������� �������������
	\param once ���� ������� true, ����������� �������� ���� ���, ����� �� ��� ���,
	���� �� ����� ��������� ������ ����������� � ������� ������ /ref unsubscribe_hndl_id.
	*/
	[[nodiscard]]
	subscribe_invoker_result subscribe_hndl_id_tv(async_space_t scx, const _address_hndl_t & id, subscribe_options opt,
		subscribe_message_handler && h);

	[[nodiscard]]
	subscribe_invoker_result subscribe_hndl_id(async_space_t scx, const _address_hndl_t & id, subscribe_options opt,
		subscribe_message_handler && h) final;

	/*subscribe_invoker_result subscribe_hndl_id(async_space_t scx, const _address_hndl_t & id, subscribe_options opt,
		const subscribe_handler_t & h) final;*/

	/*! ����� �� ��������� ����������� ��� ���������� ��������������
	\param id ������������� ����������
	*/
	void unsubscribe_hndl_id( const _address_hndl_t & id )noexcept final;

	template<typename THandler>
	[[nodiscard]]
	subscribe_result subscribe_event_tv( async_space_t scx,
		const _address_event_t & id, 
		bool once,
		THandler && h) {

		if (!closed()) {
			return _evhLst.subscribe(scx, id, once, std::forward<THandler>(h), event_checker());
			}
		else {
			return subscribe_result::enum_type::exception;
			}
		}

	[[nodiscard]]
	subscribe_result subscribe_event( async_space_t scx,
		const _address_event_t & id, 
		bool once,
		__event_handler_f && h) final;

	void subscribe_event(events_list && el)final {
		_evhLst.add(std::move(el));
		}

	void unsubscribe_event( const _address_event_t & id )noexcept final;

	peer_commands_result_t execute_command( peer_command_t cmd )final;
	};


using xpeer_rt0_wlink_type = xpeer_base_rt0_t::link_t;
}

