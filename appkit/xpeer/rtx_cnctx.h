#pragma once


#include <functional>
#include <memory>
#include "../internal_invariants.h"
#include "./base_terms.h"
#include "./xpeer_mkprereq.h"
#include "../time/i_timers.h"



namespace crm::detail {


struct __rtx_connection_ctx_t : public std::enable_shared_from_this<__rtx_connection_ctx_t>{
	virtual ~__rtx_connection_ctx_t();

	__rtx_connection_ctx_t(std::weak_ptr<distributed_ctx_t> ctx,
		local_object_create_connection_t && ctrLocalId_,
		remote_object_check_identity_t && checkerRemoteId_,
		std::weak_ptr<i_xpeer_t> );
	__rtx_connection_ctx_t( std::weak_ptr<distributed_ctx_t> ctx,
		local_object_create_connection_t && ctrLocalId_,
		remote_object_check_identity_t && checkerRemoteId_, 
		std::weak_ptr<i_xpeer_t>,
		const sndrcv_timeline_t & tml);

	__rtx_connection_ctx_t( const __rtx_connection_ctx_t & ) = delete;
	__rtx_connection_ctx_t& operator=(const __rtx_connection_ctx_t &) = delete;

	bool expired_waiting()const noexcept;
	remote_object_connection_value_t ctr_local_id()const;
	bool checker_remote_id(const identity_value_t &, std::unique_ptr<i_peer_remote_properties_t> & remoteProp)const;

	void ntf_hndl(std::unique_ptr<dcf_exception_t> && Ntf )noexcept;

	const sndrcv_timeline_t& timeline()const;
	std::shared_ptr<distributed_ctx_t> ctx()const;

private:
	std::weak_ptr<i_xpeer_t> _xpeer;
	sndrcv_timeline_t _timeline;
	std::weak_ptr<distributed_ctx_t> _ctx;
	std::unique_ptr<i_xtimer_oncecall_t> _expiredTimer;
	/*! ������� �������� ����������������� ������ ���� */
	local_object_create_connection_t _ctrLocalId;
	/*! ������� �������� ����������������� ������ ��������� ���� */
	remote_object_check_identity_t _checkerRemoteId;
	std::atomic<async_operation_result_t> _invokeR{ async_operation_result_t::st_abandoned };
	std::atomic<bool> _closedf{ false };

	void close_timer()noexcept;
	void close_peer()noexcept;

protected:
	void start();
	void set_result(async_operation_result_t r)noexcept;

public:
	virtual void close()noexcept;
	virtual void invoke_error(async_operation_result_t, std::unique_ptr<dcf_exception_t> && exc )noexcept = 0;
	virtual void invoke_error(std::unique_ptr<dcf_exception_t> && exc)noexcept = 0;
	async_operation_result_t invoke_result()const noexcept;
	std::shared_ptr< i_xpeer_t> peer()const noexcept;
	};


struct __i_rtx_out_connection_handler_t {
	virtual ~__i_rtx_out_connection_handler_t(){}

	__i_rtx_out_connection_handler_t()noexcept {}

	typedef std::function<void(async_operation_result_t,
		std::unique_ptr<dcf_exception_t> && exc,
		detail::reconnect_setup_t rc)> invoke_handler_t;

	virtual void invoke(async_operation_result_t,
		std::unique_ptr<dcf_exception_t> && exc,
		detail::reconnect_setup_t rc)noexcept = 0;

	__i_rtx_out_connection_handler_t(const __i_rtx_out_connection_handler_t &) = delete;
	__i_rtx_out_connection_handler_t& operator=(const __i_rtx_out_connection_handler_t &) = delete;

	__i_rtx_out_connection_handler_t(__i_rtx_out_connection_handler_t &&) = delete;
	__i_rtx_out_connection_handler_t& operator=(__i_rtx_out_connection_handler_t &&) = delete;
	};


template<typename THandler,
	typename _THandler = typename std::decay_t<THandler>>
struct __rtx_out_connection_functor_wrapper_t final : __i_rtx_out_connection_handler_t {
	_THandler _ih;

	void invoke(async_operation_result_t r,
		std::unique_ptr<dcf_exception_t> && exc,
		detail::reconnect_setup_t rc)noexcept final {

		_ih( r, std::move(exc), rc);
		}

	__rtx_out_connection_functor_wrapper_t(_THandler && h)
		: _ih(std::move(h)) {}
	};


class __rtx_out_connection_ctx_timered_t : public __rtx_connection_ctx_t{
	typedef __rtx_out_connection_ctx_timered_t self_t;
	using base_type = __rtx_connection_ctx_t;

private:
	std::unique_ptr<i_endpoint_t> _targetEP;
	std::unique_ptr< __i_rtx_out_connection_handler_t> _handshakeHndl;
	std::atomic<bool> _hndlHasDone = false;
	
#ifdef CBL_MPF_OBJECTS_COUNTER_CHECKER_XPEER_CONNECTION_STATE
	crm::utility::__xobject_counter_logger_t<self_t,1> _xDbgCounter;
#endif

private:
	void __ctr();
	void invoke(async_operation_result_t, std::unique_ptr<dcf_exception_t> && exc, reconnect_setup_t rc)noexcept;
	std::unique_ptr< __i_rtx_out_connection_handler_t> invoker()noexcept;

	template<typename THandler>
	auto make_connection_functor_ptr(THandler && h) {
		return std::make_unique<__rtx_out_connection_functor_wrapper_t<THandler>>(std::forward<THandler>(h));
		}

public:
	~__rtx_out_connection_ctx_timered_t();

	template<typename THandshakeHandler>
	__rtx_out_connection_ctx_timered_t(std::weak_ptr<distributed_ctx_t> ctx_,
		std::unique_ptr<i_endpoint_t> && targetEP,
		THandshakeHandler && handshakeHndl_,
		local_object_create_connection_t && ctrLocalId_,
		remote_object_check_identity_t && checkerRemoteId_,
		std::weak_ptr<i_xpeer_t> wpp,
		const sndrcv_timeline_t & tml)
		: __rtx_connection_ctx_t(ctx_, std::move(ctrLocalId_), std::move(checkerRemoteId_), std::move(wpp), tml)
		, _targetEP(std::move(targetEP))
		, _handshakeHndl(make_connection_functor_ptr(std::forward<THandshakeHandler>(handshakeHndl_))) {
		CBL_VERIFY(_handshakeHndl);
		}

	template<typename THandshakeHandler>
	__rtx_out_connection_ctx_timered_t(std::weak_ptr<distributed_ctx_t> ctx_,
		std::unique_ptr<i_endpoint_t> && targetEP,
		THandshakeHandler && handshakeHndl_,
		local_object_create_connection_t && ctrLocalId_,
		remote_object_check_identity_t && checkerRemoteId_,
		std::weak_ptr<i_xpeer_t> wpp)
		: __rtx_connection_ctx_t(ctx_, std::move(ctrLocalId_), std::move(checkerRemoteId_), std::move(wpp))
		, _targetEP(std::move(targetEP))
		, _handshakeHndl(make_connection_functor_ptr(std::forward<THandshakeHandler>(handshakeHndl_))) {
		CBL_VERIFY(_handshakeHndl);
		}

	void invoke_ready()noexcept;
	void invoke_error(async_operation_result_t, std::unique_ptr<dcf_exception_t> && exc)noexcept final;
	void invoke_error(std::unique_ptr<dcf_exception_t> && exc)noexcept final;
	void invoke_error(std::unique_ptr<dcf_exception_t> && exc, reconnect_setup_t rc)noexcept;
	void close()noexcept final;

	template<typename THandshakeHandler>
	static std::shared_ptr<__rtx_out_connection_ctx_timered_t> launch(std::weak_ptr<distributed_ctx_t> ctx_,
		std::unique_ptr<i_endpoint_t> && targetEP,
		THandshakeHandler && handshakeHndl_,
		local_object_create_connection_t && ctrLocalId_,
		remote_object_check_identity_t && checkerRemoteId_,
		std::weak_ptr<i_xpeer_t> wpp) {

		auto obj = std::make_shared< __rtx_out_connection_ctx_timered_t>(
			ctx_,
			std::move(targetEP),
			std::forward<THandshakeHandler>(handshakeHndl_),
			std::move(ctrLocalId_),
			std::move(checkerRemoteId_),
			std::move(wpp));
		obj->__ctr();
		return obj;
		}

	template<typename THandshakeHandler>
	static std::shared_ptr<__rtx_out_connection_ctx_timered_t> launch(std::weak_ptr<distributed_ctx_t> ctx_,
		std::unique_ptr<i_endpoint_t> && targetEP,
		THandshakeHandler && handshakeHndl_,
		local_object_create_connection_t && ctrLocalId_,
		remote_object_check_identity_t && checkerRemoteId_,
		std::weak_ptr<i_xpeer_t> wpp,
		const sndrcv_timeline_t & tml) {

		auto obj = std::make_shared< __rtx_out_connection_ctx_timered_t>(
			ctx_,
			std::move(targetEP),
			std::forward<THandshakeHandler>(handshakeHndl_),
			std::move(ctrLocalId_),
			std::move(checkerRemoteId_),
			std::move(wpp),
			tml);
		obj->__ctr();
		return obj;
		}

	const std::unique_ptr<i_endpoint_t>& endpoint()const;
	};


struct __i_rtx_in_connection_handler_t {
	virtual ~__i_rtx_in_connection_handler_t() {}

	__i_rtx_in_connection_handler_t() {}

	typedef std::function<void(async_operation_result_t,
		std::unique_ptr<dcf_exception_t> && exc,
		syncdata_list_t && sl)> invoke_handler_t;

	virtual void invoke(async_operation_result_t,
		std::unique_ptr<dcf_exception_t> && exc,
		syncdata_list_t && sl)noexcept = 0;

	__i_rtx_in_connection_handler_t(const __i_rtx_in_connection_handler_t &) = delete;
	__i_rtx_in_connection_handler_t& operator=(const __i_rtx_in_connection_handler_t &) = delete;

	__i_rtx_in_connection_handler_t(__i_rtx_in_connection_handler_t &&) = delete;
	__i_rtx_in_connection_handler_t& operator=(__i_rtx_in_connection_handler_t &&) = delete;
	};


template<typename _THandler,
	typename THandler = typename std::decay_t<_THandler>>
struct __rtx_in_connection_functor_wrapper_t final : __i_rtx_in_connection_handler_t {
	THandler _ih;

	void invoke(async_operation_result_t r,
		std::unique_ptr<dcf_exception_t> && exc,
		syncdata_list_t && sl)noexcept final {

		_ih(r, std::move(exc), std::move(sl));
		}

	__rtx_in_connection_functor_wrapper_t(THandler && h)
		: _ih(std::move(h)) {}
	};


class __rtx_in_connection_ctx_timered_t :public __rtx_connection_ctx_t{
	typedef __rtx_in_connection_ctx_timered_t self_t;
	using base_type = __rtx_connection_ctx_t;

private:
	std::unique_ptr< __i_rtx_in_connection_handler_t> _handshakeHndl;
	std::atomic<bool> _hndlHasDone = false;
	/*! ������� �������� ����������������� ������ ���� */
	local_object_create_connection_t _localIdCtr;
	/*! ������� �������� ����������������� ������ ��������� ���� */
	remote_object_check_identity_t _remoteIdChecker;

#ifdef CBL_MPF_OBJECTS_COUNTER_CHECKER_XPEER_CONNECTION_STATE
	crm::utility::__xobject_counter_logger_t<self_t,1> _xDbgCounter;
#endif

	void invoke(async_operation_result_t ar, std::unique_ptr<dcf_exception_t> && exc, syncdata_list_t &&)noexcept;
	std::unique_ptr< __i_rtx_in_connection_handler_t> invoker()noexcept;

	template<typename THandler>
	auto make_in_connection_functor_ptr(THandler && h) {
		return std::make_unique < __rtx_in_connection_functor_wrapper_t<THandler>>(std::forward<THandler>(h));
		}

public:
	~__rtx_in_connection_ctx_timered_t();

	template<typename THandshakeHandler>
	__rtx_in_connection_ctx_timered_t(std::weak_ptr<distributed_ctx_t> ctx_,
		THandshakeHandler && handshakeHndl_,
		local_object_create_connection_t && ctrLocalId_,
		remote_object_check_identity_t && checkerRemoteId_,
		std::weak_ptr<i_xpeer_t> wpp,
		const sndrcv_timeline_t & tml)
		: __rtx_connection_ctx_t(ctx_, std::move(ctrLocalId_), std::move(checkerRemoteId_), std::move(wpp), tml)
		, _handshakeHndl(make_in_connection_functor_ptr(std::forward<THandshakeHandler>(handshakeHndl_)))
		, _localIdCtr(std::move(ctrLocalId_))
		, _remoteIdChecker(std::move(checkerRemoteId_)) {

		CBL_VERIFY(_handshakeHndl);
		}

	template<typename THandshakeHandler>
	__rtx_in_connection_ctx_timered_t(std::weak_ptr<distributed_ctx_t> ctx_,
		THandshakeHandler && handshakeHndl_,
		local_object_create_connection_t && ctrLocalId_,
		remote_object_check_identity_t && checkerRemoteId_,
		std::weak_ptr<i_xpeer_t> wpp)
		: __rtx_connection_ctx_t(ctx_, std::move(ctrLocalId_), std::move(checkerRemoteId_), std::move(wpp))
		, _handshakeHndl(make_in_connection_functor_ptr(std::forward<THandshakeHandler>(handshakeHndl_)))
		, _localIdCtr(std::move(ctrLocalId_))
		, _remoteIdChecker(std::move(checkerRemoteId_)) {

		CBL_VERIFY(_handshakeHndl);
		}

private:
	void __ctr();

public:
	void invoke_ready( syncdata_list_t && sl)noexcept;
	void invoke_error(std::unique_ptr<dcf_exception_t> && exc)noexcept final;
	void invoke_error(async_operation_result_t, std::unique_ptr<dcf_exception_t> && exc)noexcept final;

	void close()noexcept final;

	template<typename THandshakeHandler>
	static std::shared_ptr<__rtx_in_connection_ctx_timered_t> launch(std::weak_ptr<distributed_ctx_t> ctx_,
		THandshakeHandler && handshakeHndl_,
		local_object_create_connection_t && ctrLocalId_,
		remote_object_check_identity_t && checkerRemoteId_,
		std::weak_ptr<i_xpeer_t> wpp) {

		auto obj = std::make_shared<__rtx_in_connection_ctx_timered_t>(
			std::move(ctx_),
			std::forward<THandshakeHandler>(handshakeHndl_),
			std::move(ctrLocalId_),
			std::move(checkerRemoteId_),
			std::move(wpp));
		obj->__ctr();
		return obj;
		}

	template<typename THandshakeHandler>
	static std::shared_ptr<__rtx_in_connection_ctx_timered_t> launch(std::weak_ptr<distributed_ctx_t> ctx_,
		THandshakeHandler && handshakeHndl_,
		local_object_create_connection_t && ctrLocalId_,
		remote_object_check_identity_t && checkerRemoteId_,
		std::weak_ptr<i_xpeer_t> wpp,
		const sndrcv_timeline_t & tml) {

		auto obj = std::make_shared<__rtx_in_connection_ctx_timered_t>(
			std::move(ctx_),
			std::forward<THandshakeHandler>(handshakeHndl_),
			std::move(ctrLocalId_),
			std::move(checkerRemoteId_),
			std::move(wpp),
			tml);
		obj->__ctr();
		return obj;
		}
	};

class __handshake_state_t {
	typedef std::function<void(async_operation_result_t result)> result_hndl_f;

private:
	std::shared_ptr<__rtx_connection_ctx_t> _pstate;
	std::atomic<bool> _isRemoteTuched = false;

protected:
	std::shared_ptr<__rtx_connection_ctx_t> pstate()const noexcept;

public:
	__handshake_state_t()noexcept;
	__handshake_state_t(std::shared_ptr<__rtx_connection_ctx_t> && state)noexcept;

	__handshake_state_t( const __handshake_state_t & ) = delete;
	__handshake_state_t& operator=(const __handshake_state_t &) = delete;

	__handshake_state_t( __handshake_state_t && )noexcept;
	__handshake_state_t& operator=(__handshake_state_t &&)noexcept;

	virtual ~__handshake_state_t();
	void close()noexcept;

	bool remote_id_validate( const identity_value_t & id, std::unique_ptr<i_peer_remote_properties_t> & remoteProp );
	remote_object_connection_value_t local_id_ctr();
	bool empty()const noexcept;
	bool set_remote_tuched()noexcept;

	void invoke_error(std::unique_ptr<dcf_exception_t> && exc)noexcept;
	void invoke_abandoned()noexcept;
	void commit()noexcept;
	};

class __handshake_in_state_t final : public __handshake_state_t {
#ifdef CBL_MPF_OBJECTS_COUNTER_CHECKER_XPEER_CONNECTION_STATE
	crm::utility::__xobject_counter_logger_t<__handshake_in_state_t, 1> _xDbgCounter;
#endif

public:
	__handshake_in_state_t()noexcept;
	__handshake_in_state_t(std::shared_ptr<__rtx_in_connection_ctx_timered_t> && state)noexcept;

	__handshake_in_state_t(const __handshake_in_state_t &) = delete;
	__handshake_in_state_t& operator=(const __handshake_in_state_t &) = delete;

	void invoke_state_setup( syncdata_list_t && sl )noexcept;
	};

class __handshake_out_state_t final : public __handshake_state_t {
#ifdef CBL_MPF_OBJECTS_COUNTER_CHECKER_XPEER_CONNECTION_STATE
	crm::utility::__xobject_counter_logger_t<__handshake_out_state_t, 1> _xDbgCounter;
#endif

public:
	__handshake_out_state_t();
	__handshake_out_state_t(std::shared_ptr<__rtx_out_connection_ctx_timered_t> && state);

	__handshake_out_state_t(const __handshake_out_state_t &) = delete;
	__handshake_out_state_t& operator=(const __handshake_out_state_t &) = delete;

	void invoke_state_setup()noexcept;
	};

class __initialize_state_t {
private:
	std::shared_ptr<__rtx_connection_ctx_t> _pstate;
	object_initialize_handler_t _initializeHndl;
	std::atomic<bool> _invokef{ false };

#ifdef CBL_MPF_OBJECTS_COUNTER_CHECKER_XPEER_CONNECTION_STATE
	crm::utility::__xobject_counter_logger_t<__initialize_state_t,1> _xDbgCounter;
#endif

public:
	virtual ~__initialize_state_t();

	__initialize_state_t()noexcept;
	__initialize_state_t(std::shared_ptr<__rtx_connection_ctx_t> wctx,
		object_initialize_handler_t && initializeHndl)noexcept;

	void invoke( crm::datagram_t && dt, object_initialize_result_guard_t && resultHndl );
	bool empty()const;
	void error(std::unique_ptr<dcf_exception_t> && exc)noexcept;
	std::shared_ptr< __rtx_connection_ctx_t> cnx()&& noexcept;
	std::shared_ptr< __rtx_connection_ctx_t> cnx()const & noexcept;
	};


template<typename _TPeer,
	typename TPeer = typename std::decay_t<_TPeer>>
struct indentity_tail {
	using link_t = typename TPeer::link_t;

	typedef std::function<void(async_operation_result_t result,
		std::unique_ptr<dcf_exception_t> && exc,
		reconnect_setup_t rc,
		std::unique_ptr<__outbound_connection_result_t<link_t>> && pHndl)> tail_functor;

	std::unique_ptr<__outbound_connection_result_t<link_t>> _pHndl;
	tail_functor _f;

	indentity_tail()noexcept {}

	~indentity_tail() {
		invoke(async_operation_result_t::st_abandoned, nullptr, reconnect_setup_t::yes);
		}

	indentity_tail(tail_functor && f,
		std::unique_ptr<__outbound_connection_result_t<link_t>> && pHndl)noexcept
		: _pHndl(std::move(pHndl))
		, _f(std::move(f)){}

	indentity_tail(indentity_tail && o)noexcept
		: _pHndl(std::move(o._pHndl))
		, _f(std::move(o._f)){}

	indentity_tail(const indentity_tail &) = delete;
	indentity_tail& operator=(const indentity_tail &) = delete;

	void invoke(async_operation_result_t result,
		std::unique_ptr<dcf_exception_t> && exc,
		detail::reconnect_setup_t rc)noexcept {

		decltype(_f) f = std::move(_f);
		CBL_VERIFY(!_f);

		if(f) {
			if(async_operation_result_t::st_abandoned == result) {
				if(!exc) {
					exc = CREATE_MPF_PTR_EXC_FWD(nullptr);
					}
				}

			f(result, std::move(exc), rc, std::move(_pHndl));
			}
		}

	void operator()(async_operation_result_t result,
		std::unique_ptr<dcf_exception_t> && exc,
		detail::reconnect_setup_t rc)noexcept {
		
		invoke(result, std::move(exc), rc);
		}
	};
}


