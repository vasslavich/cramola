#pragma once


#include "../channel_terms/subscribe_terms.h"
#include "../queues/i_message_queue.h"
#include "./base_terms.h"
#include "./xwlink.h"
#include "../messages/internal_terms.h"
#include "../channel_terms/i_stream.h"
#include "../channel_terms/events_terms.h"


namespace crm{

class ipeer_t;
class opeer_t;

namespace detail{


struct i_xpeer_rt1_base_t : public i_xpeer_t{
	static const subscriber_node_id_t subscribe_address_datagram_for_this;
	static const std::string command_name_identity;
	static const std::string command_name_initialize;

	using rdm_local_identity_t = i_x1_stream_address_handler::rdm_local_identity_t;
	using remote_object_endpoint_t = i_x1_stream_address_handler::remote_object_endpoint_t;
	using xpeer_desc_t = i_x1_stream_address_handler::xpeer_desc_t;
	using input_value_t = i_x1_stream_address_handler::input_value_t;
	using ctr_1_input_queue_t = i_x1_stream_address_handler::ctr_1_input_queue_t;

	/*! ������� ��������� � ���������� �������� ��������� */
/*	using subscribe_handler_t = subscribe_handler_callway_t<input_value_t>;*/

	/*! ������� ��������� ������� ���������� ������� */
	typedef std::function<void( const rdm_local_identity_t &,
		closing_reason_t,
		std::shared_ptr<i_peer_statevalue_t> && )> rtx_object_closed_t;

	virtual const xpeer_desc_t & desc()const noexcept = 0;

	/*! �������� �� ��������� ��������� ��� ���������� ��������������.
	\param id ������� �������������
	\param once ���� ������� true, ����������� �������� ���� ���, ����� �� ��� ���,
	���� �� ����� ��������� ������ ����������� � ������� ������ /ref unsubscribe_hndl_id.
	*/
	virtual subscribe_invoker_result subscribe_hndl_id( async_space_t scx,
		const _address_hndl_t & id,
		subscribe_options opt,
		subscribe_message_handler && handler ) = 0;

	/*! ����� �� ��������� ����������� ��� ���������� ��������������
	\param id ������������� ����������
	*/
	virtual void unsubscribe_hndl_id( const _address_hndl_t & id )noexcept = 0;

	virtual void subscribe_event( events_list && el ) = 0;

	virtual subscribe_result subscribe_event( async_space_t scx,
		const _address_event_t & id,
		bool once,
		__event_handler_f && handler ) = 0;

	virtual void unsubscribe_event( const _address_event_t & id )noexcept = 0;
	};


struct i_xpeer_rt1_t : public i_xpeer_rt1_base_t{

	typedef i_message_input_t<input_value_t> input_stack_t;
	typedef input_stack_t::value_type input_value_t;
	typedef i_message_output_t<std::unique_ptr<i_iol_ohandler_t>> output_stack_t;
	typedef output_stack_t::value_type output_value_t;
	typedef input_stack_t::value_type subscribe_hndl_value_t;
	typedef remote_object_state_t state_t;


	typedef std::function<void( ipeer_t peer, input_value_t && )> input_handler_t;
	typedef std::function<void( input_value_t && )> input_handler_opeer_t;

	/*! ������� ��������� ������� ���������� ������� */
	typedef std::function<void( const rdm_local_identity_t &,
		closing_reason_t,
		std::shared_ptr<i_peer_statevalue_t> && )> rtx_object_closed_t;

	/*! ����������� ������� ��������� ���������� �������� ����������� ���� � ��������� ���� � ������� */
	typedef std::function<void()> register_node_completed_t;

	/*! ������� ��������� ������� ���������� ������� */
	typedef std::function<void( const rdm_local_identity_t &,
		closing_reason_t,
		std::shared_ptr<i_peer_statevalue_t> && )> rtx_object_closed_t;

	virtual const session_description_t & session()const noexcept = 0;
	virtual const rtl_roadmap_obj_t& this_rdm()const noexcept = 0;
	virtual const rtl_roadmap_obj_t& remote_rdm()const noexcept = 0;
	virtual std::shared_ptr<const base_stream_x_rt1_t> source_stream()const noexcept = 0;
	virtual std::shared_ptr<base_stream_x_rt1_t> source_stream()noexcept = 0;

	virtual void write( outbound_message_unit_t && blob ) = 0;

	virtual std::shared_ptr<i_x1_stream_address_handler> address_handler()const noexcept = 0;
	};
}
}
