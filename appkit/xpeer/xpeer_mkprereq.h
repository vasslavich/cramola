#pragma once


#include "./base_terms.h"
#include "../channel_terms/base_terms.h"
#include "../tableval/tableval.h"
#include "../protocol/i_encoding.h"


namespace crm{


class remote_object_connection_value_t final : public srlz::serialized_base_definitions {
private:
	identity_value_t _identity;
	syncdata_list_t _syncdata;

#ifdef CBL_MPF_OBJECTS_COUNTER_CHECKER_XPEER_CONNECTION_STATE
	crm::utility::__xobject_counter_logger_t<remote_object_connection_value_t> _xDbgCounter;
#endif

public:
	template<typename S>
	void srlz(S &&dest)const {
		crm::srlz::serialize(dest, _identity);
		crm::srlz::serialize(dest, _syncdata);
	}

	template<typename S>
	void dsrlz(S &&src ) {
		crm::srlz::deserialize(src, _identity);
		crm::srlz::deserialize(src, _syncdata);
	}

public:
	remote_object_connection_value_t();
	remote_object_connection_value_t( identity_value_t && identity_, syncdata_list_t && sl );

	remote_object_connection_value_t( const remote_object_connection_value_t & ) = delete;
	remote_object_connection_value_t& operator=( const remote_object_connection_value_t & ) = delete;

	remote_object_connection_value_t( remote_object_connection_value_t  && );
	remote_object_connection_value_t& operator=( remote_object_connection_value_t && );

	const identity_value_t& identity()const;
	identity_value_t identity_release();

	const syncdata_list_t& syncdata()const;
	syncdata_list_t syncdata_release();
	};

struct base_peer_identity_properties_t final : srlz::serialized_base_definitions{


	template<typename S>
	void srlz(S &&)const {}

	template<typename S>
	void dsrlz(S &&) {}

	binary_vector_t srlz_()const;
	static base_peer_identity_properties_t dsrlz_( const binary_vector_t & bin );
	};

/*! ������� ������������� ���������� ������� */
typedef std::function<datagram_t( syncdata_list_t && sl )> create_remote_object_initialize_list_t;

/*! ������� �������� ����������������� ������ ��������� ���� */
typedef std::function<bool( const identity_value_t & id,
	std::unique_ptr<i_peer_remote_properties_t> & remoteProp )> remote_object_check_identity_t;

/*! ������� �������� ��������� ����������������� ������ */
typedef std::function<remote_object_connection_value_t()> local_object_create_connection_t;


}


