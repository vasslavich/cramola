#pragma once


#include <functional>
#include <chrono>
#include <atomic>
#include <memory>
#include "../internal_invariants.h"
#include "../notifications/internal_terms.h"
#include "../time/terms.h"


namespace crm::detail{


class ping_watcher_t{
public:
	typedef std::function<void( std::unique_ptr<crm::dcf_exception_t> && notify )> notification_handler_t;
	typedef std::function<void()> sendout_t;

private:
	typedef decltype(std::declval<std::chrono::milliseconds>().count()) milliseconds_type_t;

	sendout_t _pingOut;
	sndrcv_timeline_periodicity_t _timeline;
	std::atomic<milliseconds_type_t> _acceptLastTime;
	std::atomic<milliseconds_type_t> _emitLastTime;
	std::atomic<bool> _started{ false };
	std::atomic<bool> _cancel{ false };

#ifdef CBL_MPF_OBJECTS_COUNTER_CHECKER_PINGWATHCER
	crm::utility::__xobject_counter_logger_t<ping_watcher_t, 1> _xDbgCounter;
#endif

	static milliseconds_type_t curr_time_count();

public:
	ping_watcher_t( const ping_watcher_t & ) = delete;
	ping_watcher_t& operator=( const ping_watcher_t& ) = delete;

	ping_watcher_t( const std::chrono::milliseconds & timeout,
		const std::chrono::milliseconds & interval,
		sendout_t && pingOut );

	ping_watcher_t( const sndrcv_timeline_periodicity_t & tm,
		sendout_t && pingOut );

	bool started()const noexcept;
	bool canceled()const noexcept;
	void start();
	void stop()noexcept;
	void add_emit()noexcept;
	void add_accept()noexcept;
	void check();
	bool expired()const noexcept;

	bool accept_timeout_exceeded()const noexcept;
	bool emit_timeout_exceeded()const noexcept;

	bool accepted_interval_exceeded()const noexcept;
	bool emit_interval_exceeded()const noexcept;

	const std::chrono::milliseconds& timeout()const noexcept;
	const std::chrono::milliseconds& interval()const noexcept;

	std::chrono::milliseconds accept_last_interval()const noexcept;
	std::chrono::milliseconds emit_last_interval()const noexcept;

	milliseconds_type_t accept_last_time_count()const noexcept;
	milliseconds_type_t emit_last_time_count()const noexcept;
	milliseconds_type_t accept_curr_delta()const noexcept;
	milliseconds_type_t emit_curr_delta()const noexcept;
	};
}
