#pragma once


#include <memory>
#include <atomic>
#include <chrono>
#include "./rt1_xpeer.h"
#include "./base_predecl.h"
#include "./rtx_cnctx.h"


namespace crm ::detail{


/*! �������� ����� ��������� ����������� �� ��������� ���� */
class xpeer_in_rt1_t  final :
	public xpeer_base_rt1_t{

public:
	typedef xpeer_in_rt1_t self_t;
	typedef xpeer_base_rt1_t base_t;
	typedef std::function<void(async_operation_result_t result)> connection_result_t;

	typedef std::function<void( std::unique_ptr<dcf_exception_t> && exc, 
		link_t && peer,
		syncdata_list_t && sl,
		std::unique_ptr<execution_bool_tail> && registerCompleted )> identification_success_f;

private:
	std::shared_ptr<detail::__handshake_in_state_t> _handshakeSt;

	std::shared_ptr<detail::__handshake_in_state_t> handshake_state_capture() noexcept;
	void handshake_state_register(std::shared_ptr<detail::__handshake_in_state_t> && st);
	/*! ������� ��������� �������� ����������������� ������ �� �������, ��������� �������� */
	void on_handshake_success();

	std::unique_ptr<execution_bool_tail> make_register_completed();

	void command_handler_dtg_identity( std::unique_ptr<isendrecv_datagram_t> && iDtg)final;
	void command_handler_dgt_initialize( std::unique_ptr<isendrecv_datagram_t> && iDgt)final;

public:
	/* ������������� ���������� �������-����������� */
	xpeer_in_rt1_t( const std::weak_ptr<distributed_ctx_t> & ctx,
		std::shared_ptr<detail::ix_rt1_stream_t> && source_,
		std::unique_ptr<gtw_rt1_x_t> && gtw,
		rtx_object_closed_t && closingHndl,
		std::shared_ptr<i_peer_statevalue_t> && stval,
		__event_handler_point_f && eh );

private:
	void register_completed(async_operation_result_t result);
	std::shared_ptr<detail::ix_rt1_stream_t> isource() noexcept;

	void start_handshake(local_object_create_connection_t && localIdCtr,
		remote_object_check_identity_t && remoteIdChecker,
		ctr_1_input_queue_t && ctrInput,
		identification_success_f && hndl);

	static void accept( std::shared_ptr<self_t> && obj,
		local_object_create_connection_t && localIdCtr,
		remote_object_check_identity_t && remoteIdChecker,
		ctr_1_input_queue_t && ctrInput,
		identification_success_f && connectionHndl );

public:
	~xpeer_in_rt1_t();

	template<typename TEventHandler>
	static void accept2stack(const std::weak_ptr<distributed_ctx_t> & ctx,
		std::shared_ptr<detail::ix_rt1_stream_t> && source_,
		std::unique_ptr<gtw_rt1_x_t> && gtw,
		rtx_object_closed_t && closingHndl,
		local_object_create_connection_t && localIdCtr,
		remote_object_check_identity_t && remoteIdChecker,
		ctr_1_input_queue_t && ctrInput,
		identification_success_f && connectionHndl,
		std::shared_ptr<i_peer_statevalue_t> && stval,
		TEventHandler && eh,
		async_space_t scx ) {

		/* ������ */
		auto obj = std::make_shared<xpeer_in_rt1_t>(ctx,
			std::move(source_),
			std::move(gtw),
			std::move(closingHndl),
			std::move(stval),
			__event_handler_point_f(std::forward<TEventHandler>(eh), ctx, scx ));

		MPF_TEST_EMULATION_IN_CONTEXT_EXPR_COND(obj->ctx(), 317, is_trait_at_emulation_context(obj->level()));

		accept(std::move(obj),
			std::move(localIdCtr),
			std::move(remoteIdChecker),
			std::move(ctrInput),
			std::move(connectionHndl));
		}

	static std::unique_ptr< input_stack_t> make_input_stack(std::weak_ptr<self_t>, input_handler_t &&);

	template<typename TEventHandler>
	static void accept2handler(const std::weak_ptr<distributed_ctx_t> & ctx,
		std::shared_ptr<detail::ix_rt1_stream_t> && source_,
		std::unique_ptr<gtw_rt1_x_t> && gtw,
		rtx_object_closed_t && closingHndl,
		local_object_create_connection_t && localIdCtr,
		remote_object_check_identity_t && remoteIdChecker,
		input_handler_t && hndlInput,
		identification_success_f && connectionHndl,
		std::shared_ptr<i_peer_statevalue_t> && stval,
		TEventHandler && eh,
		async_space_t scx) {
		
		/* ������ */
		auto obj = std::make_shared<xpeer_in_rt1_t>(ctx,
			std::move(source_),
			std::move(gtw),
			std::move(closingHndl),
			std::move(stval),
			__event_handler_point_f(std::forward<TEventHandler>(eh), ctx, scx));

		/* ���������� �������� ��������� */
		auto ctrInput = [wptr = std::weak_ptr(obj), hndlInput]()mutable {
			return make_input_stack(wptr, std::move(hndlInput));
			};

		MPF_TEST_EMULATION_IN_CONTEXT_EXPR_COND(obj->ctx(), 318, is_trait_at_emulation_context(obj->level()));

		accept(std::move(obj),
			std::move(localIdCtr),
			std::move(remoteIdChecker),
			std::move(ctrInput),
			std::move(connectionHndl));
		}

	template<typename TEventHandler>
	static void accept2i(const std::weak_ptr<distributed_ctx_t> & ctx,
		std::shared_ptr<detail::ix_rt1_stream_t> && source_,
		std::unique_ptr<gtw_rt1_x_t> && gtw,
		rtx_object_closed_t && closingHndl,
		local_object_create_connection_t && localIdCtr,
		remote_object_check_identity_t && remoteIdChecker,
		std::unique_ptr<i_input_messages_stock_t> && hndlInput,
		identification_success_f && connectionHndl,
		std::shared_ptr<i_peer_statevalue_t> && stval,
		TEventHandler && eh,
		async_space_t scx) {

		MPF_TEST_EMULATION_IN_CONTEXT_EXPR_COND(CHECK_PTR(ctx), 319, is_trait_at_emulation_context(rtl_level_t::l1));

		/* ����������� �� ���� �������� �������� �������� ���������
		------------------------------------------------------------------*/
		if(auto ictr2Stack = dynamic_cast<input2stack_t*>(hndlInput.get())) {

			/* �������� ������� ���� */
			accept2stack(ctx,
				std::dynamic_pointer_cast<ix_rt1_stream_t>(source_),
				std::move(gtw),
				std::move(closingHndl),
				std::move(localIdCtr),
				std::move(remoteIdChecker),
				std::move(ictr2Stack->ictr),
				std::move(connectionHndl),
				std::move(stval),
				std::forward<TEventHandler>(eh),
				scx );
			}

		else if(auto ictr2Hndl = dynamic_cast<hub_input2handler_rt1_t*>(hndlInput.get())) {

			/* ����������� ����������� */
			accept2handler(ctx,
				std::dynamic_pointer_cast<ix_rt1_stream_t>(source_),
				std::move(gtw),
				std::move(closingHndl),
				std::move(localIdCtr),
				std::move(remoteIdChecker),
				std::move(ictr2Hndl->ihndl),
				std::move(connectionHndl),
				std::move(stval),
				std::forward<TEventHandler>(eh),
				scx );
			}
		else {
			FATAL_ERROR_FWD(nullptr);
			}
		}

	xpeer_in_rt1_t(const xpeer_in_rt1_t &) = delete;
	xpeer_in_rt1_t& operator=(const xpeer_in_rt1_t &) = delete;

	xpeer_in_rt1_t(xpeer_in_rt1_t && o) = delete;
	xpeer_in_rt1_t& operator=(xpeer_in_rt1_t &&o) = delete;

	using base_t::tag;
	using base_t::set_tag;
	};
}


