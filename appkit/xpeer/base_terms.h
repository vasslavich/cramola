#pragma once


#include "../channel_terms/terms.h"
#include "./address_prop_trait.h"


namespace crm {


template<typename T, typename Enable = void>
struct is_xpeer_push_trait_z_t : public std::false_type {};

template<typename T>
struct is_xpeer_push_trait_z_t < T,
	std::void_t<
	decltype(std::declval<std::decay_t<T>>().push(std::declval<onotification_t>()))>>
	: std::true_type{};


template<typename T>
constexpr bool is_xpeer_push_trait_z_v = is_xpeer_push_trait_z_t<T>::value;


struct connection_stage_descriptions_t {
	static const std::string identity_user_options;
	};


struct remote_connection_desc_t {
	identity_descriptor_t thisId;
	identity_descriptor_t remoteId;
	connection_key_t cnk;
	};


struct i_peer_statevalue_t {
	virtual ~i_peer_statevalue_t() = 0;

	virtual void close()noexcept = 0;

private:
#ifdef CBL_MPF_OBJECTS_COUNTER_CHECKER_XPEER_STATE_VALUE
	crm::utility::__xobject_counter_logger_t<i_peer_statevalue_t> _xDbgCounter;
#endif
	};

/*! ������� ��������� ������� ��������� ������� */
struct i_peer_remote_properties_t {
	virtual ~i_peer_remote_properties_t() = 0;
	};

struct i_xpeer_t {
	virtual ~i_xpeer_t() = 0;

	typedef rt1_endpoint_t remote_object_endpoint_t;

	static const std::string identity_properties_id_desc;
	static const std::string identity_properties_user_properties;

	virtual remote_object_state_t state()const noexcept = 0;
	virtual void close()noexcept = 0;
	virtual bool closed()const noexcept = 0;
	virtual bool is_opened()const noexcept = 0;
	virtual detail::rtl_level_t level()const noexcept = 0;
	virtual const std::string& address()const noexcept = 0;
	virtual detail::rtl_table_t direction()const noexcept = 0;
	virtual int port()const noexcept = 0;
	virtual const identity_descriptor_t& id()const noexcept = 0;
	virtual std::shared_ptr<i_peer_statevalue_t> state_value()noexcept = 0;
	virtual std::shared_ptr<const i_peer_statevalue_t> state_value()const noexcept = 0;
	virtual void set_state_value(std::shared_ptr<i_peer_statevalue_t> && stv) noexcept = 0;
	virtual std::shared_ptr<const i_peer_remote_properties_t> remote_properties()const noexcept = 0;
	virtual connection_key_t connection_key()const noexcept = 0;
	virtual std::shared_ptr<distributed_ctx_t> ctx()const noexcept = 0;
	virtual const identity_descriptor_t& this_id()const noexcept = 0;
	virtual const identity_descriptor_t& remote_id()const noexcept = 0;
	virtual const local_object_id_t& local_id()const noexcept = 0;
	virtual bool renewable_connection()const noexcept = 0;
	virtual remote_object_endpoint_t remote_object_endpoint()const noexcept = 0;
	virtual const std::string& name()const noexcept = 0;
	virtual detail::peer_commands_result_t execute_command(peer_command_t cmd) = 0;
	virtual void invoke_event(event_type_t)noexcept = 0;
	};


namespace detail {


struct xpeer_push_handler_base {
	virtual ~xpeer_push_handler_base() {}
	virtual rtl_level_t peer_level()const noexcept = 0;
	virtual local_object_id_t local_id()const noexcept = 0;
	};


struct xpeer_push_handler_pin {
	std::unique_ptr< xpeer_push_handler_base> p;

	template<typename O,
		typename std::enable_if_t < is_out_message_requirements_v<O>, int> = 0>
		void operator()(O && o)const {
		if (p) {
			auto link = p.get_link();
			link.push(std::forward<O>(o));
			}
		}

	local_object_id_t local_id()const noexcept;
	};
}
}
