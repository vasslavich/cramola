#pragma once


#include "./rt0_opeer.h"
#include "./outbound_xconnector.h"


namespace crm ::detail {


struct xpeer_out_connector_rt0_t  final : public xpeer_out_connector_t<xpeer_out_rt0_t> {
public:
	typedef xpeer_out_rt0_t peer_t;
	typedef peer_t::remote_object_endpoint_t remote_object_endpoint_t;

private:
	std::shared_ptr<default_binary_protocol_handler_t> _ctrGateway;
	xpeer_base_rt0_t::ctr_0_input_queue_t _ctrInput;
	object_initialize_handler_t _outXInit;
	std::unique_ptr<i_endpoint_t> _endpoint;
	post_connection_t _postConnectionEvent;
	std::shared_ptr<i_xpeer_source_factory_t> _xsf;
	identity_descriptor_t _thisId;
	std::shared_ptr<xpeer_cop_handler> _coph;
	__event_handler_point_f _eh;

	std::function<std::shared_ptr<i_peer_statevalue_t>()> _stvalCtr;
	exceptions_suppress_checker_t _remoteExcChecker;

public:
	xpeer_out_connector_rt0_t(const xpeer_out_connector_rt0_t &) = delete;
	xpeer_out_connector_rt0_t& operator=(const xpeer_out_connector_rt0_t &) = delete;

	xpeer_out_connector_rt0_t(std::shared_ptr<default_binary_protocol_handler_t> ctrGateway,
		xpeer_base_rt0_t::ctr_0_input_queue_t && ctrInput,
		object_initialize_handler_t && outXInit,
		std::shared_ptr<i_xpeer_source_factory_t> xsf,
		post_connection_t && postConnectionEvent_,
		const identity_descriptor_t & thisId_,
		std::unique_ptr<i_endpoint_t> && endpoint,
		std::function<std::shared_ptr<i_peer_statevalue_t>()> && stvalCtr,
		__event_handler_point_f && eh,
		exceptions_suppress_checker_t && remoteExcChecker );

private:
	std::unique_ptr<i_endpoint_t> target_endpoint()const override;
	const identity_descriptor_t& this_id()const noexcept;
	void event_post_connection( std::unique_ptr<dcf_exception_t> && exc, peer_link_t && peer ) override;
	post_connection_t get_event_post_connection()const override;
	remote_object_endpoint_t remote_object_endpoint()const final;
	bool check_notify(const std::unique_ptr<dcf_exception_t>&)noexcept final;

	void connect(std::weak_ptr<distributed_ctx_t> ctx,
		peer_t::rtx_object_closed_t && closeHndl,
		connection_hndl_t && resultHndl,
		local_object_create_connection_t && identityCtr,
		remote_object_check_identity_t && identityChecker )override;
	};
}



