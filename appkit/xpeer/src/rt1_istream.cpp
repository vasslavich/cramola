#include "../../internal.h"
#include "../../streams/istreams_routetbl.h"
#include "../../context/context.h"
#include "../rt1_istream.h"
#include "../../logging/keycounter.h"
#include "../../cmdrt/terms.h"
#include "../../channel_terms/xtract.h"


using namespace crm;
using namespace crm::detail;


std::shared_ptr<ix_rt1_stream_t> ix_rt1_stream_t::create(std::weak_ptr<distributed_ctx_t> ctx_,
	std::weak_ptr < i_rt1_main_router_t> outs,
	const rt0_node_rdm_t & nodeId,
	const identity_descriptor_t & thisId_,
	const identity_descriptor_t & remoteId_,
	const session_description_t & session) {

	std::shared_ptr<ix_rt1_stream_t> obj(new ix_rt1_stream_t(
		ctx_, outs, nodeId, thisId_, remoteId_, session));
	return std::move(obj);
	}

std::shared_ptr<i_rt1_main_router_t> ix_rt1_stream_t::router()const noexcept {
	return _xbase.router();
	}

std::shared_ptr<i_x1_stream_address_handler> ix_rt1_stream_t::address_point()const noexcept {
	return _addrh;
	}

std::shared_ptr<i_decode_tail_stream2peer_t_x1> ix_rt1_stream_t::reset_tail()noexcept {
	return std::atomic_exchange(&_tail, {});
	}

bool ix_rt1_stream_t::set_tail(std::shared_ptr<i_decode_tail_stream2peer_t_x1> && tail_) noexcept {
	if (!closed()) {
		if (auto oldp = std::atomic_exchange(&_tail, tail_)) {
			oldp->close();
			}

		return true;
		}
	else {
		return false;
		}
	}

std::shared_ptr<i_decode_tail_stream2peer_t_x1> ix_rt1_stream_t::tail()const  noexcept {
	return std::atomic_load_explicit(&_tail, std::memory_order::memory_order_acquire);
	}

void ix_rt1_stream_t::set_state(remote_object_state_t st)  noexcept {
	CBL_VERIFY(_addrh);
	_addrh->set_state(st);
	}

remote_object_state_t ix_rt1_stream_t::get_state()const  noexcept {
	CBL_VERIFY(_addrh);
	return _addrh->state();
	}

void ix_rt1_stream_t::push_to_x(outbound_message_unit_t && buf) {
#pragma message( CBL_VAR_NAME_VALUE(CBL_BUILD_VAR_MESSAGE_ROADMAP_MARKER_OUTBOUND, 4))

	CBL_MPF_KEYTRACE_SET(buf, 16);

	MPF_TEST_EMULATION_IN_CONTEXT_COND_TRCX(_xbase.ctx(), 404, 
		is_trait_at_emulation_context(buf),
		trait_at_keytrace_set(buf));

	/* �� ��������� �������� closed(), �����, ������ ���� �� ������
	��������� �������� ��������� �� ����������
	---------------------------------------------------------------*/

	auto och(_xbase.router());
	if(och) {

		CBL_VERIFY((remote_rdm().table() == this_rdm().table()
			&& remote_rdm().target_id() == this_rdm().source_id()
			&& remote_rdm().source_id() == this_rdm().target_id()
			&& remote_rdm().rt0() == this_rdm().rt0()));

		rtl_roadmap_line_t rtl_;
		rtl_.points = msg_rdm();

		/* �������� �������������� ������ 1 */
		och->push2rt(rt1_x_message_t::make(std::move(rtl_), std::move(buf)));
		}
	else {
		close();
		THROW_MPF_EXC_FWD(make_trace_line());
		}
	}

rtl_table_t ix_rt1_stream_t::direction()const noexcept {
	CBL_VERIFY(_addrh);
	return _addrh->direction();
	}

const identity_descriptor_t& ix_rt1_stream_t::remote_id()const noexcept {
	static_assert(std::is_lvalue_reference_v<decltype(_addrh->remote_id())>, __FILE_LINE__);

	CBL_VERIFY(_addrh);
	return _addrh->remote_id();
	}

const identity_descriptor_t& ix_rt1_stream_t::this_id()const  noexcept {
	static_assert(std::is_lvalue_reference_v<decltype(_addrh->this_id())>, __FILE_LINE__);

	CBL_VERIFY(_addrh);
	return _addrh->this_id();
	}

const local_command_identity_t& ix_rt1_stream_t::local_object_command_id()const noexcept {
	static_assert(std::is_lvalue_reference_v<decltype(_addrh->local_object_command_id())>, __FILE_LINE__);

	CBL_VERIFY(_addrh);
	return _addrh->local_object_command_id();
	}


const rtl_roadmap_obj_t& ix_rt1_stream_t::this_rdm()const  noexcept {
	static_assert(std::is_lvalue_reference_v<decltype(_addrh->this_rdm())>, __FILE_LINE__);

	CBL_VERIFY(_addrh);
	return _addrh->this_rdm();
	}

const rtl_roadmap_obj_t& ix_rt1_stream_t::remote_rdm()const  noexcept {
	static_assert(std::is_lvalue_reference_v<decltype(_addrh->remote_rdm())>, __FILE_LINE__);

	CBL_VERIFY(_addrh);
	return _addrh->remote_rdm();
	}

const session_description_t& ix_rt1_stream_t::session()const noexcept {
	static_assert(std::is_lvalue_reference_v<decltype(_addrh->session())>, __FILE_LINE__);

	CBL_VERIFY(_addrh);
	return _addrh->session();
	}

const rt0_node_rdm_t& ix_rt1_stream_t::node_id()const  noexcept {
	static_assert(std::is_lvalue_reference_v<decltype(_addrh->node_id())>, __FILE_LINE__);

	CBL_VERIFY(_addrh);
	return _addrh->node_id();
	}

const local_object_id_t& ix_rt1_stream_t::local_id()const  noexcept {
	static_assert(std::is_lvalue_reference_v<decltype(_addrh->local_id())>, __FILE_LINE__);

	CBL_VERIFY(_addrh);
	return _addrh->local_id();
	}

const rtl_roadmap_obj_t& ix_rt1_stream_t::msg_rdm()const  noexcept {
	static_assert(std::is_lvalue_reference_v<decltype(_addrh->msg_rdm())>, __FILE_LINE__);

	CBL_VERIFY(_addrh);
	return _addrh->msg_rdm();
	}

const rtl_roadmap_link_t& ix_rt1_stream_t::msg_rdm_link()const  noexcept {
	static_assert(std::is_lvalue_reference_v<decltype(msg_rdm().link())>, __FILE_LINE__);

	return msg_rdm().link();
	}

const rtl_roadmap_event_t& ix_rt1_stream_t::ev_rdm()const  noexcept {
	static_assert(std::is_lvalue_reference_v<decltype(_addrh->ev_rdm())>, __FILE_LINE__);

	CBL_VERIFY(_addrh);
	return _addrh->ev_rdm();
	}

std::string ix_rt1_stream_t::make_trace_line()const noexcept {
	return "ix_rt1_stream_t:" + this_id().to_str() + ":" + remote_id().to_str();
	}

void ix_rt1_stream_t::close_objects()noexcept {
	close_tail();
	}

void ix_rt1_stream_t::close_tail()noexcept {
	if (auto tail = reset_tail()) {
		tail->close();
		}
	}

bool ix_rt1_stream_t::closed()const  noexcept {
	return _closef.load(std::memory_order::memory_order_acquire);
	}

void ix_rt1_stream_t::unbind_reslink() noexcept{
	if(auto c = _xbase.ctx()) {
		if(auto stb = c->streams_handler()) {
			CBL_VERIFY(_addrh);
			CHECK_NO_EXCEPTION(stb->close(_addrh->local_id()));
			}
		}
	}

void ix_rt1_stream_t::close() noexcept{
	close(unregister_message_track_reason_t::closed_by_self);
	}

void ix_rt1_stream_t::close(unregister_message_track_reason_t r) noexcept{
	bool clf = false;
	if (_closef.compare_exchange_strong(clf, true)) {

		/* ���������� ��������� ����������� ��������� ���������� �������� */
		unregister_routing(r);
		close_objects();

		CBL_MPF_TRACELEVEL_EVENT_RT1_CONNECTION_TRACE("inbound_connection:stream:close(0):s="
			+ this_rdm().remote_object_endpoint().to_string()
			+ ":t=" + this_id().to_str()
			+ ":session=" + session().object_hash().to_str());
		}
	}

void ix_rt1_stream_t::close_from_router()noexcept {
	bool clf = false;
	if(_closef.compare_exchange_strong(clf, true)) {

		close_objects();

		CBL_MPF_TRACELEVEL_EVENT_RT1_CONNECTION_TRACE("inbound_connection:stream:close(1):s="
			+ this_rdm().remote_object_endpoint().to_string()
			+ ":t=" + this_id().to_str()
			+ ":session=" + session().object_hash().to_str());
		}
	}

void ix_rt1_stream_t::unroute_command_link()noexcept {
	CHECK_NO_EXCEPTION(close_from_router());
	}

bool ix_rt1_stream_t::is_once()const  noexcept {
	return false;
	}

void ix_rt1_stream_t::register_routing(__error_handler_point_f && erh0) {
	auto rt(_xbase.router());
	if(rt) {
		auto scx = erh0.space();

		__error_handler_point_f errh2( 
			utility::bind_once_call({ "ix_rt1_stream_t::register_routing" }, 
				[sptr = shared_from_this()]( std::unique_ptr<dcf_exception_t> && exc, __error_handler_point_f  && rh0 )noexcept {
			if( !exc ){

				bool registered = false;
				if( !sptr->_rtRegistered.compare_exchange_strong( registered, true ) ){
					FATAL_ERROR_FWD(nullptr);
					}
				}
			else{
				CHECK_NO_EXCEPTION( sptr->close_from_router() );
				}

			if( sptr->closed() ){
				if( !exc ){
					exc = CREATE_MPF_PTR_EXC_FWD(nullptr);
					}
				}

			rh0( std::move( exc ) );
			}, std::placeholders::_1, std::move( erh0 ) ), _xbase.ctx(), scx );

		rt->register_messages_slot( msg_rdm(), weak_from_this(), std::move( errh2 ) );
		}
	else {
		THROW_MPF_EXC_FWD(nullptr);
		}
	}

void ix_rt1_stream_t::unregister_routing(unregister_message_track_reason_t r) noexcept{

	if(_rtRegistered.load(std::memory_order::memory_order_acquire)) {
		auto rt(_xbase.router());
		if(rt)
			rt->unregister_messages_slot(msg_rdm(), r);
		}
	}

bool ix_rt1_stream_t::is_integrity_provided()const noexcept {
	return true;
	}

ix_rt1_stream_t::~ix_rt1_stream_t() {
	CHECK_NO_EXCEPTION(unbind_reslink());
	CHECK_NO_EXCEPTION(close());
	}


ix_rt1_stream_t::ix_rt1_stream_t(std::weak_ptr<distributed_ctx_t> ctx_,
	std::weak_ptr < i_rt1_main_router_t> outs,
	const rt0_node_rdm_t & nodeId,
	const identity_descriptor_t & thisId_,
	const identity_descriptor_t & remoteId_,
	const session_description_t & session_)
	: _xbase(outs, ctx_)
	, _addrh(std::make_shared<__address_handler>(ctx_, nodeId, thisId_, remoteId_, session_)){

	if(is_null(nodeId))
		THROW_MPF_EXC_FWD(nullptr);

	CBL_MPF_TRACELEVEL_EVENT_RT1_CONNECTION_TRACE("inbound_connection:stream:create:s="
		+ this_rdm().remote_object_endpoint().to_string()
		+ ":t=" + this_id().to_str()
		+ ":session=" + session().object_hash().to_str());
	}

void ix_rt1_stream_t::rt1_router_hndl(std::unique_ptr<i_command_from_rt02rt1_t> && response) noexcept {

	if(auto exc = command_cast<i_rt0_exception_t>(std::move(response))) {
		CHECK_NO_EXCEPTION(close_from_router());
		}
	else {
		FATAL_ERROR_FWD(nullptr);
		}
	}

void  ix_rt1_stream_t::addressed_exception(const _address_hndl_t & address,
	std::unique_ptr<dcf_exception_t> && e,
	invoke_context_trait ic)noexcept {

	auto is = tail();
	if(is) {
		is->addressed_exception(address, std::move(e), ic);
		}
	}

/* �������� ��������� � ������ 1 �� ������� 0 */
void ix_rt1_stream_t::rt1_router_hndl(rt0_x_message_unit_t && msg, invoke_context_trait ic) {
	if(msg.unit().rdm().points == this_rdm()) {
		CBL_MPF_KEYTRACE_SET(msg, 137);

		CBL_VERIFY(msg.unit().rdm().points.target_id() == this_id()
			&& msg.unit().rdm().points.source_id() == remote_id()
			&& msg.unit().rdm().points.session() == session()
			&& msg.unit().rdm().points.rt0() == node_id());

		/* ������ �������� ���������������� ������� ������ 1 */
		if(msg.unit().package().type().type() == iol_types_t::st_disconnection) {

			CBL_MPF_TRACELEVEL_EVENT_RT1_CONNECTION_TRACE("inbound_connection:remote stream closed:s="
				+ this_rdm().remote_object_endpoint().to_string()
				+ ":t=" + this_id().to_str()
				+ ":session=" + session().object_hash().to_str());

			close(unregister_message_track_reason_t::closed_by_self_from_remoted);
			}
		else if(msg.unit().package().type().type() == iol_types_t::st_RZi_LX_error) {
			CBL_MPF_KEYTRACE_SET(msg, 138);

#pragma message(CBL_MESSAGE_ROADMAP_FOOTMARK(CBL_MESSAGE_ROADMAP,RZi_L0))

			auto destAddr1 = _address_hndl_t::make(msg.unit().package().address().destination_subscriber_id(),
				iol_types_t::st_RZi_LX_error,
				rtl_level_t::l1,
				async_space_t::undef );

			address_descriptor_t destAddr0;
			std::unique_ptr<dcf_exception_t> e;

			exception_RZi_LX_extract(_xbase.ctx(), msg.capture_unit().capture_package(), destAddr0, e);

			CBL_VERIFY(e);
			CBL_VERIFY(destAddr0.destination_subscriber_id() == destAddr1.id());
			CBL_VERIFY(destAddr0.level() == rtl_level_t::l1);

#ifdef CBL_MPF_MESSAGE_TRACEWAY_KEYS_BY_ID
			crm::file_logger_t::logging(trait_at_keytrace_set(msg).to_str() + " rebind to " + destAddr1.id().to_str(), 100203423);
#endif

			addressed_exception(destAddr1, std::move(e), ic);
			}
		else {
			auto is = tail();
			if(is) {
				CBL_MPF_KEYTRACE_SET(msg, 139);

#ifdef CBL_MPF_ENABLE_TIMETRACE
				/* ����� ����������� ������ ����� ������������� 1-�� ������ -
				����� ������� ������ ��� �������� �� �������������� 1-�� ������ */
				frame.timeline_set_rt1_receiver_router();
#endif//CBL_MPF_ENABLE_TIMETRACE

#pragma message( CBL_VAR_NAME_VALUE(CBL_BUILD_VAR_SNDRCV_REVERSE_GUARD, 91))
				is->decode(std::move(msg));
				}
			else {
				CBL_MPF_KEYTRACE_SET(msg, 140);

#ifdef CBL_MPF_TRACELEVEL_FAULTED_RT1
				std::string log;
				log += "RT1:ix_rt1_stream_t::rt1_router_hndl= null input stack, this object = " + this_id().to_str();
				log += "|" + remote_id().to_str() + "|" + cvt2string(this_rdm().table());
				crm::file_logger_t::logging(log, 0, __CBL_FILEW__, __LINE__);
#endif

				THROW_MPF_EXC_FWD(nullptr);
				}
			}
		}
	else {
		std::string log;
		log += "RT1:ix_rt1_stream_t: bad routed message, object target= ";
		log += msg.unit().rdm().points.target_id().to_str() + "|" + msg.unit().rdm().points.source_id().to_str() + "|" + cvt2string(msg.unit().rdm().points.table());
		log += ", this object = " + this_id().to_str() + "|" + remote_id().to_str() + "|" + cvt2string(this_rdm().table());
		
		FATAL_ERROR_FWD(std::move(log));
		}
	}


