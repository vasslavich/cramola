#include "../../internal.h"
#include "../../context/context.h"
#include "../pingwatcher.h"


using namespace crm;
using namespace crm::detail;


static sndrcv_timeline_periodicity_t verify_timeline( const sndrcv_timeline_periodicity_t & tm ) {
	if( !tm.is_cyclic() || !tm.is_timeouted() )
		THROW_MPF_EXC_FWD(nullptr);

	if( tm.interval() > tm.timeout() ) {
		THROW_MPF_EXC_FWD(nullptr);
		}

	return tm;
	}

static sndrcv_timeline_periodicity_t verify_timeline( const std::chrono::milliseconds & timeout,
	const std::chrono::milliseconds & interval ) {

	if( interval > timeout ) {
		THROW_MPF_EXC_FWD(nullptr);
		}

	if( timeout == std::chrono::milliseconds() || interval == std::chrono::milliseconds() )
		THROW_MPF_EXC_FWD(nullptr);

	if( timeout == std::chrono::milliseconds::max() || interval == std::chrono::milliseconds::max() )
		THROW_MPF_EXC_FWD(nullptr);

	return sndrcv_timeline_periodicity_t::make_cyclic_timeouted( timeout, interval );
	}

ping_watcher_t::ping_watcher_t( const std::chrono::milliseconds & timeout,
	const std::chrono::milliseconds & interval,
	sendout_t && pingOut )
	: _pingOut( std::move( pingOut ) )
	, _timeline( verify_timeline( timeout, interval ) ) {}

ping_watcher_t::ping_watcher_t( const sndrcv_timeline_periodicity_t & tm,
	sendout_t && pingOut )
	: _pingOut( std::move( pingOut ) )
	, _timeline( verify_timeline(tm ) ){}

ping_watcher_t::milliseconds_type_t ping_watcher_t::curr_time_count() {
	return std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
	}

ping_watcher_t::milliseconds_type_t ping_watcher_t::accept_last_time_count()const noexcept{
	return _acceptLastTime.load( std::memory_order::memory_order_acquire );
	}

ping_watcher_t::milliseconds_type_t ping_watcher_t::emit_last_time_count()const  noexcept{
	return _emitLastTime.load( std::memory_order::memory_order_acquire );
	}

ping_watcher_t::milliseconds_type_t ping_watcher_t::accept_curr_delta()const  noexcept{
	const auto msEpoch = curr_time_count();
	const auto lastValue = accept_last_time_count();
	if( msEpoch > lastValue ) {
		return msEpoch - lastValue;
		}
	else
		return 0;
	}

ping_watcher_t::milliseconds_type_t ping_watcher_t::emit_curr_delta()const noexcept{
	const auto msEpoch = curr_time_count();
	const auto lastValue = emit_last_time_count();
	if( msEpoch > lastValue ) {
		return msEpoch - lastValue;
		}
	else
		return 0;
	}

bool ping_watcher_t::started()const  noexcept{
	return _started.load( std::memory_order::memory_order_acquire );
	}

bool ping_watcher_t::canceled()const  noexcept{
	return _cancel.load( std::memory_order::memory_order_acquire );
	}

void ping_watcher_t::start() {
	if( !canceled() ){

		bool stf = false;
		if( _started.compare_exchange_strong( stf, true ) ){
			add_accept();
			add_emit();
			}
		}
	}

void ping_watcher_t::stop() noexcept{
	_cancel.store( true, std::memory_order::memory_order_release );
	}

void ping_watcher_t::add_accept()  noexcept{
	if( started() && !canceled() ) {
		_acceptLastTime.store( curr_time_count(), std::memory_order::memory_order_release );
		}
	}

void ping_watcher_t::add_emit()  noexcept{
	if( started() && !canceled() ) {
		_emitLastTime.store( curr_time_count(), std::memory_order::memory_order_release );
		}
	}

bool ping_watcher_t::accepted_interval_exceeded()const  noexcept{
	if( started() && !canceled() ) {
		return accept_curr_delta() > _timeline.interval().count();
		}
	else
		return false;
	}

std::chrono::milliseconds ping_watcher_t::accept_last_interval()const  noexcept{
	return std::chrono::milliseconds( accept_curr_delta() );
	}

std::chrono::milliseconds ping_watcher_t::emit_last_interval()const  noexcept{
	return std::chrono::milliseconds(emit_curr_delta());
	}

bool ping_watcher_t::accept_timeout_exceeded()const  noexcept{
	return accept_curr_delta() > timeout().count();
	}

bool ping_watcher_t::emit_timeout_exceeded()const  noexcept{
	return emit_curr_delta() > timeout().count();
	}

bool ping_watcher_t::emit_interval_exceeded()const  noexcept{
	if( started() && !canceled() ) {
		return emit_curr_delta() > _timeline.interval().count();
		}
	else
		return false;
	}

bool ping_watcher_t::expired()const  noexcept{
	if( started() && !canceled() ) {
		return (accept_timeout_exceeded() && emit_timeout_exceeded());
		}
	else
		return false;
	}

const std::chrono::milliseconds& ping_watcher_t::timeout()const noexcept{
	return _timeline.timeout();
	}

const std::chrono::milliseconds& ping_watcher_t::interval()const noexcept{
	return _timeline.interval();
	}

void ping_watcher_t::check(){
	if( started() && !canceled() ) {
		if( emit_interval_exceeded() && accepted_interval_exceeded() ) {
			_pingOut();
			}
		}
	}


