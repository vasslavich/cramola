#include "../../context/context.h"
#include "../rtx_cnctx.h"


using namespace crm;
using namespace crm::detail;


/*======================================================================
__rtx_connection_ctx_t
========================================================================*/


__rtx_connection_ctx_t::~__rtx_connection_ctx_t(){
	CBL_VERIFY(_closedf.load(std::memory_order::memory_order_acquire));
	}

__rtx_connection_ctx_t::__rtx_connection_ctx_t(std::weak_ptr<distributed_ctx_t> ctx_, 
	local_object_create_connection_t && ctrLocalId_,
	remote_object_check_identity_t && checkerRemoteId_, 
	std::weak_ptr<i_xpeer_t> xp,
	const sndrcv_timeline_t & tml)
	: _xpeer(std::move(xp))
	, _timeline(tml)
	, _ctx(std::move(ctx_))
	, _ctrLocalId(std::move(ctrLocalId_))
	, _checkerRemoteId(std::move(checkerRemoteId_)) {
	
	CBL_VERIFY(timeline().is_once_call());
	}

__rtx_connection_ctx_t::__rtx_connection_ctx_t(std::weak_ptr<distributed_ctx_t> ctx_,
	local_object_create_connection_t && ctrLocalId_,
	remote_object_check_identity_t && checkerRemoteId_,
	std::weak_ptr<i_xpeer_t> xp )
	: __rtx_connection_ctx_t(ctx_,
		std::move(ctrLocalId_),
		std::move(checkerRemoteId_),
		std::move(xp),
		CHECK_PTR(ctx_)->policies().connection_timepoints()) {
	
	CBL_VERIFY(timeline().is_once_call());
	}

const sndrcv_timeline_t& __rtx_connection_ctx_t::timeline()const {
	return _timeline;
	}

std::shared_ptr<distributed_ctx_t> __rtx_connection_ctx_t::ctx()const {
	return _ctx.lock();
	}

void __rtx_connection_ctx_t::ntf_hndl(std::unique_ptr<dcf_exception_t> && Ntf)noexcept {
	if(auto ctx_ = ctx()) {
		CHECK_NO_EXCEPTION(ctx_->exc_hndl(std::move(Ntf)));
		}
	}

bool __rtx_connection_ctx_t::expired_waiting()const noexcept{
	return timeline().deadline_point() < std::chrono::system_clock::now();
	}

void __rtx_connection_ctx_t::start() {

	auto ctx_(ctx());
	if(ctx_) {

		if(timeline().is_timeouted()) {

			_expiredTimer = ctx_->create_timer_oncecall_sys();

			/* ������ ������� �������� */
			_expiredTimer->start([wptr = weak_from_this()](async_operation_result_t result) {

				auto sptr(std::dynamic_pointer_cast<__rtx_connection_ctx_t>(wptr.lock()));
				if(sptr) {
					auto exceptionHandler = [&sptr](std::unique_ptr<dcf_exception_t> && exc)noexcept{
						CHECK_NO_EXCEPTION(sptr->invoke_error(async_operation_result_t::st_exception, std::move(exc)));
						};

					try {
						/* ���������� ������ �� ���������� ������� �������� */
						if(result == async_operation_result_t::st_ready) {
							CHECK_NO_EXCEPTION(sptr->invoke_error(async_operation_result_t::st_timeouted, nullptr));
							}

						else if(result == async_operation_result_t::st_abandoned
							|| result == async_operation_result_t::st_canceled
							|| result == async_operation_result_t::st_timeouted) {

							CHECK_NO_EXCEPTION(sptr->invoke_error(result, nullptr));
							}

						/* ���� ������ */
						else {
							exceptionHandler(CREATE_PTR_EXC_FWD(nullptr));
							}
						}
					catch(const dcf_exception_t & exc0) {
						exceptionHandler(exc0.dcpy());
						}
					catch(const std::exception & exc1) {
						exceptionHandler(std::make_unique<dcf_exception_t>(exc1));
						}
					catch(...) {
						exceptionHandler(CREATE_PTR_EXC_FWD(nullptr));
						}
					}
				}, timeline().timeout());
			}
		}
	else {
		THROW_MPF_EXC_FWD(nullptr);
		}
	}

void __rtx_connection_ctx_t::close_timer()noexcept {
	if(_expiredTimer) {
		CHECK_NO_EXCEPTION(_expiredTimer->stop());
		CHECK_NO_EXCEPTION(_expiredTimer.reset());
		}
	}

void __rtx_connection_ctx_t::close_peer()noexcept {
	if(invoke_result() != async_operation_result_t::st_ready) {
		if(auto sp = _xpeer.lock()) {
			CHECK_NO_EXCEPTION(sp->close());
			}
		}
	}

std::shared_ptr< i_xpeer_t> __rtx_connection_ctx_t::peer()const noexcept {
	return _xpeer.lock();
	}

void __rtx_connection_ctx_t::close()noexcept {
	CHECK_NO_EXCEPTION(close_timer());
	CHECK_NO_EXCEPTION(close_peer());
	_closedf.store( true, std::memory_order::memory_order_release);
	}

remote_object_connection_value_t __rtx_connection_ctx_t::ctr_local_id()const {
	if(_ctrLocalId)
		return _ctrLocalId();
	else
		THROW_MPF_EXC_FWD(nullptr);
	}

bool __rtx_connection_ctx_t::checker_remote_id(const identity_value_t & value,
	std::unique_ptr<i_peer_remote_properties_t> & remoteProp)const{

	if(_checkerRemoteId)
		return _checkerRemoteId(value, remoteProp);
	else
		THROW_MPF_EXC_FWD(nullptr);
	}

void __rtx_connection_ctx_t::set_result(async_operation_result_t r) noexcept{
	_invokeR.store( r, std::memory_order::memory_order_release );
	}

async_operation_result_t __rtx_connection_ctx_t::invoke_result()const noexcept {
	return _invokeR.load(std::memory_order::memory_order_acquire);
	}


/*======================================================================
__rtx_out_connection_ctx_timered_t
========================================================================*/


__rtx_out_connection_ctx_timered_t::~__rtx_out_connection_ctx_timered_t() {
	CHECK_NO_EXCEPTION(close());
	}

void __rtx_out_connection_ctx_timered_t::__ctr() {
	start();
	}

void __rtx_out_connection_ctx_timered_t::close() noexcept{
	CHECK_NO_EXCEPTION(base_type::close());
	}

void __rtx_out_connection_ctx_timered_t::invoke_error(std::unique_ptr<dcf_exception_t> && exc)noexcept {
	invoke_error(async_operation_result_t::st_exception, std::move(exc));
	}

void __rtx_out_connection_ctx_timered_t::invoke_error(std::unique_ptr<dcf_exception_t> && exc, reconnect_setup_t rc)noexcept {
	invoke(async_operation_result_t::st_exception, std::move(exc), rc);
	}

void __rtx_out_connection_ctx_timered_t::invoke_error(async_operation_result_t ar,
	std::unique_ptr<dcf_exception_t> && exc )noexcept {
	
	invoke(ar, std::move(exc), reconnect_setup_t::yes);
	}

void __rtx_out_connection_ctx_timered_t::invoke_ready()noexcept {
	invoke(async_operation_result_t::st_ready, nullptr, reconnect_setup_t::yes);
	}

std::unique_ptr< __i_rtx_out_connection_handler_t>  __rtx_out_connection_ctx_timered_t::invoker()noexcept {
	decltype(_handshakeHndl) f;
	std::swap(f, _handshakeHndl);

	return f;
	}

void __rtx_out_connection_ctx_timered_t::invoke(async_operation_result_t ar, 
	std::unique_ptr<dcf_exception_t> && exc, 
	reconnect_setup_t rc)noexcept {

	bool done = false;
	if( _hndlHasDone.compare_exchange_strong( done, true ) ) {

		auto exceptionHandler = [this]( std::unique_ptr<dcf_exception_t> && exc )noexcept{
			auto ctx_( ctx() );
			if( ctx_ ) {
				ctx_->exc_hndl( std::move( exc ) );
				}
			};

		try {
			if(auto f = invoker()) {
				CHECK_NO_EXCEPTION(f->invoke(ar, std::move(exc), rc));

				set_result(ar);
				}
			else {
				exceptionHandler(CREATE_PTR_EXC_FWD(nullptr));
				}
			}
		catch( const dcf_exception_t & exc0 ) {
			exceptionHandler( exc0.dcpy() );
			}
		catch( const std::exception & exc1 ) {
			exceptionHandler( std::make_unique<dcf_exception_t>( exc1 ) );
			}
		catch( ... ) {
			exceptionHandler( CREATE_PTR_EXC_FWD(nullptr) );
			}
		}
	}

const std::unique_ptr<i_endpoint_t>& __rtx_out_connection_ctx_timered_t::endpoint()const {
	return _targetEP;
	}


/*==========================================================================
__rtx_in_connection_ctx_timered_t
============================================================================*/


__rtx_in_connection_ctx_timered_t::~__rtx_in_connection_ctx_timered_t() {
	CHECK_NO_EXCEPTION(close());
	}

void __rtx_in_connection_ctx_timered_t::__ctr() {
	start();
	}

void __rtx_in_connection_ctx_timered_t::close()noexcept {
	CHECK_NO_EXCEPTION( base_type::close() );
	}

void __rtx_in_connection_ctx_timered_t::invoke_error(std::unique_ptr<dcf_exception_t> && exc)noexcept {
	invoke_error(async_operation_result_t::st_exception, std::move(exc));
	}

std::unique_ptr< __i_rtx_in_connection_handler_t> __rtx_in_connection_ctx_timered_t::invoker()noexcept {
	decltype(_handshakeHndl) f;
	std::swap(f, _handshakeHndl);

	return f;
	}

void __rtx_in_connection_ctx_timered_t::invoke(async_operation_result_t ar,
	std::unique_ptr<dcf_exception_t> && exc, syncdata_list_t && sl)noexcept {

	bool done = false;
	if(_hndlHasDone.compare_exchange_strong(done, true)) {

		auto exceptionHandler = [this](std::unique_ptr<dcf_exception_t> && exc)noexcept {
			if(auto ctx_ = ctx()) {
				CHECK_NO_EXCEPTION(ctx_->exc_hndl(std::move(exc)));
				}
			};

		try {
			if(auto f = invoker()) {
				CHECK_NO_EXCEPTION(f->invoke(ar, std::move(exc), std::move(sl)));

				set_result(async_operation_result_t::st_ready);
				}
			else {
				exceptionHandler(CREATE_PTR_EXC_FWD(nullptr)); 
				}
			}
		catch(const dcf_exception_t & exc0) {
			exceptionHandler(exc0.dcpy());
			}
		catch(const std::exception & exc1) {
			exceptionHandler(CREATE_PTR_EXC_FWD(exc1));
			}
		catch(...) {
			exceptionHandler(CREATE_PTR_EXC_FWD(nullptr));
			}
		}
	}

void __rtx_in_connection_ctx_timered_t::invoke_error(async_operation_result_t ar, std::unique_ptr<dcf_exception_t> && exc )noexcept {
	invoke(ar, std::move(exc), syncdata_list_t{});
	}

void __rtx_in_connection_ctx_timered_t::invoke_ready(syncdata_list_t && sl ) noexcept{
	invoke(async_operation_result_t::st_ready, nullptr, std::move(sl) );
	}


/*========================================================================
__handshake_state_t
==========================================================================*/


__handshake_state_t::__handshake_state_t() noexcept {}

__handshake_state_t::__handshake_state_t(std::shared_ptr<__rtx_connection_ctx_t> && state)noexcept
	: _pstate(std::move(state)) {
	
	CBL_VERIFY(_pstate.use_count() == 1);
	}

__handshake_state_t::__handshake_state_t( __handshake_state_t && o )noexcept
	: _pstate( std::move( o._pstate ) ) {}

__handshake_state_t& __handshake_state_t::operator = (__handshake_state_t && o) noexcept {
	_pstate = std::move( o._pstate );
	return (*this);
	}

__handshake_state_t::~__handshake_state_t() {
	CHECK_NO_EXCEPTION( close() );
	}

void __handshake_state_t::close() noexcept{
	CHECK_NO_EXCEPTION(invoke_abandoned() );
	}

void __handshake_state_t::invoke_abandoned()noexcept {
	if(auto p = pstate()) {
		CHECK_NO_EXCEPTION(p->invoke_error(CREATE_ABANDONED_PTR_EXC_FWD(nullptr)));
		}
	}

void __handshake_state_t::invoke_error(std::unique_ptr<dcf_exception_t> && exc)noexcept {
	if(auto p = pstate()) {
		if(exc) {
			CHECK_NO_EXCEPTION(p->invoke_error(std::move(exc)));
			}
		else {
			p->invoke_error(CREATE_ABANDONED_PTR_EXC_FWD(nullptr));
			}
		}
	}

std::shared_ptr<__rtx_connection_ctx_t> __handshake_state_t::pstate()const noexcept{
	return  _pstate;
	}

bool __handshake_state_t::remote_id_validate( const identity_value_t & id, 
	std::unique_ptr<i_peer_remote_properties_t> & remoteProp ) {

	if(auto p = pstate()) {
		return p->checker_remote_id(id, remoteProp);
		}
	else {
		THROW_MPF_EXC_FWD(nullptr);
		}
	}

remote_object_connection_value_t __handshake_state_t::local_id_ctr() {
	if(auto p = pstate()) {
		return p->ctr_local_id();
		}
	else {
		THROW_MPF_EXC_FWD(nullptr);
		}
	}

bool __handshake_state_t::set_remote_tuched()noexcept {
	bool isCRT = false;
	if( _isRemoteTuched.compare_exchange_strong( isCRT, true ) ) {
		return false;
		}
	else {
		return true;
		}
	}

bool __handshake_state_t::empty()const noexcept {
	return nullptr == _pstate;
	}

void __handshake_state_t::commit()noexcept {
	_pstate.reset();
	}


/*========================================================================
__handshake_in_state_t
==========================================================================*/


__handshake_in_state_t::__handshake_in_state_t()noexcept {}

__handshake_in_state_t::__handshake_in_state_t(std::shared_ptr<__rtx_in_connection_ctx_timered_t> && state)noexcept
	: __handshake_state_t(std::move(state)){}

void __handshake_in_state_t::invoke_state_setup(syncdata_list_t && sl)noexcept {
	if(auto pInState = std::dynamic_pointer_cast<__rtx_in_connection_ctx_timered_t>(pstate())) {
		CHECK_NO_EXCEPTION(pInState->invoke_ready(std::move(sl)));
		}
	else {
		FATAL_ERROR_FWD(nullptr);
		}
	}


/*========================================================================
__handshake_out_state_t
==========================================================================*/


__handshake_out_state_t::__handshake_out_state_t() {}

__handshake_out_state_t::__handshake_out_state_t(std::shared_ptr<__rtx_out_connection_ctx_timered_t> && state)
	: __handshake_state_t(std::move(state)) {}

void __handshake_out_state_t::invoke_state_setup()noexcept {
	commit();
	}


/*========================================================================
__initialize_state_t
==========================================================================*/


__initialize_state_t::~__initialize_state_t() {
	error(nullptr);
	}

std::shared_ptr< __rtx_connection_ctx_t> __initialize_state_t::cnx() && noexcept {
	return std::atomic_exchange(&_pstate, std::shared_ptr< __rtx_connection_ctx_t>{});
	}

std::shared_ptr< __rtx_connection_ctx_t> __initialize_state_t::cnx()const & noexcept {
	return std::atomic_load_explicit(&_pstate, std::memory_order::memory_order_acquire);
	}

void __initialize_state_t::error(std::unique_ptr<dcf_exception_t> && exc)noexcept {
	if (auto pctx = cnx()) {
		std::ostringstream sctx;
		if (auto xsp = pctx->peer()) {
			sctx << "peer identity{";
			sctx << "name=" << xsp->name() << ":";
			sctx << "connection=" << xsp->connection_key().to_string() << ":";
			sctx << "d=" << cvt2string(xsp->direction()) << "}";
			}

		if (exc) {
			exc->add_message_line(cvt<wchar_t>(sctx.str()));
			pctx->invoke_error(std::move(exc));
			}
		else {
			pctx->invoke_error(CREATE_ABANDONED_PTR_EXC_FWD(sctx.str()));
			}
		}
	}

__initialize_state_t::__initialize_state_t() noexcept{}

__initialize_state_t::__initialize_state_t(std::shared_ptr<__rtx_connection_ctx_t> wctx,
	object_initialize_handler_t && initializeHndl)noexcept
	: _pstate(std::move(wctx))
	, _initializeHndl(std::move(initializeHndl)) {
	}

void __initialize_state_t::invoke( datagram_t && dt, object_initialize_result_guard_t && resultHndl ){
	bool invokef = false;
	if( _invokef.compare_exchange_strong( invokef, true ) ){
		if( _initializeHndl ){
			_initializeHndl( std::move( dt ), std::move( resultHndl ) );
			}
		}
	}

bool __initialize_state_t::empty()const {
	return nullptr == _initializeHndl;
	}




const std::string remote_object_initialize_response = "[stage][connection][initialize][response]";


/*========================================================================
unregister_rt1_stream_t
==========================================================================*/


unregister_rt1_stream_t::unregister_rt1_stream_t()noexcept{}

unregister_rt1_stream_t::unregister_rt1_stream_t( std::weak_ptr<distributed_ctx_t> ctx,
	std::function<void()> && f )noexcept
	: _f( std::move( f ) )
	, _ctx( ctx ){}

unregister_rt1_stream_t::unregister_rt1_stream_t( unregister_rt1_stream_t && o )noexcept
	: _f( std::move( o._f ) )
	, _ctx( std::move( o._ctx ) ) {}

unregister_rt1_stream_t& unregister_rt1_stream_t::operator = (unregister_rt1_stream_t && o)noexcept {
	_f = std::move( o._f );
	_ctx = std::move( o._ctx );

	return (*this);
	}

bool unregister_rt1_stream_t::valid()const noexcept {
	return nullptr != _f;
	}

void unregister_rt1_stream_t::reset() noexcept {
	_f = nullptr;
	}

void unregister_rt1_stream_t::invoke()noexcept {
	if( _f ) {
		auto ctx( _ctx.lock() );
		if( ctx ) {
			try {
				_f();
				}
			catch( ... ) {
				ctx->exc_hndl( CREATE_MPF_PTR_EXC_FWD(nullptr) );
				}
			}
		}

	reset();
	}

unregister_rt1_stream_t::~unregister_rt1_stream_t() {
	CHECK_NO_EXCEPTION( invoke() );
	}

