#include "../../internal.h"
#include "../../context/context.h"
#include "../rt1_cninzr.h"
#include "../rt1_ostream.h"
#include "../../channel_terms/xtract.h"
#include "../../logging/logger.h"
#include "../../utilities/scopedin/postactor.h"
#include "../rt1_opeer.h"
#include "../../protocol/xcvt.h"


using namespace crm;
using namespace crm::detail;


xpeer_out_connector_rt1_t::xpeer_out_connector_rt1_t(
	std::weak_ptr<distributed_ctx_t> ctx_,
	std::weak_ptr<i_rt1_main_router_t> rt1,
	const identity_descriptor_t & thisId,
	const identity_descriptor_t & remoteId,
	rt1_endpoint_t && ep,
	std::shared_ptr<default_binary_protocol_handler_t> gtwCtr,
	std::unique_ptr<i_input_messages_stock_t> && instackCtr,
	object_initialize_handler_t && outXIniHndl,
	post_connection_t && cnHndl,
	std::function<std::shared_ptr<i_peer_statevalue_t>()> && stvalCtr,
	__event_handler_point_f && eh,
	exceptions_suppress_checker_t && remoteExcChecker,
	stream_creator_f && sctr )
	: _ctx(ctx_)
	, _rt1(rt1)
	, _thisId(thisId)
	, _remoteId(remoteId)
	, _ep( std::move(ep))
	, _gtwCtr(std::move(gtwCtr))
	, _instackCtr( std::move( instackCtr ) )
	, _outXIniHndl(std::move(outXIniHndl)) 
	, _cnHndl( std::move( cnHndl ) )
	, _evHndl( std::move( eh ) )
	, _stvalCtr(std::move(stvalCtr))
	, _remoteExcChecker(std::move(remoteExcChecker))
	, _sctr(std::move(sctr)){

	if( !_gtwCtr )
		FATAL_ERROR_FWD(nullptr);
	
	if( is_null( _thisId) || is_null( _remoteId)  )
		THROW_MPF_EXC_FWD(nullptr);

	if( _rt1.expired() )
		THROW_MPF_EXC_FWD(nullptr);

	CBL_VERIFY(!(_ep.address().empty() || _ep.port() == 0));
	}

std::unique_ptr<i_endpoint_t> xpeer_out_connector_rt1_t::target_endpoint()const {
	auto iendp( _ep );
	return std::make_unique<rt1_endpoint_t>( std::move( iendp ) );
	}

const identity_descriptor_t& xpeer_out_connector_rt1_t::target_id()const {
	return _remoteId;
	}

const identity_descriptor_t& xpeer_out_connector_rt1_t::this_id()const {
	return _thisId;
	}

xpeer_out_connector_rt1_t::remote_object_endpoint_t  xpeer_out_connector_rt1_t::remote_object_endpoint()const {

	CBL_VERIFY(!(_ep.address().empty() || _ep.port() == 0));
	return remote_object_endpoint_t::make_remote(_ep.address(), _ep.port(), _ep.endpoint_id());
	}

void xpeer_out_connector_rt1_t::event_post_connection( std::unique_ptr<dcf_exception_t> && exc, peer_link_t && peer ) {
	if( _cnHndl ) {
		_cnHndl( std::move(exc), std::move( peer ) );
		}
	else {
		logger_t::logging( L"[xpeer_out_connector_rt1_t::event_post_connection]:not implemented",
			1, __CBL_FILEW__, __LINE__ );
		}
	}

xpeer_out_connector_rt1_t::post_connection_t xpeer_out_connector_rt1_t::get_event_post_connection()const {
	return _cnHndl;
	}

void xpeer_out_connector_rt1_t::connect(std::weak_ptr<distributed_ctx_t> ctx,
	peer_t::rtx_object_closed_t&& closeHndl,
	connection_hndl_t&& resultHndl,
	local_object_create_connection_t&& identityCtr,
	remote_object_check_identity_t&& identityChecker) {

	if (_gtwCtr) {
		auto connectHndl_ = utility::invoke_functor_shared<void, async_operation_result_t, std::shared_ptr<ox_rt1_stream_t>, std::unique_ptr<dcf_exception_t>&&>(
			[rt = _rt1,
			ctx,
			eh_ = _evHndl,
			iemitter_ = _gtwCtr->create_emitter(),
			initHndl_ = _outXIniHndl,
			instackCtr_ = _instackCtr,
			stvalCtr_ = _stvalCtr(),
			closeHndl_ = std::move(closeHndl),
			remoteExcChecker_ = _remoteExcChecker,
			resultHndl_ = std::move(resultHndl),
			identityCtr_ = std::move(identityCtr),
			identityChecker_ = std::move(identityChecker)]
		(async_operation_result_t r, std::shared_ptr<ox_rt1_stream_t> stream, std::unique_ptr<dcf_exception_t>&& e)mutable {

			detail::post_scope_action_t resultChecker([&resultHndl_, r] {
				if (resultHndl_) {
					resultHndl_(xpeer_base_rt1_t::link_t{}, r, CREATE_MPF_PTR_EXC_FWD(nullptr), reconnect_setup_t::yes);
					}
				});

			CBL_MPF_TRACELEVEL_EVENT_RT1_CONNECTION_TRACE("ostream:connect t0 l0 resume:"
				+ (stream ? stream->endpoint().to_string() : "")
				+ ":" + cvt2string(r));

			if (r == async_operation_result_t::st_ready) {
				xpeer_out_rt1_t::connect2i(ctx,
					rt,
					std::move(stream),
					std::make_unique<binary_packet_cvt_t>(ctx, std::move(iemitter_)),
					std::move(closeHndl_),
					std::move(resultHndl_),
					std::move(identityCtr_),
					std::move(identityChecker_),
					std::move(initHndl_),
					instackCtr_.get(),
					std::move(stvalCtr_),
					std::move(eh_),
					std::move(remoteExcChecker_));

				resultChecker.reset();
				}
			else {
				bool next{ true };
				if (e && remoteExcChecker_) {
					next = remoteExcChecker_(connection_stage_t::initialize, e);
					}

				auto ctx_(ctx.lock());
				if (ctx_ && resultHndl_) {
					if (next) {
						ctx_->launch_async(__FILE_LINE__,
							std::move(resultHndl_), xpeer_base_rt1_t::link_t{}, r, CREATE_MPF_PTR_EXC_FWD(nullptr), reconnect_setup_t::yes);

						}
					else {
						ctx_->launch_async(__FILE_LINE__,
							std::move(resultHndl_), xpeer_base_rt1_t::link_t{}, r, e ? std::move(e) : CREATE_MPF_PTR_EXC_FWD(nullptr), reconnect_setup_t::no);

						}

					}
				}
			});

		if (_sctr) {
			if (auto ctx_ = _ctx.lock()) {
				ctx_->launch_async(__FILE_LINE__, _sctr, connection_handler_guard(std::move(connectHndl_)));
				}
			}
		}
	else {
		if (resultHndl) {
			if (auto ctx_ = _ctx.lock()) {
				ctx_->launch_async(__FILE_LINE__, std::move(resultHndl),
					xpeer_base_rt1_t::link_t{},
					async_operation_result_t::st_exception,
					CREATE_ABANDONED_PTR_EXC_FWD(nullptr),
					reconnect_setup_t::no);
				}
			}
		}
	}

bool xpeer_out_connector_rt1_t::check_notify(const std::unique_ptr<dcf_exception_t> & n)noexcept {
	if (_remoteExcChecker) {
		return _remoteExcChecker(connection_stage_t::initialize, n);	
		}
	 else {
		return true;
		}
	}



