#include "../../context/context.h"
#include "../xpeer_cop_handler.h"


using namespace crm;
using namespace crm::detail;


xpeer_cop_handler::xpeer_cop_handler(tail_f && f_, std::weak_ptr<distributed_ctx_t> wptr)noexcept
	: _f(std::move(f_))
	, _ctx(std::move(wptr)) {}

xpeer_cop_handler::~xpeer_cop_handler() {
	CHECK_NO_EXCEPTION(close());
	}

void xpeer_cop_handler::invoke()noexcept {
	bool infv{ false };
	if(_invf.compare_exchange_strong(infv, true)) {
		CBL_VERIFY(_f);

		if(auto c = _ctx.lock()) {
			CHECK_NO_EXCEPTION(c->launch_async(async_space_t::sys, __FILE_LINE__, std::move(_f)));
			}
		}
	}

void xpeer_cop_handler::close()noexcept {
	CHECK_NO_EXCEPTION(invoke());
	}
