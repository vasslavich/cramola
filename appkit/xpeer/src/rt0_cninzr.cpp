#include "../../internal.h"
#include "../../context/context.h"
#include "../rt0_cninzr.h"
#include "../../protocol/xcvt.h"


using namespace crm;
using namespace crm::detail;


xpeer_out_connector_rt0_t::xpeer_out_connector_rt0_t( std::shared_ptr<default_binary_protocol_handler_t> ctrGateway,
	i_xpeer_rt0_t::ctr_0_input_queue_t && ctrInput,
	object_initialize_handler_t && outXInit,
	std::shared_ptr<i_xpeer_source_factory_t> xsf,
	post_connection_t && postConnectionEvent_,
	const identity_descriptor_t & thisId_,
	std::unique_ptr<i_endpoint_t> && endpoint,
	std::function<std::shared_ptr<i_peer_statevalue_t>()> && stvalCtr,
	__event_handler_point_f && eh,
	exceptions_suppress_checker_t && remoteExcChecker )
	: _ctrGateway( std::move( ctrGateway ) )
	, _ctrInput( std::move( ctrInput ) )
	, _outXInit( std::move( outXInit ) )
	, _endpoint( std::move( endpoint ) )
	, _postConnectionEvent( std::move( postConnectionEvent_ ) )
	, _xsf( std::move( xsf ) )
	, _thisId( thisId_ )
	, _eh(std::move(eh))
	, _stvalCtr( std::move( stvalCtr ) )
	, _remoteExcChecker( std::move( remoteExcChecker ) ){

	if( !_ctrGateway )
		FATAL_ERROR_FWD(nullptr);

	if( !_postConnectionEvent )
		FATAL_ERROR_FWD(nullptr);
	}

std::unique_ptr<i_endpoint_t> xpeer_out_connector_rt0_t::target_endpoint()const  {

	auto cpyObj( CHECK_PTR( _endpoint )->copy() );
	CBL_VERIFY( cpyObj != _endpoint );

	return std::move( cpyObj );
	}

const identity_descriptor_t& xpeer_out_connector_rt0_t::this_id()const noexcept {
	return _thisId;
	}

void xpeer_out_connector_rt0_t::event_post_connection( std::unique_ptr<dcf_exception_t> && exc, peer_link_t && peer ) {
	_postConnectionEvent( std::move(exc), std::move( peer ) );
	}

xpeer_out_connector_rt0_t::post_connection_t xpeer_out_connector_rt0_t::get_event_post_connection()const {
	return _postConnectionEvent;
	}

xpeer_out_connector_rt0_t::remote_object_endpoint_t xpeer_out_connector_rt0_t::remote_object_endpoint()const{
	auto pEndp = static_cast<const rt1_endpoint_t*>(_endpoint.get());
	return remote_object_endpoint_t::make_remote(pEndp->address(),
		pEndp->port(),
		pEndp->endpoint_id());
}

void xpeer_out_connector_rt0_t::connect(std::weak_ptr<distributed_ctx_t> ctx,
	peer_t::rtx_object_closed_t && closeHndl,
	connection_hndl_t && resultHndl,
	local_object_create_connection_t && identityCtr,
	remote_object_check_identity_t && identityChecker) {

	if (_ctrGateway) {
		auto remoteExcChecker = _remoteExcChecker;
		auto eh = _eh;

		if (auto coph = peer_t::connect(ctx,
			CHECK_PTR(_xsf)->create(),
			std::make_unique<binary_object_cvt_t>(ctx, _ctrGateway),
			target_endpoint(),
			_thisId,
			std::move(closeHndl),
			std::move(resultHndl),
			std::move(identityCtr),
			std::move(identityChecker),
			object_initialize_handler_t(_outXInit),
			i_xpeer_rt0_t::ctr_0_input_queue_t(_ctrInput),
			_stvalCtr(),
			std::move(eh),
			std::move(remoteExcChecker))) {

			if (auto coph0 = std::atomic_exchange(&_coph, coph)) {
				CHECK_NO_EXCEPTION(coph0->close());
				}
			}
		else {
			THROW_MPF_EXC_FWD(nullptr);
			}
		}
	else
		THROW_MPF_EXC_FWD(nullptr);
	}

bool xpeer_out_connector_rt0_t::check_notify(const std::unique_ptr<dcf_exception_t>& n)noexcept {
	if (_remoteExcChecker) {
		return _remoteExcChecker(connection_stage_t::initialize, n);
		}
	else
		return true;
	}


