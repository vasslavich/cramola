#include "../../internal.h"
#include "../../context/context.h"
#include "../rt0_xpeer.h"


using namespace crm;
using namespace crm::detail;


x0_xpeer_address_handler::x0_xpeer_address_handler(const std::weak_ptr<distributed_ctx_t> & ctx0,
	const identity_descriptor_t &thisId,
	std::string address,
	int port,
	peer_direction_t direction )
	: _thisId(thisId)
	, _thisLocid(CHECK_PTR(ctx0)->make_local_object_identity())
	, _address(std::move(address))
	, _port(port)
	, _direction(direction){

	CBL_VERIFY(!(_address.empty() || _port == 0));
	}

connection_key_t x0_xpeer_address_handler::connection_key()const noexcept {
	return connection_key_t{ address(), port() };
	}

remote_object_state_t x0_xpeer_address_handler::state()const noexcept {
	return _state.load(std::memory_order::memory_order_acquire);
	}

void x0_xpeer_address_handler::set_state(remote_object_state_t st) noexcept{
	if (remote_object_state_t::closed != _state.load(std::memory_order::memory_order_acquire)) {
		auto iprev = _state.exchange(st);
		ERR_VERIFY_TEXT(iprev <= st, std::string(cvt2string(iprev)) + "->" + std::string(cvt2string(st)));
		}
	}

const session_description_t& x0_xpeer_address_handler::session()const noexcept {
	static_assert(std::is_lvalue_reference_v<decltype(desc().session())>, __CBL_FILEW__);
	return desc().session();
	}

const x0_xpeer_address_handler::rdm_local_identity_t& x0_xpeer_address_handler::rdm_local_identity()const noexcept {
	static_assert(std::is_lvalue_reference_v<decltype(_localRdm.get())>, __CBL_FILEW__);
	return _localRdm.get();
	}

const x0_xpeer_address_handler::xpeer_desc_t& x0_xpeer_address_handler::desc()const noexcept {
	static_assert(std::is_lvalue_reference_v<decltype(rdm_local_identity())>, __CBL_FILEW__);
	return rdm_local_identity();
	}

const local_object_id_t& x0_xpeer_address_handler::local_id()const noexcept {
	return _thisLocid;
	}

const rt0_node_rdm_t& x0_xpeer_address_handler::rt0rdm()const noexcept {
	static_assert(std::is_lvalue_reference_v<decltype(rdm_local_identity())>, __CBL_FILEW__);
	return rdm_local_identity();
	}

rtl_table_t x0_xpeer_address_handler::direction()const noexcept {
	return _direction;
	}

x0_xpeer_address_handler::remote_object_endpoint_t x0_xpeer_address_handler::remote_object_endpoint()const noexcept {
	CBL_VERIFY(!(address().empty() || port() == 0));
	return remote_object_endpoint_t::make_remote(address(), port(), remote_id());
	}

const identity_descriptor_t& x0_xpeer_address_handler::this_id()const noexcept {
	return _thisId;
	}

const identity_descriptor_t& x0_xpeer_address_handler::remote_id()const noexcept {
	static_assert(std::is_lvalue_reference_v<decltype(_remoteId.get())>, __CBL_FILEW__);
	return _remoteId.get();
	}

const identity_descriptor_t& x0_xpeer_address_handler::id()const noexcept {
	static_assert(std::is_lvalue_reference_v<decltype(remote_id())>, __CBL_FILEW__);
	return remote_id();
	}

const std::string& x0_xpeer_address_handler::address()const noexcept {
	return _address;
	}

int x0_xpeer_address_handler::port()const noexcept {
	return _port;
	}

void x0_xpeer_address_handler::set_rdm(rt0_node_rdm_t && rdm) {
	if(!_localRdm.set(std::move(rdm)))
		THROW_MPF_EXC_FWD(nullptr);
	}

void x0_xpeer_address_handler::set_remote_id(const identity_descriptor_t  &id) {
	_remoteId.set(id);
	}

peer_direction_t x0_xpeer_address_handler::peer_direction()const noexcept {
	return _direction;
	}
