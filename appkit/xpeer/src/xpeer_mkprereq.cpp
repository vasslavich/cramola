#include "../../internal.h"
#include "../xpeer_mkprereq.h"


using namespace crm;
using namespace crm::detail;


static_assert(srlz::is_serializable_v< base_peer_identity_properties_t>, __FILE_LINE__);
static_assert(srlz::detail::is_i_srlzd_eof_v<base_peer_identity_properties_t>, __FILE_LINE__);


binary_vector_t base_peer_identity_properties_t::srlz_()const{
	auto wstream( srlz::wstream_constructor_t::make() );
	srlz::serialize( wstream, (*this) );

	return wstream.release();
	}

base_peer_identity_properties_t base_peer_identity_properties_t::dsrlz_( const binary_vector_t & bin ){
	auto rstream( srlz::rstream_constructor_t::make( bin.begin(), bin.cend() ) );

	base_peer_identity_properties_t obj;
	srlz::deserialize( rstream, obj );

	return std::move( obj );
	}


/*====================================================================
remote_object_connection_value_t
======================================================================*/


static_assert(srlz::is_serializable_v< remote_object_connection_value_t>, __FILE_LINE__);


remote_object_connection_value_t::remote_object_connection_value_t(){}

remote_object_connection_value_t::remote_object_connection_value_t( identity_value_t && identity_,
	syncdata_list_t && sl )
	: _identity( std::move( identity_ ) )
	, _syncdata( std::move( sl ) ){}

remote_object_connection_value_t::remote_object_connection_value_t( remote_object_connection_value_t  && o )
	: _identity( std::move( o._identity ) )
	, _syncdata( std::move( o._syncdata ) ){}

remote_object_connection_value_t& remote_object_connection_value_t::operator=( remote_object_connection_value_t && o ){
	_identity = std::move( o._identity );
	_syncdata = std::move( o._syncdata );
	return (*this);
	}

const identity_value_t& remote_object_connection_value_t::identity()const{
	return _identity;
	}

identity_value_t remote_object_connection_value_t::identity_release(){
	return std::move( _identity );
	}

const syncdata_list_t& remote_object_connection_value_t::syncdata()const{
	return _syncdata;
	}

syncdata_list_t remote_object_connection_value_t::syncdata_release(){
	return std::move( _syncdata );
	}


