#include "../../internal.h"
#include "../../context/context.h"
#include "../rt0_xpeer.h"
#include "../../queues/qdef.h"
#include "../xpeer_push_handler.h"
#include "../../streams/istreams_routetbl.h"


using namespace crm;
using namespace crm::detail;


static_assert(is_xpeer_trait_z_v<decltype(std::declval<xpeer_base_rt0_t>().get_link())>, __FILE_LINE__);
static_assert(is_xpeer_trait_z_v<decltype(make_invokable_xpeer(std::declval<xpeer_base_rt0_t>().get_link()))>, __FILE_LINE__);


void xpeer_base_rt0_t::trace_send(const i_iol_ohandler_t& msg)const noexcept {

	bool tracef = false;
#ifdef CBL_MPF_TRACELEVEL_EVENT_RT0_SEND_INBOUND
	if (direction() == rtl_table_t::in)
		tracef = true;
#endif

#ifdef CBL_MPF_TRACELEVEL_EVENT_RT0_SEND_OUTBOUND
	if (direction() == rtl_table_t::out)
		tracef = true;
#endif

	bool traceType = false;

#if (CBL_MPF_TRACELEVEL_EVENT_RT0_SEND_LEVEL == CBL_MPF_TRACELEVEL_EVENT_RT0_SEND_ALL )
	traceType = true;
#elif (CBL_MPF_TRACELEVEL_EVENT_RT0_SEND_LEVEL == CBL_MPF_TRACELEVEL_EVENT_RT0_SEND_1)
	if (msg.type().type() != iol_types_t::st_ping) {
		traceType = true;
		}
#endif

	if (tracef && traceType) {
		if (msg.level() == level()) {
			crm::file_logger_t::logging(CBL_MPF_PREFIX_RT0("send:" + address_trace() + ":" + std::to_string(msg.type().utype())),
				CBL_MPF_TRACELEVEL_EVENT_RT0_SEND_TAG);
			}
		}
	}

void xpeer_base_rt0_t::trace_recv(const i_iol_ihandler_t& msg)const noexcept {

	bool tracef = false;
#ifdef CBL_MPF_TRACELEVEL_EVENT_RT0_RECV_INBOUND
	if (direction() == rtl_table_t::in)
		tracef = true;
#endif

#ifdef CBL_MPF_TRACELEVEL_EVENT_RT0_RECV_OUTBOUND
	if (direction() == rtl_table_t::out)
		tracef = true;
#endif

	bool traceType = false;

#if (CBL_MPF_TRACELEVEL_EVENT_RT0_RECV_LEVEL == CBL_MPF_TRACELEVEL_EVENT_RT0_RECV_ALL )
	traceType = true;
#elif (CBL_MPF_TRACELEVEL_EVENT_RT0_RECV_LEVEL == CBL_MPF_TRACELEVEL_EVENT_RT0_RECV_1)
	if (msg.type().type() != iol_types_t::st_ping) {
		traceType = true;
		}
#endif

	if (tracef && traceType) {
		if (msg.level() == level()) {
			crm::file_logger_t::logging(CBL_MPF_PREFIX_RT0("accept:" + address_trace() + ":" + std::to_string(msg.type().utype())),
				CBL_MPF_TRACELEVEL_EVENT_RT0_INPUT_TAG);
			}
		}
	}

void xpeer_base_rt0_t::exc_hndl( std::unique_ptr<dcf_exception_t> && ntf ) noexcept {
	auto sctx( ctx() );
	if(sctx )
		sctx->exc_hndl( std::move( ntf ) );
	}

void xpeer_base_rt0_t::exc_hndl( const dcf_exception_t & exc ) noexcept {
	exc_hndl( exc.dcpy() );
	}

void xpeer_base_rt0_t::exc_hndl( dcf_exception_t && exc )noexcept {
	exc_hndl( exc.dcpy() );
	}

void xpeer_base_rt0_t::begin_connect(const std::unique_ptr<i_endpoint_t> &address,
	std::function<void(std::unique_ptr<dcf_exception_t> && exc)> && endConnect) {

	CHECK_PTR(_source)->async_connect(address, std::move(endConnect));
	}

void xpeer_base_rt0_t::set_session_input( const identity_descriptor_t & nodeId_,
	const session_description_t & session_ ) {
	CBL_VERIFY(_addrh);

	const auto cnkey = _addrh->connection_key();
	CBL_VERIFY( !is_null( cnkey ) );

	auto rdml = rt0_node_rdm_t::make_input_rdm( nodeId_, session_, cnkey );
	_addrh->set_rdm(std::move(rdml));
	}

void xpeer_base_rt0_t::set_session_output( const identity_descriptor_t & nodeId_,
	const session_description_t & session_,
	const i_endpoint_t & epRequested ) {
	CBL_VERIFY(_addrh);

	const auto cnkey = _addrh->connection_key();
	CBL_VERIFY( !is_null( cnkey ) );

	auto rdml = rt0_node_rdm_t::make_output_rdm( nodeId_,
		session_,
		epRequested.to_key(),
		cnkey );
	_addrh->set_rdm(std::move(rdml));
	}

void xpeer_base_rt0_t::close()noexcept {
	CHECK_NO_EXCEPTION(close(closing_reason_t::extern_force));
	CHECK_NO_EXCEPTION(destroy());
	}

#ifdef CBL_USE_SMART_OBJECTS_COUNTING
crm::log::counter_t xpeer_base_rt0_t::subcounter(const std::string &name) {
	return _THIS_COUNTER.subcounter(name);
	}
#endif//CBL_USE_SMART_OBJECTS_COUNTING


void xpeer_base_rt0_t::end_read_push_ready( const std::vector<uint8_t> & bin, size_t count ) {

	auto slotInput( CHECK_PTR( _gateway ) );
	if( slotInput ) {

#ifdef CBL_ENABLE_TEST_ERROR_EMULATION_LEVEL_0
		bool breakError = false;
		if( peer_direction() == peer_direction_t::in ) {
#ifdef CBL_ENABLE_TEST_ERROR_EMULATION_LEVEL_0_IN
			breakError = true;
#endif//CCBL_ENABLE_TEST_ERROR_EMULATION_LEVEL_0_IN
			}
		else if( peer_direction() == peer_direction_t::out ) {
#ifdef CBL_ENABLE_TEST_ERROR_EMULATION_LEVEL_0_OUT
			breakError = true;
#endif//CBL_ENABLE_TEST_ERROR_EMULATION_LEVEL_0_OUT
			}
		else {
			FATAL_ERROR_FWD(nullptr);
			}

		if(breakError) {
			if(auto c = ctx()) {
				if(c->test_emulation_connection_corrupt(rtl_level_t::l0)) {

					std::string log;
					log += "xpeer_base_rt0_t::end_read:emulator error(connection break):direction=";
					log += cvt2string(peer_direction());
					crm::file_logger_t::logging(log, 0, __CBL_FILEW__, __LINE__);

					CHECK_NO_EXCEPTION(close(closing_reason_t::test_error));
					CHECK_NO_EXCEPTION(destroy());
					}
				}
			}
#endif//CBL_ENABLE_TEST_ERROR_EMULATION_LEVEL_0

		/* ��������� ������ � ������ �������������� */
#pragma message( CBL_VAR_NAME_VALUE(CBL_BUILD_VAR_SNDRCV_REVERSE_GUARD, 0))
		slotInput->push( bin, count);
		}
	}

peer_commands_result_t xpeer_base_rt0_t::execute_command( peer_command_t cmd ) {
	switch( cmd ) {
		case peer_command_t::ping:
			send_ping_request();
			return peer_commands_result_t::enum_type::success;

		default:
			return peer_commands_result_t::enum_type::not_supported;
		}
	}

void xpeer_base_rt0_t::send_ping_request() {
	push(oping_id_t{});
	}

void xpeer_base_rt0_t::send_echo_ping( std::unique_ptr<i_iol_ihandler_t> && sourceObj) {
	if(sourceObj && _source && _source->ping_emit_interval_exceeded()) {
		oping_id_t obj{};
		obj.set_echo_params(*sourceObj, false);

		push(std::move(obj));
		}
	}

xpeer_base_rt0_t::xpeer_base_rt0_t(const std::weak_ptr<distributed_ctx_t> & ctx0,
	std::unique_ptr<i_xpeer_source_t> && source_,
	std::string address,
	int port,
	std::unique_ptr<iol_gtw_t> && gtw_,
	rtx_object_closed_t && closeHndl_,
	std::shared_ptr<i_peer_statevalue_t> && stval,
	const identity_descriptor_t &thisId,
	peer_direction_t direction,
	__event_handler_point_f&& eh)
	: _ctx( ctx0 )
	, _addrh(std::make_shared<x0_xpeer_address_handler>(ctx0, thisId, std::move(address), port, direction))
	, _source(std::move(source_))
	, _gateway(std::move(gtw_))
	, _eventClosingHndl(std::move(closeHndl_))
	, _stateValue(std::move(stval))
	, _evhLst( CHECK_PTR( ctx0 )->events_make_line() )
	, _evHndl(std::move(eh))
	{
	if(!_eventClosingHndl)
		THROW_MPF_EXC_FWD(nullptr);

	/* ������� ��������� ������� - ������������������ */
	set_state(remote_object_state_t::init_stage);
	}

void xpeer_base_rt0_t::close_trace( closing_reason_t r ) noexcept{
	try {
		std::string log( "xpeer_base_rt0_t:close" );
		log += ":";
		log += cvt2string( peer_direction() );
		log += ":";
		log += remote_object_endpoint().to_string();
		log += ":";
		log += cvt2string( r );

		switch( r ) {
			case closing_reason_t::extern_force:
				break;

			case closing_reason_t::break_connection:
				break;

			case closing_reason_t::connection_procedure_timeouted:
				break;

			case closing_reason_t::ping_timeout:
				break;

			case closing_reason_t::rw_exception:
				break;

			default:
				break;
			}

		crm::file_logger_t::logging( CBL_MPF_PREFIX_RT0( log ), CBL_MPF_TRACELEVEL_FAULT_RT0_TAG, __CBL_FILEW__, __LINE__ );
		}
	catch( const dcf_exception_t & exc0 ) {
		exc_hndl( exc0.dcpy() );
		}
	catch( const std::exception & exc1 ) {
		exc_hndl( CREATE_MPF_PTR_EXC_FWD( exc1 ) );
		}
	catch( ... ) {
		exc_hndl( CREATE_MPF_PTR_EXC_FWD(nullptr) );
		}
	}

#ifdef CBL_ENABLE_TEST_ERROR_EMULATION_LEVEL_0
std::string xpeer_base_rt0_t::check_erreml_key()const noexcept {
	CBL_VERIFY(!address().empty());

	return address() + ":rt0:" + cvt2string(direction()) + ":" + this_id().to_str();
	}
#endif

void xpeer_base_rt0_t::close( closing_reason_t reason ) noexcept{


	bool tmp = false;
	if (_closedf.compare_exchange_strong(tmp, true)) {
		if (auto c = ctx()) {
			CHECK_NO_EXCEPTION_BEGIN

				/* ������ ������� - ������ */
				set_state(remote_object_state_t::closed);

#ifdef CBL_MPF_TRACELEVEL_FAULTED_RT0
			close_trace(reason);
#endif

			auto pSTV = std::atomic_exchange(&_stateValue, {});
			if (pSTV) {
				pSTV->close();
				}

			/* ������� �������� */
			if (_source) {
				_source->close();
				}

			/* ������� ������ ��������� �������������� */
			if (_gateway) {
				_gateway->close();
				}

			if (auto q = std::atomic_exchange(&_inQueue, {})) {
				q->clear();
				}

			/* ����������� �������� ���� */
			if (_eventClosingHndl) {
				c->launch_async(__FILE_LINE__, std::move(_eventClosingHndl), rdm_local_identity(), reason, std::move(pSTV));
				}

			/* ������� ����������� */
			event_invoke(event_type_t::st_disconnection,
				std::make_unique<event_close_connection_t>(desc()));

			/* ������� ����� ������������ */
			_evhLst.close();

			CHECK_NO_EXCEPTION_END
			}
		}
	}

#ifdef CBL_USE_SEQUENTIAL_CALL_CHECKER
xpeer_base_rt0_t::seqcall_checker_holder xpeer_base_rt0_t::inc_scope_seqcall() {
	return _seqCallCheckerIO.inc_scope();
	}
#endif

void xpeer_base_rt0_t::set_handshake_mode( std::shared_ptr<input_stack_t> qinput ){
	using qtype = std::decay_t<decltype(CHECK_PTR(ctx())->queue_manager().tcreate_queue<input_value_t>(
		__FILE_LINE__, queue_options_t::message_stack_node))>;


	subscribe_command_handler();

	/* ��������� ������� �������� ���������
	----------------------------------------------------------*/
	if( !qinput ){
		qinput = std::make_shared< qtype>(CHECK_PTR(ctx())->queue_manager().tcreate_queue<input_value_t>(
			__FILE_LINE__,
			queue_options_t::message_stack_node));
		}

	auto oldQ( std::atomic_exchange( &_inQueue, qinput ) );
	if( oldQ )
		oldQ->clear();

	std::weak_ptr<distributed_ctx_t> wctx_( ctx() );
	CHECK_PTR( _gateway )->set_input_queue( std::make_shared<qinput_adpt_t>( std::move( qinput ),
		[wthis = weak_from_this(), wctx_]( qinput_adpt_t::base_item_t && item ){

	#ifdef CBL_MPF_ENABLE_TIMETRACE
		/* ��������� ������� � ����������� ���������� */
		item.object->set_timeline( detail::hanlder_times_stack_t::stage_t::rt0_receiver );

		/* ������ �������������� ���������� � ��� */
		CHECK_PTR( wctx_ )->log_time( item.object->destination_id(), "message clocks", item.object->timeline().clocks() );
	#endif//CBL_MPF_ENABLE_TIMETRACE

		input_stack_t::value_type outVal;

		/* ��������������� ��������� ��������� */
		auto sthis( wthis.lock() );
		if( sthis ){
#ifdef CBL_USE_SEQUENTIAL_CALL_CHECKER
			auto seqHld = sthis->inc_scope_seqcall();
#endif

		#if defined( CBL_MPF_TRACELEVEL_EVENT_RT0_RECV_INBOUND ) || defined (CBL_MPF_TRACELEVEL_EVENT_RT0_RECV_OUTBOUND)
			CHECK_NO_EXCEPTION(sthis->trace_recv(*CHECK_PTR(item.m())));
		#endif

			/* ���� ���� - ��������� �������� */
			if( item.m()->type().type() == iol_types_t::st_ping && item.m()->level() == sthis->level() ){
				sthis->send_echo_ping(std::move(item).m());
				}
			else if(item.m()->type().type() >= iol_types_t::st_stream_header && 
				item.m()->type().type() <= iol_types_t::st_stream_disconnection) {

				if(auto stb = CHECK_PTR(sthis->ctx())->streams_handler()) {
					xpeer_push_handler_pin responseOuth{ std::make_unique<xpeer_push_handler_l0>(sthis->get_link(), sthis->desc()) };

					if(auto istreamMessage = message_cast<i_istream_line_t>(std::move(item).m())) {
						stb->push(sthis->local_id(),
							std::move(istreamMessage),
							std::move(responseOuth),
							sthis->get_event_handler());
						}
					else {
						FATAL_ERROR_FWD(nullptr);
						}
					}
				}
			else{
				auto rdm = make_message(sthis->desc(), (*item.m()));
				outVal = input_stack_t::value_type(std::move(item).m(), std::move(rdm), wthis);
				}
			}

		return std::move( outVal );
		} ) );
	}

void xpeer_base_rt0_t::handshake_end( bool /*result*/) {}

sndrcv_timeline_t xpeer_base_rt0_t::ping_timeline()const {
	if(CHECK_PTR(ctx())->policies().ping_rt())
		return sndrcv_timeline_t::make(CHECK_PTR(ctx())->policies().get_preffered_inactivity_or_ping());
	else
		return sndrcv_timeline_t::null;
	}

void xpeer_base_rt0_t::io_rw_listen() {

	CBL_VERIFY(!is_null(this_id()));

	/* ������ ������� ��������� ���������������
	-----------------------------------------------------*/
#pragma message( CBL_VAR_NAME_VALUE(CBL_BUILD_VAR_SNDRCV_REVERSE_SETUP_FUNCTOR, 0))
#pragma message(CBL_MESSAGE_ROADMAP_FOOTMARK(CBL_MESSAGE_ROADMAP,Ri_L0_FAULT_RESPONSE))

	CHECK_PTR(_gateway)->start({
		[wptr = weak_from_this()] (const address_descriptor_t& addr, std::unique_ptr<dcf_exception_t>&& exc_)noexcept{
		if (auto sptr = wptr.lock()) {

			CBL_NO_EXCEPTION_BEGIN

				onotification_t exc{};
			exc.set_ntf(std::move(exc_));

			exc.set_echo_params(addr);
			CBL_VERIFY(is_sendrecv_response(exc));

			sptr->push(std::move(exc));

			CBL_NO_EXCEPTION_END(sptr->ctx());
			}
		},

		[wptr = weak_from_this()](std::unique_ptr<ifunctor_package_t>&& m) {
			if (auto sptr = wptr.lock()) {
				sptr->reverse_functor_message(std::move(m));
				}
			} });

	/* ����� ������������ ������/������ ��/� ��������� ������ 
	---------------------------------------------------------*/
	if(_source) {
#ifdef CBL_OBJECTS_COUNTER_CHECKER
		auto objcnt1 = weak_from_this().use_count();
#endif

		auto closeHndl = [sptr = shared_from_this()](std::unique_ptr<dcf_exception_t> && ){
			sptr->close(closing_reason_t::rw_exception);
			};

		auto pingoutHndl = [wptr = weak_from_this()]{
			outbound_message_unit_t r;
			if(auto sptr = wptr.lock()) {
				if(sptr->_gateway) {
					oping_id_t pm{};
					sptr->normalizex(pm);

					r = sptr->_gateway->serialize_gateout(std::move(pm));
					}
				}

			return r;
			};

		auto readlineHndl = [wptr = weak_from_this()](const std::vector<uint8_t> & buf, size_t count){
			if(auto sptr = wptr.lock()) {
				sptr->end_read_push_ready(buf, count);
				}
			};

		_source->start(std::move(readlineHndl), std::move(closeHndl), std::move(pingoutHndl), ping_timeline());		

#ifdef CBL_OBJECTS_COUNTER_CHECKER
		CBL_VERIFY(weak_from_this().use_count() > objcnt1);
#endif
		}
	}

__event_handler_point_f xpeer_base_rt0_t::get_event_handler()const noexcept {
	return _evHndl;
	}

void xpeer_base_rt0_t::destroy() noexcept{
	CHECK_NO_EXCEPTION( close( closing_reason_t::destroy ) );
	}

xpeer_base_rt0_t::~xpeer_base_rt0_t() {
	CHECK_NO_EXCEPTION( destroy() );
	CBL_MPF_TRACELEVEL_EVENT_RT0_CONNECTION_TRACE("destroy:" + session().object_hash().to_str());
	}

/*void xpeer_base_rt0_t::write(const crm::srlz::i_rstream_t & proxy) {
	if( !closed() ) {

		const auto xrseq( proxy.reader() );
		if( xrseq ) {
			if(_source) {
				_source->write(xrseq->pop2end<binary_vector_t>());
				}
			else {
				THROW_MPF_EXC_FWD(nullptr);
				}
			}
		else {
			THROW_MPF_EXC_FWD(nullptr);
			}
		}
	else {
		THROW_MPF_EXC_FWD(nullptr);
		}
	}*/

void xpeer_base_rt0_t::write(outbound_message_unit_t && blob) {
	CBL_MPF_KEYTRACE_SET(blob, 470);

#ifdef CBL_IS_KEEP_OUTCOME_MESSAGE_RESPONSE_GUARD_L0
	CBL_VERIFY_SNDRCV_KEY_SPIN_TAG(blob);
	CBL_VERIFY((is_sendrecv_cycle(blob) && CBL_IS_SNDRCV_KEY_SPIN_TAGGED_ON(blob)) || 
		(!is_sendrecv_cycle(blob) && CBL_IS_SNDRCV_KEY_SPIN_TAGGED_OFF(blob)));
#endif

	if( !closed() ) {
		if(_source) {
			_source->write(std::move(blob));
			}
		else {
			THROW_MPF_EXC_FWD(nullptr);
			}
		}
	else {
		THROW_MPF_EXC_FWD(nullptr);
		}
	}

void xpeer_base_rt0_t::__unsubscribe_hndl_id(const _address_hndl_t & id)noexcept {
	if(_gateway)
		_gateway->unsubscribe_hndl_id(id);
	}

void xpeer_base_rt0_t::unsubscribe_hndl_id( const _address_hndl_t & id )noexcept {
	__unsubscribe_hndl_id( id );
	}

subscribe_invoker_result xpeer_base_rt0_t::__subscribe_hndl_id(async_space_t scx,
	const _address_hndl_t & id, subscribe_options opt, subscribe_message_handler && h) {

	if(_gateway) {
		return _gateway->subscribe_hndl_id(scx, id, opt, std::move(h));
		}
	else{
		THROW_MPF_EXC_FWD(nullptr);
		}
	}

void xpeer_base_rt0_t::subscribe_command_handler() {

	/* ����������� ����������� ��������� ��������� ������
	----------------------------------------------------------------------------*/
	if (subscribe_invoker_result::enum_type::success != __subscribe_hndl_id(async_space_t::sys,
		_address_hndl_t::make(subscribe_address_datagram_for_this, iol_types_t::st_sendrecv_datagram, level(), async_space_t::undef),
		subscribe_options(assign_options::overwrite_if_exits, false),
		subscribe_message_handler("xpeer_base_rt0_t::subscribe_command_handler",
			[wthis = weak_from_this()](std::unique_ptr<dcf_exception_t>&& exc, std::unique_ptr<i_iol_ihandler_t>&& item, invoke_context_trait ) {

		if (auto sthis = std::dynamic_pointer_cast<self_t>(wthis.lock())) {
			if (!exc) {
				if (item)
					sthis->command_handler(std::move(item));
				}
			else {
				CHECK_NO_EXCEPTION(sthis->close(closing_reason_t::rw_exception));
				CHECK_NO_EXCEPTION(sthis->destroy());
				}
			}
		}, inovked_as::sync))) {

		THROW_MPF_EXC_FWD(nullptr);
		}
	}

void xpeer_base_rt0_t::command_handler( std::unique_ptr<i_iol_ihandler_t> && iCmd) {
	if(iCmd && !closed()) {
#ifdef CBL_USE_SEQUENTIAL_CALL_CHECKER
		auto seqHld = inc_scope_seqcall();
#endif

		bool rf = false;
		if(iCmd->type().type() == iol_types_t::st_sendrecv_datagram) {	
			if(auto iDgt = message_cast<isendrecv_datagram_t>(std::move(iCmd))) {
				if(iDgt->key() == command_name_identity) {
					command_handler_dtg_identity(std::move(iDgt));
					rf = true;
					}
				else if(iDgt->key() == command_name_initialize) {
					command_handler_dgt_initialize(std::move(iDgt));
					rf = true;
					}
				}
			}

		if(!rf) {
			FATAL_ERROR_FWD(nullptr);
			}
		}
	else {
		THROW_MPF_EXC_FWD(nullptr);
		}
	}

void xpeer_base_rt0_t::on_handshake_success() {}

void xpeer_base_rt0_t::on_extern_completed(std::function<void()> && externHndl) {
	on_handshake_success();
	externHndl();
	}

void xpeer_base_rt0_t::invoke_event(event_type_t t)noexcept {
	if(t == event_type_t::st_connected) {
		event_invoke(event_type_t::st_connected, std::make_unique<event_open_connection_t>(get_link()));
		}
	else {
		FATAL_ERROR_FWD(nullptr);
		}
	}

std::function<std::unique_ptr<i_event_t>(event_type_t)> xpeer_base_rt0_t::event_checker() noexcept {
	return [wptr = weak_from_this()](event_type_t t) {

		std::unique_ptr<i_event_t> evr;
		if(auto sptr = wptr.lock()) {

			switch(t) {
				case event_type_t::lower_bound:
					FATAL_ERROR_FWD(nullptr);
					break;

				case event_type_t::st_undefined:
					FATAL_ERROR_FWD(nullptr);
					break;

				case event_type_t::st_disconnection:
					if(sptr->closed())
						evr = std::make_unique<event_close_connection_t>(sptr->desc());

					break;

				case event_type_t::st_connected:
					if(sptr->is_opened() && sptr->state() == remote_object_state_t::impersonated)
						evr = std::make_unique<event_open_connection_t>(sptr->get_link());

					break;

				case event_type_t::upper_bound:
					FATAL_ERROR_FWD(nullptr);
					break;

				default:
					FATAL_ERROR_FWD(nullptr);
					break;
				}
			}

		return evr;
		};
	}

void xpeer_base_rt0_t::unsubscribe_event( const _address_event_t & id ) noexcept{
	CHECK_NO_EXCEPTION( _evhLst.unsubscribe( id ) );
	}

void xpeer_base_rt0_t::event_invoke(event_type_t type, std::unique_ptr<event_args_t> && args)noexcept {
	CHECK_NO_EXCEPTION( _evhLst.invoke( type, std::move( args ) ) );
	}

subscribe_invoker_result xpeer_base_rt0_t::subscribe_hndl_id(async_space_t scx, const _address_hndl_t & id, subscribe_options opt,
	subscribe_message_handler && h) {

	return subscribe_hndl_id_tv(scx, id, opt, std::move(h));
	}

/*subscribe_invoker_result xpeer_base_rt0_t::subscribe_hndl_id(async_space_t scx, const _address_hndl_t & id, subscribe_options opt,
	const subscribe_handler_t & h) {

	return subscribe_hndl_id_tv(scx, id, opt, subscribe_handler_t(h));
	}*/

subscribe_invoker_result xpeer_base_rt0_t::subscribe_hndl_id_tv(async_space_t scx, const _address_hndl_t & id, subscribe_options opt,
	subscribe_message_handler && h) {

	if(id.id().xoffer() <= distributed_ctx_t::subscribe_address_user_range())
		THROW_MPF_EXC_FWD(nullptr);

	return __subscribe_hndl_id(scx, id, opt, std::move(h));
	}

subscribe_result xpeer_base_rt0_t::subscribe_event( async_space_t scx,
	const _address_event_t & id, bool once, __event_handler_f && h) {

	return subscribe_event_tv(scx, id, once, std::move(h));
	}

void xpeer_base_rt0_t::reverse_functor_message(std::unique_ptr<ifunctor_package_t>&& m) {
	if (m) {
#ifdef CBL_USE_SEQUENTIAL_CALL_CHECKER
		auto seqHld = inc_scope_seqcall();
#endif

		if (auto c = ctx()) {
			MPF_TEST_EMULATION_IN_CONTEXT_L_COND_TRCX(c, level(), 550, is_trait_at_emulation_context(*m), trait_at_keytrace_set(*m));

			ostrob_message_envelop_t<functor_result> rm{ c->invoke_handler(std::move(*m).to_handler()) };
			rm.capture_response_tail(*m);

			MPF_TEST_EMULATION_IN_CONTEXT_L_COND_TRCX(c, level(), 551, is_trait_at_emulation_context(*m), trait_at_keytrace_set(*m));

			push(std::move(rm));
			}
		}
	else {
		THROW_EXC_FWD(nullptr);
		}
	}
