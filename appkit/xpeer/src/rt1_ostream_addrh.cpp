#include "../../context/context.h"
#include "../rt1_ostream.h"
#include "../../erremlt/utilities.h"
#include "../../utilities/scopedin/postactor.h"


using namespace crm;
using namespace crm::detail;




#if (CBL_SESSION_MAKE_TYPE == CBL_SESSION_MAKE_SHORT_PREFIX)
static std::string create_stag(std::weak_ptr<distributed_ctx_t> , const char* ) {
	static std::atomic<std::uintmax_t> counter{ 0 };

	return std::to_string(++counter);
	}

#elif(CBL_SESSION_MAKE_TYPE == CBL_SESSION_MAKE_LONG_PREFIX)
static std::string create_stag(std::weak_ptr<distributed_ctx_t> ctx, const char* tag) {
	auto ctx_(ctx.lock());
	if (!ctx_)
		THROW_MPF_EXC_FWD(nullptr);

	return std::string("xpeer_out_rt0_t:" + ctx_->curr_time_str() + ":" + global_object_identity_t::rand().to_str());

	auto ctx_(ctx.lock());
	if (!ctx_)
		THROW_MPF_EXC_FWD(nullptr);

	std::string stag;
	stag += "ox_rt1_stream_t";
	if (tag)
		stag += ":" + std::string(tag);
	stag += ":" + ctx_->curr_time_str() + ":" + global_object_identity_t::rand().to_str();

	return std::move(stag);
	}
#endif


ox_rt1_stream_t::rdm_key_holder_t::rdm_key_holder_t(std::weak_ptr<distributed_ctx_t> ctx_,
	const identity_descriptor_t & thisId_,
	const identity_descriptor_t & remoteId_,
	const char * tag)
	:

	/* ������� �� ������ !!! */
	_sessionId(CHECK_PTR(ctx_)->make_local_object_identity())
	, _rdmMsg(rtl_roadmap_obj_t::make_rt1_ostream(rt0_node_rdm_t(),
		thisId_,
		remoteId_,
		session_description_t(_sessionId, create_stag(ctx_, tag))))
	, _remoteRdm(rtl_roadmap_obj_t::make_rt1_ostream(
		rt0_node_rdm_t(),
		remoteId_,
		thisId_,
		session_description_t(_sessionId,
			_rdmMsg.session().name()))) {

	CBL_VERIFY(_rdmMsg.session() == _remoteRdm.session());
	}


bool ox_rt1_stream_t::rdm_key_holder_t::setval()const  noexcept {
	return _setf.load(std::memory_order::memory_order_acquire);
	}

bool ox_rt1_stream_t::rdm_key_holder_t::set_rt0(const rt0_node_rdm_t &nodeId) {

	post_scope_action_t entrySet([this] {
		if(!_setf.load(std::memory_order::memory_order_acquire))
			_entryf.store(false);
		});

	bool entryf = false;
	if(_entryf.compare_exchange_strong(entryf, true)) {
		CBL_VERIFY(!_setf.load(std::memory_order::memory_order_acquire));

		_rdmMsg.set_rt0(nodeId);
		_remoteRdm.set_rt0(nodeId);
		_setf.store(true, std::memory_order::memory_order_release);

		return true;
		}
	else
		return false;
	}

const rtl_roadmap_obj_t& ox_rt1_stream_t::rdm_key_holder_t::get_rdm_msg()const noexcept {
	if(setval())
		return _rdmMsg;
	else
		return rtl_roadmap_obj_t::null;
	}

const rtl_roadmap_obj_t& ox_rt1_stream_t::rdm_key_holder_t::get_remote_rdm()const noexcept {
	if(setval())
		return _remoteRdm;
	else
		return rtl_roadmap_obj_t::null;
	}

const session_description_t& ox_rt1_stream_t::rdm_key_holder_t::session()const noexcept {
	return _rdmMsg.session();
	}




ox_rt1_stream_t::__address_handler::__address_handler(std::weak_ptr<distributed_ctx_t> ctx_,
	const identity_descriptor_t & thisId_,
	const identity_descriptor_t & remoteId_,
	const rt1_endpoint_t & ep,
	const char * tag)
	: _ep(ep)
	, _thisId(thisId_)
	, _remoteId(remoteId_)
	, _locid(CHECK_PTR(ctx_)->make_local_object_identity())
	, _rdmEv(rtl_roadmap_event_t::make(thisId_, CHECK_PTR(ctx_)->make_event_identity(), rtl_table_t::out, rtl_level_t::l1))
	, _rdmPair(ctx_, thisId_, remoteId_, tag) {
	
	if(endpoint().endpoint_id().is_null())
		THROW_MPF_EXC_FWD(nullptr);

	if(remoteId_ != ep.endpoint_id())
		THROW_MPF_EXC_FWD(nullptr);

	if (_ep.address().empty() || _ep.port() == 0) {
		FATAL_ERROR_FWD(nullptr);
		}

	CBL_VERIFY(!is_null(direction()));
	}

const rtl_roadmap_link_t& ox_rt1_stream_t::__address_handler::msg_rdm_link()const noexcept {
	return _rdmPair.get_rdm_msg().link();
	}

const rt1_endpoint_t& ox_rt1_stream_t::__address_handler::endpoint()const noexcept {
	return _ep;
	}

bool ox_rt1_stream_t::__address_handler::rdm_registered()const noexcept {
	return _rdmPair.setval();
	}

bool ox_rt1_stream_t::__address_handler::set_rt0(const rt0_node_rdm_t &nodeId) {
	return _rdmPair.set_rt0(nodeId);
	}

const rtl_roadmap_event_t& ox_rt1_stream_t::__address_handler::ev_rdm()const  noexcept {
	return _rdmEv;
	}

const rtl_roadmap_obj_t& ox_rt1_stream_t::__address_handler::msg_rdm()const  noexcept {
	if(!rdm_registered()) {
		return rtl_roadmap_obj_t::null;
		}
	else
		return _rdmPair.get_rdm_msg();
	}

const rtl_roadmap_obj_t& ox_rt1_stream_t::__address_handler::this_rdm()const noexcept {
	return msg_rdm();
	}

const rtl_roadmap_obj_t& ox_rt1_stream_t::__address_handler::remote_rdm()const noexcept {
	if(!rdm_registered()) {
		return rtl_roadmap_obj_t::null;
		}
	else
		return _rdmPair.get_remote_rdm();
	}

const identity_descriptor_t& ox_rt1_stream_t::__address_handler::remote_id()const noexcept {
	return _remoteId;
	}

const identity_descriptor_t& ox_rt1_stream_t::__address_handler::this_id()const noexcept {
	return _thisId;
	}

const identity_descriptor_t& ox_rt1_stream_t::__address_handler::id()const noexcept {
	return remote_id();
	}

const ox_rt1_stream_t::__address_handler::xpeer_desc_t& ox_rt1_stream_t::__address_handler::desc()const noexcept {
	return this_rdm();
	}

const rt0_node_rdm_t& ox_rt1_stream_t::__address_handler::node_id()const noexcept {
	if(!rdm_registered()) {
		return rt0_node_rdm_t::null;
		}
	else {
		CBL_VERIFY(_rdmPair.get_rdm_msg().rt0() == _rdmPair.get_remote_rdm().rt0());
		return _rdmPair.get_rdm_msg().rt0();
		}
	}

const local_object_id_t& ox_rt1_stream_t::__address_handler::local_id()const noexcept {
	return _locid;
	}

const local_command_identity_t& ox_rt1_stream_t::__address_handler::local_object_command_id()const noexcept {
	return ev_rdm().lcid();
	}

const session_description_t& ox_rt1_stream_t::__address_handler::session()const noexcept {
	CBL_VERIFY((!rdm_registered() && !is_null(_rdmPair.get_rdm_msg().session()))
		|| _rdmPair.get_rdm_msg().session() == _rdmPair.get_remote_rdm().session());

	return _rdmPair.session();
	}

rtl_table_t ox_rt1_stream_t::__address_handler::direction()const noexcept {
	return rtl_table_t::out;
	}

const std::string& ox_rt1_stream_t::__address_handler::address()const noexcept {
	return _ep.address();
	}

int ox_rt1_stream_t::__address_handler::port()const noexcept {
	return _ep.port();
	}

ox_rt1_stream_t::__address_handler::remote_object_endpoint_t ox_rt1_stream_t::__address_handler::remote_object_endpoint()const noexcept {
	return _ep;
	}

connection_key_t ox_rt1_stream_t::__address_handler::connection_key()const noexcept {
	return _ep.to_key();
	}






