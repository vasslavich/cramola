#include "../../internal.h"
#include "../rt0_outbound_cnsettings.h"
#include "../../logging/logger.h"


using namespace crm;
using namespace crm::detail;


void rt0_connection_settings_t::assign(rt0_connection_settings_t&& o) {
	_thisId = std::move(o._thisId);
	_ep = std::move(o._ep);
	_inputStackCtr = std::move(o._inputStackCtr);
	_keepAlive = o._keepAlive;
	}

rt0_connection_settings_t::rt0_connection_settings_t(rt0_connection_settings_t&& o)noexcept {
	assign(std::move(o));
	}

rt0_connection_settings_t& rt0_connection_settings_t::operator=(rt0_connection_settings_t&& o)noexcept {
	assign(std::move(o));
	return (*this);
	}

const identity_descriptor_t& rt0_connection_settings_t::this_id()const noexcept {
	return _thisId;
	}

std::unique_ptr<i_endpoint_t> rt0_connection_settings_t::remote_endpoint()const noexcept {
	return _ep->copy();
	}

syncdata_list_t rt0_connection_settings_t::local_object_syncdata() {
	return syncdata_list_t();
	}

bool rt0_connection_settings_t::keep_alive()const noexcept {
	return _keepAlive;
	}

remote_object_connection_value_t rt0_connection_settings_t::local_object_identity() {
	identity_value_t identity;
	identity.id_value = _thisId;

	return remote_object_connection_value_t(std::move(identity), local_object_syncdata());
	}

bool rt0_connection_settings_t::remote_object_identity_validate(const identity_value_t&,
	std::unique_ptr<i_peer_remote_properties_t>&) {

	return true;
	}

bool rt0_connection_settings_t::connnection_exception_suppress_checker(connection_stage_t,
	const std::unique_ptr<dcf_exception_t>&)noexcept {

	return true;
	}

void rt0_connection_settings_t::local_object_initialize(datagram_t&& /*dt*/,
	object_initialize_result_guard_t&& resultHndl) {

	if (resultHndl)
		resultHndl(crm::datagram_t());
	}

void rt0_connection_settings_t::connection_handler(
	std::unique_ptr<dcf_exception_t>&&, xpeer_link&&) {}

void rt0_connection_settings_t::disconnection_handler(
	const i_xpeer_rt0_t::xpeer_desc_t&,
	std::shared_ptr<i_peer_statevalue_t>&&) {}

void rt0_connection_settings_t::event_handler(std::unique_ptr<i_event_t>&& ev) {
	if (ev)
		logger_t::logging("unhandled:event:uspace:", 375762);
	}

std::unique_ptr<i_input_messages_stock_t> rt0_connection_settings_t::input_stack() {
	return std::move(_inputStackCtr);
	}

