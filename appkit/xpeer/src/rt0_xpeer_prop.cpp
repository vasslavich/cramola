#include "../../internal.h"
#include "../rt0_xpeer.h"
#include "../../xpeer_invoke/xpeer_invoke_trait.h"


using namespace crm;
using namespace crm::detail;


static_assert(is_xpeer_trait_z_v<decltype(std::declval< xpeer_base_rt0_t>().get_link())>, __FILE__);


std::string xpeer_base_rt0_t::address_trace()const {
	return address() + ":" + std::to_string(port())
		+ ":t=" + this_id().to_str()
		+ ":r=" + remote_id().to_str()
		+ ":d=" + cvt2string(peer_direction())
		+ ":h=" + session().object_hash().to_str();
	}

std::shared_ptr<distributed_ctx_t> xpeer_base_rt0_t::ctx() noexcept {
	return _ctx.lock();
	}

std::shared_ptr<distributed_ctx_t> xpeer_base_rt0_t::ctx()const noexcept {
	return _ctx.lock();
	}

xpeer_base_rt0_t::link_t xpeer_base_rt0_t::get_link()noexcept {
	return link_t(shared_from_this(), _addrh);
	}

xpeer_base_rt0_t::clink_t xpeer_base_rt0_t::get_link()const noexcept {
	return clink_t(shared_from_this(), _addrh);
	}

std::shared_ptr<const i_peer_remote_properties_t> xpeer_base_rt0_t::remote_properties()const noexcept {
	return std::atomic_load_explicit(&_remoteProp, std::memory_order::memory_order_acquire);
	}

bool xpeer_base_rt0_t::renewable_connection()const noexcept {
	return false;
	}

void xpeer_base_rt0_t::set_remote_properties(std::unique_ptr<i_peer_remote_properties_t> && prop)noexcept {

	std::atomic_store_explicit(&_remoteProp,
		std::shared_ptr<i_peer_remote_properties_t>(std::move(prop)),
		std::memory_order::memory_order_release);
	}

connection_key_t xpeer_base_rt0_t::connection_key()const noexcept {
	CBL_VERIFY(_addrh);
	return _addrh->connection_key();
	}

remote_object_state_t xpeer_base_rt0_t::state()const noexcept {
	CBL_VERIFY(_addrh);
	return _addrh->state();
	}

bool xpeer_base_rt0_t::closed()const noexcept {
	return state() == remote_object_state_t::closed || _closedf.load(std::memory_order::memory_order_acquire);
	}

const session_description_t& xpeer_base_rt0_t::session()const noexcept {
	CBL_VERIFY(_addrh);

	static_assert(std::is_lvalue_reference_v<decltype(_addrh->session())>, __CBL_FILEW__);
	return _addrh->session();
	}

const xpeer_base_rt0_t::rdm_local_identity_t& xpeer_base_rt0_t::rdm_local_identity()const noexcept {
	CBL_VERIFY(_addrh);

	static_assert(std::is_lvalue_reference_v<decltype(_addrh->rdm_local_identity())>, __CBL_FILEW__);
	return _addrh->rdm_local_identity();
	}

const xpeer_base_rt0_t::xpeer_desc_t& xpeer_base_rt0_t::desc()const noexcept {
	CBL_VERIFY(_addrh);

	static_assert(std::is_lvalue_reference_v<decltype(_addrh->desc())>, __CBL_FILEW__);
	return _addrh->desc();
	}

const local_object_id_t& xpeer_base_rt0_t::local_id()const noexcept {
	CBL_VERIFY(_addrh);

	static_assert(std::is_lvalue_reference_v<decltype(_addrh->local_id())>, __CBL_FILEW__);
	return _addrh->local_id();
	}

const rt0_node_rdm_t& xpeer_base_rt0_t::rt0rdm()const noexcept {
	CBL_VERIFY(_addrh);

	static_assert(std::is_lvalue_reference_v<decltype(_addrh->rt0rdm())>, __CBL_FILEW__);
	return _addrh->rt0rdm();
	}

rtl_table_t xpeer_base_rt0_t::direction()const noexcept {
	CBL_VERIFY(_addrh);
	return _addrh->direction();
	}

xpeer_base_rt0_t::remote_object_endpoint_t xpeer_base_rt0_t::remote_object_endpoint()const noexcept {
	CBL_VERIFY(_addrh);
	return _addrh->remote_object_endpoint();
	}

rtl_level_t xpeer_base_rt0_t::level()const noexcept{
	CBL_VERIFY(_addrh);
	return _addrh->level();
	}

const identity_descriptor_t& xpeer_base_rt0_t::this_id()const noexcept{
	CBL_VERIFY(_addrh);

	static_assert(std::is_lvalue_reference_v<decltype(_addrh->this_id())>, __CBL_FILEW__);
	return _addrh->this_id();
	}

const identity_descriptor_t& xpeer_base_rt0_t::remote_id()const noexcept {
	CBL_VERIFY(_addrh);

	static_assert(std::is_lvalue_reference_v<decltype(_addrh->remote_id())>, __CBL_FILEW__);
	return _addrh->remote_id();
	}

const identity_descriptor_t& xpeer_base_rt0_t::id()const noexcept {
	CBL_VERIFY(_addrh);

	static_assert(std::is_lvalue_reference_v<decltype(_addrh->id())>, __CBL_FILEW__);
	return _addrh->id();
	}

void xpeer_base_rt0_t::set_id(const identity_descriptor_t  &id) {
	CBL_VERIFY(_addrh);
	_addrh->set_remote_id(id);
	}

bool xpeer_base_rt0_t::is_opened()const noexcept {
	return !closed();
	}

const std::string& xpeer_base_rt0_t::address()const noexcept {
	CBL_VERIFY(_addrh);

	static_assert(std::is_lvalue_reference_v<decltype(_addrh->address())>, __CBL_FILEW__);
	return _addrh->address();
	}

int xpeer_base_rt0_t::port()const noexcept {
	CBL_VERIFY(_addrh);
	return _addrh->port();
	}

void xpeer_base_rt0_t::set_state(remote_object_state_t st)noexcept {
	CBL_VERIFY(_addrh);
	_addrh->set_state(st);
	}

std::shared_ptr<xpeer_base_rt0_t::input_stack_t> xpeer_base_rt0_t::get_input_queue() noexcept {
	return std::atomic_load_explicit(&_inQueue, std::memory_order::memory_order_acquire);
	}

std::shared_ptr<xpeer_base_rt0_t::input_stack_t> xpeer_base_rt0_t::get_input_queue() const noexcept {
	return std::atomic_load_explicit(&_inQueue, std::memory_order::memory_order_acquire);
	}

std::shared_ptr<i_peer_statevalue_t>  xpeer_base_rt0_t::state_value() noexcept {
	return std::atomic_load_explicit(&_stateValue, std::memory_order::memory_order_acquire);
	}

std::shared_ptr<const i_peer_statevalue_t>  xpeer_base_rt0_t::state_value()const noexcept {
	return std::atomic_load_explicit(&_stateValue, std::memory_order::memory_order_acquire);
	}

void  xpeer_base_rt0_t::set_state_value(std::shared_ptr<i_peer_statevalue_t> && stv)noexcept {
	std::atomic_exchange(&_stateValue, std::move(stv));
	}

std::shared_ptr<i_x0_xpeer_address_handler> xpeer_base_rt0_t::address_handler()const noexcept {
	return _addrh;
	}

event_subscribe_assist_t::event_handler_t &xpeer_base_rt0_t::get_handler(){
	return _evhLst;
	}

peer_direction_t xpeer_base_rt0_t::peer_direction()const noexcept {
	CBL_VERIFY(_addrh);
	return _addrh->peer_direction();
	}

const std::string& xpeer_base_rt0_t::name()const noexcept {
	CBL_VERIFY(_addrh);
	return _addrh->name();
	}

