#include "../../internal.h"
#include "../rt1_opeer.h"
#include "../../context/context.h"
#include "../rt1_ostream.h"
#include "../rt1_end_opeer.h"
#include "../../xpeer_invoke/xpeer_asendrecv.h"
#include "../i_input_handlers.h"


using namespace crm;
using namespace crm::detail;


#ifdef CBL_MPF_GLOBAL_TRACEWAY_COUNTERS
std::array<std::atomic<std::intmax_t>, (size_t)remote_object_state_t::closed>  GlobalOutboundConnectionsStageCounters{0};

size_t crm::detail::get_stat_counter_outbound_connections_counter(remote_object_state_t st)noexcept {
	return GlobalOutboundConnectionsStageCounters.at((size_t)st).load(std::memory_order::memory_order_acquire);
	}

std::shared_ptr<post_scope_action_t<std::function<void()>>> make_global_outbound_connection_decrementer(remote_object_state_t st)noexcept {
	auto p = std::make_shared<post_scope_action_t<std::function<void()>>>([st] {
		--GlobalOutboundConnectionsStageCounters.at((size_t)st);
		});

	++GlobalOutboundConnectionsStageCounters.at((size_t)st);

	return p;
	}
#else
size_t crm::detail::get_stat_counter_outbound_connections_counter(remote_object_state_t )noexcept { return 0; }
#endif


/*=========================================================================
xpeer_out_rt1_t
===========================================================================*/

remote_connection_desc_t xpeer_out_rt1_t::remote_connection_descriptor()const noexcept {
	remote_connection_desc_t desc;
	desc.cnk = connection_key();
	desc.remoteId = remote_id();
	desc.thisId = this_id();

	return desc;
	}

std::shared_ptr<ox_rt1_stream_t> xpeer_out_rt1_t::osource()noexcept {
	auto isrc(source_stream());
	if (isrc) {
		auto osrc = std::dynamic_pointer_cast<ox_rt1_stream_t>(isrc);
		if (osrc)
			return osrc;
		else {
			return nullptr;
			}
		}
	else {
		return nullptr;
		}
	}


void xpeer_out_rt1_t::_session_create(std::weak_ptr<__rtx_out_connection_ctx_timered_t> connCtx,
	ctr_1_input_queue_t && qinput) {

	CBL_MPF_TRACELEVEL_EVENT_RT1_CONNECTION_TRACE("outbound_connection:session create(local):s="
		+ CHECK_PTR(connCtx)->endpoint()->to_string()
		+ ":t=" + this_id().to_str()
		+ ":session=" + session().object_hash().to_str());

	_on_session_create(std::move(connCtx), std::move(qinput));
	}

void xpeer_out_rt1_t::_on_session_create(std::weak_ptr<__rtx_out_connection_ctx_timered_t> connCtx,
	ctr_1_input_queue_t && qinput) {

	CBL_MPF_TRACELEVEL_EVENT_RT1_CONNECTION_TRACE("outbound_connection:session create end(local):s="
		+ CHECK_PTR(connCtx)->endpoint()->to_string()
		+ ":t=" + this_id().to_str()
		+ ":session=" + session().object_hash().to_str());

	set_handshake_mode(qinput ? qinput() : std::shared_ptr<input_stack_t>());

	/* ������� � ����� �������� ���������� �������� ������ ������������������ ������� */
	start_handshake(std::move(connCtx));

	MPF_TEST_EMULATION_IN_CONTEXT_EXPR_COND(ctx(), 320, is_trait_at_emulation_context(level()));
	}


void xpeer_out_rt1_t::handshake_exception_handler(std::weak_ptr<__rtx_out_connection_ctx_timered_t> connCtx,
	std::unique_ptr<dcf_exception_t> && ntf) noexcept {

	close();

	if (auto ctx_ = ctx()) {
		auto basefExceptionHndl = [connCtx](std::unique_ptr<dcf_exception_t> && ntf_) {
			if (auto pctx = connCtx.lock()) {
				CHECK_NO_EXCEPTION(pctx->invoke_error(std::move(ntf_)));
				}
			};

		if (_remoteExceptionChecker) {

			auto extf = _remoteExceptionChecker;
			auto nextf = [extf, connCtx, basefExceptionHndl](std::unique_ptr<dcf_exception_t> && ntf_) {
				if (auto pctx = connCtx.lock()) {
					try {
						bool extr = false;
						CHECK_NO_EXCEPTION(extr = extf(connection_stage_t::impersonate, ntf_));

						if (!extr) {
							CHECK_NO_EXCEPTION(pctx->invoke_error(std::move(ntf_), reconnect_setup_t::no));
							}
						else {
							basefExceptionHndl(nullptr);
							}
						}
					catch (const dcf_exception_t & exc0) {
						basefExceptionHndl(exc0.dcpy());
						}
					catch (const std::exception & exc1) {
						basefExceptionHndl(CREATE_MPF_PTR_EXC_FWD(exc1));
						}
					catch (...) {
						basefExceptionHndl(nullptr);
						}
					}
				};

			ctx_->launch_async_exthndl(__FILE_LINE__, std::move(nextf), std::move(ntf));
			}
		else {
			basefExceptionHndl(std::move(ntf));
			}
		}
	}


void xpeer_out_rt1_t::start_connection_request(std::weak_ptr<__rtx_out_connection_ctx_timered_t> cctx_) {

#ifdef CBL_MPF_GLOBAL_TRACEWAY_COUNTERS
	auto pdec = make_global_outbound_connection_decrementer(remote_object_state_t::connect_stage);
#endif

	try {
		auto timelineRequest = CHECK_PTR(cctx_)->timeline();

		/* ����������� ������� */
		auto reqCtr = [wthis = weak_from_this()]{
			if (auto sthis = wthis.lock()) {
				CBL_VERIFY(!is_null(sthis->this_rdm().session()));
				CBL_VERIFY(!is_null(sthis->this_id()));
				CBL_VERIFY(!is_null(sthis->session()));

				return oconnection_t(sthis->this_rdm().session());
				}
			else {
				THROW_MPF_EXC_FWD(nullptr);
				}
			};

		/* ���������� ������ */
		auto resHndl = [wthis = weak_from_this(), cctx_

#ifdef CBL_MPF_GLOBAL_TRACEWAY_COUNTERS
			, pdec
#endif
		](message_handler && m)noexcept {
			if (m.has_result()) {
				CBL_MPF_KEYTRACE_SET((*m.source_message()), 400);
				}

			if (auto sthis = std::dynamic_pointer_cast<self_t>(wthis.lock())) {
				auto exceptionHandler = [&cctx_, &sthis](std::unique_ptr<dcf_exception_t> && exc_)noexcept {
					CBL_VERIFY(exc_);
					sthis->handshake_exception_handler(cctx_, std::move(exc_));
					};

				if (!sthis->closed()) {
					try {
#ifdef CBL_USE_SEQUENTIAL_CALL_CHECKER
						auto seqHld = sthis->inc_scope_seqcall();
#endif

						if (auto pctx = cctx_.lock()) {
							if (m.has_result()) {
								auto iReq = std::move(m).as<irequest_t>();

								CBL_MPF_TRACELEVEL_EVENT_RT1_CONNECTION_TRACE("outbound_connection:session initialize(remote) end:s="
									+ pctx->endpoint()->to_string()
									+ ":t=" + sthis->this_id().to_str()
									+ ":session=" + sthis->session().object_hash().to_str());

								CBL_VERIFY(!is_null(iReq.address().destination_id()) &&
									iReq.address().destination_id() == sthis->this_id() &&
									sthis->this_id().id().at<size_t>(0) != 0);

								if (iReq.source_id() != sthis->remote_id() || iReq.destination_id() != sthis->this_id()) {
									CHECK_NO_EXCEPTION(pctx->invoke_error(CREATE_MPF_PTR_EXC_FWD(nullptr)));
									}
								/* ��������� �� ���������� ������ �������������� � ������� ������ */
								if (iReq.type().type() == iol_types_t::st_disconnection) {
									CHECK_NO_EXCEPTION(pctx->invoke_error(CREATE_MPF_PTR_EXC_FWD(nullptr)));
									}

								if (iReq.ucode() != irequest_t::what_t::identity) {
									CHECK_NO_EXCEPTION(pctx->invoke_error(CREATE_MPF_PTR_EXC_FWD(nullptr)));
									}

								sthis->set_state(remote_object_state_t::connect_stage_ready);

								MPF_TEST_EMULATION_IN_CONTEXT_O_PEER_MESSAGE_COND(sthis->ctx(), 321, is_trait_at_emulation_context(iReq));

								/* ������� � ��������� ��������� */
								if (auto c = sthis->ctx()) {
									c->launch_async(__FILE_LINE__, [sthis, cctx_] {
										sthis->on_connection_request(std::move(cctx_));
										});
									}

								iReq.reset_invoke_exception_guard().reset();
								}
							else if (m.has_exception()) {
								CBL_MPF_TRACELEVEL_EVENT_RT1_CONNECTION_TRACE("outbound_connection:session initialize(remote) end:s="
									+ pctx->endpoint()->to_string()
									+ ":t=" + sthis->this_id().to_str()
									+ ":session=" + sthis->session().object_hash().to_str()
									+ ":exception=" + cvt(m.exception()->msg() + L":" + m.exception()->file() + L":" + std::to_wstring(m.exception()->line())));

								exceptionHandler(std::move(m).exception());
								}
							else {
								CBL_MPF_TRACELEVEL_EVENT_RT1_CONNECTION_TRACE("outbound_connection:session initialize(remote) end:s="
									+ pctx->endpoint()->to_string()
									+ ":t=" + sthis->this_id().to_str()
									+ ":session=" + sthis->session().object_hash().to_str()
									+ ":result=" + cvt2string(m.state()));

								exceptionHandler(CREATE_MPF_PTR_EXC_FWD(nullptr));
								}
							}
						else {
							exceptionHandler(CREATE_MPF_PTR_EXC_FWD(nullptr));
							}
						}
					catch (const dcf_exception_t & exc0) {
						exceptionHandler(exc0.dcpy());
						}
					catch (const std::exception & exc1) {
						exceptionHandler(std::make_unique<dcf_exception_t>(exc1));
						}
					catch (...) {
						exceptionHandler(CREATE_MPF_PTR_EXC_FWD(nullptr));
						}
					}
				}
			else {
				if (auto c = cctx_.lock()) {
					c->invoke_error(CREATE_MPF_PTR_EXC_FWD(nullptr));
					}
				}
			};

		auto ctx_(ctx());
		if (ctx_) {
			make_async_sndrcv_once_sys(ctx_,
				get_link(), 
				std::move(reqCtr),
				std::move(resHndl),
				(__FILE_LINE__ + std::string(":") + remote_object_endpoint().to_string() + ":" + this_id().to_str()),
				timelineRequest,
				launch_as::async,
				inovked_as::sync );

			CBL_MPF_TRACELEVEL_EVENT_RT1_CONNECTION_TRACE("outbound_connection:session initialize(remote) begin:s="
				+ remote_object_endpoint().to_string()
				+ ":t=" + this_id().to_str()
				+ ":session=" + session().object_hash().to_str());
			}
		}
	catch (const dcf_exception_t & e0) {
		handshake_exception_handler(cctx_, e0.dcpy());
		}
	catch (const std::exception & e1) {
		handshake_exception_handler(cctx_, CREATE_MPF_PTR_EXC_FWD(e1));
		}
	catch (...) {
		handshake_exception_handler(cctx_, CREATE_MPF_PTR_EXC_FWD(nullptr));
		}
	}

void xpeer_out_rt1_t::on_connection_request(std::weak_ptr<__rtx_out_connection_ctx_timered_t> connCtx) {

#ifdef CBL_MPF_GLOBAL_TRACEWAY_COUNTERS
	auto pdec = make_global_outbound_connection_decrementer(remote_object_state_t::identity_stage);
#endif

	try {
		set_state(remote_object_state_t::identity_stage);

		auto requestTimeline = CHECK_PTR(connCtx)->timeline();

		/* ����������� ������� */
		auto reqCtr = [wptr = weak_from_this(), connCtx]{
			if (auto sctx = connCtx.lock()) {

				/* ��������� ����������������� ���������� ������� ������������� */
				auto idVal = sctx->ctr_local_id();
				if (is_null(idVal.identity().id_value))
					THROW_MPF_EXC_FWD(L"bad identity constructor");

				/* ��������� � ������������������ ������� */
				osendrecv_datagram_t msg{};
				msg.set_key(command_name_identity);
				msg.value().add_value("", idVal);
				msg.set_destination_subscriber_id(subscribe_address_datagram_for_this);

				return msg;
				}
			else {
				THROW_EXC_FWD(nullptr);
				}
			};

		/* ���������� ������ */
		auto resHndl = [wthis = weak_from_this(), connCtx
#ifdef CBL_MPF_GLOBAL_TRACEWAY_COUNTERS
			, pdec
#endif
		](message_handler && m)noexcept {
			if (m.has_result()) {
				CBL_MPF_KEYTRACE_SET((*m.source_message()), 401);
				}

			if (auto sptr = std::dynamic_pointer_cast<xpeer_out_rt1_t>(wthis.lock())) {
				if (!sptr->closed()) {

					auto exceptionHandler = [&sptr, connCtx](std::unique_ptr<dcf_exception_t>&& exc)noexcept {
						CHECK_NO_EXCEPTION(sptr->handshake_exception_handler(connCtx, std::move(exc)));
						};

					try {
#ifdef CBL_USE_SEQUENTIAL_CALL_CHECKER
						auto seqHld = sptr->inc_scope_seqcall();
#endif

						if (auto pctx = connCtx.lock()) {

							CBL_MPF_TRACELEVEL_EVENT_RT1_CONNECTION_TRACE("outbound_connection:session sync end:s="
								+ pctx->endpoint()->to_string()
								+ ":t=" + sptr->this_id().to_str()
								+ ":session=" + sptr->session().object_hash().to_str()
								+ ":result=" + cvt2string(reuslt));

							if (m.has_result()) {
								auto iDtg = std::move(m).as<isendrecv_datagram_t>();

								CBL_VERIFY(!is_null(iDtg.address().destination_id()) &&
									iDtg.address().destination_id() == sptr->this_id() &&
									sptr->this_id().id.at<size_t>(0) != 0);

								if (iDtg.key() != command_name_identity) {
									CHECK_NO_EXCEPTION(pctx->invoke_error(CREATE_MPF_PTR_EXC_FWD(nullptr)));
									}

								auto cvRemote(iDtg.value().get_value<remote_object_connection_value_t>("", 0));
								std::unique_ptr<i_peer_remote_properties_t> remoteProp;

								if (sptr->remote_id() != cvRemote.identity().id_value) {
									CHECK_NO_EXCEPTION(pctx->invoke_error(CREATE_MPF_PTR_EXC_FWD(nullptr)));
									}

								if (!pctx->checker_remote_id(cvRemote.identity(), remoteProp)) {
									CHECK_NO_EXCEPTION(pctx->invoke_error(CREATE_MPF_PTR_EXC_FWD(nullptr)));
									}

								/* ��������� ��������� �������������
								-----------------------------------------------*/
								const auto pPropId = cvRemote.identity().properties.find_by_key(identity_properties_id_desc);
								if (pPropId) {
									const auto idValue = base_peer_identity_properties_t::dsrlz_(pPropId->data);
									}

								/* �������� ��������� �������
								----------------------------------------------*/
								if (remoteProp)
									sptr->set_remote_properties(std::move(remoteProp));

								MPF_TEST_EMULATION_IN_CONTEXT_O_PEER_MESSAGE_COND(sptr->ctx(), 322, is_trait_at_emulation_context(iDtg));

								sptr->set_state(remote_object_state_t::identity_stage_ready);

								iDtg.reset_invoke_exception_guard().reset();
								}
							else {
								exceptionHandler(CREATE_MPF_PTR_EXC_FWD(nullptr));
								}
							}
						else if (m.has_exception()) {
							exceptionHandler(std::move(m).exception());
							}
						else {
							exceptionHandler(CREATE_MPF_PTR_EXC_FWD(nullptr));
							}
						}
					catch (const dcf_exception_t & exc0) {
						exceptionHandler(exc0.dcpy());
						}
					catch (const std::exception & exc1) {
						exceptionHandler(std::make_unique<dcf_exception_t>(exc1));
						}
					catch (...) {
						exceptionHandler(CREATE_PTR_EXC_FWD(nullptr));
						}
					}
				}
			else {
				if (auto c = connCtx.lock()) {
					c->invoke_error(CREATE_MPF_PTR_EXC_FWD(nullptr));
					}
				}
			};

		auto ctx_(ctx());
		if (ctx_) {
			make_async_sndrcv_once_sys(ctx_, 
				get_link(),
				std::move(reqCtr),
				std::move(resHndl),
				(__FILE_LINE__ + std::string(":") + CHECK_PTR(connCtx)->endpoint()->to_string() + ":" + this_id().to_str()),
				CHECK_PTR(connCtx)->timeline(),
				launch_as::async,
				inovked_as::sync );

			CBL_MPF_TRACELEVEL_EVENT_RT1_CONNECTION_TRACE("outbound_connection:session sync begin:s="
				+ CHECK_PTR(connCtx)->endpoint()->to_string()
				+ ":t=" + this_id().to_str()
				+ ":session=" + session().object_hash().to_str());
			}
		}
	catch (const dcf_exception_t & e0) {
		handshake_exception_handler(connCtx, e0.dcpy());
		}
	catch (const std::exception & e1) {
		handshake_exception_handler(connCtx, CREATE_MPF_PTR_EXC_FWD(e1));
		}
	catch (...) {
		handshake_exception_handler(connCtx, CREATE_MPF_PTR_EXC_FWD(nullptr));
		}
	}


/*! ������� ��������� �������� ����������������� ������ �� �������, ��������� �������� */
void xpeer_out_rt1_t::start_handshake(std::weak_ptr<__rtx_out_connection_ctx_timered_t> connCtx) {

	CBL_MPF_TRACELEVEL_EVENT_RT1_CONNECTION_TRACE("outbound_connection:session handshake begin(local):s="
		+ CHECK_PTR(connCtx)->endpoint()->to_string()
		+ ":t=" + this_id().to_str()
		+ ":session=" + session().object_hash().to_str());

	/* ������ ������ ������-������ */
	io_rw_listen();

	/* ����� ��������� �������� ������������� */
	start_connection_request(std::move(connCtx));

	MPF_TEST_EMULATION_IN_CONTEXT_EXPR_COND(ctx(), 323, is_trait_at_emulation_context(level()));
	}

/*! ��������� �������� ������������� ������ ������� */
void xpeer_out_rt1_t::on_handshake_success() {

	/* ��������� �������� ���������� */
	handshake_end(true);
	set_state(remote_object_state_t::impersonated);

	CBL_MPF_TRACELEVEL_EVENT_RT1_CONNECTION_TRACE("outbound_connection:established:s="
		+ desc().remote_object_endpoint().to_string()
		+ ":t=" + this_id().to_str()
		+ ":session=" + session().object_hash().to_str());

	MPF_TEST_EMULATION_IN_CONTEXT_EXPR_COND(ctx(), 324, is_trait_at_emulation_context(level()));
	}

/* ������������� ���������� �������-����������� */
xpeer_out_rt1_t::xpeer_out_rt1_t(const std::weak_ptr<distributed_ctx_t> & ctx,
	std::weak_ptr<i_rt1_main_router_t> rt,
	std::shared_ptr<ox_rt1_stream_t> && source_,
	std::unique_ptr<gtw_rt1_x_t> && gtw,
	rtx_object_closed_t && closeHndl,
	std::shared_ptr<i_peer_statevalue_t> && stval,
	__event_handler_point_f && xh,
	exceptions_suppress_checker_t && remoteExcChecker)
	: base_t(ctx,
		std::move(source_),
		std::move(gtw),
		std::move(closeHndl),
		std::move(stval),
		std::move(xh))
	, _rt(rt)
	, _remoteExceptionChecker(std::move(remoteExcChecker)) {
	}

std::shared_ptr<__initialize_state_t> xpeer_out_rt1_t::initialize_state()noexcept {
	return std::atomic_exchange(&_iniSt, std::shared_ptr<__initialize_state_t>{});
	}

void xpeer_out_rt1_t::handshake_state_register(std::shared_ptr<__rtx_out_connection_ctx_timered_t> ctx,
	object_initialize_handler_t && initializeHndl) {

	if (initializeHndl) {
		auto pIniSt(std::make_shared<detail::__initialize_state_t>(std::move(ctx), std::move(initializeHndl)));
		auto old(std::atomic_exchange(&_iniSt, std::move(pIniSt)));
		if (old) {
			THROW_MPF_EXC_FWD(nullptr);
			}
		}
	}

void xpeer_out_rt1_t::command_handler_dtg_identity(std::unique_ptr<isendrecv_datagram_t> &&) {
	FATAL_ERROR_FWD(nullptr);
	}

void xpeer_out_rt1_t::command_handler_dgt_initialize(std::unique_ptr<isendrecv_datagram_t> && iDtg){
	if (iDtg){

		CBL_VERIFY(!is_null(iDtg->address().destination_id()) &&
			iDtg->address().destination_id() == this_id() &&
			this_id().id.at<size_t>(0) != 0);

		CBL_VERIFY(state() == remote_object_state_t::identity_stage_ready || state() >= remote_object_state_t::closed);

#ifdef CBL_MPF_MESSAGE_TRACEWAY_KEYS_BY_ID
		auto ktid = trait_at_keytrace_set((*iDtg));
#endif
		CBL_MPF_KEYTRACE_SET(ktid, 402);

#ifdef CBL_MPF_GLOBAL_TRACEWAY_COUNTERS
		auto pdec = make_global_outbound_connection_decrementer(remote_object_state_t::connect_stage);
#endif

		CBL_MPF_TRACELEVEL_EVENT_RT1_CONNECTION_TRACE("outbound_connection:initialize setup:s="
			+ remote_object_endpoint().to_string()
			+ ":t=" + this_id().to_str()
			+ ":session=" + session().object_hash().to_str());

		/* ����������� ����������������� ������ ��������� ���� */
		auto initializeHndl(initialize_state());
		if (initializeHndl){

			if (auto cnx = std::dynamic_pointer_cast<__rtx_out_connection_ctx_timered_t>(std::move(*initializeHndl).cnx())){

				/* ������� �������� ���������� */
				auto excSend = [wptr = weak_from_this(), cnx

#ifdef CBL_MPF_MESSAGE_TRACEWAY_KEYS_BY_ID
					, ktid
#endif
#ifdef CBL_MPF_GLOBAL_TRACEWAY_COUNTERS
					, pdec
#endif
				](
					std::unique_ptr<isendrecv_datagram_t> && iDtg, std::unique_ptr<dcf_exception_t> && exc_) noexcept {

					CBL_VERIFY(exc_);
					CBL_MPF_KEYTRACE_SET(ktid, 406);

					if (cnx){
						cnx->invoke_error(exc_->dcpy());
						}

					/* �������� � ����� */
					if (iDtg){
						iDtg->invoke_reverse_exception(exc_->dcpy());
						}

					if (auto sptr = std::dynamic_pointer_cast<self_t>(wptr.lock())){

						/* ����������� �������� ���� */
						if (auto rt = sptr->_rt.lock()){
							CHECK_NO_EXCEPTION(rt->opeer_fail(sptr->remote_connection_descriptor(), std::move(exc_)));
							}
						}
					};

				try{
					MPF_TEST_EMULATION_IN_CONTEXT_O_PEER_MESSAGE_COND(ctx(), 325, is_trait_at_emulation_context(*iDtg));

					auto value = iDtg->value_release();

					/* ������� ��������� ������ ����� ���������� ������� ������������� */
					auto resultHndl = crm::utility::bind_once_call( "xpeer_out_rt1_t::command_handler_dgt_initialize", [wptr = weak_from_this(), cnx

#ifdef CBL_MPF_GLOBAL_TRACEWAY_COUNTERS
						, pdec
#endif
					](initialize_result_desc_t && result,
						decltype(excSend) && excSend_,
						std::unique_ptr<isendrecv_datagram_t> && iDtg)noexcept {

						CBL_MPF_KEYTRACE_SET((*iDtg), 405);

						if (auto sthis = std::dynamic_pointer_cast<self_t>(wptr.lock())){
							try{
#ifdef CBL_USE_SEQUENTIAL_CALL_CHECKER
								auto seqHld = sthis->inc_scope_seqcall();
#endif

								CBL_MPF_TRACELEVEL_EVENT_RT1_CONNECTION_TRACE("outbound_connection:initialize setup end:s="
									+ sthis->remote_object_endpoint().to_string()
									+ ":t=" + sthis->this_id().to_str()
									+ ":session=" + sthis->session().object_hash().to_str()
									+ ":" + (result.exception ? cvt(result.exception->msg()) : "ok"));

								if (!result.exception){

									MPF_TEST_EMULATION_IN_CONTEXT_O_PEER_MESSAGE_COND(sthis->ctx(), 326, is_trait_at_emulation_context(*iDtg));

									/* ��������� � ����������� ������������� */
									osendrecv_datagram_t msg(std::move(result.responseValue));
									msg.set_key(command_name_initialize);
									msg.capture_response_tail((*iDtg));

									CBL_VERIFY(!is_trait_at_emulation_context(msg));

									sthis->set_state(remote_object_state_t::initialize_stage_ready);

									/* �������� ��������� � ����� */
									sthis->push(std::move(msg));

									cnx->invoke_ready();
									}
								else{
									excSend_(std::move(iDtg), std::move(result.exception));
									}
								}
							catch (const dcf_exception_t & e0){
								excSend_(std::move(iDtg), e0.dcpy());
								}
							catch (const std::exception & e1){
								excSend_(std::move(iDtg), CREATE_MPF_PTR_EXC_FWD(e1));
								}
							catch (...){
								excSend_(std::move(iDtg), CREATE_MPF_PTR_EXC_FWD(nullptr));
								}
							}
						}, std::placeholders::_1, std::move(excSend), std::move(iDtg));

					CBL_MPF_KEYTRACE_SET(ktid, 403);

					if (auto c = ctx()){
						c->launch_async(async_space_t::ext, __FILE_LINE__,
							[initializeHndl
#ifdef CBL_MPF_MESSAGE_TRACEWAY_KEYS_BY_ID
							, ktid
#endif
							](decltype(value) && v, object_initialize_result_guard_t && g){

							CBL_MPF_KEYTRACE_SET(ktid, 404);

							initializeHndl->invoke(std::move(v), std::move(g));
							}, std::move(value), object_initialize_result_guard_t(ctx(), async_space_t::sys, detail::inovked_as::async, std::move(resultHndl)));
						}
					}
				catch (const dcf_exception_t & exc0){
					excSend(std::move(iDtg), exc0.dcpy());
					}
				catch (const std::exception & exc1){
					excSend(std::move(iDtg), CREATE_PTR_EXC_FWD(exc1));
					}
				catch (...){
					excSend(std::move(iDtg), CREATE_PTR_EXC_FWD(nullptr));
					}
				}
			else{
				close();
				THROW_MPF_EXC_FWD(nullptr);
				}
			}
		else{
			close();
			THROW_MPF_EXC_FWD(nullptr);
			}
		}
	else{
		close();
		THROW_MPF_EXC_FWD(nullptr);
		}
	}


void xpeer_out_rt1_t::connection_result_handler(crm::async_operation_result_t result,
	std::unique_ptr<dcf_exception_t> && exc,
	reconnect_setup_t rc,
	std::unique_ptr<__outbound_connection_result_t<link_t>> && pHndl)noexcept {

	auto exchndl = [this, &pHndl, rc](std::unique_ptr<dcf_exception_t> && e)noexcept {
		if (pHndl) {
			CHECK_NO_EXCEPTION(pHndl->refuse(std::move(e), rc));
			CHECK_NO_EXCEPTION(close());
			}
		};

	try {
		CBL_MPF_TRACELEVEL_EVENT_RT1_CONNECTION_TRACE("connection context:resume:s="
			+ remote_object_endpoint().to_string()
			+ ":t=" + this_id().to_str()
			+ ":session=" + session().object_hash().to_str()
			+ ":" + std::to_string((int)rc));

		if (result == crm::async_operation_result_t::st_ready) {
			on_extern_completed([&] {
				if (pHndl) {
					MPF_TEST_EMULATION_IN_CONTEXT_EXPR_COND(ctx(), 327, is_trait_at_emulation_context(level()));
				
					pHndl->ready(get_link());
					invoke_event(event_type_t::st_connected);
					}
				});
			}
		else if (exc) {
			if (pHndl) {
				CHECK_NO_EXCEPTION(pHndl->refuse(std::move(exc), rc));
				}
			}

		}
	catch (const dcf_exception_t & e0) {
		exchndl(e0.dcpy());
		}
	catch (const std::exception & e1) {
		exchndl(CREATE_PTR_EXC_FWD(e1));
		}
	catch (...) {
		exchndl(CREATE_MPF_PTR_EXC_FWD(nullptr));
		}
	}

xpeer_out_rt1_t::indentity_tail xpeer_out_rt1_t::make_identity_handler(
	std::weak_ptr<self_t> wptr,
	connection_result_t && connectionHndl)noexcept {

	if (auto sptr = wptr.lock()) {
		using connection_handler_kp = __outbound_connection_result_t<link_t>;

		CBL_MPF_IF_NOT_ERROR_EMULATION(CBL_VERIFY(sptr.use_count() == 2));
		auto h = [wptr](crm::async_operation_result_t result,
			std::unique_ptr<dcf_exception_t> && exc,
			reconnect_setup_t rc,
			std::unique_ptr<__outbound_connection_result_t<link_t>> && pHndl)noexcept {

			if (auto sptr = wptr.lock()) {
				CHECK_NO_EXCEPTION(sptr->connection_result_handler(result, std::move(exc), rc, std::move(pHndl)));

				if (result != async_operation_result_t::st_ready || exc) {
					CHECK_NO_EXCEPTION(sptr->close());
					}
				}
			};

		/* ���������� ���������� ���������� ��������� �������� ������������� */
		return indentity_tail(std::move(h),
			std::make_unique<connection_handler_kp>(sptr->ctx(), std::move(connectionHndl)
#ifdef CBL_MPF_TRACELEVEL_EVENT_CONNECTION
				, cvt(sptr->remote_object_endpoint().to_string())
#else
#endif


				));
		}
	else {
		return {};
		}
	}

void xpeer_out_rt1_t::connect(std::shared_ptr<self_t>  && obj,
	const std::weak_ptr<distributed_ctx_t> & ctx_,
	connection_result_t && connectionHndl,
	local_object_create_connection_t && localIdentityCtr,
	remote_object_check_identity_t && remoteIdentityChecker,
	ctr_1_input_queue_t && destStream,
	object_initialize_handler_t && objectInitializer) {

	CBL_MPF_IF_NOT_ERROR_EMULATION(CBL_VERIFY(obj.use_count() == 1));

	CBL_MPF_TRACELEVEL_EVENT_RT1_CONNECTION_TRACE("outbound_connection:create:s="
		+ obj->remote_object_endpoint().to_string()
		+ ":t=" + obj->this_id().to_str()
		+ ":session=" + obj->session().object_hash().to_str());

	MPF_TEST_EMULATION_IN_CONTEXT_EXPR_COND(obj->ctx(), 328, is_trait_at_emulation_context(obj->level()));

	auto identityH = make_identity_handler(obj, std::move(connectionHndl));

	MPF_TEST_EMULATION_IN_CONTEXT_EXPR_COND(obj->ctx(), 329, is_trait_at_emulation_context(obj->level()));

	obj->initialize();
	CBL_MPF_IF_NOT_ERROR_EMULATION(CBL_VERIFY(obj.use_count() == 2));

	obj->set_state(remote_object_state_t::connect_stage);

	auto pctx = __rtx_out_connection_ctx_timered_t::launch(ctx_,
		obj->remote_object_endpoint().copy(),
		std::move(identityH),
		std::move(localIdentityCtr),
		std::move(remoteIdentityChecker),
		obj->weak_from_this());

	obj->handshake_state_register(pctx, std::move(objectInitializer));
	CBL_MPF_IF_NOT_ERROR_EMULATION(CBL_VERIFY(obj.use_count() >= 2));
	CBL_MPF_IF_NOT_ERROR_EMULATION(CBL_VERIFY(pctx.use_count() == 2));

	obj->_session_create(std::move(pctx), std::move(destStream));
	CBL_MPF_IF_NOT_ERROR_EMULATION(CBL_VERIFY(obj.use_count() >= 2));
	CBL_MPF_IF_NOT_ERROR_EMULATION(CBL_VERIFY(pctx.use_count() == 2));
	}

void xpeer_out_rt1_t::connect2stack(const std::weak_ptr<distributed_ctx_t> & ctx,
	std::weak_ptr<i_rt1_main_router_t> rt,
	std::shared_ptr<ox_rt1_stream_t> && source_,
	std::unique_ptr<gtw_rt1_x_t> && gtw,
	rtx_object_closed_t && closingHndl,
	connection_result_t && connectionHndl,
	local_object_create_connection_t && localIdentityCtr,
	remote_object_check_identity_t && remoteIdentityChecker,
	object_initialize_handler_t && objectInitializer,
	ctr_1_input_queue_t && hndlStack,
	std::shared_ptr<i_peer_statevalue_t> && stval,
	__event_handler_point_f && xh,
	exceptions_suppress_checker_t && remoteExcChecker) {

	auto obj = std::make_shared<self_t>(
		ctx,
		rt,
		std::move(source_),
		std::move(gtw),
		std::move(closingHndl),
		std::move(stval),
		std::move(xh),
		std::move(remoteExcChecker));

	MPF_TEST_EMULATION_IN_CONTEXT_EXPR_COND(obj->ctx(), 330, is_trait_at_emulation_context(obj->level()));

	connect(std::move(obj),
		ctx,
		std::move(connectionHndl),
		std::move(localIdentityCtr),
		std::move(remoteIdentityChecker),
		std::move(hndlStack),
		std::move(objectInitializer));
	}

xpeer_out_rt1_t::~xpeer_out_rt1_t() {
	CBL_MPF_TRACELEVEL_EVENT_RT1_CONNECTION_TRACE("outbound link:destroyed:s="
		+ remote_object_endpoint().to_string()
		+ ":t=" + this_id().to_str()
		+ ":session=" + session().object_hash().to_str());
	}

class __handler_input_opeer_t : public i_xpeer_rt1_t::input_stack_t {
private:
	i_xpeer_rt1_t::input_handler_opeer_t _hndl;

public:
	__handler_input_opeer_t(i_xpeer_rt1_t::input_handler_opeer_t && hndl_)
		: _hndl(std::move(hndl_)) {

		CBL_VERIFY(_hndl);
		}

	std::uintmax_t stored_count()const noexcept {
		return 0;
		}

	void clear()noexcept {}

	void push(value_type && message) {
		_hndl(std::move(message));
		}
	};



void xpeer_out_rt1_t::connect2handler(const std::weak_ptr<distributed_ctx_t> & ctx,
	std::weak_ptr<i_rt1_main_router_t> rt,
	std::shared_ptr<ox_rt1_stream_t> && source_,
	std::unique_ptr<gtw_rt1_x_t> && gtw,
	rtx_object_closed_t && closingHndl,
	connection_result_t && connectionHndl,
	local_object_create_connection_t && localIdentityCtr,
	remote_object_check_identity_t && remoteIdentityChecker,
	object_initialize_handler_t && objectInitializer,
	input_handler_opeer_t && hndlInput,
	std::shared_ptr<i_peer_statevalue_t> && stval,
	__event_handler_point_f && xh,
	exceptions_suppress_checker_t && remoteExcChecker) {

	auto obj = std::make_shared<self_t>(
		ctx,
		rt,
		std::move(source_),
		std::move(gtw),
		std::move(closingHndl),
		std::move(stval),
		std::move(xh),
		std::move(remoteExcChecker));

	/* ����������� ����������� �������� ��������� */
	auto ctrInput = [hndlInput, wptr = std::weak_ptr<self_t>(obj), addrh = obj->address_handler()]()mutable {
		return std::make_unique<__handler_input_opeer_t>(std::move(hndlInput));
		};

	MPF_TEST_EMULATION_IN_CONTEXT_EXPR_COND(obj->ctx(), 331, is_trait_at_emulation_context(obj->level()));

	connect(std::move(obj),
		ctx,
		std::move(connectionHndl),
		std::move(localIdentityCtr),
		std::move(remoteIdentityChecker),
		std::move(ctrInput),
		std::move(objectInitializer));
	}

void xpeer_out_rt1_t::connect2i(const std::weak_ptr<distributed_ctx_t> & ctx,
	std::weak_ptr<i_rt1_main_router_t> rt,
	std::shared_ptr<ox_rt1_stream_t> && source_,
	std::unique_ptr<gtw_rt1_x_t> && gtw,
	rtx_object_closed_t && closingHndl,
	connection_result_t && connectionHndl,
	local_object_create_connection_t && localIdentityCtr,
	remote_object_check_identity_t && remoteIdentityChecker,
	object_initialize_handler_t && objectInitializer,
	const i_input_messages_stock_t * hndlInput,
	std::shared_ptr<i_peer_statevalue_t> && stval,
	__event_handler_point_f && xh,
	exceptions_suppress_checker_t && remoteExcChecker) {

	MPF_TEST_EMULATION_IN_CONTEXT_EXPR_COND(CHECK_PTR(ctx), 332, is_trait_at_emulation_context(rtl_level_t::l1));

	if (auto ictr2Stack = dynamic_cast<const input2stack_t*>(hndlInput)) {
		connect2stack(ctx,
			rt,
			std::move(source_),
			std::move(gtw),
			std::move(closingHndl),
			std::move(connectionHndl),
			std::move(localIdentityCtr),
			std::move(remoteIdentityChecker),
			std::move(objectInitializer),
			i_xpeer_rt1_t::ctr_1_input_queue_t(ictr2Stack->ictr),
			std::move(stval),
			std::move(xh),
			std::move(remoteExcChecker));
		}

	else if (auto ictr2Hndl = dynamic_cast<const opeer_l1_input2handler_t*>(hndlInput)) {
		connect2handler(ctx,
			rt,
			std::move(source_),
			std::move(gtw),
			std::move(closingHndl),
			std::move(connectionHndl),
			std::move(localIdentityCtr),
			std::move(remoteIdentityChecker),
			std::move(objectInitializer),
			opeer_l1_input2handler_t::input_handler_t(ictr2Hndl->ihndl),
			std::move(stval),
			std::move(xh),
			std::move(remoteExcChecker));
		}
	else {
		FATAL_ERROR_FWD(nullptr);
		}
	}
