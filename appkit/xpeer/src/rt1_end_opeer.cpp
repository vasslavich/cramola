#include "../../internal.h"
#include "../../context/context.h"
#include "../rt1_end_opeer.h"
#include "../../xpeer_invoke/xpeer_traits.h"


using namespace crm;
using namespace crm::detail;


static_assert(!is_xpeer_unique_compliant_v<opeer_t>, __FILE__);
static_assert(is_xpeer_copyable_object_v<opeer_t>, __FILE__);
static_assert(is_xpeer_trait_z_v<decltype(make_invokable_xpeer(opeer_t{})) > , __FILE_LINE__);
static_assert(is_xpeer_copyable_object_v<decltype(make_invokable_xpeer(opeer_t{}))> , __FILE_LINE__);


std::string opeer_t::trace_line()const noexcept {
	return _lnk.trace_line();
	}

opeer_t::opeer_t()noexcept {}

/*opeer_t::opeer_t(opeer_t &&o)noexcept
	: _lnk(std::move(o._lnk))
	
#if defined( CBL_MPF_OBJECTS_COUNTER_CHECKER_RT1_PEER ) && ( CBL_MPF_TRACELEVEL_ST >= CBL_MPF_TRACELEVEL_ST_4)
	, _xDbgCounter(std::move(o._xDbgCounter))
#endif
	
	{}

opeer_t& opeer_t::operator=(opeer_t &&o) noexcept {
	if(this != std::addressof(o)) {
		_lnk = std::move(o._lnk);

#if defined( CBL_MPF_OBJECTS_COUNTER_CHECKER_RT1_PEER ) && ( CBL_MPF_TRACELEVEL_ST >= CBL_MPF_TRACELEVEL_ST_4)
		_xDbgCounter = std::move(o._xDbgCounter);
#endif
		}

	return (*this);
	}*/

opeer_t::opeer_t(outcoming_connection_t::handler_t && lnk)noexcept
	: _lnk(std::move(lnk)){}

void opeer_t::close()noexcept {}

bool opeer_t::closed()const noexcept {
	return _lnk.closed();
	}

remote_object_state_t opeer_t::state()const noexcept {
	return _lnk.state();
	}

opeer_t::link_type opeer_t::link()noexcept{
	return _lnk.link();
	}

subscribe_invoker_result opeer_t::subscribe_hndl_id(async_space_t scx, const _address_hndl_t &hndl, subscribe_options opt,
	subscribe_message_handler && f) {

	return _lnk.subscribe_hndl_id(scx, hndl, opt, std::move(f));
	}

void opeer_t::unsubscribe_hndl_id(const _address_hndl_t &hndl)noexcept {
	_lnk.unsubscribe_hndl_id(hndl);
	}

subscribe_result opeer_t::subscribe_event(async_space_t scx, const _address_event_t &hndl, bool once,
	__event_handler_f && f) {

	return _lnk.subscribe_event(scx, hndl, once, std::move(f));
	}

void opeer_t::subscribe_event(events_list && el) {

	_lnk.subscribe_event(std::move(el));
	}

void opeer_t::unsubscribe_event(const _address_event_t &hndl) noexcept {
	_lnk.unsubscribe_event(hndl);
	}

opeer_t::xpeer_desc_t opeer_t::desc()const noexcept {
	return _lnk.desc();
	}

std::string opeer_t::address()const noexcept {
	return _lnk.address();
	}

int opeer_t::port()const noexcept {
	return _lnk.port();
	}

bool opeer_t::renewable_connection()const noexcept {
	return _lnk.renewable_connection();
	}

rtl_table_t opeer_t::direction()const noexcept {
	return _lnk.direction();
	}

identity_descriptor_t opeer_t::this_id()const noexcept {
	return _lnk.this_id();
	}

identity_descriptor_t opeer_t::remote_id()const noexcept {
	return _lnk.remote_id();
	}

identity_descriptor_t opeer_t::id()const noexcept {
	return _lnk.id();
	}

local_object_id_t opeer_t::local_id()const noexcept {
	return _lnk.local_id();
	}

opeer_t::remote_object_endpoint_t opeer_t::remote_object_endpoint()const noexcept {
	return _lnk.remote_object_endpoint();
	}

bool opeer_t::is_opened()const noexcept {
	return _lnk.is_opened();
	}

rtl_level_t opeer_t::level()const noexcept {
	return rtl_level_t::l1;
	}

std::shared_ptr<i_peer_statevalue_t> opeer_t::state_value() noexcept {
	return _lnk.state_value();
	}

std::shared_ptr<const i_peer_statevalue_t> opeer_t::state_value()const noexcept {
	return _lnk.state_value();
	}

void opeer_t::set_state_value(std::shared_ptr<i_peer_statevalue_t> && stv)noexcept {
	_lnk.set_state_value(std::move(stv));
	}

std::shared_ptr<const i_peer_remote_properties_t> opeer_t::remote_properties()const noexcept {
	return _lnk.remote_properties();
	}

connection_key_t opeer_t::connection_key()const noexcept {
	return connection_key_t{ address(), port() };
	}

std::shared_ptr<distributed_ctx_t> opeer_t::ctx()const noexcept {
	return _lnk.ctx();
	}

peer_commands_result_t opeer_t::execute_command(peer_command_t cmd) {
	return _lnk.execute_command(cmd);
	}

static_assert(is_xpeer_trait_z_v< opeer_t>, __FILE_LINE__);



