#include "../../context/context.h"
#include "../rt0_end_ipeer.h"
#include "../../xpeer_invoke/xpeer_traits.h"
#include "../../xpeer_invoke/xpeer_invoke_trait.h"


using namespace crm;
using namespace crm::detail;


static_assert(!is_xpeer_unique_compliant_v<ipeer_l0>, __FILE__);
static_assert(is_xpeer_copyable_object_v<ipeer_l0>, __FILE__);
static_assert(is_xpeer_trait_z_v<ipeer_l0>, __FILE_LINE__);
static_assert(std::is_same_v < ipeer_l0, std::decay_t<decltype(make_invokable_xpeer(ipeer_l0{})) >> , __FILE__);
static_assert(is_xpeer_trait_z_v<decltype(make_invokable_xpeer(ipeer_l0{})) > , __FILE_LINE__);


ipeer_l0::ipeer_l0()noexcept {}

ipeer_l0::ipeer_l0(const ipeer_l0& o)noexcept
	: _lnk(o._lnk) {}

ipeer_l0& ipeer_l0::operator=(const ipeer_l0& o) noexcept {
	if(this != std::addressof(o)) {
		_lnk = o._lnk;
		}

	return (*this);
	}

ipeer_l0::ipeer_l0(ipeer_l0&& o)noexcept
	: _lnk(std::move(o._lnk))

#if defined( CBL_MPF_OBJECTS_COUNTER_CHECKER_RT1_PEER ) && ( CBL_MPF_TRACELEVEL_ST >= CBL_MPF_TRACELEVEL_ST_4)
	, _xDbgCounter(std::move(o._xDbgCounter))
#endif

	{}

ipeer_l0& ipeer_l0::operator=(ipeer_l0&& o)noexcept {
	if(this != std::addressof(o)) {
		_lnk = std::move(o._lnk);

#if defined( CBL_MPF_OBJECTS_COUNTER_CHECKER_RT0_PEER ) && ( CBL_MPF_TRACELEVEL_ST >= CBL_MPF_TRACELEVEL_ST_4)
		_xDbgCounter = std::move(o._xDbgCounter);
#endif
		}

	return (*this);
	}

ipeer_l0::ipeer_l0( link_t&& lnk)noexcept
	: _lnk(std::move(lnk)) {}

void ipeer_l0::close()noexcept {}

bool ipeer_l0::closed()const noexcept {
	return _lnk.closed();
	}

remote_object_state_t ipeer_l0::state()const noexcept {
	return _lnk.state();
	}

subscribe_invoker_result ipeer_l0::subscribe_hndl_id(async_space_t scx, const _address_hndl_t& hndl, subscribe_options opt,
	subscribe_message_handler&& f) {

	return _lnk.subscribe_hndl_id(scx, hndl, opt, std::move(f));
	}

void ipeer_l0::unsubscribe_hndl_id(const _address_hndl_t& hndl)noexcept {
	_lnk.unsubscribe_hndl_id(hndl);
	}

void ipeer_l0::subscribe_event(events_list&& el) {

	_lnk.subscribe_event(std::move(el));
	}

subscribe_result ipeer_l0::subscribe_event(async_space_t scx, const _address_event_t& hndl, bool once,
	__event_handler_f&& f) {

	return _lnk.subscribe_event(scx, hndl, once, std::move(f));
	}

void ipeer_l0::unsubscribe_event(const _address_event_t& hndl)noexcept {
	_lnk.unsubscribe_event(hndl);
	}

const ipeer_l0::xpeer_desc_t& ipeer_l0::desc()const noexcept {
	static_assert(std::is_lvalue_reference_v<decltype(_lnk.desc())>, __CBL_FILEW__);
	return _lnk.desc();
	}

std::string ipeer_l0::address()const noexcept {
	return _lnk.address();
	}

int ipeer_l0::port()const noexcept {
	return _lnk.port();
	}

bool ipeer_l0::renewable_connection()const noexcept {
	return _lnk.renewable_connection();
	}

rtl_table_t ipeer_l0::direction()const noexcept {
	return _lnk.direction();
	}

const local_object_id_t& ipeer_l0::local_id()const noexcept {
	static_assert(std::is_lvalue_reference_v<decltype(_lnk.local_id())>, __CBL_FILEW__);
	return _lnk.local_id();
	}

const identity_descriptor_t& ipeer_l0::this_id()const noexcept {
	static_assert(std::is_lvalue_reference_v<decltype(_lnk.this_id())>, __CBL_FILEW__);
	return _lnk.this_id();
	}

const identity_descriptor_t& ipeer_l0::remote_id()const noexcept {
	static_assert(std::is_lvalue_reference_v<decltype(_lnk.remote_id())>, __CBL_FILEW__);
	return _lnk.remote_id();
	}

const identity_descriptor_t& ipeer_l0::id()const noexcept {
	static_assert(std::is_lvalue_reference_v<decltype(_lnk.id())>, __CBL_FILEW__);
	return _lnk.id();
	}

ipeer_l0::remote_object_endpoint_t ipeer_l0::remote_object_endpoint()const noexcept {
	return _lnk.remote_object_endpoint();
	}

bool ipeer_l0::is_opened()const noexcept {
	return _lnk.is_opened();
	}

rtl_level_t ipeer_l0::level()const noexcept {
	return rtl_level_t::l0;
	}

std::shared_ptr<i_peer_statevalue_t> ipeer_l0::state_value() noexcept {
	return _lnk.state_value();
	}

std::shared_ptr<const i_peer_statevalue_t> ipeer_l0::state_value()const noexcept {
	return _lnk.state_value();
	}

void ipeer_l0::set_state_value(std::shared_ptr<i_peer_statevalue_t>&& stv) noexcept {
	_lnk.set_state_value(std::move(stv));
	}

std::shared_ptr<const i_peer_remote_properties_t> ipeer_l0::remote_properties()const noexcept {
	return _lnk.remote_properties();
	}

connection_key_t ipeer_l0::connection_key()const noexcept {
	return connection_key_t{ address(), port() };
	}

std::shared_ptr<distributed_ctx_t> ipeer_l0::ctx()const noexcept {
	return _lnk.ctx();
	}

peer_commands_result_t ipeer_l0::execute_command(peer_command_t cmd) {
	return _lnk.execute_command(cmd);
	}

static_assert(is_xpeer_trait_z_v< ipeer_l0>, __FILE_LINE__);
