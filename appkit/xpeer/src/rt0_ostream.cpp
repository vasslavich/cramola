#include "../../internal.h"
#include "../../context/context.h"
#include "../rt0_ostream.h"
#include "../../cmdrt/terms.h"
#include "../../queues/qdef.h"
#include "../../xpeer/i_input_handlers.h"
#include "../../channel_terms/i_route_io.h"
#include "../../streams/istreams_routetbl.h"


using namespace crm;
using namespace crm::detail;


#ifdef CBL_USE_SMART_OBJECTS_COUNTING
crm::log::counter_t ostream_rt0_x_t::subcounter(const std::string & name) {
	return _THIS_COUNTER.subcounter(name);
	}
#endif//CBL_USE_SMART_OBJECTS_COUNTING

void ostream_rt0_x_t::exc_hndl( std::unique_ptr<dcf_exception_t> && ntf )noexcept{
	auto sctx( _ctx.lock() );
	if( sctx )
		sctx->exc_hndl( std::move( ntf ) );
	}

void ostream_rt0_x_t::exc_hndl( const dcf_exception_t & exc ) noexcept{
	exc_hndl( exc.dcpy() );
	}

void ostream_rt0_x_t::exc_hndl( dcf_exception_t && exc )noexcept{
	exc_hndl( exc.dcpy() );
	}

std::shared_ptr<i_rt0_main_router_input_t> ostream_rt0_x_t::input_channel_routed()const noexcept{
	return _inputStockRouted.lock();
	}

const std::unique_ptr<i_input_messages_stock_t>& ostream_rt0_x_t::input_channel_auto()const noexcept{
	return _inputStockAuto;
	}

void ostream_rt0_x_t::input_hndl(input_value_t && item) {

	if (_rtype == stream_route_type::routed_) {
		if (item.message() && item.message()->type().type() == iol_types_t::st_packet) {

#pragma message( CBL_VAR_NAME_VALUE(CBL_BUILD_VAR_SNDRCV_REVERSE_GUARD, 31))		
			if (auto ipacket = message_cast<ipacket_t>(std::move(item).message())) {

				rtl_roadmap_line_t rdl_;
				rdl_.points = make_message(rt0_rdm(), (*ipacket));

#ifdef USE_DCF_CHECK
				if (is_null(rdl_.points.target_id()) || rdl_.points.table() == rtl_table_t::undefined) {
					FATAL_ERROR_FWD(rdl_.points.to_str());
					}
#endif

				rt0_x_message_unit_t xObj(rt0_x_message_t::make(std::move(rdl_), std::move(*ipacket).packet(), connection_key()));

#pragma message( CBL_VAR_NAME_VALUE(CBL_BUILD_VAR_SNDRCV_REVERSE_GUARD, 32))
				if (xObj.reset_invoke_exception_guard(ipacket->reset_invoke_exception_guard())) {
					FATAL_ERROR_FWD(nullptr);
					}

				MPF_TEST_EMULATION_IN_CONTEXT_COND_TRCX(ctx(), 218,
					is_trait_at_emulation_context(xObj.unit().package()),
					trait_at_keytrace_set(xObj.unit().package()));

				if (auto nchannel = input_channel_routed()) {
					nchannel->evhndl_from_peer(std::move(xObj));
					}
				else {
					xObj.invoke_exception(CREATE_MPF_PTR_EXC_FWD(nullptr));
					}
				}
			else {
				FATAL_ERROR_FWD(nullptr);
				}
			}
		else {
			if (auto nchannel = input_channel_routed()) {
#pragma message( CBL_VAR_NAME_VALUE(CBL_BUILD_VAR_SNDRCV_REVERSE_GUARD, 31))
				nchannel->evhndl_form_peer_rt_id(std::move(item));
				}
			else {

#pragma message( CBL_VAR_NAME_VALUE(CBL_BUILD_VAR_SNDRCV_REVERSE_GUARD, 31))
				item.message()->invoke_reverse_exception(CREATE_MPF_PTR_EXC_FWD(nullptr));
				}
			}
		}
	else if (_rtype == stream_route_type::auto_) {
		if (auto & nchannel = input_channel_auto()) {
			if (auto pStock = dynamic_cast<const opeer_l0_input2handler_t*>(nchannel.get())) {
#pragma message( CBL_VAR_NAME_VALUE(CBL_BUILD_VAR_SNDRCV_REVERSE_GUARD, 31))

				pStock->ihndl(std::move(item));
				}
			else {
				FATAL_ERROR_FWD(nullptr);
				}
			}
		else {

#pragma message( CBL_VAR_NAME_VALUE(CBL_BUILD_VAR_SNDRCV_REVERSE_GUARD, 31))
			item.message()->invoke_reverse_exception(CREATE_MPF_PTR_EXC_FWD(nullptr));
			}
		}
	else {
		FATAL_ERROR_FWD(nullptr);
		}
	}

ostream_rt0_x_t::ostream_rt0_x_t(std::weak_ptr<distributed_ctx_t> ctx_,
	std::shared_ptr<i_rt0_main_router_input_t> && inputStock)
	: _ctx(ctx_)
	, _inputStockRouted( std::move( inputStock ) )
	, _rtype{stream_route_type::routed_ }{
	
	auto ctx(ctx_.lock());
	if(!ctx)
		THROW_MPF_EXC_FWD(nullptr);

	CBL_MPF_TRACELEVEL_EVENT_RT0_CONNECTION_TRACE("outbound_connection 1:stream:create:s="
		+ connection_key().to_string()
		+ ":s=" + remote_id().to_str()
		+ ":t=" + this_id().to_str()
		+ ":session=" + desc().session().object_hash().to_str());
	}

ostream_rt0_x_t::ostream_rt0_x_t(std::weak_ptr<distributed_ctx_t> ctx_,
	std::unique_ptr<i_input_messages_stock_t> && inputStock)
	: _ctx(ctx_)
	, _inputStockAuto(std::move(inputStock))
	, _rtype{ stream_route_type::auto_ }{
	
	auto ctx(ctx_.lock());
	if (!ctx)
		THROW_MPF_EXC_FWD(nullptr);

	CBL_MPF_TRACELEVEL_EVENT_RT0_CONNECTION_TRACE("outbound_connection 2:stream:create:s="
		+ connection_key().to_string()
		+ ":s=" + remote_id().to_str()
		+ ":t=" + this_id().to_str()
		+ ":session=" + desc().session().object_hash().to_str());
	}

void ostream_rt0_x_t::ctr( const identity_descriptor_t & thisId_,
	local_object_create_connection_t && idCtr,
	remote_object_check_identity_t && idChecker,
	post_connection_t && postConnHndl,
	close_event_hndl_t && closeHndl,
	std::unique_ptr<i_endpoint_t> && endpoint,
	std::shared_ptr<default_binary_protocol_handler_t> && ctrGateway,
	object_initialize_handler_t && objectInitializer,
	std::shared_ptr<i_xpeer_source_factory_t> xpFact,
	std::function<std::shared_ptr<i_peer_statevalue_t>()> && stvctr,
	exceptions_suppress_checker_t && connectionExcChecker,
	__event_handler_point_f&& eh,
	async_space_t scx) {

	std::weak_ptr<self_t> wthis(shared_from_this());
	auto inputCtr = [wthis]() {
		auto sthis( wthis.lock() );
		if( sthis ) {
			return std::make_unique<qinput_adptf_t<input_value_t>>( [wthis]( input_value_t && obj ) {

				auto sthis( wthis.lock() );
				if( sthis )
					sthis->input_hndl( std::move( obj ) );
				else {
					THROW_EXC_FWD(nullptr);
					}
				} );
			}
		else {
			THROW_EXC_FWD(nullptr);
			}
		};

	auto postConn = [postConnHndl](std::unique_ptr<dcf_exception_t> && exc, link_t && peer ) {

		CBL_MPF_TRACELEVEL_EVENT_RT0_CONNECTION_TRACE("stream:outbound:open:"
			+ peer.connection_key().to_string()
			+ ":s=" + peer.remote_id().to_str()
			+ ":t=" + peer.this_id().to_str()
			+ ":session=" + peer.desc().session().object_hash().to_str()
			+ ":exc=" + (exc ? cvt(exc->msg()) : ""));

		postConnHndl( std::move(exc), std::forward<link_t>( peer ) );
		};

	/* ������ ������������� ����������� */
	auto initializer(std::make_unique<xpeer_out_connector_rt0_t>( ctrGateway,
		std::move(inputCtr),
		std::move(objectInitializer),
		std::move(xpFact),
		std::move(postConn),
		thisId_,
		std::move(endpoint),
		std::move(stvctr),
		std::move(eh),
		std::move(connectionExcChecker)));

	auto pPnt = remote_peer_t::launch(_ctx,
		std::move(initializer),
		std::move(idCtr),
		std::move(idChecker),
		std::move(closeHndl),
		scx );
	_peerLnk = std::move(pPnt);

	CBL_MPF_TRACELEVEL_EVENT_RT0_CONNECTION_TRACE("outbound_connection:stream:initialize:s="
		+ connection_key().to_string()
		+ ":s=" + remote_id().to_str()
		+ ":t=" + this_id().to_str()
		+ ":session=" + desc().session().object_hash().to_str());
	}

void ostream_rt0_x_t::unbind_reslink() noexcept {
	if(auto stb = CHECK_PTR(ctx())->streams_handler()) {
		CHECK_NO_EXCEPTION(stb->close(local_id()));
		}
	}

ostream_rt0_x_t::~ostream_rt0_x_t() {
	CHECK_NO_EXCEPTION(unbind_reslink());
	CHECK_NO_EXCEPTION( close() );
	}

bool ostream_rt0_x_t ::closed()const  noexcept{
	return _closedf.load(std::memory_order::memory_order_acquire);
	}

bool ostream_rt0_x_t::is_opened()const  noexcept{
	return !closed();
	}

void ostream_rt0_x_t::close()noexcept{
	_closedf.store(true, std::memory_order::memory_order_release);
	}

peer_commands_result_t ostream_rt0_x_t::execute_command( peer_command_t cmd ) {
	return _peerLnk.execute_command( cmd );
	}

ostream_rt0_x_t::link_t ostream_rt0_x_t::link()  noexcept{
	return _peerLnk.link();
	}

local_object_id_t ostream_rt0_x_t::local_id()const noexcept{
	return _peerLnk.local_id();
	}

identity_descriptor_t ostream_rt0_x_t::this_id()const  noexcept{
	return _peerLnk.this_id();
	}

remote_object_state_t ostream_rt0_x_t::state()const  noexcept{
	return _peerLnk.state();
	}

identity_descriptor_t ostream_rt0_x_t::remote_id()const  noexcept{
	return _peerLnk.remote_id();
	}

identity_descriptor_t ostream_rt0_x_t::id()const  noexcept{
	return remote_id();
	}

ostream_rt0_x_t::xpeer_desc_t ostream_rt0_x_t::desc()const  noexcept{
	return _peerLnk.desc();
	}

rt0_node_rdm_t ostream_rt0_x_t::rt0_rdm()const  noexcept{
	return _peerLnk.desc();
	}

std::string ostream_rt0_x_t::address()const  noexcept{
	return _peerLnk.address();
	}

int ostream_rt0_x_t::port()const  noexcept{
	return _peerLnk.port();
	}

ostream_rt0_x_t::remote_object_endpoint_t ostream_rt0_x_t::remote_object_endpoint()const  noexcept{
	return _peerLnk.remote_object_endpoint();
	}

std::shared_ptr<const i_peer_remote_properties_t> ostream_rt0_x_t::remote_properties()const noexcept {
	return _peerLnk.remote_properties();
	}

std::shared_ptr<distributed_ctx_t> ostream_rt0_x_t::ctx()const  noexcept{
	return _peerLnk.ctx();
	}

connection_key_t ostream_rt0_x_t::connection_key()const  noexcept{
	return connection_key_t{ _peerLnk.address(), _peerLnk.port() };
	}

void ostream_rt0_x_t::write(outbound_message_unit_t && blob ) {
	_peerLnk.write( std::move( blob ) );
	}

void ostream_rt0_x_t::invoke_after_ready( async_space_t scx, std::unique_ptr<subscribe_event_connection_t> && hndl ) {
	_peerLnk.invoke_after_ready( scx, std::move( hndl ) );
	}

void ostream_rt0_x_t::subscribe_event(events_list&& el) {
	_peerLnk.subscribe_event(std::move(el));
	}

void ostream_rt0_x_t::unsubscribe_event( const _address_event_t &hndl ) noexcept{
	_peerLnk.unsubscribe_event( hndl );
	}

void ostream_rt0_x_t::unsubscribe_hndl_id(const _address_hndl_t& hndl)noexcept {
	_peerLnk.unsubscribe_hndl_id(hndl);
	}

bool ostream_rt0_x_t::renewable_connection()const  noexcept {
	return _peerLnk.renewable_connection();
	}

rtl_table_t ostream_rt0_x_t::direction()const noexcept {
	return _peerLnk.direction();
	}

std::shared_ptr<i_peer_statevalue_t> ostream_rt0_x_t::state_value()  noexcept{
	return _peerLnk.state_value();
	}

std::shared_ptr<const i_peer_statevalue_t> ostream_rt0_x_t::state_value()const  noexcept{
	return _peerLnk.state_value();
	}

void ostream_rt0_x_t::set_state_value( std::shared_ptr<i_peer_statevalue_t> && stval )  noexcept{
	_peerLnk.set_state_value( std::move( stval ) );
	}

ostream_rt0_x_t::ostream_lnk_t ostream_rt0_x_t::create_routed(std::weak_ptr<distributed_ctx_t> ctx_,
	std::shared_ptr<default_binary_protocol_handler_t> ctrGtw,
	object_initialize_handler_t && objectInitializer,
	std::shared_ptr<i_xpeer_source_factory_t> xpFact,
	post_connection_t && postConnHndl,
	close_event_hndl_t && closeHndl,
	const identity_descriptor_t & thisId_,
	local_object_create_connection_t && idCtr,
	remote_object_check_identity_t && idChecker,
	std::unique_ptr<i_endpoint_t> && endpoint,
	std::shared_ptr<i_rt0_main_router_input_t>  && inputStock,
	std::function<std::shared_ptr<i_peer_statevalue_t>()> && stvctr,
	exceptions_suppress_checker_t && connectionExcChecker,
	__event_handler_point_f&& eh,
	async_space_t scx) {

	auto obj = std::make_shared<self_t>(ctx_, std::move(inputStock));

	obj->ctr(thisId_,
		std::move(idCtr),
		std::move(idChecker),
		std::move(postConnHndl),
		std::move(closeHndl),
		std::move(endpoint),
		std::move(ctrGtw),
		std::move(objectInitializer),
		std::move(xpFact),
		std::move(stvctr),
		std::move(connectionExcChecker),
		std::move(eh),
		scx );
	return ostream_rt0_x_t::ostream_lnk_t( std::move( obj ) );
	}

ostream_rt0_x_t::ostream_lnk_t ostream_rt0_x_t::create_auto(std::weak_ptr<distributed_ctx_t> ctx_,
	std::shared_ptr<default_binary_protocol_handler_t> ctrGtw,
	object_initialize_handler_t&& objectInitializer,
	std::shared_ptr<i_xpeer_source_factory_t> xpFact,
	post_connection_t&& postConnHndl,
	close_event_hndl_t&& closeHndl,
	const identity_descriptor_t& thisId_,
	local_object_create_connection_t&& idCtr,
	remote_object_check_identity_t&& idChecker,
	std::unique_ptr<i_endpoint_t>&& endpoint,
	std::unique_ptr<i_input_messages_stock_t>&& inputStock,
	std::function<std::shared_ptr<i_peer_statevalue_t>()>&& stvctr,
	exceptions_suppress_checker_t&& connectionExcChecker,
	__event_handler_point_f&& eh,
	async_space_t scx) {

	auto obj = std::make_shared<self_t>(ctx_, std::move(inputStock));

	obj->ctr(thisId_,
		std::move(idCtr),
		std::move(idChecker),
		std::move(postConnHndl),
		std::move(closeHndl),
		std::move(endpoint),
		std::move(ctrGtw),
		std::move(objectInitializer),
		std::move(xpFact),
		std::move(stvctr),
		std::move(connectionExcChecker),
		std::move(eh),
		scx);
	return ostream_rt0_x_t::ostream_lnk_t(std::move(obj));
	}

