#include "../../internal.h"
#include "../../context/context.h"
#include "../rt1_base_xstream.h"
#include "../../queues/qdef.h"


using namespace crm;
using namespace crm::detail;


rt1_base_xstream_t::rt1_base_xstream_t( std::weak_ptr<i_rt1_main_router_t> router,
	std::weak_ptr<distributed_ctx_t> ctx)
	: _router( router )
	, _ctx( ctx ) {}

rt1_base_xstream_t::~rt1_base_xstream_t() {
	CHECK_NO_EXCEPTION( close() );
	}

void rt1_base_xstream_t::close()noexcept {}

std::shared_ptr<i_rt1_main_router_t> rt1_base_xstream_t::router()const noexcept {
	return _router.lock();
	}

std::shared_ptr<distributed_ctx_t> rt1_base_xstream_t::ctx()const noexcept {
	return _ctx.lock();
	}

void rt1_base_xstream_t::ntf_hndl( std::unique_ptr<crm::dcf_exception_t> && obj ) noexcept {
	auto ctx_( _ctx.lock() );
	if( ctx_ )
		ctx_->exc_hndl( std::move( obj ) );
	}


