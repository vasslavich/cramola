#include "../../context/context.h"
#include "../../queues/qdef.h"
#include "../rt1_xpeer.h"


using namespace crm;
using namespace crm::detail;


std::shared_ptr<const i_peer_remote_properties_t> xpeer_base_rt1_t::remote_properties()const noexcept {
	return std::atomic_load_explicit(&_remoteProp, std::memory_order::memory_order_acquire);
	}

bool xpeer_base_rt1_t::renewable_connection()const noexcept {
	return false;
	}

xpeer_base_rt1_t::remote_object_endpoint_t xpeer_base_rt1_t::remote_object_endpoint()const noexcept {
	CBL_VERIFY(_addrh);
	return _addrh->remote_object_endpoint();
	}

void xpeer_base_rt1_t::set_remote_properties(std::unique_ptr<i_peer_remote_properties_t> && prop) noexcept {
	std::atomic_store_explicit(&_remoteProp,
		std::shared_ptr<i_peer_remote_properties_t>(std::move(prop)),
		std::memory_order::memory_order_release);
	}

xpeer_base_rt1_t::link_t xpeer_base_rt1_t::get_link() noexcept {
	return link_t(std::dynamic_pointer_cast<xpeer_base_rt1_t>(shared_from_this()), _addrh);
	}

xpeer_base_rt1_t::clink_t xpeer_base_rt1_t::get_link()const noexcept {
	return clink_t(std::dynamic_pointer_cast<const xpeer_base_rt1_t>(shared_from_this()), _addrh);
	}

std::shared_ptr<distributed_ctx_t> xpeer_base_rt1_t::ctx() noexcept {
	return _ctx.lock();
	}

std::shared_ptr<distributed_ctx_t> xpeer_base_rt1_t::ctx()const noexcept {
	return _ctx.lock();
	}

std::string xpeer_base_rt1_t::address_trace()const {
	return address() + ":" + std::to_string(port())
		+ ":t=" + this_id().id().to_str()
		+ ":r=" + remote_id().id().to_str()
		+ ":d=" + cvt2string(direction())
		+ ":h=" + session().object_hash().to_str();
	}

const rtl_roadmap_obj_t& xpeer_base_rt1_t::this_rdm()const noexcept {
	CBL_VERIFY(_addrh);

	static_assert(std::is_lvalue_reference_v<decltype(_addrh->this_rdm())>, __CBL_FILEW__);
	return _addrh->this_rdm();
	}

const identity_descriptor_t& xpeer_base_rt1_t::this_id()const noexcept {
	CBL_VERIFY(_addrh);

	static_assert(std::is_lvalue_reference_v<decltype(_addrh->this_id())>, __CBL_FILEW__);
	return _addrh->this_id();
	}

const rtl_roadmap_obj_t& xpeer_base_rt1_t::remote_rdm()const noexcept {
	CBL_VERIFY(_addrh);

	static_assert(std::is_lvalue_reference_v<decltype(_addrh->remote_rdm())>, __CBL_FILEW__);
	return _addrh->remote_rdm();
	}

const identity_descriptor_t& xpeer_base_rt1_t::remote_id()const noexcept {
	CBL_VERIFY(_addrh);

	static_assert(std::is_lvalue_reference_v<decltype(_addrh->remote_id())>, __CBL_FILEW__);
	return _addrh->remote_id();
	}

const identity_descriptor_t& xpeer_base_rt1_t::id()const noexcept {
	CBL_VERIFY(_addrh);

	static_assert(std::is_lvalue_reference_v<decltype(_addrh->id())>, __CBL_FILEW__);
	return _addrh->id();
	}

const std::string& xpeer_base_rt1_t::name()const noexcept {
	CBL_VERIFY(_addrh);

	static_assert(std::is_lvalue_reference_v<decltype(_addrh->name())>, __CBL_FILEW__);
	return _addrh->name();
	}

const std::string& xpeer_base_rt1_t::address()const  noexcept {
	CBL_VERIFY(_addrh);

	static_assert(std::is_lvalue_reference_v<decltype(_addrh->address())>, __CBL_FILEW__);
	return _addrh->address();
	}

int xpeer_base_rt1_t::port()const  noexcept {
	CBL_VERIFY(_addrh);
	return _addrh->port();
	}

connection_key_t xpeer_base_rt1_t::connection_key()const  noexcept {
	return connection_key_t{ address(), port() };
	}

rtl_table_t xpeer_base_rt1_t::direction()const noexcept {
	CBL_VERIFY(_addrh);
	return _addrh->direction();
	}

const session_description_t& xpeer_base_rt1_t::session()const noexcept {
	CBL_VERIFY(_addrh);

	static_assert(std::is_lvalue_reference_v<decltype(_addrh->session())>, __CBL_FILEW__);
	return _addrh->session();
	}

int xpeer_base_rt1_t::tag()const noexcept {
	return _tag.load(std::memory_order::memory_order_acquire);
	}

void xpeer_base_rt1_t::set_tag(int value) noexcept {
	_tag.store(value, std::memory_order::memory_order_release);
	}

std::shared_ptr<const base_stream_x_rt1_t> xpeer_base_rt1_t::source_stream()const noexcept {
	return _sourceStream.lock();
	}

std::shared_ptr<base_stream_x_rt1_t> xpeer_base_rt1_t::source_stream() noexcept {
	return _sourceStream.lock();
	}

const xpeer_base_rt1_t::rdm_local_identity_t& xpeer_base_rt1_t::rdm_local_identity()const noexcept {
	static_assert(std::is_lvalue_reference_v<decltype(desc())>, __CBL_FILEW__);
	return desc();
	}

const local_object_id_t& xpeer_base_rt1_t::local_id()const noexcept {
	CBL_VERIFY(_addrh);

	static_assert(std::is_lvalue_reference_v<decltype(_addrh->local_id())>, __CBL_FILEW__);
	return _addrh->local_id();
	}

const xpeer_base_rt1_t::xpeer_desc_t& xpeer_base_rt1_t::desc()const noexcept {
	CBL_VERIFY(_addrh);

	static_assert(std::is_lvalue_reference_v<decltype(_addrh->desc())>, __CBL_FILEW__);
	return _addrh->desc();
	}

void xpeer_base_rt1_t::set_state(remote_object_state_t st) noexcept {
	CBL_VERIFY(_addrh);
	_addrh->set_state(st);
	}

remote_object_state_t xpeer_base_rt1_t::state()const noexcept {
	CBL_VERIFY(_addrh);
	return _addrh->state();
	}

bool xpeer_base_rt1_t::closed()const noexcept {
	return state() == remote_object_state_t::closed || _stop.load(std::memory_order::memory_order_acquire);
	}

bool xpeer_base_rt1_t::is_opened()const noexcept {
	auto fc0 = !closed();
	auto fc1 = source_stream();
	auto fc2 = fc1 && !fc1->closed();

	return fc0 && fc1 && fc2;
	}

rtl_level_t xpeer_base_rt1_t::level()const noexcept {
	CBL_VERIFY(_addrh);
	return _addrh->level();
	}

std::shared_ptr<xpeer_base_rt1_t::input_stack_t> xpeer_base_rt1_t::get_input_queue() noexcept {
	return std::atomic_load_explicit(&_inqAdpt, std::memory_order::memory_order_acquire);
	}

std::shared_ptr<xpeer_base_rt1_t::input_stack_t> xpeer_base_rt1_t::get_input_queue()const noexcept {
	return std::atomic_load_explicit(&_inqAdpt, std::memory_order::memory_order_acquire);
	}

std::shared_ptr<i_peer_statevalue_t>  xpeer_base_rt1_t::state_value() noexcept {
	return std::atomic_load_explicit(&_stateValue, std::memory_order::memory_order_acquire);
	}

std::shared_ptr<const i_peer_statevalue_t>  xpeer_base_rt1_t::state_value()const noexcept {
	return std::atomic_load_explicit(&_stateValue, std::memory_order::memory_order_acquire);
	}

void  xpeer_base_rt1_t::set_state_value(std::shared_ptr<i_peer_statevalue_t> && stv) noexcept {
	std::atomic_exchange(&_stateValue, std::move(stv));
	}

std::shared_ptr<i_x1_stream_address_handler> xpeer_base_rt1_t::address_handler()const noexcept {
	return _addrh;
	}

