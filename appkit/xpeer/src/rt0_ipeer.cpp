#include "../../internal.h"
#include "../../context/context.h"
#include "../rt0_ipeer.h"


using namespace crm;
using namespace crm::detail;


std::shared_ptr<__handshake_in_state_t> xpeer_in_rt0_t::handshake_state_capture()noexcept {
	return handshake_state_release();
	}

std::shared_ptr<__handshake_in_state_t> xpeer_in_rt0_t::handshake_state_release()noexcept {
	return std::atomic_exchange(&_handshakeSt, std::shared_ptr<detail::__handshake_in_state_t>());
	}

void xpeer_in_rt0_t::handshake_state_register(std::shared_ptr<__handshake_in_state_t> && st) {
	auto old(std::atomic_exchange(&_handshakeSt, std::move(st)));
	if(old) {
		CHECK_NO_EXCEPTION(old->invoke_error(nullptr));
		}
	}

void xpeer_in_rt0_t::start_handshake(std::shared_ptr<__rtx_in_connection_ctx_timered_t> && connCtx,
	ctr_0_input_queue_t && inputStack) {

	set_state(remote_object_state_t::identity_stage);

	/* ��������������� ��������� ������� */
	set_handshake_mode(inputStack ? inputStack() : std::shared_ptr<input_stack_t>());

	/* ���������� ��������� �����������
	----------------------------------------------------------------------------*/
	CBL_MPF_IF_NOT_ERROR_EMULATION(CBL_VERIFY(connCtx.use_count() == 1));
	handshake_state_register(std::make_shared<detail::__handshake_in_state_t>(std::move(connCtx)));

	MPF_TEST_EMULATION_IN_CONTEXT_EXPR_COND(ctx(), 201, is_trait_at_emulation_context(level()));
	}

void xpeer_in_rt0_t::on_handshake_success() {
	
	/* ��������� �������� ���������� */
	handshake_end( true );
	set_state( remote_object_state_t::identity_stage_ready);

	MPF_TEST_EMULATION_IN_CONTEXT_EXPR_COND(ctx(), 202, is_trait_at_emulation_context(level()));
	CBL_MPF_TRACELEVEL_EVENT_RT0_CONNECTION_TRACE("inbound handshake ok:" + connection_key().to_string());
	}

xpeer_in_rt0_t::~xpeer_in_rt0_t() {
	CHECK_NO_EXCEPTION(destroy());
	}

/* ������������� ���������� �������-����������� */
xpeer_in_rt0_t::xpeer_in_rt0_t( const std::weak_ptr<distributed_ctx_t> & ctx,
	std::unique_ptr<i_xpeer_source_t> && source_,
	std::string addr,
	int port,
	std::unique_ptr<iol_gtw_t> && gtw_,
	rtx_object_closed_t closingHndl,
	std::shared_ptr<i_peer_statevalue_t> && stval,
	const identity_descriptor_t & thisId,
	__event_handler_point_f&& eh)
	: base_t(ctx,
		std::move(source_),
		std::move(addr),
		std::move(port),
		std::move(gtw_),
		std::move(closingHndl),
		std::move(stval),
		thisId,
		peer_direction_t::in,
		std::move(eh)) {}

void xpeer_in_rt0_t::destroy()noexcept {
	if(auto st = handshake_state_release()) {
		CHECK_NO_EXCEPTION(st->close());
		}

	CHECK_NO_EXCEPTION( base_t::destroy());
	}

void xpeer_in_rt0_t::command_handler_dgt_initialize( std::unique_ptr<isendrecv_datagram_t> &&   ) {
	FATAL_ERROR_FWD(nullptr);
	}

void xpeer_in_rt0_t::command_handler_dtg_identity(std::unique_ptr<isendrecv_datagram_t>&& iDtg_) {

	/* ����������� ����������������� ������ ��������� ���� */
	const auto identityHO(handshake_state_capture());
	if (identityHO && !identityHO->empty()) {

		auto exceptionHandler = [this, &identityHO, &iDtg_](std::unique_ptr<dcf_exception_t>&& exc_)noexcept {

			if (iDtg_) {
				/* �������� ��������� ���������� ������� */
				iDtg_->invoke_reverse_exception(exc_->dcpy());
				}

			/* ��������� ���������� �������� �� ���� ������� */
			if (identityHO) {
				CHECK_NO_EXCEPTION(identityHO->invoke_error(std::move(exc_)));
				}
			};

		try {
			MPF_TEST_EMULATION_IN_CONTEXT_COND_TRCX(ctx(), 203,
				is_trait_at_emulation_context(*iDtg_),
				trait_at_keytrace_set(*iDtg_));

			auto cvRemote = iDtg_->value().get_value<remote_object_connection_value_t>("", 0);
			std::unique_ptr<i_peer_remote_properties_t> remoteProp;

			/* ��������� ������� ��������� */
			if (identityHO->remote_id_validate(cvRemote.identity(), remoteProp)) {

				/* �������, ��� ����������� ��� ���� ��������� */
				const auto isRemoteTuched = identityHO->set_remote_tuched();
				if (!isRemoteTuched) {

					if (state() >= remote_object_state_t::identity_stage_ready) {
						FATAL_ERROR_FWD(nullptr);
						}

					/* ��������� ��������� ������������� */
					set_id(cvRemote.identity().id_value);

					const auto pPropId = cvRemote.identity().properties.find_by_key(identity_properties_id_desc);
					if (pPropId) {
						const auto idValue = base_peer_identity_properties_t::dsrlz_(pPropId->data);
						}

					/* �������� ��������� �������
					----------------------------------------------*/
					if (remoteProp)
						set_remote_properties(std::move(remoteProp));


					/* ���������� ����������� ������ ����������������� ��������� ������� ���� � ������� �������������
					---------------------------------------------------------------------------------------------------*/
					set_session_input(cvRemote.identity().id_value, iDtg_->session());
					}

				/* �������� ����������� ����������������� ������
				-------------------------------------------------------------------*/
				auto idVal = identityHO->local_id_ctr();
				if (idVal.identity().id_value != this_id())
					THROW_MPF_EXC_FWD(nullptr);

				MPF_TEST_EMULATION_IN_CONTEXT_COND_TRCX(ctx(), 204,
					is_trait_at_emulation_context(*iDtg_),
					trait_at_keytrace_set(*iDtg_));

				/* ��������� � ������������������ ������� */
				osendrecv_datagram_t msg{};
				msg.set_key(command_name_identity);
				msg.value().add_value("", idVal);
				msg.capture_response_tail((*iDtg_));

				/* �������� � ���� */
				push(std::move(msg));

				/* ����������� � ���������� ��������� �������������
				--------------------------------------------------------------------*/
				identityHO->invoke_state_setup(cvRemote.syncdata_release());
				identityHO->commit();

				CBL_VERIFY(state() >= remote_object_state_t::identity_stage_ready);
				}
			else {
				exceptionHandler(CREATE_MPF_PTR_EXC_FWD(nullptr));
				}
			}
		catch (const dcf_exception_t & exc0) {
			exceptionHandler(exc0.dcpy());
			}
		catch (const std::exception & exc1) {
			exceptionHandler(CREATE_PTR_EXC_FWD(exc1));
			}
		catch (...) {
			exceptionHandler(CREATE_PTR_EXC_FWD(nullptr));
			}
		}
	else {
		close(closing_reason_t::break_connection);
		}
	}

std::unique_ptr<execution_bool_tail> xpeer_in_rt0_t::make_register_completed() {

	CBL_MPF_IF_NOT_ERROR_EMULATION(CBL_VERIFY(weak_from_this().use_count() == 1));

	auto registerAccept = [wptr = weak_from_this()

#ifdef CBL_MPF_TRACELEVEL_EVENT_CONNECTION
		,ck = connection_key().to_string()
#endif
	](async_operation_result_t result) {

		CBL_MPF_TRACELEVEL_EVENT_RT0_CONNECTION_TRACE(
			std::string("inbound register:") + cvt2string(result) + ":" + ck);

		if(result != async_operation_result_t::st_ready) {
			if(auto sptr = wptr.lock()) {
				CHECK_NO_EXCEPTION(sptr->close());
				}
			}
		};

	if(CHECK_PTR(ctx())->policies().connection_timepoints().is_timeouted()) {
		auto completeTimer = CHECK_PTR(ctx())->create_timer_deffered_action_sys(
			{"rt0-peer-in-completed-h"},
			std::move(registerAccept),
			CHECK_PTR(ctx())->policies().connection_timepoints().timeout());

		/* ������� ����������� */
		return register_in_connection_completed_make_ptr(ctx(), utility::bind_once_call({"xpeer_in_rt0_t::make_register_completed 1"},
			[](bool r, decltype(completeTimer) && timer) noexcept{
			if(timer) {
				if(r) {
					CHECK_NO_EXCEPTION(timer->invoke(async_operation_result_t::st_ready));
					}
				else {
					CHECK_NO_EXCEPTION(timer->invoke(async_operation_result_t::st_exception));
					}
				}
			}, std::placeholders::_1, std::move(completeTimer)));
		}
	else {
		return register_in_connection_completed_make_ptr(ctx(), utility::bind_once_call({ "xpeer_in_rt0_t::make_register_completed 2" },
			[](bool r, std::function<void(async_operation_result_t)> && registerAccept) {
			if(registerAccept) {
				if(r) {
					CHECK_NO_EXCEPTION(registerAccept(async_operation_result_t::st_ready));
					}
				else {
					CHECK_NO_EXCEPTION(registerAccept(async_operation_result_t::st_exception));
					}
				}
			else {
				THROW_EXC_FWD(nullptr);
				}
			}, std::placeholders::_1, std::move(registerAccept)));
		}
	}


void xpeer_in_rt0_t::accept(local_object_create_connection_t && localIdCtr,
	remote_object_check_identity_t && remoteIdChecker,
	identification_success_f && connectionHndl,
	ctr_0_input_queue_t && inputStack) {

	using connection_handler_kp = __inbound_connection_result_t<link_t>;

	CBL_MPF_TRACELEVEL_EVENT_RT0_CONNECTION_TRACE("inbound handshake begin:" + connection_key().to_string());

	auto registerCompleted = make_register_completed();
	CBL_MPF_IF_NOT_ERROR_EMULATION(CBL_VERIFY(weak_from_this().use_count() == 1));

	MPF_TEST_EMULATION_IN_CONTEXT_EXPR_COND(ctx(), 205, is_trait_at_emulation_context(level()));

	/* ������� ��������� ��������� ����������� ��������� ������������� */
	auto identityRslt = utility::bind_once_call({ "xpeer_in_rt0_t::accept" }, 
		[wptr = weak_from_this()](async_operation_result_t result,
		std::unique_ptr<dcf_exception_t> && exc,
		syncdata_list_t && sl,
		std::unique_ptr<connection_handler_kp> && pHndl,
		decltype(registerCompleted) && registerCompleted)noexcept{

		if(auto sptr = std::dynamic_pointer_cast<self_t>(wptr.lock())) {
			auto exchndl = [&pHndl, &sptr](std::unique_ptr<dcf_exception_t> && e)noexcept {
				if(pHndl) {
					CHECK_NO_EXCEPTION(pHndl->refuse(std::move(e)));
					}

				CHECK_NO_EXCEPTION(sptr->close());
				};

			try {
				/* �������� ���������� ��������� �������� �������������
				--------------------------------------------------------------------*/
				if(result == async_operation_result_t::st_ready && !exc) {
					if(!is_null(sptr->rt0rdm())) {
						sptr->on_extern_completed([&] {
							if(pHndl) {
								MPF_TEST_EMULATION_IN_CONTEXT_EXPR_COND(sptr->ctx(), 206, is_trait_at_emulation_context(sptr->level()));
								pHndl->ready(sptr->get_link(), std::move(sl), std::move(registerCompleted));
								}
							});
						}
					else {
						if(pHndl) {
							CHECK_NO_EXCEPTION(pHndl->refuse(CREATE_MPF_PTR_EXC_FWD(nullptr)));
							}
						}
					}
				else {
					if(pHndl) {
						if(exc) {
							CHECK_NO_EXCEPTION(pHndl->refuse(std::move(exc)));
							}
						else {
							CHECK_NO_EXCEPTION(pHndl->refuse(CREATE_MPF_PTR_EXC_FWD(nullptr)));
							}
						}
					}
				}
			catch(const dcf_exception_t & e0) {
				exchndl(e0.dcpy());
				}
			catch(const std::exception & e1) {
				exchndl(CREATE_PTR_EXC_FWD(e1));
				}
			catch(...) {
				exchndl(CREATE_MPF_PTR_EXC_FWD(nullptr));
				}
			}
		},
		std::placeholders::_1, 
		std::placeholders::_2, 
		std::placeholders::_3,
		std::make_unique<connection_handler_kp>(ctx(), std::move(connectionHndl)),
		std::move(registerCompleted));

	MPF_TEST_EMULATION_IN_CONTEXT_EXPR_COND(ctx(), 207, is_trait_at_emulation_context(level()));

	set_state(remote_object_state_t::connect_stage_ready);

	/* ����� ��������� ������ ����������� ����������� */
	start_handshake(detail::__rtx_in_connection_ctx_timered_t::launch(ctx(),
		std::move(identityRslt),
		std::move(localIdCtr),
		std::move(remoteIdChecker),
		weak_from_this()),
		std::move(inputStack));
	CBL_MPF_IF_NOT_ERROR_EMULATION(CBL_VERIFY(weak_from_this().use_count() == 1));

	/* ������ ��������� ��������� ���������� �����/������ */
	io_rw_listen();
	//CBL_MPF_IF_NOT_ERROR_EMULATION(CBL_VERIFY(weak_from_this().use_count() == 2));

	MPF_TEST_EMULATION_IN_CONTEXT_EXPR_COND(ctx(), 208, is_trait_at_emulation_context(level()));
	}

