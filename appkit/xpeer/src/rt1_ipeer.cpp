#include "../../internal.h"
#include "../../context/context.h"
#include "../rt1_ipeer.h"
#include "../../context/context.h"
#include "../rt1_istream.h"
#include "../rt1_end_ipeer.h"


using namespace crm;
using namespace crm::detail;


xpeer_in_rt1_t::~xpeer_in_rt1_t() {

	CBL_MPF_TRACELEVEL_EVENT_RT1_CONNECTION_TRACE("inbound link:destroyed:"
		+ connection_key().to_string()
		+ ":s=" + remote_id().to_str()
		+ ":t=" + this_id().to_str()
		+ ":session=" + session().object_hash().to_str());
	}

std::shared_ptr<__handshake_in_state_t> xpeer_in_rt1_t::handshake_state_capture()  noexcept {
	return std::atomic_exchange(&_handshakeSt, std::shared_ptr<__handshake_in_state_t>());
	}

void xpeer_in_rt1_t::handshake_state_register(std::shared_ptr<__handshake_in_state_t> && st) {
	auto old(std::atomic_exchange(&_handshakeSt, std::move(st)));
	if(old) {
		THROW_MPF_EXC_FWD(nullptr);
		}
	}

void xpeer_in_rt1_t::on_handshake_success() {

	/* ��������� �������� ���������� */
	handshake_end( true );
	set_state(remote_object_state_t::identity_stage_ready);

	CBL_MPF_TRACELEVEL_EVENT_RT1_CONNECTION_TRACE("in-peer:handshake ok:"
		+ connection_key().to_string()
		+ ":s=" + remote_id().to_str()
		+ ":t=" + this_id().to_str()
		+ ":session=" + session().object_hash().to_str());
	}

void xpeer_in_rt1_t::command_handler_dtg_identity( std::unique_ptr<isendrecv_datagram_t> && iDtg_) {
	CBL_MPF_KEYTRACE_SET((*iDtg_), 420);

	/* ����������� ����������������� ������ ��������� ���� */
	const auto identityHndl(handshake_state_capture());
	if (identityHndl && !identityHndl->empty()) {
		auto exceptionHandler = [this, &identityHndl, &iDtg_](std::unique_ptr<dcf_exception_t>&& exc_)noexcept {
			CBL_VERIFY(exc_);

			if (iDtg_) {
				/* �������� ��������� ���������� ������� */
				iDtg_->invoke_reverse_exception(exc_->dcpy());
				}

			if (identityHndl) {
				identityHndl->invoke_error(std::move(exc_));
				}

			close();
			};

		try {
			CBL_VERIFY(iDtg_->key() == command_name_identity);
			CBL_VERIFY(iDtg_->address().destination_id() == this_id() && !is_null(this_id()));

			MPF_TEST_EMULATION_IN_CONTEXT_I_PEER_MESSAGE_COND(ctx(), 310, is_trait_at_emulation_context(*iDtg_));

			auto cv = iDtg_->value().get_value<remote_object_connection_value_t>("", 0);
			std::unique_ptr<i_peer_remote_properties_t> remoteProp;

			/* ��������� ������� ���������  */
			if (identityHndl->remote_id_validate(cv.identity(), remoteProp)) {

				/* �������, ��� ����������� ��� ���� ��������� */
				const auto isRemoteTuched = identityHndl->set_remote_tuched();
				if (!isRemoteTuched) {

					if (state() >= remote_object_state_t::identity_stage_ready) {
						FATAL_ERROR_FWD(nullptr);
						}

					auto remoteId = cv.identity().id_value;
					if (remoteId != remote_id())
						THROW_MPF_EXC_FWD(nullptr);
					if (remoteId != id())
						THROW_MPF_EXC_FWD(nullptr);

					/* ��������� ��������� ������������� */
					const auto pPropId = cv.identity().properties.find_by_key(identity_properties_id_desc);
					if (pPropId) {
						const auto idValue = base_peer_identity_properties_t::dsrlz_(pPropId->data);
						}

					/* �������� ��������� �������
					----------------------------------------------*/
					if (remoteProp)
						set_remote_properties(std::move(remoteProp));
					}

				/* �������� ����������� ����������������� ������
				-------------------------------------------------------------------*/
				auto idVal = identityHndl->local_id_ctr();
				if (idVal.identity().id_value != this_id())
					THROW_MPF_EXC_FWD(nullptr);

				MPF_TEST_EMULATION_IN_CONTEXT_I_PEER_MESSAGE_COND(ctx(), 311, is_trait_at_emulation_context(*iDtg_));

				/* ��������� � ������������������ ������� */
				osendrecv_datagram_t msg{};
				msg.set_key(command_name_identity);
				msg.value().add_value("", idVal);
				msg.capture_response_tail((*iDtg_));

				CBL_VERIFY(!is_trait_at_emulation_context(msg));

				/* �������� � ���� */
				push(std::move(msg));

				CBL_MPF_KEYTRACE_SET((*iDtg_), 421);

				/* ����������� � ���������� ��������� �������������
				--------------------------------------------------------------------*/
				identityHndl->invoke_state_setup(cv.syncdata_release());
				identityHndl->commit();

				CBL_VERIFY(state() >= remote_object_state_t::identity_stage_ready);
				CBL_MPF_KEYTRACE_SET((*iDtg_), 422);
				}
			else {
				exceptionHandler(CREATE_EXTERN_CODE_PTR_EXC_FWD(nullptr));
				}
			}
		catch (const dcf_exception_t & exc0) {
			exceptionHandler(exc0.dcpy());
			}
		catch (const std::exception & exc1) {
			exceptionHandler(CREATE_PTR_EXC_FWD(exc1));
			}
		catch (...) {
			exceptionHandler(CREATE_PTR_EXC_FWD(nullptr));
			}
		}
	else {
		close( closing_reason_t::break_connection );
		}
	}

void xpeer_in_rt1_t::command_handler_dgt_initialize( std::unique_ptr<isendrecv_datagram_t> &&) {
	FATAL_ERROR_FWD(nullptr);
	}

std::shared_ptr<ix_rt1_stream_t> xpeer_in_rt1_t::isource()noexcept {
	auto src( source_stream() );
	if(src) {
		auto isrc = std::dynamic_pointer_cast<ix_rt1_stream_t>(src);
		if(isrc)
			return isrc;
		else
			return nullptr;
		}
	else
		return nullptr;
	}

xpeer_in_rt1_t::xpeer_in_rt1_t( const std::weak_ptr<distributed_ctx_t> & ctx_,
	std::shared_ptr<ix_rt1_stream_t> && source_,
	std::unique_ptr<gtw_rt1_x_t> && gtw,
	rtx_object_closed_t && closingHndl,
	std::shared_ptr<i_peer_statevalue_t> && stval,
	__event_handler_point_f && eh )
	: base_t( ctx_, 
		std::move( source_ ),
		std::move(gtw),
		std::move(closingHndl),
		std::move(stval),
		std::move(eh)){}

void xpeer_in_rt1_t::register_completed(async_operation_result_t result) {
	CBL_MPF_TRACELEVEL_EVENT_RT1_CONNECTION_TRACE(std::string("in-peer:register:")
		+ cvt2string(result)
		+ connection_key().to_string()
		+ ":s=" + remote_id().to_str()
		+ ":t=" + this_id().to_str()
		+ ":session=" + session().object_hash().to_str());

	if (result == async_operation_result_t::st_ready) {
		event_invoke(event_type_t::st_connected, std::make_unique<event_open_connection_t>(get_link()));
		}
	else {
		CHECK_NO_EXCEPTION(close());
		}
	}

std::unique_ptr<execution_bool_tail> xpeer_in_rt1_t::make_register_completed() {
		
	CBL_MPF_IF_NOT_ERROR_EMULATION(CBL_VERIFY(weak_from_this().use_count() == 1));

	/* ������� ���������� ��������� ����������� ������� �����������
	----------------------------------------------------------------------*/
	auto registerAccept_ = [wthis = weak_from_this()](async_operation_result_t result) {
		if(auto sp = std::dynamic_pointer_cast<xpeer_in_rt1_t>( wthis.lock())) {
			sp->register_completed(result);
			}
		};

	if(CHECK_PTR(ctx())->policies().connection_timepoints().is_timeouted()) {

		/* ������ �� ������� ���������� ��������� �����������
		---------------------------------------------------------------------------*/
		auto completeTimer = CHECK_PTR(ctx())->create_timer_deffered_action_sys(
			{ "rt1-peer-in-completed-h" },
			std::move(registerAccept_),
			CHECK_PTR(ctx())->policies().connection_timepoints().timeout());

		/* ���������� �����������
		---------------------------------------------------------------------------*/
		return register_in_connection_completed_make_ptr(ctx(), 
			crm::utility::bind_once_call({ "xpeer_in_rt1_t::make_register_completed" },
			[](bool r, decltype(completeTimer) && timer) {
			if(timer) {
				if(r) {
					CHECK_NO_EXCEPTION(timer->invoke(async_operation_result_t::st_ready));
					}
				else {
					CHECK_NO_EXCEPTION(timer->invoke(async_operation_result_t::st_exception));
					}
				}
			}, std::placeholders::_1, std::move(completeTimer)));
		}
	else {
		/* ���������� �����������
		---------------------------------------------------------------------------*/
		return register_in_connection_completed_make_ptr(ctx(),
			crm::utility::bind_once_call({ "xpeer_in_rt1_t::make_register_completed" },
				[](bool r, std::function<void(async_operation_result_t)> && registerAccept) {
			if(registerAccept) {
				if(r) {
					CHECK_NO_EXCEPTION(registerAccept(async_operation_result_t::st_ready));
					}
				else {
					CHECK_NO_EXCEPTION(registerAccept(async_operation_result_t::st_exception));
					}
				}
			}, std::placeholders::_1, std::move(registerAccept_)));
		}
	}

void xpeer_in_rt1_t::start_handshake( local_object_create_connection_t && localIdCtr,
	remote_object_check_identity_t && remoteIdChecker,
	ctr_1_input_queue_t && ctrInput,
	identification_success_f && hndl) {

	using connection_handler_kp = __inbound_connection_result_t<link_t>;

	CBL_VERIFY(ctrInput);

	set_state(remote_object_state_t::identity_stage);

	MPF_TEST_EMULATION_IN_CONTEXT_EXPR_COND(ctx(), 312, is_trait_at_emulation_context(level()));

	auto rgcmpl = make_register_completed();
	CBL_MPF_IF_NOT_ERROR_EMULATION(CBL_VERIFY(weak_from_this().use_count() == 1));

	MPF_TEST_EMULATION_IN_CONTEXT_EXPR_COND(ctx(), 313, is_trait_at_emulation_context(level()));

	/* ���������� ���������� ��������� �������������
	---------------------------------------------------------------------------*/
	auto identityRslt = crm::utility::bind_once_call({ "xpeer_in_rt1_t::start_handshake" }, 
		[wptr = weak_from_this()](async_operation_result_t result,
		std::unique_ptr<dcf_exception_t> && exc,
		syncdata_list_t && sl,
		decltype(rgcmpl) && registerCompleted,
		std::unique_ptr<connection_handler_kp> && pHndl)noexcept{

		if(auto sptr = std::dynamic_pointer_cast<self_t>(wptr.lock())) {
			auto exchndl = [&pHndl, &sptr](std::unique_ptr<dcf_exception_t> && e)noexcept {
				if(pHndl) {
					CHECK_NO_EXCEPTION(pHndl->refuse(std::move(e)));
					}

				CHECK_NO_EXCEPTION(sptr->close());
				};

			try {
				if(result == async_operation_result_t::st_ready) {
					sptr->on_extern_completed([&] {
						if(pHndl) {
							MPF_TEST_EMULATION_IN_CONTEXT_EXPR_COND(sptr->ctx(), 314, is_trait_at_emulation_context(sptr->level()));
							pHndl->ready(sptr->get_link(), std::move(sl), std::move(registerCompleted));
							}
						});
					}
				else if(exc) {
					if(pHndl) {
						CHECK_NO_EXCEPTION(pHndl->refuse(std::move(exc)));
						}
					}

				}
			catch(const dcf_exception_t & e0) {
				exchndl(e0.dcpy());
				}
			catch(const std::exception & e1) {
				exchndl(CREATE_PTR_EXC_FWD(e1));
				}
			catch(...) {
				exchndl(CREATE_MPF_PTR_EXC_FWD(nullptr));
				}
			}
		},
			std::placeholders::_1, 
			std::placeholders::_2, 
			std::placeholders::_3,
			std::move(rgcmpl), 
			std::make_unique<connection_handler_kp>(ctx(), std::move(hndl)));

	CBL_MPF_IF_NOT_ERROR_EMULATION(CBL_VERIFY(weak_from_this().use_count() == 1));
	initialize();
	CBL_MPF_IF_NOT_ERROR_EMULATION(CBL_VERIFY(weak_from_this().use_count() == 2));

	set_handshake_mode(ctrInput());

	MPF_TEST_EMULATION_IN_CONTEXT_EXPR_COND(ctx(), 315, is_trait_at_emulation_context(level()));

	/* ���������� ��������� �����������
	----------------------------------------------------------------------------*/
	handshake_state_register(std::make_shared<__handshake_in_state_t>(__rtx_in_connection_ctx_timered_t::launch(ctx(),
		std::move(identityRslt),
		std::move(localIdCtr),
		std::move(remoteIdChecker),
		weak_from_this())));

	CBL_MPF_TRACELEVEL_EVENT_RT1_CONNECTION_TRACE("in-peer:handshake begin:"
		+ connection_key().to_string()
		+ ":s=" + remote_id().to_str()
		+ ":t=" + this_id().to_str()
		+ ":session=" + session().object_hash().to_str());
	}

void xpeer_in_rt1_t::accept(std::shared_ptr<self_t> && obj,
	local_object_create_connection_t && localIdCtr,
	remote_object_check_identity_t && remoteIdChecker,
	ctr_1_input_queue_t && ctrInput,
	identification_success_f && connectionHndl) {

	CBL_MPF_IF_NOT_ERROR_EMULATION(CBL_VERIFY(obj.use_count() == 1));
	if(ctrInput) {

		obj->set_state(remote_object_state_t::connect_stage_ready);

		CBL_MPF_TRACELEVEL_EVENT_RT1_CONNECTION_TRACE("in-peer:accept:"
			+ obj->connection_key().to_string()
			+ ":t=" + obj->this_id().to_str()
			+ ":session=" + obj->session().object_hash().to_str());

		/* ����� ��������� �������� ������������� */
		obj->start_handshake(std::move(localIdCtr),
			std::move(remoteIdChecker),
			std::move(ctrInput),
			std::move(connectionHndl));

		/* ����� ����� ������������� ����������� �����/������ */
		obj->io_rw_listen();

		MPF_TEST_EMULATION_IN_CONTEXT_EXPR_COND(obj->ctx(), 316, is_trait_at_emulation_context(obj->level()));
		}
	else {
		THROW_MPF_EXC_FWD(nullptr);
		}
	}


class __handler_input_ipeer_t : public i_xpeer_rt1_t::input_stack_t {
private:
	ipeer_t peer;
	i_xpeer_rt1_t::input_handler_t hndl;

public:
	__handler_input_ipeer_t(ipeer_t && plnk,
		i_xpeer_rt1_t::input_handler_t && hndl_)
		: peer(std::move(plnk))
		, hndl(std::move(hndl_)) {

		CBL_VERIFY(hndl);
		}

	std::uintmax_t stored_count()const noexcept final {
		return 0;
		}

	void clear()noexcept final {}

	void push(value_type && message)final {
		hndl(peer, std::move(message));
		}
	};


std::unique_ptr<xpeer_in_rt1_t::input_stack_t> xpeer_in_rt1_t::make_input_stack(std::weak_ptr<self_t> wptr, input_handler_t && hndlInput) {
	if(auto sptr = wptr.lock()) {
		return std::make_unique<__handler_input_ipeer_t>(ipeer_t(sptr->get_link()), std::move(hndlInput));
		}
	else {
		return {};
		}
	}
