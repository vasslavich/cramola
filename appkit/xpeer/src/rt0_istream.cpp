#include "../../internal.h"
#include "../../context/context.h"
#include "../../xpeer/rt0_xpeer.h"
#include "../rt0_istream.h"
#include "../../cmdrt/terms.h"
#include "../../queues/qdef.h"
#include "../../channel_terms/i_route_io.h"
#include "../../streams/istreams_routetbl.h"


using namespace crm;
using namespace crm::detail;


void istream_rt0_x_t::unbind_reslink() noexcept {
	if(auto stb = CHECK_PTR(ctx())->streams_handler()) {
		CHECK_NO_EXCEPTION(stb->close(local_id()));
		}
	}

istream_rt0_x_t::~istream_rt0_x_t(){
	CHECK_NO_EXCEPTION(unbind_reslink());

	CBL_MPF_TRACELEVEL_EVENT_RT0_CONNECTION_TRACE("inbound_connection:stream:destroyed:s="
		+ connection_key().to_string()
		+ ":s=" + remote_id().to_str()
		+ ":t=" + this_id().to_str()
		+ ":session=" + session().object_hash().to_str());
	}

/*! ��������� ��������� ��������� � ���� */
void istream_rt0_x_t::route_to_target_stock( const std::weak_ptr<i_rt0_main_router_input_t> & rt, 
	input_value_t && v ) {

	if(!closed() && v.message() ) {

		if(v.message()->type().type() == iol_types_t::st_packet) {
			if(is_null(v.message()->destination_id()) || is_null( v.message()->source_id()))
				FATAL_ERROR_FWD(nullptr);

#pragma message( CBL_VAR_NAME_VALUE(CBL_BUILD_VAR_SNDRCV_REVERSE_GUARD, 31))
			if(auto ipacket = message_cast<ipacket_t>(std::move(v).message())) {

				rtl_roadmap_line_t rdl_;
				rdl_.points = make_message( rt0rdm(), (*ipacket));

#ifdef USE_DCF_CHECK
				if (is_null(rdl_.points.target_id()) || rdl_.points.table() == rtl_table_t::undefined) {
					FATAL_ERROR_FWD(rdl_.points.to_str());
					}
#endif

				rt0_x_message_unit_t xObj(rt0_x_message_t::make(std::move(rdl_), std::move(*ipacket).packet(), connection_key()));

#pragma message( CBL_VAR_NAME_VALUE(CBL_BUILD_VAR_SNDRCV_REVERSE_GUARD, 32))
				if(xObj.reset_invoke_exception_guard(ipacket->reset_invoke_exception_guard())) {
					FATAL_ERROR_FWD(nullptr);
					}

				MPF_TEST_EMULATION_IN_CONTEXT_COND_TRCX(ctx(), 209,
					is_trait_at_emulation_context(xObj.unit().package()),
					trait_at_keytrace_set(xObj.unit().package()));

				if( auto nchannel = rt.lock() ) {
					nchannel->evhndl_from_peer(std::move(xObj));
					}
				else {
					xObj.invoke_exception( CREATE_MPF_PTR_EXC_FWD(nullptr) );
					}
				}
			else {
				FATAL_ERROR_FWD(nullptr);
				}
			}
		else {
			if( auto nchannel = rt.lock() ) {
				CBL_VERIFY( !is_null( this_id() ) );

#pragma message( CBL_VAR_NAME_VALUE(CBL_BUILD_VAR_SNDRCV_REVERSE_GUARD, 31))
				nchannel->evhndl_form_peer_rt_id(std::move(v));
				}
			else {
				v.message()->invoke_reverse_exception(CREATE_MPF_PTR_EXC_FWD(nullptr));
				}
			}
		}
	}

void istream_rt0_x_t::accept( local_object_create_connection_t && localIdCtr,
	remote_object_check_identity_t && remoteIdChecker,
	identification_success_f && connectionHndl,
	std::weak_ptr<i_rt0_main_router_input_t> && rt ) {

	std::weak_ptr<self_t> wthis( std::dynamic_pointer_cast<self_t>(shared_from_this()) );
	auto inputCtr = [wthis, rt]() {
		return std::make_unique<qinput_adptf_t<input_value_t>>( [wthis, rt]( input_value_t && obj ) {
			auto sthis( wthis.lock() );
			if( sthis ) {
				sthis->route_to_target_stock( rt, std::move( obj ) );
				}
			} );
		};
	
	base_t::accept( std::move( localIdCtr ),
		std::move( remoteIdChecker ),
		std::move( connectionHndl ),
		std::move( inputCtr ) );

	CBL_MPF_TRACELEVEL_EVENT_RT0_CONNECTION_TRACE("inbound_connection:stream:accept:s="
		+ connection_key().to_string()
		+ ":s=" + remote_id().to_str()
		+ ":t=" + this_id().to_str()
		+ ":session=" + session().object_hash().to_str());
	}

istream_rt0_x_t::istream_rt0_x_t( const std::weak_ptr<distributed_ctx_t> & ctx,
	std::unique_ptr<i_xpeer_source_t> && source_,
	std::string addr,
	int port,
	std::unique_ptr<iol_gtw_t> && gtw_,
	rtx_object_closed_t && closingHndl,
	std::shared_ptr<i_peer_statevalue_t> && stv,
	const identity_descriptor_t & thisId ,
	__event_handler_point_f&& eh )
	: base_t( ctx,
		std::move( source_ ),
		std::move(addr),
		port,
		std::move( gtw_ ),
		std::move(closingHndl),
		std::move(stv),
		thisId,
		std::move(eh)){
	
	CBL_MPF_TRACELEVEL_EVENT_RT0_CONNECTION_TRACE("inbound_connection:stream:create:s="
		+ connection_key().to_string()
		+ ":s=" + remote_id().to_str()
		+ ":t=" + this_id().to_str()
		+ ":session=" + session().object_hash().to_str());
	}

void istream_rt0_x_t::accept( const std::weak_ptr<distributed_ctx_t> & ctx,
	std::unique_ptr<i_xpeer_source_t> && source_,
	std::unique_ptr<iol_gtw_t> && gtw_,
	rtx_object_closed_t && closingHndl,
	local_object_create_connection_t && localIdCtr,
	remote_object_check_identity_t && remoteIdChecker,
	identification_success_f && connectionHndl,
	const identity_descriptor_t & thisId,
	std::weak_ptr<i_rt0_main_router_input_t> && rt,
	std::shared_ptr<i_peer_statevalue_t> && stv,
	__event_handler_point_f&& eh ) {

	auto addr = CHECK_PTR(source_)->saddress();
	auto port = CHECK_PTR(source_)->port();

	if(addr.empty() || port <= 0) {
		THROW_MPF_EXC_FWD(nullptr);
		}

	if(auto obj = std::make_shared<self_t>(ctx,
		std::move(source_),
		std::move(addr),
		port,
		std::move(gtw_),
		std::move(closingHndl),
		std::move(stv),
		thisId,
		std::move(eh))) {

		obj->accept(std::move(localIdCtr),
			std::move(remoteIdChecker),
			std::move(connectionHndl),
			std::move(rt));
		}
	else {
		THROW_MPF_EXC_FWD(nullptr);
		}
	}

