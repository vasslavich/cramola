#include "../../internal.h"
#include "../../streams/istreams_routetbl.h"
#include "../../context/context.h"
#include "../../logging/keycounter.h"
#include "../rt1_ostream.h"
#include "../../cmdrt/terms.h"
#include "../../channel_terms/xtract.h"


using namespace crm;
using namespace crm::detail;



connection_handler_guard::~connection_handler_guard() {
	CHECK_NO_EXCEPTION(invoke(async_operation_result_t::st_abandoned, nullptr));
	}

void connection_handler_guard::invoke(async_operation_result_t r, 
	std::shared_ptr<ox_rt1_stream_t> p,
	std::unique_ptr<dcf_exception_t> && e) {

	bool if_{ false };
	if(_if.compare_exchange_strong(if_, true)) {
		auto f = std::move( _f );
		f(r, std::move(p), std::move(e));
		}
	}

 void connection_handler_guard::invoke(async_operation_result_t r, std::shared_ptr<ox_rt1_stream_t> s) {
	invoke(r, s, nullptr);
	}

void connection_handler_guard::invoke(std::unique_ptr<dcf_exception_t> && e)noexcept {
	invoke(async_operation_result_t::st_exception, nullptr, std::move(e));
	}

void connection_handler_guard::operator()(async_operation_result_t r, std::shared_ptr<ox_rt1_stream_t> p) {
	invoke(r, std::move(p));
	}

 void connection_handler_guard::operator()(async_operation_result_t r, 
	 std::shared_ptr<ox_rt1_stream_t> p,
	 std::unique_ptr<dcf_exception_t> && e) {

	invoke(r, std::move(p), std::move(e));
	}

connection_handler_guard::connection_handler_guard(connection_handler_guard && o)noexcept
	: _f(std::move(o._f))
	, _if(o._if.exchange(true))

#ifdef CBL_MPF_OBJECTS_COUNTER_CHECKER_RT1_PEER
	, _xDbgCounter(std::move(o._xDbgCounter)) 
#endif
	{}

connection_handler_guard & 
connection_handler_guard::operator=( connection_handler_guard && o )noexcept{

	if( this != std::addressof( o ) ){

		_f = std::move( o._f );
		_if = o._if.exchange( true );

		#ifdef CBL_MPF_OBJECTS_COUNTER_CHECKER_RT1_PEER
		_xDbgCounter = std::move(o._xDbgCounter);
		#endif
		}

	return (*this);
	}



std::shared_ptr<i_rt1_main_router_t> ox_rt1_stream_t::router()const noexcept {
	return _xbase.router();
	}

std::shared_ptr<i_x1_stream_address_handler> ox_rt1_stream_t::address_point()const noexcept{
	return std::dynamic_pointer_cast<i_x1_stream_address_handler>(_addrh);
	}

bool ox_rt1_stream_t::set_tail(std::shared_ptr<i_decode_tail_stream2peer_t_x1> && tail_)noexcept {
	if (!closed()) {
		if (auto p = std::atomic_exchange(&_tail, tail_)) {
			p->close();
			}

		return true;
		}
	else {
		return false;
		}
	}

std::shared_ptr<i_decode_tail_stream2peer_t_x1> ox_rt1_stream_t::reset_tail()noexcept {
	return std::atomic_exchange(&_tail, {});
	}

std::shared_ptr<i_decode_tail_stream2peer_t_x1> ox_rt1_stream_t::tail()const noexcept {
	return std::atomic_load_explicit(&_tail, std::memory_order::memory_order_acquire);
	}

void ox_rt1_stream_t::set_state(remote_object_state_t st) noexcept {
	CBL_VERIFY(_addrh);
	_addrh->set_state(st);
	}

remote_object_state_t ox_rt1_stream_t::get_state()const noexcept {
	CBL_VERIFY(_addrh);
	return _addrh->state();
	}

/*size_t ox_rt1_stream_t::stack_size(const std::weak_ptr<distributed_ctx_t> ctx_)noexcept {
	auto ctx(ctx_.lock());
	if(ctx)
		return ctx->policies().stack_maxlen();
	else
		return 0;
	}*/

void ox_rt1_stream_t::ntf_hndl(std::unique_ptr<crm::dcf_exception_t> && obj)noexcept {
	_xbase.ntf_hndl(std::move(obj));
	}

void ox_rt1_stream_t::ntf_hndl(crm::dcf_exception_t && exc)noexcept{
	ntf_hndl(exc.dcpy());
	}

void ox_rt1_stream_t::ntf_hndl(const crm::dcf_exception_t & exc)noexcept{
	ntf_hndl(exc.dcpy());
	}

void ox_rt1_stream_t::register_rt0(const rt0_node_rdm_t &nodeId, __error_handler_point_f && rh) {

	MPF_TEST_EMULATION_IN_CONTEXT_EXPR_COND(ctx(), 333, is_trait_at_emulation_context(rtl_level_t::l1));

	if(is_null(nodeId) || nodeId.direction() != rtl_table_t::out)
		THROW_MPF_EXC_FWD(nullptr);

	/* �������� ��������� ������������� */
	CBL_VERIFY(_addrh);
	if(!_addrh->set_rt0(nodeId))
		THROW_MPF_EXC_FWD(nullptr);

	/* ������ ������������������ */
	register_routing(std::move(rh));
	}

bool ox_rt1_stream_t::rdm_registered()const noexcept {
	return _addrh->rdm_registered();
	}

void ox_rt1_stream_t::push_to_x(outbound_message_unit_t && buf) {

	CBL_MPF_KEYTRACE_SET(buf, 16);

	MPF_TEST_EMULATION_IN_CONTEXT_COND_TRCX(_xbase.ctx(), 405,
		is_trait_at_emulation_context(buf),
		trait_at_keytrace_set(buf));

	if(!closed()) {
		if(rdm_registered()) {

			/* �� ��������� �������� closed(), �����, ������ ���� �� ������
			��������� �������� ��������� �� ����������
			*/

			auto och(_xbase.router());
			if(och) {

				CBL_VERIFY((remote_rdm().table() == msg_rdm().table()
					&& remote_rdm().target_id() == msg_rdm().source_id()
					&& remote_rdm().source_id() == msg_rdm().target_id()
					&& remote_rdm().rt0() == msg_rdm().rt0()));

				rtl_roadmap_line_t rtl_;
				rtl_.points = msg_rdm();

				CBL_VERIFY(!rtl_.points.rt0().connection_key().address.empty());

				/* �������� �������������� ������ 1 */
				och->push2rt(rt1_x_message_t::make(std::move(rtl_), std::move(buf)));
				}
			else {
				close();
				THROW_MPF_EXC_FWD(make_trace_line());
				}
			}
		else {
			THROW_MPF_EXC_FWD(L"ox_rt1_stream_t::push_to_x:rdm not registered yet");
			}
		}
	}


rtl_table_t ox_rt1_stream_t::direction()const noexcept {
	CBL_VERIFY(_addrh);
	return _addrh->direction();
	}

const rt1_endpoint_t& ox_rt1_stream_t::endpoint()const noexcept {
	CBL_VERIFY(_addrh);
	return _addrh->endpoint();
	}

bool ox_rt1_stream_t::closed()const noexcept {
	return _closef.load(std::memory_order::memory_order_acquire);
	}

void ox_rt1_stream_t::close() noexcept {
	close(unregister_message_track_reason_t::closed_by_self);
	}

void ox_rt1_stream_t::unbind_reslink()noexcept {
	if(auto c = _xbase.ctx()) {
		if(auto stb = c->streams_handler()) {
			CBL_VERIFY(_addrh);
			CHECK_NO_EXCEPTION(stb->close(_addrh->local_id()));
			}
		}
	}

void ox_rt1_stream_t::close_object_connection_command()noexcept {
	if(_requestRT0) {
		_requestRT0->cancel();
		}
	}

void ox_rt1_stream_t::close_tail()noexcept {
	if (auto tail = reset_tail()) {
		tail->close();
		}
	}

void ox_rt1_stream_t::close_objects()noexcept {
	close_object_connection_command();
	close_tail();
	}

void ox_rt1_stream_t::close(unregister_message_track_reason_t r)noexcept {
	bool clf = false;
	if(_closef.compare_exchange_strong(clf, true)) {

		unregister_routing(r);
		close_objects();

		CBL_MPF_TRACELEVEL_EVENT_RT1_CONNECTION_TRACE("outbound_connection:stream:close(0):s="
			+ endpoint().to_string()
			+ ":t=" + this_id().to_str()
			+ ":session=" + session().object_hash().to_str());
		}
	}

void ox_rt1_stream_t::close_from_router() noexcept {
	bool clf = false;
	if(_closef.compare_exchange_strong(clf, true)) {

		close_objects();

		CBL_MPF_TRACELEVEL_EVENT_RT1_CONNECTION_TRACE("outbound_connection:stream:close(1):s="
			+ endpoint().to_string()
			+ ":t=" + this_id().to_str()
			+ ":session=" + session().object_hash().to_str());
		}
	}

void ox_rt1_stream_t::unroute_command_link() noexcept {
	CHECK_NO_EXCEPTION(close_from_router());
	}

bool ox_rt1_stream_t::is_once()const noexcept {
	return false;
	}

source_object_key_t ox_rt1_stream_t::source_key()const noexcept {
	return source_object_key_t::make(endpoint(), session(), this_id());
	}

void ox_rt1_stream_t::register_routing(__error_handler_point_f && rh0 ) {
	if(rdm_registered()) {

		auto rt(_xbase.router());
		if(rt) {
			__error_handler_point_f rh2(crm::utility::bind_once_call({"ox_rt1_stream_t::register_routing"},
				[sptr = shared_from_this()](std::unique_ptr<dcf_exception_t>&& exc, decltype(rh0) && rh0)noexcept {
				if( !exc ){

					bool regf = false;
					if( !sptr->_rtReget.compare_exchange_strong( regf, true ) ){
						FATAL_ERROR_FWD(nullptr);
						}
					}
				else{
					sptr->close_from_router();
					}

				if( sptr->closed() ){
					if( !exc ){
						exc = CREATE_MPF_PTR_EXC_FWD(nullptr);
						}
					}

				rh0( std::move( exc ) );
				}, std::placeholders::_1, std::move( rh0 ) ), ctx(), async_space_t::sys );

			rt->register_messages_slot( msg_rdm(), weak_from_this(), std::move( rh2 ) );
			}
		}
	else {
		FATAL_ERROR_FWD(nullptr);
		}
	}

void ox_rt1_stream_t::unregister_routing()noexcept{
	unregister_routing(unregister_message_track_reason_t::closed_by_self);
	}

void ox_rt1_stream_t::unregister_routing(unregister_message_track_reason_t r)noexcept {

	auto ctx_(ctx());
	auto rt(_xbase.router());
	if(rt) {

		if(rdm_registered() && _rtReget.load(std::memory_order::memory_order_acquire)) {
			CHECK_NO_EXCEPTION(rt->unregister_messages_slot(msg_rdm(), r));
			}
		else {
			auto key = source_object_key_t::make(endpoint(), session(), this_id());
			CHECK_NO_EXCEPTION(rt->unregister_remote_object_slot(endpoint(), remote_id(), key));
			}
		}
	}


std::string ox_rt1_stream_t::make_trace_line()const noexcept {
	return "ox_rt1_stream_t:" + this_id().to_str() + ":" + remote_id().to_str();
	}

const rtl_roadmap_event_t& ox_rt1_stream_t::ev_rdm()const noexcept {
	static_assert(std::is_lvalue_reference_v<decltype(_addrh->ev_rdm())>, __FILE_LINE__);

	CBL_VERIFY(_addrh);
	return _addrh->ev_rdm();
	}

const rtl_roadmap_obj_t& ox_rt1_stream_t::msg_rdm()const noexcept {
	static_assert(std::is_lvalue_reference_v<decltype(_addrh->msg_rdm())>, __FILE_LINE__);

	CBL_VERIFY(_addrh);
	return _addrh->msg_rdm();
	}

const rtl_roadmap_link_t& ox_rt1_stream_t::msg_rdm_link()const noexcept {
	static_assert(std::is_lvalue_reference_v<decltype(_addrh->msg_rdm_link())>, __FILE_LINE__);

	CBL_VERIFY(_addrh);
	return _addrh->msg_rdm_link();
	}

const local_object_id_t& ox_rt1_stream_t::local_id()const noexcept {
	static_assert(std::is_lvalue_reference_v<decltype(_addrh->local_id())>, __FILE_LINE__);

	CBL_VERIFY(_addrh);
	return _addrh->local_id();
	}


const local_command_identity_t& ox_rt1_stream_t::local_object_command_id()const noexcept {
	static_assert(std::is_lvalue_reference_v<decltype(_addrh->local_object_command_id())>, __FILE_LINE__);

	CBL_VERIFY(_addrh);
	return _addrh->local_object_command_id();
	}

const rtl_roadmap_obj_t& ox_rt1_stream_t::this_rdm()const noexcept {
	static_assert(std::is_lvalue_reference_v<decltype(_addrh->this_rdm())>, __FILE_LINE__);

	CBL_VERIFY(_addrh);
	return _addrh->this_rdm();
	}

const identity_descriptor_t& ox_rt1_stream_t::this_id()const noexcept {
	static_assert(std::is_lvalue_reference_v<decltype(_addrh->this_id())>, __FILE_LINE__);

	CBL_VERIFY(_addrh);
	return _addrh->this_id();
	}

const rtl_roadmap_obj_t& ox_rt1_stream_t::remote_rdm()const noexcept {
	static_assert(std::is_lvalue_reference_v<decltype(_addrh->remote_rdm())>, __FILE_LINE__);

	CBL_VERIFY(_addrh);
	return _addrh->remote_rdm();
	}

const identity_descriptor_t& ox_rt1_stream_t::remote_id()const noexcept {
	static_assert(std::is_lvalue_reference_v<decltype(_addrh->remote_id())>, __FILE_LINE__);

	CBL_VERIFY(_addrh);
	return _addrh->remote_id();
	}

const rt0_node_rdm_t& ox_rt1_stream_t::node_id()const noexcept {
	static_assert(std::is_lvalue_reference_v<decltype(_addrh->node_id())>, __FILE_LINE__);

	CBL_VERIFY(_addrh);
	return _addrh->node_id();
	}

const session_description_t& ox_rt1_stream_t::session()const noexcept {
	static_assert(std::is_lvalue_reference_v<decltype(_addrh->session())>, __FILE_LINE__);

	CBL_VERIFY(_addrh);
	return _addrh->session();
	}

ox_rt1_stream_t::~ox_rt1_stream_t() {
	CHECK_NO_EXCEPTION(unbind_reslink());
	CHECK_NO_EXCEPTION(close());
	}


#ifdef CBL_MPF_TEST_EMULATION_REMOTE_HOST_CONNECTION_TIMEOUT
static sndrcv_timeline_t test_make_connection_timepoints_rand(const rt1_endpoint_t & ep) {
	static int64_t max_timeout_secs = CBL_MPF_TEST_ERROR_EMULATOR_REMOTE_HOST_CONNECTION_TIMEOUT_MAX_SECS;
	static int64_t increment = CBL_MPF_TEST_ERROR_EMULATOR_REMOTE_HOST_CONNECTION_TIMEOUT_INCREMENT_SECS;

	static std::atomic<int64_t> seconds = 0;

#if (CBL_MPF_TEST_EMULATION_REMOTE_HOST_CONNECTION_TIMEOUT_MODE == CBL_MPF_TEST_EMULATION_REMOTE_HOST_CONNECTION_TIMEOUT_MODE_UNLIMITED)
	crm::file_logger_t::logging(("RT1_OSTREAM_CONNECT_TO_RT0:" + ep.to_string() + ":unlimited"),
		CBL_MPF_TEST_EMULATION_REMOTE_HOST_CONNECTION_TIMEOUT_TAG, __CBL_FILEW__, __LINE__);

	return sndrcv_timeline_t::make_once_call();
#elif (CBL_MPF_TEST_EMULATION_REMOTE_HOST_CONNECTION_TIMEOUT_MODE == CBL_MPF_TEST_EMULATION_REMOTE_HOST_CONNECTION_TIMEOUT_MODE_TIMEOUTED)

	auto ut = ((seconds += increment) % max_timeout_secs);
	crm::file_logger_t::logging(("RT1_OSTREAM_CONNECT_TO_RT0:" + ep.to_string() + ":timeline:" + std::to_string(ut)),
		CBL_MPF_TEST_EMULATION_REMOTE_HOST_CONNECTION_TIMEOUT_TAG, __CBL_FILEW__, __LINE__);

	return sndrcv_timeline_t::make_once_call_timeouted(std::chrono::seconds(ut + 1));

#elif (CBL_MPF_TEST_EMULATION_REMOTE_HOST_CONNECTION_TIMEOUT_MODE == CBL_MPF_TEST_EMULATION_REMOTE_HOST_CONNECTION_TIMEOUT_MODE_MIX)

	auto ut = ((seconds += increment) % max_timeout_secs);
	if(ut > 0) {

	#ifdef CBL_MPF_TRACELEVEL_EVENT_CONNECTION
		crm::file_logger_t::logging(("RT1_OSTREAM_CONNECT_TO_RT0:" + ep.to_string() + ":timeline:" + std::to_string(ut)),
			CBL_MPF_TEST_EMULATION_REMOTE_HOST_CONNECTION_TIMEOUT_TAG, __CBL_FILEW__, __LINE__);
	#endif

		return sndrcv_timeline_t::make_once_call_timeouted(std::chrono::seconds(ut));
		}
	else {

	#ifdef CBL_MPF_TRACELEVEL_EVENT_CONNECTION
		crm::file_logger_t::logging(("RT1_OSTREAM_CONNECT_TO_RT0:" + ep.to_string() + ":timeline:unlimited:"),
			CBL_MPF_TEST_EMULATION_REMOTE_HOST_CONNECTION_TIMEOUT_TAG, __CBL_FILEW__, __LINE__);
	#endif

		return sndrcv_timeline_t::make_once_call();
		}
#endif
	}
#endif

/* ��������� ��������� ������ */
struct response_translate_t : public i_command_result_t {
	rt0_node_rdm_t rt0rdm;
	};

/* ��������� ������� ������ ����������� � 0 */
struct final_do_t : public i_command_result_t {
	unregister_rt1_stream_t finalize;

#ifdef CBL_MPF_OBJECTS_COUNTER_CHECKER_HOST_COMMAND
	crm::utility::__xobject_counter_logger_t<final_do_t, 1> _xDbgCounter;
#endif

	~final_do_t() {
		CHECK_NO_EXCEPTION(finalize.invoke());
		}

	void reset()noexcept {
		CHECK_NO_EXCEPTION( finalize.reset());
		}

	template<typename THndl>
	final_do_t(typename THndl && h)noexcept
		: finalize(std::forward<THndl>(h)) {}
	};

struct connect_on_handler {
	std::unique_ptr<final_do_t> unreg;
	std::shared_ptr<ox_rt1_stream_t> pstream;
	connection_handler_guard ch;

#ifdef CBL_MPF_OBJECTS_COUNTER_CHECKER_HOST_COMMAND
	crm::utility::__xobject_counter_logger_t<connect_on_handler, 1> _xDbgCounter;
#endif

	~connect_on_handler() {
		CHECK_NO_EXCEPTION(invoke(async_operation_result_t::st_abandoned, nullptr, nullptr, nullptr));
		}

	void operator()(async_operation_result_t rc,
		std::unique_ptr<i_command_from_rt02rt1_t> && response,
		std::unique_ptr<i_command_result_t> && r,
		std::unique_ptr<dcf_exception_t> && n_) {
		
		invoke(rc, std::move(response), std::move(r), std::move(n_));
		}

	connect_on_handler(std::unique_ptr<final_do_t> && unreg_,
		std::shared_ptr<ox_rt1_stream_t> pstream_,
		connection_handler_guard && ch_ )
		: unreg(std::move(unreg_))
		, pstream(std::move(pstream_)) 
		, ch(std::move(ch_)){}

	connect_on_handler(const connect_on_handler&) = delete;
	connect_on_handler& operator=(const connect_on_handler &) = delete;

	connect_on_handler(connect_on_handler && o)
		: unreg(std::move(o.unreg))
		, pstream(std::move(o.pstream))
		, ch(std::move(o.ch))

	#ifdef CBL_MPF_OBJECTS_COUNTER_CHECKER_RT1_PEER
		, _xDbgCounter(std::move(o._xDbgCounter)) 
	#endif
		{}

	void invoke(async_operation_result_t rc,
		std::unique_ptr<i_command_from_rt02rt1_t> && response,
		std::unique_ptr<i_command_result_t> && r,
		std::unique_ptr<dcf_exception_t> && n_) {

		if(auto p = std::atomic_exchange(&pstream, std::shared_ptr<ox_rt1_stream_t>{})) {
			auto xch_ = std::move(ch);
			auto unreg_ = std::move(unreg);

			p->connecton_on_invoke(std::move(xch_), std::move(unreg_), rc, std::move(response), std::move(r), std::move(n_));
			}
		}
	};

void unsubscribe_from_cmdrt(std::unique_ptr<i_command_from_rt02rt1_t> && xcmd_,
	std::shared_ptr<rt1_command_router_t> cmdrt )noexcept {

	auto cmd(command_cast<i_rt0_cmd_outcoming_oppened_t>(std::move(xcmd_)));
	if(cmd && !is_null(cmd->subscriber_for_wait_connection())) {
		if(cmdrt) {
			auto rch = cmdrt->push2channel(cmd_unsubscribe_connection_t(
				cmd->endpoint().to_key(),
				cmd->subscriber_for_wait_connection(),
				cmd->node_id().direction()));

			if( !(rch == addon_push_event_result_t::enum_type::success ||
				rch == addon_push_event_result_t::enum_type::link_unregistered )) {

				FATAL_ERROR_FWD(nullptr);
				}
			}
		}
	}

void ox_rt1_stream_t::connecton_on_invoke(connection_handler_guard && chd,
	std::unique_ptr<i_command_result_t> && unreg,
	async_operation_result_t rc,
	std::unique_ptr<i_command_from_rt02rt1_t> && response,
	std::unique_ptr<i_command_result_t> && r,
	std::unique_ptr<dcf_exception_t> && n_) {

	CBL_MPF_TRACELEVEL_EVENT_RT1_CONNECTION_TRACE("outbound_connection:stream:connect t0 l0 result:s"
		+ endpoint().to_string()
		+ ":t=" + this_id().to_str()
		+ ":session=" + session().object_hash().to_str()
		+ ":result=" + cvt2string(rc)
		+ ":exc=" + (n_ ? cvt(n_->msg()) : ""));

	CHECK_NO_EXCEPTION(close_object_connection_command());
	CHECK_NO_EXCEPTION(unsubscribe_from_cmdrt(std::move(response), _xbase.router()->command_router()));

	auto responseValue = dynamic_cast<const response_translate_t*>(r.get());
	if(!closed() && rc == async_operation_result_t::st_ready && !n_ && responseValue && !is_null(responseValue->rt0rdm)) {

		register_rt0(responseValue->rt0rdm,
			__error_handler_point_f(crm::utility::bind_once_call({ "ox_rt1_stream_t::connecton_on_invoke" }, [wthis = weak_from_this()]
			(std::unique_ptr<dcf_exception_t> && exc, std::unique_ptr<i_command_result_t> && unregf, connection_handler_guard && chd_) {
			if(auto sthis = wthis.lock()) {
				if(!exc) {
					MPF_TEST_EMULATION_IN_CONTEXT_EXPR_COND(sthis->ctx(), 334, is_trait_at_emulation_context(rtl_level_t::l1));

					chd_(async_operation_result_t::st_ready, std::move(sthis));
					if(auto pfinal = dynamic_cast<final_do_t*>(unregf.get())) {
						pfinal->reset();
						}
					}
				else {
					chd_.invoke(std::move(exc));
					}
				}

			}, std::placeholders::_1, std::move(unreg), std::move(chd)), ctx(), async_space_t::sys));
		}
	else {
		chd.invoke(n_ ? std::move(n_) : CREATE_ABANDONED_PTR_EXC_FWD(nullptr));
		}
	}

void ox_rt1_stream_t::connect(connection_handler_guard&& chd) {

	CBL_VERIFY(!_requestRT0);

	CBL_MPF_TRACELEVEL_EVENT_RT1_CONNECTION_TRACE("outbound_connection::stream:connect to l0 initialize:s="
		+ endpoint().to_string()
		+ ":t=" + this_id().to_str()
		+ ":session=" + session().object_hash().to_str());

	MPF_TEST_EMULATION_IN_CONTEXT_EXPR_COND(ctx(), 335, is_trait_at_emulation_context(rtl_level_t::l1));

	if (endpoint().address().empty() || endpoint().port() == 0)
		FATAL_ERROR_FWD(nullptr);

	/* ���������� ���������� */
	connect_on_handler resultHndl(
		/* ������� �������� �������� � �������������� 0
		---------------------------------------------------------*/
		std::make_unique<final_do_t>(unregister_rt1_stream_t(ctx(),
			[rt = _xbase.router(), ep = endpoint(), remoteId = remote_id(), skey = source_key()]{
			if (rt) {
				CHECK_NO_EXCEPTION(rt->unregister_remote_object_slot(ep, remoteId, skey));
				}
			})),

		shared_from_this(),
				std::move(chd));

	/* ��������������� ������ */
	auto responseHndl = [wthis = weak_from_this()]
	(const std::unique_ptr<i_command_from_rt02rt1_t>& response) {

		std::unique_ptr<response_translate_t> ir;
		if (auto sthis = wthis.lock()) {
			if (response) {
				auto stCn(dynamic_cast<const i_rt0_cmd_outcoming_oppened_t*>(response.get()));
				if (stCn) {

					if (stCn->endpoint().address() == sthis->endpoint().address() &&
						stCn->endpoint().port() == sthis->endpoint().port()) {

						ir = std::make_unique<response_translate_t>();
						ir->rt0rdm = stCn->node_id();
						}
					}
				}
			}

		return std::move(ir);
		};

	/* ������� ������� ����������� */
	i_rt0_cmd_reqconnection_t cmd{endpoint(), 
		source_key(), 
		CHECK_PTR(ctx())->policies().timeout_reconnect_attempt() * 10};

	/* ������� */
	auto timeline =
#ifdef CBL_MPF_TEST_EMULATION_REMOTE_HOST_CONNECTION_TIMEOUT
		test_make_connection_timepoints_rand(endpoint())
#else
		CHECK_PTR(ctx())->policies().connection_timepoints()
#endif	
		;

	MPF_TEST_EMULATION_IN_CONTEXT_EXPR_COND(ctx(), 336, is_trait_at_emulation_context(rtl_level_t::l1));

	/* �������� ������� */
	if(timeline.is_timeouted()) {
		
		_requestRT0 = xchannel_cmd_sndrcv_t::invoke_timed(xchannel_cmd_sndrcv_make_tag_msg(("RT1_OSTREAM_CONNECT_TO_RT0(1):" + endpoint().to_string())),
			ctx(),
			CHECK_PTR(_xbase.router())->command_router(),
			std::move(cmd),
			std::move(responseHndl),
			std::move(resultHndl),
			sndrcv_timeline_timeouted_once{ timeline.timeout() } );
		}
	else {
		
		_requestRT0 = xchannel_cmd_sndrcv_t::invoke_binded(xchannel_cmd_sndrcv_make_tag_msg(("RT1_OSTREAM_CONNECT_TO_RT0(2):" + endpoint().to_string())),
			ctx(),
			CHECK_PTR(_xbase.router())->command_router(),
			std::move(cmd),
			std::move(responseHndl),
			std::move(resultHndl) );
		}

	CBL_MPF_TRACELEVEL_EVENT_RT1_CONNECTION_TRACE("outbound_connection:stream:connect to l0 begin:s="
		+ endpoint().to_string()
		+ ":t=" + this_id().to_str()
		+ ":session=" + session().object_hash().to_str());
	}

bool ox_rt1_stream_t::is_integrity_provided()const noexcept {
	return true;
	}

void ox_rt1_stream_t::create(std::weak_ptr<i_rt1_main_router_t> rt,
	std::weak_ptr<distributed_ctx_t> ctx_,
	const identity_descriptor_t & thisId_,
	const identity_descriptor_t & remoteId_,
	const rt1_endpoint_t & ep,
	connection_handler_guard && connectHndl,
	const char * tag) {

	std::shared_ptr<ox_rt1_stream_t> obj(new ox_rt1_stream_t(
		rt, ctx_, thisId_, remoteId_, ep, tag));
	obj->connect(std::move(connectHndl));
	}

std::shared_ptr<distributed_ctx_t> ox_rt1_stream_t::ctx()noexcept {
	return _xbase.ctx();
	}

std::shared_ptr<const distributed_ctx_t> ox_rt1_stream_t::ctx()const noexcept {
	return _xbase.ctx();
	}

ox_rt1_stream_t::ox_rt1_stream_t(std::weak_ptr<i_rt1_main_router_t> rt,
	std::weak_ptr<distributed_ctx_t> ctx_,
	const identity_descriptor_t & thisId_,
	const identity_descriptor_t & remoteId_,
	const rt1_endpoint_t & ep,
	const char * tag)
	: _xbase(rt, ctx_)
	, _addrh(std::make_shared<__address_handler>(ctx_, thisId_, remoteId_, ep, tag)) {

	CBL_MPF_TRACELEVEL_EVENT_RT1_CONNECTION_TRACE("outbound_connection:stream:create:s="
		+ endpoint().to_string()
		+ ":t=" + this_id().to_str()
		+ ":session=" + session().object_hash().to_str());
	}

void ox_rt1_stream_t::rt1_router_hndl(std::unique_ptr<i_command_from_rt02rt1_t> && response)noexcept {

	if(auto exc = command_cast<i_rt0_exception_t>(std::move(response))) {
		CHECK_NO_EXCEPTION(close_from_router());
		}
	else {
		FATAL_ERROR_FWD(nullptr);
		}
	}

void  ox_rt1_stream_t::addressed_exception(const _address_hndl_t & address,
	std::unique_ptr<dcf_exception_t> && e,
	invoke_context_trait ic)noexcept {

	auto is = tail();
	if(is) {
		is->addressed_exception(address, std::move(e), ic);
		}
	}

void ox_rt1_stream_t::rt1_router_hndl(rt0_x_message_unit_t && msg, invoke_context_trait ic) {
	CBL_VERIFY(rdm_registered());

	if(msg.unit().rdm().points == msg_rdm()) {
		CBL_MPF_KEYTRACE_SET(msg, 147);

		CBL_VERIFY(msg.unit().rdm().points.target_id() == this_id()
			&& msg.unit().rdm().points.source_id() == remote_id()
			&& msg.unit().rdm().points.session() == msg_rdm().session()
			&& msg.unit().rdm().points.rt0() == msg_rdm().rt0());

		/* ������ �������� ���������������� ������� ������ 1 */
		if(msg.unit().package().type().type() == iol_types_t::st_disconnection) {

			CBL_MPF_TRACELEVEL_EVENT_RT1_CONNECTION_TRACE("outbound_connection:stream remote closed:s="
				+ endpoint().to_string()
				+ ":t=" + this_id().to_str()
				+ ":session=" + session().object_hash().to_str());

			close(unregister_message_track_reason_t::closed_by_self_from_remoted);
			}
		else if(msg.unit().package().type().type() == iol_types_t::st_RZi_LX_error) {
			CBL_MPF_KEYTRACE_SET(msg, 148);

#pragma message(CBL_MESSAGE_ROADMAP_FOOTMARK(CBL_MESSAGE_ROADMAP,RZi_L0))

			auto destAddr1 = _address_hndl_t::make(msg.unit().package().address().destination_subscriber_id(),
				iol_types_t::st_RZi_LX_error,
				rtl_level_t::l1,
				async_space_t::undef );

			address_descriptor_t destAddr0;
			std::unique_ptr<dcf_exception_t> e;

			exception_RZi_LX_extract(_xbase.ctx(), msg.capture_unit().capture_package(), destAddr0, e);

			CBL_VERIFY(e);
			CBL_VERIFY(destAddr0.destination_subscriber_id() == destAddr1.id());
			CBL_VERIFY(destAddr0.level() == rtl_level_t::l1);

#ifdef CBL_MPF_MESSAGE_TRACEWAY_KEYS_BY_ID
			crm::file_logger_t::logging(trait_at_keytrace_set(msg).to_str() + " rebind to " + destAddr1.id().to_str(), 100203423);
#endif

			addressed_exception(destAddr1, std::move(e), ic);
			}
		else {
			auto is = tail();
			if(is) {
				CBL_MPF_KEYTRACE_SET(msg, 149);

#ifdef CBL_MPF_ENABLE_TIMETRACE
				/* ����� ����������� ������ ����� ������������� 1-�� ������ -
				����� ������� ������ ��� �������� �� �������������� 1-�� ������ */
				msg.timeline_set_rt1_receiver_router();
#endif//CBL_MPF_ENABLE_TIMETRACE

#pragma message( CBL_VAR_NAME_VALUE(CBL_BUILD_VAR_SNDRCV_REVERSE_GUARD, 91))
				is->decode(std::move(msg));
				}
			else {
				CBL_MPF_KEYTRACE_SET(msg, 150);

				std::string log;
				log += "RT1:ox_rt1_stream_t::rt1_router_hndl= null input stack, this object = ";
				log += this_id().to_str() + "|" + remote_id().to_str() + "|" + cvt2string(msg_rdm().table());

				THROW_MPF_EXC_FWD(log);
				}
			}
		}
	else {
		std::string log;
		log += "RT1:ox_rt1_stream_t: bad routed message, object target= ";
		log += msg.unit().rdm().points.target_id().to_str() + "|" + msg.unit().rdm().points.source_id().to_str() + "|" + cvt2string(msg.unit().rdm().points.table());
		log += ", this object = " + this_id().to_str() + "|" + remote_id().to_str() + "|" + cvt2string(msg_rdm().table());
		
		FATAL_ERROR_FWD(std::move(log));
		}
	}


