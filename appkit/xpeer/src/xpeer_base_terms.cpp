#include "../../internal.h"
#include "../i_rt0_xpeer.h"
#include "../i_rt1_xpeer.h"
#include "../i_rt1_router.h"
#include "../i_input_handlers.h"
#include "../../xpeer_invoke/xpeer_invoke_trait.h"


using namespace crm;
using namespace crm::detail;


i_xpeer_t::~i_xpeer_t(){}
i_peer_statevalue_t::~i_peer_statevalue_t(){}
i_rt1_router_targetlink_t::~i_rt1_router_targetlink_t(){}
i_stream_x_irt1_t::~i_stream_x_irt1_t(){}
i_stream_x_ort1_t::~i_stream_x_ort1_t(){}
i_rt1_main_router_t::~i_rt1_main_router_t(){}
i_peer_remote_properties_t::~i_peer_remote_properties_t(){}
i_rt0_peer_handler_t::~i_rt0_peer_handler_t(){}


const std::string connection_stage_descriptions_t::identity_user_options = i_xpeer_t::identity_properties_user_properties;


const std::string i_xpeer_t::identity_properties_id_desc = "id_desc";
const std::string i_xpeer_t::identity_properties_user_properties = "user_prop";



i_x0_xpeer_address_handler::~i_x0_xpeer_address_handler(){}
const std::string i_x0_xpeer_address_handler::null_address;
const int i_x0_xpeer_address_handler::null_port = 0;


const subscriber_node_id_t i_xpeer_rt0_t::subscribe_address_datagram_for_this = subscriber_node_id_t::make_with_key( 0x1 );
const std::string i_xpeer_rt0_t::command_name_identity = "rt0:identity_command";
const std::string i_xpeer_rt0_t::command_name_initialize = "rt0:initialize_command";

const subscriber_node_id_t i_xpeer_rt1_base_t::subscribe_address_datagram_for_this = subscriber_node_id_t::make_with_key( 0x2 );
const std::string i_xpeer_rt1_base_t::command_name_identity = "rt1:identity_command";
const std::string i_xpeer_rt1_base_t::command_name_initialize = "rt1:initialize_command";


static_assert(is_xpeer_trait_z_v<decltype(std::declval<xpeer_base_rt1_t::link_t>())>, __FILE_LINE__);


i_xpeer_rt0_t::input_value_t::input_value_t()noexcept {}

i_xpeer_rt0_t::input_value_t::input_value_t(std::unique_ptr<i_iol_ihandler_t>&& m,
	rtl_roadmap_obj_t&& d,
	std::weak_ptr< i_xpeer_rt0_t> xpeerLnk)noexcept
	: m_(std::move(m))
	, d_(std::move(d))
	, _xpeerWlnk(std::move(xpeerLnk)) {}

const rtl_roadmap_obj_t& i_xpeer_rt0_t::input_value_t::rdm()const noexcept {
	return d_;
	}

const std::unique_ptr<i_iol_ihandler_t>& i_xpeer_rt0_t::input_value_t::message()const& noexcept {
	return m_;
	}

std::unique_ptr<i_iol_ihandler_t> i_xpeer_rt0_t::input_value_t::message() && noexcept {
	return std::move(m_);
	}


local_object_id_t xpeer_push_handler_pin::local_id()const noexcept {
	if(p) {
		return p->local_id();
		}
	else {
		return local_object_id_t::null;
		}
	}

