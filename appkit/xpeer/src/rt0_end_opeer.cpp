#include "../../internal.h"
#include "../../context/context.h"
#include "../rt0_end_opeer.h"
#include "../../xpeer_invoke/xpeer_traits.h"


using namespace crm;
using namespace crm::detail;


static_assert(!is_xpeer_unique_compliant_v<opeer_l0>, __FILE__);
static_assert(is_xpeer_copyable_object_v<opeer_l0>, __FILE__);
static_assert(is_xpeer_trait_z_v<decltype(make_invokable_xpeer(opeer_l0{})) > , __FILE_LINE__);
static_assert(is_xpeer_copyable_object_v<decltype(make_invokable_xpeer(opeer_l0{})) > , __FILE_LINE__);


std::string opeer_l0::trace_line()const noexcept {
	return address() + ":" + std::to_string(port()) + ":" + id().to_str();
	}

opeer_l0::opeer_l0()noexcept {}

/*opeer_l0::opeer_l0(opeer_l0&& o)noexcept
	: _lnk(std::move(o._lnk))

#if defined( CBL_MPF_OBJECTS_COUNTER_CHECKER_RT0_PEER ) && ( CBL_MPF_TRACELEVEL_ST >= CBL_MPF_TRACELEVEL_ST_4)
	, _xDbgCounter(std::move(o._xDbgCounter))
#endif

	{}

opeer_l0& opeer_l0::operator=(opeer_l0&& o) noexcept {
	if(this != std::addressof(o)) {
		_lnk = std::move(o._lnk);

#if defined( CBL_MPF_OBJECTS_COUNTER_CHECKER_RT0_PEER ) && ( CBL_MPF_TRACELEVEL_ST >= CBL_MPF_TRACELEVEL_ST_4)
		_xDbgCounter = std::move(o._xDbgCounter);
#endif
		}

	return (*this);
	}*/

opeer_l0::opeer_l0(ostream_rt0_x_t::ostream_lnk_t&& lnk)noexcept
	: _lnk(std::move(lnk)) {}

void opeer_l0::close()noexcept {}

bool opeer_l0::closed()const noexcept {
	return _lnk.closed();
	}

remote_object_state_t opeer_l0::state()const noexcept {
	return _lnk.state();
	}

opeer_l0::link_type opeer_l0::link()noexcept {
	return _lnk.link();
	}

subscribe_invoker_result opeer_l0::subscribe_hndl_id(async_space_t scx, const _address_hndl_t& hndl, subscribe_options opt,
	subscribe_message_handler&& f) {

	return _lnk.subscribe_hndl_id(scx, hndl, opt, std::move(f));
	}

void opeer_l0::unsubscribe_hndl_id(const _address_hndl_t& hndl)noexcept {
	_lnk.unsubscribe_hndl_id(hndl);
	}

subscribe_result opeer_l0::subscribe_event(async_space_t scx, const _address_event_t& hndl, bool once,
	__event_handler_f&& f) {

	return _lnk.subscribe_event(scx, hndl, once, std::move(f));
	}

void opeer_l0::subscribe_event(events_list&& el) {

	_lnk.subscribe_event(std::move(el));
	}

void opeer_l0::unsubscribe_event(const _address_event_t& hndl) noexcept {
	_lnk.unsubscribe_event(hndl);
	}

opeer_l0::xpeer_desc_t opeer_l0::desc()const noexcept {
	return _lnk.desc();
	}

std::string opeer_l0::address()const noexcept {
	return _lnk.address();
	}

int opeer_l0::port()const noexcept {
	return _lnk.port();
	}

bool opeer_l0::renewable_connection()const noexcept {
	return _lnk.renewable_connection();
	}

rtl_table_t opeer_l0::direction()const noexcept {
	return _lnk.direction();
	}

identity_descriptor_t opeer_l0::this_id()const noexcept {
	return _lnk.this_id();
	}

identity_descriptor_t opeer_l0::remote_id()const noexcept {
	return _lnk.remote_id();
	}

identity_descriptor_t opeer_l0::id()const noexcept {
	return _lnk.id();
	}

local_object_id_t opeer_l0::local_id()const noexcept {
	return _lnk.local_id();
	}

opeer_l0::remote_object_endpoint_t opeer_l0::remote_object_endpoint()const noexcept {
	return _lnk.remote_object_endpoint();
	}

bool opeer_l0::is_opened()const noexcept {
	return _lnk.is_opened();
	}

rtl_level_t opeer_l0::level()const noexcept {
	return rtl_level_t::l0;
	}

std::shared_ptr<i_peer_statevalue_t> opeer_l0::state_value() noexcept {
	return _lnk.state_value();
	}

std::shared_ptr<const i_peer_statevalue_t> opeer_l0::state_value()const noexcept {
	return _lnk.state_value();
	}

void opeer_l0::set_state_value(std::shared_ptr<i_peer_statevalue_t>&& stv)noexcept {
	_lnk.set_state_value(std::move(stv));
	}

std::shared_ptr<const i_peer_remote_properties_t> opeer_l0::remote_properties()const noexcept {
	return _lnk.remote_properties();
	}

connection_key_t opeer_l0::connection_key()const noexcept {
	return connection_key_t{ address(), port() };
	}

std::shared_ptr<distributed_ctx_t> opeer_l0::ctx()const noexcept {
	return _lnk.ctx();
	}

peer_commands_result_t opeer_l0::execute_command(peer_command_t cmd) {
	return _lnk.execute_command(cmd);
	}

static_assert(is_xpeer_trait_z_v< opeer_l0>, __FILE_LINE__);



