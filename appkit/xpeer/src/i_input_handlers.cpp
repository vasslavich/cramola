#include "../../internal.h"
#include "../i_input_handlers.h"


using namespace crm;
using namespace crm::detail;



rtmessate_entry_t::rtmessate_entry_t() {}

rtmessate_entry_t::rtmessate_entry_t(std::weak_ptr<io_rt0_x_channel_t> channel2x,
	std::shared_ptr<i_rt0_peer_handler_t> peer, unregister_f && unsubscriber)
	: channel2x_(std::move(channel2x))
	, xpeer_(std::move(peer))
	, _hunsubscriber(std::move(unsubscriber)) {
	}

rtmessate_entry_t rtmessate_entry_t::make(std::weak_ptr<io_rt0_x_channel_t> channel2x,
	std::shared_ptr<i_rt0_peer_handler_t> peer, unregister_f && unsubscriber) {

	return rtmessate_entry_t(std::move(channel2x), std::move(peer), std::move(unsubscriber));
	}

rtl_rdm_keypair_t::rtl_rdm_keypair_t()noexcept {}

rtl_rdm_keypair_t::rtl_rdm_keypair_t(const  rtl_roadmap_obj_t & rdm,
	std::unique_ptr<rtmessate_entry_t> && channel)noexcept
	: rdm(rdm)
	, channel(std::move(channel)) {}

rtl_rdm_keypair_t::~rtl_rdm_keypair_t() {
	invoke_remote_unsubscribe();
	}

void rtl_rdm_keypair_t::invoke_remote_unsubscribe()noexcept {
	CHECK_NO_EXCEPTION_BEGIN

		if (channel) {
			if (auto h = std::move(channel->_hunsubscriber)) {
				h(rdm, channel->xpeer_);
				}
			}

	CHECK_NO_EXCEPTION_END
	}


