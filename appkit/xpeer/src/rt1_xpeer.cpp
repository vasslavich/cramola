#include "../../internal.h"
#include "../rt1_xpeer.h"
#include "../../context/context.h"
#include "../../queues/qdef.h"
#include "../../streams/istreams_routetbl.h"
#include "../xpeer_push_handler.h"


using namespace crm;
using namespace crm::detail;


static_assert(is_xpeer_push_trait_z_v< xpeer_base_rt1_t>, __FILE_LINE__);
static_assert(is_xpeer_push_trait_z_v< decltype(std::declval<xpeer_base_rt1_t>().get_link())>, __FILE_LINE__);
static_assert(is_xpeer_push_trait_z_v<decltype(make_invokable_xpeer(std::declval<xpeer_base_rt1_t>().get_link()))>, __FILE_LINE__);
static_assert(is_xpeer_trait_z_v<decltype(std::declval<xpeer_base_rt1_t>().get_link())>, __FILE_LINE__);
static_assert(is_xpeer_trait_z_v<decltype(make_invokable_xpeer(std::declval<xpeer_base_rt1_t>().get_link()))>, __FILE_LINE__);


#ifdef CBL_USE_SMART_OBJECTS_COUNTING
crm::log::counter_t xpeer_base_rt1_t::subcounter(const std::string & name) {
	return _THIS_COUNTER.subcounter(name);
	}
#endif

void xpeer_base_rt1_t::ntf_hndl( std::unique_ptr<dcf_exception_t> && ntf )noexcept {
	auto sctx( ctx() );
	if( sctx )
		sctx->exc_hndl( std::move( ntf ) );
	}

void xpeer_base_rt1_t::exc_hndl( const dcf_exception_t & exc )noexcept {
	ntf_hndl( exc.dcpy() );
	}

void xpeer_base_rt1_t::exc_hndl( dcf_exception_t && exc ) noexcept {
	ntf_hndl( exc.dcpy() );
	}

#ifdef CBL_ENABLE_TEST_ERROR_EMULATION_LEVEL_1
std::string xpeer_base_rt1_t::check_erreml_key()const noexcept {
	CBL_VERIFY(!address().empty());

	return address() + ":rt1:" + cvt2string(direction()) + ":" + this_id().to_str();
	}
#endif

void xpeer_base_rt1_t::decode( message_frame_unit_t && obj ) {

#ifdef CBL_ENABLE_TEST_ERROR_EMULATION_LEVEL_1
	bool breakError = false;
	if(direction() == rtl_table_t::in) {
#ifdef CBL_ENABLE_TEST_ERROR_EMULATION_LEVEL_1_IN
		breakError = true;
#endif
		}
	else if(direction() == rtl_table_t::out) {
#ifdef CBL_ENABLE_TEST_ERROR_EMULATION_LEVEL_1_OUT
		breakError = true;
#endif
		}
	else {
		FATAL_ERROR_FWD(nullptr);
		}

	if (breakError){
		if(auto c = ctx()) {
			if(c->test_emulation_connection_corrupt(rtl_level_t::l1)) {
				CBL_MPF_KEYTRACE_SET(obj, 151);

				crm::file_logger_t::logging(std::string("xpeer_base_rt1_t:emulator error:") + cvt2string(direction()),
					CBL_MPF_TRACELEVEL_FAULT_RT1_TAG);

				close(closing_reason_t::test_error);
				}
			}
		}
#endif

	CBL_VERIFY( obj.unit().address().destination_id() == this_id() && 
		obj.unit().address().source_id() == remote_id() );

#pragma message( CBL_VAR_NAME_VALUE(CBL_BUILD_VAR_SNDRCV_REVERSE_GUARD, 101))
	if (_slotGtw) {
		CBL_MPF_KEYTRACE_SET(obj, 152);
		_slotGtw->push(std::move(obj));
		}
	else {
		CBL_MPF_KEYTRACE_SET(obj, 160);
		}
	}

void xpeer_base_rt1_t::addressed_exception(const _address_hndl_t & address,
	std::unique_ptr<dcf_exception_t> && e,
	invoke_context_trait ic)noexcept {
	
	CHECK_PTR(_slotGtw)->addressed_exception(address, std::move(e), ic);
	}

void xpeer_base_rt1_t::trace_send(const i_iol_ohandler_t& msg)const noexcept {

	bool tracef = false;
#ifdef CBL_MPF_TRACELEVEL_EVENT_RT1_SEND_INBOUND
	if (direction() == rtl_table_t::in)
		tracef = true;
#endif

#ifdef CBL_MPF_TRACELEVEL_EVENT_RT1_SEND_OUTBOUND
	if (direction() == rtl_table_t::out)
		tracef = true;
#endif

	bool traceType = false;

#if (CBL_MPF_TRACELEVEL_EVENT_RT1_SEND_LEVEL == CBL_MPF_TRACELEVEL_EVENT_RT1_SEND_ALL )
	traceType = true;
#endif

	if (tracef && traceType) {
		if (msg.level() == level()) {
			crm::file_logger_t::logging(CBL_MPF_PREFIX_RT1("send:" + address_trace() + ":" + std::to_string(msg.type().utype())),
				CBL_MPF_TRACELEVEL_EVENT_RT1_SEND_TAG);
			}
		}
	}

void xpeer_base_rt1_t::trace_recv(const i_iol_ihandler_t& msg)const noexcept {

	bool tracef = false;
#ifdef CBL_MPF_TRACELEVEL_EVENT_RT1_RECV_INBOUND
	if (direction() == rtl_table_t::in)
		tracef = true;
#endif

#ifdef CBL_MPF_TRACELEVEL_EVENT_RT1_RECV_OUTBOUND
	if (direction() == rtl_table_t::out)
		tracef = true;
#endif

	bool traceType = false;

#if (CBL_MPF_TRACELEVEL_EVENT_RT1_RECV_LEVEL == CBL_MPF_TRACELEVEL_EVENT_RT1_RECV_ALL )
	traceType = true;
#endif

	if (tracef && traceType) {
		if (msg.level() == level()) {
			crm::file_logger_t::logging(CBL_MPF_PREFIX_RT1("recv:" + address_trace() + ":" + std::to_string(msg.type().utype())),
				CBL_MPF_TRACELEVEL_EVENT_RT1_INPUT_TAG);
			}
		}
	}

std::function<std::unique_ptr<i_event_t>(event_type_t)> xpeer_base_rt1_t::event_checker() noexcept {
	return [wptr = weak_from_this()](event_type_t t) {

		std::unique_ptr<i_event_t> evr;
		if(auto sptr = wptr.lock()) {

			switch(t) {
				case event_type_t::lower_bound:
					FATAL_ERROR_FWD(nullptr);
					break;

				case event_type_t::st_undefined:
					FATAL_ERROR_FWD(nullptr);
					break;

				case event_type_t::st_disconnection:
					if(sptr->closed())
						evr = std::make_unique<event_close_connection_t>(sptr->desc());

					break;

				case event_type_t::st_connected:
					if(sptr->is_opened() && sptr->state() == remote_object_state_t::impersonated)
						evr = std::make_unique<event_open_connection_t>(sptr->get_link());

					break;

				case event_type_t::upper_bound:
					FATAL_ERROR_FWD(nullptr);
					break;

				default:
					FATAL_ERROR_FWD(nullptr);
					break;
				}
			}

		return evr;
		};
	}

peer_commands_result_t xpeer_base_rt1_t::execute_command( peer_command_t ) {
	return peer_commands_result_t::enum_type::not_supported;
	}

xpeer_base_rt1_t::xpeer_base_rt1_t( const std::weak_ptr<distributed_ctx_t> & ctx_,
	std::shared_ptr<base_stream_x_rt1_t> && source_,
	std::unique_ptr<gtw_rt1_x_t> && gtw,
	rtx_object_closed_t && closeHndl,
	std::shared_ptr<i_peer_statevalue_t> && stval,
	__event_handler_point_f && eh )
	: _ctx( ctx_ )
	, _sourceStream(std::move(source_))
	, _slotGtw(std::move(gtw))
	, _eventClosingHndl( closeHndl )
	, _stateValue( std::move( stval ) )
	, _evhLst( CHECK_PTR( ctx_ )->events_make_line() )
	, _evHndl(std::move(eh))
#ifdef CBL_MPF_OBJECTS_COUNTER_CHECKER_RT1_PEER
	, _xDbgCounter(cvt2string(CHECK_PTR(_sourceStream)->direction()))
#endif
	{
	if(_sourceStream.expired()) {
		THROW_MPF_EXC_FWD(nullptr);
		}

	if(!_eventClosingHndl) {
		THROW_MPF_EXC_FWD(nullptr);
		}

	_addrh = CHECK_PTR(_sourceStream)->address_point();
	if(!_addrh) {
		THROW_MPF_EXC_FWD(nullptr);
		}

	CBL_VERIFY(!is_null(direction()));

	/* ������� ��������� ������� - ������������������ */
	set_state(remote_object_state_t::init_stage);
	}

void xpeer_base_rt1_t::initialize() {
	if (auto s = source_stream()) {
		if (!s->set_tail(shared_from_this())) {
			THROW_EXC_FWD(nullptr);
			}
		}
	else {
		THROW_MPF_EXC_FWD(nullptr);
		}
	}

#ifdef CBL_USE_SEQUENTIAL_CALL_CHECKER
xpeer_base_rt1_t::seqcall_checker_holder xpeer_base_rt1_t::inc_scope_seqcall() {
	return _seqCallCheckerIO.inc_scope();
	}

#endif
void xpeer_base_rt1_t::set_handshake_mode( std::shared_ptr<input_stack_t> && qinput ) {

	subscribe_command_handler();

	/* ��������� ������� ������ �����������
	-----------------------------------------------------------*/
	if( qinput ) {

		auto oldQ( std::atomic_exchange( &_inqAdpt, qinput ) );
		if( oldQ )
			oldQ->clear();

		CHECK_PTR( _slotGtw )->set_input_queue( std::make_shared<inqueue_adpt_rt1_rt0_t>( std::move( qinput ),
			[wthis = weak_from_this(), wctx_ = std::weak_ptr<distributed_ctx_t>(ctx())]( inqueue_adpt_rt1_rt0_t::base_item_t && item ) {

#ifdef CBL_MPF_ENABLE_TIMETRACE
			/* ��������� ������� � ����������� ���������� */
			item.object->set_timeline( detail::hanlder_times_stack_t::stage_t::rt1_receiver );

			/* ������ �������������� ���������� � ��� */
			CHECK_PTR( wctx_ )->log_time( item.object->destination_id(), "message clocks", item.object->timeline().clocks() );
#endif//CBL_MPF_ENABLE_TIMETRACE

			input_stack_t::value_type outVal;

			/* ��������������� ��������� ��������� */
			auto sthis( wthis.lock() );
			if( sthis ) {
#ifdef CBL_USE_SEQUENTIAL_CALL_CHECKER
				auto seqHld = sthis->inc_scope_seqcall();
#endif

#if defined( CBL_MPF_TRACELEVEL_EVENT_RT1_RECV_INBOUND ) || defined (CBL_MPF_TRACELEVEL_EVENT_RT1_RECV_OUTBOUND)
				CHECK_NO_EXCEPTION(sthis->trace_recv(*CHECK_PTR(item.object)));
#endif

				/* ���� ���� - ��������� �������� */
				if( item.m()->type().type() >= iol_types_t::st_user_boundary || item.m()->type().type() < iol_types_t::st_stream_header ) {
					outVal = input_stack_t::value_type(std::move(item).m(), sthis->desc(), wthis);
					}
				else if( item.m()->type().type() >= iol_types_t::st_stream_header && 
					item.m()->type().type() <= iol_types_t::st_stream_disconnection ) {

					if(auto stb = CHECK_PTR(sthis->ctx())->streams_handler()) {
						xpeer_push_handler_pin responseOuth{ std::make_unique<xpeer_push_handler_l1>(sthis->get_link(), sthis->desc()) };

						if(auto istreamMessage = message_cast<i_istream_line_t>(std::move(item).m())) {
							stb->push(sthis->local_id(),
								std::move(istreamMessage),
								std::move(responseOuth),
								sthis->get_event_handler());
							}
						else {
							FATAL_ERROR_FWD(nullptr);
							}
						}
					}
				else {
					FATAL_ERROR_FWD("type:" + std::to_string(item.m()->type().utype()));
					}
				}

			return std::move( outVal );
			} ) );
		}
	else
		THROW_MPF_EXC_FWD(nullptr);
	}

void xpeer_base_rt1_t::handshake_end( bool /*result*/ ) { }

void xpeer_base_rt1_t::on_handshake_success() {}

void xpeer_base_rt1_t::on_extern_completed(std::function<void()> && externHndl) {
	on_handshake_success();
	externHndl();
	}


void xpeer_base_rt1_t::invoke_event(event_type_t t)noexcept {
	if(t == event_type_t::st_connected) {
		event_invoke(event_type_t::st_connected, std::make_unique<event_open_connection_t>(get_link()));
		}
	else {
		FATAL_ERROR_FWD(nullptr);
		}
	}

void xpeer_base_rt1_t::io_rw_listen() {

	CBL_VERIFY(!is_null(this_id()));
	CBL_VERIFY(!is_null(remote_id()));

	/* ������ ������� �������������
	---------------------------------------------------------*/
	CHECK_PTR(_slotGtw)->start({
		[wptr = weak_from_this()] (const address_descriptor_t& addr, std::unique_ptr<dcf_exception_t>&& exc_)noexcept{
		if (auto sptr = wptr.lock()) {

			CBL_NO_EXCEPTION_BEGIN

				onotification_t exc{};
			exc.set_ntf(std::move(exc_));

			exc.set_echo_params(addr);
			CBL_VERIFY(!is_sendrecv_response(exc));

			sptr->push(std::move(exc));

			CBL_NO_EXCEPTION_END(sptr->ctx());
			}
		},

		[wptr = weak_from_this()](std::unique_ptr<ifunctor_package_t>&& m) {
			if (auto sptr = wptr.lock()) {
				sptr->reverse_functor_message(std::move(m));
				}
			} });
	}

void xpeer_base_rt1_t::close_base_stream() noexcept {
	auto so( source_stream() );
	if( so ) {
		CHECK_NO_EXCEPTION( so->close() );
		}
	}

void xpeer_base_rt1_t::close( closing_reason_t reason )noexcept {

	bool tmp = false;
	if (_stop.compare_exchange_strong(tmp, true)) {
		if (auto c = ctx()) {

			CHECK_NO_EXCEPTION_BEGIN

			/* ������ ������� - ���������� ������� */
			set_state(remote_object_state_t::closed);

			auto pSTV(std::atomic_exchange(&_stateValue, {}));
			if (pSTV) {
				pSTV->close();
				}

			/* ������� ����� ������� ������ */
			close_base_stream();

			/* ������� ������ �������������� */
			if (_slotGtw) {
				_slotGtw->close();
				}

			if (auto q = std::atomic_exchange(&_inqAdpt, {})) {
				q->clear();
				}

			/* ����������� �������� ���� */
			if (_eventClosingHndl) {
				c->launch_async(async_space_t::ext, __FILE_LINE__, std::move(_eventClosingHndl), rdm_local_identity(), reason, std::move(pSTV));
				}

			/* ������� ����������� */
			event_invoke(event_type_t::st_disconnection,
				std::make_unique<event_close_connection_t>(desc()));

			/* ������� ����� ������������ */
			_evhLst.close();

			CHECK_NO_EXCEPTION_END
			}
		}
	}

void xpeer_base_rt1_t::_dctr()noexcept {
	CHECK_NO_EXCEPTION( close(closing_reason_t::destroy) );

#ifdef CBL_MPF_OBJECTS_COUNTER_CHECKER_RT1_PEER
	CHECK_NO_EXCEPTION( _xDbgCounter.dec() );
#endif
	}

xpeer_base_rt1_t:: ~xpeer_base_rt1_t() {
	CHECK_NO_EXCEPTION( _dctr() );
	CBL_MPF_TRACELEVEL_EVENT_RT1_CONNECTION_TRACE("destroy:" + session().object_hash().to_str());
	}

void xpeer_base_rt1_t::close()noexcept {
	close( closing_reason_t::extern_force );
	}

void xpeer_base_rt1_t::write( outbound_message_unit_t && rt1Obj ) {
	CBL_MPF_KEYTRACE_SET(rt1Obj, 471);
	CBL_VERIFY_SNDRCV_KEY_SPIN_TAG(rt1Obj);
	CBL_VERIFY((is_sendrecv_cycle(rt1Obj) && CBL_IS_SNDRCV_KEY_SPIN_TAGGED_ON(rt1Obj)) || 
		(!is_sendrecv_cycle(rt1Obj) && CBL_IS_SNDRCV_KEY_SPIN_TAGGED_OFF(rt1Obj)));

	MPF_TEST_EMULATION_IN_CONTEXT_COND_TRCX(_ctx.lock(), 454,
		is_trait_at_emulation_context(rt1Obj),
		trait_at_keytrace_set(rt1Obj));

	auto so( source_stream() );
	if( !so || so->closed() ) {
		close( closing_reason_t::break_connection );
		}
	else {

#pragma message( CBL_VAR_NAME_VALUE(CBL_BUILD_VAR_MESSAGE_ROADMAP_MARKER_OUTBOUND, 4))
		so->push_to_x( std::move( rt1Obj ) );
		}
	}

void xpeer_base_rt1_t::flush() {}

void xpeer_base_rt1_t::__unsubscribe_hndl_id( const _address_hndl_t & id )noexcept {
	if(_slotGtw)
		_slotGtw->unsubscribe_hndl_id(id);
	}

void xpeer_base_rt1_t::unsubscribe_hndl_id( const _address_hndl_t & id )noexcept {
	__unsubscribe_hndl_id( id );
	}

void xpeer_base_rt1_t::subscribe_command_handler() {

	/* ����������� ����������� ��������� ��������� ������
	----------------------------------------------------------------------------*/
	auto hndl = [wthis = weak_from_this()](std::unique_ptr<dcf_exception_t> && exc, std::unique_ptr<i_iol_ihandler_t> && item, invoke_context_trait ) {

		auto sthis( wthis.lock() );
		if( sthis ) {
			if( !exc ) {
				if(item)
					sthis->command_handler(std::move(item));
				else {
					sthis->close();
					}
				}
			else {
				sthis->close( closing_reason_t::break_connection );
				}
			}
		};

	if(subscribe_invoker_result::enum_type::success != __subscribe_hndl_id(async_space_t::sys,
		_address_hndl_t::make(subscribe_address_datagram_for_this, iol_types_t::st_sendrecv_datagram, level(), async_space_t::undef),
		subscribe_options(assign_options::insert_if_not_exists, false),
		subscribe_message_handler("xpeer_base_rt1_t::subscribe_command_handler", std::move(hndl), inovked_as::sync ))) {
		
		THROW_MPF_EXC_FWD(nullptr);
		}
	}

void xpeer_base_rt1_t::command_handler( std::unique_ptr<i_iol_ihandler_t> && iCmd) {
	if(iCmd) {
#ifdef CBL_USE_SEQUENTIAL_CALL_CHECKER
		auto seqHld = inc_scope_seqcall();
#endif

		bool rf = false;
		if(iCmd->type().type() == iol_types_t::st_sendrecv_datagram) {
			if(auto iDgt = message_cast<isendrecv_datagram_t>(std::move(iCmd))) {
				if(iDgt->key() == command_name_identity) {
					command_handler_dtg_identity(std::move(iDgt));
					rf = true;
					}
				else if(iDgt->key() == command_name_initialize) {
					command_handler_dgt_initialize(std::move(iDgt));
					rf = true;
					}
				}
			}

		if(!rf) {
			FATAL_ERROR_FWD(nullptr);
			}
		}
	else {
		THROW_MPF_EXC_FWD(nullptr);
		}
	}

void xpeer_base_rt1_t::unsubscribe_event( const _address_event_t & id ) noexcept {
	_evhLst.unsubscribe( id );
	}

void xpeer_base_rt1_t::event_invoke(event_type_t type, std::unique_ptr<event_args_t> && args)noexcept {
	_evhLst.invoke( type, std::move( args ) );
	}

void xpeer_base_rt1_t::event_invoke( std::unique_ptr<i_event_t> && ev )noexcept {

	auto h( get_event_handler() );
	if( h ) {
		if(auto c = ctx()) {

			/* ����� ����������� ������� ���������� */
			c->launch_async_exthndl( __FILE_LINE__, std::move( h ), std::move( ev ) );
			}
		}
	}

__event_handler_point_f xpeer_base_rt1_t::get_event_handler()const noexcept {
	return _evHndl;
	}

subscribe_invoker_result xpeer_base_rt1_t::subscribe_hndl_id(async_space_t scx, const _address_hndl_t & id, subscribe_options opt,
	subscribe_message_handler && h) {

	return subscribe_hndl_id_tv(scx, id, opt, std::move(h));
	}

subscribe_invoker_result xpeer_base_rt1_t::subscribe_hndl_id_tv(async_space_t scx, const _address_hndl_t & id, subscribe_options opt,
	subscribe_message_handler && h) {

	if(id.id().xoffer() <= distributed_ctx_t::subscribe_address_user_range())
		THROW_MPF_EXC_FWD(nullptr);

	return __subscribe_hndl_id(scx, id, opt, std::move(h));
	}

subscribe_invoker_result xpeer_base_rt1_t::__subscribe_hndl_id(async_space_t scx, const _address_hndl_t & id, subscribe_options opt,
	subscribe_message_handler && h) {

	if(_slotGtw) {
		return _slotGtw->subscribe_hndl_id(scx, id, opt, std::move(h));
		}
	else {
		THROW_MPF_EXC_FWD(nullptr);
		}
	}

void xpeer_base_rt1_t::reverse_functor_message(std::unique_ptr<ifunctor_package_t>&& m) {
	if (m) {
#ifdef CBL_USE_SEQUENTIAL_CALL_CHECKER
		auto seqHld = inc_scope_seqcall();
#endif

		if (auto c = ctx()) {
			MPF_TEST_EMULATION_IN_CONTEXT_L_COND_TRCX(c, level(), 555, is_trait_at_emulation_context(*m), trait_at_keytrace_set(*m));

			ostrob_message_envelop_t<functor_result> rm{ c->invoke_handler(std::move(*m).to_handler()) };
			rm.capture_response_tail(*m);

			MPF_TEST_EMULATION_IN_CONTEXT_L_COND_TRCX(c, level(), 556, is_trait_at_emulation_context(*m), trait_at_keytrace_set(*m));

			push(std::move(rm));
			}
		}
	else {
		THROW_EXC_FWD(nullptr);
		}
	}
