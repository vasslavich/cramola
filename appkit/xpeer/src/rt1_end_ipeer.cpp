#include "../../context/context.h"
#include "../rt1_end_ipeer.h"
#include "../../xpeer_invoke/xpeer_traits.h"
#include "../../xpeer_invoke/xpeer_invoke_trait.h"


using namespace crm;
using namespace crm::detail;


static_assert(!is_xpeer_unique_compliant_v<ipeer_t>, __FILE__);
static_assert(is_xpeer_copyable_object_v<ipeer_t>, __FILE__);
static_assert(is_xpeer_trait_z_v<ipeer_t>, __FILE_LINE__);
static_assert(std::is_same_v < ipeer_t, std::decay_t<decltype(make_invokable_xpeer( ipeer_t{} ))>> , __FILE__);
static_assert(is_xpeer_trait_z_v<decltype(make_invokable_xpeer(ipeer_t{})) >, __FILE_LINE__);


ipeer_t::ipeer_t()noexcept{}

ipeer_t::ipeer_t(const ipeer_t &o)noexcept
	: _lnk(o._lnk){}

ipeer_t& ipeer_t::operator=(const ipeer_t &o) noexcept{
	if(this != std::addressof(o)) {
		_lnk = o._lnk;
		}

	return (*this);
	}

ipeer_t::ipeer_t(ipeer_t &&o)noexcept
	: _lnk(std::move(o._lnk))
	
#if defined( CBL_MPF_OBJECTS_COUNTER_CHECKER_RT1_PEER ) && ( CBL_MPF_TRACELEVEL_ST >= CBL_MPF_TRACELEVEL_ST_4)
	, _xDbgCounter(std::move(o._xDbgCounter))
#endif
	
	{}

ipeer_t& ipeer_t::operator=(ipeer_t &&o)noexcept {
	if(this != std::addressof(o)) {
		_lnk = std::move(o._lnk);

#if defined( CBL_MPF_OBJECTS_COUNTER_CHECKER_RT1_PEER ) && ( CBL_MPF_TRACELEVEL_ST >= CBL_MPF_TRACELEVEL_ST_4)
		_xDbgCounter = std::move(o._xDbgCounter);
#endif
		}

	return (*this);
	}

ipeer_t::ipeer_t(xpeer_base_rt1_t::link_t && lnk)noexcept
	: _lnk(std::move(lnk)) {}

void ipeer_t::close()noexcept {}

bool ipeer_t::closed()const noexcept {
	return _lnk.closed();
	}

remote_object_state_t ipeer_t::state()const noexcept {
	return _lnk.state();
	}

subscribe_invoker_result ipeer_t::subscribe_hndl_id(async_space_t scx, const _address_hndl_t &hndl, subscribe_options opt,
	subscribe_message_handler && f) {

	return _lnk.subscribe_hndl_id(scx, hndl, opt, std::move(f));
	}

void ipeer_t::unsubscribe_hndl_id(const _address_hndl_t &hndl)noexcept {
	_lnk.unsubscribe_hndl_id(hndl);
	}

void ipeer_t::subscribe_event(events_list && el) {

	_lnk.subscribe_event(std::move(el));
	}

subscribe_result ipeer_t::subscribe_event(async_space_t scx, const _address_event_t &hndl, bool once,
	__event_handler_f && f) {

	return _lnk.subscribe_event(scx, hndl, once, std::move(f));
	}

void ipeer_t::unsubscribe_event(const _address_event_t &hndl)noexcept {
	_lnk.unsubscribe_event(hndl);
	}

const ipeer_t::xpeer_desc_t& ipeer_t::desc()const noexcept {
	static_assert(std::is_lvalue_reference_v<decltype(_lnk.desc())>, __CBL_FILEW__);
	return _lnk.desc();
	}

std::string ipeer_t::address()const noexcept {
	return _lnk.address();
	}

int ipeer_t::port()const noexcept {
	return _lnk.port();
	}

bool ipeer_t::renewable_connection()const noexcept {
	return _lnk.renewable_connection();
	}

rtl_table_t ipeer_t::direction()const noexcept {
	return _lnk.direction();
	}

const local_object_id_t& ipeer_t::local_id()const noexcept {
	static_assert(std::is_lvalue_reference_v<decltype(_lnk.local_id())>, __CBL_FILEW__);
	return _lnk.local_id();
	}

const identity_descriptor_t& ipeer_t::this_id()const noexcept {
	static_assert(std::is_lvalue_reference_v<decltype(_lnk.this_id())>, __CBL_FILEW__);
	return _lnk.this_id();
	}

const identity_descriptor_t& ipeer_t::remote_id()const noexcept {
	static_assert(std::is_lvalue_reference_v<decltype(_lnk.remote_id())>, __CBL_FILEW__);
	return _lnk.remote_id();
	}

const identity_descriptor_t& ipeer_t::id()const noexcept {
	static_assert(std::is_lvalue_reference_v<decltype(_lnk.id())>, __CBL_FILEW__);
	return _lnk.id();
	}

ipeer_t::remote_object_endpoint_t ipeer_t::remote_object_endpoint()const noexcept {
	return _lnk.remote_object_endpoint();
	}

bool ipeer_t::is_opened()const noexcept {
	return _lnk.is_opened();
	}

rtl_level_t ipeer_t::level()const noexcept {
	return rtl_level_t::l1;
	}

std::shared_ptr<i_peer_statevalue_t> ipeer_t::state_value() noexcept {
	return _lnk.state_value();
	}

std::shared_ptr<const i_peer_statevalue_t> ipeer_t::state_value()const noexcept {
	return _lnk.state_value();
	}

void ipeer_t::set_state_value(std::shared_ptr<i_peer_statevalue_t> && stv) noexcept {
	_lnk.set_state_value(std::move(stv));
	}

std::shared_ptr<const i_peer_remote_properties_t> ipeer_t::remote_properties()const noexcept {
	return _lnk.remote_properties();
	}

connection_key_t ipeer_t::connection_key()const noexcept {
	return connection_key_t{ address(), port() };
	}

std::shared_ptr<distributed_ctx_t> ipeer_t::ctx()const noexcept {
	return _lnk.ctx();
	}

peer_commands_result_t ipeer_t::execute_command(peer_command_t cmd) {
	return _lnk.execute_command(cmd);
	}

static_assert(is_xpeer_trait_z_v< ipeer_t>, __FILE_LINE__);

