#include "../../internal.h"
#include "../rt0_opeer.h"
#include "../../context/context.h"
#include "../../logging/keycounter.h"
#include "../../xpeer_invoke/xpeer_asendrecv.h"


using namespace crm;
using namespace crm::detail;


#if (CRM_SESSION_TAG_MODE == CRM_SESSION_TAG_ON)

#if (CBL_SESSION_MAKE_TYPE == CBL_SESSION_MAKE_SHORT_PREFIX)
static std::string create_stag(std::weak_ptr<distributed_ctx_t> ) {
	static std::atomic<std::uintmax_t> counter{ 0 };
	return std::to_string(++counter);
	}

#elif(CBL_SESSION_MAKE_TYPE == CBL_SESSION_MAKE_LONG_PREFIX)
static std::string create_stag(std::weak_ptr<distributed_ctx_t> ctx) {
	auto ctx_(ctx.lock());
	if (!ctx_)
		THROW_MPF_EXC_FWD(nullptr);

	return std::string("xpeer_out_rt0_t:" + ctx_->curr_time_str() + ":" + global_object_identity_t::rand().to_str());
	}
#endif

#elif (CRM_SESSION_TAG_MODE == CRM_SESSION_TAG_OFF)
static std::string create_stag(std::weak_ptr<distributed_ctx_t>) {
	return {};
	}
#else
#error wrong session tag mode
#endif


void xpeer_out_rt0_t::_connect(std::weak_ptr<__rtx_out_connection_ctx_timered_t> connCtx,
	ctr_0_input_queue_t && destStream) {

	begin_connect(CHECK_PTR(connCtx)->endpoint(),
		/* ���������� ������� ����������� �� ��������� ������ */
		[sptr = std::dynamic_pointer_cast<self_t>(shared_from_this()), connCtx, destStream](std::unique_ptr<dcf_exception_t> && exc) {

		if(auto cntx = connCtx.lock()) {
			ctr_0_input_queue_t dstream(destStream);
			try {
				if(!exc) {
					if(!sptr->closed())
						sptr->_on_connect(connCtx, std::move(dstream));
					}
				else {
					if(!is_terminate_operation_exception(exc)) {
						sptr->_connect(connCtx, std::move(dstream));
						}
					else {
						CHECK_NO_EXCEPTION(cntx->invoke_error(std::move(exc)));
						}
					}
				}
			catch(const dcf_exception_t & exc) {
				sptr->exc_hndl(exc);
				}
			catch(...) {
				sptr->exc_hndl(CREATE_MPF_EXC_FWD(nullptr));
				}
			}
		});
	}

void xpeer_out_rt0_t::_on_connect(std::weak_ptr<__rtx_out_connection_ctx_timered_t> connCtx,
	ctr_0_input_queue_t && destStream) {

	/* ������� � ��������� ��������� ��������� �������� ������������� */
	set_state(remote_object_state_t::connect_stage_ready);

	if(auto cntx = connCtx.lock()) {

		CBL_MPF_TRACELEVEL_EVENT_RT0_CONNECTION_TRACE("outbound connect response:" + cntx->endpoint()->to_string());

		/* �������� ��������� ������ */
		set_session_output(this_id(),
			session_description_t(local_id(), create_stag(CHECK_PTR(ctx()))),
			*(CHECK_PTR(cntx->endpoint())));

		std::shared_ptr<input_stack_t> inputStack;
		if(destStream)
			inputStack = destStream();

		set_handshake_mode(inputStack);

		/* ������ ����� ������������� ����������� �����/������ */
		io_rw_listen();

		/* ������� � ����� �������� ���������� �������� ������ ������������������ ������� */
		start_handshake(std::move(connCtx));

		MPF_TEST_EMULATION_IN_CONTEXT_EXPR_COND(ctx(), 210, is_trait_at_emulation_context(level()));
		}
	else {
		THROW_EXC_FWD(nullptr);
		}
	}

void xpeer_out_rt0_t::handshake_exception_handler(std::weak_ptr<__rtx_out_connection_ctx_timered_t> connCtx,
	std::unique_ptr<dcf_exception_t> && ntf) noexcept{

	close();

	auto basefExceptionHndl = [connCtx](std::unique_ptr<dcf_exception_t> && ntf_) {
		if(auto cntx = connCtx.lock()) {
			CHECK_NO_EXCEPTION(cntx->invoke_error(std::move(ntf_)));
			}
		};

	if(_remoteExceptionChecker) {

		auto extf = _remoteExceptionChecker;
		auto nextf = [extf, connCtx, basefExceptionHndl](std::unique_ptr<dcf_exception_t> && ntf) {
			try {
				bool extr = false;
				CHECK_NO_EXCEPTION(extr = extf(connection_stage_t::impersonate, ntf));

				if(!extr) {
					if(auto cntx = connCtx.lock()) {
						CHECK_NO_EXCEPTION(cntx->invoke_error(std::move(ntf), reconnect_setup_t::no));
						}
					}
				else {
					basefExceptionHndl(nullptr);
					}
				}
			catch(const dcf_exception_t & exc0) {
				basefExceptionHndl(exc0.dcpy());
				}
			catch(const std::exception & exc1) {
				basefExceptionHndl(CREATE_MPF_PTR_EXC_FWD(exc1));
				}
			catch(...) {
				basefExceptionHndl(nullptr);
				}
			};

		if(auto c = ctx()) {
			CHECK_NO_EXCEPTION(c->launch_async_exthndl(__FILE_LINE__, std::move(nextf), std::move(ntf)));
			}
		}
	else {
		basefExceptionHndl(std::move(ntf));
		}
	}

void xpeer_out_rt0_t::start_handshake( std::weak_ptr<__rtx_out_connection_ctx_timered_t> connCtx_ ) {

	set_state(remote_object_state_t::identity_stage);

	/* ����������� ������� */
	auto reqCtr = [connCtx_] {

		if(auto cntx = connCtx_.lock()) {

			/* ��������� ����������������� ���������� ������� ������������� */
			auto idVal = cntx->ctr_local_id();
			if(is_null(idVal.identity().id_value)) {
				THROW_MPF_EXC_FWD(L"bad identity constructor");
				}

			/* ��������� � ������������������ ������� */
			osendrecv_datagram_t msg{};
			msg.set_key(command_name_identity);
			msg.value().add_value("", idVal);
			msg.set_destination_subscriber_id(subscribe_address_datagram_for_this);

			return msg;
			}
		else {
			THROW_EXC_FWD(nullptr);
			}
		};

	/* ���������� ������ */
	auto resHndl = [wptr = weak_from_this(), connCtx_]( message_handler && m )noexcept {
		if(auto cntx = connCtx_.lock()) {
			if(auto sptr = std::dynamic_pointer_cast<self_t>(wptr.lock())) {
				if (!sptr->closed()) {

					auto exceptionHandler = [&sptr, connCtx_](std::unique_ptr<dcf_exception_t>&& exc) {
						CHECK_NO_EXCEPTION(sptr->handshake_exception_handler(connCtx_, std::move(exc)));
						};

					try {
#ifdef CBL_USE_SEQUENTIAL_CALL_CHECKER
						auto seqHld = sptr->inc_scope_seqcall();
#endif

						CBL_MPF_TRACELEVEL_EVENT_RT0_CONNECTION_TRACE("outbound handshake response:"
							+ cntx->endpoint()->to_string()
							+ ":" + cvt2string(reuslt));

						if(m.has_result()){
							auto iDtg = std::move(m).as<isendrecv_datagram_t>();
							if (iDtg.key() != command_name_identity) {
								CHECK_NO_EXCEPTION(cntx->invoke_error(CREATE_MPF_PTR_EXC_FWD(nullptr)));
								}

							CBL_VERIFY(!is_null(iDtg.address().destination_id()) && iDtg.address().destination_id() == sptr->this_id());

							auto cvRemote(iDtg.value().get_value<remote_object_connection_value_t>("", 0));
							std::unique_ptr<i_peer_remote_properties_t> remoteProp;

							if (!cntx->checker_remote_id(cvRemote.identity(), remoteProp)) {
								CHECK_NO_EXCEPTION(cntx->invoke_error(CREATE_MPF_PTR_EXC_FWD(nullptr)));
								}

							/* ��������� ��������� �������������
							-----------------------------------------------------------------------*/
							sptr->set_id(cvRemote.identity().id_value);

							const auto pPropId = cvRemote.identity().properties.find_by_key(identity_properties_id_desc);
							if (pPropId) {
								const auto idValue = base_peer_identity_properties_t::dsrlz_(pPropId->data);
								}

							/* �������� ��������� �������
							----------------------------------------------*/
							if (remoteProp)
								sptr->set_remote_properties(std::move(remoteProp));

							MPF_TEST_EMULATION_IN_CONTEXT_COND_TRCX(sptr->ctx(), 211,
								is_trait_at_emulation_context(iDtg),
								trait_at_keytrace_set(iDtg));

							sptr->set_state(remote_object_state_t::identity_stage_ready);

							iDtg.reset_invoke_exception_guard().reset();
							}
						else if (m.has_exception()) {
							exceptionHandler(std::move(m).exception());
							}
						else {
							CHECK_NO_EXCEPTION(cntx->invoke_error(m.state(), nullptr));
							}
						}
					catch (const dcf_exception_t & exc0) {
						exceptionHandler(exc0.dcpy());
						}
					catch (const std::exception & exc1) {
						exceptionHandler(std::make_unique<dcf_exception_t>(exc1));
						}
					catch (...) {
						exceptionHandler(CREATE_PTR_EXC_FWD(nullptr));
						}
					}
				}
			else {
				CHECK_NO_EXCEPTION(cntx->invoke_error(async_operation_result_t::st_abandoned, nullptr));
				}
			}
		};

	auto ctx_(ctx());
	if(ctx_) {
		static_assert(detail::is_sendrecv_request_invokable_v<decltype(reqCtr), decltype(get_link())>, __FILE_LINE__);

		make_async_sndrcv_once_sys(ctx_,
			get_link(),
			std::move(reqCtr),
			std::move(resHndl),
			(__FILE_LINE__ + std::string(":") + CHECK_PTR(connCtx_)->endpoint()->to_string() + ":" + this_id().to_str()),
			CHECK_PTR(connCtx_)->timeline(),
			launch_as::async,
			inovked_as::sync );

		CBL_MPF_TRACELEVEL_EVENT_RT0_CONNECTION_TRACE("outbound handshake begin:" + CHECK_PTR(connCtx_)->endpoint()->to_string());
		}
	else {
		THROW_MPF_EXC_FWD(nullptr);
		}
	}

/*! ��������� �������� ������������� ������ ������� */
void xpeer_out_rt0_t::on_handshake_success() {

	/* ��������� �������� ���������� */
	handshake_end( true );

	/* �������� ������ ������� */
	set_state(remote_object_state_t::impersonated);

	MPF_TEST_EMULATION_IN_CONTEXT_EXPR_COND(ctx(), 212, is_trait_at_emulation_context(level()));
	CBL_MPF_TRACELEVEL_EVENT_RT0_CONNECTION_TRACE("outbound handshake ok:" + connection_key().to_string());
	}

/* ������������� ���������� �������-����������� */
xpeer_out_rt0_t::xpeer_out_rt0_t(const std::weak_ptr<distributed_ctx_t> & ctx_,
	std::unique_ptr<i_xpeer_source_t> && source_,
	std::string address,
	int port,
	std::unique_ptr<iol_gtw_t> && gtw_,
	rtx_object_closed_t && closeHndl,
	exceptions_suppress_checker_t && remoteExcCheck,
	std::shared_ptr<i_peer_statevalue_t> && stval,
	const identity_descriptor_t &  thisId,
	__event_handler_point_f&& eh)
	: base_t(ctx_,
		std::move(source_),
		std::move(address),
		port,
		std::move(gtw_),
		std::move(closeHndl),
		std::move(stval),
		thisId,
		peer_direction_t::out,
		std::move(eh))
	, _remoteExceptionChecker(std::move(remoteExcCheck)){

	auto ctx0_(ctx());
	if(!ctx0_)
		THROW_MPF_EXC_FWD(nullptr);
	}

void xpeer_out_rt0_t::destroy()noexcept {
	if(auto st = initialize_state_release()) {
		CHECK_NO_EXCEPTION(st.reset());
		}

	CHECK_NO_EXCEPTION(base_t::destroy());
	}

xpeer_out_rt0_t::~xpeer_out_rt0_t() {
	CHECK_NO_EXCEPTION(destroy());
	}

void xpeer_out_rt0_t::connection_result_handler(async_operation_result_t result,
	std::unique_ptr<dcf_exception_t> && exc,
	reconnect_setup_t rc,
	std::unique_ptr<__outbound_connection_result_t<link_t>> && pHndl)noexcept {

	auto exchndl = [this, &pHndl, rc](std::unique_ptr<dcf_exception_t> && e)noexcept {
		if(pHndl) {
			CHECK_NO_EXCEPTION(pHndl->refuse(std::move(e), rc));
			}

		CHECK_NO_EXCEPTION(close());
		};

	try {
		if(result == async_operation_result_t::st_ready) {
			on_extern_completed([&] {
				if(pHndl) {
					MPF_TEST_EMULATION_IN_CONTEXT_EXPR_COND(ctx(), 213, is_trait_at_emulation_context(level()));

					pHndl->ready(get_link());
					invoke_event(event_type_t::st_connected);
					}
				});
			}
		else if(exc) {
			if(pHndl) {
				pHndl->refuse(std::move(exc), rc);
				}
			}
		else {
			if(pHndl) {
				pHndl->refuse(CREATE_MPF_PTR_EXC_FWD(nullptr), rc);
				}
			}
		}
	catch(const dcf_exception_t & e0) {
		exchndl(e0.dcpy());
		}
	catch(const std::exception & e1) {
		exchndl(CREATE_PTR_EXC_FWD(e1));
		}
	catch(...) {
		exchndl(CREATE_MPF_PTR_EXC_FWD(nullptr));
		}
	}

xpeer_out_rt0_t::indentity_tail xpeer_out_rt0_t::make_identity_handler(
	std::weak_ptr<self_t> wptr,
	connection_result_t && connectionHndl)noexcept {

	if (auto sptr = wptr.lock()) {
		using connection_handler_kp = __outbound_connection_result_t<link_t>;

		CBL_MPF_IF_NOT_ERROR_EMULATION(CBL_VERIFY(sptr.use_count() == 2));

		auto h = [wptr](async_operation_result_t result,
			std::unique_ptr<dcf_exception_t> && exc,
			reconnect_setup_t rc,
			std::unique_ptr<__outbound_connection_result_t<link_t>> && pHndl)noexcept {

			if (auto sptr = wptr.lock()) {
				CHECK_NO_EXCEPTION(sptr->connection_result_handler(result, std::move(exc), rc, std::move(pHndl)));

				if (result != async_operation_result_t::st_ready || exc) {
					CHECK_NO_EXCEPTION(sptr->close());
					}
				}
			};

		/* ���������� ���������� ���������� ��������� �������� ������������� */
		return indentity_tail(std::move(h),
			std::make_unique<connection_handler_kp>(sptr->ctx(), std::move(connectionHndl)
#ifdef CBL_MPF_TRACELEVEL_EVENT_CONNECTION
				, cvt(sptr->remote_object_endpoint().to_string())
#else
#endif

				));
		}
	else {
		return {};
		}
	}

std::shared_ptr<xpeer_cop_handler> xpeer_out_rt0_t::connect(const std::weak_ptr<distributed_ctx_t> & ctx_,
	std::unique_ptr<i_xpeer_source_t> && source_,
	std::unique_ptr<iol_gtw_t> && gtw_,
	std::unique_ptr<i_endpoint_t> && targetEP,
	const identity_descriptor_t & thisId,
	rtx_object_closed_t && closeHndl,
	connection_result_t && connectionHndl,
	local_object_create_connection_t && localIdentityCtr,
	remote_object_check_identity_t && remoteIdentityChecker,
	object_initialize_handler_t && objectInitialize,
	ctr_0_input_queue_t && ctrInputStack,
	std::shared_ptr<i_peer_statevalue_t> && stval,
	__event_handler_point_f&& eh,
	exceptions_suppress_checker_t && remoteExcCheck) {

	CBL_VERIFY(targetEP);

	CBL_MPF_TRACELEVEL_EVENT_RT0_CONNECTION_TRACE("outbound connect begin:" + CHECK_PTR(targetEP)->to_string());

	if(auto obj = std::shared_ptr<self_t>(new self_t(ctx_,
		std::move(source_),
		targetEP->address(),
		targetEP->port(),
		std::move(gtw_),
		std::move(closeHndl),
		std::move(remoteExcCheck),
		std::move(stval),
		thisId,
		std::move(eh)))) {

		MPF_TEST_EMULATION_IN_CONTEXT_EXPR_COND(obj->ctx(), 214, is_trait_at_emulation_context(obj->level()));

		auto identityRslt = make_identity_handler(obj, std::move(connectionHndl));

		obj->set_state(remote_object_state_t::connect_stage);

		if(auto cntx = __rtx_out_connection_ctx_timered_t::launch(ctx_,
			std::move(targetEP),
			std::move(identityRslt),
			std::move(localIdentityCtr),
			std::move(remoteIdentityChecker),
			obj->weak_from_this())) {

			obj->handshake_state_register(cntx, std::move(objectInitialize));

			CBL_MPF_IF_NOT_ERROR_EMULATION(CBL_VERIFY(obj.use_count() == 1));
			CBL_MPF_IF_NOT_ERROR_EMULATION(CBL_VERIFY(cntx.use_count() == 2));

			/* ����� ��������� ����������� */
			obj->_connect(cntx, std::move(ctrInputStack));

			CBL_MPF_IF_NOT_ERROR_EMULATION(CBL_VERIFY(cntx.use_count() == 2));

			MPF_TEST_EMULATION_IN_CONTEXT_EXPR_COND(obj->ctx(), 215, is_trait_at_emulation_context(obj->level()));

			return  std::make_shared<xpeer_cop_handler>([wptr = std::weak_ptr<self_t>(obj), wctx = std::weak_ptr<__rtx_out_connection_ctx_timered_t>(cntx)]{
				if(auto sptr = wptr.lock()) {
					CHECK_NO_EXCEPTION(sptr->close());
					}

				if(auto pctx = wctx.lock()) {
					pctx->close();
					}
				}, ctx_);
			}
		else {
			THROW_MPF_EXC_FWD(nullptr);
			}
		}
	else {
		THROW_MPF_EXC_FWD(nullptr);
		}
	}

std::shared_ptr<__initialize_state_t> xpeer_out_rt0_t::initialize_state()noexcept{
	return initialize_state_release();
	}

std::shared_ptr<__initialize_state_t> xpeer_out_rt0_t::initialize_state_release()noexcept {
	return std::atomic_exchange(&_iniSt, std::shared_ptr<__initialize_state_t>());
	}

void xpeer_out_rt0_t::handshake_state_register(std::shared_ptr< __rtx_out_connection_ctx_timered_t> ctx, 
	object_initialize_handler_t && initializeHndl ) {

	if(initializeHndl) {
		auto pIniSt(std::make_shared<__initialize_state_t>(ctx, std::move(initializeHndl)));
		auto old(std::atomic_exchange(&_iniSt, std::move(pIniSt)));
		if(old) {
			THROW_MPF_EXC_FWD(nullptr);
			}
		}
	}

void xpeer_out_rt0_t::command_handler_dtg_identity( std::unique_ptr<isendrecv_datagram_t> && ) {
	FATAL_ERROR_FWD(nullptr);
	}

void xpeer_out_rt0_t::command_handler_dgt_initialize(std::unique_ptr<isendrecv_datagram_t>&& iDtg_) {	
	if (iDtg_) {

		CBL_VERIFY(!is_null(iDtg_->address().destination_id()) && iDtg_->address().destination_id() == this_id());
		CBL_VERIFY(state() == remote_object_state_t::identity_stage_ready || state() >= remote_object_state_t::closed);

#ifdef CBL_MPF_MESSAGE_TRACEWAY_KEYS_BY_ID
		auto ktid = trait_at_keytrace_set((*iDtg_));
#endif
		CBL_MPF_KEYTRACE_SET(ktid, 402);

		CBL_MPF_TRACELEVEL_EVENT_RT0_CONNECTION_TRACE("outbound_connection:initialize setup:s="
			+ connection_key().to_string()
			+ ":session=" + session().object_hash().to_str());

		/* ���������� ������������� */
		auto initializeHndl(initialize_state());
		if (initializeHndl) {

			if (auto cnx = std::dynamic_pointer_cast<__rtx_out_connection_ctx_timered_t>(std::move(*initializeHndl).cnx())) {

				/* ������� �������� ���������� */
				auto excSend = [cnx

#ifdef CBL_MPF_MESSAGE_TRACEWAY_KEYS_BY_ID
					, ktid
#endif
				](
					std::unique_ptr<isendrecv_datagram_t>&& pm, std::unique_ptr<dcf_exception_t>&& exc_) noexcept {

					CBL_VERIFY(exc_);
					CBL_MPF_KEYTRACE_SET(ktid, 406);

					if (cnx) {
						cnx->invoke_error(exc_->dcpy());
						}

					if (pm) {
						/* �������� � ����� */
						pm->invoke_reverse_exception(std::move(exc_));
						}
					};

				try {
					MPF_TEST_EMULATION_IN_CONTEXT_COND_TRCX(ctx(), 216,
						is_trait_at_emulation_context(*iDtg_),
						trait_at_keytrace_set(*iDtg_));

					auto value = iDtg_->value_release();

					/* ������� ��������� ������ ����� ���������� ������� ������������� */
					auto externInitializerTail = utility::bind_once_call({ "xpeer_out_rt0_t::command_handler_dgt_initialize" },
						[wptr = weak_from_this(), cnx

#ifdef CBL_MPF_MESSAGE_TRACEWAY_KEYS_BY_ID
						, ktid
#endif					
						](
							initialize_result_desc_t&& result,
							decltype(excSend) && excSend_,
							std::unique_ptr<isendrecv_datagram_t>&& iDtg_)noexcept{

						CBL_MPF_KEYTRACE_SET(ktid, 405);

						if (auto sthis = std::dynamic_pointer_cast<self_t>(wptr.lock())) {
							try {
#ifdef CBL_USE_SEQUENTIAL_CALL_CHECKER
								auto seqHld = sthis->inc_scope_seqcall();
#endif

								CBL_MPF_TRACELEVEL_EVENT_RT0_CONNECTION_TRACE("outbound_connection:initialize setup end:s="
									+ sthis->remote_object_endpoint().to_string()
									+ ":t=" + sthis->this_id().to_str()
									+ ":session=" + sthis->session().object_hash().to_str()
									+ ":" + (result.exception ? cvt(result.exception->msg()) : "ok"));

								if (!result.exception) {

									MPF_TEST_EMULATION_IN_CONTEXT_COND_TRCX(sthis->ctx(), 217,
										is_trait_at_emulation_context(*iDtg_),
										trait_at_keytrace_set(*iDtg_));

									/* ��������� � ����������� ������������� */
									osendrecv_datagram_t msg{ std::move(result.responseValue) };
									msg.set_key(command_name_initialize);
									msg.capture_response_tail((*iDtg_));

									sthis->set_state(remote_object_state_t::initialize_stage_ready);

									/* �������� ��������� � ����� */
									sthis->push(std::move(msg));

									cnx->invoke_ready();
									}
								else {
									excSend_(std::move(iDtg_), std::move(result.exception));
									}
								}
							catch (const dcf_exception_t& e0) {
								excSend_(std::move(iDtg_), e0.dcpy());
								}
							catch (const std::exception& e1) {
								excSend_(std::move(iDtg_), CREATE_MPF_PTR_EXC_FWD(e1));
								}
							catch (...) {
								excSend_(std::move(iDtg_), CREATE_MPF_PTR_EXC_FWD(nullptr));
								}
							}
						}, std::placeholders::_1, std::move(excSend), std::move(iDtg_));

					CBL_MPF_KEYTRACE_SET(ktid, 403);

					if (auto c = ctx()) {
						c->launch_async(async_space_t::ext, __FILE_LINE__,
							[initializeHndl
#ifdef CBL_MPF_MESSAGE_TRACEWAY_KEYS_BY_ID
							, ktid
#endif
							](decltype(value) && v, object_initialize_result_guard_t&& g) {

							CBL_MPF_KEYTRACE_SET(ktid, 404);

							initializeHndl->invoke(std::move(v), std::move(g));
							}, std::move(value), object_initialize_result_guard_t(ctx(), async_space_t::sys, inovked_as::async, std::move(externInitializerTail)));
						}
					}
				catch (const dcf_exception_t& exc0) {
					excSend(std::move(iDtg_), exc0.dcpy());
					}
				catch (const std::exception& exc1) {
					excSend(std::move(iDtg_), CREATE_PTR_EXC_FWD(exc1));
					}
				catch (...) {
					excSend(std::move(iDtg_), CREATE_PTR_EXC_FWD(nullptr));
					}
				}
			else {
				close();
				THROW_MPF_EXC_FWD(nullptr);
				}
			}
		else {
			close();
			THROW_MPF_EXC_FWD(nullptr);
			}
		}
	else {
		close();
		THROW_MPF_EXC_FWD(nullptr);
		}
	}

