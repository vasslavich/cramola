#include "../../context/context.h"
#include "../rt1_istream.h"


using namespace crm;
using namespace crm::detail;


ix_rt1_stream_t::__address_handler::__address_handler(std::weak_ptr<distributed_ctx_t> ctx_,
	const rt0_node_rdm_t & nodeId,
	const identity_descriptor_t & thisId_,
	const identity_descriptor_t & remoteId_,
	const session_description_t & session_)
	: _nodeId(nodeId)
	, _locid(CHECK_PTR(ctx_)->make_local_object_identity())
	, _remoteRdm(rtl_roadmap_obj_t::make_rt1_istream(nodeId, remoteId_, thisId_, session_))
	, _rdmEv(rtl_roadmap_event_t::make(thisId_, CHECK_PTR(ctx_)->make_event_identity(), rtl_table_t::in, rtl_level_t::l1))
	, _rdmMsg(rtl_roadmap_obj_t::make_rt1_istream(nodeId, thisId_, remoteId_, session_)) {

	CBL_VERIFY(!is_null(direction()));
	CBL_VERIFY(!(_nodeId.connection_key().address.empty() || _nodeId.connection_key().port == 0));
	}

const rtl_roadmap_obj_t& ix_rt1_stream_t::__address_handler::msg_rdm()const  noexcept {
	return _rdmMsg;
	}

const rtl_roadmap_event_t& ix_rt1_stream_t::__address_handler::ev_rdm()const  noexcept {
	return _rdmEv;
	}

const rtl_roadmap_obj_t& ix_rt1_stream_t::__address_handler::this_rdm()const noexcept {
	return msg_rdm();
	}

const rtl_roadmap_obj_t& ix_rt1_stream_t::__address_handler::remote_rdm()const noexcept {
	return _remoteRdm;
	}

const identity_descriptor_t& ix_rt1_stream_t::__address_handler::remote_id()const noexcept {
	return remote_rdm().target_id();
	}

const identity_descriptor_t& ix_rt1_stream_t::__address_handler::this_id()const noexcept {
	return msg_rdm().target_id();
	}

const rt0_node_rdm_t& ix_rt1_stream_t::__address_handler::node_id()const noexcept {
	return _nodeId;
	}

const local_object_id_t& ix_rt1_stream_t::__address_handler::local_id()const noexcept {
	return _locid;
	}

const local_command_identity_t& ix_rt1_stream_t::__address_handler::local_object_command_id()const noexcept {
	return ev_rdm().lcid();
	}

const identity_descriptor_t& ix_rt1_stream_t::__address_handler::id()const noexcept {
	return remote_id();
	}

const ix_rt1_stream_t::__address_handler::xpeer_desc_t& ix_rt1_stream_t::__address_handler::desc()const noexcept {
	return this_rdm();
	}

const session_description_t& ix_rt1_stream_t::__address_handler::session()const noexcept {
	CBL_VERIFY(this_rdm().session() == remote_rdm().session());

	static_assert(std::is_lvalue_reference_v<decltype(this_rdm().session())>, __CBL_FILEW__);
	return this_rdm().session();
	}

rtl_table_t ix_rt1_stream_t::__address_handler::direction()const noexcept {
	return rtl_table_t::in;
	}

const std::string& ix_rt1_stream_t::__address_handler::address()const noexcept {
	return _nodeId.connection_key().address;
	}

int ix_rt1_stream_t::__address_handler::port()const noexcept {
	return _nodeId.connection_key().port;
	}

ix_rt1_stream_t::__address_handler::remote_object_endpoint_t ix_rt1_stream_t::__address_handler::remote_object_endpoint()const noexcept {
	CBL_VERIFY(!(address().empty() || port() == 0));
	return remote_object_endpoint_t::make_remote(address(), port(), remote_id());
	}

connection_key_t ix_rt1_stream_t::__address_handler::connection_key()const noexcept {
	return _nodeId.connection_key();
	}

