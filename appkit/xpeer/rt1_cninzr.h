#pragma once


#include "../internal_invariants.h"
#include "./i_rt1_xpeer.h"
#include "./rt1_opeer.h"
#include "./rt1_ostream.h"
#include "./outbound_xconnector.h"


namespace crm::detail{


struct xpeer_out_connector_rt1_t  final :
	public xpeer_out_connector_t <xpeer_out_rt1_t > {

public:
	typedef xpeer_out_rt1_t peer_t;
	typedef peer_t::remote_object_endpoint_t remote_object_endpoint_t;
	typedef xpeer_out_connector_t<peer_t> base_functor_t;
	using stream_connection_handler_f = ox_rt1_stream_t::connection_handler;
	using stream_creator_f = std::function<void( stream_connection_handler_f && hndl )>;

private:
	std::weak_ptr<distributed_ctx_t> _ctx;
	std::weak_ptr<i_rt1_main_router_t> _rt1;
	identity_descriptor_t _thisId;
	identity_descriptor_t _remoteId;
	rt1_endpoint_t _ep;
	std::shared_ptr<default_binary_protocol_handler_t> _gtwCtr;
	std::shared_ptr<i_input_messages_stock_t> _instackCtr;
	object_initialize_handler_t _outXIniHndl;
	post_connection_t _cnHndl;
	__event_handler_point_f _evHndl;
	std::function<std::shared_ptr<i_peer_statevalue_t>()> _stvalCtr;
	exceptions_suppress_checker_t _remoteExcChecker;
	stream_creator_f _sctr;

public:
	xpeer_out_connector_rt1_t( std::weak_ptr<distributed_ctx_t> ctx_,
		std::weak_ptr<i_rt1_main_router_t>  rt1,
		const identity_descriptor_t & thisId,
		const identity_descriptor_t & remoteId,
		rt1_endpoint_t && ep,
		std::shared_ptr<default_binary_protocol_handler_t> gtwCtr,
		std::unique_ptr<i_input_messages_stock_t> && instackCtr,
		object_initialize_handler_t && outXIniHndl,
		post_connection_t && cnHndl,
		std::function<std::shared_ptr<i_peer_statevalue_t>()> && stvalCtr,
		__event_handler_point_f && eh,
		exceptions_suppress_checker_t && remoteExcChecker,
		stream_creator_f && sctr );

private:
	std::unique_ptr<i_endpoint_t> target_endpoint()const override;
	const identity_descriptor_t& target_id()const;
	const identity_descriptor_t& this_id()const override;
	void event_post_connection( std::unique_ptr<dcf_exception_t> && exc, peer_link_t && peer )override;
	post_connection_t get_event_post_connection()const override;
	remote_object_endpoint_t remote_object_endpoint()const final;
	bool check_notify(const std::unique_ptr<dcf_exception_t>&) noexcept final;

	void connect(std::weak_ptr<distributed_ctx_t> ctx,
		peer_t::rtx_object_closed_t && closeHndl,
		connection_hndl_t && resultHndl,
		local_object_create_connection_t && identityCtr,
		remote_object_check_identity_t && identityChecker) override;
	};
}

