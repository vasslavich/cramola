#pragma once


#include <memory>
#include "./base_terms.h"
#include "./rt0_xpeer.h"
#include "./rt1_xpeer.h"

namespace crm {

struct xpeer_push_handler_l0 : detail::xpeer_push_handler_base {

	using peer_type = detail::xpeer_base_rt0_t;
	using xpeer_desc_t = typename peer_type::xpeer_desc_t;
	using link_type = peer_type::link_t;

	link_type _xio;
	xpeer_desc_t _xioDesc;

	detail::rtl_level_t peer_level()const noexcept {
		return detail::rtl_level_t::l0;
		}

	xpeer_push_handler_l0(link_type && xio, const xpeer_desc_t & xioDesc)
		: _xio(std::move(xio))
		, _xioDesc(xioDesc) {}

	const xpeer_desc_t& desc()const noexcept {
		return _xioDesc;
		}

	local_object_id_t local_id()const noexcept {
		return _xio.local_id();
		}

	template<typename O,
		typename std::enable_if_t <is_out_message_requirements_v<O>, int> = 0>
	void push(O && o) {
		_xio.push(std::forward<O>(o));
		}
	};


struct xpeer_push_handler_l1 : detail::xpeer_push_handler_base {

	using peer_type = detail::xpeer_base_rt1_t;
	using xpeer_desc_t = typename peer_type::xpeer_desc_t;
	using link_type = peer_type::link_t;

	link_type _xio;
	xpeer_desc_t _xioDesc;

	detail::rtl_level_t peer_level()const noexcept {
		return detail::rtl_level_t::l1;
		}

	local_object_id_t local_id()const noexcept {
		return _xio.local_id();
		}

	xpeer_push_handler_l1(link_type&& xio, const xpeer_desc_t& xioDesc)
		: _xio(std::move(xio))
		, _xioDesc(xioDesc) {}

	const xpeer_desc_t& desc()const noexcept {
		return _xioDesc;
		}

	template<typename O,
		typename std::enable_if_t <is_out_message_requirements_v<O>, int> = 0>
		void push(O && o) {
		_xio.push(std::forward<O>(o));
		}
	};
}

