#pragma once


#include "./base_predecl.h"
#include "./i_rt0_xpeer.h"


namespace crm::detail{


struct i_rt0_ostream_t{
	virtual ~i_rt0_ostream_t(){}

	virtual void close()noexcept = 0;
	virtual bool closed()const noexcept = 0;
	virtual bool is_opened()const noexcept = 0;

	virtual local_object_id_t local_id()const noexcept = 0;
	virtual identity_descriptor_t this_id()const noexcept = 0;
	virtual remote_object_state_t state()const noexcept = 0;
	virtual identity_descriptor_t remote_id()const noexcept = 0;
	virtual identity_descriptor_t id()const noexcept = 0;
	virtual i_xpeer_rt0_t::xpeer_desc_t desc()const noexcept = 0;
	virtual rt0_node_rdm_t rt0_rdm()const noexcept = 0;
	virtual connection_key_t connection_key()const noexcept = 0;
	virtual std::string address()const noexcept = 0;
	virtual int port()const noexcept = 0;
	virtual i_xpeer_rt0_t::remote_object_endpoint_t remote_object_endpoint()const noexcept = 0;

	virtual void write( outbound_message_unit_t && ) = 0;
	};
}
