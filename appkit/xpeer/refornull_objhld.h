#pragma once


#include <atomic>
#include "../utilities/scopedin/postactor.h"


namespace crm::detail{


template<typename TValue>
struct refornull_objhld_t {
public :
	using value_t = std::decay_t<TValue>;
	static const value_t default_value;

private:
	value_t _v{};
	std::atomic<bool> _entryf{false};
	std::atomic<bool> _inif{false};

public:
	template<typename ... AL,
		typename std::enable_if_t<std::is_constructible_v<value_t, AL...>,int> = 0 >
	bool set( AL && ... val ) {
		post_scope_action_t entrySet( [this] {
			if( !_inif.load( std::memory_order::memory_order_acquire ) )
				_entryf.store( false );
			} );

		bool entryf = false;
		if( _entryf.compare_exchange_strong( entryf, true ) ) {
			CBL_VERIFY( !_inif.load( std::memory_order::memory_order_acquire ) );

			value_t v( std::forward<AL>( val )... );
			std::swap( _v, v );

			_inif.store( true, std::memory_order::memory_order_release );

			return true;
			}
		else
			return false;
		}

	const value_t& get()const noexcept{
		if( _inif.load( std::memory_order::memory_order_acquire ) ) {
			return _v;
			}
		else {
			return default_value;
			}
		}
	};

template<typename TValue>
const typename refornull_objhld_t<TValue>::value_t refornull_objhld_t<TValue>::default_value{};
}
