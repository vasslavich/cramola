#pragma once


#include <memory>
#include <atomic>
#include <chrono>
#include "./rt0_xpeer.h"
#include "./rtx_cnctx.h"


namespace crm::detail{


/*! �������� ����� ��������� ����������� �� ��������� ���� */
class xpeer_in_rt0_t :
	public xpeer_base_rt0_t{

public:
	typedef xpeer_in_rt0_t self_t;
	typedef xpeer_base_rt0_t base_t;

	typedef std::function<void( std::unique_ptr<dcf_exception_t> && exc,
		link_t && peer,
		syncdata_list_t && sl,
		std::unique_ptr<execution_bool_tail> && registerCompleted )> identification_success_f;

private:
	std::shared_ptr<__handshake_in_state_t> _handshakeSt;

#ifdef CBL_MPF_OBJECTS_COUNTER_CHECKER_RT0_PEER
	crm::utility::__xobject_counter_logger_t<self_t, 1> _xDbgCounter;
#endif

	void destroy()noexcept final;

	std::shared_ptr<__handshake_in_state_t> handshake_state_release()noexcept;
	std::shared_ptr<__handshake_in_state_t> handshake_state_capture()noexcept;
	void handshake_state_register(std::shared_ptr<__handshake_in_state_t> && st);

	void start_handshake(std::shared_ptr<__rtx_in_connection_ctx_timered_t> && connCtx,
		ctr_0_input_queue_t && inputStack );
	void on_handshake_success();

protected:
	/* ������������� ���������� �������-����������� */
	xpeer_in_rt0_t(const std::weak_ptr<distributed_ctx_t> & ctx,
		std::unique_ptr<i_xpeer_source_t> && source_,
		std::string addr,
		int port,
		std::unique_ptr<iol_gtw_t> && gtw_,
		rtx_object_closed_t closingHndl,
		std::shared_ptr<i_peer_statevalue_t> && stval,
		const identity_descriptor_t & thisId,
		__event_handler_point_f&& eh);
	void accept( local_object_create_connection_t && localIdCtr,
		remote_object_check_identity_t && remoteIdChecker,
		identification_success_f && connectionHndl,
		ctr_0_input_queue_t && inputStack );

private:
	/* ���������� �������� ������������ ��������� ������������ ������� ������� */
	void command_handler_dtg_identity( std::unique_ptr<isendrecv_datagram_t> && iDtg)final;
	void command_handler_dgt_initialize( std::unique_ptr<isendrecv_datagram_t> && iDgt)final;

	std::unique_ptr<execution_bool_tail> make_register_completed();

public:
	~xpeer_in_rt0_t();

	xpeer_in_rt0_t( const xpeer_in_rt0_t & ) = delete;
	xpeer_in_rt0_t& operator=(const xpeer_in_rt0_t &) = delete;

	xpeer_in_rt0_t( xpeer_in_rt0_t && o ) = delete;
	xpeer_in_rt0_t& operator=(xpeer_in_rt0_t &&o) = delete;
	};
}

