#pragma once


#include <memory>
#include "./base_predecl.h"
#include "./rt0_ipeer.h"


namespace crm::detail{


/*! �������� ����� ��������� ����������� */
class istream_rt0_x_t  final : public xpeer_in_rt0_t {
private:
	typedef xpeer_in_rt0_t base_t;
	typedef istream_rt0_x_t self_t;

	/*! ��������� ��������� ��������� � ���� */
	void route_to_target_stock( const std::weak_ptr<i_rt0_main_router_input_t> & rt, 
		input_value_t && item );
	void unbind_reslink()noexcept;
public:
	~istream_rt0_x_t();

	istream_rt0_x_t( const std::weak_ptr<distributed_ctx_t> & ctx,
		std::unique_ptr<i_xpeer_source_t> && source_,
		std::string addr,
		int port,
		std::unique_ptr<iol_gtw_t> && gtw_,
		rtx_object_closed_t && closingHndl,
		std::shared_ptr<i_peer_statevalue_t> && stv,
		const identity_descriptor_t & thisId,
		__event_handler_point_f&& eh );
	void accept( local_object_create_connection_t && localIdCtr,
		remote_object_check_identity_t && remoteIdChecker,
		identification_success_f && connectionHndl,
		std::weak_ptr<i_rt0_main_router_input_t> && rt );

	static void accept( const std::weak_ptr<distributed_ctx_t> & ctx,
		std::unique_ptr<i_xpeer_source_t> && source_,
		std::unique_ptr<iol_gtw_t> && gtw_,
		rtx_object_closed_t && closingHndl,
		local_object_create_connection_t && localIdCtr,
		remote_object_check_identity_t && remoteIdChecker,
		identification_success_f && connectionHndl,
		const identity_descriptor_t & thisId ,
		std::weak_ptr<i_rt0_main_router_input_t> && rt,
		std::shared_ptr<i_peer_statevalue_t> && stv,
		__event_handler_point_f&& eh );
	};
}

