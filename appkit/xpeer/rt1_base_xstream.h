#pragma once


#include "../internal_invariants.h"
#include "./i_rt1_router.h"


namespace crm::detail{


class rt1_base_xstream_t {
private:
	std::weak_ptr<i_rt1_main_router_t> _router;
	std::weak_ptr<distributed_ctx_t> _ctx;

public:
	rt1_base_xstream_t( std::weak_ptr<i_rt1_main_router_t> router,
		std::weak_ptr<distributed_ctx_t> ctx );
	~rt1_base_xstream_t();

	void close()noexcept;

	void ntf_hndl( std::unique_ptr<crm::dcf_exception_t> && obj )noexcept;

	std::shared_ptr<i_rt1_main_router_t> router()const noexcept;
	std::shared_ptr<distributed_ctx_t> ctx()const noexcept;
	};
}

