#pragma once


#include "../tableval/tableval.h"
#include "../xpeer/rt1_xpeer.h"
#include "../channel_terms/terms.h"
#include "../queues/i_message_queue.h"
#include "../messages/terms.h"
#include "../xpeer/xpeer_mkprereq.h"
#include "../xpeer/i_input_handlers.h"


namespace crm {


class rt1_connection_settings_t {
public:
	using xpeer_link = xpeer_rt1_link;

private:
	identity_descriptor_t _thisId;
	std::unique_ptr<rt1_endpoint_t> _ep;
	std::unique_ptr<i_input_messages_stock_t> _inputStackCtr;
	bool _keepAlive{ false };

	void assign(rt1_connection_settings_t && o);

public:
	virtual ~rt1_connection_settings_t() {}

	rt1_connection_settings_t( const identity_descriptor_t & thisId,
		std::unique_ptr<rt1_endpoint_t> && ep,
		std::unique_ptr<i_input_messages_stock_t> && inputStackCtr,
		bool keepAlive_ = false);

	template<typename H,
		typename std::enable_if_t<is_rt1_outbound_connection_input_handler_v<H>, int> = 0>
	rt1_connection_settings_t(const identity_descriptor_t& thisId,
		std::unique_ptr<rt1_endpoint_t>&& ep,
		H&& h,
		bool keepAlive_ = false)
		: rt1_connection_settings_t(thisId,
			std::move(ep),
			std::make_unique<opeer_l1_input2handler_t>(std::forward<H>(h)),
			keepAlive_) {}

	rt1_connection_settings_t(const rt1_connection_settings_t &) = delete;
	rt1_connection_settings_t& operator=(const rt1_connection_settings_t &) = delete;

	rt1_connection_settings_t(rt1_connection_settings_t  && o)noexcept;
	rt1_connection_settings_t& operator=(rt1_connection_settings_t && o)noexcept;

	const identity_descriptor_t& this_id()const noexcept;
	const std::unique_ptr<rt1_endpoint_t>& remote_endpoint()const noexcept;
	virtual syncdata_list_t local_object_syncdata();
	virtual remote_object_connection_value_t local_object_identity();
	virtual bool remote_object_identity_validate(const identity_value_t &,
		std::unique_ptr<i_peer_remote_properties_t> &);
	virtual bool connnection_exception_suppress_checker(detail::connection_stage_t,
		const std::unique_ptr<dcf_exception_t> &)noexcept;
	virtual void local_object_initialize(datagram_t && /*dt*/,
		detail::object_initialize_result_guard_t && resultHndl);
	virtual void connection_handler(std::unique_ptr<dcf_exception_t> &&, xpeer_link&&);

	virtual void disconnection_handler(const detail::i_xpeer_rt1_t::xpeer_desc_t &,
		std::shared_ptr<i_peer_statevalue_t> &&);
	virtual void event_handler(std::unique_ptr<i_event_t> && ev);
	std::unique_ptr<i_input_messages_stock_t> input_stack();
	bool keep_alive()const noexcept;
	};

template<typename S, typename = void>
struct is_outbound_connection_settings_rt1 : std::false_type {};

template<typename S>
struct is_outbound_connection_settings_rt1 < S,
	typename std::enable_if_t<std::is_base_of_v<rt1_connection_settings_t, S>>> : std::true_type {};

template<typename S>
constexpr bool is_outbound_connection_settings_rt1_v = is_outbound_connection_settings_rt1<S>::value;

}

