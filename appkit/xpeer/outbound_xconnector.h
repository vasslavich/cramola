#pragma once


#include <functional>
#include "../internal_invariants.h"
#include "../exceptions.h"
#include "../channel_terms/i_endpoint.h"
#include "./xpeer_mkprereq.h"


namespace crm::detail{


template<typename TPeer>
class xpeer_out_connector_t{
	template<typename TPeer>
	friend class xpeer_out_connection_t;

private:
	typedef xpeer_out_connector_t<TPeer> self_t;

#ifdef CBL_MPF_OBJECTS_COUNTER_CHECKER_OUTBOUND_STLINK
	crm::utility::__xobject_counter_logger_t<self_t, 1> _xDbgCounter;
#endif

public:
	/*! ��� ���� */
	typedef typename TPeer peer_t;
	typedef typename peer_t::remote_object_endpoint_t remote_object_endpoint_t;
	typedef typename peer_t::link_t peer_link_t;
	typedef typename std::function<void( std::unique_ptr<dcf_exception_t> && exc, peer_link_t && peer )> post_connection_t;
	typedef typename peer_t::connection_result_t connection_hndl_t;
	typedef typename std::function<bool()> cancel_hndl_t;


	virtual ~xpeer_out_connector_t(){}

	virtual std::unique_ptr<i_endpoint_t> target_endpoint()const = 0;
	virtual const identity_descriptor_t& this_id()const = 0;
	virtual void connect( std::weak_ptr<distributed_ctx_t> ctx,
		typename peer_t::rtx_object_closed_t && closeHndl,
		connection_hndl_t && resultHndl,
		local_object_create_connection_t && identityCtr,
		remote_object_check_identity_t && identityChecker ) = 0;

	virtual void event_post_connection( std::unique_ptr<dcf_exception_t> && exc, peer_link_t && peer ) = 0;
	virtual post_connection_t get_event_post_connection()const = 0;
	virtual remote_object_endpoint_t remote_object_endpoint()const = 0;
	virtual bool check_notify(const std::unique_ptr<dcf_exception_t>&)noexcept = 0;
	};

}
