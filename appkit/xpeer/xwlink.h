#pragma once


#include <chrono>
#include <memory>
#include "../channel_terms/events_terms.h"


namespace crm::detail{


/*! ������ ������ �� �����������
\tparam Xpeer ���������������� ������ �� ������
*/
template<typename Xpeer>
class peer_lnk_t {
	friend typename Xpeer;

public:
	typedef typename Xpeer peer_t;
	using address_handler_t = typename std::decay_t<decltype(*(std::declval<peer_t>().address_handler()))>;
	typedef typename peer_t::rdm_local_identity_t rdm_local_identity_t;
	typedef typename peer_t::remote_object_endpoint_t remote_object_endpoint_t;
	typedef typename peer_t::xpeer_desc_t xpeer_desc_t;
	typedef typename peer_t::state_t state_t;
	typedef typename peer_t::input_value_t input_value_t;
	typedef typename peer_t::output_value_t output_value_t;

private:
	std::weak_ptr<peer_t> _wp;
	std::shared_ptr<address_handler_t> _addrh;

protected:
	std::shared_ptr<const peer_t> xptr()const noexcept {
		return _wp.lock();
		}

	std::shared_ptr<peer_t> xptr()noexcept {
		return _wp.lock();
		}

	const std::shared_ptr<address_handler_t>& addrh()const noexcept {
		return _addrh;
		}

	static const address_handler_t null_addrh;

public:
	void close()noexcept {
		_wp.reset();
		}

	peer_lnk_t()noexcept {}

	peer_lnk_t(std::weak_ptr<peer_t> wp_, std::shared_ptr<address_handler_t> addrh_)noexcept
		: _wp(std::move(wp_))
		, _addrh(std::move(addrh_)) {

		CBL_VERIFY(_addrh);
		}

	auto id()const noexcept ->decltype(_addrh->id()) {
		if(_addrh)
			return _addrh->id();
		else
			return null_addrh.id();
		}

	auto local_id()const  noexcept ->decltype(_addrh->local_id()) {
		if(_addrh)
			return _addrh->local_id();
		else
			return null_addrh.local_id();
		}

	bool valid()const noexcept {
		return !_wp.expired();
		}

	auto desc()const   noexcept ->decltype(_addrh->desc()) {
		if(_addrh)
			return _addrh->desc();
		else
			return null_addrh.desc();
		}

	std::shared_ptr<distributed_ctx_t> ctx()const  noexcept {
		if(auto p = _wp.lock())
			return p->ctx();
		else
			return nullptr;
		}

	template<typename O,
		typename std::enable_if_t < is_out_message_requirements_v<O>, int> = 0>
	void push(O && obj)const {
		CHECK_PTR(_wp)->push(std::forward<O>(obj));
		}

	template<typename O,
		typename std::enable_if_t <(
			srlz::is_serializable_v<O> &&
			!is_out_message_requirements_v<O>
			), int> = 0>
	void push(O&& obj){
		push(ostrob_message_envelop_t(std::forward<O>(obj)));
		}

	void write(outbound_message_unit_t && bin) const{
		CHECK_PTR(_wp)->write(std::move(bin));
		}

	auto address()const  noexcept ->decltype(_addrh->address()) {
		if(_addrh)
			return _addrh->address();
		else
			return null_addrh.address();
		}

	auto this_id()const  noexcept ->decltype(_addrh->this_id()) {
		if(_addrh)
			return _addrh->this_id();
		else
			return null_addrh.this_id();
		}

	auto remote_id()const  noexcept ->decltype(_addrh->remote_id()) {
		if(_addrh)
			return _addrh->remote_id();
		else
			return null_addrh.remote_id();
		}

	rtl_table_t direction()const  noexcept {
		if(_addrh)
			return _addrh->direction();
		else
			return null_addrh.direction();
		}

	int port()const  noexcept {
		if(_addrh)
			return _addrh->port();
		else
			return null_addrh.port();
		}

	bool renewable_connection()const  noexcept {
		if(auto p = _wp.lock())
			return p->renewable_connection();
		else
			return false;
		}

	std::shared_ptr<i_peer_statevalue_t> state_value()noexcept {
		if(auto p = _wp.lock()) {
			return p->state_value();
			}
		else {
			return nullptr;
			}
		}

	std::shared_ptr<i_peer_statevalue_t> state_value()const noexcept {
		if(auto p = _wp.lock()) {
			return p->state_value();
			}
		else {
			return nullptr;
			}
		}

	auto remote_object_endpoint()const  noexcept  ->decltype(_addrh->remote_object_endpoint()) {
		if(_addrh)
			return _addrh->remote_object_endpoint();
		else
			return null_addrh.remote_object_endpoint();
		}

	void set_state_value(std::shared_ptr<i_peer_statevalue_t> && stval) {
		CHECK_PTR(_wp)->set_state_value(std::move(stval));
		}

	void set_state(remote_object_state_t st) {
		if(_addrh)
			_addrh->set_state(st);
		else
			THROW_EXC_FWD(nullptr);
		}

	template<typename THandl>
	[[nodiscard]]
	subscribe_invoker_result subscribe_hndl_id(async_space_t scx,
		const _address_hndl_t &hndl, 
		subscribe_options opt,
		typename THandl && f) {

		return CHECK_PTR(_wp)->subscribe_hndl_id(scx, hndl, opt, std::forward<THandl>(f));
		}

	void unsubscribe_hndl_id(const _address_hndl_t &hndl)noexcept {
		if(auto p = _wp.lock())
			p->unsubscribe_hndl_id(hndl);
		}

	template<typename THandler,
		typename std::enable_if_t<std::is_constructible_v<__event_handler_f, std::decay_t<THandler>>, int> = 0>
	[[nodiscard]]
	subscribe_result subscribe_event(async_space_t scx, const _address_event_t &hndl, bool once, THandler&& f) {
		return CHECK_PTR(_wp)->subscribe_event(scx, hndl, once, std::forward<THandler>(f));
		}

	void subscribe_event(events_list && el) {
		CHECK_PTR(_wp)->subscribe_event(std::move(el));
		}

	void unsubscribe_event(const _address_event_t &hndl)noexcept {
		if(auto p = _wp.lock())
			p->unsubscribe_event(hndl);
		}

	int tag()const  noexcept {
		if(auto p = _wp.lock())
			return p->tag();
		else
			return -1;
		}

	void set_tag(int value) {
		CHECK_PTR(_wp)->set_tag(value);
		}

	bool closed()const noexcept {
		if(auto p = _wp.lock())
			return p->closed();
		else
			return true;
		}

	state_t state() const noexcept {
		if(_addrh)
			return _addrh->state();
		else
			return null_addrh.state();
		}

	bool is_opened() const noexcept {
		if(auto p = _wp.lock())
			return p->is_opened();
		else
			return false;
		}

	rtl_level_t level()const  noexcept {
		if(_addrh)
			return _addrh->level();
		else
			return null_addrh.level();
		}

	auto link()  noexcept {
		if(auto p = _wp.lock())
			return p->get_link();
		else
			return typename peer_t::link_t{};
		}

	auto connection_key()const  noexcept ->decltype(_addrh->connection_key()) {
		if(_addrh)
			return _addrh->connection_key();
		else
			return null_addrh.connection_key();
		}

	std::shared_ptr<const i_peer_remote_properties_t> remote_properties()const noexcept {
		if(auto p = _wp.lock())
			return p->remote_properties();
		else
			return std::shared_ptr<const i_peer_remote_properties_t>{};
		}

	peer_commands_result_t execute_command(peer_command_t cmd) {
		return CHECK_PTR(_wp)->execute_command(cmd);
		}

	void invoke_event(event_type_t t)noexcept {
		if(auto p = _wp.lock()) {
			p->invoke_event( t);
			}
		}
	};

	
template<typename Xpeer>	
const typename peer_lnk_t<Xpeer>::address_handler_t peer_lnk_t<Xpeer>::null_addrh;


template<typename Xpeer>
class peer_lnk_l1_t : public peer_lnk_t<Xpeer> {
private:
	using base_type = typename peer_lnk_t<Xpeer>;

public:
	peer_lnk_l1_t()noexcept {}

	peer_lnk_l1_t(std::weak_ptr<peer_t> wp_, std::shared_ptr<address_handler_t> addrh_)noexcept
		: base_type(std::move(wp_), std::move(addrh_)) {}

	auto this_rdm()const  noexcept ->decltype(addrh()->this_rdm()) {
		if(auto ah_ = addrh())
			return ah_->this_rdm();
		else
			return null_addrh.this_rdm();
		}

	auto remote_rdm()const  noexcept ->decltype(addrh()->remote_rdm()) {
		if(auto ah_ = addrh())
			return ah_->remote_rdm();
		else
			return null_addrh.remote_rdm();
		}
	};
}


