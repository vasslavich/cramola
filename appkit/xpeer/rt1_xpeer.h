#pragma once


#include <memory>
#include <atomic>
#include <chrono>
#include "../internal_invariants.h"
#include "./i_rt1_xpeer.h"
#include "../protocol/i_bincntr.h"
#include "../protocol/base_predecl.h"
#include "./xpeer_mkprereq.h"
#include "../channel_terms/events_terms.h"
#include "../channel_terms/subscribe_terms.h"
#include "../erremlt/utilities.h"
#include "../utilities/check_validate/checker_seqcall.h"

namespace crm::detail {


/*! �������� ����� ����������� � ���� */
class xpeer_base_rt1_t :
	/* ��������� ������� ����������� 1-�� ������ */
	public i_xpeer_rt1_t,
	public i_decode_tail_stream2peer_t_x1,
	/* ���������������� �������� ������ �� ������ */
	public std::enable_shared_from_this<xpeer_base_rt1_t> {

public:
	typedef xpeer_base_rt1_t self_t;

	typedef peer_lnk_l1_t<xpeer_base_rt1_t> link_t;
	typedef peer_lnk_l1_t<const xpeer_base_rt1_t> clink_t;

	using event_open_connection_t = event_open_connection_t<link_t>;
	using event_close_connection_t = event_close_connection_t<link_t>;

	std::shared_ptr<distributed_ctx_t> ctx()noexcept;
	std::shared_ptr<distributed_ctx_t> ctx()const noexcept;
	void ntf_hndl(std::unique_ptr<dcf_exception_t> && ntf)noexcept;
	void exc_hndl(const dcf_exception_t & exc)noexcept;
	void exc_hndl(dcf_exception_t && exc)noexcept;

private:
	typedef binary_vector_t io_buffer_t;
	typedef std::weak_ptr<self_t> self_weak_t;
	typedef std::weak_ptr<const self_t> cself_weak_t;
	typedef std::shared_ptr<self_t> self_shared_t;
	typedef std::shared_ptr<const self_t> cself_shared_t;

	/*! �������� */
	std::weak_ptr<distributed_ctx_t> _ctx;
	/*! �������� �������� ������� */
	std::weak_ptr<base_stream_x_rt1_t> _sourceStream;
	std::shared_ptr<i_x1_stream_address_handler> _addrh;
	std::unique_ptr<gtw_rt1_x_t> _slotGtw;
	rtx_object_closed_t _eventClosingHndl;
	/*! ���������������� ������ ���� */
	std::shared_ptr<i_peer_statevalue_t> _stateValue;
	/*! ���������������� ���, ��������� � ����� */
	mutable std::atomic<int> _tag{ 0 };
	std::atomic<bool> _stop{ false };
	io_buffer_t _inBuffer;
	std::shared_ptr<input_stack_t> _inqAdpt;
	/*! ��������� ������� ��������� ������� */
	std::shared_ptr<i_peer_remote_properties_t> _remoteProp;

	/*! ���������� �� ������� ������� */
	event_subscribe_assist_t::event_handler_t _evhLst;

	__event_handler_point_f _evHndl;

#ifdef CBL_MPF_OBJECTS_COUNTER_CHECKER_RT1_PEER
	crm::utility::__xobject_counter_logger_t<self_t, 1> _xDbgCounter;
#endif

#ifdef CBL_USE_SEQUENTIAL_CALL_CHECKER
	checker_seqcall_t _seqCallCheckerIO;
#endif

	std::string address_trace()const;
	void trace_send(const i_iol_ohandler_t &msg)const noexcept;
	void trace_recv(const i_iol_ihandler_t &msg)const noexcept;

#ifdef CBL_ENABLE_TEST_ERROR_EMULATION_LEVEL_1
	std::string check_erreml_key()const noexcept;
#endif

protected:
	virtual void on_handshake_success();
	void event_invoke(event_type_t type, std::unique_ptr<event_args_t> && args)noexcept;
	void on_extern_completed(std::function<void()> && externHndl);

	int tag()const noexcept;
	void set_tag(int value)noexcept;
	void set_state(remote_object_state_t st)noexcept;
	void set_remote_properties(std::unique_ptr<i_peer_remote_properties_t> && prop)noexcept;

public:
	std::shared_ptr<i_x1_stream_address_handler> address_handler()const noexcept;

	/*! ������ ������ �� ��������� ������� */
	link_t get_link()noexcept;
	/*! ������ ������ �� ��������� ������� */
	clink_t get_link()const noexcept;
	const session_description_t& session()const noexcept final;
	remote_object_state_t state()const noexcept final;
	const rtl_roadmap_obj_t& this_rdm()const noexcept final;
	const rtl_roadmap_obj_t& remote_rdm()const noexcept final;
	const identity_descriptor_t& this_id()const noexcept final;
	const identity_descriptor_t& remote_id()const noexcept final;
	const identity_descriptor_t& id()const noexcept final;
	const std::string& name()const noexcept final;
	const std::string& address()const  noexcept final;
	int port()const  noexcept final;
	connection_key_t connection_key()const  noexcept final;
	rtl_table_t direction()const noexcept final;
	std::shared_ptr<const base_stream_x_rt1_t> source_stream()const noexcept final;
	std::shared_ptr<base_stream_x_rt1_t> source_stream()noexcept final;
	const rdm_local_identity_t& rdm_local_identity()const noexcept;
	const xpeer_desc_t& desc()const noexcept final;
	const local_object_id_t& local_id()const noexcept final;
	std::shared_ptr<i_peer_statevalue_t> state_value()noexcept final;
	std::shared_ptr<const i_peer_statevalue_t> state_value()const noexcept final;
	void set_state_value(std::shared_ptr<i_peer_statevalue_t> && stv)noexcept;
	bool renewable_connection()const noexcept final;
	remote_object_endpoint_t remote_object_endpoint()const noexcept final;
	std::shared_ptr<const i_peer_remote_properties_t> remote_properties()const noexcept final;

#ifdef CBL_USE_SMART_OBJECTS_COUNTING
	crm::log::counter_t subcounter(const std::string & name);
#endif//CBL_USE_SMART_OBJECTS_COUNTING

private:
	void decode(message_frame_unit_t && lst)final;
	void subscribe_command_handler();
	void command_handler(std::unique_ptr<i_iol_ihandler_t> && iCmd);

	void addressed_exception(const _address_hndl_t & address,
		std::unique_ptr<dcf_exception_t> && ntf,
		invoke_context_trait ic)noexcept final;

public:
	void invoke_event(event_type_t t)noexcept final;

protected:
#ifdef CBL_USE_SEQUENTIAL_CALL_CHECKER
	using seqcall_checker_holder = std::decay_t<decltype(_seqCallCheckerIO.inc_scope())>;
	seqcall_checker_holder inc_scope_seqcall();
#endif

	/* ������������� ���������� �������-����������� */
	xpeer_base_rt1_t(const std::weak_ptr<distributed_ctx_t> & ctx,
		std::shared_ptr<base_stream_x_rt1_t> && source_,
		std::unique_ptr<gtw_rt1_x_t> && gtw,
		rtx_object_closed_t && closeHndl,
		std::shared_ptr<i_peer_statevalue_t> && stval,
		__event_handler_point_f && xh);

	virtual void command_handler_dtg_identity(std::unique_ptr<isendrecv_datagram_t> && iDgt) = 0;
	virtual void command_handler_dgt_initialize(std::unique_ptr<isendrecv_datagram_t> && iDgt) = 0;
	void set_handshake_mode(std::shared_ptr<input_stack_t> && qinput);
	void handshake_end(bool result);
	void io_rw_listen();
	void reverse_functor_message(std::unique_ptr<ifunctor_package_t>&& m);

	void close(closing_reason_t reason)noexcept;
	void close_base_stream()noexcept;

private:
	template<typename O,
		typename std::enable_if_t < is_out_message_requirements_v<O>, int> = 0>
		void normalizex(O && obj)const {
		if (auto c = ctx()) {

			/* ������ */
			if (is_null(obj.session())) {
				CBL_VERIFY(!is_null(session()));
				obj.set_session(session());
				}

			/* ������������� ��������� */
			if (is_null(obj.id())) {
				obj.set_id(c->generate_message_id());
				}

			/* ������� */
			if (is_null(obj.destination_id()))
				obj.set_destination_id(remote_id());

			/* �������� */
			if (is_null(obj.source_id()))
				obj.set_source_id(this_id());

			/* ������� ������������� */
			if (!obj.fl_level())
				obj.set_level(level());

			/* ������������� ����������� ����� */
			if (!CHECK_PTR(_sourceStream)->is_integrity_provided() || c->policies().use_crc()) {
				obj.setf_use_crc();
				}

#ifdef CBL_MPF_ENABLE_TIMETRACE
			obj.set_timeline(hanlder_times_stack_t::stage_t::rt1_sender);
#endif//CBL_MPF_ENABLE_TIMETRACE
			}
		else {
			THROW_MPF_EXC_FWD(nullptr);
			}
		}

	void _dctr()noexcept;

public:
	xpeer_base_rt1_t(const xpeer_base_rt1_t &) = delete;
	xpeer_base_rt1_t& operator=(const xpeer_base_rt1_t &) = delete;

	xpeer_base_rt1_t(xpeer_base_rt1_t && o) = delete;
	xpeer_base_rt1_t& operator=(xpeer_base_rt1_t &&o) = delete;

	virtual ~xpeer_base_rt1_t();

	void close()noexcept;
	bool closed()const noexcept final;
	bool is_opened()const noexcept final;
	rtl_level_t level()const noexcept final;

private:
	void write(outbound_message_unit_t && rt1Obj)final;
	void flush();

	template<typename O,
		typename std::enable_if_t < is_out_message_requirements_v<O>, int> = 0>
		void push_normalizedx(O && obj) {

#pragma message( CBL_VAR_NAME_VALUE(CBL_BUILD_VAR_MESSAGE_ROADMAP_MARKER_OUTBOUND, 0))
		CBL_MPF_KEYTRACE_SET(obj, 14);

		MPF_TEST_EMULATION_IN_CONTEXT_COND_TRCX(ctx(), 402,
			is_trait_at_emulation_context(obj),
			trait_at_keytrace_set(obj));

		normalizex(obj);

		CBL_VERIFY(!is_null(obj.session()));
		CBL_VERIFY_SNDRCV_KEY_SPIN_TAG(obj);

#if defined( CBL_MPF_TRACELEVEL_EVENT_RT1_SEND_INBOUND ) || defined (CBL_MPF_TRACELEVEL_EVENT_RT1_SEND_OUTBOUND)
		CHECK_NO_EXCEPTION(trace_send(obj));
#endif

		CBL_VERIFY(obj.type().type() != iol_types_t::st_notification || !is_sendrecv_request(obj.address()));
		write(CHECK_PTR(_slotGtw)->serialize_gateout(std::forward<O>(obj)));
		}

	std::function<std::unique_ptr<i_event_t>(event_type_t)> event_checker()noexcept;

protected:
	void initialize();

	std::shared_ptr<input_stack_t> get_input_queue()noexcept;
	std::shared_ptr<input_stack_t> get_input_queue()const noexcept;

	[[nodiscard]]
	subscribe_invoker_result __subscribe_hndl_id(async_space_t scx,
		const _address_hndl_t & id,
		subscribe_options opt,
		subscribe_message_handler && h);
	void __unsubscribe_hndl_id(const _address_hndl_t & id)noexcept;

	void event_invoke(std::unique_ptr<i_event_t> && ev)noexcept;
	__event_handler_point_f get_event_handler()const noexcept;


	template<typename THandler,
		typename std::enable_if_t<std::is_constructible_v<__event_handler_f, std::decay_t<THandler>>, int> = 0>
		[[nodiscard]]
	subscribe_result subscribe_event_(async_space_t scx,
		const _address_event_t& id,
		bool once,
		THandler&& h) {

		if (!closed()) {
			return _evhLst.subscribe(scx, id, once, std::forward<THandler>(h), event_checker());
			}
		else {
			return subscribe_result::enum_type::exception;
			}
		}

public:
	/*! ��������� ������ � ������� ��������� ��������� */
	template<typename O,
		typename std::enable_if_t < is_out_message_requirements_v<O>, int> = 0>
		void push(O && obj) {
		push_normalizedx(std::forward<O>(obj));
		}

	/*! �������� �� ��������� ��������� ��� ���������� ��������������.
	\param id ������� �������������
	\param once ���� ������� true, ����������� �������� ���� ���, ����� �� ��� ���,
	���� �� ����� ��������� ������ ����������� � ������� ������ /ref unsubscribe_hndl_id.
	*/
	[[nodiscard]]
	subscribe_invoker_result subscribe_hndl_id_tv(async_space_t scx,
		const _address_hndl_t & id,
		subscribe_options opt,
		subscribe_message_handler && h);

	/*! ����� �� ��������� ����������� ��� ���������� ��������������
	\param id ������������� ����������
	*/
	void unsubscribe_hndl_id(const _address_hndl_t & id)noexcept final;

	template<typename THandler,
		typename std::enable_if_t<(
			std::is_constructible_v<__event_handler_f, std::decay_t<THandler>> && 
			std::is_base_of_v<__event_handler_f, std::decay_t<THandler>>
			), int> = 0>
	[[nodiscard]]
	subscribe_result subscribe_event(async_space_t scx,
		const _address_event_t & id,
		bool once,
		THandler && h) {
		return subscribe_event_(scx, id, once, std::forward<THandler>(h));
		}

	[[nodiscard]]
	subscribe_result subscribe_event(async_space_t scx,
		const _address_event_t& id,
		bool once,
		__event_handler_f && h) {

		return subscribe_event_(scx, id, once, std::move(h));
		}

	void unsubscribe_event(const _address_event_t & id)noexcept final;

	[[nodiscard]]
	subscribe_invoker_result subscribe_hndl_id(async_space_t scx,
		const _address_hndl_t & id,
		subscribe_options opt,
		subscribe_message_handler && h) final;

	void subscribe_event(events_list && el) final {
		if (!closed()) {
			_evhLst.add(std::move(el));
			}
		else {
			THROW_EXC_FWD("closed");
			}
		}

	peer_commands_result_t execute_command(peer_command_t cmd)final;
	};
}

namespace crm {

using xpeer_rt1_link = detail::xpeer_base_rt1_t::link_t;
}

