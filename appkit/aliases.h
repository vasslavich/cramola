#pragma once


#include "./terms.h"
#include "./channel_terms/i_route_io.h"


namespace crm{
using xpeer_direction_t = detail::rtl_table_t;


template<typename TAction>
using  scoped_execution_t = detail::post_scope_action_t<TAction>;

template<typename TAction>
decltype(auto) make_scoped_execution( TAction && a ){
	return detail::post_scope_action_t( std::forward<TAction>( a ) );
	}
}
