#pragma once


#include <atomic>
#include <chrono>
#include <mutex>
#include <iterator>
#include "../exceptions.h"
#include "./bufdef.h"


namespace crm::detail{


template<typename TCritical, typename = std::void_t<>>
struct TCriticalTrait {
	static bool onws_lock(const TCritical& /*c*/) {
		return true;
		}
	};


template<typename TCritical>
struct TCriticalTrait<TCritical, 
	std::void_t<decltype(std::declval<TCritical>().owns_lock())>>{

	static bool onws_lock(const TCritical& c) {
		return c.owns_lock();
		}
	};


template<typename TCondVar, typename TCritical, typename = std::void_t<>>
struct TCondVarTrait {};


template<typename TCondVar, typename TCritical>
struct TCondVarTrait<TCondVar, TCritical, std::void_t<
	decltype(std::declval<TCondVar>().wait(std::declval<TCritical>())),
	decltype(std::declval<TCondVar>().wait(std::declval<TCritical>(), std::declval<std::function<bool()>>())),
	decltype(std::declval<TCondVar>().wait_for(std::declval<TCritical>(), std::declval<std::chrono::seconds>())),
	decltype(std::declval<TCondVar>().wait_for(std::declval<TCritical>(), std::declval<std::chrono::seconds>(), std::declval<std::function<bool()>>()))
	>> {

private:
	template<typename TWaitCond>
	static bool wait_timed(TCondVar & c, TCritical & m, TWaitCond && f){
		return c.wait_for(m, f.timeout());
		}

	template<typename TWaitCond>
	static bool wait_pred(TCondVar & c, TCritical & m, TWaitCond && f){
		return c.wait(m, std::forward<TWaitCond>(f));
		}

	template<typename TWaitCond>
	static bool wait_timed_pred(TCondVar & c, TCritical & m, TWaitCond && f){

		const auto t_ = f.timeout();
		return c.wait_for(m, t_, std::forward<TWaitCond>(f));
		}

	template<typename TWaitCondFault>
	struct wait_wrong_type : std::false_type{};

public:
	template<typename TWaitCond>
	static bool wait(TCondVar & c, TCritical & m, TWaitCond && f){

		if constexpr (is_wait_cond_timed_and_pred_v<TWaitCond>){
			return wait_timed_pred(c, m, std::forward<TWaitCond>(f));
			}
		else{
			if constexpr (is_wait_cond_timed_v<TWaitCond>){
				return wait_timed(c, m, std::forward<TWaitCond>(f));
				}
			else{
				if constexpr (is_wait_cond_pred_v<TWaitCond>){
					return wait_pred(c, m, std::forward<TWaitCond>(f));
					}
				else{
					static_assert(wait_wrong_type<TWaitCond>::value, __FILE_LINE__);
					}
				}
			}
		}

	static bool wait(TCondVar & c, TCritical & m ){
		return c.wait(m);
		}
	};


template<typename Tcritical,
	typename Tcritical_scope,
	typename Tcondvar,
	typename Tsyncobj >
struct mt_fifo_queue_as_references{
	using self_type = mt_fifo_queue_as_references<Tcritical, Tcritical_scope, Tcondvar, Tsyncobj>;

protected:
	/// buffer of data
	Tsyncobj obj_;
	/// sync objects
	Tcritical lock_;
	Tcondvar condInc_;
	Tcondvar condDec_;

	mt_fifo_queue_as_references( const self_type& ) = delete;
	mt_fifo_queue_as_references& operator=( const self_type & ) = delete;

	template<typename XContainer,
		typename XCondVar,
		typename XMytex>
		mt_fifo_queue_as_references( XContainer && c,
			XCondVar && cvInc,
			XCondVar && cvDec,
			XMytex && lck )
		: obj_( std::forward<XContainer>( c ) )
		, lock_( std::forward<XMytex>( lck ) )
		, condInc_( std::forward<XCondVar>( cvInc ) )
		, condDec_( std::forward<XCondVar>( cvDec ) ){}

	template<typename XContainer>
	mt_fifo_queue_as_references( XContainer && c )
		: obj_( std::forward<XContainer>( c ) ){}

	/**
	* capacityElements - capacity of the buffer in an elements
	*/
	mt_fifo_queue_as_references( const size_t capacityElements )
		: obj_( capacityElements ){}
	};


template<typename Tcritical,
	typename Tcritical_scope,
	typename Tcondvar,
	typename Tsyncobj >
struct mt_fifo_queue_as_non_references{
	using self_type = mt_fifo_queue_as_non_references<Tcritical, Tcritical_scope, Tcondvar, Tsyncobj>;

protected:
	/// buffer of data
	Tsyncobj obj_;
	/// sync objects
	mutable Tcritical lock_;
	Tcondvar condInc_;
	Tcondvar condDec_;

	mt_fifo_queue_as_non_references( const self_type& ) = delete;
	mt_fifo_queue_as_non_references& operator=( const self_type & ) = delete;

	template<typename XContainer,
		typename XCondVar,
		typename XMytex>
	mt_fifo_queue_as_non_references( XContainer && c,
			XCondVar && cvInc,
			XCondVar && cvDec,
			XMytex && lck )
		: obj_( std::forward<XContainer>( c ) )
		, lock_( std::forward<XMytex>( lck ) )
		, condInc_( std::forward<XCondVar>( cvInc ) )
		, condDec_( std::forward<XCondVar>( cvDec ) ){}

	template<typename XContainer>
	mt_fifo_queue_as_non_references( XContainer && c )
		: obj_( std::forward<XContainer>( c ) ){}

	/**
	* capacityElements - capacity of the buffer in an elements
	*/
	mt_fifo_queue_as_non_references( const size_t capacityElements )
		: obj_( capacityElements ){}
	};


template<typename Tcritical, 
	typename Tcritical_scope, 
	typename Tcondvar, 
	typename Tsyncobj,
	typename BaseClass_ = std::conditional_t<std::is_lvalue_reference_v<Tcritical>,
		mt_fifo_queue_as_references<Tcritical, Tcritical_scope, Tcondvar, Tsyncobj>,
		mt_fifo_queue_as_non_references<Tcritical, Tcritical_scope, Tcondvar, Tsyncobj>>>
class mt_fifo_queue_t : public BaseClass_{
public:
	typedef typename std::decay_t<Tsyncobj>::value_type value_type;
	typedef typename Tcritical lck_t;
	typedef typename Tcritical_scope lck_scp_t;
	typedef typename Tcondvar cndvar_t;
	typedef bufusage_prec_t result_t;
	using self_type = mt_fifo_queue_t<Tcritical, Tcritical_scope, Tcondvar, Tsyncobj>;

private:
	std::uintmax_t stored_count()const {
		return obj_.ready_count();
		}

	std::uintmax_t max_count()const {
		return obj_.size();
		}

	void _clear() {
		obj_.clear();
		condInc_.notify_all();
		}

	void reset() {
		_clear();
		}

	struct pred_cond_{
		size_t * condValue_{nullptr};
		self_type * this_{nullptr};

		self_type* pthis()const{
			return this_;
			}

		pred_cond_(self_type * pthis_)
			: this_( pthis_ ){
			CBL_VERIFY(this_);
			}

		size_t value()const{
			return (*condValue_);
			}

		void set_value(size_t val){
			if( val )
				(*condValue_) = val;
			}
		};

	struct pred_cond_inc_  : pred_cond_ {
		pred_cond_inc_(self_type * pthis_ )
			: pred_cond_(pthis_){}

		bool operator()(){
			if (pthis()->max_count() < pthis()->stored_count())
				FATAL_ERROR_FWD(nullptr);

			set_value(pthis()->max_count() - pthis()->stored_count());
			return 0 != value();
			}
		};

	struct pred_cond_dec_ : pred_cond_ {
		pred_cond_dec_(self_type * pthis_ )
			: pred_cond_(pthis_){}

		bool operator()(){
			set_value(pthis()->stored_count());
			return 0 != value();
			}
		};

	template<typename Rep, typename Period, typename TDuration, typename base_type = std::decay_t<TDuration>>
	struct timeout_pred_cond_ : base_type{
		std::chrono::duration<Rep, Period> _timeout;

		timeout_pred_cond_(const std::chrono::duration<Rep, Period> & timeout_, self_type * pthis_)
			: base_type(pthis_)
			, _timeout(timeout_){}

		decltype(auto) timeout()const noexcept{ return _timeout; }
		};

	template<typename Rep, typename Period>
	timeout_pred_cond_<Rep, Period, pred_cond_inc_> make_pred_cond_inc(const std::chrono::duration<Rep, Period> & timeout_){
		return timeout_pred_cond_<Rep, Period, pred_cond_inc_>(timeout_, this);
		}

	template<typename Rep, typename Period>
	timeout_pred_cond_<Rep, Period, pred_cond_dec_> make_pred_cond_dec(const std::chrono::duration<Rep, Period> & timeout_){
		return timeout_pred_cond_<Rep, Period, pred_cond_dec_>(timeout_, this);
		}

	pred_cond_inc_ make_pred_cond_inc(){
		return pred_cond_inc_(this);
		}

	pred_cond_dec_ make_pred_cond_dec(){
		return pred_cond_dec_(this);
		}

	template<typename TPred>
	size_t sem_wait_inc_(lck_scp_t &lock, TPred && waitCnd){

		size_t value{ 0 };
		waitCnd.condValue_ = std::addressof( value );

		waitCnd.set_value(max_count() - stored_count());
		if (!waitCnd.value()){

			CBL_VERIFY(TCriticalTrait<decltype(lock)>::onws_lock(lock));
			if (TCondVarTrait<decltype(condInc_), decltype(lock)>::wait(condInc_, lock, std::forward<TPred>(waitCnd))){

				CBL_VERIFY( waitCnd.value() );
				CBL_VERIFY((stored_count() <= max_count() && stored_count() >= 0));
				CBL_VERIFY(TCriticalTrait<decltype(lock)>::onws_lock(lock));
				}
			}

		return value;
		}

	size_t sem_wait_inc(lck_scp_t &lock){
		return sem_wait_inc_( lock, make_pred_cond_inc() );
		}

	template<typename TRep, typename TPeriod>
	size_t sem_wait_inc(lck_scp_t &lock, const std::chrono::duration<TRep, TPeriod> & timeout){
		return sem_wait_inc_(lock, make_pred_cond_inc(timeout));
		}

	template<typename TPred>
	size_t sem_wait_dec_( lck_scp_t &lock, TPred && cond ) {
		size_t value{ 0 };
		cond.condValue_ = std::addressof( value );

		cond.set_value(stored_count());
		if (!cond.value()){
			CBL_VERIFY(TCriticalTrait<decltype(lock)>::onws_lock(lock));
			
			if (TCondVarTrait<decltype(condDec_), decltype(lock)>::wait(condDec_, lock, std::forward<TPred>(cond))){
				CBL_VERIFY(TCriticalTrait<decltype(lock)>::onws_lock(lock));
				CBL_VERIFY( cond.value() );
				}
			}

		return value;
		}

	size_t sem_wait_dec(lck_scp_t &lock){
		return sem_wait_dec_(lock, make_pred_cond_dec());
		}

	template<typename TRep, typename TPeriod>
	size_t sem_wait_dec(lck_scp_t &lock, const std::chrono::duration<TRep, TPeriod> & timeout){
		return sem_wait_dec_(lock, make_pred_cond_dec(timeout));
		}

	void sem_inc( const size_t increment ) {
		if( increment ) {
			if(1 == increment)
				condDec_.notify_one();
			else
				condDec_.notify_all();
			}
		}

	void sem_dec( const size_t decrement ) {
		if( decrement ) {
			CBL_VERIFY( stored_count() >= 0 );

			if(1 == decrement)
				condInc_.notify_one();
			else
				condInc_.notify_all();
			}
		}

public:
	const std::string& name()const noexcept{
		return obj_.name();
		}

	template<typename XContainer,
		typename XCondVar,
		typename XMytex>
		mt_fifo_queue_t(XContainer && c,
			XCondVar && cvInc,
			XCondVar && cvDec,
			XMytex && lck )
		: BaseClass_(std::forward<XContainer>(c),
			std::forward<XCondVar>( cvInc ),
			std::forward<XCondVar>( cvDec ),
			std::forward<XMytex>(lck) ){
		
		static_assert(std::is_reference_v<decltype(c)>, __FILE_LINE__);
		}

	template<typename XContainer>
	mt_fifo_queue_t(XContainer && c)
		: BaseClass_(std::forward<XContainer>(c)){}

	/**
	* capacityElements - capacity of the buffer in an elements
	*/
	mt_fifo_queue_t( const size_t capacityElements )
		: BaseClass_( capacityElements ){}

	void clear() {
		lck_scp_t lock( lock_ );
		_clear();
		}

	/**
	* insert to tail of the buffer, wait for insert
	*	msg : pointer to source buffer
	*	bufcnt : 	in - count of elements for inserting,
	*				out - count of elements that is really stored
	*	wait :   	in - count of milliseconds for wait
	*  return : 0 - success, not zero - error code of this template or Tsyncobj
	*/
	template<typename TInIt, typename Rep, typename Period>
	bufusage_prec_t push(
		TInIt first, TInIt last,
		typename const std::chrono::duration<Rep, Period> &wait ) {

		const auto count = std::distance( first, last );
		if( count >= 0 ) {
			lck_scp_t lock( lock_ );
			bufusage_prec_t r;

			//  wait place(in the buffer) for insert
			const auto gap = sem_wait_inc( lock, wait );
			if( gap < (size_t)count ) {
				r.errc = bufusage_prec_t::codes_t::ERR_TIME_OUT;
				return r;
				}

			r = obj_.push( first, last );
			if( r ) {
				CBL_VERIFY( r.usedElements == count );
				sem_inc( r.usedElements );
				}

			return r;
			}
		else {
			THROW_EXC_FWD(nullptr);
			}
		}

private:
	template<typename TReaderTimed>
	void push_timed( TReaderTimed & rs ){

		lck_scp_t lock( lock_ );
		while( rs.ready() && !rs.canceled() ){

			//  wait place(in the buffer) for insert
			size_t gap = 0;
			if( rs.read_count() ){
				gap = sem_wait_inc( lock, rs.timeout_quant() );
				}
			else{
				gap = sem_wait_inc( lock, rs.timeout() );
				}

			if( gap ){
				auto range_ = rs.next( gap );
				auto insr = obj_.push( range_.first, range_.second );
				CBL_VERIFY( std::distance( range_.first, range_.second ) == insr.usedElements );
				CBL_VERIFY( insr.usedElements <= gap );

				sem_inc( insr.usedElements );
				}
			else{
				if( !rs.read_count() )
					return;
				}
			}

		CBL_VERIFY( !rs.ready() || rs.canceled() || rs.empty() );
		}

	template<typename TReaderCanceled>
	void push_canceled( TReaderCanceled & rs ){

		lck_scp_t lock( lock_ );
		while( rs.ready() && !rs.canceled() ){

			//  wait place(in the buffer) for insert
			if( auto gap = sem_wait_inc( lock ) ){

				auto range_ = rs.next( gap );
				auto insr = obj_.push( range_.first, range_.second );
				CBL_VERIFY( std::distance( range_.first, range_.second ) == insr.usedElements );
				CBL_VERIFY( insr.usedElements <= gap );

				sem_inc( insr.usedElements );
				}
			else{
				if( !rs.read_count() )
					return;
				}
			}

		CBL_VERIFY( !rs.ready() || rs.canceled() || rs.empty() );
		}
public:

	template<typename TReader,
		typename std::enable_if_t<is_iterator_reader_v<TReader>, int> = 0>
	void push( TReader & rs ){
		if constexpr( is_break_timed_and_cancel_v <TReader> ){
			push_timed( rs );
			}
		else{
			if constexpr( is_break_cancel_v<TReader> ){
				push_canceled( rs );
				}
			else{
				static_assert(fail_pop_definition<TReader>::value, __FILE_LINE__);
				}
			}
		}

	/**
	* insert to tail of the buffer(move semantics), wait for insert
	*	msg : pointer to source message
	*	bufcnt : 	in - count of elements for inserting,
	*				out - count of elements that is really stored
	*	wait :   	in - count of milliseconds for wait
	*  return : 0 - success, not zero - error code of this template or Tsyncobj
	*/
	template<typename TInIt, typename Rep, typename Period>
	bufusage_prec_t push_with_move(
		TInIt first, TInIt last, typename const std::chrono::duration<Rep, Period> &wait ) {

		const auto count = std::distance( first, last );
		if( count ) {
			lck_scp_t lock( lock_ );
			bufusage_prec_t r;

			//  wait place(in the buffer) for insert
			const auto gap = sem_wait_inc( lock, wait );
			if( gap < count ) {
				r.errc = bufusage_prec_t::codes_t::ERR_TIME_OUT;
				return r;
				}

			r = obj_.push_with_move( first, last );
			if( r ) {
				CBL_VERIFY( r.usedElements == count );
				sem_inc( r.usedElements );
				}

			return r;
			}
		else {
			bufusage_prec_t r( bufusage_prec_t::codes_t::ERR_SUCCESS );
			return r;
			}
		}

	/**
	* insert to tail of the buffer, wait for insert
	*	msg : pointer to source buffer
	*	bufcnt : 	in - count of elements for inserting,
	*				out - count of elements that is really stored
	*	wait :   	in - count of milliseconds for wait
	*  return : 0 - success, not zero - error code of this template or Tsyncobj
	*/
	template<typename Rep, typename Period, typename XValue>
	bufusage_prec_t push( XValue && obj,
		typename const std::chrono::duration<Rep, Period> &wait ) {

		lck_scp_t lock( lock_ );
		bufusage_prec_t r;

		//  wait place(in the buffer) for insert
		const auto gap = sem_wait_inc( lock, wait );
		if( !gap ) {
			r.errc = bufusage_prec_t::codes_t::ERR_TIME_OUT;
			return r;
			}

		r = obj_.push( std::forward<XValue>( obj ) );
		if( r ) {
			CBL_VERIFY( r.usedElements == 1 );
			sem_inc( r.usedElements );
			}

		return r;
		}

	template<typename Rep, typename Period, typename TCtr, typename ... TArgs>
	bufusage_prec_t push_emplace( typename const std::chrono::duration<Rep, Period> &wait,
		typename TCtr && ctr,
		typename TArgs && ... args ) {

		lck_scp_t lock( lock_ );
		bufusage_prec_t r;

		//  wait place(in the buffer) for insert
		const auto gap = sem_wait_inc( lock, wait );
		if( !gap ) {
			r.errc = bufusage_prec_t::codes_t::ERR_TIME_OUT;
			return r;
			}

		r = obj_.push_emplace( std::forward<TCtr>(ctr), std::forward<TArgs>( args )... );
		if( r ) {
			CBL_VERIFY( r.usedElements == 1 );
			sem_inc( r.usedElements );
			}

		return r;
		}

	/**
	* get first message from the buffer, wait for get
	* 	msg : destination buffer
	* 	bufcnt :	in - size of buffer,
	* 				out - count messages than is really read from the object
	* 	wait : count of milliseconds for wait
	* 	return : 0 - success, not zero - error code of this template or Tsyncobj
	*/
	template<typename TInOut,
		typename Rep, 
		typename Period,
		typename std::enable_if_t<srlz::detail::is_iterator_v<TInOut>, int> = 0>
	bufusage_prec_t pop(
		TInOut first, 
		TInOut last, 
		TInOut &next,
		typename const std::chrono::duration<Rep, Period> &wait ) {

		lck_scp_t lock( lock_ );
		bufusage_prec_t r;

		if( !sem_wait_dec( lock, wait ) ) {
			r.errc = bufusage_prec_t::codes_t::ERR_EMPTY;
			return r;
			}

		r = obj_.pop( first, last, next );
		if( r ) {
			CBL_VERIFY( r.usedElements == std::distance( first, next ) );
			sem_dec( r.usedElements );
			}

		return r;
		}

private:
	template<typename TOutputBufferTimed>
	void pop_timed( TOutputBufferTimed && buffer ){

		lck_scp_t lock( lock_ );
		while( buffer.ready() && !buffer.canceled() ){

			/* the next portion */
			auto itBegin = buffer.next_begin();
			auto itEnd = buffer.next_end();
			auto itNext = itBegin;

			while( itBegin != itEnd && !buffer.canceled() ){

				/* wait ready block */
				if( buffer.writed_count() ){
					while( !sem_wait_dec( lock, buffer.timeout_quant() ) && !buffer.canceled() );
					}
				else{
					if( !sem_wait_dec( lock, buffer.timeout() ) ){
						return;
						}
					}

				auto r = obj_.pop( itBegin, itEnd, itNext );
				CBL_VERIFY(r.usedElements == static_cast<size_t>(std::distance(itBegin, itNext)));

				buffer.set_output_writed_count( r.usedElements );

				itBegin = itNext;
				sem_dec( r.usedElements );
				}
			}

		CBL_VERIFY( !buffer.ready() || buffer.canceled() || buffer.empty() );
		}

	template<typename TOutputBufferTimed>
	void pop_canceled( TOutputBufferTimed && buffer ){

		lck_scp_t lock( lock_ );
		while( buffer.ready() && !buffer.canceled() ){

			/* the next portion */
			auto itBegin = buffer.next_begin();
			auto itEnd = buffer.next_end();
			auto itNext = itBegin;

			while( itBegin != itEnd && !buffer.canceled() ){

				if( sem_wait_dec( lock ) ){

					auto r = obj_.pop( itBegin, itEnd, itNext );
					CBL_VERIFY( r.usedElements == std::distance( itBegin, itNext ) );

					buffer.set_output_writed_count( r.usedElements );

					itBegin = itNext;
					sem_dec( r.usedElements );
					}
				else{
					if( buffer.canceled() ){
						return;
						}
					}
				}
			}

		CBL_VERIFY( !buffer.ready() || buffer.canceled() || buffer.empty() );
		}

	template<typename>
	struct fail_pop_definition : std::false_type{};

public:
	template<typename TOutputBuffer,
		typename std::enable_if_t<is_output_buffer_v<TOutputBuffer>, int> = 0>
	void pop( TOutputBuffer & buffer ){

		if constexpr( is_break_timed_and_cancel_v <TOutputBuffer> ){
			pop_timed( buffer );
			}
		else{
			if constexpr( is_break_cancel_v<TOutputBuffer> ){
				pop_canceled( buffer );
				}
			else{
				static_assert(fail_pop_definition<TOutputBuffer>::value, __FILE_LINE__);
				}
			}
		}

	template<typename TContainer, 
		typename Rep, 
		typename Period,
		typename std::enable_if_t<srlz::detail::is_container_v<TContainer>, int> = 0>
	bufusage_prec_t pop(
		TContainer & to,
		typename const std::chrono::duration<Rep, Period> &wait) {

		bufusage_prec_t r;

		if (!to.empty()) {

			lck_scp_t lock(lock_);

			auto stCount = sem_wait_dec(lock, wait);
			if (!stCount) {
				r.errc = bufusage_prec_t::codes_t::ERR_EMPTY;
				return r;
				}

			auto next = to.begin();
			r = obj_.pop(to.begin(), to.end(), next);
			if (r) {
				CBL_VERIFY(r.usedElements == std::distance(to.begin(), next));
				sem_dec(r.usedElements);
				}
			}
		else {
			r.errc = bufusage_prec_t::codes_t::ERR_OVERFLOW;
			}

		return r;
		}

	template<typename Rep, typename Period>
	bufusage_prec_t pop( value_type & dest,
		typename const std::chrono::duration<Rep, Period> &wait ) {

		lck_scp_t lock( lock_ );
		bufusage_prec_t r;

		if( !sem_wait_dec( lock, wait ) ) {
			r.errc = bufusage_prec_t::codes_t::ERR_EMPTY;
			return r;
			}

		r = obj_.pop( dest );
		if( r ) {
			CBL_VERIFY( r.usedElements == 1 );
			sem_dec( r.usedElements );
			}

		return r;
		}

	/**
	* sync object is empty
	*/
	bool empty()const {
		lck_scp_t lock( lock_ );
		return obj_.empty();
		}

	size_t size()const {
		lck_scp_t lock( lock_ );
		return obj_.size();
		}

	/*! ����� ������� ������� */
	size_t ready_count()const noexcept{
		return obj_.ready_count();
		}

	size_t free_space_count()const noexcept{
		return max_count() - stored_count();
		}
	};
}


