#include "../../internal.h"
#include "../bufdef.h"
#include <vector>


using namespace crm;


static_assert(is_wait_cond_pred_v<std::function<bool()>>, __FILE_LINE__);


static_assert(is_break_cancel_v<decltype(make_break_cancel(std::function<bool()>{}))> , __FILE_LINE__);
static_assert(is_break_cancel_v<break_cancel>, __FILE_LINE__);

using compile_test_break_timed = break_timed<std::chrono::milliseconds::rep, std::chrono::milliseconds::period>;
static_assert(is_break_timed_v <compile_test_break_timed>, __FILE_LINE__);
static_assert(std::is_same_v< compile_test_break_timed, 
	std::decay_t<decltype(make_break_timed(std::chrono::milliseconds{}, std::chrono::milliseconds{})) >>, __FILE_LINE__);

static_assert(is_break_timed_v<decltype(make_break_timed(std::chrono::milliseconds{}, std::chrono::milliseconds{}))>, __FILE_LINE__);


using compile_test_input_iterator_reader = input_iterator_reader<decltype(std::declval<std::vector<uint8_t>>().cbegin()),
	decltype(make_break_timed(std::chrono::milliseconds{}, std::chrono::milliseconds{}))>;
static_assert(is_break_timed_v<compile_test_input_iterator_reader>, __FILE_LINE__);
static_assert(is_break_conditional_v<compile_test_input_iterator_reader>, __FILE_LINE__);


using compile_test_input_iterator_reader_cancel = input_iterator_reader<decltype(std::declval<std::vector<uint8_t>>().cbegin()),
	decltype(make_break_cancel(std::function<bool()>{}))>;
static_assert(is_break_cancel_v<compile_test_input_iterator_reader_cancel>, __FILE_LINE__);
static_assert(is_break_conditional_v<compile_test_input_iterator_reader_cancel>, __FILE_LINE__);


using compile_test_break_cancel_timed = break_cancel_timed<std::chrono::milliseconds::rep, std::chrono::milliseconds::period>;
static_assert(is_break_timed_and_cancel_v<compile_test_break_cancel_timed>, __FILE_LINE__);
static_assert(is_break_cancel_v<compile_test_break_cancel_timed>, __FILE_LINE__);
static_assert(is_break_timed_v<compile_test_break_cancel_timed>, __FILE_LINE__);
static_assert(is_break_conditional_v<compile_test_break_cancel_timed>, __FILE_LINE__);


using mkcb = decltype(make_break_cancel_timed(std::function<bool()>{}, std::chrono::milliseconds{}, std::chrono::milliseconds{}));
static_assert(std::is_same_v<compile_test_break_cancel_timed, 
	decltype(make_break_cancel_timed(std::function<bool()>{}, std::chrono::milliseconds{}, std::chrono::milliseconds{})) > , __FILE_LINE__);

using compile_test_input_iterator_reader_cancel_timed = input_iterator_reader<decltype(std::declval<std::vector<uint8_t>>().cbegin()),
	decltype(make_break_cancel_timed(std::function<bool()>{}, std::chrono::milliseconds{}, std::chrono::milliseconds{}))>;

std::vector<char> vc;
compile_test_input_iterator_reader_cancel_timed ck(make_break_cancel_timed([]{return false; }, std::chrono::milliseconds{}, std::chrono::milliseconds{}));
auto t = ck.timeout();
static_assert(is_iterator_reader_v<decltype(ck)>, __FILE_LINE__);

static_assert(is_break_timed_and_cancel_v<compile_test_input_iterator_reader_cancel_timed>, __FILE_LINE__);
static_assert(is_break_cancel_v<compile_test_input_iterator_reader_cancel_timed>, __FILE_LINE__);
static_assert(is_break_timed_v<compile_test_input_iterator_reader_cancel_timed>, __FILE_LINE__);
static_assert(is_break_conditional_v<compile_test_input_iterator_reader_cancel_timed>, __FILE_LINE__);

static_assert(is_iterator_reader_v<compile_test_input_iterator_reader_cancel_timed>, __FILE_LINE__);


using outbuffer_timed = output_container_writer < std::vector<uint8_t>,
	decltype(make_break_timed(std::chrono::milliseconds{}, std::chrono::milliseconds{})) > ;
static_assert(is_break_timed_v<outbuffer_timed>, __FILE_LINE__);
static_assert(is_break_conditional_v<outbuffer_timed>, __FILE_LINE__);


using outbuffer_canceled = output_container_writer < std::vector<uint8_t>,
	decltype(make_break_cancel(std::function<bool()>{})) > ;
static_assert(is_break_cancel_v<outbuffer_canceled>, __FILE_LINE__);
static_assert(is_break_conditional_v<outbuffer_canceled>, __FILE_LINE__);


using outbuffer_canceled_timed = output_container_writer < std::vector<uint8_t>,
	decltype(make_break_cancel_timed(std::function<bool()>{}, std::chrono::milliseconds{}, std::chrono::milliseconds{})) > ;
using t1 = decltype(std::declval<outbuffer_canceled_timed>().token_break());
//t1 a1;
static_assert(is_break_conditional_v<decltype(std::declval<outbuffer_canceled_timed>().token_break())>, __FILE_LINE__);
static_assert(is_break_timed_and_cancel_v<outbuffer_canceled_timed>, __FILE_LINE__);
static_assert(is_break_cancel_v<outbuffer_canceled_timed>, __FILE_LINE__);
static_assert(is_break_timed_v<outbuffer_canceled_timed>, __FILE_LINE__);
static_assert(is_break_conditional_v<outbuffer_canceled_timed>, __FILE_LINE__);

using binary_blob_reader_cancel_timed = binary_reader < std::vector<uint8_t>, 
	decltype(make_break_cancel_timed(std::function<bool()>{}, std::chrono::milliseconds{}, std::chrono::milliseconds{}))>;
static_assert(is_iterator_reader_v<binary_blob_reader_cancel_timed>, __FILE_LINE__);

static_assert(is_output_buffer_v<outbuffer_canceled_timed>, __FILE_LINE__);


bufusage_prec_t::bufusage_prec_t( codes_t code_ )noexcept
	: usedElements( 0 ), errc( code_ ) {}

bufusage_prec_t::bufusage_prec_t( codes_t code_, size_t usedElements_ )noexcept
	: usedElements{ usedElements_ }
	, errc{ code_ }{}

bufusage_prec_t::operator bool()const noexcept{
	return errc == codes_t::ERR_SUCCESS;
	}

bufusage_prec_t& bufusage_prec_t::operator = (codes_t value) noexcept{
	errc = value;
	return (*this);
	}

bool bufusage_prec_t::is_empty()const noexcept{
	return errc == codes_t::ERR_EMPTY;
	}

const char* bufusage_prec_t::errc_str( const codes_t code ) noexcept{
	switch( code ) {
		case codes_t::ERR_EMPTY: return "empty";
		case codes_t::ERR_OVERFLOW: return "overflow";
		case codes_t::ERR_SUCCESS: return "success";
		case codes_t::ERR_TIME_OUT: return "timeout";
		default: break;
		}

	return "undefined";
	}

