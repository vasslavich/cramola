#pragma once


#include <atomic>
#include <chrono>
#include <mutex>
#include "../exceptions.h"
#include "./functors.h"



namespace crm{
namespace detail{


template<
	typename Tcritical,
	typename Tcritical_scope,
	typename Tcondvar,
	typename Tsyncobj,
	typename TsyncobjFunctor = functor_container_queue_t<Tsyncobj>>
class mt_queue_t {
public:
	typedef typename Tsyncobj container_t;
	typedef typename std::decay_t<container_t>::value_type value_type;
	typedef typename Tcritical lck_t;
	typedef typename Tcritical_scope lck_scp_t;
	typedef typename Tcondvar cndvar_t;
	typedef typename TsyncobjFunctor functor_t;

private:
	/*! ������� ��������� */
	container_t _container;
	/*! ���������� */
	lck_t _lock;
	/*! ������ "���� ����� ��� �������" */
	cndvar_t _condInc;
	/*! ������ "�� ������" ������� */
	cndvar_t _condDec;
	/*! ������� ����� ��������� ������� */
	std::atomic<std::intmax_t> _nStored = 0;
	/*! ����������� ��������� ����� ��������� � ���������� */
	std::uintmax_t _maxCount = 0;

	void _clear() {
		functor_t::clear( _container );
		_nStored.store( 0, std::memory_order::memory_order_release );

		_condInc.notify_all();
		}

	void reset() {
		_clear();
		}

	template<typename Rep, typename Period>
	size_t sem_wait_inc( lck_scp_t &lock,
		typename const std::chrono::duration<Rep, Period> &waitTime ) {

		CBL_VERIFY( lock.owns_lock() );
		size_t stValue = 0;
		if( _condInc.wait_for( lock, waitTime, [this, &stValue] {
			if( max_count() < stored_count() )
				FATAL_ERROR_FWD(nullptr);

			stValue = max_count() - stored_count();
			return stValue != 0;
			} ) ) {

			CBL_VERIFY( (stored_count() <= max_count() && stored_count() >= 0) );
			CBL_VERIFY( lock.owns_lock() );

			return stValue;
			}
		else
			return 0;
		}

	template<typename Rep, typename Period>
	size_t sem_wait_dec( lck_scp_t &lock,
		typename const std::chrono::duration<Rep, Period> &waitTime ) {

		CBL_VERIFY( lock.owns_lock() );
		size_t stValue = 0;
		if( _condDec.wait_for( lock, waitTime, [this, &stValue] {
			stValue = stored_count();
			return 0 != stValue;
			} ) ) {

			CBL_VERIFY( lock.owns_lock() );
			return stValue;
			}
		else
			return 0;
		}

	void sem_inc() {
		stored_inc(1);
		_condDec.notify_one();
		}

	void sem_dec() {
		CBL_VERIFY( (size_t)stored_count() >= 1 );

		stored_dec(1);
		CBL_VERIFY( stored_count() >= 0 );

		_condInc.notify_one();
		}

public:
	~mt_queue_t() {
		CHECK_NO_EXCEPTION(_clear());
		}

	std::uintmax_t max_count()const {
		return _maxCount;
		}

	size_t stored_count() {
		const auto value = _nStored.load( std::memory_order::memory_order_acquire );
		if( value < 0 )
			FATAL_ERROR_FWD(nullptr);

		return (size_t)value;
		}

	void stored_dec(std::intmax_t count) {
		if( _nStored.fetch_sub( count ) <= 0 )
			FATAL_ERROR_FWD(nullptr);
		}

	void stored_inc(std::intmax_t count) {
		_nStored.fetch_add( count );
		}

private:
	void copy( mt_queue_t &&o ) {
		if( this != std::addressof(o) ) {
			lck_scp_t lock_1( src._lock, std::defer_lock );
			lck_scp_t lock_2( _lock, std::defer_lock );
			std::lock( lock_1, lock_2 );

			_container = std::move( o._container );
			_nStored = std::move( o._nStored );
			_maxCount = std::move( o._maxCount );

			o.reset();
			}
		}

	void copy( const mt_queue_t &o ) {
		if( this != &o ) {
			lck_scp_t lock_1( src._lock, std::defer_lock );
			lck_scp_t lock_2( _lock, std::defer_lock );
			std::lock( lock_1, lock_2 );

			_container = o._container;
			_nStored = o._nStored;
			_maxCount = o._maxCount;
			}
		}

public:
	mt_queue_t()noexcept {}

	template<typename XContainer, 
		typename XCondVar,
		typename XMytex>
		mt_queue_t(XContainer && c,
			XCondVar && cvInc,
			XCondVar && cvDec,
			XMytex && lck,
			std::size_t maxCount)
		: _container(std::forward<XContainer>(c))
		, _lock(std::forward<XMytex>(lck))
		, _condInc(std::forward<XCondVar>(cvInc))
		, _condDec(std::forward<XCondVar>(cvDec))
		, _nStored{0}
		, _maxCount{maxCount}{}

	mt_queue_t(std::size_t maxCount)
		: _nStored( 0 )
		, _maxCount(maxCount){}

	mt_queue_t( const mt_queue_t& src ) {
		copy( src );
		}

	mt_queue_t( mt_queue_t && src ) {
		copy( std::move( src ) );
		}

	mt_queue_t& operator=(const mt_queue_t &src) {
		if(this != std::addressof(src)) {
			copy(src);
			}

		return (*this);
		}

	mt_queue_t& operator=(mt_queue_t &&src) {
		if(this != std::addressof(src)) {
			copy(std::move(src));
			}

		return (*this);
		}

	void clear(){
		lck_scp_t lock( _lock );
		_clear();
		}

	/*! ������� �������� */
	template<typename Rep, typename Period, typename XValue>
	bool push(XValue && item,
		typename const std::chrono::duration<Rep, Period> &wait ) {

		lck_scp_t lock( _lock );
		if( sem_wait_inc( lock, wait ) ) {

			// ������� �������� � ������
			functor_t::push(_container, std::forward< XValue>(item));

			// ������ ��������� �������
			sem_inc();

			return true;
			}
		else {
			return false;
			}
		}

	template<typename Rep, typename Period, typename TCtr, typename ... TArgs>
	bool push_emplace( typename const std::chrono::duration<Rep, Period> &wait, 
		typename TCtr && ctr, typename TArgs && ... args ) {

		lck_scp_t lock( _lock );
		if( sem_wait_inc( lock, wait ) ) {

			/* �������� ������� � ������� �������� */
			value_type obj( ctr( std::forward<TArgs>( args )... ) );

			// ������� �������� � ������
			functor_t::emplace( _container, std::move(obj) );

			// ������ ��������� �������	
			sem_inc();

			return true;
			}
		else {
			return false;
			}
		}

	template<typename Rep, typename Period, typename TreadyCond>
	bool pop(
		value_type & obj,
		typename const std::chrono::duration<Rep, Period> &wait,

		/* ����������� �������, ���� �� ������������� ������� */
		typename TreadyCond cond = []( const value_type & /*ehs*/ ) { return true; } ) {


		lck_scp_t lock( _lock );
		if( sem_wait_dec(lock,wait ) ) {

			/* �������������� ������� */
			if( cond( functor_t::front( _container ) ) ) {

				/* ������ �������� */
				obj = std::move( functor_t::front( _container ) );

				/* �������� �� ����� */
				CBL_VERIFY( _container.size() );
				functor_t::pop( _container );

				/* ����������� */
				sem_dec();

				return true;
				}
			else {
				return false;
				}
			}
		else {
			return false;
			}
		}

	template<typename Rep, typename Period >
	bool pop(
		value_type & dest,
		typename const std::chrono::duration<Rep, Period> &wait ) {

		return pop( dest, wait, []( const value_type & /*ehs*/ ) { return true; } );
		}

	template<typename Rep, typename Period,
		typename TreadyCond, typename TmoveFunctor>
	bool pop(
		typename TmoveFunctor & destGrap,
		typename const std::chrono::duration<Rep, Period> &wait,

		/* ������� ���������� �������� */
		typename TreadyCond cond ) {

		lck_scp_t lock( _lock );
		if( sem_wait_dec( lock, wait ) ){

			/* �������������� ������� */
			if( cond( functor_t::front( _container ) ) ) {

				/* ������ �������� */
				destGrap( std::move( functor_t::front( _container ) ) );

				/* �������� �� ���������� */
				CBL_VERIFY( _container.size() );
				functor_t::pop( _container );

				/* ����������� */
				sem_dec();

				return true;
				}
			else {
				return false;
				}
			}
		else {
			return false;
			}
		}

	bool empty()const {
		lck_scp_t lock( _lock );
		return functor_t::empty( _container );
		}

	size_t size()const {
		lck_scp_t lock( _lock );
		return functor_t::size( _container );
		}
	};
}
}


