#ifndef DCF_MT_DYNAMIC_PUSHPOP_ADPT_HEADER
#define	DCF_MT_DYNAMIC_PUSHPOP_ADPT_HEADER


#include <atomic>
#include <chrono>
#include <mutex>
#include "../internal_invariants.h"
#include "./functors.h"


namespace crm{
namespace detail{


template<
	typename Tcritical, 
	typename Tcritical_scope, 
	typename Tcondvar, 
	typename Tsyncobj,
	typename TsyncobjFunctor = functor_container_queue_t<Tsyncobj >>
class mt_dynamic_queue_t {
public:
	typedef typename Tsyncobj object_t;
	typedef typename object_t::value_type value_type;
	typedef typename Tcritical lck_t;
	typedef typename Tcritical_scope lck_scp_t;
	typedef typename Tcondvar cndvar_t;
	typedef typename TsyncobjFunctor functor_t;

private:
	/// buffer of data
	object_t _container;
	/// count of stored values
	std::atomic<std::intmax_t> _nStored = 0;
	/*! ���������� */
	mutable lck_t _lock;
	/*! ������ "�� ������" ������� */
	cndvar_t _condDec;
	/*! ������������� ������� */
	std::string _objectId;

#ifdef CBL_QUEUE_SIZE_CHECKER
	/*! ������������ ������������ ����� ������� */
	std::atomic<std::intmax_t> _lastMaxStored = 0;
#endif

	void _clear() {
		functor_t::clear( _container );
		_nStored.store( 0, std::memory_order::memory_order_release );

#ifdef CBL_QUEUE_SIZE_CHECKER
		_lastMaxStored.store( 0, std::memory_order::memory_order_release );
#endif
		}

	void reset() {
		_clear();
		}

	template<typename Rep, typename Period>
	size_t sem_wait_dec( lck_scp_t &lock,
		typename const std::chrono::duration<Rep, Period> &waitTime ) {

		CBL_VERIFY( lock.owns_lock() );
		size_t stValue = 0;
		if( _condDec.wait_for( lock, waitTime, [this, &stValue] {
			stValue = stored_count();
			return 0 != stValue;
			} ) ) {

			CBL_VERIFY( lock.owns_lock() );
			return stValue;
			}
		else
			return 0;
		}

	void sem_dec(std::intmax_t count) {

		if( count > 0 ) {
			CBL_VERIFY( stored_count() >= (size_t)count );

			stored_dec( (size_t)count );
			CBL_VERIFY( stored_count() >= 0 );
			}
		else if( count < 0 ) {
			FATAL_ERROR_FWD( nullptr );
			}
		}

	void sem_dec() {
		sem_dec( 1 );
		}

	void sem_inc() {

		stored_inc( 1 );
		_condDec.notify_one();
		}

	void stored_dec(std::intmax_t count ) {
		const auto prev = _nStored.fetch_sub( count );

#if(CBL_CHECK_LEVEL_TRAIT == CBL_CHECK_LEVEL_TRAIT_MOST_HARD)
		if( prev <= 0 )
			FATAL_ERROR_FWD(nullptr);
#endif
		}

	void stored_inc( std::intmax_t count ) {
#ifdef CBL_QUEUE_SIZE_CHECKER
		const auto lastMax = _lastMaxStored.load(std::memory_order::memory_order_acquire);
#endif

		const auto stVal = _nStored.fetch_add( count );

		/* compiler only */
		stVal;

#ifdef CBL_QUEUE_SIZE_CHECKER
		if( stVal > lastMax ) {
			_lastMaxStored.store( stVal, std::memory_order::memory_order_release);
			if( lastMax && ( lastMax % CBL_TRACE_OBJECTS_COUNTER_STEP ) == 0 ) {

				std::string log( SIZE_SEQUENCE_FWDARNING );
				log += ":" + _objectId + ":" + std::to_string( stVal );
				crm::file_logger_t::logging( log, 0, __CBL_FILEW__, __LINE__ );
				}
			}
		else {
			if((lastMax - stVal) >= CBL_TRACE_OBJECTS_COUNTER_STEP) {
				_lastMaxStored.store(stVal, std::memory_order::memory_order_release);
				}
			}
#endif
		}

public:
	size_t stored_count() {
		const auto value = _nStored.load( std::memory_order::memory_order_acquire );
		if( value < 0 )
			FATAL_ERROR_FWD(nullptr);

		return (size_t)value;
		}

private:
	void copy( mt_dynamic_queue_t &&o ) {
		if( this != &o ) {
			lck_scp_t lock_1( src._lock, std::defer_lock );
			lck_scp_t lock_2( _lock, std::defer_lock );
			std::lock( lock_1, lock_2 );

			_container = std::move( o._container );
			_nStored = std::move( o._nStored );
			_objectId = std::move( o._objectId );

			o.reset();
			}
		}

	void copy( const mt_dynamic_queue_t &o ) {
		if( this != &o ) {
			lck_scp_t lock_1( src._lock, std::defer_lock );
			lck_scp_t lock_2( _lock, std::defer_lock );
			std::lock( lock_1, lock_2 );

			_container = o._container;
			_nStored = o._nStored;
			_objectId = o._objectId;
			}
		}

public:
	mt_dynamic_queue_t(const std::string & objectId) 
		: _objectId(objectId){
		
		if( _objectId.empty() ) {
			FATAL_ERROR_FWD(nullptr);
			}
		}

	mt_dynamic_queue_t( const mt_dynamic_queue_t& src ) {
		copy( src );
		}

	mt_dynamic_queue_t( mt_dynamic_queue_t&& src ) {
		copy( std::move( src ) );
		}

	mt_dynamic_queue_t& operator=(const mt_dynamic_queue_t &src) {
		if(this != std::addressof(src)) {
			copy(src);
			}

		return (*this);
		}

	mt_dynamic_queue_t& operator=(mt_dynamic_queue_t &&src) {
		if(this != std::addressof(src)) {
			copy(std::move(src));
			}

		return (*this);
		}

	void clear() {
		lck_scp_t lock( _lock );
		_clear();
		}

	/*! ������� �������� */
	void push( const value_type & item ) {

		lck_scp_t lock( _lock );

		// ������� ��������� � ������
		functor_t::push( _container, item );	
		// ������ ��������� �������
		sem_inc();

		CBL_VERIFY( stored_count() == functor_t::size( _container ) );
		}

	/*! ������� �������� */
	void push( value_type && item ) {

		lck_scp_t lock( _lock );

		// ������� ��������� � ������
		functor_t::push( _container, std::forward<value_type>( item ) );
		// ������ ��������� �������	
		sem_inc();

		CBL_VERIFY( stored_count() == functor_t::size( _container ) );
		}

	template<typename ... TArgs>
	void push_emplace( typename TArgs && ... args ) {
		value_type obj( std::forward<TArgs>( args )... );
		push( std::move( obj ) );
		}

	template<typename TCtr, typename ... TArgs>
	void push_emplace_ctr( typename TCtr && ctr, typename TArgs && ... args ) {
		value_type obj( ctr( std::forward<TArgs>( args )... ) );
		push( std::move( obj ) );
		}

	template<typename Rep, typename Period, typename TreadyCond>
	bool pop(
		value_type & obj,
		typename const std::chrono::duration<Rep, Period> &wait,

		/* ����������� �������, ���� �� ������������� ������� */
		typename TreadyCond cond = []( const value_type & /*ehs*/ ) { return true; } ) {

		bool result = false;
	
		lck_scp_t lock( _lock );
		if( sem_wait_dec( lock, wait ) ) {
			CBL_VERIFY( stored_count() == functor_t::size( _container ) );

			/* �������������� ������� */
			if( cond( functor_t::front( _container ) ) ) {

				/* ������ �������� */
				obj = std::move( functor_t::front( _container ) );
				/* �������� �� ����� */
				functor_t::pop( _container );
				/* ����������� */
				sem_dec();

				CBL_VERIFY( stored_count() == functor_t::size( _container ) );

				result = true;
				}
			}

		return result;
		}

	template<typename Rep, typename Period, typename TreadyCond>
	std::list<value_type> pop( typename const std::chrono::duration<Rep, Period> &wait,

		/* ����������� �������, ���� �� ������������� ������� */
		typename TreadyCond cond = []( const value_type & /*ehs*/ ) { return true; } ) {

		std::list<value_type> result;

		lck_scp_t lock( _lock );
		const auto stCount = sem_wait_dec( lock, wait );
		if( stCount ) {
			CBL_VERIFY( stored_count() == stCount && stCount == functor_t::size( _container ) );

			bool condf = false;
			size_t decCount = 0;

			do {
				/* �������������� ������� */
				condf = cond( functor_t::front( _container ) );
				if( condf ) {

					/* ������ �������� */
					auto obj = std::move( functor_t::front( _container ) );
					/* � ������� ��������� */
					result.push_back( std::move( obj ) );
					/* �������� �� ����� */
					functor_t::pop( _container );

					++decCount;
					}
				} 
			while( condf && decCount < stCount );

			if( decCount > 0 ) {

				sem_dec( decCount );
				CBL_VERIFY( stored_count() == functor_t::size( _container ) );
				}
			}

		return std::move(result);
		}

	template<typename TreadyCond>
	bool pop( value_type & obj,

		/* ����������� �������, ���� �� ������������� ������� */
		typename TreadyCond cond = []( const value_type & /*ehs*/ ) { return true; } ) {

		bool result = false;

		lck_scp_t lock( _lock );
		if( stored_count() ) {
			CBL_VERIFY( stored_count() == functor_t::size( _container ) );

			/* �������������� ������� */
			if( cond( functor_t::front( _container ) ) ) {

				/* ������ �������� */
				obj = std::move( functor_t::front( _container ) );
				/* �������� �� ����� */
				functor_t::pop( _container );
				/* ����������� */
				sem_dec();

				CBL_VERIFY( stored_count() == functor_t::size( _container ) );

				result = true;
				}
			}

		return result;
		}

	template<typename Rep, typename Period >
	bool pop(
		value_type & dest,
		typename const std::chrono::duration<Rep, Period> &wait ) {

		return pop( dest, wait, []( const value_type & /*ehs*/ ) { return true; } );
		}

	template<typename Rep, typename Period>
	std::list<value_type> pop( typename const std::chrono::duration<Rep, Period> &wait ) {

		return pop( wait, []( const value_type & /*ehs*/ ) { return true; } );
		}

	template<typename Rep, typename Period,
		typename TreadyCond, typename TmoveFunctor>
	bool pop(
		typename TmoveFunctor & destGrap,
		typename const std::chrono::duration<Rep, Period> &wait,

		/* ������� ���������� �������� */
		typename TreadyCond cond){

		bool result = false;

		lck_scp_t lock( _lock );
		if( sem_wait_dec( lock, wait ) ) {
			CBL_VERIFY( stored_count() == functor_t::size( _container ) );

			/* ������������� ������� */
			if( cond( functor_t::front( _container ) ) ) {

				/* ������ �������� */
				destGrap( std::move( functor_t::front( _container ) ) );
				/* �������� �� ���������� */
				functor_t::pop( _container );
				/* ����������� */
				sem_dec();

				CBL_VERIFY( stored_count() == functor_t::size( _container ) );

				result = true;
				}
			}

		return result;
		}

	bool empty()const {
		lck_scp_t lock( _lock );
		return functor_t::empty( _container );
		}

	size_t size()const {
		lck_scp_t lock( _lock );
		return functor_t::size( _container );
		}

	template<typename TElemConverter>
	auto iterate_begin2end( TElemConverter & cvt )const
	->std::list<std::decay_t<decltype(cvt( std::declval<value_type>() ))>> {

		if( detail::is_container_iterable_t<object_t>::value ) {
			using result_t = std::list<std::decay_t<decltype(cvt( std::declval<value_type>() ))>>;
			result_t rl;

			lck_scp_t lock( _lock );

			auto it = functor_t::cbegin( _container );
			auto end = functor_t::cend( _container );

			for( ; it != end; ++it ) {
				rl.emplace_back( cvt( (*it) ) );
				}

			return rl;
			}
		else {
			FATAL_ERROR_FWD(nullptr);
			}
		}
	};
}
}
#endif	/// DCF_MT_DYNAMIC_PUSHPOP_ADPT_HEADER

