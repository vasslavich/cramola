#pragma once


#include <atomic>
#include "../utilities/scopedin/postactor.h"
#include "./bufdef.h"
#include "../internal_invariants.h"
#include "../exceptions.h"
#include "./functors.h"
#include "../typeframe/internal_terms.h"


namespace crm{
namespace detail{


struct ringbuf_curr_state_t{
	std::size_t Ready{ 0 };
	std::size_t Tail{ 0 };
	std::size_t Head{ 0 };
	};


template<typename Container,
	typename ContainerFunctor = ringbuf_ctr_container_functor_t<Container>,
	const bool isContainerAsRef = std::is_reference_v<Container>>
	struct ringbuf_handler{
	using container_t = typename Container;
	using functor_t = typename ContainerFunctor;
	using const_iterator = typename functor_t::const_iterator;
	using iterator = typename functor_t::iterator;
	using size_type = typename functor_t::size_type;
	using value_type = typename std::decay_t<Container>::value_type;

	/*! ��������� �������� */
	container_t _buf;
	/// count of a ready elements for reading
	std::atomic<size_type> _nReady{ 0 };
	/// tail position(first position of ready elements)
	std::atomic<size_type> _nTail{ 0 };
	/// head position(first position for insert of a next elements)
	std::atomic<size_type> _nHead{ 0 };

	~ringbuf_handler()noexcept{}


	template<typename XContainer,
		typename std::enable_if_t < std::is_constructible_v<Container, XContainer>, int> = 0>
		ringbuf_handler( XContainer && container )
		: _buf( std::forward<XContainer>( container ) ){

		if constexpr( isContainerAsRef ){
			static_assert(std::is_reference_v<decltype(_buf)>, __FILE_LINE__);
			static_assert(std::is_reference_v<XContainer>, __FILE_LINE__);
			}
		}

	template<typename ... TContainerArgs,
		typename std::enable_if_t<(sizeof ... (TContainerArgs) > 1), int> = 0>
		ringbuf_handler( typename TContainerArgs && ... containerArgs )
		: _buf( std::forward<TContainerArgs>( containerArgs )... ){}

	ringbuf_handler()noexcept{}

	ringbuf_handler( const ringbuf_handler& src )
		: _buf( src._buf )
		, _nReady( src._nReady.load( std::memory_order::memory_order_acquire ) )
		, _nTail( src._nTail.load( std::memory_order::memory_order_acquire ) )
		, _nHead( src._nHead.load( std::memory_order::memory_order_acquire ) ){}

	ringbuf_handler( ringbuf_handler && src )noexcept
		: _buf( std::move( src._buf ) )
		, _nReady( src._nReady.load( std::memory_order::memory_order_acquire ) )
		, _nTail( src._nTail.load( std::memory_order::memory_order_acquire ) )
		, _nHead( src._nHead.load( std::memory_order::memory_order_acquire ) ){}

	ringbuf_handler& operator=( const ringbuf_handler & o ){
		assign( o );
		return (*this);
		}

	ringbuf_handler& operator=( ringbuf_handler  && o )noexcept{
		assign( std::move( o ) );
		return (*this);
		}

	void assign( const ringbuf_handler &src ){
		if( this != std::addressof( src ) ){
			_buf = src._buf;

			_nReady = src._nReady.load( std::memory_order::memory_order_acquire );
			_nTail = src._nTail.load( std::memory_order::memory_order_acquire );
			_nHead = src._nHead.load( std::memory_order::memory_order_acquire );
			}
		}

	void assign( ringbuf_handler &&src )noexcept{

		if( this != std::addressof( src ) ){
			_buf = std::move( src._buf );

			_nReady = src._nReady.load(std::memory_order::memory_order_acquire );
			_nTail = src._nTail.load(std::memory_order::memory_order_acquire);
			_nHead = src._nHead.load(std::memory_order::memory_order_acquire);
			}
		}

	void clear()noexcept{
		functor_t::clear( _buf );
		set_state( {} );
		}

	ringbuf_curr_state_t state()const noexcept{
		ringbuf_curr_state_t st;
		st.Ready = _nReady.load( std::memory_order::memory_order_acquire );
		st.Tail = _nTail.load( std::memory_order::memory_order_acquire );
		st.Head = _nHead.load( std::memory_order::memory_order_acquire );
		return std::move( st );
		}

	void set_state( const ringbuf_curr_state_t & st )noexcept{
		_nReady.store( st.Ready, std::memory_order::memory_order_release );
		_nTail.store( st.Tail, std::memory_order::memory_order_release );
		_nHead.store( st.Head, std::memory_order::memory_order_release );
		}

	size_type size()const noexcept{
		return functor_t::size( _buf );
		}

	size_type ready()const noexcept{
		return _nReady.load( std::memory_order::memory_order_acquire );
		}

	void ready_set( size_type c )noexcept{
		CBL_VERIFY( size() >= c );
		_nReady.store( c, std::memory_order::memory_order_release );
		}

	void ready_dec( size_type c )noexcept{
		CBL_VERIFY( ready() >= c );
		_nReady -= c;
		}

	void ready_inc( size_type c )noexcept{
		CBL_VERIFY( size() >= (ready() + c) );
		_nReady += c;
		}

	size_type tail()const noexcept{
		return _nTail.load( std::memory_order::memory_order_acquire );
		}

	void tail_set( size_type c )noexcept{
		CBL_VERIFY( size() >= c );
		_nTail.store( c, std::memory_order::memory_order_release );
		}

	void tail_dec( size_type c )noexcept{
		CBL_VERIFY( tail() >= c );
		_nTail -= c;
		}

	void tail_inc( size_type c )noexcept{
		CBL_VERIFY( size() >= (tail() + c) );
		_nTail += c;
		}

	size_type head()const noexcept{
		return _nHead.load( std::memory_order::memory_order_acquire );
		}

	void head_set( size_type c )noexcept{
		CBL_VERIFY( size() >= c );
		_nHead.store( c, std::memory_order::memory_order_release );
		}

	void head_dec( size_type c )noexcept{
		CBL_VERIFY( head() >= c );
		_nHead -= c;
		}

	void head_inc( size_type c )noexcept{
		CBL_VERIFY( size() >= (head() + c) );
		_nHead += c;
		}

	template<typename XValue>
	void write( XValue && v, size_type off ){
		CBL_VERIFY( size() > off );

		auto itIns = std::next( functor_t::begin( _buf ), off );
		(*itIns) = std::forward<XValue>( v );
		}

	template< typename TInIt >
	typename TInIt write( TInIt first_, TInIt last_, size_type off ){
		CBL_VERIFY( functor_t::size( _buf ) > off );

		const auto sourceCount = std::distance( first_, last_ );
		if( sourceCount >= 0 ){

			auto outIt = std::next( functor_t::begin( _buf ), off );
			auto inIt = first_;

			const size_type vcnt = (std::min<size_type>)(sourceCount, functor_t::size( _buf ) - off);
			for( size_type idx = 0; idx < vcnt; ++idx, ++inIt, ++outIt ){
				(*outIt) = std::move( *inIt );
				}

			return inIt;
			}
		else{
			THROW_EXC_FWD(nullptr);
			}
		}

	template<typename TOutIt>
	typename TOutIt read( TOutIt first_, TOutIt last_, size_type off ){
		CBL_VERIFY( functor_t::size( _buf ) > off );

		const auto outSize = std::distance( first_, last_ );
		if( outSize > 0 ){
			const size_type ecount = (std::min<size_type>)(outSize, functor_t::size( _buf ) - off);
			auto inIt = std::next( functor_t::begin( _buf ), off );
			auto outIt = first_;

			for( size_type idx = 0; idx < ecount; ++idx, ++inIt, ++outIt ){
				(*outIt) = std::move( *inIt );
				}

			return outIt;
			}
		else{
			THROW_EXC_FWD(nullptr);
			}
		}

	template<typename XValue>
	void read( XValue & out, size_type off ){
		CBL_VERIFY( functor_t::size( _buf ) > off );

		auto inIt = std::next( functor_t::begin( _buf ), off );
		(out) = std::move( *inIt );
		}

	value_type& front(){
		if( ready() == 0 )
			FATAL_ERROR_FWD(nullptr);

		return *(std::next( functor_t::begin( _buf ), tail() ));
		}

	const value_type& front()const{
		if( ready() == 0 )
			FATAL_ERROR_FWD(nullptr);

		return *(std::next( functor_t::begin( _buf ), tail() ));
		}
	};

template< typename DataHandler>
class ringbuf_ctr_t{
public:
	using self_type = typename ringbuf_ctr_t<DataHandler>;
	using handler_t = std::decay_t<DataHandler>;
	using container_t = typename handler_t::container_t;
	using size_type = typename handler_t::size_type;
	using value_type = typename handler_t::value_type;

private:
	std::string _name;
	DataHandler _h;

	ringbuf_curr_state_t get_state()const{
		return _h.state();
		}

	void set_state( const ringbuf_curr_state_t & st ){
		_h.set_state( st );
		}

public:
	const std::string & name()const noexcept{
		return _name;
		}

	size_type ready_count()const{
		return _h.ready();
		}

	void copy( const self_type &src ){
		if( this != std::addressof( src ) ){
			_name = src._name;
			_h = src._h;
			}
		}

	void copy(self_type &&src )noexcept{
		if( this != std::addressof( src ) ){
			_name = std::move(src._name);
			_h = std::move( src._h );
			}
		}

	void clear(){
		_h.clear();
		}

	template<typename XHandler,
		typename std::enable_if_t < std::is_constructible_v<DataHandler, XHandler>, int> = 0>
		ringbuf_ctr_t( std::string_view name_, XHandler && h )
		: _name(name_)
		, _h( std::forward<XHandler>( h ) ){}

	template<typename ... THandlerArgs,
		typename std::enable_if_t<(sizeof ... (THandlerArgs) > 1) && std::is_constructible_v<DataHandler, THandlerArgs...>, int> = 0>
	ringbuf_ctr_t(std::string_view name_, typename THandlerArgs && ... hArgs )
		: _name(name_)
		, _h( std::forward<THandlerArgs>( hArgs )... ){}

	ringbuf_ctr_t()noexcept{}

	ringbuf_ctr_t( const self_type& src )
		: _name(src._name)
		, _h(src._h){}

	ringbuf_ctr_t(self_type&& src )noexcept
		: _name(std::move(src._name))
		, _h(std::move(src._h)){}

	ringbuf_ctr_t& operator=( const self_type &src ){
		copy( src );
		return (*this);
		}

	ringbuf_ctr_t& operator=(self_type &&src )noexcept{
		copy( std::move( src ) );
		return (*this);
		}

private:
	/*-----------------------------------------------------------------------------
	*
	* 		write operations
	*
	* ----------------------------------------------------------------------------*/
	template< typename TInIt >
	TInIt w_head_to_end( TInIt first, TInIt last ){
		const size_type count = std::distance( first, last );

		CBL_VERIFY( ((_h.size() >= _h.ready()) && (_h.size() - _h.ready()) >= count) );
		CBL_VERIFY( ((_h.size() >= _h.head()) && (_h.size() - _h.head()) >= count) );

		if( _h.write( first, last, _h.head()) == last ){

			auto head_ = _h.head();
			head_ += count;
			_h.head_set( head_ % _h.size() );

			_h.ready_inc( count );
			CBL_VERIFY( _h.ready() <= _h.size() );

			return last;
			}
		else{
			FATAL_ERROR_FWD(nullptr);
			}
		}

	template< typename TCtr, typename ... TArgs >
	void w_head_to_end_emplace( typename TCtr && ctr, typename TArgs && ... args ){
		CBL_VERIFY( ((_h.size() >= _h.ready()) && (_h.size() - _h.ready()) >= 1) );
		CBL_VERIFY( ((_h.size() >= _h.head()) && (_h.size() - _h.head()) >= 1) );

		_h.write( ctr( std::forward<TArgs>( args )... ), _h.head() );

		auto head_ = _h.head();
		++head_;
		_h.head_set( head_ % _h.size() );

		_h.ready_inc( 1 );
		CBL_VERIFY( _h.ready() <= _h.size() );
		}

	template< typename TInIt >
	TInIt w_head_to_end_move( TInIt first, TInIt last ){
		const size_type count = std::distance( first, last );

		CBL_VERIFY( ((_h.size() >= _h.ready()) && (_h.size() - _h.ready()) >= count) );
		CBL_VERIFY( ((_h.size() >= _h.head()) && (_h.size() - _h.head()) >= count) );

		if( _h.write( first, last, _h.head() ) == last ){

			auto head_ = _h.head();
			head_ += count;
			_h.head_set( head_ % _h.size() );

			_h.ready_inc( count );
			CBL_VERIFY( _h.ready() <= _h.size() );

			return last;
			}
		else{
			FATAL_ERROR_FWD(nullptr);
			}
		}

	template<typename TInIt>
	TInIt w_obj_empty( TInIt first, TInIt last ){
		const size_type count = std::distance( first, last );

		// checking
		CBL_VERIFY( opush_is_empty() );
		CBL_VERIFY( 0 == _h.head() );

		CBL_VERIFY( _h.size() >= _h.head() );
		CBL_VERIFY( (_h.size() - _h.head()) >= count );

		CBL_VERIFY( _h.size() >= _h.ready() );
		CBL_VERIFY( (_h.size() - _h.ready()) >= count );

		// write
		return w_head_to_end( first, last );
		}

	template<typename TCtr, typename ... TArgs>
	void w_obj_empty_emplace( typename TCtr && ctr, typename TArgs && ... args ){

		// checking
		CBL_VERIFY( opush_is_empty() );
		CBL_VERIFY( 0 == _h.head() );

		CBL_VERIFY( _h.size() >= _h.head() );
		CBL_VERIFY( (_h.size() - _h.head()) >= 1 );

		CBL_VERIFY( _h.size() >= _nR_h.ready()eady );
		CBL_VERIFY( (_h.size() - _h.ready()) >= 1 );

		// write
		w_head_to_end_emplace( std::forward<TCtr>( ctr ), std::forward<TArgs>( args )... );
		}

	template<typename TInIt>
	TInIt w_obj_empty_move( TInIt first, TInIt last ){
		const size_type count = std::distance( first, last );

		// checking
		CBL_VERIFY( opush_is_empty() );
		CBL_VERIFY( 0 == _h.head() );

		CBL_VERIFY( _h.size() >= _h.head() );
		CBL_VERIFY( (_h.size() - _h.head()) >= count );

		CBL_VERIFY( _h.size() >= _h.ready() );
		CBL_VERIFY( (_h.size() - _h.ready()) >= count );

		// write
		return w_head_to_end_move( first, last );
		}

	template<typename TInIt>
	TInIt w_obj_tail_head( TInIt first, TInIt last ){
		const size_type count = std::distance( first, last );

		// checking
		CBL_VERIFY( opush_is_tail_head() );
		CBL_VERIFY( _h.tail() >= 0 );

		CBL_VERIFY( _h.size() >= _h.head() );
		CBL_VERIFY( ((_h.size() - _h.head()) + _h.tail()) >= count );

		CBL_VERIFY( _h.size() >= _h.ready() );
		CBL_VERIFY( (_h.size() - _h.ready()) >= count );

		// write begin-half of the message
		const size_type beginCnt = (std::min)((_h.size() - _h.head()), count);
		first = w_head_to_end( first, first + beginCnt );

		// write end-half to tail
		if( beginCnt < count ){
			const size_type lostCnt = count - beginCnt;

			CBL_VERIFY( 0 == _h.head() && opush_is_head_tail() );
			CBL_VERIFY( (_h.tail() - _h.head()) >= lostCnt );

			first = w_head_to_end( first, first + lostCnt );
			CBL_VERIFY( first == last);
			}

		return first;
		}

	template<typename TCtr, typename ... TArgs>
	void w_obj_tail_head_emplace( typename TCtr && ctr, typename TArgs && ... args ){

		// checking
		CBL_VERIFY( opush_is_tail_head() );
		CBL_VERIFY( _h.tail() >= 0 );

		CBL_VERIFY( _h.size() >= _h.head() );
		CBL_VERIFY( ((_h.size() - _h.head()) + _h.tail()) >= 1 );

		CBL_VERIFY( _h.size() >= _h.ready() );
		CBL_VERIFY( (_h.size() - _h.ready()) >= 1 );

		// write begin-half of the message
		const size_type beginCnt = (std::min < std::intmax_t>)((_h.size() - _h.head()), 1);
		if( beginCnt )
			w_head_to_end_emplace( std::forward<TCtr>( ctr ), std::forward<TArgs>( args )... );
		else{
			CBL_VERIFY( 0 == _h.head() && opush_is_head_tail() );
			CBL_VERIFY( (_h.tail() - _h.head()) >= 1 );

			w_head_to_end_emplace( std::forward<TCtr>( ctr ), std::forward<TArgs>( args )... );
			}
		}

	template<typename TInIt>
	TInIt w_obj_tail_head_move( TInIt first, TInIt last ){
		const size_type count = std::distance( first, last );

		// checking
		CBL_VERIFY( opush_is_tail_head() );
		CBL_VERIFY( _h.tail() >= 0 );

		CBL_VERIFY( _h.size() >= _h.head() );
		CBL_VERIFY( ((_h.size() - _h.head()) + _h.tail()) >= count );

		CBL_VERIFY( _h.size() >= _h.ready() );
		CBL_VERIFY( (_h.size() - _h.ready()) >= count );

		// write begin-half of the message
		const size_type beginCnt = (std::min)((_h.size() - _h.head()), count);
		first = w_head_to_end_move( first, first + beginCnt );

		// write end-half to tail
		if( beginCnt < count ){
			const size_type lostCnt = count - beginCnt;

			CBL_VERIFY( 0 == _h.head() && opush_is_head_tail() );
			CBL_VERIFY( (_h.tail() - _h.head()) >= lostCnt );

			first = w_head_to_end_move( first, first + lostCnt );
			CBL_VERIFY( first == last );
			}

		return first;
		}

	template<typename TInIt>
	TInIt w_obj_head_tail( TInIt first, TInIt last ){
		const size_type count = std::distance( first, last );

		CBL_VERIFY( opush_is_head_tail() );

		CBL_VERIFY( _h.tail() >= _h.head() );
		CBL_VERIFY( (_h.tail() - _h.head()) >= count );

		CBL_VERIFY( _h.size() >= _h.ready() );
		CBL_VERIFY( (_h.size() - _h.ready()) >= count );

		// write
		return w_head_to_end( first, last );
		}

	template<typename TCtr, typename ... TArgs>
	void w_obj_head_tail_emplace( typename TCtr && ctr, typename TArgs && ... args ){
		CBL_VERIFY( opush_is_head_tail() );

		CBL_VERIFY( _h.tail() >= _h.head() );
		CBL_VERIFY( (_h.tail() - _h.head()) >= 1 );

		CBL_VERIFY( _h.size() >= _h.ready() );
		CBL_VERIFY( (_h.size() - _h.ready()) >= 1 );

		// write
		w_head_to_end_emplace( std::forward<TCtr>( ctr ), std::forward<TArgs>( args )... );
		}

	template<typename TInIt>
	TInIt w_obj_head_tail_move( TInIt first, TInIt last ){
		const size_type count = std::distance( first, last );

		CBL_VERIFY( opush_is_head_tail() );

		CBL_VERIFY( _h.tail() >= _h.head() );
		CBL_VERIFY( (_h.tail() - _h.head()) >= count );

		CBL_VERIFY( _h.size() >= _h.ready() );
		CBL_VERIFY( (_h.size() - _h.ready()) >= count );

		// write
		return w_head_to_end_move( first, last );
		}

	bool opush_is_empty() const{
		return (_h.head() == _h.tail() && 0 == _h.ready());
		}

	bool opush_is_filled() const{
		return (_h.head() == _h.tail() && _h.size() == _h.ready());
		}

	bool opush_is_tail_head() const{
		return (_h.tail() < _h.head());
		}

	bool opush_is_head_tail() const{
		return (_h.head() <= _h.tail());
		}

public:
	value_type& front(){
		return _h.front();
		}

	const value_type& front()const{
		return _h.frong();
		}

	template<typename TInIt>
	bufusage_prec_t push( TInIt first, TInIt last ){

		size_type count = std::distance( first, last );
		bufusage_prec_t r;

		if( !count ){
			r.errc = bufusage_prec_t::codes_t::ERR_SUCCESS;
			r.usedElements = 0;
			return r;
			}

		CBL_VERIFY( count >= 0 );
		CBL_VERIFY( _h.ready() >= 0 );
		CBL_VERIFY( _h.ready() <= _h.size() );

	#ifdef USE_DCF_CHECK
		const size_type oldgap = _h.size() - _h.ready();
		const size_type oldready = _h.ready();
	#endif//USE_DCF_CHECK

		// if place in the buffer is not enough for insert (size-marker + message)
		if( (_h.size() - _h.ready()) < count ){
			r.errc = bufusage_prec_t::codes_t::ERR_OVERFLOW;
			r.usedElements = _h.size() - _h.ready();
			return r;
			}

		const auto currSt = get_state();
		{
		crm::detail::post_scope_action_t stAdjust( [this, &currSt]{
			set_state( currSt );
			} );

		// buffer is empty
		if( opush_is_empty() ){
			TInIt next = w_obj_empty( first, last );
			CBL_VERIFY( next == last );
			}
		else if( opush_is_tail_head() ){
			TInIt next = w_obj_tail_head( first, last );
			CBL_VERIFY( next == last );
			}
		else if( opush_is_head_tail() ){
			TInIt next = w_obj_head_tail( first, last );
			CBL_VERIFY( next == last );
			}
		else if( opush_is_filled() ){
			THROW_EXC_FWD(nullptr);
			}
		else{
			THROW_EXC_FWD(nullptr);
			}

		//check for fulfilled( or one message with zero length)
		CBL_VERIFY( _h.ready() <= _h.size() );
		CBL_VERIFY( (_h.head() == _h.tail() ? (_h.ready() == _h.size() || 0 == _h.ready()) : 1) );
		CBL_VERIFY( (_h.ready() == _h.size() ? _h.head() == _h.tail() : 1) );

	#ifdef USE_DCF_CHECK
		CBL_VERIFY( ((_h.size() - _h.ready()) + count) == oldgap );
		CBL_VERIFY( (oldready + count) == _h.ready() );
	#endif//USE_DCF_CHECK

		r.errc = bufusage_prec_t::codes_t::ERR_SUCCESS;
		r.usedElements = count;

		stAdjust.reset();
		return r;
		}
		}

	bufusage_prec_t push( const value_type & obj ){
		return push( &obj, &obj + 1 );
		}

	template<typename TInIt>
	bufusage_prec_t push_with_move( TInIt first, TInIt last ){
		const size_type count = std::distance( first, last );

		bufusage_prec_t r;

		if( !count ){
			r.errc = bufusage_prec_t::codes_t::ERR_SUCCESS;
			r.usedElements = 0;
			return r;
			}

		CBL_VERIFY( count >= 0 );
		CBL_VERIFY( _h.ready() >= 0 );
		CBL_VERIFY( _h.ready() <= _h.size() );

	#ifdef USE_DCF_CHECK
		const size_type oldgap = _h.size() - _h.ready();
		const size_type oldready = _h.ready();
	#endif//USE_DCF_CHECK

		// if place in the buffer is not enough for insert (size-marker + message)
		if( (_h.size() - _h.ready()) < count ){
			r.errc = bufusage_prec_t::codes_t::ERR_OVERFLOW;
			r.usedElements = _h.size() - _h.ready();
			return r;
			}

		const auto currSt = get_state();
		{
		post_scope_action_t stAdjust( [this, &currSt]{
			set_state( currSt );
			} );

		// buffer is empty
		if( opush_is_empty() ){
			TInIt next = w_obj_empty_move( first, last );
			CBL_VERIFY( next == last );
			}
		else if( opush_is_tail_head() ){
			TInIt next = w_obj_tail_head_move( first, last );
			CBL_VERIFY( next == last );
			}
		else if( opush_is_head_tail() ){
			TInIt next = w_obj_head_tail_move( first, last );
			CBL_VERIFY( next == last );
			}
		else if( opush_is_filled() ){
			THROW_EXC_FWD(nullptr);
			}
		else{
			THROW_EXC_FWD(nullptr);
			}

		//check for fulfilled( or one message with zero length)
		CBL_VERIFY( _h.ready() <= _h.size() );
		CBL_VERIFY( (_h.head() == _h.tail() ? (_h.ready() == _h.size() || 0 == _h.ready()) : 1) );
		CBL_VERIFY( (_h.ready() == _h.size() ? _h.head() == _h.tail() : 1) );

	#ifdef USE_DCF_CHECK
		CBL_VERIFY( ((_h.size() - _h.ready()) + count) == oldgap );
		CBL_VERIFY( (oldready + count) == _h.ready() );
	#endif//USE_DCF_CHECK

		r.errc = bufusage_prec_t::codes_t::ERR_SUCCESS;
		r.usedElements = count;

		stAdjust.reset();
		return r;
		}
		}

	bufusage_prec_t push( value_type && obj ){
		return push_with_move( &obj, &obj + 1 );
		}

	template<typename TCtr, typename ... TArgs>
	bufusage_prec_t push_emplace( typename TCtr && ctr, typename TArgs && ... args ){

		bufusage_prec_t r;

		CBL_VERIFY( _h.ready() >= 0 );
		CBL_VERIFY( _h.ready() <= _h.size() );

	#ifdef USE_DCF_CHECK
		const size_type oldgap = _h.size() - _h.ready();
		const size_type oldready = _h.ready();
	#endif//USE_DCF_CHECK

		// if place in the buffer is not enough for insert (size-marker + message)
		if( (_h.size() - _h.ready()) < 1 ){
			r.errc = bufusage_prec_t::codes_t::ERR_OVERFLOW;
			r.usedElements = _h.size() - _h.ready();
			return r;
			}

		const auto currSt = get_state();
		{
		crm::detail::post_scope_action_t stAdjust( [this, &currSt]{
			set_state( currSt );
			} );

		// buffer is empty
		if( opush_is_empty() ){
			w_obj_empty_emplace( std::forward<TCtr>( ctr ), std::forward<TArgs>( args )... );
			}
		else if( opush_is_tail_head() ){
			w_obj_tail_head_emplace( std::forward<TCtr>( ctr ), std::forward<TArgs>( args )... );
			}
		else if( opush_is_head_tail() ){
			w_obj_head_tail_emplace( std::forward<TCtr>( ctr ), std::forward<TArgs>( args )... );
			}
		else if( opush_is_filled() ){
			THROW_EXC_FWD(nullptr);
			}
		else{
			THROW_EXC_FWD(nullptr);
			}

		//check for fulfilled( or one message with zero length)
		CBL_VERIFY( _h.ready() <= _h.size() );
		CBL_VERIFY( (_h.head() == _h.tail() ? (_h.ready() == _h.size() || 0 == _h.ready()) : 1) );
		CBL_VERIFY( (_h.ready() == _h.size() ? _h.head() == _h.tail() : 1) );

	#ifdef USE_DCF_CHECK
		CBL_VERIFY( ((_h.size() - _h.ready()) + 1) == oldgap );
		CBL_VERIFY( (oldready + 1) == _h.ready() );
	#endif//USE_DCF_CHECK

		r.errc = bufusage_prec_t::codes_t::ERR_SUCCESS;
		r.usedElements = 1;

		stAdjust.reset();
		return r;
		}
		}

private:
	/*----------------------------------------------------------------------
	*
	* 			read operations
	*
	* ---------------------------------------------------------------------*/
	bool opop_is_head_tail()const{
		return (_h.head() <= _h.tail());
		}

	bool opop_is_empty()const{
		const bool isEmpty = (0 == _h.ready());

	#ifdef USE_DCF_CHECK
		if( isEmpty ){
			CBL_VERIFY( 0 == _h.tail() );
			CBL_VERIFY( 0 == _h.head());
			}
	#endif//USE_DCF_CHECK

		return isEmpty;
		}

	bool opop_is_tail_head()const{
		return (_h.tail() < _h.head());
		}

	template<typename TInIt>
	TInIt r_tail_end( TInIt first, TInIt last ){
		const auto destCount_ = std::distance( first, last );
		if( destCount_ >= 0 ){
			CBL_VERIFY( ((_h.size() >= _h.tail()) && (_h.size() - _h.tail()) >= (size_type)destCount_) );

			auto resIt = _h.read( first, last, _h.tail() );
			const auto resCount = std::distance( first, resIt );
			if( resCount > 0 ){
				auto tail_ = _h.tail();
				tail_ += resCount;
				_h.tail_set( tail_ % _h.size() );

				_h.ready_dec( resCount );

				return std::next( first, resCount );
				}
			else{
				CBL_VERIFY( resCount == 0 );
				return first;
				}
			}
		else{
			THROW_EXC_FWD(nullptr);
			}
		}

	template<typename TInIt>
	TInIt r_obj_head_tail( TInIt first, TInIt last ){

		const size_type count = std::distance( first, last );
		const size_type l = (count > ready_count() ? ready_count() : count);

		if( count < l )THROW_EXC_FWD(nullptr);

		// read first half of the message
		const size_type rbeginCnt = (std::min)(_h.size() - _h.tail(), l);
		auto itEnd = std::next( first, rbeginCnt );
		first = r_tail_end( first, itEnd );

		// read end half of the message
		if( rbeginCnt < l ){
			CBL_VERIFY( 0 == _h.tail() );

			const size_type rendCnt = l - rbeginCnt;
			CBL_VERIFY( _h.head() >= rendCnt );

			auto rEnd2 = std::next( first, rendCnt );
			first = r_tail_end( first, rEnd2 );
			}

		return first;
		}

	template<typename TInIt>
	TInIt r_obj_tail_head( TInIt first, TInIt last ){

		const auto count = std::distance( first, last );
		if(count >= 0) {
			const auto rc = ready_count();
			const auto lread = std::min<size_t>(static_cast<decltype(rc)>(count), rc);
			
			// read lread elements
			if(lread) {

				const auto count_ = std::distance(first, last);
				if(count_ < 0)
					THROW_EXC_FWD(nullptr);

				if((size_type)count_ < lread)
					THROW_EXC_FWD(nullptr);

				CBL_VERIFY((_h.head() - _h.tail()) >= lread);
				auto rtEnd1 = std::next(first, lread);
				first = r_tail_end(first, rtEnd1);
				}

			return first;
			}
		else {
			THROW_EXC_FWD(nullptr);
			}
		}

public:
	template<typename TInOut>
	bufusage_prec_t pop( TInOut first, TInOut last, TInOut &next ){

		next = first;

		CBL_VERIFY( _h.ready() <= _h.size() );

		bufusage_prec_t r;

		// if buffer is empty
		if( opop_is_empty() ){
			r.errc = bufusage_prec_t::codes_t::ERR_EMPTY;
			return r;
			}

	#ifdef USE_DCF_CHECK
		const size_type oldgap = _h.size() - _h.ready();
		const size_type oldready = _h.ready();
	#endif//USE_DCF_CHECK

		const auto currSt = get_state();
		{
		crm::detail::post_scope_action_t stAdjust( [this, &currSt]{
			set_state( currSt );
			} );

		// read next message
		if( opop_is_head_tail() ){
			next = r_obj_head_tail( first, last );
			}
		else if( _h.tail() < _h.head() ){
			next = r_obj_tail_head( first, last );
			}
		else{
			THROW_EXC_FWD(nullptr);
			}

		size_type lbuf = std::distance( first, next );

	#ifdef USE_DCF_CHECK
		CBL_VERIFY( (oldgap + lbuf) == (_h.size() - _h.ready()) );
		CBL_VERIFY( oldready == (_h.ready() + lbuf) );
	#endif//USE_DCF_CHECK

		// check for empty
		if( _h.head() == _h.tail() ){
			if( 0 == _h.ready() ){
				_h.head_set( 0 );
				_h.tail_set( 0 );
				}
			else{
				CBL_VERIFY( _h.size() == _h.ready() );
				}
			}
		else{
			CBL_VERIFY( 0 != _h.ready() );
			}

		r.usedElements = lbuf;
		r.errc = bufusage_prec_t::codes_t::ERR_SUCCESS;

		stAdjust.reset();
		return r;
		}
		}

	bufusage_prec_t pop( value_type & item ){
		value_type * first = &item;
		value_type * last = first + 1;
		value_type * next = first;

		auto result = pop( first, last, next );
		CBL_VERIFY( (result.errc == bufusage_prec_t::codes_t::ERR_SUCCESS && next == last)
			|| (result.errc != bufusage_prec_t::codes_t::ERR_SUCCESS && next == first) );
		return result;
		}

	bool empty()const{
		const bool flempty = 0 == _h.ready();

		if( flempty ){
			CBL_VERIFY( opush_is_empty() );
			CBL_VERIFY( opop_is_empty() );
			}

		return flempty;
		}

	size_type size()const{
		return _h.size();
		}

	size_t ready()const noexcept{
		return _h.ready();
		}
	};
}
}

