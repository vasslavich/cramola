#ifndef CBL_AXI_CONCURRENCY_MT_DYNAMIC_PUSHPOP_HEADER
#define	CBL_AXI_CONCURRENCY_MT_DYNAMIC_PUSHPOP_HEADER


#include <atomic>
#include <chrono>
#include <mutex>
#include <functional>
#include "../../core/err/err.h"
#include "../declare.h"
#include "../utilities/functors.h"
#include "../constants/__enums.h"


namespace crm{
namespace aux{


template<typename Tcritical,
	typename Tcritical_scope,
	typename Tcondvar,
	typename Tsyncobj,
	typename TsyncobjFunctor = functor_container_dictionary_t<Tsyncobj >>
class mt_dictionary_t {
public:
	typedef typename Tsyncobj object_t;
	typedef typename object_t::key_type key_type;
	typedef typename object_t::mapped_type value_type;
	typedef typename Tcritical lck_t;
	typedef typename Tcritical_scope lck_scp_t;
	typedef typename Tcondvar cndvar_t;
	typedef typename TsyncobjFunctor functor_t;

private:

	/// buffer of data
	object_t _container;
	/// count of stored values
	std::atomic<std::intmax_t> _nStored = 0;
	/*! ���������� */
	mutable lck_t _lock;
	/*! ������ "�� ������" ������� */
	cndvar_t _condDec;
	/*! ������������� ������� */
	std::string _objectId;

#ifdef CBL_QUEUE_SIZE_CHECKER
	/*! ������������ ������������ ����� ������� */
	std::atomic<std::intmax_t> _lastMaxStored = 0;
#endif

	void _clear(){
		functor_t::clear( _container );
		_nStored.store( 0, std::memory_order::memory_order_release );

#ifdef CBL_QUEUE_SIZE_CHECKER
		_lastMaxStored.store( 0, std::memory_order::memory_order_release );
#endif
	}

	void reset() {
		_clear();
		}

	template<typename Rep, typename Period>
	size_t sem_wait_dec( lck_scp_t &lock,
		typename const std::chrono::duration<Rep, Period> &waitTime ) {

		CBL_VERIFY( lock.owns_lock() );
		size_t stValue = 0;
		if( _condDec.wait_for( lock, waitTime, [this, &stValue] {
			stValue = stored_count();
			return 0 != stValue;
			} ) ) {

			CBL_VERIFY( lock.owns_lock() );
			return stValue;
			}
		else
			return 0;
		}

	void sem_dec() {

		CBL_VERIFY( stored_count() > 0 );

		stored_dec();
		CBL_VERIFY( stored_count() >= 0 );
		}

	void sem_inc() {

		stored_inc(1);
		_condDec.notify_one();
		}

public:
	void stored_dec() {
		if( _nStored.fetch_sub( 1 ) <= 0 )
			FATAL_ERROR_FWD(nullptr);
		}

	void stored_inc(std::intmax_t count ) {

#ifdef CBL_QUEUE_SIZE_CHECKER
		const auto lastMax = _lastMaxStored.load( std::memory_order::memory_order_acquire );
#endif
		
		const auto stVal = _nStored.fetch_add( count );

#ifdef CBL_QUEUE_SIZE_CHECKER
		if( stVal > lastMax ) {
			_lastMaxStored.store( stVal, std::memory_order::memory_order_release );
			if( lastMax && ( lastMax % CBL_TRACE_OBJECTS_COUNTER_STEP ) == 0 ) {

				std::string log( SIZE_SEQUENCE_FWDARNING );
				log += ":" + _objectId + "=" + std::to_string( stVal );
				crm::file_logger_t::logging( log, 0, __CBL_FILEW__, __LINE__ );
				}
			}
		else {
			if((lastMax - stVal) >= CBL_TRACE_OBJECTS_COUNTER_STEP) {
				_lastMaxStored.store(stVal, std::memory_order::memory_order_release);
				}
			}
#endif
		}

	size_t stored_count() {
		const auto value = _nStored.load( std::memory_order::memory_order_acquire );
		if( value < 0 )
			FATAL_ERROR_FWD(nullptr);

		return (size_t)value;
		}

private:
	void copy( mt_dictionary_t &&o ) {
		if( this != &o ) {
			lck_scp_t lock_1( src._lock, std::defer_lock );
			lck_scp_t lock_2( _lock, std::defer_lock );
			std::lock( lock_1, lock_2 );

			_container = std::move( o._container );
			_nStored = std::move( o._nStored );
			_objectId = std::move( o._objectId );

			o.reset();
			}
		}

	void copy( const mt_dictionary_t &o ) {
		if( this != &o ) {
			lck_scp_t lock_1( src._lock, std::defer_lock );
			lck_scp_t lock_2( _lock, std::defer_lock );
			std::lock( lock_1, lock_2 );

			_container = o._container;
			_nStored = o._nStored;
			_objectId = o._objectId;
			}
		}

public:
	mt_dictionary_t( const std::string & objectId )
		: _objectId( objectId ) {

		if( _objectId.empty() ) {
			FATAL_ERROR_FWD(nullptr);
			}
		}

	mt_dictionary_t( const mt_dictionary_t& src ) {
		copy( src );
		}

	mt_dictionary_t( mt_dictionary_t&& src ) {
		copy( std::move( src ) );
		}

	mt_dictionary_t& operator=(const mt_dictionary_t &src) {
		if(this != std::addressof(src)) {
			copy(src);
			}

		return (*this);
		}

	mt_dictionary_t& operator=(mt_dictionary_t &&src) {
		if(this != std::addressof(src)) {
			copy(std::move(src));
			}

		return (*this);
		}

	void clear() {
		lck_scp_t lock( _lock );
		_clear();
		}

	/*! ������� �������� */
	unique_collection_result_t push( const key_type & key, const value_type & item ) {

		lck_scp_t lock( _lock );

		// ������� ��������� � ������
		const auto r = functor_t::insert( _container, key, item );
		if( unique_collection_result_t::success == r ) {
			
			// ������ ��������� �������
			sem_inc();
			}

		return r;
		}

	/*! ������� �������� */
	unique_collection_result_t push( const key_type & key, value_type && item ) {

		lck_scp_t lock( _lock );

		// ������� ��������� � ������
		const auto r = functor_t::insert( _container, key, std::forward<value_type>( item ) );
		if( unique_collection_result_t::success == r ) {

			// ������ ��������� �������
			sem_inc();
			}

		return r;
		}

	/*! ���������� �������� */
	template<typename Rep, typename Period>
	bool pop( value_type & obj,
		typename const std::chrono::duration<Rep, Period> &wait ) {

		lck_scp_t lock( _lock );
		if( sem_wait_dec( lock, wait ) ) {

			auto itSrc = functor_t::first( _container );
			CBL_VERIFY( itSrc != functor_t::last( _container ) );

			functor_t::move( obj, itSrc );
			functor_t::erase( _container, itSrc );

			/* ����������� */
			sem_dec();

			return true;
			}
		else
			return false;
		}

	bool empty() {
		lck_scp_t lock( _lock );
		return functor_t::empty( _container );
		}

	size_t size()const {
		lck_scp_t lock( _lock );
		return _container.size();
		}
	};
}
}
#endif/// CBL_AXI_CONCURRENCY_MT_DYNAMIC_PUSHPOP_HEADER
