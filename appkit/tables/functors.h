#pragma once


#include <type_traits>
#include <functional>
#include "../exceptions.h"


namespace crm{
namespace detail {


template<typename T, typename Enable = void>
struct is_container_iterable_t : public std::false_type {};


template<typename T>
struct is_container_iterable_helper_iterator_t {
	typedef typename T::value_type value_type;
	typedef typename T::const_iterator const_iterator;
	typedef typename T::iterator iterator;

	typedef typename std::decay_t<decltype(std::declval<T>().cbegin())> cbegin_iterator_t;
	typedef typename std::decay_t<decltype(std::declval<T>().cend())> cend_iterator_t;

	typedef typename std::decay_t<decltype(*(std::declval<cbegin_iterator_t>()))> cbegin_value_t;
	typedef typename std::decay_t<decltype(*(std::declval<cend_iterator_t>()))> cend_value_t;

	typedef typename std::decay_t<decltype(std::declval<T>().begin())> begin_iterator_t;
	typedef typename std::decay_t<decltype(std::declval<T>().end())> end_iterator_t;

	typedef typename std::decay_t<decltype(*(std::declval<begin_iterator_t>()))> begin_value_t;
	typedef typename std::decay_t<decltype(*(std::declval<end_iterator_t>()))> end_value_t;
	};

template<typename T>
struct is_container_iterable_t<T,
	typename std::enable_if<

	(std::is_same<typename is_container_iterable_helper_iterator_t<T>::iterator,
	typename is_container_iterable_helper_iterator_t<T>::begin_iterator_t>::value)
	&& (std::is_same<typename is_container_iterable_helper_iterator_t<T>::iterator,
	typename is_container_iterable_helper_iterator_t<T>::end_iterator_t>::value)
	&& (std::is_same<typename is_container_iterable_helper_iterator_t<T>::const_iterator,
	typename is_container_iterable_helper_iterator_t<T>::cbegin_iterator_t>::value)
	&& (std::is_same<typename is_container_iterable_helper_iterator_t<T>::const_iterator,
	typename is_container_iterable_helper_iterator_t<T>::cend_iterator_t>::value)
	&& (std::is_same<typename is_container_iterable_helper_iterator_t<T>::value_type,
	typename is_container_iterable_helper_iterator_t<T>::cbegin_value_t>::value)
	&& (std::is_same<typename is_container_iterable_helper_iterator_t<T>::value_type,
	typename is_container_iterable_helper_iterator_t<T>::cend_value_t>::value)
	&& (std::is_same<typename is_container_iterable_helper_iterator_t<T>::value_type,
	typename is_container_iterable_helper_iterator_t<T>::begin_value_t>::value)
	&& (std::is_same<typename is_container_iterable_helper_iterator_t<T>::value_type,
	typename is_container_iterable_helper_iterator_t<T>::end_value_t>::value)
>::type>
: std::true_type{};


template<typename TBaseType>
class functor_container_dictionary_t {
public:
	typedef typename TBaseType base_type_t;
	typedef typename base_type_t::key_type key_type_t;
	typedef typename base_type_t::mapped_type value_type_t;
	typedef typename base_type_t::const_iterator const_iterator;
	typedef typename base_type_t::iterator iterator;

	static unique_collection_result_t insert( base_type_t & target, const key_type_t & key, const typename value_type_t & item ) {
		if( target.insert( std::make_pair( key, item ) ).second )
			return unique_collection_result_t::success;
		else
			return unique_collection_result_t::exists;
		}

	static unique_collection_result_t insert( base_type_t & target, const key_type_t & key, value_type_t && item ) {
		if( target.insert( std::make_pair( key, std::move( item ) ) ).second )
			return unique_collection_result_t::success;
		else
			return unique_collection_result_t::exists;
		}

	static void erase( base_type_t & target, const key_type_t & key ) {
		target.erase( key );
		}

	static void erase( base_type_t & target, const const_iterator & pos ) {
		target.erase( pos );
		}

	static iterator first( base_type_t & target ) {
		return target.begin();
		}

	static const_iterator first( const base_type_t & target ) {
		return target.cbegin();
		}

	static const_iterator last( const base_type_t & target ) {
		return target.cend();
		}

	static void move( value_type_t & target, iterator & pos ) {
		target = std::move( (*pos).second );
		}

	static void clear( base_type_t & obj ) {
		obj.clear();
		}

	static bool empty( const base_type_t & obj ) {
		return obj.empty();
		}

	static size_t size( const base_type_t & obj ) {
		return obj.size();
		}
	};



template<typename TBaseType>
class functor_container_queue_t {
public:
	typedef typename TBaseType base_type_t;
	typedef typename base_type_t::value_type value_type_t;

	static void push( base_type_t & target, const value_type_t & item ) {
		target.push( item );
		}

	static void push( base_type_t & target, value_type_t && item ) {
		target.push( std::forward<value_type_t>( item ) );
		}

	template<typename ... TArgs>
	static void emplace( base_type_t & target, typename TArgs && ... args ) {
		target.emplace( std::forward<TArgs>( args )... );
		}

	static void pop( base_type_t & target ) {
		target.pop();
		}

	static value_type_t& front( base_type_t & target ) {
		return target.front();
		}

	static const value_type_t& front( const base_type_t & target ) {
		return target.front();
		}

	static void clear( base_type_t & obj ) {
		CHECK_NO_EXCEPTION( base_type_t().swap( obj ) );
		}

	static bool empty( const base_type_t & obj ) {
		return obj.empty();
		}

	static size_t size( const base_type_t & obj ) {
		return obj.size();
		}
	};


template<typename TBaseType,
	typename std::enable_if_t<is_container_iterable_t<TBaseType>::value> * = 0>
class functor_container_deque_t {
public:
	typedef typename TBaseType base_type_t;
	typedef typename base_type_t::value_type value_type_t;
	typedef typename base_type_t::const_iterator const_iterator;
	typedef typename base_type_t::iterator iterator;

	static const_iterator cbegin( base_type_t & target ) {
		return target.cbegin();
		}

	static const_iterator cbegin( const base_type_t & target ) {
		return target.cbegin();
		}

	static iterator begin( base_type_t & target ) {
		return target.begin();
		}

	static const_iterator cend( base_type_t & target ) {
		return target.cend();
		}

	static const_iterator cend( const base_type_t & target ) {
		return target.cend();
		}

	static iterator end( base_type_t & target ) {
		return target.end();
		}

	static void push( base_type_t & target, const value_type_t & item ) {
		target.push_back( item );
		}

	static void push( base_type_t & target, value_type_t && item ) {
		target.push_back( std::forward<value_type_t>( item ) );
		}

	template<typename ... TArgs>
	static void emplace( base_type_t & target, typename TArgs && ... args ) {
		target.emplace( std::forward<TArgs>( args )... );
		}

	static void pop( base_type_t & target ) {
		target.pop_front();
		}

	static value_type_t& front( base_type_t & target ) {
		return target.front();
		}

	static const value_type_t& front( const base_type_t & target ) {
		return target.front();
		}

	static void clear( base_type_t & obj ) {
		CHECK_NO_EXCEPTION( base_type_t().swap( obj ) );
		}

	static bool empty( const base_type_t & obj ) {
		return obj.empty();
		}

	static size_t size( const base_type_t & obj ) {
		return obj.size();
		}
	};



template<typename TContainerFunctor, typename = std::void_t<>>
struct is_container_functor_t : std::false_type {};

template<typename TContainerFunctor>
struct is_container_functor_t<TContainerFunctor, std::void_t<
	decltype(std::declval< TContainerFunctor>().is_empty(std::declval<const typename TContainerFunctor::container_t>())),
	decltype(std::declval< TContainerFunctor>().resize(std::declval<typename TContainerFunctor::container_t>(), 0)),
	decltype(std::declval< TContainerFunctor>().clear(std::declval<typename TContainerFunctor::container_t>())),
	decltype(std::declval< TContainerFunctor>().size(std::declval<const typename TContainerFunctor::container_t>())),
	decltype(std::declval< TContainerFunctor>().begin(std::declval<typename TContainerFunctor::container_t>())),
	decltype(std::declval< TContainerFunctor>().end(std::declval<typename TContainerFunctor::container_t>())),
	decltype(std::declval< TContainerFunctor>().cbegin(std::declval<const typename TContainerFunctor::container_t>())),
	decltype(std::declval< TContainerFunctor>().cend(std::declval<const typename TContainerFunctor::container_t>()))
	>>: std::true_type{};

template<typename TFunctor>
constexpr bool is_container_functor_v = is_container_functor_t<TFunctor>::value;



template<typename TContainerFunctor, typename = std::void_t<>>
struct is_container_functor_has_move_t : std::false_type {};

template<typename TContainerFunctor>
struct is_container_functor_has_move_t <TContainerFunctor, std::void_t<
	decltype(std::declval< TContainerFunctor>().get_move(std::declval<typename TContainerFunctor::container_t::iterator>())),
	decltype(std::declval< TContainerFunctor>().set_move(std::declval<typename TContainerFunctor::container_t::iterator>(),
		std::declval<typename TContainerFunctor::container_t::value_type&&>()))
	>>: std::true_type{};

template<typename TFunctor>
constexpr bool is_container_functor_it_move_v = is_container_functor_has_move_t<TFunctor>::value;



template<typename TContainer>
class ringbuf_ctr_container_functor_t {
public:
	using container_t = typename std::decay_t<TContainer>;
	using value_type = typename container_t::value_type;
	using size_type = typename container_t::size_type ;
	using const_iterator = typename container_t::const_iterator ;
	using iterator = typename container_t::iterator ;

	static bool is_empty(const container_t & obj) {
		return obj.empty();
		}

	static void resize(container_t & obj, size_type newSize) {
		obj.resize(newSize);
		}

	static void clear(container_t & obj) {
		obj.clear();
		}

	static size_type size(const container_t & obj) {
		return obj.size();
		}

	static const_iterator cbegin(const container_t & obj) {
		return obj.cbegin();
		}

	static iterator begin(container_t & obj) {
		return obj.begin();
		}

	static const_iterator cend(const container_t & obj) {
		return obj.cend();
		}

	static iterator end(const container_t & obj) {
		return obj.end();
		}

	static value_type get_move(iterator & from) {
		return boost::move((*from));
		}

	static void set_move(iterator & to, value_type && val) {
		(*to) = boost::move(val);
		}
	};
}
}

