#pragma once


#include <cstddef>
#include <type_traits>
#include <utility>
#include <vector>
#include <memory>
#include <algorithm>
#include "../typeframe/type_trait_base.h"
#include "../typeframe/streamcat/interators/pod_iterator.h"
#include "../functorapp/functorapp.h"


namespace crm{


typedef struct bufusage_prec_t{
	enum class codes_t{
		/// undefined value
		ERR_UNDEFINED = -1,
		/// success operation
		ERR_SUCCESS = 0,
		/// indicate to state of an empty object
		ERR_EMPTY,
		/// timeout of wait operation
		ERR_TIME_OUT,
		/// buffer is filled
		ERR_OVERFLOW
		};

	size_t usedElements = 0;
	codes_t errc{codes_t::ERR_UNDEFINED};

	bufusage_prec_t(codes_t code_ = codes_t::ERR_UNDEFINED)noexcept;
	bufusage_prec_t(codes_t code_, size_t usedElements)noexcept;
	operator bool()const noexcept;
	bufusage_prec_t& operator=(codes_t value) noexcept;
	bool is_empty()const noexcept;
	static const char* errc_str(const codes_t code)noexcept;

	}bufusage_prec_t;


struct break_conditional{
	virtual ~break_conditional(){}
	};


template<typename TWaitCond, typename = std::void_t<>>
struct is_wait_cond_pred_t : std::false_type{};

template<typename TWaitCond>
struct is_wait_cond_pred_t < TWaitCond, std::void_t<
	typename std::enable_if_t<std::is_invocable_r_v<bool, std::decay_t<TWaitCond>>>>>
	: std::true_type{};

template<typename TWaitCond>
constexpr bool is_wait_cond_pred_v = is_wait_cond_pred_t<TWaitCond>::value;





template<typename TWaitCond, typename = std::void_t<>>
struct is_wait_cond_timed_t : std::false_type{};

template<typename TWaitCond>
struct is_wait_cond_timed_t < TWaitCond, std::void_t<
	typename std::enable_if_t<std::is_constructible_v<std::chrono::milliseconds, std::decay_t<decltype(std::declval<std::decay_t<TWaitCond>>().timeout())>>>>>
	: std::true_type{};

template<typename TWaitCond>
constexpr bool is_wait_cond_timed_v = is_wait_cond_timed_t<TWaitCond>::value;




template<typename TWaitCond>
constexpr bool is_wait_cond_timed_and_pred_v = is_wait_cond_timed_v<TWaitCond> && is_wait_cond_pred_v<TWaitCond>;

template<typename TWaitCond>
constexpr bool is_wait_cond_v = is_wait_cond_timed_v<TWaitCond> || is_wait_cond_pred_v<TWaitCond>;





template<typename TCond, typename = std::void_t<>>
struct is_break_conditional : std::false_type{};

template<typename TCond>
constexpr bool is_break_conditional_v = is_break_conditional<TCond>::value;

template<typename TCond>
struct is_break_conditional<TCond,
	std::enable_if_t<std::is_base_of_v<break_conditional, std::decay_t<TCond>>>> : std::true_type{};



template<typename TWaitCond, typename = std::void_t<>>
struct is_break_cancel_t : std::false_type{};

template<typename TWaitCond>
struct is_break_cancel_t < TWaitCond, std::void_t<
	decltype(std::declval<std::decay_t<TWaitCond>>().canceled()),
	typename std::enable_if_t<is_break_conditional_v<TWaitCond>>>>
	: std::true_type{};

template<typename TWaitCond>
constexpr bool is_break_cancel_v = is_break_cancel_t<TWaitCond>::value;


template<typename TWaitCond, typename = std::void_t<>>
struct is_break_timed_t : std::false_type{};

template<typename TWaitCond>
struct is_break_timed_t < TWaitCond, std::void_t<
	typename std::enable_if_t<is_break_conditional_v<TWaitCond>>,
	typename std::enable_if_t < std::is_constructible_v<std::chrono::milliseconds, std::decay_t<decltype(std::declval<std::decay_t<TWaitCond>>().timeout_quant())>>>,
	typename std::enable_if_t<std::is_constructible_v<std::chrono::milliseconds, std::decay_t<decltype(std::declval<std::decay_t<TWaitCond>>().timeout())>>>>>
	: std::true_type{};

template<typename TWaitCond>
constexpr bool is_break_timed_v = is_break_timed_t<TWaitCond>::value;




template<typename TWaitCond>
constexpr bool is_break_timed_and_cancel_v = is_break_timed_v<TWaitCond> && is_break_cancel_v<TWaitCond>;






template<typename TReader>
bool constexpr is_iterator_reader_v = is_iterator_reader_t<TReader>::value;


template<typename TInIt, typename TBreakCond, typename _TBreakCond = std::decay_t<TBreakCond>>
class input_iterator_reader : public _TBreakCond{
public:
	using break_token_type = _TBreakCond;
	using iterator = std::decay_t<TInIt>;
	using value_type = typename std::iterator_traits<iterator>::value_type;
	using difference_type = typename std::iterator_traits<iterator>::difference_type;

private:
	iterator _currit{};
	iterator _end{};
	difference_type _distance{};
	difference_type _size{};

public:
	template<typename XBreakCond,
		typename std::enable_if_t<is_break_conditional_v<XBreakCond>, int> = 0>
	input_iterator_reader(XBreakCond && bc)noexcept
		: _TBreakCond(std::forward<XBreakCond>(bc)){}

	template<typename XIt,
		typename XBreakCond,
		typename std::enable_if_t<is_break_conditional_v<XBreakCond>, int> = 0>
	input_iterator_reader(XIt begin_, XIt end_, XBreakCond && bc)
		: _TBreakCond(std::forward<XBreakCond>(bc))
		, _currit(begin_)
		, _end(end_)
		, _distance(std::distance(begin_, end_))
		, _size(_distance){}

	template<typename XIt>
	void assign(XIt begin_, XIt end_){
		_currit = begin_;
		_end = end_;
		_distance = std::distance(begin_, end_);
		_size = _distance;
		}

	const break_token_type& token_break()const noexcept{
		return (*this);
		}

	bool ready()const noexcept{
		return _distance > 0;
		}

	bool empty()const noexcept{
		return _size == _distance;
		}

	size_t ready_count()const noexcept{
		if (ready()){
			return _distance;
			}
		else{
			return 0;
			}
		}

	value_type next(){
		if (ready()){
			value_type v = (*_currit);

			_currit = std::next(_currit);
			--_distance;

			return v;
			}
		else{
			THROW_EXC_FWD(nullptr);
			}
		}

	std::pair<iterator, iterator> next(size_t count_){
		if (ready()){
			const auto xshifft = (std::min)(ready_count(), count_);
			if (xshifft){
				auto i1 = _currit;
				auto i2 = std::next(_currit, xshifft);

				_currit = i2;
				_distance -= xshifft;

				return std::make_pair(i1, i2);
				}
			else{
				return std::make_pair(iterator{}, iterator{});
				}
			}
		else{
			return std::make_pair(iterator{}, iterator{});
			}
		}

	size_t read_count()const noexcept{
		return _size - _distance;
		}
	};


template<typename TInIt, typename TBreakCond, typename _TBreakCond = std::decay_t<TBreakCond>>
class binary_readers_collection : public _TBreakCond{
public:
	using break_token_type = typename _TBreakCond;
	using self_type = binary_readers_collection<TInIt, TBreakCond>;
	using base_interface = input_iterator_reader<TInIt, _TBreakCond>;
	using iterator = typename base_interface::iterator;
	using value_type = typename base_interface::value_type;
	using difference_type = typename base_interface::difference_type;

private:
	std::vector<std::unique_ptr<base_interface>> _readers;
	size_t _currIdx{ 0 };
	size_t _readCount{ 0 };

public:
	template<typename XBreakCond,
		typename std::enable_if_t<is_break_conditional_v<XBreakCond>, int> = 0>
	binary_readers_collection(XBreakCond && bc)noexcept
		: _TBreakCond(std::forward<XBreakCond>(bc)){}

	template<typename TInReader,
		typename _TInReader = std::decay_t<TInReader>,
		typename std::enable_if_t<std::is_base_of_v<base_interface, _TInReader>, int> = 0 >
	void add(std::unique_ptr<TInReader> && rd){
		_readers.push_back(std::move(rd));
		}

	const break_token_type& token_break()const noexcept{
		return (*this);
		}

	bool empty()const noexcept{
		return 0 == _currIdx && _readers[_currIdx]->empty();
		}

	bool ready()const noexcept{
		if ((_currIdx + 1) < _readers.size()){
			return true;
			}
		else{
			if (_currIdx < _readers.size()){
				return _readers[_currIdx]->ready();
				}
			else{
				return false;
				}
			}
		}

	size_t ready_count()const noexcept{
		if (ready()){
			return _readers.at(_currIdx)->ready_count();
			}
		else{
			return 0;
			}
		}

	value_type next(){
		if (ready()){
			auto v = _readers.at(_currIdx)->next();
			if (!_readers.at(_currIdx)->ready()){
				++_currIdx;
				}

			++_readCount;
			return v;
			}
		else{
			THROW_EXC_FWD(nullptr);
			}
		}

	std::pair<iterator, iterator> next(size_t count){
		if (ready()){
			auto r = _readers.at(_currIdx)->next(count);
			if (!_readers.at(_currIdx)->ready()){
				++_currIdx;
				}

			_readCount += std::distance( r.first, r.second );
			return r;
			}
		else{
			return std::make_pair(iterator{}, iterator{});
			}
		}

	size_t read_count()const noexcept{
		return _readCount;
		}

	bool completed()const noexcept{
		return 0 == ready_count();
		}
	};


template<typename TReader, typename = std::void_t<>>
struct is_iterator_reader_t : std::false_type{};

template<typename TReader>
struct is_iterator_reader_t < TReader, std::void_t<
	typename TReader::value_type,
	typename TReader::iterator,
	decltype(std::declval<TReader>().ready()),
	decltype(std::declval<TReader>().read_count()),
	typename std::enable_if_t<is_break_conditional_v<TReader>>,
	typename std::enable_if_t<is_break_conditional_v<decltype(std::declval<const TReader>().token_break())>>,
	typename std::enable_if_t<std::is_same_v<
	std::decay_t<decltype(std::declval<TReader>().next())>,
	typename TReader::value_type>>,
	typename std::enable_if_t<std::is_same_v<
	std::decay_t<decltype(std::declval<TReader>().next((size_t)0))>,
	std::pair<typename TReader::iterator, typename TReader::iterator>>>>>
	: std::true_type{};




template<typename TMonoValue,
	typename TBreakCond, 
	typename _TBreakCond = std::decay_t<TBreakCond>,
	typename _TxMonoValue = std::decay_t<TMonoValue>,
	const typename std::enable_if_t<srlz::detail::is_mono_serializable_v<_TxMonoValue>, int> = 0>
	class mono_reader : public input_iterator_reader<const uint8_t*, _TBreakCond>{
	private:
		using base_type = input_iterator_reader<const uint8_t*, _TBreakCond>;
		using iterator_t = crm::srlz::detail::const_binary_iterator_pointer_t<_TxMonoValue>;

		iterator_t _cit;
		static_assert(std::is_same_v<decltype(_cit.cbegin()), const uint8_t*>, __FILE_LINE__);

		void adjust_base(){
			assign(_cit.cbegin(), _cit.cend());
			}

	public:
		mono_reader(const mono_reader&) = delete;
		mono_reader& operator=(const mono_reader&) = delete;

		mono_reader(mono_reader && o)noexcept = delete;
		mono_reader& operator=(mono_reader && o)noexcept = delete;

		template<typename XBreakCond,
			typename std::enable_if_t<is_break_conditional_v<XBreakCond>, int> = 0>
		mono_reader(_TxMonoValue v, XBreakCond && bc)
			: base_type(std::forward<XBreakCond>(bc))
			, _cit(v){
			adjust_base();
			}
	};


template<typename TBinary,
	typename TBreakCond,
	typename _TBreakCond = std::decay_t<TBreakCond>,
	typename std::enable_if_t<srlz::detail::is_container_contiguous_of_bytes_v<TBinary>, int> = 0>
class binary_reader : public input_iterator_reader<const uint8_t*, _TBreakCond>{
private:
	using base_type = input_iterator_reader<const uint8_t*, _TBreakCond>;

	TBinary _c;

public:
	template<typename XBinary, 
		typename XBreakCond,
		typename std::enable_if_t<srlz::detail::is_container_contiguous_of_bytes_v<XBinary>, int> = 0>
	binary_reader(XBinary && bc, XBreakCond && brTkn)
		: base_type(std::forward<XBreakCond>(brTkn))
		, _c(std::forward<XBinary>(bc)){

		assign(_c.data(), _c.data() + _c.size());
		}
	};



template<typename TOutputBuffer, typename = std::void_t<>>
struct is_output_buffer_t : std::false_type{};

template<typename TOutputBuffer>
bool constexpr is_output_buffer_v = is_output_buffer_t<TOutputBuffer>::value;


template<typename TOutputBuffer>
struct is_output_buffer_t < TOutputBuffer, std::void_t<
	typename TOutputBuffer::value_type,
	typename TOutputBuffer::iterator,
	decltype(std::declval<TOutputBuffer>().ready()),
	decltype(std::declval<TOutputBuffer>().writed_count()),
	typename std::enable_if_t<is_break_conditional_v<TOutputBuffer>>,
	typename std::enable_if_t<is_break_conditional_v<decltype(std::declval<TOutputBuffer>().token_break())>>,
	decltype(std::declval<TOutputBuffer>().set_output_writed_count(0ull)),
	typename std::enable_if_t<std::is_same_v<
	std::decay_t<decltype(std::declval<TOutputBuffer>().next_begin())>,
	typename TOutputBuffer::iterator>>,
	typename std::enable_if_t<std::is_same_v<
	std::decay_t<decltype(std::declval<TOutputBuffer>().next_end())>,
	typename TOutputBuffer::iterator>>>>
	: std::true_type{};



template<typename TContainer,
	typename _TBreakCond,
	const size_t MaxSerializedSize = 1024 * 1024 * 1024,
	const typename std::enable_if_t<srlz::detail::is_container_of_bytes_v<TContainer>, int> = 0,
	typename TBreakCond = std::decay_t<_TBreakCond>>
class output_container_writer : public TBreakCond{
public:
	using break_token_type = TBreakCond;
	using container = std::decay_t<TContainer>;
	using iterator = typename container::value_type*;
	using const_iterator = const typename container::value_type*;
	using value_type = typename std::iterator_traits<iterator>::value_type;
	using difference_type = typename std::iterator_traits<iterator>::difference_type;

	private:
		enum class state{
			init,
			sized,
			containered
			};

		size_t _wrCount{0};
		srlz::format_traits::length_header _cnt{0};
		container _c;
		state _st{state::init};

	public:
		template<typename XBreakCond>
		output_container_writer(XBreakCond && bc)noexcept
			: TBreakCond(std::forward<XBreakCond>(bc)){}

		const break_token_type& token_break()const noexcept{
			return (*this);
			}

		bool ready()const noexcept{
			return _st < state::containered;
			}

		bool empty()const noexcept{
			return 0 == _wrCount;
			}

		size_t writed_count()const noexcept{
			return _wrCount;
			}

		iterator next_begin_size(){
			CBL_VERIFY(_wrCount < sizeof(_cnt));
			return (uint8_t*)(std::addressof(_cnt)) + _wrCount;
			}

		iterator next_end_size(){
			return (uint8_t*)(std::addressof(_cnt)) + sizeof(_cnt);
			}

		iterator next_begin_container(){
			CBL_VERIFY(_wrCount >= sizeof(_cnt));
			if (_wrCount < (sizeof(_cnt) + _c.size())){
				return _c.data() + (_wrCount - sizeof(_cnt));
				}
			else{
				return next_end_container();
				}
			}

		iterator next_end_container(){
			return _c.data() + _c.size();
			}

		iterator next_begin(){
			switch (_st){
					case state::init:
						return next_begin_size();
					case state::sized:
						return next_begin_container();
					case state::containered:
						return {};
					default:
						FATAL_ERROR_FWD(nullptr);
				}
			}

		iterator next_end(){
			switch (_st){
					case state::init:
						return next_end_size();
					case state::sized:
						return next_end_container();
					case state::containered:
						return {};
					default:
						FATAL_ERROR_FWD(nullptr);
				}
			}

		void set_output_writed_count(size_t count_){
			switch (_st){
					case state::init:
						{
						_wrCount += count_;
						if (_wrCount == sizeof(_cnt)){
							_st = state::sized;

							if (_cnt <= MaxSerializedSize){
								_c.resize(_cnt);
								}
							else{
								FATAL_ERROR_FWD(nullptr);
								}
							}
						else if (_wrCount > sizeof(_cnt)){
							FATAL_ERROR_FWD(nullptr);
							}
						}
						break;

					case state::sized:
						{
						_wrCount += count_;
						if (_wrCount == (sizeof(_cnt) + _c.size())){
							_st = state::containered;
							}
						else if (_wrCount > (sizeof(_cnt) + _c.size())){
							FATAL_ERROR_FWD(nullptr);
							}
						}
						break;

					default:
						THROW_EXC_FWD(nullptr);
				}
			}

		bool completed()const noexcept{
			if (_st == state::containered){
				CBL_VERIFY(_wrCount == (sizeof(_cnt) + _c.size()));
				CBL_VERIFY(_cnt == _c.size());

				return true;
				}
			else{
				return false;
				}
			}

		static constexpr bool is_nothrow_container_release_v = std::is_nothrow_move_constructible_v<container> && std::is_nothrow_move_assignable_v<container>;

		container release() && noexcept(is_nothrow_container_release_v){
			return std::move(_c);
			}
	};




struct break_cancel : virtual break_conditional{
private:
	using functor_wrapper_t = utility::invoke_functor_shared<bool>;

	functor_wrapper_t _f;

public:
	template<typename TCancelTkn,
		typename std::enable_if_t<!std::is_same_v<break_cancel, std::decay_t<TCancelTkn>> && std::is_invocable_r_v<bool, TCancelTkn>, int> = 0>
	break_cancel(TCancelTkn && tk)
		: _f(std::forward<TCancelTkn>(tk)){}

	bool canceled()const{
		return _f();
		}
	};

template<typename TCancelTkn,
	typename std::enable_if_t<std::is_invocable_r_v<bool, TCancelTkn>, int> = 0>
auto make_break_cancel(TCancelTkn && tkn){
	return break_cancel(std::forward<TCancelTkn>(tkn));
	}


template<typename TRep, typename TPeriod>
struct break_timed : virtual break_conditional{
private:
	std::chrono::duration<TRep, TPeriod> _timeout;
	std::chrono::duration<TRep, TPeriod> _timeoutQuant;

public:
	break_timed(const std::chrono::duration<TRep, TPeriod> & timeout,
		const std::chrono::duration<TRep, TPeriod> & timeoutQuant)
		: _timeout(timeout)
		, _timeoutQuant(timeoutQuant){}

	const std::chrono::duration<TRep, TPeriod>& timeout()const noexcept{
		return _timeout;
		}

	const std::chrono::duration<TRep, TPeriod>& timeout_quant()const noexcept{
		return _timeout;
		}
	};

template<typename TRep, typename TPeriod>
auto make_break_timed(const std::chrono::duration<TRep, TPeriod> & timeout,
	const std::chrono::duration<TRep, TPeriod> & timeoutQuant){

	return break_timed<TRep, TPeriod>(timeout, timeoutQuant);
	}

template<typename TDuration,
	typename std::enable_if_t<srlz::detail::is_chrono_duration_v<TDuration>, int> = 0>
auto make_break_timed( TDuration && timeout,
		TDuration && timeoutQuant){

	return break_timed<typename TDuration::rep, typename TDuration::period>( std::forward<TDuration>(timeout),
		std::forward<TDuration>(timeoutQuant));
	}

template<typename TRep, typename TPeriod>
struct break_cancel_timed :
	public break_timed<TRep, TPeriod>,
	public break_cancel{

private:
	using timed_base = break_timed<TRep, TPeriod>;
	using cancel_base = break_cancel;

public:
	template<typename TRep,
		typename TPeriod,
		typename TCancelTkn,
		typename std::enable_if_t<std::is_invocable_r_v<bool, TCancelTkn>, int> = 0>
	break_cancel_timed(TCancelTkn && tk,
			const std::chrono::duration<TRep, TPeriod> & timeout,
			const std::chrono::duration<TRep, TPeriod> & timeoutQuant)
		: timed_base(timeout, timeoutQuant)
		, cancel_base(std::forward<TCancelTkn>(tk)){}
	};

template<typename TRep,
	typename TPeriod,
	typename TCancelTkn,
	typename std::enable_if_t<std::is_invocable_r_v<bool, TCancelTkn>, int> = 0>
auto make_break_cancel_timed(TCancelTkn && tkn,
	const std::chrono::duration<TRep, TPeriod> & timeout,
	const std::chrono::duration<TRep, TPeriod> & timeoutQuant){

	return break_cancel_timed<TRep, TPeriod>(std::forward<TCancelTkn>(tkn), timeout, timeoutQuant);
	}

template<typename TDuration,
	typename TCancelTkn,
	typename std::enable_if_t<std::is_invocable_r_v<bool, TCancelTkn> && srlz::detail::is_chrono_duration_v<TDuration>, int> = 0>
auto make_break_cancel_timed(TCancelTkn && tkn,
		TDuration && timeout,
		TDuration && timeoutQuant){

	return break_cancel_timed<typename TDuration::rep, typename TDuration::period>(std::forward<TCancelTkn>(tkn),
		std::forward<TDuration>(timeout),
		std::forward<TDuration>(timeoutQuant));
	}
}


