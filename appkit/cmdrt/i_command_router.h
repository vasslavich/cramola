#pragma once


#include "./i_channel_commands.h"
#include "../protocol/i_encoding.h"
#include "../channel_terms/rtl_rdm_object.h"


namespace crm::detail{


class rt1_command_router_t;


struct i_rt0_xchannel_maker_t{
	virtual ~i_rt0_xchannel_maker_t(){}

	virtual std::shared_ptr<default_binary_protocol_handler_t> protocol()const noexcept = 0;
	virtual const identity_descriptor_t& this_id()const = 0;
	virtual std::shared_ptr<detail::i_rt0_channel_route_t> rt0_channel_router()const = 0;
	virtual std::unique_ptr<i_rt0_peer_handler_t> find_peer( const rt0_node_rdm_t & rdm )const noexcept = 0;
	};
}

