#pragma once


#include <unordered_map>
#include "./i_channel_commands.h"
#include "./i_command_router.h"
#include "./i_commands.h"
#include "./command_pack.h"
#include "./cmd_sndrcv.h"


namespace crm::detail{


class rt1_command_router_t : public std::enable_shared_from_this<rt1_command_router_t> {
private:
	typedef std::recursive_mutex lck_t;
	typedef std::unique_lock<lck_t> lck_scp_t;

	std::weak_ptr<distributed_ctx_t> _ctx;
	std::unordered_map<rtx_command_type_t, std::map<rtl_roadmap_event_t, std::unique_ptr<i_command_link_wrapper_t>>> _cbTbl;
	mutable lck_t _cbTblLck;
	std::shared_ptr<io_x_rt1_channel_commands_t> _xChannel;
	std::thread _thCommandRead;
	std::atomic<bool> _cancelf{false};
	std::atomic<bool> _startf{false};

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	mutable crm::utility::__xvalue_counter_logger_t<> __sizeTraceer = __FILE_LINE__;
	void __size_trace()const noexcept;
#endif

	bool canceled()const noexcept;

	static void unbind_link_invoke( std::shared_ptr<i_rt1_command_link_t> p )noexcept;
	void unbind_link_async( std::shared_ptr<i_rt1_command_link_t> && wobj )const noexcept;

	std::shared_ptr<distributed_ctx_t> ctx()noexcept;
	std::shared_ptr<distributed_ctx_t> ctx()const noexcept;
	std::shared_ptr<detail::io_x_rt1_channel_commands_t> x_channel()const noexcept;
	void start_x();
	void launch_x_read_events();
	void join_x_read_events()noexcept;

	template<typename _TxCommand,
		typename C = typename std::decay_t<_TxCommand>,
		typename std::enable_if_t<std::is_base_of_v<i_command_from_rt1_t, C>&& std::is_final_v<C>, int> = 0>
	void sndrcv_command_execute(_TxCommand && sourceCommand,
		__command_error_handler_point_f && resultHndl ) {

		CBL_VERIFY(resultHndl);

		auto resultHndl2 = crm::utility::bind_once_call({ "rt1_command_router_t::command execute" }, [](crm::async_operation_result_t rc,
			std::unique_ptr<i_command_from_rt02rt1_t>&& response,
			std::unique_ptr<i_command_result_t>&&,
			std::unique_ptr<dcf_exception_t>&& n,
			__command_error_handler_point_f&& errh) {

			if (rc == crm::async_operation_result_t::st_ready && !n) {
				errh(std::move(response));
				}
			else {
				errh(n ? std::move(n) : CREATE_MPF_PTR_EXC_FWD(nullptr));
				}
			}, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::move(resultHndl));

		xchannel_cmd_sndrcv_t::invoke_push(xchannel_cmd_sndrcv_make_tag,
			_ctx,
			weak_from_this(),
			std::forward< _TxCommand>(sourceCommand),
			std::move(resultHndl2));
		}

	template<typename _TxCommand,
		typename C = typename std::decay_t<_TxCommand>,
		typename std::enable_if_t<std::is_base_of_v<i_command_from_rt1_t, C>&& std::is_final_v<C>, int> = 0>
	void sndrcv_command_execute(_TxCommand && sourceCommand ) {
		if (auto hndl = std::dynamic_pointer_cast<xchannel_cmd_sndrcv_t::strong_handler_anchor_watch_t>(xchannel_cmd_sndrcv_t::invoke_binded(xchannel_cmd_sndrcv_make_tag,
			ctx(),
			shared_from_this(),
			std::forward< _TxCommand>(sourceCommand)))) {

			hndl->wait();
			if (hndl->code() == crm::async_operation_result_t::st_ready) {
				/* it's o'key! */
				}
			else if (auto e0 = hndl->captue_exc()) {
				if (auto e1 = dynamic_cast<dcf_exception_t*>(e0.get())) {
					e1->rethrow();
					}
				else {
					THROW_MPF_EXC_FWD(nullptr);
					}
				}
			else {
				THROW_MPF_EXC_FWD(nullptr);
				}
			}
		else {
			THROW_MPF_EXC_FWD(nullptr);
			}
		}
	
	void add( const std::list<rtx_command_type_t> & evl, std::unique_ptr<i_command_link_wrapper_t> && link );

	void __route2target( const rtl_roadmap_event_t & rdm,
		std::unique_ptr<i_command_from_rt02rt1_t> && response );
	void __route2target( std::shared_ptr<i_rt1_command_link_t> && lnk,
		std::unique_ptr<i_command_from_rt02rt1_t> && response);
	void __route2broadcast( std::map<rtl_roadmap_event_t, std::unique_ptr<i_command_link_wrapper_t>> & tbl,
		std::unique_ptr<i_command_from_rt02rt1_t> && response);

	void route_from_channel( std::unique_ptr<i_command_from_rt02rt1_t> && response);

	void ntf_hndl( std::unique_ptr<dcf_exception_t> && ntf )const noexcept;

	template<typename _TxCommand,
		typename C = typename std::decay_t<_TxCommand>,
		typename std::enable_if_t<std::is_base_of_v<i_command_from_rt1_t, C>&& std::is_final_v<C>, int> = 0>
	addon_push_event_result_t __push2channel_invoke( const std::shared_ptr<detail::io_x_rt1_channel_commands_t> & xch,
		_TxCommand && cmd )const noexcept {

		CBL_VERIFY(xch);
		addon_push_event_result_t r{ addon_push_event_result_t::enum_type::other };
		try {
			r = xch->push_to_rt0_command(std::forward<_TxCommand>(cmd));
			}
		catch (const dcf_exception_t& e0) {
			ntf_hndl(e0.dcpy());
			r = addon_push_event_result_t::enum_type::exception;
			}
		catch (const std::exception& e1) {
			ntf_hndl(CREATE_MPF_PTR_EXC_FWD(e1));
			r = addon_push_event_result_t::enum_type::exception;
			}
		catch (...) {
			ntf_hndl(CREATE_MPF_PTR_EXC_FWD(nullptr));
			r = addon_push_event_result_t::enum_type::exception;
			}

		return r;
		}

	template<typename _TxCommand,
		typename C = typename std::decay_t<_TxCommand>,
		typename std::enable_if_t<std::is_base_of_v<i_command_from_rt1_t, C>&& std::is_final_v<C>, int> = 0>
	addon_push_event_result_t push2channel( const std::shared_ptr<detail::io_x_rt1_channel_commands_t>& xch,
		_TxCommand && cmd )const noexcept {

		CBL_VERIFY(xch);
		CBL_VERIFY((!crm::is_null(cmd.command_source_emitter_rdm()) || is_without_response(cmd)));

		cmd.set_channel_id(xch->get_channel_info().id());
		return __push2channel_invoke(xch, std::forward<_TxCommand>(cmd));
		}

public:
	rt1_command_router_t( std::weak_ptr<distributed_ctx_t> ctx,
		std::shared_ptr<io_x_rt1_channel_commands_t> xChannel );
	void start();

	static std::shared_ptr<rt1_command_router_t> make( std::weak_ptr<distributed_ctx_t> ctx,
		std::shared_ptr<io_x_rt1_channel_commands_t> xChannel, 
		bool startNow = false );

	~rt1_command_router_t();
	void close()noexcept;

	void register2channel( const rtl_roadmap_obj_t & messageRDM,
		__command_error_handler_point_f && rh );
	void register2channel( std::list<rtx_command_type_t> && evlist,
		const rtl_roadmap_event_t & commandRDM,
		std::unique_ptr<i_command_link_wrapper_t> && link,
		__command_error_handler_point_f && rh );
	void register2channel( std::list<rtx_command_type_t> && evlist,
		const rtl_roadmap_event_t & commandRDM,
		const rtl_roadmap_obj_t & messageRDM,
		std::unique_ptr<i_command_link_wrapper_t> && link,
		__command_error_handler_point_f && rh );
	void register2channel( std::list<rtx_command_type_t> && evlist,
		const rtl_roadmap_event_t & commandRDM,
		const rtl_roadmap_obj_t & messageRDM,
		std::unique_ptr<i_command_link_wrapper_t> && link );
	
	void unregister_from_channel( const rtl_roadmap_event_t & commandRDM, 
		const rtl_roadmap_obj_t & messageRDM,
		unregister_message_track_reason_t r )noexcept;
	void unregister_from_channel( const rtl_roadmap_event_t & commandRDM )noexcept;
	void unregister_from_channel( const rtl_roadmap_obj_t & messageRDM, 
		unregister_message_track_reason_t r )noexcept;

	void unregister_from_channel( const rt1_endpoint_t & targetEP,
		const identity_descriptor_t & targetId,
		const source_object_key_t & srcKey )noexcept;

	void remove_by_id( const rtl_roadmap_event_t & objectId )noexcept;
	void remove_all()noexcept;

	template<typename _TxCommand,
		typename C = typename std::decay_t<_TxCommand>,
		typename std::enable_if_t<std::is_base_of_v<i_command_from_rt1_t, C>&& std::is_final_v<C>, int> = 0>
	addon_push_event_result_t push2channel(_TxCommand && cmd )const noexcept {
		if (auto xch = x_channel()) {
			return push2channel(xch, std::forward<_TxCommand>(cmd));
			}
		else {
			return addon_push_event_result_t::enum_type::link_unregistered;
			}
		}

	void bind( const std::list<rtx_command_type_t> & evl,
		std::unique_ptr<i_command_link_wrapper_t> && link );
	};
}


