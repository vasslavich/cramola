#pragma once

#include "./i_commands.h"


namespace crm::detail {


/*! ������� � ������ 0 �� ������� ������ 1 */
struct rt0_x_command_pack_t final {

public:
	enum class cmd_subtype {
		undefined,
		rt02rt1,
		fromrt1
		};


private:
	rtx_command_type_t _type = rtx_command_type_t::undefined;
	mutable binary_vector_t _pck;
	std::function<binary_vector_t(bool move_)> _serializer;
	std::unique_ptr<i_rt0_command_t> _pcmd;
	cmd_subtype _t{ cmd_subtype::undefined };

public:
	binary_vector_t at_binary()const;

private:
	const binary_vector_t& binary_srlz()const;

public:
	rt0_x_command_pack_t cpy_mem()const;
	cmd_subtype surtype()const noexcept;

	template<typename Tws>
	void srlz(Tws&& dest)const {
		srlz::serialize(dest, _type);
		srlz::serialize(dest, binary_srlz());
		}

	template<typename Trs>
	void dsrlz(Trs& src) {
		srlz::deserialize(src, _type);
		srlz::deserialize(src, _pck);
		}

	size_t get_serialized_size()const noexcept {
		return srlz::object_trait_serialized_size(_type)
			+ srlz::object_trait_serialized_size(binary_srlz());
		}

private:
	const i_rt0_command_t* pcmd()const noexcept;
	i_rt0_command_t* pcmd()noexcept;

public:
	rt0_x_command_pack_t()noexcept = default;

	rtx_command_type_t type()const noexcept;
	bool empty()const noexcept;

	rt0_x_command_pack_t(const rt0_x_command_pack_t&) = delete;
	rt0_x_command_pack_t& operator=(const rt0_x_command_pack_t&) = delete;

	rt0_x_command_pack_t( rt0_x_command_pack_t&&)noexcept = default;
	rt0_x_command_pack_t& operator=( rt0_x_command_pack_t&&)noexcept = default;

	const std::chrono::milliseconds& wait_timeout()const noexcept;


	//// rt0 -> rt1


	const rtl_roadmap_event_t& command_target_receiver_id()const noexcept;
	void set_target_receiver_id(const rtl_roadmap_event_t& targetId);

	const local_command_identity_t& source_command_id()const noexcept;
	void set_source_command_id(const local_command_identity_t& cmdid)noexcept;

	void set_invoked_opt(inovked_as)noexcept;
	inovked_as invoked_as()const noexcept;



	///  from rt1

	rtl_level_t level()const noexcept;

	const channel_identity_t& channel_id()const noexcept;
	void set_channel_id(const channel_identity_t& chId);

	const local_command_identity_t& command_id()const noexcept;
	void set_command_id(const local_command_identity_t& id);

	void set_command_evrdm(const rtl_roadmap_event_t& sourceObjectRdm);
	const rtl_roadmap_event_t& command_source_emitter_rdm()const noexcept;




	template<typename _TxCommand,
		typename C = typename std::decay_t<_TxCommand>,
		typename = std::enable_if_t<std::is_base_of_v<i_rt0_command_t, C>&& std::is_final_v<C>>>
		rt0_x_command_pack_t(_TxCommand&& c)noexcept
		: _type(c.type()) {

		//static_assert(is_null_trait_v<decltype(c.wait_timeout())>, __FILE_LINE__);
		//CBL_VERIFY(!is_null(c.wait_timeout()));

		if constexpr (std::is_base_of_v<i_command_from_rt02rt1_t, C>) {
			_t = cmd_subtype::rt02rt1;

			if (is_null(c.command_target_receiver_id()) && !is_broadcast_command(c)) {
				FATAL_ERROR_FWD(nullptr);
				}
			}
		else {
			if constexpr (std::is_base_of_v<i_command_from_rt1_t, C>) {
				_t = cmd_subtype::fromrt1;

				if (is_null(c.command_source_emitter_rdm()) && !is_without_response(c)) {
					FATAL_ERROR_FWD(nullptr);
					}
				}
			else {
				FATAL_ERROR_FWD(nullptr);
				}
			}

		_pcmd = std::make_unique<C>(std::forward<_TxCommand>(c));
		_serializer = [pcmd = _pcmd.get()](bool move_) {
			auto wstream(crm::srlz::wstream_constructor_t::make());

			/* write type descriptor of the command */
			srlz::serialize(wstream, srlz::typemap_key_t(srlz::i_type_ctr_base_t::make_key<C>()));

			/* write body of the command */
			if (move_) {
				if (auto pC = dynamic_cast<C*>(pcmd)) {
					srlz::serialize(wstream, std::move(*pC));
					}
				else {
					FATAL_ERROR_FWD(nullptr);
					}
				}
			else {
				if (auto pC = dynamic_cast<const C*>(pcmd)) {
					srlz::serialize(wstream, *pC);
					}
				else {
					FATAL_ERROR_FWD(nullptr);
					}
				}

			return wstream.release();
			};
		}

	std::unique_ptr<i_rt0_command_t> extract(const std::shared_ptr<distributed_ctx_t>& ctx) noexcept;
	};

struct command_rdm_t {
	rt0_x_command_pack_t cmd;
	rtl_roadmap_event_t target;

	//command_rdm_t()noexcept = default;

	//command_rdm_t(const command_rdm_t&) = delete;
	//command_rdm_t& operator=(const command_rdm_t&) = delete;

	//command_rdm_t(command_rdm_t&&)noexcept = default;
	//command_rdm_t& operator=(command_rdm_t&&)noexcept = default;
	};
}

