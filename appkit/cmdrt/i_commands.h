#pragma once


#include "./i_channel_base.h"


namespace crm::detail {


struct i_command_result_t {
	virtual ~i_command_result_t() = 0;
	};

struct i_rt0_command_t {
	virtual ~i_rt0_command_t() = 0;

	virtual std::unique_ptr<i_rt0_command_t> copy()const noexcept = 0;
	virtual rtx_command_type_t type()const = 0;

	const std::chrono::milliseconds& wait_timeout()const noexcept;
	void set_context(std::weak_ptr< distributed_ctx_t> ctx)noexcept;
	std::shared_ptr<distributed_ctx_t> ctx()const noexcept;

	template<typename TRep, typename TPeriod>
	void set_wait_timeout(const std::chrono::duration<TRep, TPeriod>& value)noexcept {
		_waitTimeout = value;
		}

	template<typename Ts>
	void srlz(Ts&& dest)const {
		srlz::serialize(dest, _waitTimeout);
		}

	template<typename Ts>
	void dsrlz(Ts&& src) {
		srlz::deserialize(src, _waitTimeout);
		}

	size_t get_serialized_size()const noexcept;

private:
	std::weak_ptr<distributed_ctx_t> _ctx;
	std::chrono::milliseconds _waitTimeout{ std::chrono::duration_cast<std::chrono::milliseconds>(CBL_TIMEOUT_IO_CHANNELS) };
	};
}
