#include <future>
#include "../../internal.h"
#include "../terms.h"
#include "../cmd_sndrcv.h"
#include "../rt1_commandrt.h"
#include "../../utilities/scopedin/postactor.h"
#include "../rtx_cmd.h"


using namespace crm;
using namespace crm::detail;



i_command_result_t::~i_command_result_t() {}


void xchannel_cmd_sndrcv_t::strong_handler_anchor_t::cancel()noexcept {
	if( auto sp =  std::atomic_exchange( &_spCmd, std::shared_ptr<xchannel_cmd_sndrcv_t>())) {
		CHECK_NO_EXCEPTION(sp->close());
		CHECK_NO_EXCEPTION(sp.reset());
		}
	}

xchannel_cmd_sndrcv_t::strong_handler_anchor_t::~strong_handler_anchor_t() {
	CHECK_NO_EXCEPTION(cancel());
	}

void xchannel_cmd_sndrcv_t::strong_handler_anchor_watch_t::cancel()noexcept {
	if(auto sp = std::atomic_exchange(&_spCmd, std::shared_ptr<xchannel_cmd_sndrcv_t>())) {
		CHECK_NO_EXCEPTION(sp->close());
		CHECK_NO_EXCEPTION(sp.reset());
		}
	}

xchannel_cmd_sndrcv_t::strong_handler_anchor_watch_t::~strong_handler_anchor_watch_t() {
	CHECK_NO_EXCEPTION(cancel());
	}


void xchannel_cmd_sndrcv_t::weak_handler_anchor_t::cancel()noexcept {
	if(auto p = _wpCmd.lock()) {
		CHECK_NO_EXCEPTION(p->close());
		}
	}

xchannel_cmd_sndrcv_t::weak_handler_anchor_t::~weak_handler_anchor_t() {
	CHECK_NO_EXCEPTION(cancel());
	}



xchannel_cmd_sndrcv_t::command_handler_watch_base_t::command_handler_watch_base_t()noexcept{}

void xchannel_cmd_sndrcv_t::command_handler_watch_base_t::assign( command_handler_watch_base_t && o )noexcept {
	_ntf = std::move( o._ntf );
	_response = std::move( o._response );
	_result = std::move( o._result );
	_code = std::move( o._code );
	_waitObject = std::move( o._waitObject );
	_setf = o._setf.load();
	_infokef = o._infokef.load();
	}

void xchannel_cmd_sndrcv_t::command_handler_watch_base_t::set_result( async_operation_result_t c,
	std::unique_ptr<i_command_from_rt02rt1_t> && response,
	std::unique_ptr<i_command_result_t> && r,
	std::unique_ptr<dcf_exception_t> && n ) {

	bool invf = false;
	if( _infokef.compare_exchange_strong( invf, true ) ) {
		_result = std::move( r );
		_response = std::move( response );
		_ntf = std::move( n );
		_code = c;

		_setf.store( true, std::memory_order::memory_order_release );
		}
	}

xchannel_cmd_sndrcv_t::command_handler_watch_base_t::command_handler_watch_base_t( command_handler_watch_base_t && o )noexcept {
	assign( std::move( o ) );
	}

xchannel_cmd_sndrcv_t::command_handler_watch_base_t& xchannel_cmd_sndrcv_t::command_handler_watch_base_t::operator=(command_handler_watch_base_t && o)noexcept {
	assign( std::move( o ) );
	return (*this);
	}

bool xchannel_cmd_sndrcv_t::command_handler_watch_base_t::wait( const std::chrono::milliseconds & timeout ) {
	return _waitObject.wait_for( timeout ) == std::future_status::ready;
	}

void xchannel_cmd_sndrcv_t::command_handler_watch_base_t::wait() {
	_waitObject.wait();
	}

std::unique_ptr<dcf_exception_t> xchannel_cmd_sndrcv_t::command_handler_watch_base_t::captue_exc()noexcept {
	if( _setf.load( std::memory_order::memory_order_acquire ) ) {
		return std::move( _ntf );
		}
	else {
		return nullptr;
		}
	}

std::unique_ptr<i_command_result_t> xchannel_cmd_sndrcv_t::command_handler_watch_base_t::capture_result()noexcept {
	if( _setf.load( std::memory_order::memory_order_acquire ) ) {
		return std::move( _result );
		}
	else {
		return nullptr;
		}
	}

std::unique_ptr<i_command_from_rt02rt1_t> xchannel_cmd_sndrcv_t::command_handler_watch_base_t::capture_response()noexcept {
	if( _setf.load( std::memory_order::memory_order_acquire ) ) {
		return std::move( _response );
		}
	else {
		return nullptr;
		}
	}

bool xchannel_cmd_sndrcv_t::command_handler_watch_base_t::ready()const noexcept {
	return _setf.load( std::memory_order::memory_order_acquire );
	}

async_operation_result_t xchannel_cmd_sndrcv_t::command_handler_watch_base_t::code()const noexcept {
	if( _setf.load( std::memory_order::memory_order_acquire ) ) {
		return _code;
		}
	else {
		return async_operation_result_t::st_exception;
		}
	}

xchannel_cmd_sndrcv_t::xchannel_cmd_sndrcv_t(
#ifdef CBL_MPF_OBJECTS_COUNTER_CHECKER_HOST_COMMAND
	std::string && tag,
#else
	std::string &&,
#endif

	std::weak_ptr<distributed_ctx_t> ctx_,
	std::weak_ptr<rt1_command_router_t> rt,
	response_verifier_t && rcvVrf,
	std::unique_ptr<i_result_hndl_invoker> && hndl,
	rtl_level_t l)
	: _ctx(ctx_)
	, _rdmEv(rtl_roadmap_event_t::make(identity_descriptor_t::null, CHECK_PTR(ctx_)->make_event_identity(), rtl_table_t::rt1_sndrcv_command, l))
	, _sndCommandId(CHECK_PTR(ctx_)->make_command_identity())
	, _router(rt)
	, _rcvVrf(std::move(rcvVrf))
	, _resultHndl(std::move(hndl))

#ifdef CBL_MPF_OBJECTS_COUNTER_CHECKER_HOST_COMMAND
	, _xDbgCounter(tag)
#endif

	{

	if(!_rcvVrf)
		_rcvVrf = make_default_verifier();

	if(!_resultHndl)
		THROW_MPF_EXC_FWD(nullptr);

#if defined(CBL_MPF_TRACELEVEL_COMMANDS_HOST) && defined(CBL_MPF_OBJECTS_COUNTER_CHECKER_HOST_COMMAND)
	_tag = std::string("xchannel_cmd_sndrcv_t:");
	_tag += move(tag);
	_tag += ":";
	_tag += std::to_string(_xDbgCounter.this_counter()) + ":" + std::to_string(_xDbgCounter.type_counter());

	crm::file_logger_t::logging((_tag + ":ctr"), 1050, __CBL_FILEW__, __LINE__);
#endif
	}

void xchannel_cmd_sndrcv_t::register_routing(anchor_mode acMode) {
	auto rt_(rt());
	if(rt_) {
		if(anchor_mode::route_table == acMode) {
			rt_->bind({}, std::make_unique<command_link_wrapper_strong_t>(shared_from_this()));
			}
		else {
			rt_->bind({}, std::make_unique<command_link_wrapper_weak_t>(weak_from_this()));
			}
		}
	else {
		THROW_MPF_EXC_FWD(nullptr);
		}
	}

bool xchannel_cmd_sndrcv_t::check_set_unregf()noexcept {
	bool f = false;
	return _unregistered.compare_exchange_strong( f, true );
	}

void xchannel_cmd_sndrcv_t::unroute_command_link()noexcept {
	CHECK_NO_EXCEPTION(check_set_unregf());
	}

bool xchannel_cmd_sndrcv_t::is_once()const noexcept {
	return true;
	}

const rtl_roadmap_event_t& xchannel_cmd_sndrcv_t::ev_rdm()const noexcept {
	return _rdmEv;
	}

const rtl_roadmap_obj_t& xchannel_cmd_sndrcv_t::msg_rdm()const noexcept {
	return rtl_roadmap_obj_t::null;
	}

const local_command_identity_t& xchannel_cmd_sndrcv_t::local_object_command_id()const noexcept {
	static_assert(std::is_lvalue_reference_v<decltype(ev_rdm().lcid())>, __CBL_FILEW__);
	return ev_rdm().lcid();
	}

std::shared_ptr<distributed_ctx_t> xchannel_cmd_sndrcv_t::ctx() const noexcept {
	return _ctx.lock();
	}

std::shared_ptr<rt1_command_router_t> xchannel_cmd_sndrcv_t::rt()const noexcept {
	return _router.lock();
	}

void xchannel_cmd_sndrcv_t::rt1_router_hndl( std::unique_ptr<i_command_from_rt02rt1_t> && response)noexcept {

#if defined(CBL_MPF_TRACELEVEL_COMMAND_STAGE_PRINT)
	CHECK_NO_EXCEPTION(crm::file_logger_t::logging((_tag + ":route handle"), 1052, __CBL_FILEW__, __LINE__));
#endif

	if(response->command_target_receiver_id() == ev_rdm() || is_broadcast_command(response) ) {
		CHECK_NO_EXCEPTION(invoke_hndl(async_operation_result_t::st_ready, std::move(response)));
		}
	else {

		/* ��������� �������� - ������������ ������������� ������ �������� �����
		-----------------------------------------------------------------------*/
		FATAL_ERROR_FWD(nullptr);
		}
	}

void xchannel_cmd_sndrcv_t::invoke_hndl(async_operation_result_t)noexcept {
	if(invoke_test_and_set()) {
		if(_resultHndl) {
			decltype(_resultHndl) rh;
			std::swap(rh, _resultHndl);

			(*rh)(async_operation_result_t::st_abandoned, nullptr, nullptr, CREATE_ABANDONED_PTR_EXC_FWD(nullptr));
			}
		else {
			FATAL_ERROR_FWD(nullptr);
			}
		}
	}

void xchannel_cmd_sndrcv_t::invoke_hndl(std::unique_ptr<dcf_exception_t> && e)noexcept {
	if(invoke_test_and_set()) {
		if(_resultHndl) {
			decltype(_resultHndl) rh;
			std::swap(rh, _resultHndl);

			if(!e) {
				e = CREATE_MPF_PTR_EXC_FWD(nullptr);
				}

			(*rh)(async_operation_result_t::st_exception, nullptr, nullptr, std::move(e));
			}
		else {
			FATAL_ERROR_FWD(nullptr);
			}
		}
	}

void xchannel_cmd_sndrcv_t::invoke_hndl(async_operation_result_t rc_,
	std::unique_ptr<i_command_from_rt02rt1_t> && response ) noexcept {

	if(invoke_test_and_set()) {

		/* �������� ���������� ������� � ������������� ������������ ������ �� ������ */
		post_scope_action_t dltQueue([this] {
			timer_stop();
			});

#if defined(CBL_MPF_TRACELEVEL_COMMAND_STAGE_PRINT)
		CHECK_NO_EXCEPTION(crm::file_logger_t::logging((_tag + ":invoke_hndl[" + cvt2string(rc_) + "]"), 1053, __CBL_FILEW__, __LINE__));
#endif

		if(response) {
			if(response->invoked_as() == inovked_as::async) {
				__invoke_hndl_sync_(rc_, std::move(response));
				}
			else {
				__invoke_hndl_async_(rc_, std::move(response));
				}
			}
		else {
			__invoke_hndl_sync_(rc_, std::move(response));
			}
		}
	}

bool xchannel_cmd_sndrcv_t::invoke_test_and_set()noexcept {
	bool done = false;
	if(_hndlHasDone.compare_exchange_strong(done, true)) {

		/* �������� ���������� ������� � ������������� ������������ ������ �� ������ */
		timer_stop();

		return true;
		}
	else {
		return false;
		}
	}

void xchannel_cmd_sndrcv_t::__invoke_hndl_sync_(async_operation_result_t rc_,
	std::unique_ptr<i_command_from_rt02rt1_t> && response) noexcept {

	CBL_NO_EXCEPTION_BEGIN

	if(_resultHndl) {
		decltype(_resultHndl) rh;
		std::swap(rh, _resultHndl);

		decltype(_rcvVrf) rv;
		std::swap(rv, _rcvVrf);

		std::unique_ptr<i_command_result_t> result;
		std::unique_ptr<dcf_exception_t> exc;

		if(auto excResponse = dynamic_cast<const i_rt0_exception_t*>(response.get())) {
			exc = excResponse->exc();
			if(!exc) {
				exc = CREATE_MPF_PTR_EXC_FWD(nullptr);
				}
			}
		else if(rc_ == async_operation_result_t::st_ready) {
			try {
				result = rv(response);
				}
			catch(const dcf_exception_t & n0) {
				exc = n0.dcpy();
				}
			catch(const std::exception & exc1) {
				exc = CREATE_MPF_PTR_EXC_FWD(exc1);
				}
			catch(...) {
				exc = CREATE_MPF_PTR_EXC_FWD(nullptr);
				}
			}
		else {
			exc = CREATE_MPF_PTR_EXC_FWD(std::string("exception:code operation=") + cvt2string(rc_));
			}

		(*rh)(rc_, std::move(response), std::move(result), std::move(exc));
		}
	else {
		FATAL_ERROR_FWD(nullptr);
		}

	CBL_NO_EXCEPTION_END(ctx())
	}

void xchannel_cmd_sndrcv_t::__invoke_hndl_async_(async_operation_result_t rc_,
	std::unique_ptr<i_command_from_rt02rt1_t> && response) noexcept {
	
	if(_resultHndl) {
		if(auto ctx_ = ctx()) {
			ctx_->launch_async(__FILE_LINE__, [rc_](
				std::unique_ptr<i_result_hndl_invoker> && hndl,
				response_verifier_t && verifier,
				std::unique_ptr<i_command_from_rt02rt1_t> && response_) {

				std::unique_ptr<i_command_result_t> result;
				std::unique_ptr<dcf_exception_t> exc;
				async_operation_result_t rc = rc_;

				if(hndl) {
					post_scope_action_t hndlInvoker([&rc, &hndl, &result, &exc, &response_] {
						if(hndl) {
							(*hndl)(rc, std::move(response_), std::move(result), std::move(exc));
							}
						});

					if(auto excResponse = dynamic_cast<const i_rt0_exception_t*>(response_.get())) {
						exc = excResponse->exc();
						if(!exc) {
							exc = CREATE_MPF_PTR_EXC_FWD(nullptr);
							}
						}
					else if(rc == async_operation_result_t::st_ready) {
						try {
							result = verifier(response_);
							}
						catch(const dcf_exception_t & n0) {
							exc = n0.dcpy();
							}
						catch(const std::exception & exc1) {
							exc = CREATE_MPF_PTR_EXC_FWD(exc1);
							}
						catch(...) {
							exc = CREATE_MPF_PTR_EXC_FWD(nullptr);
							}
						}
					else {
						exc = CREATE_MPF_PTR_EXC_FWD(std::string("exception:code operation=") + cvt2string(rc));
						}
					}

				}, std::move(_resultHndl), std::move(_rcvVrf), std::move(response));
			}
		}
	else {
#ifdef CBL_MPF_TRACELEVEL_COMMAND_STAGE_PRINT
		CHECK_NO_EXCEPTION(crm::file_logger_t::logging("no hanlder", 1055, __CBL_FILEW__, __LINE__));
#endif
		}
	}

void xchannel_cmd_sndrcv_t::timer_invoke(async_operation_result_t rc) noexcept{
	if(rc == async_operation_result_t::st_ready) {
		invoke_hndl(async_operation_result_t::st_timeouted, nullptr);
		}
	else {
		invoke_hndl(rc, nullptr);
		}
	}

void xchannel_cmd_sndrcv_t::start_timer(const sndrcv_timeline_t & tml, anchor_mode acMode) {
	if(anchor_mode::timer == acMode) {
		if(tml.is_timeouted()) {

			CBL_VERIFY(!_timer);
			_timer = CHECK_PTR(ctx())->create_timer_oncecall_sys();

			/* ������ ������� */
			if(anchor_mode::timer == acMode) {
				_timer->start(
					/* ������������ ������ �� ������ */
					[sptr = shared_from_this()](async_operation_result_t rc)noexcept {
					sptr->timer_invoke(rc);
					},
					tml.timeout());
				}
			else {
				_timer->start(
					[wptr = weak_from_this()](async_operation_result_t rc)noexcept {
					if(auto sptr = wptr.lock()) {
						sptr->timer_invoke(rc);
						}
					},
					tml.timeout());
				}
			}
		else {
			FATAL_ERROR_FWD(nullptr);
			}
		}
	}


xchannel_cmd_sndrcv_t::response_verifier_t  xchannel_cmd_sndrcv_t::make_default_verifier()const {

	auto srcCmdId = _sndCommandId;
	return [srcCmdId](const std::unique_ptr<i_command_from_rt02rt1_t> & cmd) {
		if(auto excResult = dynamic_cast<const i_rt0_exception_t*>(cmd.get())) {
			auto exc = excResult->exc();
			if(auto isExc = dynamic_cast<dcf_exception_t*>(exc.get())) {
				isExc->rethrow();

				/* compiler error only */
				FATAL_ERROR_FWD(nullptr);
				}
			else {
				THROW_EXC_FWD((exc ? exc->msg() : L""));
				}
			}
		else if(auto responseCmd = dynamic_cast<const i_rt0_cmd_sndrcv_result_t*>(cmd.get())) {
			if(srcCmdId == responseCmd->source_command_id())
				return nullptr;
			else {
				THROW_EXC_FWD(nullptr);
				}
			}
		else {
			THROW_EXC_FWD(nullptr);
			}
		};
	}

std::shared_ptr<xchannel_cmd_sndrcv_t::strong_handler_anchor_t> xchannel_cmd_sndrcv_t::make_as_strong_anchor(std::string && tag,
	std::weak_ptr<distributed_ctx_t> ctx,
	std::weak_ptr<rt1_command_router_t> rt,
	response_verifier_t && rcvVrf,
	std::unique_ptr<i_result_hndl_invoker> && hndl,
	rtl_level_t cmdLevel) {
	
	auto obj = std::make_shared< xchannel_cmd_sndrcv_t>(std::move(tag), ctx, rt, std::move(rcvVrf), std::move(hndl), cmdLevel);
	auto h = std::make_shared<strong_handler_anchor_t>();

	h->_spCmd = std::move(obj);

	CBL_VERIFY(h->_spCmd.use_count() == 1);
	return h;
	}

std::shared_ptr<xchannel_cmd_sndrcv_t::strong_handler_anchor_watch_t> xchannel_cmd_sndrcv_t::make_as_strong_anchor_watch(
	std::string && tag,
	std::weak_ptr<distributed_ctx_t> ctx,
	std::weak_ptr<rt1_command_router_t> rt,
	response_verifier_t && rcvVrf,
	std::unique_ptr<i_result_hndl_invoker> && hndl_,
	rtl_level_t cmdLevel) {

	std::promise<void> p;
	auto fut = p.get_future();

	auto task = utility::bind_once_call( "xchannel_cmd_sndrcv_t::make_as_strong_anchor_watch 1", [](
		async_operation_result_t c, 
		std::unique_ptr<i_command_from_rt02rt1_t> && response, 
		std::unique_ptr<i_command_result_t> && r, 
		std::unique_ptr<dcf_exception_t> && n,
		std::unique_ptr<i_result_hndl_invoker> && hndl_, 
		std::promise<void> && p_ ) {
		try {
			if(hndl_) {
				(*hndl_)(c, std::move(response), std::move(r), std::move(n));
				hndl_ = nullptr;

				p_.set_value();
				}
			}
		catch(const dcf_exception_t & e0) {
			p_.set_exception(std::make_exception_ptr(e0));
			}
		catch(const std::exception & e1) {
			p_.set_exception(std::make_exception_ptr(e1));
			}
		catch(...) {
			p_.set_exception(std::current_exception());
			}

		}, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::move(hndl_), std::move(p));

	auto h(std::make_shared<xchannel_cmd_sndrcv_t::strong_handler_anchor_watch_t>());
	h->_waitObject = std::move(fut);

	auto obj(std::make_shared< xchannel_cmd_sndrcv_t>(std::move(tag), ctx, rt, std::move(rcvVrf), make_result_handler(std::move(task)), cmdLevel));
	h->_spCmd = std::move(obj);

	CBL_VERIFY(h->_spCmd.use_count() == 1);
	return h;

	}
std::shared_ptr<xchannel_cmd_sndrcv_t::strong_handler_anchor_watch_t> xchannel_cmd_sndrcv_t::make_as_strong_anchor_watch(
	std::string&& tag,
	std::weak_ptr<distributed_ctx_t> ctx,
	std::weak_ptr<rt1_command_router_t> rt,
	response_verifier_t&& rcvVrf,
	rtl_level_t cmdLevel) {

	auto h(std::make_shared<xchannel_cmd_sndrcv_t::strong_handler_anchor_watch_t>());
	std::weak_ptr<xchannel_cmd_sndrcv_t::strong_handler_anchor_watch_t> wh(h);

	std::promise<void> p;
	auto fut = p.get_future();

	auto task = utility::bind_once_call("xchannel_cmd_sndrcv_t::make_as_strong_anchor_watch 2", [wh](
		async_operation_result_t c, 
		std::unique_ptr<i_command_from_rt02rt1_t> && response,
		std::unique_ptr<i_command_result_t> && r,
		std::unique_ptr<dcf_exception_t> && n,
		std::promise<void> && p_) {

		try {
			auto sh(wh.lock());
			if(sh) {
				sh->set_result(c, std::move(response), std::move(r), std::move(n));
				p_.set_value();
				}

			}
		catch(const dcf_exception_t & e0) {
			p_.set_exception(std::make_exception_ptr(e0));
			}
		catch(const std::exception & e1) {
			p_.set_exception(std::make_exception_ptr(e1));
			}
		catch(...) {
			p_.set_exception(std::current_exception());
			}
		}, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::move(p));
	h->_waitObject = std::move(fut);

	auto obj(std::make_shared< xchannel_cmd_sndrcv_t>(std::move(tag), ctx, rt, std::move(rcvVrf),make_result_handler(std::move(task)), cmdLevel));
	h->_spCmd = std::move(obj);

	CBL_VERIFY(h->_spCmd.use_count() == 1);
	return h;
	}

std::shared_ptr<xchannel_cmd_sndrcv_t::strong_handler_anchor_watch_t> xchannel_cmd_sndrcv_t::make_as_strong_anchor_watch(
	std::string && tag,
	std::weak_ptr<distributed_ctx_t> ctx,
	std::weak_ptr<rt1_command_router_t> rt,
	rtl_level_t cmdLevel) {

	return make_as_strong_anchor_watch(std::move(tag), std::move(ctx), std::move(rt), nullptr, cmdLevel);
	}

std::pair<std::shared_ptr<xchannel_cmd_sndrcv_t>, std::shared_ptr<xchannel_cmd_sndrcv_t::weak_handler_anchor_t>> 
xchannel_cmd_sndrcv_t::make_as_weak_anchor(
	std::string && tag,
	std::weak_ptr<distributed_ctx_t> ctx,
	std::weak_ptr<rt1_command_router_t> rt,
	response_verifier_t && rcvVrf,
	std::unique_ptr<i_result_hndl_invoker> && hndl,
	rtl_level_t cmdLevel ) {

	auto obj = std::make_shared< xchannel_cmd_sndrcv_t>(std::move(tag), std::move(ctx), std::move(rt), std::move(rcvVrf), std::move(hndl), cmdLevel);
	auto h = std::make_shared<weak_handler_anchor_t>();

	h->_wpCmd = obj;

	CBL_VERIFY(obj.use_count() == 1);
	return std::make_pair(std::move(obj), std::move(h));
	}


bool xchannel_cmd_sndrcv_t::canceled()noexcept {
	return _canceled.load( std::memory_order::memory_order_acquire );
	}

void xchannel_cmd_sndrcv_t::timer_stop()noexcept {
	if( _timer ) {
		CHECK_NO_EXCEPTION( _timer->stop() );
		}
	}

void xchannel_cmd_sndrcv_t::close() noexcept {
	_canceled.store(true, std::memory_order::memory_order_release);

	invoke_hndl(async_operation_result_t::st_abandoned, nullptr);
	timer_stop();
	unregistrer_routing();
	}

void xchannel_cmd_sndrcv_t::unregistrer_routing() noexcept{
	if( check_set_unregf() ) {

		auto rt( _router.lock() );
		if( rt ) {
			auto ctx_( ctx() );
			if( ctx_ ) {

				auto rdm = ev_rdm();
				ctx_->launch_async( __FILE_LINE__, [rt, rdm] { rt->remove_by_id( rdm ); } );
				}
			}
		}
	}

void xchannel_cmd_sndrcv_t::_dctr() noexcept{
	close();
	}

xchannel_cmd_sndrcv_t::~xchannel_cmd_sndrcv_t() {
 	_dctr();
	}

bool xchannel_cmd_sndrcv_t::__validate_cmd( const i_command_from_rt1_t & cmd )const noexcept{
	return is_with_response(std::addressof(cmd));
	}

