#include "../../internal.h"
#include "../rtx_cmd.h"
#include "../../notifications/internal_terms.h"
#include "../../notifications/exceptions/excdrv.h"
#include "../../context/context.h"
#include "../../typeframe.h"


using namespace crm;
using namespace crm::detail;


i_command_from_rt02rt1_t::i_command_from_rt02rt1_t() noexcept{}

void i_command_from_rt02rt1_t::set_invoked_opt(inovked_as ia)noexcept {
	_is = ia;
	}

inovked_as i_command_from_rt02rt1_t::invoked_as()const noexcept {
	return _is;
	}

const rtl_roadmap_event_t& i_command_from_rt02rt1_t::command_target_receiver_id()const noexcept {
	return _targetId;
	}

void i_command_from_rt02rt1_t::set_target_receiver_id( const rtl_roadmap_event_t & targetId ) {
	_targetId = targetId;
	}

i_command_from_rt02rt1_t::i_command_from_rt02rt1_t( const rtl_roadmap_event_t & tid )
	: _targetId( tid ) {}


const local_command_identity_t& i_command_from_rt02rt1_t::source_command_id()const noexcept {
	return _sourceCommandId;
	}

void i_command_from_rt02rt1_t::set_source_command_id( const local_command_identity_t& cmdid )noexcept {
	_sourceCommandId = cmdid;
	}

size_t i_command_from_rt02rt1_t::get_serialized_size()const noexcept{
	return 	i_rt0_command_t::get_serialized_size() 
		+ srlz::object_trait_serialized_size(_targetId)
		+ srlz::object_trait_serialized_size(_sourceCommandId);
	}




i_rt0_cmd_outcoming_oppened_t::i_rt0_cmd_outcoming_oppened_t( const rt0_node_rdm_t &nodeId_,
	const rtl_roadmap_event_t& tid_,
	const rt1_endpoint_t & ep_,
	const _address_event_t & subscriber4WaitConnection )
	: base_type( tid_ )
	, _nodeId( nodeId_ )
	, _ep( ep_ )
	, _subscriber4WaitConnection( subscriber4WaitConnection ) {}

size_t i_rt0_cmd_outcoming_oppened_t::get_serialized_size()const noexcept {

	return base_type::get_serialized_size()
		+ srlz::object_trait_serialized_size( _nodeId )
		+ srlz::object_trait_serialized_size( _ep )
		+ srlz::object_trait_serialized_size( _subscriber4WaitConnection );
	}

const _address_event_t& i_rt0_cmd_outcoming_oppened_t::subscriber_for_wait_connection()const noexcept {
	return _subscriber4WaitConnection;
	}

const rt1_endpoint_t& i_rt0_cmd_outcoming_oppened_t::endpoint()const noexcept {
	return _ep;
	}

std::unique_ptr<i_rt0_command_t> i_rt0_cmd_outcoming_oppened_t::copy()const noexcept {
	return std::make_unique<i_rt0_cmd_outcoming_oppened_t>( i_rt0_cmd_outcoming_oppened_t( *this ) );
	}

const rt0_node_rdm_t& i_rt0_cmd_outcoming_oppened_t::node_id() const noexcept {
	return _nodeId;
	}

std::shared_ptr<exceptions_driver_t> i_rt0_exception_t::excdrv()const noexcept {
	if(auto ctx_ = ctx()) {
		auto td( ctx_->types_driver() );
		if( td ) {
			return td->exceptions_driver();
			}
		else
			return nullptr;
		}
	else {
		return nullptr;
		}
	}

i_rt0_exception_t::i_rt0_exception_t( )noexcept {}

i_rt0_exception_t::i_rt0_exception_t( rtx_exception_code_t code,
	std::unique_ptr<dcf_exception_t> && exc,
	const rtl_roadmap_event_t & tid )noexcept
	: base_type( tid )
	, _exc( std::move(exc) )
	, _code( code ){

	CBL_VERIFY( !is_null( command_target_receiver_id() ) );
	
	if( _exc ) {
		auto excDrv = excdrv();
		if( excDrv ) {
			auto ws = srlz::wstream_constructor_t::make();
			excDrv->serialize(ws, (*_exc));

			_bin = ws.release();
			}
		}
	}

size_t i_rt0_exception_t::get_serialized_size()const noexcept{

	return base_type::get_serialized_size()
		+ srlz::object_trait_serialized_size( _code )
		+ srlz::object_trait_serialized_size( _bin );
	}

const rtx_exception_code_t& i_rt0_exception_t::code()const noexcept {
	return _code;
	}

std::unique_ptr<dcf_exception_t> i_rt0_exception_t::exc()const {
	if( !_exc ) {
		if( !_bin.empty() ) {

			auto excDrv = CHECK_PTR( CHECK_PTR( ctx() )->types_driver() )->exceptions_driver();
			if( excDrv ) {
				auto rs = srlz::rstream_constructor_t::make(_bin.cbegin(), _bin.cend());
				_exc = excDrv->deserialize( rs );

				return _exc->dcpy();
				}
			else {
				FATAL_ERROR_FWD(nullptr);
				}
			}
		else {
			return {};
			}
		}
	else {
		return _exc->dcpy();
		}
	}

std::unique_ptr<i_rt0_command_t> i_rt0_exception_t::copy()const noexcept {
	return std::make_unique<i_rt0_exception_t>( i_rt0_exception_t( *this ) );
	}



i_rt0_cmd_incoming_closed_t::i_rt0_cmd_incoming_closed_t( const rt0_node_rdm_t & ep_, const rtl_roadmap_event_t &tid )
	: base_type( tid )
	, _nodeId( ep_ ) {}

size_t i_rt0_cmd_incoming_closed_t::get_serialized_size()const noexcept {
	return base_type::get_serialized_size()
		+ srlz::object_trait_serialized_size( _nodeId );
	}

std::unique_ptr<i_rt0_command_t> i_rt0_cmd_incoming_closed_t::copy()const noexcept {
	return std::make_unique<i_rt0_cmd_incoming_closed_t>( i_rt0_cmd_incoming_closed_t( *this ) );
	}

const rt0_node_rdm_t& i_rt0_cmd_incoming_closed_t::node_id()const noexcept {
	return _nodeId;
	}




i_rt0_cmd_outcoming_closed_t::i_rt0_cmd_outcoming_closed_t( const rt0_node_rdm_t & ep, const rtl_roadmap_event_t &tid )
	: base_type( tid )
	, _nodeId( ep ) {}

size_t i_rt0_cmd_outcoming_closed_t::get_serialized_size()const noexcept{
	return base_type::get_serialized_size()
		+ srlz::object_trait_serialized_size( _nodeId );
	}

std::unique_ptr<i_rt0_command_t> i_rt0_cmd_outcoming_closed_t::copy()const noexcept {
	return std::make_unique<i_rt0_cmd_outcoming_closed_t>( (*this) );
	}

const rt0_node_rdm_t& i_rt0_cmd_outcoming_closed_t::node_id()const noexcept {
	return _nodeId;
	}



i_rt0_invalid_xpeer_t::i_rt0_invalid_xpeer_t()noexcept {}

i_rt0_invalid_xpeer_t::i_rt0_invalid_xpeer_t( rtl_roadmap_obj_t && l )noexcept
	: i_is_broadcast_command_t( rtl_roadmap_event_t::null )
	, _rdmlink( std::move(l) ) {}

const rtl_roadmap_obj_t& i_rt0_invalid_xpeer_t::target_rdm()const noexcept {
	return _rdmlink;
	}

std::unique_ptr<i_rt0_command_t> i_rt0_invalid_xpeer_t::copy()const noexcept {
	return std::make_unique<i_rt0_invalid_xpeer_t>( i_rt0_invalid_xpeer_t( *this ) );
	}

size_t i_rt0_invalid_xpeer_t::get_serialized_size()const noexcept{
	return base_type::get_serialized_size()
		+ srlz::object_trait_serialized_size( _rdmlink );
	}




i_rt0_cmd_sndrcv_result_t::i_rt0_cmd_sndrcv_result_t() {}
i_rt0_cmd_sndrcv_result_t::i_rt0_cmd_sndrcv_result_t( const rtl_roadmap_event_t & targetRdm,
	const std::string & message )
	: base_type( targetRdm )
	, _message(message){}

const std::string& i_rt0_cmd_sndrcv_result_t::message()const noexcept {
	return _message;
	}

std::unique_ptr<i_rt0_command_t> i_rt0_cmd_sndrcv_result_t::copy()const noexcept {
	return std::make_unique<i_rt0_cmd_sndrcv_result_t>( i_rt0_cmd_sndrcv_result_t( *this ) );
	}

size_t i_rt0_cmd_sndrcv_result_t::get_serialized_size()const noexcept{
	return base_type::get_serialized_size()
		+ srlz::object_trait_serialized_size( _message );
	}


i_rt0_cmd_sndrcv_fault_t::i_rt0_cmd_sndrcv_fault_t(const distributed_ctx_t & ctx, 
	outbound_message_desc_t && sndrcvh,
	std::unique_ptr<dcf_exception_t> && e )noexcept
	: _sndrcvh(std::move(sndrcvh)) {
	
	if(e) {
		if(auto pTDrv = ctx.types_driver()) {
			auto ws = srlz::wstream_constructor_t::make();
			if(dcf_exception_t::serialize((*pTDrv), ws, (*e))) {

				_pckexc = ws.release();
				}
			}
		}
	}

void i_rt0_cmd_sndrcv_fault_t::unpack(const distributed_ctx_t & ctx) {
	if(auto pTDrv = ctx.types_driver()) {
		if(!_pckexc.empty()) {
			auto rs = srlz::rstream_constructor_t::make(_pckexc.cbegin(), _pckexc.cend());
			_e = dcf_exception_t::deserialize((*pTDrv), rs);
			}
		}
	}

std::unique_ptr<dcf_exception_t> i_rt0_cmd_sndrcv_fault_t::ntf() && noexcept {
	return std::move(_e);
	}

const std::unique_ptr<dcf_exception_t>& i_rt0_cmd_sndrcv_fault_t::ntf()const & noexcept {
	return _e;
	}


const outbound_message_desc_t& i_rt0_cmd_sndrcv_fault_t::hndl()const noexcept {
	return _sndrcvh;
	}

std::unique_ptr<i_rt0_command_t> i_rt0_cmd_sndrcv_fault_t::copy()const noexcept{
	auto p = std::make_unique<i_rt0_cmd_sndrcv_fault_t>();
	p->_sndrcvh = _sndrcvh;
	p->_pckexc = _pckexc;

	return p;
	}

size_t i_rt0_cmd_sndrcv_fault_t::get_serialized_size()const noexcept {
	return srlz::object_trait_serialized_size(_sndrcvh)
		+ srlz::object_trait_serialized_size(_pckexc);
	}