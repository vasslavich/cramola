#include "../../internal.h"
#include "../terms.h"
#include "../rt1_commandrt.h"


using namespace crm;
using namespace crm::detail;


#ifdef CBL_OBJECTS_COUNTER_CHECKER
void rt1_command_router_t::__size_trace()const noexcept{

	size_t s0 = 0;
	size_t s1 = 0;
	for( const auto & i0 : _cbTbl ) {
		++s0;
		s1 += i0.second.size();
		}

	CHECK_NO_EXCEPTION(__sizeTraceer.CounterCheck(/*s0 + */s1));
	}
#endif


command_link_wrapper_weak_t::command_link_wrapper_weak_t()noexcept{}

command_link_wrapper_weak_t::command_link_wrapper_weak_t( std::weak_ptr<i_rt1_command_link_t> ptr_ )
	: ptr( std::move( ptr_) ) {

	if( ptr.expired() )
		THROW_MPF_EXC_FWD(nullptr);
	}

std::shared_ptr<i_rt1_command_link_t> command_link_wrapper_weak_t::link()const & noexcept { return ptr.lock(); }
std::shared_ptr<i_rt1_command_link_t> command_link_wrapper_weak_t::link()&& noexcept { return ptr.lock(); }

i_command_link_type_t command_link_wrapper_weak_t::type()const noexcept { return i_command_link_type_t::weak; }

std::unique_ptr<i_command_link_wrapper_t> command_link_wrapper_weak_t::copy()const noexcept {
	auto p = std::make_unique<command_link_wrapper_weak_t>();
	p->ptr = ptr;

	return p;
	}


command_link_wrapper_strong_t::command_link_wrapper_strong_t()noexcept {}

command_link_wrapper_strong_t::command_link_wrapper_strong_t( std::shared_ptr<i_rt1_command_link_t> ptr_ )
	: ptr( ptr_ ) {

	if( !ptr )
		THROW_MPF_EXC_FWD(nullptr);
	}

std::shared_ptr<i_rt1_command_link_t> command_link_wrapper_strong_t::link()const & noexcept { return ptr; }
std::shared_ptr<i_rt1_command_link_t> command_link_wrapper_strong_t::link()&& noexcept { return std::move(ptr); }

i_command_link_type_t command_link_wrapper_strong_t::type()const noexcept { return i_command_link_type_t::shared; }

std::unique_ptr<i_command_link_wrapper_t> command_link_wrapper_strong_t::copy()const noexcept {
	auto p = std::make_unique<command_link_wrapper_strong_t>();
	p->ptr = ptr;

	return p;
	}

void rt1_command_router_t::unbind_link_async( std::shared_ptr<i_rt1_command_link_t> && wobj )const noexcept {

	if( wobj ) {

		auto ctx_( _ctx.lock() );
		if( ctx_ ) {
			ctx_->launch_async( __FILE_LINE__, []
				( std::shared_ptr<i_rt1_command_link_t> && wobj ) {

				if( wobj ) {
					unbind_link_invoke( std::move(wobj) );
					}

				}, std::move( wobj ) );
			}
		}
	}

void rt1_command_router_t::unbind_link_invoke( std::shared_ptr<i_rt1_command_link_t> p ) noexcept {
	if(p) {
		p->unroute_command_link();
		}
	}

rt1_command_router_t::rt1_command_router_t( std::weak_ptr<distributed_ctx_t> ctx,
	std::shared_ptr<io_x_rt1_channel_commands_t> xChannel )
	: _ctx( ctx )
	, _xChannel( std::move( xChannel ) ){}

std::shared_ptr<rt1_command_router_t> rt1_command_router_t::make( std::weak_ptr<distributed_ctx_t> ctx,
	std::shared_ptr<io_x_rt1_channel_commands_t> xChannel,
	bool startNow ) {
	
	auto obj = std::make_shared<rt1_command_router_t>( ctx, xChannel );
	if( startNow )
		obj->start();

	return std::move( obj );
	}

rt1_command_router_t::~rt1_command_router_t() {
	CHECK_NO_EXCEPTION( close() );
	}

void rt1_command_router_t::close() noexcept {
	bool cancelf = false;
	if( _cancelf.compare_exchange_strong( cancelf, true ) ) {
		CHECK_NO_EXCEPTION( remove_all() );
		CHECK_NO_EXCEPTION( join_x_read_events() );
		}
	}

void rt1_command_router_t::remove_all()noexcept {
	lck_scp_t lck( _cbTblLck );

	auto evIt = _cbTbl.begin();
	for( ; evIt != _cbTbl.end(); ++evIt ) {

		for( auto rdmIt = evIt->second.begin(); rdmIt != evIt->second.end(); ++rdmIt ) {
			if( rdmIt->second ) {
				CHECK_NO_EXCEPTION( unbind_link_async( rdmIt->second->link() ) );
				}
			}

		(*evIt).second.clear();
		}

	_cbTbl.clear();

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	CHECK_NO_EXCEPTION( __size_trace() );
#endif
	}

bool rt1_command_router_t::canceled()const noexcept{
	return _cancelf.load( std::memory_order::memory_order_acquire );
	}

std::list<rtx_command_type_t> evl_remove_duplicate( const std::list<rtx_command_type_t> & evl ) {
	std::list<rtx_command_type_t> rl;
	for( const auto & ie : evl ) {
		if( std::find( rl.cbegin(), rl.cend(), ie ) == rl.cend() ) {
			rl.push_back( ie );
			}
		}

	return std::move( rl );
	}

std::list<rtx_command_type_t> evl_add_if_not( std::list<rtx_command_type_t> && evl_, rtx_command_type_t value ) {
	auto rl( std::move( evl_ ) );
	if( std::find( rl.cbegin(), rl.cend(), value ) == rl.cend() ) {
		rl.push_back( value );
		}

	return std::move( rl );
	}

void rt1_command_router_t::add( const std::list<rtx_command_type_t> & evl_,
	std::unique_ptr<i_command_link_wrapper_t> && link_ ) {

	if( link_ ) {
		auto evl( evl_add_if_not( evl_remove_duplicate( evl_ ), rtx_command_type_t::exception ) );
		auto objLck( link_->link() );
		if( objLck ) {

			lck_scp_t lck( _cbTblLck );
			for( auto & ev : evl ) {
				auto it = _cbTbl[ev].find( objLck->ev_rdm() );
				if( it != _cbTbl[ev].end() ) {

					/* ������������� ����� �� ������� �������� � ��������� ��������� �������-����������,
					�������, ������������� ������ � �������, � ���������� ���������������, ������ ����
					����������� ������� */
					FATAL_ERROR_FWD(nullptr);
					}
				else {
					if( !_cbTbl[ev].insert( {objLck->ev_rdm(), link_->copy() } ).second )
						FATAL_ERROR_FWD(nullptr);

#ifdef CBL_OBJECTS_COUNTER_CHECKER
					CHECK_NO_EXCEPTION( __size_trace() );
#endif
					}
				}
			}
		else {
			THROW_MPF_EXC_FWD(nullptr);
			}
		}
	else {
		THROW_MPF_EXC_FWD(nullptr);
		}
	}

void rt1_command_router_t::remove_by_id( const rtl_roadmap_event_t & objectId ) noexcept {
	lck_scp_t lck( _cbTblLck );

	auto evIt = _cbTbl.begin();
	for( ; evIt != _cbTbl.end(); ++evIt ) {

		auto & mapIdRed = evIt->second;
		auto idIt = mapIdRed.find( objectId );
		if( idIt != mapIdRed.end() ) {

			if( idIt->second ) {
				unbind_link_invoke( std::move(*idIt->second).link() );
				}

			CHECK_NO_EXCEPTION( mapIdRed.erase( idIt ) );
			}
		}

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	CHECK_NO_EXCEPTION( __size_trace() );
#endif
	}

void rt1_command_router_t::bind( const std::list<rtx_command_type_t> & evl,
	std::unique_ptr<i_command_link_wrapper_t> && link ) {

	add( evl, std::move( link ) );
	}

void rt1_command_router_t::join_x_read_events()noexcept{
	if( _thCommandRead.joinable() ){
		_thCommandRead.join();
		}
	}

void rt1_command_router_t::launch_x_read_events() {
	if( !_thCommandRead.joinable() ){

		_thCommandRead = std::thread( [this, timeoutWait = CHECK_PTR(_ctx)->policies().timeout_wait_io()]{

#ifdef CBL_TRACE_THREAD_CREATE_POINT
			CHECK_NO_EXCEPTION(crm::file_logger_t::logging("rt1_command_router_t::launch_x_read_events", CBL_TRACE_THREAD_CREATE_POINT_TAG ));
#endif

			while( !canceled() ){
				try{

					auto ichannel( x_channel() );
					if( ichannel ){
						while( !canceled() ){

							command_rdm_t obj;
							if( ichannel->pop_from_rt0( obj, timeoutWait) ){
								if( !obj.cmd.empty() ){

									auto pCmd( obj.cmd.extract( ctx() ) );
									route_from_channel( command_cast<i_command_from_rt02rt1_t>(std::move( pCmd )) );
									}
								}
							}
						}
					}
				catch( const dcf_exception_t & exc0 ){
					ntf_hndl( exc0.dcpy() );
					}
				catch( const std::exception & exc1 ){
					ntf_hndl( CREATE_MPF_PTR_EXC_FWD( exc1 ) );
					}
				catch( ... ){
					ntf_hndl( CREATE_MPF_PTR_EXC_FWD(nullptr) );
					}
				}
			} );
		}
	else{
		THROW_MPF_EXC_FWD(nullptr);
		}
	}

void rt1_command_router_t::__route2target(std::shared_ptr<i_rt1_command_link_t> && lnk,
	std::unique_ptr<i_command_from_rt02rt1_t> && response) {

	if(lnk) {
		if(lnk->is_once()) {
			unbind_link_invoke(lnk);
			}

		response->set_invoked_opt(inovked_as::async);
		lnk->rt1_router_hndl(std::move(response));
		}
	}

void rt1_command_router_t::__route2target( const rtl_roadmap_event_t & rdm,
	std::unique_ptr<i_command_from_rt02rt1_t> && response) {

	CBL_VERIFY( !is_null( rdm ) );

	for( auto & rtxItem : _cbTbl ) {

		auto it2 = rtxItem.second.begin();
		while( it2 != rtxItem.second.end() ) {
		
			if( (*it2).first == rdm ) {

				auto p = it2->second->link();
				auto oncef = !p || p->is_once();

				post_scope_action_t next( [&] {
					if( oncef ) {
						it2 = rtxItem.second.erase( it2 );
						}
					} );

				if( p ) {
					auto response1 = command_cast<i_command_from_rt02rt1_t>(response->copy());
					if(response1 ) {
						__route2target( std::move( p ), std::move(response1 ) );
						}
					else {
						FATAL_ERROR_FWD(nullptr);
						}
					}
				else {
					THROW_MPF_EXC_FWD(nullptr);
					}
				}
			else {
				++it2;
				}
			}
		}
	}

void rt1_command_router_t::__route2broadcast( std::map<rtl_roadmap_event_t, std::unique_ptr<i_command_link_wrapper_t>> & tbl,
	std::unique_ptr<i_command_from_rt02rt1_t> && response) {

	CBL_VERIFY( is_broadcast_command(response) );

	auto ctx( _ctx.lock() );
	if( ctx ) {
		auto it = tbl.begin();
		while( it != tbl.end() ) {

			CBL_VERIFY( it->second );
			bool itNext = false;

			auto targetPtr = CHECK_PTR( it->second->link() );
			auto oncef = !targetPtr || targetPtr->is_once();

			post_scope_action_t remove( [&] {
				if( oncef ) {

					it = tbl.erase( it );
					itNext = true;
					}
				} );

			if( targetPtr ) {
				auto response1 = command_cast<i_command_from_rt02rt1_t>(response->copy());
				if(response1 ) {
					__route2target( std::move( targetPtr ), std::move(response1 ) );
					}
				else {
					FATAL_ERROR_FWD(nullptr);
					}
				}
			else {
				THROW_MPF_EXC_FWD(nullptr);
				}

			if( !itNext )
				++it;
			}
		}
	}

void rt1_command_router_t::route_from_channel( std::unique_ptr<i_command_from_rt02rt1_t> && response) {
	if(response) {
		auto ctx( _ctx.lock() );
		if( ctx ) {

			const auto objectKey = response->command_target_receiver_id();

			lck_scp_t lck( _cbTblLck );
			auto itEv = _cbTbl.find(response->type() );
			if( itEv != _cbTbl.end() ) {

				if( !is_null( objectKey ) ) {

					auto targetRec_ = (*itEv).second.find( objectKey );
					if( targetRec_ != (*itEv).second.end() ) {

						auto pTarget = targetRec_->second->link();
						auto oncef = !pTarget || pTarget->is_once();

						post_scope_action_t unbind( [&] {
							if( oncef ) {
								CHECK_NO_EXCEPTION( (*itEv).second.erase( targetRec_ ) );
								}
							} );

						if( pTarget ) {
							__route2target( std::move( pTarget ), std::move(response) );
							}
						else {
							THROW_MPF_EXC_FWD(nullptr);
							}
						}
					}
				else {
					if( is_broadcast_command(response) ) {
						__route2broadcast( (*itEv).second, std::move(response) );

						}
					else {
						FATAL_ERROR_FWD(nullptr);
						}
					}

				if( (*itEv).second.empty() ) {
					_cbTbl.erase( itEv );
					}

#ifdef CBL_OBJECTS_COUNTER_CHECKER
				__size_trace();
#endif
				}
			else {
				if( !is_null( objectKey ) ) {
					__route2target( objectKey, std::move(response) );
					}
				}
			}
		}
	}

void rt1_command_router_t::start_x() {
	launch_x_read_events();
	}

void rt1_command_router_t::start() {
	bool stf = false;
	if( _startf.compare_exchange_strong( stf, true ) ) {
		start_x();
		}
	}


#ifdef CBL_MPF_TEST_EMULATION_TIMEOUT_COMMAND_LOCAL
static sndrcv_timeline_timeouted_once test_make_timepoints_rand() {

#if (CBL_MPF_TEST_EMULATION_REMOTE_HOST_COMMAND_LOCAL_TIMEOUT_MODE == CBL_MPF_TEST_EMULATION_REMOTE_HOST_COMMAND_LOCAL_TIMEOUT_MODE_UNLIMITED)

	return sndrcv_timeline_timeouted_once{ sndrcv_timeline_periodicity_t::make_max_interval() };

#else
	static int64_t max_timeout_secs = CBL_MPF_TEST_ERROR_EMULATOR_REMOTE_HOST_CONNECTION_TIMEOUT_MAX_SECS;
	static int64_t increment = CBL_MPF_TEST_ERROR_EMULATOR_REMOTE_HOST_CONNECTION_TIMEOUT_INCREMENT_SECS;

	static std::atomic<int64_t> seconds = 0;

#if (CBL_MPF_TEST_EMULATION_REMOTE_HOST_COMMAND_LOCAL_TIMEOUT_MODE == CBL_MPF_TEST_EMULATION_REMOTE_HOST_COMMAND_LOCAL_TIMEOUT_MODE_TIMEOUTED)

	auto ut = ((seconds += increment) % max_timeout_secs);
	return sndrcv_timeline_timeouted_once{ std::chrono::seconds(ut + 1) };

#elif (CBL_MPF_TEST_EMULATION_REMOTE_HOST_COMMAND_LOCAL_TIMEOUT_MODE == CBL_MPF_TEST_EMULATION_REMOTE_HOST_COMMAND_LOCAL_TIMEOUT_MODE_MIX)

	auto ut = ((seconds += increment) % max_timeout_secs);
	if(ut > 0) {
		return sndrcv_timeline_timeouted_once{ std::chrono::seconds(ut) };
		}
	else {
		return sndrcv_timeline_timeouted_once{ std::chrono::milliseconds::max() };
		}
#endif
#endif
	}
#endif


void rt1_command_router_t::register2channel( std::list<rtx_command_type_t> && evlist, 
	const rtl_roadmap_event_t & commandRDM, 
	const rtl_roadmap_obj_t & messageRDM,
	std::unique_ptr<i_command_link_wrapper_t> && link_,
	__command_error_handler_point_f && rh) {

	add( evlist, std::move( link_ ) );

	i_rt0_cmd_addlink_t command{
		/* ����������� �� ��������� ��������� ������������ ������� ������ 1 */
		messageRDM,
		/* ����������� �� ��������� ������� ������������ ������� ������ 1 */
		commandRDM,
		/* ������ ������� */
		std::move(evlist) };

	sndrcv_command_execute( std::move( command ), std::move( rh ) );
	}

void rt1_command_router_t::register2channel( std::list<rtx_command_type_t> && evlist,
	const rtl_roadmap_event_t & commandRDM,
	const rtl_roadmap_obj_t & messageRDM,
	std::unique_ptr<i_command_link_wrapper_t> && link_ ) {

	add( evlist, std::move( link_ ) );

	i_rt0_cmd_addlink_t command{
		/* ����������� �� ��������� ��������� ������������ ������� ������ 1 */
		messageRDM,
		/* ����������� �� ��������� ������� ������������ ������� ������ 1 */
		commandRDM,
		/* ������ ������� */
		std::move(evlist) };

	sndrcv_command_execute( std::move( command ) );
	}

void rt1_command_router_t::register2channel( const rtl_roadmap_obj_t & messageRDM,
	__command_error_handler_point_f && rh ) {

	i_rt0_cmd_addlink_t command{
		/* ����������� �� ��������� ��������� ������������ ������� ������ 1 */
		messageRDM };

	sndrcv_command_execute( std::move( command ), std::move( rh ) );
	}

void rt1_command_router_t::register2channel( std::list<rtx_command_type_t> && evlist,
	const rtl_roadmap_event_t & commandRDM,
	std::unique_ptr<i_command_link_wrapper_t> && link_,
	__command_error_handler_point_f && rh ) {

	add( evlist, std::move(link_) );

	i_rt0_cmd_addlink_t command{
		/* ����������� �� ��������� ������� ������������ ������� ������ 1 */
		commandRDM,
		/* ������ ������� */
		std::move(evlist) };

	sndrcv_command_execute( std::move( command ), std::move( rh ) );
	}

void rt1_command_router_t::unregister_from_channel( const rtl_roadmap_event_t & commandRDM,
	const rtl_roadmap_obj_t & messageRDM,
	unregister_message_track_reason_t r )noexcept {
	
	auto xch( x_channel() );
	if( xch ) {
		auto rch = push2channel(xch, i_rt0_cmd_sublink_t{
			/* �������� �������� �� ��������� ��������� */
			messageRDM,
			/* �������� �������� �� ��������� ������� ������������ ������� ������ 1 */
			commandRDM,
			r });

		if(!(rch == addon_push_event_result_t::enum_type::link_unregistered ||
			rch == addon_push_event_result_t::enum_type::success)) {

			FATAL_ERROR_FWD(nullptr);
			}
		}
	}

void rt1_command_router_t::unregister_from_channel( const rtl_roadmap_event_t & commandRDM ) noexcept {

	auto xch( x_channel() );
	if( xch ) {
		auto rch = push2channel(xch, i_rt0_cmd_sublink_t{
			/* �������� �������� �� ��������� ������� ������������ ������� ������ 1 */
			commandRDM });

		if(!(rch == addon_push_event_result_t::enum_type::link_unregistered ||
			rch == addon_push_event_result_t::enum_type::success)) {

			FATAL_ERROR_FWD(nullptr);
			}
		}
	}

void rt1_command_router_t::unregister_from_channel( const rtl_roadmap_obj_t & messageRDM,
	unregister_message_track_reason_t r ) noexcept {

	auto xch( x_channel() );
	if( xch ) {
		auto rch = push2channel(xch, i_rt0_cmd_sublink_t{
			/* �������� �������� �� ��������� ��������� */
			messageRDM,
			r });

		if(!(rch == addon_push_event_result_t::enum_type::link_unregistered ||
			rch == addon_push_event_result_t::enum_type::success)) {

			FATAL_ERROR_FWD(nullptr);
			}
		}
	}

void rt1_command_router_t::unregister_from_channel( const rt1_endpoint_t & targetEP,
	const identity_descriptor_t & targetId,
	const source_object_key_t & srcKey )noexcept {

	auto xch( x_channel() );
	if( xch ) {
		auto rch = push2channel(xch, i_rt0_cmd_sublink_sourcekey_t{
			targetEP,
			targetId,
			srcKey });

		if(!(rch == addon_push_event_result_t::enum_type::link_unregistered ||
			rch == addon_push_event_result_t::enum_type::success )){

			FATAL_ERROR_FWD(nullptr);
			}
		}
	}

std::shared_ptr<distributed_ctx_t> rt1_command_router_t::ctx()noexcept {
	return _ctx.lock();
	}

std::shared_ptr<distributed_ctx_t> rt1_command_router_t::ctx()const noexcept {
	return _ctx.lock();
	}

std::shared_ptr<io_x_rt1_channel_commands_t> rt1_command_router_t::x_channel()const noexcept {
	return _xChannel;
	}

void rt1_command_router_t::ntf_hndl( std::unique_ptr<dcf_exception_t> && ntf )const noexcept {
	if(auto c = ctx()) {
		c->exc_hndl(std::move(ntf));
		}
	}











void __command_error_handler_point_f::functor::operator()(std::unique_ptr<dcf_exception_t> && e)noexcept {
	invoke(nullptr, std::move(e));
	}

void __command_error_handler_point_f::functor::operator()(std::unique_ptr<i_command_from_rt02rt1_t> && response) {
	invoke(std::move(response), nullptr);
	}

__command_error_handler_point_f::functor::functor()noexcept {}



__command_error_handler_point_f::__command_error_handler_point_f()noexcept {}

void __command_error_handler_point_f::operator()(std::unique_ptr<dcf_exception_t> && e)noexcept{
	if(_h) {
		_h.invoke_async(std::move(e));
		}
	}

void __command_error_handler_point_f::operator()(std::unique_ptr<i_command_from_rt02rt1_t> && response) {
	if(_h) {
		_h.invoke_async(std::move(response));
		}
	}

__command_error_handler_point_f::operator bool()const noexcept {
	return _h;
	}

static_assert(!std::is_copy_constructible_v< __command_error_handler_point_f>, __FILE__);
static_assert(!std::is_copy_assignable_v<__command_error_handler_point_f>, __FILE__);
static_assert(std::is_move_constructible_v< __command_error_handler_point_f>, __FILE__);
static_assert(std::is_move_assignable_v<__command_error_handler_point_f>, __FILE__);
