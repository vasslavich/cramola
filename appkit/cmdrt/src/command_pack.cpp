#include "../../internal.h"
#include "../../channel_terms/internal_terms.h"
#include "../../notifications/exceptions/excdrv.h"
#include "../command_pack.h"


using namespace crm;
using namespace crm::detail;


static_assert(srlz::is_serializable_v<command_rdm_t>, __FILE_LINE__);
static_assert(std::is_aggregate_v< command_rdm_t>, __FILE_LINE__);
static_assert(srlz::detail::is_aggregate_initializable_v<command_rdm_t>, __FILE_LINE__);
static_assert(srlz::detail::__fields_count_hepler_t<command_rdm_t>::count == 2, __FILE_LINE__);
static_assert(srlz::detail::fields_count_v<command_rdm_t> == 2, __FILE_LINE__);
static_assert(std::is_class_v<command_rdm_t>, __FILE_LINE__);


binary_vector_t rt0_x_command_pack_t::at_binary()const {
	if (_serializer) {
		return _serializer(false);
		}
	else {
		return _pck;
		}
	}

const binary_vector_t& rt0_x_command_pack_t::binary_srlz()const {
	if (_pck.empty() && _serializer) {
		_pck = _serializer(true);
		return _pck;
		}
	else {
		return _pck;
		}
	}

rt0_x_command_pack_t rt0_x_command_pack_t::cpy_mem()const {
	rt0_x_command_pack_t r;
	r._type = _type;
	r._pck = binary_srlz();
	r._t = _t;

	return r;
	}

const i_rt0_command_t* rt0_x_command_pack_t::pcmd()const noexcept {
	return _pcmd.get();
	}

i_rt0_command_t* rt0_x_command_pack_t::pcmd()noexcept {
	return _pcmd.get();
	}

rt0_x_command_pack_t::cmd_subtype rt0_x_command_pack_t::surtype()const noexcept {
	return _t;
	}

const std::chrono::milliseconds& rt0_x_command_pack_t::wait_timeout()const noexcept {
	return CHECK_PTR(pcmd())->wait_timeout();
	}


const rtl_roadmap_event_t& rt0_x_command_pack_t::command_target_receiver_id()const noexcept {
	if (_t == cmd_subtype::rt02rt1) {
		return dynamic_cast<const i_command_from_rt02rt1_t*>(pcmd())->command_target_receiver_id();
		}
	else {
		FATAL_ERROR_FWD(nullptr);
		}
	}
void rt0_x_command_pack_t::set_target_receiver_id(const rtl_roadmap_event_t& targetId) {
	if (_t == cmd_subtype::rt02rt1) {
		dynamic_cast<i_command_from_rt02rt1_t*>(pcmd())->set_target_receiver_id(targetId);
		}
	else {
		FATAL_ERROR_FWD(nullptr);
		}
	}


const local_command_identity_t& rt0_x_command_pack_t::source_command_id()const noexcept {
	if (_t == cmd_subtype::rt02rt1) {
		return dynamic_cast<const i_command_from_rt02rt1_t*>(pcmd())->source_command_id();
		}
	else {
		FATAL_ERROR_FWD(nullptr);
		}
	}
void rt0_x_command_pack_t::set_source_command_id(const local_command_identity_t& cmdid)noexcept {
	if (_t == cmd_subtype::rt02rt1) {
		dynamic_cast<i_command_from_rt02rt1_t*>(pcmd())->set_source_command_id(cmdid);
		}
	else {
		FATAL_ERROR_FWD(nullptr);
		}
	}


void rt0_x_command_pack_t::set_invoked_opt(inovked_as v)noexcept {
	if (_t == cmd_subtype::rt02rt1) {
		dynamic_cast<i_command_from_rt02rt1_t*>(pcmd())->set_invoked_opt(v);
		}
	else {
		FATAL_ERROR_FWD(nullptr);
		}
	}
inovked_as rt0_x_command_pack_t::invoked_as()const noexcept {
	if (_t == cmd_subtype::rt02rt1) {
		return dynamic_cast<const i_command_from_rt02rt1_t*>(pcmd())->invoked_as();
		}
	else {
		FATAL_ERROR_FWD(nullptr);
		}
	}





rtl_level_t rt0_x_command_pack_t::level()const noexcept {
	if (_t == cmd_subtype::fromrt1) {
		return dynamic_cast<const i_command_from_rt1_t*>(pcmd())->level();
		}
	else {
		FATAL_ERROR_FWD(nullptr);
		}
	}


void rt0_x_command_pack_t::set_command_evrdm(const rtl_roadmap_event_t& sourceObjectRdm) {
	if (_t == cmd_subtype::fromrt1) {
		dynamic_cast<i_command_from_rt1_t*>(pcmd())->set_command_evrdm(sourceObjectRdm);
		}
	else {
		FATAL_ERROR_FWD(nullptr);
		}
	}
const rtl_roadmap_event_t& rt0_x_command_pack_t::command_source_emitter_rdm()const noexcept {
	if (_t == cmd_subtype::fromrt1) {
		return dynamic_cast<const i_command_from_rt1_t*>(pcmd())->command_source_emitter_rdm();
		}
	else {
		FATAL_ERROR_FWD(nullptr);
		}
	}


const channel_identity_t& rt0_x_command_pack_t::channel_id()const noexcept {
	if (_t == cmd_subtype::fromrt1) {
		return dynamic_cast<const i_command_from_rt1_t*>(pcmd())->channel_id();
		}
	else {
		FATAL_ERROR_FWD(nullptr);
		}
	}
void rt0_x_command_pack_t::set_channel_id(const channel_identity_t& chId) {
	if (_t == cmd_subtype::fromrt1) {
		dynamic_cast<i_command_from_rt1_t*>(pcmd())->set_channel_id(chId);
		}
	else {
		FATAL_ERROR_FWD(nullptr);
		}
	}



const local_command_identity_t& rt0_x_command_pack_t::command_id()const noexcept {
	if (_t == cmd_subtype::fromrt1) {
		return dynamic_cast<const i_command_from_rt1_t*>(pcmd())->command_id();
		}
	else {
		FATAL_ERROR_FWD(nullptr);
		}
	}
void rt0_x_command_pack_t::set_command_id(const local_command_identity_t& id) {
	if (_t == cmd_subtype::fromrt1) {
		dynamic_cast<i_command_from_rt1_t*>(pcmd())->set_command_id(id);
		}
	else {
		FATAL_ERROR_FWD(nullptr);
		}
	}




std::unique_ptr<i_rt0_command_t> rt0_x_command_pack_t::extract(
	const std::shared_ptr<distributed_ctx_t>& ctx) noexcept {

	if (_pcmd) {
		return std::move(_pcmd);
		}
	else if(!_pck.empty()) {
		auto rstream(crm::srlz::rstream_constructor_t::make(_pck.cbegin(), _pck.cend()));

		/* read type descriptor of the command */
		srlz::typemap_key_t typekey;
		srlz::deserialize(rstream, typekey);

		if (auto ictrX = ctx->typemap().find(typekey)) {
			if (auto ictr = std::dynamic_pointer_cast<srlz::i_type_ctr_i_srlzd_t<i_rt0_command_t>>(ictrX)) {
				if (auto pdcp = ictr->create()) {
					pdcp->decode(rstream);
					if (auto & pObj = pdcp->pObj) {
						pObj->set_context(ctx);

						if (pObj->type() != _type)
							FATAL_ERROR_FWD(nullptr);

#ifdef USE_DCF_CHECK
						if (auto rt0rt1 = dynamic_cast<const i_command_from_rt02rt1_t*>(pObj.get())) {
							if (is_null(rt0rt1->command_target_receiver_id()) && !is_broadcast_command(rt0rt1)) {
								FATAL_ERROR_FWD(nullptr);
								}
							}
						else if (auto rt1rt0 = dynamic_cast<const i_command_from_rt1_t*>(pObj.get())) {
							if (is_null(rt1rt0->command_source_emitter_rdm()) && !is_without_response(rt1rt0)) {
								FATAL_ERROR_FWD(nullptr);
								}
							}
#endif

						return std::move(pObj);
						}
					else {
						SERIALIZE_THROW_EXC_FWD(nullptr);
						}
					}
				else {
					SERIALIZE_THROW_EXC_FWD(nullptr);
					}
				}
			else {
				SERIALIZE_THROW_EXC_FWD(nullptr);
				}
			}
		else {
			SERIALIZE_THROW_EXC_FWD(nullptr);
			}
		}
	else {
		SERIALIZE_THROW_EXC_FWD(nullptr);
		}
	}

bool rt0_x_command_pack_t::empty()const noexcept {
	return _pck.empty() && !_pcmd;
	}

rtx_command_type_t rt0_x_command_pack_t::type()const noexcept {
	return _type;
	}


