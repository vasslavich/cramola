#include "../../internal.h"
#include "../rtx_cmd.h"
#include "../../notifications/internal_terms.h"
#include "../../notifications/exceptions/excdrv.h"
#include "../../context/context.h"
#include "../../typeframe.h"


using namespace crm;
using namespace crm::detail;


i_command_from_rt1_unit::~i_command_from_rt1_unit() {
	if(f) {
		CHECK_NO_EXCEPTION(f());
		}
	}
i_command_from_rt1_unit::i_command_from_rt1_unit(fail_f && f_)
	: f(std::move(f_)) {}

void i_command_from_rt1_unit::reset_failback()noexcept {
	f = nullptr;
	}



void i_command_from_rt1_t::set_command_evrdm( const rtl_roadmap_event_t & sourceObjectRdm ) {
	CBL_VERIFY( !is_null( sourceObjectRdm ) );
	_sourceObjectRdm = sourceObjectRdm;
	}

const rtl_roadmap_event_t& i_command_from_rt1_t::command_source_emitter_rdm()const noexcept{
	return _sourceObjectRdm;
	}

const channel_identity_t& i_command_from_rt1_t::channel_id()const noexcept {
	return _channelId;
	}

void i_command_from_rt1_t::set_channel_id( const channel_identity_t & chId ) {
	_channelId = chId;
	}

const local_command_identity_t& i_command_from_rt1_t::command_id()const noexcept {
	return _cmdId;
	}

void i_command_from_rt1_t::set_command_id( const local_command_identity_t& id ) {
	_cmdId = id;
	}

size_t i_command_from_rt1_t::get_serialized_size()const noexcept{
	return i_rt0_command_t::get_serialized_size() 
		+ srlz::object_trait_serialized_size(_channelId)
		+ srlz::object_trait_serialized_size(_cmdId)
		+ srlz::object_trait_serialized_size(_sourceObjectRdm);
	}




i_rt0_cmd_sublink_t::i_rt0_cmd_sublink_t( const rtl_roadmap_obj_t & rdm_,
	unregister_message_track_reason_t r )
	: _rdmMsg( rdm_ )
	, _reason(r){}

i_rt0_cmd_sublink_t::i_rt0_cmd_sublink_t( const rtl_roadmap_event_t & rdm_ )
	: _rdmEv( rdm_ ){}

i_rt0_cmd_sublink_t::i_rt0_cmd_sublink_t( const rtl_roadmap_obj_t & rdmMsg,
	const rtl_roadmap_event_t & rdmEv,
	unregister_message_track_reason_t r )
	: _rdmEv( rdmEv )
	, _rdmMsg(rdmMsg)
	, _reason(r){}

size_t i_rt0_cmd_sublink_t::get_serialized_size()const noexcept {
	return base_type::get_serialized_size() 
		+ srlz::object_trait_serialized_size( _rdmEv )
		+ srlz::object_trait_serialized_size( _rdmMsg )
		+ srlz::object_trait_serialized_size( _reason );
	}

std::unique_ptr<i_rt0_command_t> i_rt0_cmd_sublink_t::copy()const noexcept {
	return std::make_unique<i_rt0_cmd_sublink_t>( i_rt0_cmd_sublink_t( *this ) );
	}

register_slot_group_t i_rt0_cmd_sublink_t::register_group()const noexcept {
	if( !crm::is_null(_rdmEv.glid()) && !crm::is_null(_rdmMsg.target_id()) )
		return register_slot_group_t::message_events;
	else if( !crm::is_null(_rdmEv.glid()) )
		return register_slot_group_t::events;
	else if( !crm::is_null(_rdmMsg.target_id()) )
		return register_slot_group_t::messages;
	else {
		FATAL_ERROR_FWD(nullptr);
		}
	}

const rtl_roadmap_event_t& i_rt0_cmd_sublink_t::ev_rdm()const noexcept {
	return _rdmEv;
	}

const rtl_roadmap_obj_t& i_rt0_cmd_sublink_t::msg_rdm()const noexcept {
	return _rdmMsg;
	}

unregister_message_track_reason_t i_rt0_cmd_sublink_t::reason()const noexcept {
	return _reason;
	}

void i_rt0_cmd_sublink_t::set_reason( unregister_message_track_reason_t r ) {
	_reason = r;
	}




i_rt0_cmd_sublink_sourcekey_t::i_rt0_cmd_sublink_sourcekey_t()noexcept{}

std::unique_ptr<i_rt0_command_t> i_rt0_cmd_sublink_sourcekey_t::copy()const noexcept {
	return std::make_unique<i_rt0_cmd_sublink_sourcekey_t>( i_rt0_cmd_sublink_sourcekey_t( *this ) );
	}

i_rt0_cmd_sublink_sourcekey_t::i_rt0_cmd_sublink_sourcekey_t(const rt1_endpoint_t & targetEP,
	const identity_descriptor_t & targetId,
	const source_object_key_t & rdm )
	: _targetEP( targetEP )
	, _targetId( targetId )
	, _sourceKey( rdm ){}

const rt1_endpoint_t & i_rt0_cmd_sublink_sourcekey_t::target_endpoint()const noexcept {
	return _targetEP;
	}

const identity_descriptor_t& i_rt0_cmd_sublink_sourcekey_t::target_id()const noexcept {
	return _targetId;
	}

const source_object_key_t& i_rt0_cmd_sublink_sourcekey_t::source_key()const noexcept {
	return _sourceKey;
	}

size_t i_rt0_cmd_sublink_sourcekey_t::get_serialized_size()const noexcept{
	return base_type::get_serialized_size()
		+ srlz::object_trait_serialized_size( _targetEP )
		+ srlz::object_trait_serialized_size( _targetId )
		+ srlz::object_trait_serialized_size( _sourceKey );
	}







i_rt0_cmd_addlink_t::i_rt0_cmd_addlink_t( const rtl_roadmap_obj_t & rdm_  )
	: _rdmMsg( rdm_ ){}

i_rt0_cmd_addlink_t::i_rt0_cmd_addlink_t( const rtl_roadmap_event_t & rdm_,
	std::list<rtx_command_type_t> && evlst )
	: _rdmEv( rdm_ )
	, _evlst( std::move( evlst ) ){}

i_rt0_cmd_addlink_t::i_rt0_cmd_addlink_t( const rtl_roadmap_obj_t & rdmMsg,
	const rtl_roadmap_event_t & rdmEv,
	std::list<rtx_command_type_t> && evlst )
	: _rdmMsg( rdmMsg )
	, _rdmEv( rdmEv )
	, _evlst( std::move( evlst ) ){}

size_t i_rt0_cmd_addlink_t::get_serialized_size()const noexcept{
	return base_type::get_serialized_size()
		+ srlz::object_trait_serialized_size( _evlst )
		+ srlz::object_trait_serialized_size( _rdmEv )
		+ srlz::object_trait_serialized_size( _rdmMsg );
	}

const std::list<rtx_command_type_t> & i_rt0_cmd_addlink_t::events()const noexcept {
	return _evlst;
	}

std::unique_ptr<i_rt0_command_t> i_rt0_cmd_addlink_t::copy()const noexcept {
	return std::make_unique<i_rt0_cmd_addlink_t>( i_rt0_cmd_addlink_t( *this ) );
	}

register_slot_group_t i_rt0_cmd_addlink_t::register_group()const noexcept {
	if( !crm::is_null(_rdmEv.glid()) && !crm::is_null(_rdmMsg.target_id()) )
		return register_slot_group_t::message_events;
	else if( !crm::is_null(_rdmEv.glid()) )
		return register_slot_group_t::events;
	else if( !crm::is_null(_rdmMsg.target_id()) )
		return register_slot_group_t::messages;
	else {
		FATAL_ERROR_FWD(nullptr);
		}
	}

const rtl_roadmap_event_t& i_rt0_cmd_addlink_t::ev_rdm()const noexcept {
	return _rdmEv;
	}

const rtl_roadmap_obj_t& i_rt0_cmd_addlink_t::msg_rdm()const noexcept {
	return _rdmMsg;
	}


bool crm::detail::operator==(const i_rt0_cmd_addlink_t& l, const i_rt0_cmd_addlink_t& r)noexcept {
	static_assert(std::is_base_of_v< i_command_from_rt1_t, std::decay_t<decltype(l)>>, __FILE_LINE__);

	return l.msg_rdm() == r.msg_rdm() &&
		l.ev_rdm() == r.ev_rdm() &&
		l.events() == r.events() &&
		make_comparator_cast(static_cast<const i_command_from_rt1_t&>(l)) == make_comparator_cast(static_cast<const i_command_from_rt1_t&>(r));
	}

size_t i_rt0_cmd_reqconnection_t::get_serialized_size()const noexcept {
	return base_type::get_serialized_size()
		+ srlz::object_trait_serialized_size(_ep)
		+ srlz::object_trait_serialized_size(_rdm)
		+ srlz::object_trait_serialized_size(_srcKey)
		+ srlz::object_trait_serialized_size(_timeout);
	}

const rt1_endpoint_t& i_rt0_cmd_reqconnection_t::endpoint()const noexcept {
	return _ep;
	}

std::unique_ptr<i_rt0_command_t> i_rt0_cmd_reqconnection_t::copy()const noexcept {
	return std::make_unique<i_rt0_cmd_reqconnection_t>( i_rt0_cmd_reqconnection_t( *this ) );
	}

const source_object_key_t& i_rt0_cmd_reqconnection_t::source_key()const noexcept {
	return _srcKey;
	}

const std::chrono::seconds& i_rt0_cmd_reqconnection_t::timeout()const noexcept {
	return _timeout;
	}



cmd_unsubscribe_connection_t::cmd_unsubscribe_connection_t() {}

cmd_unsubscribe_connection_t::cmd_unsubscribe_connection_t( const connection_key_t & cnk,
	const _address_event_t & subscriberId,
	rtl_table_t t ) 
	: _subscriberId(subscriberId)
	, _connectionKey(cnk)
	, _t(t){}

const connection_key_t& cmd_unsubscribe_connection_t::connection_key()const noexcept {
	return _connectionKey;
	}

const _address_event_t& cmd_unsubscribe_connection_t::subscriber_id()const noexcept {
	return _subscriberId;
	}

rtl_table_t cmd_unsubscribe_connection_t::table()const noexcept {
	return _t;
	}

std::unique_ptr<i_rt0_command_t> cmd_unsubscribe_connection_t::copy()const noexcept {
	return std::make_unique<cmd_unsubscribe_connection_t>( (*this) );
	}

size_t cmd_unsubscribe_connection_t::get_serialized_size()const noexcept{
	return base_type_t::get_serialized_size()

		+ srlz::object_trait_serialized_size( _subscriberId )
		+ srlz::object_trait_serialized_size( _connectionKey )
		+ srlz::object_trait_serialized_size( _t );
	}



module_channel_connection_string::module_channel_connection_string()noexcept {}
module_channel_connection_string::module_channel_connection_string(std::string mCName, std::string cCName)
	: _mChannelName(mCName)
	, _cChannelName(cCName){}

std::unique_ptr<i_rt0_command_t> module_channel_connection_string::copy()const noexcept {
	return std::make_unique<module_channel_connection_string>(*this);
	}

size_t module_channel_connection_string::get_serialized_size()const noexcept{
	return base_type::get_serialized_size()

		+ srlz::object_trait_serialized_size(_mChannelName)
		+ srlz::object_trait_serialized_size(_cChannelName);
	}
