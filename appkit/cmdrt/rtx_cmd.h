#pragma once


#include <functional>
#include "../internal_invariants.h"
#include "./i_channel_commands.h"
#include "../channel_terms/rtl_rdm_object.h"
#include "../channel_terms/rtl_rdm_event_desc.h"
#include "../channel_terms/subscribe_terms.h"
#include "../channel_terms/message_frame.h"


namespace crm::detail{



/*! �������� ������� � ������ 1 �� ������� �/��� ������� ������ 0 */
class i_rt0_cmd_addlink_t final : 
	public i_command_from_rt1_t {

private:
	rtl_roadmap_obj_t _rdmMsg;
	rtl_roadmap_event_t _rdmEv;
	std::list<rtx_command_type_t> _evlst;

public:
	using base_type = i_command_from_rt1_t;

	i_rt0_cmd_addlink_t()noexcept {}

	i_rt0_cmd_addlink_t( const rtl_roadmap_obj_t & rdmObj );
	i_rt0_cmd_addlink_t(const rtl_roadmap_event_t & rdmEv,
		std::list<rtx_command_type_t> && evlst );
	i_rt0_cmd_addlink_t( const rtl_roadmap_obj_t & rdmMsg,
		const rtl_roadmap_event_t & rdmEv,
		std::list<rtx_command_type_t> && evlst );

	std::unique_ptr<i_rt0_command_t> copy()const noexcept final;
	const std::list<rtx_command_type_t> & events()const noexcept;
	register_slot_group_t register_group()const noexcept;
	const rtl_roadmap_event_t& ev_rdm()const noexcept;
	const rtl_roadmap_obj_t& msg_rdm()const noexcept;
	
	rtx_command_type_t type()const noexcept final{return rtx_command_type_t::reg_link_rdm;}

	/*! ������������ ������� � �������� ������ */
	template<typename Ts>
	void srlz(Ts && dest)const{
		base_type::srlz(dest);

		srlz::serialize(dest, _evlst);
		srlz::serialize(dest, _rdmEv);
		srlz::serialize(dest, _rdmMsg);
		}

	/*! �������������� ������� �� ��������� ������� */
	template<typename Ts>
	void dsrlz(Ts && src ){
		base_type::dsrlz(src);

		srlz::deserialize(src, _evlst);
		srlz::deserialize(src, _rdmEv);
		srlz::deserialize(src, _rdmMsg);
		}

	size_t get_serialized_size()const noexcept;
	};

bool operator==(const i_rt0_cmd_addlink_t& l, const i_rt0_cmd_addlink_t& r)noexcept;

/*! ������� ������� � ������ 1 �� ������� ������ 0 */
class i_rt0_cmd_sublink_t final : public i_is_without_response_t {
private:
	rtl_roadmap_event_t _rdmEv;
	rtl_roadmap_obj_t _rdmMsg;
	unregister_message_track_reason_t _reason = unregister_message_track_reason_t::undefined;

public:
	using base_type = i_is_without_response_t;

	std::unique_ptr<i_rt0_command_t> copy()const noexcept final;
	
	i_rt0_cmd_sublink_t()noexcept {}

	i_rt0_cmd_sublink_t( const rtl_roadmap_obj_t & rdm,
		unregister_message_track_reason_t );
	i_rt0_cmd_sublink_t( const rtl_roadmap_event_t & rdm );
	i_rt0_cmd_sublink_t( const rtl_roadmap_obj_t & rdmMsg,
		const rtl_roadmap_event_t & rdmEv,
		unregister_message_track_reason_t );

	void set_reason( unregister_message_track_reason_t r );

	register_slot_group_t register_group()const noexcept;
	const rtl_roadmap_event_t& ev_rdm()const noexcept;
	const rtl_roadmap_obj_t& msg_rdm()const noexcept;
	unregister_message_track_reason_t reason()const noexcept;
	rtx_command_type_t type()const  noexcept final{return rtx_command_type_t::unreg_link_rdm;}

	/*! ������������ ������� � �������� ������ */
	template<typename Ts>
	void srlz(Ts && dest)const{
		if (_reason < unregister_message_track_reason_t::closed_by_self
			|| _reason > unregister_message_track_reason_t::not_founded){
			FATAL_ERROR_FWD(nullptr);
			}

		base_type::srlz(dest);

		srlz::serialize(dest, _rdmEv);
		srlz::serialize(dest, _rdmMsg);
		srlz::serialize(dest, _reason);
		}

	/*! �������������� ������� �� ��������� ������� */
	template<typename Ts>
	void dsrlz(Ts && src ){

		base_type::dsrlz(src);

		srlz::deserialize(src, _rdmEv);
		srlz::deserialize(src, _rdmMsg);
		srlz::deserialize(src, _reason);

		if (_reason < unregister_message_track_reason_t::closed_by_self
			|| _reason > unregister_message_track_reason_t::not_founded){
			FATAL_ERROR_FWD(nullptr);
			}
		}

	size_t get_serialized_size()const noexcept;
	};


/*! ������� ������� � ������ 1 �� ������� ������ 0 */
class i_rt0_cmd_sublink_sourcekey_t final : public i_is_without_response_t {
private:
	rt1_endpoint_t _targetEP;
	identity_descriptor_t _targetId;
	source_object_key_t _sourceKey;

public:
	using base_type  = i_is_without_response_t;

	std::unique_ptr<i_rt0_command_t> copy()const noexcept final;

	i_rt0_cmd_sublink_sourcekey_t()noexcept;

	i_rt0_cmd_sublink_sourcekey_t( const rt1_endpoint_t & targetEP,
		const identity_descriptor_t & targetId,
		const source_object_key_t & rdm );

	const rt1_endpoint_t & target_endpoint()const noexcept;
	const identity_descriptor_t& target_id()const noexcept;
	const source_object_key_t& source_key()const noexcept;
	rtx_command_type_t type()const noexcept final{return rtx_command_type_t::unreg_link_source_key;}

	/*! ������������ ������� � �������� ������ */
	template<typename Ts>
	void srlz(Ts && dest)const{
		base_type::srlz(dest);

		srlz::serialize(dest, _targetEP);
		srlz::serialize(dest, _targetId);
		srlz::serialize(dest, _sourceKey);
		}

	/*! �������������� ������� �� ��������� ������� */
	template<typename Ts>
	void dsrlz(Ts && src ){
		base_type::dsrlz(src);

		srlz::deserialize(src, _targetEP);
		srlz::deserialize(src, _targetId);
		srlz::deserialize(src, _sourceKey);
		}

	size_t get_serialized_size()const noexcept;
	};


/*! ������ ������ ���������� ����������, ��������� �������������� ������ 0 */
class i_rt0_cmd_reqconnection_t final : public i_command_from_rt1_t {
private:
	rt1_endpoint_t _ep;
	source_object_key_t _srcKey;
	rtl_roadmap_event_t _rdm;
	std::chrono::seconds _timeout{ 10 };

public:
	using base_type = i_command_from_rt1_t;

	i_rt0_cmd_reqconnection_t()noexcept{}

	template<typename TRep, typename TPeriod>
	i_rt0_cmd_reqconnection_t(const rt1_endpoint_t& ep/* ����� ����������� ��������� ���� */,
			const source_object_key_t& srcKey,
			const std::chrono::duration<TRep, TPeriod>& timeout)
		: _ep(ep)
		, _srcKey(srcKey)
		, _timeout(std::chrono::duration_cast<std::chrono::seconds>(timeout)) {}

	const std::chrono::seconds& timeout()const noexcept;
	const rt1_endpoint_t& endpoint()const noexcept;
	std::unique_ptr<i_rt0_command_t> copy()const noexcept final;
	const source_object_key_t& source_key()const noexcept;
	rtx_command_type_t type()const noexcept final{return rtx_command_type_t::out_connection_req;}

	/*! ������������ ������� � �������� ������ */
	template<typename Ts>
	void srlz(Ts && dest)const{
		base_type::srlz(dest);

		srlz::serialize(dest, _ep);
		srlz::serialize(dest, _rdm);
		srlz::serialize(dest, _srcKey);
		srlz::serialize(dest, _timeout);
		}

	/*! �������������� ������� �� ��������� ������� */
	template<typename Ts>
	void dsrlz(Ts && src){
		base_type::dsrlz(src);

		srlz::deserialize(src, _ep);
		srlz::deserialize(src, _rdm);
		srlz::deserialize(src, _srcKey);
		srlz::deserialize(src, _timeout);
		}

	size_t get_serialized_size()const noexcept;
	};


class cmd_unsubscribe_connection_t final : public i_is_without_response_t {
using base_type_t = i_is_without_response_t;

private:
	_address_event_t _subscriberId;
	connection_key_t _connectionKey;
	rtl_table_t _t;

public:
	cmd_unsubscribe_connection_t();

	cmd_unsubscribe_connection_t( const connection_key_t & cnk,
		const _address_event_t & subscriberId,
		rtl_table_t t );

	const _address_event_t& subscriber_id()const noexcept;
	const connection_key_t& connection_key()const noexcept;
	rtl_table_t table()const noexcept;
	std::unique_ptr<i_rt0_command_t> copy()const noexcept final;
	rtx_command_type_t type()const noexcept final{return rtx_command_type_t::unbind_connection; }

	template<typename Ts>
	void srlz(Ts && dest)const{
		base_type_t::srlz(dest);

		srlz::serialize(dest, _subscriberId);
		srlz::serialize(dest, _connectionKey);
		srlz::serialize(dest, _t);
		}

	template<typename Ts>
	void dsrlz(Ts && src ){
		base_type_t::dsrlz(src);

		srlz::deserialize(src, _subscriberId);
		srlz::deserialize(src, _connectionKey);
		srlz::deserialize(src, _t);
		}

	size_t get_serialized_size()const noexcept;
	};







class i_rt0_exception_t final : public i_command_from_rt02rt1_t {
private:
	mutable std::shared_ptr<dcf_exception_t> _exc;
	std::vector<uint8_t> _bin;
	rtx_exception_code_t _code;

	std::shared_ptr<exceptions_driver_t> excdrv()const noexcept;

public:
	using base_type = i_command_from_rt02rt1_t;

	i_rt0_exception_t()noexcept;

	i_rt0_exception_t(rtx_exception_code_t code,
		std::unique_ptr<dcf_exception_t> && exc,
		const rtl_roadmap_event_t &tid )noexcept;

	const rtx_exception_code_t& i_rt0_exception_t::code()const noexcept;
	std::unique_ptr<dcf_exception_t> i_rt0_exception_t::exc()const;

	std::unique_ptr<i_rt0_command_t> copy()const noexcept final;
	rtx_command_type_t type()const noexcept final{return rtx_command_type_t::exception;}
	
	/*! ������������ ������� � �������� ������ */
	template<typename Ts>
	void srlz(Ts && dest)const{
		base_type::srlz(dest);

		srlz::serialize(dest, _code);
		srlz::serialize(dest, _bin);
		}

	/*! �������������� ������� �� ��������� ������� */
	template<typename Ts>
	void dsrlz(Ts && src ){
		base_type::dsrlz(src);

		srlz::deserialize(src, _code);
		srlz::deserialize(src, _bin);
		}

	size_t get_serialized_size()const noexcept;
	};



class i_rt0_invalid_xpeer_t final : public i_is_broadcast_command_t {
private:
	rtl_roadmap_obj_t _rdmlink;

public:
	using base_type = i_is_broadcast_command_t;

	i_rt0_invalid_xpeer_t()noexcept;
	i_rt0_invalid_xpeer_t( rtl_roadmap_obj_t && targetRdm )noexcept;

	const rtl_roadmap_obj_t& target_rdm()const noexcept;

	std::unique_ptr<i_rt0_command_t> copy()const noexcept final;
	rtx_command_type_t type()const noexcept final { return rtx_command_type_t::invalid_xpeer; }

	/*! ������������ ������� � �������� ������ */
	template<typename Ts>
	void srlz(Ts && dest)const{
		base_type::srlz(dest);

		srlz::serialize(dest, _rdmlink);
		}

	/*! �������������� ������� �� ��������� ������� */
	template<typename Ts>
	void dsrlz(Ts && src ){
		base_type::dsrlz(src);

		srlz::deserialize(src, _rdmlink);
		}

	size_t get_serialized_size()const noexcept;
	};



/*! ����������� ���������� �����������, ��������� �� �������������� ������ 0, �������� ������ 1 */
class i_rt0_cmd_outcoming_oppened_t final : public i_command_from_rt02rt1_t{
private:
	rt0_node_rdm_t _nodeId;
	rt1_endpoint_t _ep;
	_address_event_t _subscriber4WaitConnection;

public:
	using base_type = i_command_from_rt02rt1_t;

	i_rt0_cmd_outcoming_oppened_t()noexcept {}

	i_rt0_cmd_outcoming_oppened_t( const rt0_node_rdm_t &nodeId,
		const rtl_roadmap_event_t& tid,
		const rt1_endpoint_t & ep,
		const _address_event_t & subscriber4WaitConnection );

	const _address_event_t& subscriber_for_wait_connection()const noexcept;
	const rt0_node_rdm_t& node_id()const noexcept;
	const rt1_endpoint_t& endpoint()const noexcept;
	std::unique_ptr<i_rt0_command_t> copy()const noexcept final;
	rtx_command_type_t type()const noexcept final { return rtx_command_type_t::out_connection_stated_ready; }

	/*! ������������ ������� � �������� ������ */
	template<typename Ts>
	void srlz(Ts && dest)const{
		base_type::srlz(dest);

		srlz::serialize(dest, _nodeId);
		srlz::serialize(dest, _ep);
		srlz::serialize(dest, _subscriber4WaitConnection);
		}

	/*! �������������� ������� �� ��������� ������� */
	template<typename Ts>
	void dsrlz(Ts && src ){
		base_type::dsrlz(src);

		srlz::deserialize(src, _nodeId);
		srlz::deserialize(src, _ep);
		srlz::deserialize(src, _subscriber4WaitConnection);
		}

	size_t get_serialized_size()const noexcept;
	};


/*! ����������� ���������� �����������, ��������� �� �������������� ������ 0, �������� ������ 1 */
class i_rt0_cmd_sndrcv_fault_t final : public i_is_broadcast_command_t {
private:
	outbound_message_desc_t _sndrcvh;
	std::vector<uint8_t> _pckexc;
	std::unique_ptr<dcf_exception_t> _e;

public:
	using base_type = i_is_broadcast_command_t;

	i_rt0_cmd_sndrcv_fault_t()noexcept {}

	i_rt0_cmd_sndrcv_fault_t(const i_rt0_cmd_sndrcv_fault_t&) = delete;
	i_rt0_cmd_sndrcv_fault_t& operator=(const i_rt0_cmd_sndrcv_fault_t&) = delete;

	i_rt0_cmd_sndrcv_fault_t(i_rt0_cmd_sndrcv_fault_t&&)noexcept = default;
	i_rt0_cmd_sndrcv_fault_t& operator=(i_rt0_cmd_sndrcv_fault_t&&)noexcept = default;

	i_rt0_cmd_sndrcv_fault_t(const distributed_ctx_t & ctx, 
		outbound_message_desc_t && sndrcvh,
		std::unique_ptr<dcf_exception_t> && e)noexcept;

	const outbound_message_desc_t& hndl()const noexcept;
	std::unique_ptr<i_rt0_command_t> copy()const noexcept final;
	rtx_command_type_t type()const noexcept final { return rtx_command_type_t::sndrcv_fault; }

	template<typename Ts>
	void srlz(Ts && dest)const{
		base_type::srlz(dest);

		srlz::serialize(dest, _sndrcvh);
		srlz::serialize(dest, _pckexc);
		}

	template<typename Ts>
	void dsrlz(Ts && src ){
		base_type::dsrlz(src);

		srlz::deserialize(src, _sndrcvh);
		srlz::deserialize(src, _pckexc);
		}

	size_t get_serialized_size()const noexcept;

	void unpack(const distributed_ctx_t & ctx);

	std::unique_ptr<dcf_exception_t> ntf() && noexcept;
	const std::unique_ptr<dcf_exception_t>& ntf()const & noexcept;
	};


/*! ����������� ������ 0 �������(��������) */
class i_rt0_cmd_incoming_closed_t final : public i_is_broadcast_command_t {
private:
	rt0_node_rdm_t _nodeId;

public:
	using base_type = i_is_broadcast_command_t;

	i_rt0_cmd_incoming_closed_t()noexcept {}
	i_rt0_cmd_incoming_closed_t( const rt0_node_rdm_t & nodeId, const rtl_roadmap_event_t &tid );
	
	std::unique_ptr<i_rt0_command_t> copy()const noexcept final;
	const rt0_node_rdm_t& node_id()const noexcept;
	rtx_command_type_t type()const noexcept final { return rtx_command_type_t::in_connectoin_closed; }

	/*! ������������ ������� � �������� ������ */
	template<typename Ts>
	void srlz(Ts && dest)const{
		base_type::srlz(dest);

		srlz::serialize(dest, _nodeId);
		}

	/*! �������������� ������� �� ��������� ������� */
	template<typename Ts>
	void dsrlz(Ts && src ){
		base_type::dsrlz(src);

		srlz::deserialize(src, _nodeId);
		}

	size_t get_serialized_size()const noexcept;
	};

/*! ����������� ������ 0 �������(���������) */
class i_rt0_cmd_outcoming_closed_t final : public i_is_broadcast_command_t {
private:
	rt0_node_rdm_t _nodeId;

public:
	using base_type = i_is_broadcast_command_t;

	i_rt0_cmd_outcoming_closed_t() {}
	i_rt0_cmd_outcoming_closed_t( const rt0_node_rdm_t & nodeId, const rtl_roadmap_event_t &tid );

	std::unique_ptr<i_rt0_command_t> copy()const noexcept final;
	const rt0_node_rdm_t& node_id()const noexcept;
	rtx_command_type_t type()const noexcept final { return rtx_command_type_t::out_connection_closed; }

	/*! ������������ ������� � �������� ������ */
	template<typename Ts>
	void srlz(Ts && dest)const{
		base_type::srlz(dest);

		srlz::serialize(dest, _nodeId);
		}

	/*! �������������� ������� �� ��������� ������� */
	template<typename Ts>
	void dsrlz(Ts && src ){
		base_type::dsrlz(src);

		srlz::deserialize(src, _nodeId);
		}

	size_t get_serialized_size()const noexcept;
	};


class i_rt0_cmd_sndrcv_result_t final : public i_command_from_rt02rt1_t {
private:
	std::string _message;

public:
	using base_type = i_command_from_rt02rt1_t;

	i_rt0_cmd_sndrcv_result_t();
	i_rt0_cmd_sndrcv_result_t( const rtl_roadmap_event_t & targetRdm,
		const std::string & message );

	const std::string& message()const noexcept;

	std::unique_ptr<i_rt0_command_t> copy()const noexcept final;
	rtx_command_type_t type()const noexcept final { return rtx_command_type_t::sndrcv_result; }

	template<typename Ts>
	void srlz(Ts && dest)const{
		base_type::srlz(dest);

		srlz::serialize(dest, _message);
		}

	template<typename Ts>
	void dsrlz(Ts && src ){
		base_type::dsrlz(src);

		srlz::deserialize(src, _message);
		}

	size_t get_serialized_size()const noexcept;
	};



/*! �������� ������� � ������ 1 �� ������� �/��� ������� ������ 0 */
class module_channel_connection_string final : public i_command_from_rt1_t {
private:
	std::string _mChannelName;
	std::string _cChannelName;

public:
	using base_type = i_command_from_rt1_t;

	module_channel_connection_string()noexcept;
	module_channel_connection_string(std::string mCName, std::string cCName);

	std::unique_ptr<i_rt0_command_t> copy()const noexcept final;
	rtx_command_type_t type()const noexcept final { return rtx_command_type_t::xchannel_rt1_initial; }

	/*! ������������ ������� � �������� ������ */
	template<typename Ts>
	void srlz(Ts && dest)const{
		base_type::srlz(dest);

		srlz::serialize(dest, _mChannelName);
		srlz::serialize(dest, _cChannelName);
		}

	/*! �������������� ������� �� ��������� ������� */
	template<typename Ts>
	void dsrlz(Ts && src ){
		base_type::dsrlz(src);

		srlz::deserialize(src, _mChannelName);
		srlz::deserialize(src, _cChannelName);
		}

	size_t get_serialized_size()const noexcept;
	};
}

