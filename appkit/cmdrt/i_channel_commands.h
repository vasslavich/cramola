#pragma once


#include <memory>
#include "./base_terms.h"
#include "./i_channel_base.h"
#include "./rtl_rdm_event_desc.h"
#include "./command_pack.h"
#include "../utilities/handlers/invoke_guard.h"


namespace crm::detail {


class i_command_from_rt02rt1_t;


struct i_rt1_command_link_t{
	virtual ~i_rt1_command_link_t() = 0;

	/*! ���������� ��������� */
	virtual void rt1_router_hndl( std::unique_ptr<i_command_from_rt02rt1_t> && response) = 0;

	/*! ����� �������� ������� */
	virtual const rtl_roadmap_event_t& ev_rdm()const noexcept = 0;

	/*! ��������� ����� ������� */
	virtual const local_command_identity_t& local_object_command_id()const noexcept = 0;

	virtual void unroute_command_link()noexcept = 0;

	virtual bool is_once()const noexcept = 0;
	};


class io_x_irt1_commands_t : public io_x_rt1_base_t{
public:
	virtual ~io_x_irt1_commands_t() = 0;

	virtual bool pop_from_rt0( command_rdm_t & ev,
		const std::chrono::milliseconds & wait ) = 0;
	};






class i_command_from_rt1_t : public i_rt0_command_t{
private:
	channel_identity_t _channelId;
	local_command_identity_t _cmdId;
	rtl_roadmap_event_t _sourceObjectRdm;

protected:
	template<typename Tws>
	void srlz(Tws && dest)const{
		i_rt0_command_t::srlz(dest);

		srlz::serialize(dest, _channelId);
		srlz::serialize(dest, _cmdId);
		srlz::serialize(dest, _sourceObjectRdm);
		}

	template<typename Trs>
	void dsrlz(Trs && src ){
		i_rt0_command_t::dsrlz(src);

		srlz::deserialize(src, _channelId);
		srlz::deserialize(src, _cmdId);
		srlz::deserialize(src, _sourceObjectRdm);
		}

	size_t get_serialized_size()const noexcept;

public:
	i_command_from_rt1_t()noexcept{}

	rtl_level_t level()const noexcept{ return rtl_level_t::l1; }

	const channel_identity_t& channel_id()const noexcept;
	void set_channel_id( const channel_identity_t & chId );

	const local_command_identity_t& command_id()const noexcept;
	void set_command_id( const local_command_identity_t& id );

	void set_command_evrdm( const rtl_roadmap_event_t & sourceObjectRdm );
	const rtl_roadmap_event_t& command_source_emitter_rdm()const noexcept;
	};



class i_is_without_response_t : public i_command_from_rt1_t {};



template<>
struct comparator_cast<i_command_from_rt1_t, std::void_t<>> {
	const i_command_from_rt1_t& v;

	comparator_cast(const i_command_from_rt1_t& v_)
		:v(v_) {}

	bool equals(const comparator_cast<i_command_from_rt1_t>& r)const noexcept {
		return v.level() == r.v.level() &&
			v.channel_id() == r.v.channel_id() &&
			v.command_id() == r.v.command_id() &&
			v.command_source_emitter_rdm() == r.v.command_source_emitter_rdm();
		}
	};




struct i_command_from_rt1_unit{
	using fail_f = std::function<void()>;

	std::unique_ptr<i_command_from_rt1_t> cmd;
	fail_f f;

	~i_command_from_rt1_unit();
	i_command_from_rt1_unit( fail_f && f_ );
	void reset_failback()noexcept;
	};



template<typename TCmd>
bool is_without_response( const std::unique_ptr<TCmd> & cmd )noexcept{
	return is_without_response( cmd.get() );
	}


template<typename TCmd>
bool is_without_response( const TCmd* pcmd ) noexcept{
	return pcmd && nullptr != dynamic_cast<const i_is_without_response_t*>(pcmd);
	}

template<typename _TCmd,
	typename TCmd = std::decay_t<_TCmd>,
	typename std::enable_if_t<!is_smart_pointer_v<TCmd> && std::is_base_of_v<i_is_without_response_t, TCmd>, int> = 0>
constexpr bool is_without_response(_TCmd &&)noexcept{
	return true;
	}

template<typename _TCmd,
	typename TCmd = std::decay_t<_TCmd>,
	typename std::enable_if_t<(
		!is_smart_pointer_v<TCmd> &&
		!std::is_pointer_v<TCmd> &&
		!std::is_base_of_v<i_is_without_response_t, TCmd>), int> = 0>
constexpr bool is_without_response(_TCmd&&)noexcept {
	return false;
	}

template<typename TCmd>
bool is_with_response( const std::unique_ptr<TCmd> & cmd )noexcept{
	return !is_without_response( cmd );
	}

template<typename TCmd>
bool is_with_response(const TCmd * cmd)noexcept {
	return !is_without_response(cmd);
	}





class i_command_from_rt02rt1_t : public i_rt0_command_t{
	rtl_roadmap_event_t _targetId;
	local_command_identity_t _sourceCommandId;
	inovked_as _is{ inovked_as::sync };

protected:
	i_command_from_rt02rt1_t( const rtl_roadmap_event_t & tid );

	template<typename Ts>
	void srlz(Ts && dest)const{
		i_rt0_command_t::srlz(dest);

		srlz::serialize(dest, _targetId);
		srlz::serialize(dest, _sourceCommandId);
		}

	template<typename Ts>
	void dsrlz(Ts && src ){
		i_rt0_command_t::dsrlz(src);

		srlz::deserialize(src, _targetId);
		srlz::deserialize(src, _sourceCommandId);
		}

	size_t get_serialized_size()const noexcept;

public:
	i_command_from_rt02rt1_t()noexcept;

	const rtl_roadmap_event_t& command_target_receiver_id()const noexcept;
	void set_target_receiver_id( const rtl_roadmap_event_t & targetId );

	const local_command_identity_t& source_command_id()const noexcept;
	void set_source_command_id( const local_command_identity_t& cmdid )noexcept;

	void set_invoked_opt( inovked_as )noexcept;
	inovked_as invoked_as()const noexcept;
	};


class i_is_broadcast_command_t : public i_command_from_rt02rt1_t {
protected:
	i_is_broadcast_command_t()noexcept{}
	i_is_broadcast_command_t(const rtl_roadmap_event_t& tid);
	};


struct io_x_ort1_commands_t{
	virtual ~io_x_ort1_commands_t() = 0;

protected:
	[[nodiscard]]
	virtual addon_push_event_result_t push_command_pack_to_rt0(rt0_x_command_pack_t&& cmd)noexcept = 0;

public:
	template<typename _TxCommand,
		typename C = typename std::decay_t<_TxCommand>,
		typename std::enable_if_t<std::is_base_of_v<i_command_from_rt1_t, C>&& std::is_final_v<C>, int> = 0>
	addon_push_event_result_t push_to_rt0_command(_TxCommand && cmd) {
		return push_command_pack_to_rt0(rt0_x_command_pack_t{ std::forward<_TxCommand>(cmd) });
		}
	};




template<typename TCmd>
bool is_broadcast_command( const std::unique_ptr<TCmd> & cmd )noexcept{
	return is_broadcast_command( cmd.get() );
	}


template<typename TCmd>
bool is_broadcast_command( const TCmd* pcmd ) noexcept{
	return pcmd && nullptr != dynamic_cast<const i_is_broadcast_command_t*>(pcmd);
	}

template<typename _TCmd,
	typename TCmd = std::decay_t<_TCmd>,
	typename std::enable_if_t<!is_smart_pointer_v<TCmd> && std::is_base_of_v<i_is_broadcast_command_t, TCmd>, int> = 0>
constexpr bool is_broadcast_command(_TCmd &&)noexcept {
	return true;
	}

template<typename _TCmd,
	typename TCmd = std::decay_t<_TCmd>,
typename std::enable_if_t<(
	!is_smart_pointer_v<TCmd> &&
	!std::is_pointer_v<TCmd> && 
	!std::is_base_of_v<i_is_broadcast_command_t, TCmd>), int> = 0>
constexpr bool is_broadcast_command(_TCmd&&)noexcept {
	return false;
	}



template<typename YCommand,
	typename XCommand,
	typename std::enable_if < (std::is_base_of<i_rt0_command_t, XCommand>::value
		&& std::is_nothrow_move_assignable<std::unique_ptr<YCommand>>::value
		&& std::is_nothrow_move_constructible<std::unique_ptr<YCommand>>::value), int>::type = 0 >
	std::unique_ptr<typename YCommand> tcommand_cast( std::unique_ptr<XCommand> && baseObj ) noexcept{

	auto xNPtr = dynamic_cast<YCommand*>(baseObj.get());
	if( xNPtr ){
		std::unique_ptr<YCommand> xU( xNPtr );
		baseObj.release();

		return std::move( xU );
		}
	else
		return std::unique_ptr<YCommand>();
	}


template<typename TCommand,
	typename XCommand = i_command_from_rt1_t,
	typename std::enable_if < (std::is_base_of<i_command_from_rt1_t, XCommand>::value
		&& std::is_base_of<i_command_from_rt1_t, TCommand>::value), int>::type = 0 >
	std::unique_ptr<typename TCommand> command_cast( std::unique_ptr<XCommand> && baseObj )noexcept{
	return tcommand_cast<TCommand, XCommand>(std::move( baseObj ));
	}

template<typename TCommand,
	typename XCommand = i_command_from_rt02rt1_t,
	typename std::enable_if < (std::is_base_of<i_command_from_rt02rt1_t, XCommand>::value
		&& std::is_base_of<i_command_from_rt02rt1_t, TCommand>::value), int>::type = 0 >
	std::unique_ptr<typename TCommand> command_cast( std::unique_ptr<XCommand> && baseObj ) noexcept{
	return tcommand_cast<TCommand, XCommand>(std::move( baseObj ));
	}

template<typename TCommand,
	typename std::enable_if<std::is_base_of<i_rt0_command_t, TCommand>::value, int>::type = 0 >
	std::unique_ptr<typename TCommand> command_cast( std::unique_ptr<i_rt0_command_t> && baseObj )noexcept{
	return tcommand_cast<TCommand, i_rt0_command_t>(std::move( baseObj ));
	}


enum class i_command_link_type_t{
	shared,
	weak
	};

struct i_command_link_wrapper_t{
	virtual ~i_command_link_wrapper_t(){}

	virtual std::shared_ptr<i_rt1_command_link_t> link()const & noexcept = 0;
	virtual std::shared_ptr<i_rt1_command_link_t> link() && noexcept = 0;
	virtual i_command_link_type_t type()const noexcept = 0;
	virtual std::unique_ptr<i_command_link_wrapper_t> copy()const noexcept = 0;
	};

struct command_link_wrapper_weak_t : public i_command_link_wrapper_t{
#ifdef CBL_MPF_OBJECTS_COUNTER_CHECKER_HOST_COMMAND
	crm::utility::__xobject_counter_logger_t<command_link_wrapper_weak_t, 1> _xDbgCounter;
#endif

	std::weak_ptr<i_rt1_command_link_t> ptr;

	command_link_wrapper_weak_t()noexcept;
	command_link_wrapper_weak_t( std::weak_ptr<i_rt1_command_link_t> ptr_ );

	std::shared_ptr<i_rt1_command_link_t> link()const & noexcept final;
	std::shared_ptr<i_rt1_command_link_t> link() && noexcept final;
	i_command_link_type_t type()const noexcept final;
	std::unique_ptr<i_command_link_wrapper_t> copy()const noexcept final;
	};

struct command_link_wrapper_strong_t : public i_command_link_wrapper_t{
#ifdef CBL_MPF_OBJECTS_COUNTER_CHECKER_HOST_COMMAND
	crm::utility::__xobject_counter_logger_t<command_link_wrapper_strong_t, 1> _xDbgCounter;
#endif

	std::shared_ptr<i_rt1_command_link_t> ptr;

	command_link_wrapper_strong_t()noexcept;
	command_link_wrapper_strong_t( std::shared_ptr<i_rt1_command_link_t> ptr_ );

	std::shared_ptr<i_rt1_command_link_t> link()const & noexcept final;
	std::shared_ptr<i_rt1_command_link_t> link() && noexcept final;
	i_command_link_type_t type()const noexcept final;
	std::unique_ptr<i_command_link_wrapper_t> copy()const noexcept final;
	};


class __command_error_handler_point_f{

	using functor_base = crm::utility::invoke_functor<void,
		std::unique_ptr<i_command_from_rt02rt1_t> &&,
		std::unique_ptr<dcf_exception_t> &&>;
	struct functor : functor_base{

		void operator()( std::unique_ptr<dcf_exception_t> && e )noexcept;
		void operator()( std::unique_ptr<i_command_from_rt02rt1_t> && response);

		template<typename TX,
			typename std::enable_if_t < std::is_invocable_v<TX, std::unique_ptr<i_command_from_rt02rt1_t> &&, std::unique_ptr<dcf_exception_t> &&>, int > = 0 >
			functor( TX && h_ )noexcept
			: functor_base( std::forward<TX>( h_ ) ){}

		functor()noexcept;
		};

	using function_handler_type = functor_invoke_guard_t <functor, make_fault_result_t>;
	function_handler_type _h;

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	utility::__xobject_counter_logger_t<__command_error_handler_point_f, 10> _xDbgCounter;
#endif

public:
	__command_error_handler_point_f()noexcept;

	template<typename TX,
		typename std::enable_if_t < std::is_invocable_v<TX, std::unique_ptr<i_command_from_rt02rt1_t> &&, std::unique_ptr<dcf_exception_t> &&>, int > = 0 >
		__command_error_handler_point_f( TX && h_,
			std::weak_ptr<distributed_ctx_t> ctx_,
			async_space_t scx )
		: _h( std::forward<TX>( h_ ), ctx_, "__command_error_handler_point_f", scx, function_handler_type::invoke_once ){}

	void operator()( std::unique_ptr<dcf_exception_t> && e )noexcept;
	void operator()( std::unique_ptr<i_command_from_rt02rt1_t> && response);

	operator bool()const noexcept;
	};
}

