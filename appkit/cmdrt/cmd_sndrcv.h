#pragma once


#include <memory>
#include <functional>
#include <chrono>
#include <future>
#include "../internal_invariants.h"
#include "../eval/async_decls.h"
#include "./i_commands.h"
#include "./i_command_router.h"
#include "./i_channel_commands.h"
#include "../time/i_timers.h"




namespace crm::detail{


class xchannel_cmd_sndrcv_t final:
	public i_rt1_command_link_t,
	public std::enable_shared_from_this<xchannel_cmd_sndrcv_t> {

public:
	struct invoke_as_watched{};
	struct invoke_as_anchor {};
	struct invoke_as_dettached{};

	struct sndrcv_tail_t {
		friend class xchannel_cmd_sndrcv_t;

	private:
		std::weak_ptr<distributed_ctx_t> ctx;
		std::weak_ptr<rt1_command_router_t> rt;

#ifdef CBL_MPF_OBJECTS_COUNTER_CHECKER_HOST_COMMAND
		crm::utility::__xobject_counter_logger_t<sndrcv_tail_t, 1> _xDbgCounter;
#endif
		};

	typedef std::function<void( async_operation_result_t,
		std::unique_ptr<i_command_from_rt02rt1_t> && response,
		std::unique_ptr<i_command_result_t> &&, 
		std::unique_ptr<dcf_exception_t> && n )> result_hndl_t;

	struct i_result_hndl_invoker {
		virtual ~i_result_hndl_invoker() {}

#ifdef CBL_MPF_OBJECTS_COUNTER_CHECKER_HOST_COMMAND
		crm::utility::__xobject_counter_logger_t<i_result_hndl_invoker, 1> _xDbgCounter;
#endif

		virtual  void operator()(async_operation_result_t,
			std::unique_ptr<i_command_from_rt02rt1_t> && response,
			std::unique_ptr<i_command_result_t> &&,
			std::unique_ptr<dcf_exception_t> && n) = 0;
		};

	template<typename THandler>
	struct result_hndl_invoker : i_result_hndl_invoker {
		THandler _h;
		std::atomic<bool> _inf{ false };

		void invoke(async_operation_result_t ar,
			std::unique_ptr<i_command_from_rt02rt1_t> && response,
			std::unique_ptr<i_command_result_t> && r,
			std::unique_ptr<dcf_exception_t> && n) {
			
			bool inf{ false };
			if(_inf.compare_exchange_strong(inf, true)) {
				auto h = std::move(_h);
				h(ar, std::move(response), std::move(r), std::move(n));
				}
			}

		template<typename XHandler>
		result_hndl_invoker(XHandler && h)noexcept
			: _h(std::forward<XHandler>(h)){}

		void operator()(async_operation_result_t ar,
			std::unique_ptr<i_command_from_rt02rt1_t> && response,
			std::unique_ptr<i_command_result_t> && r,
			std::unique_ptr<dcf_exception_t> && n) final{
			
			invoke(ar, std::move(response), std::move(r), std::move(n));
			}

		result_hndl_invoker(const result_hndl_invoker &) = delete;
		result_hndl_invoker& operator=(const result_hndl_invoker &) = delete;

		~result_hndl_invoker() {
			CHECK_NO_EXCEPTION(invoke(async_operation_result_t::st_abandoned, nullptr, nullptr, nullptr));
			}
		};

	template<typename TResultHandler>
	static decltype(auto) make_result_handler(TResultHandler && rh) {
		return std::make_unique<result_hndl_invoker<TResultHandler>>(std::forward<TResultHandler>(rh));
		}

	typedef std::function<std::unique_ptr<i_command_result_t>( const std::unique_ptr<i_command_from_rt02rt1_t> & response )> response_verifier_t;

	struct command_handler_watch_base_t : i_async_expression_handler_t {
		friend class xchannel_cmd_sndrcv_t;

	private:
		std::unique_ptr<dcf_exception_t> _ntf;
		std::unique_ptr<i_command_from_rt02rt1_t> _response;
		std::unique_ptr<i_command_result_t> _result;
		async_operation_result_t _code = async_operation_result_t::st_abandoned;
		std::shared_future<void> _waitObject;
		std::atomic<bool> _setf = false;
		std::atomic<bool> _infokef = false;

#ifdef CBL_MPF_OBJECTS_COUNTER_CHECKER_HOST_COMMAND
		crm::utility::__xobject_counter_logger_t<command_handler_watch_base_t, 1> _xDbgCounter;
#endif

		void assign(command_handler_watch_base_t && o)noexcept;
		void set_result( async_operation_result_t c,
			std::unique_ptr<i_command_from_rt02rt1_t> && response,
			std::unique_ptr<i_command_result_t> && r,
			std::unique_ptr<dcf_exception_t> && n );

	public:
		command_handler_watch_base_t()noexcept;

		command_handler_watch_base_t( const command_handler_watch_base_t & ) = delete;
		command_handler_watch_base_t& operator=(const command_handler_watch_base_t &) = delete;

		command_handler_watch_base_t(command_handler_watch_base_t && o )noexcept;
		command_handler_watch_base_t& operator=(command_handler_watch_base_t && o)noexcept;

		bool wait( const std::chrono::milliseconds & timeout );
		void wait();

		std::unique_ptr<dcf_exception_t> captue_exc()noexcept;
		std::unique_ptr<i_command_result_t> capture_result()noexcept;
		std::unique_ptr<i_command_from_rt02rt1_t> capture_response()noexcept;

		bool ready()const noexcept;
		async_operation_result_t code()const noexcept;
		};

	struct strong_handler_anchor_watch_t : command_handler_watch_base_t {
		friend class xchannel_cmd_sndrcv_t;

	private:
		std::shared_ptr<xchannel_cmd_sndrcv_t> _spCmd;

#ifdef CBL_MPF_OBJECTS_COUNTER_CHECKER_HOST_COMMAND
		crm::utility::__xobject_counter_logger_t<strong_handler_anchor_watch_t, 1> _xDbgCounter;
#endif

	public:
		~strong_handler_anchor_watch_t();
		void cancel()noexcept;
		};

	struct weak_handler_anchor_t : i_async_expression_handler_t {
		friend class xchannel_cmd_sndrcv_t;

	private:
		std::weak_ptr<xchannel_cmd_sndrcv_t> _wpCmd;

#ifdef CBL_MPF_OBJECTS_COUNTER_CHECKER_HOST_COMMAND
		crm::utility::__xobject_counter_logger_t<weak_handler_anchor_t, 1> _xDbgCounter;
#endif

	public:
		~weak_handler_anchor_t();
		void cancel()noexcept;
		};

	struct strong_handler_anchor_t : i_async_expression_handler_t {
		friend class xchannel_cmd_sndrcv_t;

	private:
		std::shared_ptr<xchannel_cmd_sndrcv_t> _spCmd;

#ifdef CBL_MPF_OBJECTS_COUNTER_CHECKER_HOST_COMMAND
		crm::utility::__xobject_counter_logger_t<strong_handler_anchor_t, 1> _xDbgCounter;
#endif

	public:
		~strong_handler_anchor_t();
		void cancel()noexcept;
		};

private:
	enum class anchor_mode {
		route_table,
		timer,
		extern_
		};

	std::weak_ptr<distributed_ctx_t> _ctx;
	rtl_roadmap_event_t _rdmEv;
	local_command_identity_t _sndCommandId;
	std::weak_ptr<rt1_command_router_t> _router;
	std::unique_ptr<i_xtimer_oncecall_t> _timer;
	response_verifier_t _rcvVrf;
	std::unique_ptr<i_result_hndl_invoker> _resultHndl;
	std::atomic<bool> _hndlHasDone{ false };
	std::atomic<bool> _canceled{ false };
	std::atomic<bool> _unregistered{ false };

#ifdef CBL_MPF_OBJECTS_COUNTER_CHECKER_HOST_COMMAND
	crm::utility::__xobject_counter_logger_t<xchannel_cmd_sndrcv_t,1> _xDbgCounter;
#endif

#if defined(CBL_MPF_TRACELEVEL_COMMANDS_HOST) && defined(CBL_MPF_OBJECTS_COUNTER_CHECKER_HOST_COMMAND)
	std::string _tag;
#endif

	std::shared_ptr<distributed_ctx_t> ctx()const noexcept;
	std::shared_ptr<rt1_command_router_t> rt()const noexcept;

	response_verifier_t  make_default_verifier()const;

	bool check_set_unregf()noexcept;
	bool invoke_test_and_set()noexcept;

	void __invoke_hndl_async_( async_operation_result_t result,
		std::unique_ptr<i_command_from_rt02rt1_t> && response )noexcept;

	void __invoke_hndl_sync_(async_operation_result_t result,
		std::unique_ptr<i_command_from_rt02rt1_t> && response)noexcept;

	void invoke_hndl(async_operation_result_t result,
		std::unique_ptr<i_command_from_rt02rt1_t> && response )noexcept;

	void invoke_hndl(std::unique_ptr<dcf_exception_t> && e)noexcept;
	void invoke_hndl(async_operation_result_t)noexcept;

	bool is_once()const noexcept final;
	void unroute_command_link()noexcept final;
	void rt1_router_hndl( std::unique_ptr<i_command_from_rt02rt1_t> && response)noexcept final;

	template<typename _TxCommand,
		typename C = typename std::decay_t<_TxCommand>,
		typename std::enable_if_t<std::is_base_of_v<i_command_from_rt1_t, C>&& std::is_final_v<C>,int> = 0>
	void start(const sndrcv_timeline_t& tml,
		_TxCommand && cmd,
		anchor_mode acMode) {
		
		if (!__validate_cmd(cmd)) {
			THROW_MPF_EXC_FWD(nullptr);
			}

		register_routing(acMode);
		start_timer(tml, acMode);

		push(std::forward< _TxCommand>(cmd));
		}

	void start_timer(const sndrcv_timeline_t & tml, anchor_mode acMode);
	void timer_invoke(async_operation_result_t rc)noexcept;

	void timer_stop()noexcept;

	const rtl_roadmap_event_t& ev_rdm()const noexcept final;
	const rtl_roadmap_obj_t& msg_rdm()const noexcept;
	const local_command_identity_t& local_object_command_id()const noexcept final;

	template<typename _TxCommand,
		typename C = typename std::decay_t<_TxCommand>,
		typename = std::enable_if_t<std::is_base_of_v<i_command_from_rt1_t, C>&& std::is_final_v<C>>>
	void push(_TxCommand&& cmd) {
		if (auto rt_ = rt()) {
			cmd.set_command_id(_sndCommandId);
			cmd.set_command_evrdm(ev_rdm());

			if (rt_->push2channel(std::forward<_TxCommand>(cmd)) != addon_push_event_result_t::enum_type::success) {
				invoke_hndl(CREATE_MPF_PTR_EXC_FWD(nullptr));
				}

#if defined(CBL_MPF_TRACELEVEL_COMMAND_STAGE_PRINT)
			std::string log;
			log += _tag + ":";
			log += ":push command:" + _sndCommandId.to_str();
			crm::file_logger_t::logging(log, 1051, __CBL_FILEW__, __LINE__);
#endif
			}
		else {
			THROW_MPF_EXC_FWD(nullptr);
			}
		}

	void register_routing(anchor_mode acMode);
	void unregistrer_routing()noexcept;

	bool canceled()noexcept;

	void _dctr()noexcept;

	bool __validate_cmd( const i_command_from_rt1_t & cmd )const noexcept;
	
	static std::shared_ptr<strong_handler_anchor_watch_t> make_as_strong_anchor_watch(std::string && tag,
		std::weak_ptr<distributed_ctx_t> ctx,
		std::weak_ptr<rt1_command_router_t> rt,
		response_verifier_t && rcvVrf,
		rtl_level_t cmdLevel );

	static std::shared_ptr<strong_handler_anchor_watch_t> make_as_strong_anchor_watch(std::string && tag,
		std::weak_ptr<distributed_ctx_t> ctx,
		std::weak_ptr<rt1_command_router_t> rt,
		rtl_level_t cmdLevel);

	static std::shared_ptr<strong_handler_anchor_watch_t> make_as_strong_anchor_watch(std::string && tag,
		std::weak_ptr<distributed_ctx_t> ctx,
		std::weak_ptr<rt1_command_router_t> rt,
		response_verifier_t && rcvVrf,
		std::unique_ptr<i_result_hndl_invoker> && hndl,
		rtl_level_t cmdLevel );

	static std::shared_ptr<strong_handler_anchor_t> make_as_strong_anchor(std::string && tag,
		std::weak_ptr<distributed_ctx_t> ctx,
		std::weak_ptr<rt1_command_router_t> rt,
		response_verifier_t && rcvVrf,
		std::unique_ptr<i_result_hndl_invoker> && hndl,
		rtl_level_t cmdLevel);

	static std::pair<std::shared_ptr<xchannel_cmd_sndrcv_t>, std::shared_ptr<weak_handler_anchor_t>> make_as_weak_anchor(std::string && tag,
		std::weak_ptr<distributed_ctx_t> ctx,
		std::weak_ptr<rt1_command_router_t> rt,
		response_verifier_t && rcvVrf,
		std::unique_ptr<i_result_hndl_invoker> && hndl,
		rtl_level_t cmdLevel );


	template<typename _TxCommand,
		typename C = typename std::decay_t<_TxCommand>,
		typename = std::enable_if_t<std::is_base_of_v<i_command_from_rt1_t, C>&& std::is_final_v<C>>>
	static std::shared_ptr<i_async_expression_handler_t> __invoke_binded(std::string && tag,
		std::weak_ptr<distributed_ctx_t> ctx,
		std::weak_ptr<rt1_command_router_t> rt,
		_TxCommand && cmd,
		response_verifier_t && rcvVrf,
		std::unique_ptr<i_result_hndl_invoker>  && hndl) {

		auto h = make_as_strong_anchor_watch(std::move(tag), std::move(ctx), std::move(rt), std::move(rcvVrf), std::move(hndl), cmd.level());
		h->_spCmd->start(sndrcv_timeline_t::make_once_call(), std::forward<_TxCommand>(cmd), anchor_mode::extern_);

		return h;
		}


	template<typename _TxCommand,
		typename C = typename std::decay_t<_TxCommand>,
		typename = std::enable_if_t<std::is_base_of_v<i_command_from_rt1_t, C>&& std::is_final_v<C>>>
	static std::shared_ptr<i_async_expression_handler_t> __invoke_timed(std::string && tag,
		std::weak_ptr<distributed_ctx_t> ctx,
		std::weak_ptr<rt1_command_router_t> rt,
		_TxCommand && cmd,
		const sndrcv_timeline_timeouted_once & tml) {
		CBL_VERIFY(tml.value < sndrcv_timeline_timeouted_once::max_interval());

		auto h = make_as_weak_anchor(std::move(tag), std::move(ctx), std::move(rt), nullptr, nullptr, cmd.level());
		h.first->start(sndrcv_timeline_t::make(tml), std::forward<_TxCommand>(cmd), anchor_mode::timer);

		return h.second;
		}


	template<typename _TxCommand,
		typename C = typename std::decay_t<_TxCommand>,
		typename = std::enable_if_t<std::is_base_of_v<i_command_from_rt1_t, C>&& std::is_final_v<C>>>
	static std::shared_ptr<i_async_expression_handler_t> __invoke_binded(std::string && tag,
		std::weak_ptr<distributed_ctx_t> ctx,
		std::weak_ptr<rt1_command_router_t> rt,
		_TxCommand && cmd) {

		auto h = make_as_strong_anchor_watch(std::move(tag), std::move(ctx), std::move(rt), cmd.level());
		h->_spCmd->start(sndrcv_timeline_t::make_once_call(), std::forward<_TxCommand>(cmd), anchor_mode::extern_);

		return h;
		}


	template<typename _TxCommand,
		typename C = typename std::decay_t<_TxCommand>,
		typename = std::enable_if_t<std::is_base_of_v<i_command_from_rt1_t, C>&& std::is_final_v<C>>>
	static std::shared_ptr<i_async_expression_handler_t> __invoke_timed(std::string && tag,
		std::weak_ptr<distributed_ctx_t> ctx,
		std::weak_ptr<rt1_command_router_t> rt,
		_TxCommand&& cmd,
		response_verifier_t && rcvVrf,
		std::unique_ptr<i_result_hndl_invoker>  && hndl,
		const sndrcv_timeline_timeouted_once & tml) {
		CBL_VERIFY(tml.value < sndrcv_timeline_timeouted_once::max_interval());

		auto h = make_as_weak_anchor(std::move(tag), std::move(ctx), std::move(rt), std::move(rcvVrf), std::move(hndl), cmd.level());

		h.first->start(sndrcv_timeline_t::make(tml), std::forward<_TxCommand>(cmd), anchor_mode::timer);
		return h.second;
		}


	template<typename _TxCommand,
		typename C = typename std::decay_t<_TxCommand>,
		typename = std::enable_if_t<std::is_base_of_v<i_command_from_rt1_t, C>&& std::is_final_v<C>>>
	static void __invoke_push(std::string && tag,
		std::weak_ptr<distributed_ctx_t> ctx,
		std::weak_ptr<rt1_command_router_t> rt,
		_TxCommand&& cmd,
		response_verifier_t && rcvVrf,
		std::unique_ptr<i_result_hndl_invoker>  && hndl,
		const sndrcv_timeline_timeouted_once & tml) {
		CBL_VERIFY(tml.value < sndrcv_timeline_timeouted_once::max_interval());

		auto h = make_as_weak_anchor(std::move(tag), std::move(ctx), std::move(rt), std::move(rcvVrf), std::move(hndl), cmd.level());

		h.first->start(sndrcv_timeline_t::make(tml), std::forward<_TxCommand>(cmd), anchor_mode::timer);
		h.second->_wpCmd.reset();
		}


	template<typename _TxCommand,
		typename C = typename std::decay_t<_TxCommand>,
		typename = std::enable_if_t<std::is_base_of_v<i_command_from_rt1_t, C>&& std::is_final_v<C>>>
	static void __invoke_push(std::string && tag,
		std::weak_ptr<distributed_ctx_t> ctx,
		std::weak_ptr<rt1_command_router_t> rt,
		_TxCommand&& cmd,
		response_verifier_t && rcvVrf,
		std::unique_ptr<i_result_hndl_invoker>  && hndl) {

		auto h = make_as_weak_anchor(std::move(tag), std::move(ctx), std::move(rt), std::move(rcvVrf), std::move(hndl), cmd.level());

		h.first->start(sndrcv_timeline_t::make_once_call(), std::forward<_TxCommand>(cmd), anchor_mode::route_table);
		h.second->_wpCmd.reset();
		}


	template<typename _TxCommand,
		typename C = typename std::decay_t<_TxCommand>,
		typename = std::enable_if_t<std::is_base_of_v<i_command_from_rt1_t, C>&& std::is_final_v<C>>>
	static void __invoke_push(std::string && tag,
		std::weak_ptr<distributed_ctx_t> ctx,
		std::weak_ptr<rt1_command_router_t> rt,
		_TxCommand&& cmd,
		std::unique_ptr<i_result_hndl_invoker>  && hndl,
		const sndrcv_timeline_timeouted_once & tml) {
		CBL_VERIFY(tml.value < sndrcv_timeline_timeouted_once::max_interval());

		__invoke_push(std::move(tag), std::move(ctx), std::move(rt), std::forward<_TxCommand>(cmd), nullptr, std::move(hndl), tml);
		}


	template<typename _TxCommand,
		typename C = typename std::decay_t<_TxCommand>,
		typename = std::enable_if_t<std::is_base_of_v<i_command_from_rt1_t, C>&& std::is_final_v<C>>>
	static void __invoke_push(std::string && tag,
		std::weak_ptr<distributed_ctx_t> ctx,
		std::weak_ptr<rt1_command_router_t> rt,
		_TxCommand&& cmd,
		std::unique_ptr<i_result_hndl_invoker>  && hndl) {

		__invoke_push(std::move(tag), std::move(ctx), std::move(rt), std::forward<_TxCommand>(cmd), nullptr, std::move(hndl));
		}

public:
	~xchannel_cmd_sndrcv_t();

	xchannel_cmd_sndrcv_t(std::string && tag,
		std::weak_ptr<distributed_ctx_t> ctx_,
		std::weak_ptr<rt1_command_router_t> rt,
		response_verifier_t && rcvVrf,
		std::unique_ptr<i_result_hndl_invoker> && hndl,
		rtl_level_t l);

	template<typename TResultHandler,
		typename _TxCommand,
		typename C = typename std::decay_t<_TxCommand>,
		typename = std::enable_if_t<std::is_base_of_v<i_command_from_rt1_t, C>&& std::is_final_v<C>>>
	static std::shared_ptr<i_async_expression_handler_t> invoke_binded(std::string && tag,
		std::weak_ptr<distributed_ctx_t> ctx,
		std::weak_ptr<rt1_command_router_t> rt,
		_TxCommand&& cmd,
		response_verifier_t && rcvVrf,
		TResultHandler && hndl) {

		return __invoke_binded(std::move(tag), 
			std::move(ctx), std::move(rt), std::forward<_TxCommand>(cmd), std::move(rcvVrf), make_result_handler(std::forward<TResultHandler>(hndl)));
		}

	template<typename _TxCommand,
		typename C = typename std::decay_t<_TxCommand>,
		typename = std::enable_if_t<std::is_base_of_v<i_command_from_rt1_t, C>&& std::is_final_v<C>>>
		static std::shared_ptr<i_async_expression_handler_t> invoke_timed(std::string&& tag,
			std::weak_ptr<distributed_ctx_t> ctx,
			std::weak_ptr<rt1_command_router_t> rt,
			_TxCommand&& cmd,
			const sndrcv_timeline_timeouted_once& tml) {
		return __invoke_timed(std::move(tag), std::move(ctx), std::move(rt), std::forward<_TxCommand>(cmd), tml);
		}

	template<typename _TxCommand,
		typename C = typename std::decay_t<_TxCommand>,
		typename = std::enable_if_t<std::is_base_of_v<i_command_from_rt1_t, C>&& std::is_final_v<C>>>
	static std::shared_ptr<i_async_expression_handler_t> invoke_binded(std::string && tag,
		std::weak_ptr<distributed_ctx_t> ctx,
		std::weak_ptr<rt1_command_router_t> rt,
		_TxCommand&& cmd ) {

		return __invoke_binded(std::move(tag), std::move(ctx), std::move(rt), std::forward<_TxCommand>(cmd));
		}

	template<typename TResultHandler,
		typename _TxCommand,
		typename C = typename std::decay_t<_TxCommand>,
		typename = std::enable_if_t<std::is_base_of_v<i_command_from_rt1_t, C>&& std::is_final_v<C>>>
	static std::shared_ptr<i_async_expression_handler_t> invoke_timed(std::string && tag,
		std::weak_ptr<distributed_ctx_t> ctx,
		std::weak_ptr<rt1_command_router_t> rt,
		_TxCommand&& cmd,
		response_verifier_t && rcvVrf,
		TResultHandler && hndl,
		const sndrcv_timeline_timeouted_once & tml) {

		return __invoke_timed(std::move(tag), 
			std::move(ctx), std::move(rt), std::forward<_TxCommand>(cmd), std::move(rcvVrf), make_result_handler(std::forward<TResultHandler>(hndl)), tml);
		}

	template<typename TResultHandler,
		typename _TxCommand,
		typename C = typename std::decay_t<_TxCommand>,
		typename = std::enable_if_t<std::is_base_of_v<i_command_from_rt1_t, C>&& std::is_final_v<C>>>
	static void invoke_push(std::string && tag,
		std::weak_ptr<distributed_ctx_t> ctx,
		std::weak_ptr<rt1_command_router_t> rt,
		_TxCommand&& cmd,
		response_verifier_t && rcvVrf,
		TResultHandler && hndl,
		const sndrcv_timeline_timeouted_once & tml) {
		
		__invoke_push(std::move(tag),
			std::move(ctx), std::move(rt), std::forward<_TxCommand>(cmd), std::move(rcvVrf), make_result_handler(std::forward<TResultHandler>(hndl)), tml);
		}

	template<typename TResultHandler,
		typename _TxCommand,
		typename C = typename std::decay_t<_TxCommand>,
		typename = std::enable_if_t<std::is_base_of_v<i_command_from_rt1_t, C>&& std::is_final_v<C>>>
	static void invoke_push(std::string && tag,
		std::weak_ptr<distributed_ctx_t> ctx,
		std::weak_ptr<rt1_command_router_t> rt,
		_TxCommand&& cmd,
		response_verifier_t && rcvVrf,
		TResultHandler && hndl) {

		__invoke_push(std::move(tag),
			std::move(ctx), std::move(rt), std::forward<_TxCommand>(cmd), std::move(rcvVrf), make_result_handler(std::forward<TResultHandler>(hndl)));
		}

	template<typename TResultHandler,
		typename _TxCommand,
		typename C = typename std::decay_t<_TxCommand>,
		typename = std::enable_if_t<std::is_base_of_v<i_command_from_rt1_t, C>&& std::is_final_v<C>>>
	static void invoke_push(std::string && tag,
		std::weak_ptr<distributed_ctx_t> ctx,
		std::weak_ptr<rt1_command_router_t> rt,
		_TxCommand&& cmd,
		TResultHandler && hndl,
		const sndrcv_timeline_timeouted_once & tml) {

		__invoke_push(std::move(tag), std::move(ctx), std::move(rt), std::forward<_TxCommand>(cmd), make_result_handler(std::forward<TResultHandler>(hndl)), tml);
		}

	template<typename TResultHandler,
		typename _TxCommand,
		typename C = typename std::decay_t<_TxCommand>,
		typename = std::enable_if_t<std::is_base_of_v<i_command_from_rt1_t, C>&& std::is_final_v<C>>>
	static void invoke_push(std::string && tag,
		std::weak_ptr<distributed_ctx_t> ctx,
		std::weak_ptr<rt1_command_router_t> rt,
		_TxCommand&& cmd,
		TResultHandler && hndl) {

		__invoke_push(std::move(tag),
			std::move(ctx), std::move(rt), std::forward<_TxCommand>(cmd), make_result_handler(std::forward<TResultHandler>(hndl)));
		}

	void close()noexcept;

#if defined(CBL_MPF_TRACELEVEL_COMMANDS_HOST) && defined(CBL_MPF_OBJECTS_COUNTER_CHECKER_HOST_COMMAND)

	template<typename ... TStringArg>
	static std::string wrap_tag( TStringArg && ... tagArgs ){
		return std::string( std::forward<TStringArg>( tagArgs )... );
		}
#else

	template<typename ... TStringArg>
	static std::string wrap_tag(TStringArg && ... /*tagArgs*/) {
		return std::string();
		}
#endif
	};
}


#if defined(CBL_MPF_TRACELEVEL_COMMANDS_HOST) && defined(CBL_MPF_OBJECTS_COUNTER_CHECKER_HOST_COMMAND)
#define xchannel_cmd_sndrcv_make_tag								xchannel_cmd_sndrcv_t::wrap_tag((__FILE_LINE__))
#define xchannel_cmd_sndrcv_make_tag_msg(msg)						xchannel_cmd_sndrcv_t::wrap_tag((msg))
#else
#define xchannel_cmd_sndrcv_make_tag								(std::string())
#define xchannel_cmd_sndrcv_make_tag_msg(msg)						(std::string())
#endif


