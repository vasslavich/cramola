#include "../../internal.h"
#include "../../global.h"
#include "../../logging/logger.h"
#include "../xcounters_accm.h"


using namespace crm;
using namespace crm::utility;


std::once_flag InitializeInstanceF;
std::shared_ptr<__xcounters_accm_use_t> Instance;

void make_instance()noexcept {
	std::call_once( InitializeInstanceF, [] {
		auto obj = std::make_shared<__xcounters_accm_use_t>();
		std::atomic_exchange( &Instance, obj );

		auto stopf = [] {
			auto sptr( std::atomic_exchange( &Instance, std::shared_ptr<__xcounters_accm_use_t>() ) );
			if( sptr ) {
				trace( (*sptr), trace_detail_level::all );
				}
			};

		subscribe_terminate_list( std::move( stopf ) );
		} );
	}

static std::shared_ptr<__xcounters_accm_use_t> get_instance()noexcept {
	auto obj = std::atomic_load_explicit( &Instance, std::memory_order::memory_order_acquire );
	if( !obj ) {
		make_instance();
		obj = std::atomic_load_explicit( &Instance, std::memory_order::memory_order_acquire );
		}

	return std::move( obj );
	}



__xcounters_entry_t::__xcounters_entry_t( const std::string & key_,
	std::intmax_t counter_,
	std::intmax_t maxval_ )noexcept
	: key( key_ )
	, counter( counter_ )
	, maxval( maxval_ ) {}
	

__xcounters_accm_use_t::__xcounters_accm_use_t()noexcept {}

std::shared_ptr<__xcounters_accm_use_t> __xcounters_accm_use_t::instance()noexcept {
	return get_instance();
	}

#ifdef NOTIFICATION_TRACE_OBJCNT_LIVE
__xcounters_accm_use_t:: seconds_baseval_t __xcounters_accm_use_t::current_timepoint()noexcept {
	return std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now().time_since_epoch()).count();
	}
#endif

void __xcounters_accm_use_t::add( const std::string & key, std::intmax_t value ) noexcept {
	lck_scp_t lck( _lck );

	auto it = _collection.find( key );
	if(it == _collection.cend()) {
		if(!_collection.insert(std::make_pair(key, __xcounters_entry_t(key, value, value))).second) {
			FATAL_ERROR_FWD(nullptr);
			}
		}
	else {
		it->second.counter += value;

		if( it->second.maxval < it->second.counter) {
			it->second.maxval = it->second.counter;
			}

		if( it->second.counter <= 0 && it->second.maxval > 0 ) {
			_collection.erase( it );
			}
		}

#ifdef NOTIFICATION_TRACE_OBJCNT_LIVE
	if((current_timepoint() - _lastTraceTP) > NOTIFICATION_TRACE_OBJCNT_LIVE_INTERVAL_SECS) {
		CHECK_NO_EXCEPTION( __traceout());
		}
#endif
	}

void __xcounters_accm_use_t::set(const std::string & key, std::intmax_t value) noexcept {
	lck_scp_t lck(_lck);

	auto it = _collection.find(key);
	if(it == _collection.cend()) {
		if(!_collection.insert(std::make_pair(key, __xcounters_entry_t(key, value, value))).second) {
			FATAL_ERROR_FWD(nullptr);
			}
		}
	else {
		it->second.counter = value;

		if(it->second.maxval < it->second.counter) {
			it->second.maxval = it->second.counter;
			}

		if(it->second.counter <= 0 && it->second.maxval > 0) {
			_collection.erase(it);
			}
		}

#ifdef NOTIFICATION_TRACE_OBJCNT_LIVE
	if((current_timepoint() - _lastTraceTP) > NOTIFICATION_TRACE_OBJCNT_LIVE_INTERVAL_SECS) {
		CHECK_NO_EXCEPTION(__traceout());
		}
#endif
	}

void __xcounters_accm_use_t::__traceout()noexcept {
	_lastTraceTP = current_timepoint();
	CHECK_NO_EXCEPTION(trace(__collection()));
	}

void __xcounters_accm_use_t::traceout()noexcept {
	lck_scp_t lck(_lck);
	CHECK_NO_EXCEPTION(__traceout());
	}

std::list<__xcounters_entry_t> __xcounters_accm_use_t::collection()const noexcept {
	lck_scp_t lck( _lck );
	return __collection();
	}

std::list<__xcounters_entry_t> __xcounters_accm_use_t::__collection()const noexcept {
	std::list<__xcounters_entry_t> lr;
	for( const auto & ic : _collection ) {
		lr.emplace_back( ic.second );
		}

	return lr;
	}

void crm::utility::trace( std::list<__xcounters_entry_t> && cl, trace_detail_level ) noexcept {
	std::ostringstream os;
	os << "X-CNT stat:";

	for(auto && il : cl) {
		if(il.counter >= CBL_OBJECTS_COUNTER_CHECKER_TRACE_MIN) {
			os
				<< "    count " << std::to_string(std::move(il.counter))
				<< "    maxval " << std::to_string(std::move(il.maxval))
				<< "    key " << std::move(il.key) << std::endl;
			}
		}

	logger_t::logging( os.str(), NOTIFICATION_TAG_TRACE_OBJCNT );
	}

void crm::utility::trace( const __xcounters_accm_base_t & accm, trace_detail_level l )noexcept {
	CHECK_NO_EXCEPTION( trace( accm.collection(), l ) );
	}

