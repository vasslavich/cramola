#pragma once


#include <atomic>
#include <string>
#include <cstdint>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <mutex>
#include "../internal_invariants.h"


namespace crm{
namespace utility {



enum class trace_detail_level {
	none,
	l0,
	l1,
	l2,
	l3,
	l4,
	l5,
	l6,
	l7,
	l8,
	l9,
	all
	};

struct __xcounters_entry_t {
	std::string key;
	std::intmax_t counter = 0;
	std::intmax_t maxval = 0;

	__xcounters_entry_t(const std::string & key,
		std::intmax_t counter,
		std::intmax_t maxval)noexcept;
	};

struct __xcounters_accm_base_t {
	virtual ~__xcounters_accm_base_t() {}

	virtual void add(const std::string & key, std::intmax_t value)noexcept = 0;
	virtual void set(const std::string & key, std::intmax_t value)noexcept = 0;
	virtual std::list<__xcounters_entry_t> collection()const noexcept = 0;
	};

void trace(const __xcounters_accm_base_t & accm, trace_detail_level l = trace_detail_level::l0)noexcept;
void trace(std::list<__xcounters_entry_t> && clst, trace_detail_level l = trace_detail_level::l0)noexcept;

struct __xcounters_accm_use_t : public __xcounters_accm_base_t {
private:
	typedef std::mutex lck_t;
	typedef std::unique_lock<std::mutex> lck_scp_t;

	std::unordered_map<std::string, __xcounters_entry_t> _collection;
	mutable lck_t _lck;

#ifdef NOTIFICATION_TRACE_OBJCNT_LIVE
	using seconds_baseval_t = decltype(std::declval<std::chrono::seconds>().count());
	std::atomic<seconds_baseval_t> _lastTraceTP = 0;
	static seconds_baseval_t current_timepoint()noexcept;
#endif

	std::list<__xcounters_entry_t> __collection()const noexcept;
	void __traceout()noexcept;

public:
	__xcounters_accm_use_t()noexcept;

	static std::shared_ptr<__xcounters_accm_use_t> instance()noexcept;

	void add(const std::string & key, std::intmax_t value)noexcept final;
	void set(const std::string & key, std::intmax_t value)noexcept final;
	std::list<__xcounters_entry_t> collection()const noexcept final;
	void traceout()noexcept;
	};


struct __xcounters_accm_off_t : public __xcounters_accm_base_t {
	static std::shared_ptr<__xcounters_accm_off_t> instance() { return nullptr; }

	void add(const std::string &, std::intmax_t)noexcept final {}
	void set(const std::string &, std::intmax_t)noexcept final {}
	std::list<__xcounters_entry_t> collection()const noexcept final {}

	void traceout()noexcept {}
	};


#ifdef CBL_OBJECTS_COUNTER_CHECKER
using __xcounters_logger_t = __xcounters_accm_use_t;
#else//CBL_OBJECTS_COUNTER_CHECKER
using __xcounters_logger_t = __xcounters_accm_off_t;
#endif
}
}
