#pragma once


#include <atomic>
#include <string>
#include <cstdint>
#include <algorithm>
#include <unordered_map>
#include <mutex>
#include "../internal_invariants.h"
#include "../notifications/internal_terms.h"
#include "./xcounters_accm.h"


namespace crm {
namespace utility {


template<typename T,
	const std::intmax_t stepCheck = CBL_OBJECTS_COUNTER_CHECKER_STEP,
	const std::intmax_t MaxCount = LLONG_MAX,
	const __xcounter_validate_assert_t ValidateDo = __xcounter_validate_assert_t::fatal>
	struct __xobject_count_checker_use_t {
	private:
		using self_t = typename __xobject_count_checker_use_t<T, stepCheck, MaxCount, ValidateDo>;
		static const std::intmax_t validate_value = (std::min)(MaxCount, stepCheck);

		static std::atomic<std::intmax_t> __ObjectsCounter;
		static std::atomic<std::uintmax_t> __ObjectsCountHistory;

		std::string _tag;
		std::intmax_t _thisId{ -1 };
		std::atomic<bool> _isDec{ false };

		void dec_xcounter()noexcept {
			auto iaccm = __xcounters_logger_t::instance();
			if (iaccm) {
				iaccm->add(_tag, -1);
				}
			}

		void inc_xcounter()noexcept {
			auto iaccm = __xcounters_logger_t::instance();
			if (iaccm) {
				iaccm->add(_tag, 1);
				}
			}

		void __ObjectsCounterDec()noexcept {
			auto i = --__ObjectsCounter;
			CBL_VERIFY(i >= 0);

			//if(0 == i || 0 == (i % validate_value)) {

			dec_xcounter();
			//	}
			}

		void ObjectsCounterDec()noexcept {

			bool isDecf = false;
			if(_isDec.compare_exchange_strong(isDecf, true)) {
				__ObjectsCounterDec();
				}
			}

		void __ctr_validate_id()noexcept {
			CBL_VERIFY(_thisId > 0);

			if(_thisId > MaxCount) {
				if(ValidateDo == __xcounter_validate_assert_t::fatal) {
					FATAL_ERROR_FWD(nullptr);
					}
				else {
					FATAL_ERROR_FWD(nullptr);
					}
				}

			//if((_thisId % validate_value) == 0) {

			inc_xcounter();
			//	}
			}

		void reset()noexcept {
			_isDec.store(true, std::memory_order::memory_order_release);
			}

		static std::string make_tag(std::string_view i)noexcept {
			static const std::string _TypeName = typeid(T).name();
			std::string s_{ i };
			s_ += (":" + _TypeName);

			CBL_VERIFY(!s_.empty());

			return s_;
			}

		void ctr_new_object()noexcept {
			_thisId = ++__ObjectsCounter;
			++__ObjectsCountHistory;

			__ctr_validate_id();
			}

	public:
		__xobject_count_checker_use_t(std::string_view i)noexcept
			: _tag(make_tag(i)) {
			ctr_new_object();
			}

		__xobject_count_checker_use_t()noexcept
			: self_t(std::string{}) {}

		__xobject_count_checker_use_t(const self_t & o)noexcept
			: _tag(o._tag) {
			ctr_new_object();
			}

		self_t& operator=(const self_t& o)noexcept {
			if (this != std::addressof(o)) {
				
				dec_xcounter();
				_tag = o._tag;
				inc_xcounter();
				}

			return (*this);
			}

		__xobject_count_checker_use_t(self_t  && o)noexcept
			: _tag(std::move(o._tag))
			, _thisId(o._thisId)
			, _isDec(o._isDec.exchange(true)) {

			o.reset();
			}

		self_t& operator=(self_t && o) noexcept {
			if(this != std::addressof(o)) {
				self_t thisOld(std::move(*this));

				_tag = std::move(o._tag);
				_thisId = o._thisId;
				_isDec.store(o._isDec.exchange(true));

				o.reset();
				}

			return (*this);
			}

		~__xobject_count_checker_use_t() {
			CHECK_NO_EXCEPTION(ObjectsCounterDec());
			}

		void dec() noexcept {
			ObjectsCounterDec();
			}

		std::intmax_t this_counter()const noexcept {
			return _thisId;
			}

		std::intmax_t type_counter()const noexcept {
			return __ObjectsCounter;
			}

		std::uintmax_t type_count_history()const noexcept {
			return __ObjectsCountHistory;
			}

		void traceout()const noexcept {
			auto iaccm = __xcounters_logger_t::instance();
			if(iaccm) {
				CHECK_NO_EXCEPTION(iaccm->traceout());
				}
			}
	};


template<typename T,
	const std::intmax_t stepCheck,
	const std::intmax_t MaxCount,
	const __xcounter_validate_assert_t ValidateDo>
	std::atomic<std::uintmax_t> __xobject_count_checker_use_t<T, stepCheck, MaxCount, ValidateDo>::__ObjectsCountHistory{ 0 };

template<typename T,
	const std::intmax_t stepCheck,
	const std::intmax_t MaxCount,
	const __xcounter_validate_assert_t ValidateDo>
	std::atomic<std::intmax_t> __xobject_count_checker_use_t<T, stepCheck, MaxCount, ValidateDo>::__ObjectsCounter{ 0 };




template<typename T,
	const std::intmax_t stepCheck = CBL_OBJECTS_COUNTER_CHECKER_STEP,
	const std::intmax_t MaxCount = LLONG_MAX,
	const __xcounter_validate_assert_t ValidateDo = __xcounter_validate_assert_t::fatal>
	struct __xobject_count_checker_off_t {
	__xobject_count_checker_off_t() noexcept {}
	__xobject_count_checker_off_t(const std::string & /*incTag*/) noexcept {}
	__xobject_count_checker_off_t(const char * /*incTag*/)noexcept {}

	~__xobject_count_checker_off_t() {}

	void dec() noexcept{}
	void traceout()noexcept {}
	};


#ifdef CBL_OBJECTS_COUNTER_CHECKER

template<typename T,
	const std::intmax_t stepCheck = CBL_OBJECTS_COUNTER_CHECKER_STEP,
	const std::intmax_t MaxCount = LLONG_MAX,
	const __xcounter_validate_assert_t ValidateDo = __xcounter_validate_assert_t::fatal>
	using __xobject_counter_logger_t = __xobject_count_checker_use_t<T, stepCheck, MaxCount, ValidateDo>;
#else


template<typename T,
	const std::intmax_t stepCheck = CBL_OBJECTS_COUNTER_CHECKER_STEP,
	const std::intmax_t MaxCount = LLONG_MAX,
	const __xcounter_validate_assert_t ValidateDo = __xcounter_validate_assert_t::fatal>
	using __xobject_counter_logger_t = __xobject_count_checker_off_t<T, stepCheck, MaxCount, ValidateDo>;
#endif














template<typename Type,
	const std::intmax_t stepCheck = CBL_OBJECTS_COUNTER_CHECKER_STEP,
	const std::intmax_t MaxCount = LLONG_MAX,
	const __xcounter_validate_assert_t ValidateDo = __xcounter_validate_assert_t::fatal>
	struct __xobjects_tablecount_checker_use_t {
	private:
		using type_hash_t = size_t;
		using self_t = typename __xobjects_tablecount_checker_use_t<Type, stepCheck, MaxCount, ValidateDo>;
		static const std::intmax_t validate_value = (std::min)(MaxCount, stepCheck);

		static std::atomic<std::intmax_t> __ObjectsCounter;
		static std::unordered_map<type_hash_t, std::intmax_t> _typeMap;
		static std::mutex _typeMapLck;

		type_hash_t _hash;
		std::intmax_t _thisId{-1};
		std::atomic<bool> _isDec{false};

		void __ObjectsCounterDec()noexcept {
			std::lock_guard<std::mutex> lck(_typeMapLck);
			auto it = _typeMap.find(_hash);
			if (it != _typeMap.end()) {
				if (--it->second < 0) {
					FATAL_ERROR_FWD(nullptr);
					}
				}
			else {
				FATAL_ERROR_FWD(nullptr);
				}
			}

		void ObjectsCounterDec()noexcept {

			bool isDecf = false;
			if (_isDec.compare_exchange_strong(isDecf, true)) {
				__ObjectsCounterDec();
				}
			}

		void __ctr_validate_id()const noexcept {
			CBL_VERIFY(_thisId > 0);

			if (_thisId > MaxCount) {
				if (ValidateDo == __xcounter_validate_assert_t::fatal) {
					FATAL_ERROR_FWD(nullptr);
					}
				else {
					FATAL_ERROR_FWD(nullptr);
					}
				}

			if ((_thisId % validate_value) == 0) {

				auto iaccm = __xcounters_logger_t::instance();
				if (iaccm) {
					iaccm->add(std::to_string(_hash), 1);
					}
				}
			}

		void reset()noexcept {
			_isDec.store(true, std::memory_order::memory_order_release);
			}

	public:
		__xobjects_tablecount_checker_use_t(type_hash_t h)
			: _hash(std::move(h)) {

			if (_hash) {
					{
					std::lock_guard<std::mutex> lock(_typeMapLck);

					auto it = _typeMap.find(_hash);
					if (it == _typeMap.end()) {
						auto insp = _typeMap.insert({_hash, 0});
						if (insp.second) {
							it = insp.first;
							}
						else {
							FATAL_ERROR_FWD(nullptr);
							}
						}

					++(*it).second;
					}

					_thisId = ++__ObjectsCounter;
					__ctr_validate_id();
				}
			else {
				FATAL_ERROR_FWD(nullptr);
				}
			}

		__xobjects_tablecount_checker_use_t(const self_t &) = delete;
		self_t& operator=(const self_t&) = delete;

		__xobjects_tablecount_checker_use_t(self_t  && o)noexcept
			: _hash(std::move(o._hash))
			, _thisId(o._thisId)
			, _isDec(o._isDec.exchange(true)) {

			o.reset();
			}

		self_t& operator=(self_t && o) noexcept {
			if (this != std::addressof(o)) {
				self_t thisOld(std::move(*this));

				_hash = std::move(o._hash);
				_thisId = o._thisId;
				_isDec.store(o._isDec.exchange(true));

				o.reset();
				}

			return (*this);
			}

		~__xobjects_tablecount_checker_use_t() {
			CHECK_NO_EXCEPTION(ObjectsCounterDec());
			}

		void dec() noexcept {
			ObjectsCounterDec();
			}

		std::intmax_t this_counter()const noexcept {
			return _thisId;
			}

		std::intmax_t type_counter()const noexcept {
			return __ObjectsCounter;
			}

		const type_hash_t& key()const noexcept {
			return _hash;
			}

		void traceout()const noexcept {
			auto iaccm = __xcounters_logger_t::instance();
			if (iaccm) {
				CHECK_NO_EXCEPTION(iaccm->traceout());
				}
			}
	};


template<typename T,
	const std::intmax_t stepCheck,
	const std::intmax_t MaxCount,
	const __xcounter_validate_assert_t ValidateDo>
	std::atomic<std::intmax_t> __xobjects_tablecount_checker_use_t<T, stepCheck, MaxCount, ValidateDo>::__ObjectsCounter{0};

template<typename T,
	const std::intmax_t stepCheck,
	const std::intmax_t MaxCount,
	const __xcounter_validate_assert_t ValidateDo>
	std::unordered_map<size_t, std::intmax_t> __xobjects_tablecount_checker_use_t<T, stepCheck, MaxCount, ValidateDo>::_typeMap;


template<typename T,
	const std::intmax_t stepCheck,
	const std::intmax_t MaxCount,
	const __xcounter_validate_assert_t ValidateDo>
	std::mutex __xobjects_tablecount_checker_use_t<T, stepCheck, MaxCount, ValidateDo>::_typeMapLck;






template<typename T,
	const std::intmax_t stepCheck = CBL_OBJECTS_COUNTER_CHECKER_STEP,
	const std::intmax_t MaxCount = LLONG_MAX,
	const __xcounter_validate_assert_t ValidateDo = __xcounter_validate_assert_t::fatal>
	struct __xobjects_table_checker_off_t {
	__xobjects_table_checker_off_t() {}
	__xobjects_table_checker_off_t(const std::string & /*incTag*/) {}
	__xobjects_table_checker_off_t(const char * /*incTag*/) {}

	~__xobjects_table_checker_off_t() {}

	void dec() noexcept {}
	void traceout()noexcept {}
	};



#ifdef CBL_OBJECTS_COUNTER_CHECKER



template<typename T,
	const std::intmax_t stepCheck = CBL_OBJECTS_COUNTER_CHECKER_STEP,
	const std::intmax_t MaxCount = LLONG_MAX,
	const __xcounter_validate_assert_t ValidateDo = __xcounter_validate_assert_t::fatal>
	using __xobjects_tablecount_checker_logger_t = __xobjects_tablecount_checker_use_t<T, stepCheck, MaxCount, ValidateDo>;
#else

template<typename T,
	const std::intmax_t stepCheck = CBL_OBJECTS_COUNTER_CHECKER_STEP,
	const std::intmax_t MaxCount = LLONG_MAX,
	const __xcounter_validate_assert_t ValidateDo = __xcounter_validate_assert_t::fatal>
	using __xobjects_tablecount_checker_logger_t = __xobjects_table_checker_off_t<T, stepCheck, MaxCount, ValidateDo>;
#endif

}
}
