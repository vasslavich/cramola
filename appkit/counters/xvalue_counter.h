#pragma once


#include <atomic>
#include <string>
#include <cstdint>
#include <algorithm>
#include "../internal_invariants.h"
#include "../notifications/internal_terms.h"
#include "./xcounters_accm.h"


namespace crm{
namespace utility{


template<const size_t stepCheck = CBL_OBJECTS_COUNTER_CHECKER_STEP,
	const size_t MaxCount = LLONG_MAX,
	const __xcounter_validate_assert_t ValidateDo = __xcounter_validate_assert_t::fatal>
struct __xvalue_count_checker_use_t {
private:
	static const size_t validate_value_inc = (std::min)( MaxCount, stepCheck );
	static const size_t validate_value_dec = 2;

	size_t _last = 0;
	std::string _name;

public:
	void CounterCheck( size_t accval)noexcept{

		if( (_last == 0 || accval == 0)
			|| ((accval <= _last) && (_last - accval) >= validate_value_dec)
			|| ((accval > _last) && (accval - _last) >= validate_value_inc)
			) {

			_last = accval;

			auto iaccm = __xcounters_logger_t::instance();
			if( iaccm ) {
				iaccm->set( _name, accval );
				}

			if( accval > MaxCount ) {
				if( ValidateDo == __xcounter_validate_assert_t::fatal ) {
					FATAL_ERROR_FWD(nullptr);
					}
				}
			}
		}

public:
	__xvalue_count_checker_use_t()noexcept {}

	template<typename TStringIniVal>
	__xvalue_count_checker_use_t( TStringIniVal && valueName )noexcept
		: _name(std::to_string((uintptr_t)this) + ":" ){

		_name += std::forward<TStringIniVal>( valueName );
		}

	__xvalue_count_checker_use_t( const __xvalue_count_checker_use_t & ) = delete;
	__xvalue_count_checker_use_t& operator=(const __xvalue_count_checker_use_t&) = delete;

	__xvalue_count_checker_use_t( __xvalue_count_checker_use_t  && o )noexcept
		: _name(std::move(o._name))
		, _last( o._last ){}

	__xvalue_count_checker_use_t& operator=(__xvalue_count_checker_use_t && o)noexcept {
		if(this != std::addressof(o)) {
			_name = std::move(o._name);
			_last = o._last;
			}

		return (*this);
		}
	};


template<const std::intmax_t stepCheck = CBL_OBJECTS_COUNTER_CHECKER_STEP,
	const std::intmax_t MaxCount = LLONG_MAX,
	const __xcounter_validate_assert_t ValidateDo = __xcounter_validate_assert_t::fatal>
struct __xvalue_count_checker_off_t {
	__xvalue_count_checker_off_t()noexcept {}
	__xvalue_count_checker_off_t( const std::string & /*tag*/ ) noexcept {}

	~__xvalue_count_checker_off_t() {}

	void CounterCheck( size_t value )noexcept{}
	};


#ifdef CBL_OBJECTS_COUNTER_CHECKER

template<const std::intmax_t stepCheck = CBL_OBJECTS_COUNTER_CHECKER_STEP,
	const std::intmax_t MaxCount = LLONG_MAX,
	const __xcounter_validate_assert_t ValidateDo = __xcounter_validate_assert_t::fatal>
	using __xvalue_counter_logger_t = __xvalue_count_checker_use_t<stepCheck, MaxCount, ValidateDo>;
#else//CBL_OBJECTS_COUNTER_CHECKER


template<const std::intmax_t stepCheck = CBL_OBJECTS_COUNTER_CHECKER_STEP,
	const std::intmax_t MaxCount = LLONG_MAX,
	const __xcounter_validate_assert_t ValidateDo = __xcounter_validate_assert_t::fatal>
	using __xvalue_counter_logger_t = __xvalue_count_checker_off_t<stepCheck, MaxCount, ValidateDo>;
#endif
}
}

