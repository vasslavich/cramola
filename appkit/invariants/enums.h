#pragma once


#include <type_traits>
#include <optional>
#include <string_view>
#include "./constants.h"
#include "../build_configure/build_typeids.h"


namespace crm {

enum class cvt_err_t {
	success,
	read_str_hex_pair,
	bad_format
	};

enum class errem_generator_mode {
	none,
	inc,
	rand
	};

namespace srlz {

enum register_type_result {
	success = 0,
	exists,
	error
	};
const char* cvt2string(register_type_result rt)noexcept;

enum class fixed_array_type {
	fixed_array_not,
	fixed_array_std,
	fixed_array_c
	};

enum class serialize_properties_length {
	not_supported,
	as_maxsize,
	as_fixedsize,
	as_variable
	};

enum class srlz_prefix_type_t {
	lower_bound,
	undefined,
	prefixed,
	eof,
	upper_bound
	};

namespace detail {

enum class iterator_tag {
	input_iterator_tag,
	output_iterator_tag,
	forward_iterator_tag,
	bidirectional_iterator_tag,
	random_access_iterator_tag,
	contiguous_iterator_tag
	};

enum class rstream_base_type {
	actor_0,
	actor_1
	};


using fixed_array_not = std::integral_constant<char, (char)fixed_array_type::fixed_array_not>;
using fixed_array_std = std::integral_constant<char, (char)fixed_array_type::fixed_array_std>;
using fixed_array_c = std::integral_constant<char, (char)fixed_array_type::fixed_array_c>;


}
}


enum class launch_as {
	async,
	now
	};

enum class peer_command_t {
	ping
	};

enum class event_type_t{
	lower_bound = -1,
	st_undefined = 0,
	st_disconnection,
	st_connected,
	upper_bound
	};

enum class xchannel_type_t{
	undefined,
	shared_objects_queue,
	copy_objects_queue,
	ipc_shmem
	};

enum class async_space_t{
	undef,
	sys,
	ext
	};


namespace detail {


enum class invoke_context_trait {
	can_be_used_as_async,
	sync
	};

enum class async_sendrecv_state_t{
	success,
	invalid,
	remote_closed,
	exception,
	undefined
	};

enum class register_slot_group_t{
	messages,
	events,
	message_events
	};


/*! ���� ��������� �� ������ */
enum class rtx_exception_code_t{
	lower_bound,
	undefined,
	not_uop,
	rtx_glid_already_exists,
	upper_bound
	};

enum class rt1_register_result_t{
	success,
	already_exists,
	fatal,
	canceled
	};

enum class xpeer_direction_t{
	undefined, in, out
	};

enum class rt1_object_close_mode_t{
	undefined,
	router_initial,
	object_initial
	};

enum class assign_options{
	overwrite_if_exits,
	insert_if_not_exists
	};

enum class stream_close_reason_t {
	undefined,
	io_closed,
	remote_object_eof,
	remote_object_disconnection,
	on_destination_side,
	exception
	};

enum class unregister_message_track_reason_t {
	lower_bound,
	undefined,
	closed_by_self/* ������ ��������(! �� ����������) */,
	closed_by_self_from_remoted,
	closed_by_rtsys,
	not_founded/* ������� ��������(! �� ����������) */,
	upper_bound
	};

enum class rtx_command_type_t {
	lower_bound,
	undefined,
	reg_link_rdm,
	unreg_link_rdm,
	unreg_link_source_key,
	out_connection_req,
	out_connection_stated_ready,
	out_connection_closed,
	in_connectoin_closed,
	exception,
	invalid_xpeer,
	sndrcv_result,
	unbind_connection,
	sndrcv_fault,
	xchannel_rt1_initial,
	upper_bound
	};

const char* cvt2string(rtx_command_type_t type)noexcept;

/*! ��� ��������� */
using iol_types_ibase_t = uint64_t;
enum class iol_types_t {
	st_undefined = 0,

	st_RZi_LX_error,
	st_Internal,

	st_disconnection,

	st_notification,
	st_packet,
	st_binary_serialized,
	st_ping,
	st_short,
	st_request,
	st_identity,
	st_connection,


	st_error_proun,

	st_sendrecv_datagram,
	st_command,


	st_strob_envelop,
	st_functor_package,

	st_stream_header,
	st_stream_header_response,
	st_stream_block_value,
	st_stream_block_request,
	st_stream_end,
	st_stream_disconnection,


	st_user_boundary = 0xFFFF
	};




/*! ��� ����� */
enum class iol_length_spcf_t {
	zeroed = 0,
	prefixed,
	eof_marked_
	};


const char * cvt2string(iol_types_t type)noexcept;
const char * cvt2string(iol_length_spcf_t l)noexcept;

/*! ��� ������� ������������� */
enum class rtl_table_t {
	lower_bound,
	undefined,
	out,
	in,
	rt1_router,
	rt1_sndrcv_command,
	upper_bound
	};

/*! ��������� ����������� ���� ������� ������������� */
const char * cvt2string(rtl_table_t t)noexcept;

enum class rtl_level_t {
	lower_bound = -1,
	undefined = 0,
	l0 = 0x00000040,
	l1 = 0x00000080,
	upper_bound
	};

const char* cvt2string(rtl_level_t l)noexcept;

bool is_null(rtl_level_t l)noexcept;
bool is_null(rtl_table_t l)noexcept;




/*! ��������� �������� � ��������� /ref iol_type_spcf_t*/
enum class iol_type_attributes_t {
	type,
	code,
	report,
	length,
	type_id,
	timeline,
	reserved,
	crc,
	__count
	};

enum class message_stack_push_options_t {
	no_pull,
	pull_out_if_overwait
	};


enum class route_level_t {
	undefined,
	rt0,
	rt1
	};

enum class closing_reason_t {
	extern_force,
	break_connection,
	connection_procedure_timeouted,
	ping_timeout,
	rw_exception,
	test_error,
	destroy
	};

const char* cvt2string(closing_reason_t value)noexcept;
}


enum class subscribe_invoke_scope_t {
	undefined,
	ready,
	abandoned
	};

enum class unique_collection_result_t {
	success,
	not_found,
	exists
	};

/*! ������� ������ ������!!! */
enum class async_operation_result_t {
	st_init, /*!< ������ � ��������� ���������� � ������ ��������� */
	st_waiting, /*!< ����������� ��������� ����������� �������� */
	st_ready, /*!< ������� ��������� ���������� ����������� �������� */
	st_exception, /*!< ������ */
	st_timeouted, /*!< �������� ��������� �� �������� */
	st_canceled, /*!< ����� �� �������� �������� ���� ���������� */
	st_abandoned /*!< ������ ����� */
	};


enum class invoke_result_t {
	st_ok,
	st_already_invoked,
	st_null_handler,
	st_canceled
	};

const char* cvt2string(async_operation_result_t)noexcept;
std::optional<async_operation_result_t> cvtfrom(std::string_view s, async_operation_result_t)noexcept;

enum class dcf_error_t {
	SUCCESS = 0,																///< �������� ���������� ��������
	BASE,																		///< �������� ������������� �������� ����� ������ \ref dcf_error_t
	OBJECT_ALREADY_LOADED,														///< ������ ��� ��������
	OBJECT_ALREADY_CREATED,														///< ������ ��� ������
	OBJECT_ALREADY_INITIALIZED,													///< ������ ��� ������������������
	OBJECT_ALREADY_DESTROYED,													///< ������ ��� ��������
	OBJECT_ALREADY_UNLOADED,													///< ������ ��� ��������
	OBJECT_NOT_FOUND,															///< ������ �� ������
	WORKFLOW_VIOLATION,															///< ��������� ������� ��������
	REFERENCES_VIOLATION,														///< ��������� ����������� �������� ������
	UNDEF,																		///< ������������� ������

	PLUGIN_EXIT_CODE_BASE = (SUCCESS + DCF_ERROR_CODE_CLASS_RANGE),				///< �������� ����� ������ ������ [��� �������� ������� �������], \ref crm::addon::plugin_exit_syscode_t
	};


namespace utility {


enum class __xcounter_validate_assert_t {
	fatal,
	ethrow
	};
}

namespace detail {


template<typename TEnum,
	typename = std::void_t<
		std::enable_if_t<std::is_enum_v<TEnum>>,
		decltype((int)TEnum::success == 0),
		decltype((int)TEnum::exception > 0)
	>>
struct [[nodiscard]] enum_nodiscard_value{
	using enum_type = typename TEnum;
	using self_type = typename enum_nodiscard_value<TEnum>;

	enum_type code{ enum_type::exception };
	
	operator bool()const noexcept {
		return code == enum_type::success;
		}

	enum_nodiscard_value()noexcept {}

	enum_nodiscard_value(enum_type c)noexcept
		: code(c) {}

	self_type& operator=(enum_type c)noexcept {
		code = c;
		return (*this);
		}

	friend bool operator==(const self_type& r, enum_type l)noexcept {
		return r.code == l;
		}

	friend bool operator!=(const self_type& r, enum_type l)noexcept {
		return !(r == l);
		}

	friend bool operator==(enum_type r, const self_type & l)noexcept {
		return l.code == r;
		}

	friend bool operator!=(enum_type r, const self_type & l)noexcept {
		return !(r == l);
		}
	};

enum class __bind_link_result {
	success = 0,
	link_unregistered,
	exception,
	other
	};

using addon_push_event_result_t = enum_nodiscard_value< __bind_link_result>;

//struct [[nodiscard]] addon_push_event_result_t{
//	enum class code_t {
//	success = 0,
//	link_unregistered,
//	exception,
//	other
//	};
//
//code_t code{ code_t::exception };
//
//operator bool()const noexcept;
//
//addon_push_event_result_t()noexcept;
//addon_push_event_result_t(code_t c)noexcept;
//
//
//addon_push_event_result_t& operator=(addon_push_event_result_t::code_t c)noexcept;
//	};
//
//bool operator==(const addon_push_event_result_t& r, addon_push_event_result_t::code_t l)noexcept;
//bool operator!=(const addon_push_event_result_t& r, addon_push_event_result_t::code_t l)noexcept;
}

using peer_direction_t = detail::rtl_table_t;


enum class __subscribe_result {
	success,
	exception
	};

using subscribe_result = detail::enum_nodiscard_value<__subscribe_result>;

enum class subscribe_invoke {
	by_subscribion,
	without_subscribtion,
	with_exception
	};

namespace detail {

enum class __subscribe_invoker_result {
	success,
	err_exists,
	exception
	};
using subscribe_invoker_result = enum_nodiscard_value<__subscribe_invoker_result>;


enum class __peer_commands_result_t {
	success,
	not_supported,
	exception
	};
using peer_commands_result_t = enum_nodiscard_value<__peer_commands_result_t>;




enum class reconnect_setup_t {
	yes,
	no
	};

enum class connection_stage_t {
	impersonate,
	initialize
	};


enum class inovked_as {
	async,
	sync
	};
}


enum class remote_object_state_t {
	oxcn_maked,
	oxcn_launch,
	null,
	init_stage,
	connect_stage,
	connect_stage_ready,
	identity_stage,
	identity_stage_ready,
	initialize_stage,
	initialize_stage_ready,
	impersonated,
	closed
	};





const char* cvt2string(remote_object_state_t s)noexcept;



/*!
���� ���������� ������� �������
*/
enum class plugin_exit_syscode_t {

	PE_SUCCESS = 0,																///< �������� ���������� ��������
	PE_BASE = (unsigned int)dcf_error_t::PLUGIN_EXIT_CODE_BASE,					///< ������� �������� ��� ����� \ref plugin_exit_syscode_t

	PE_NOT_FOUND,																///< ������ �� ������
	PE_MEM_OUT,																	///< ������������ ������ ��� ���������� ��������
	PE_RE_ACTION,																///< ������� ���������� ���������� ��������
	PE_RE_INITIALIZATION,														///< ������� ��������� �������������
	PE_HOST_SERVICES_UNDEFINED,													///< ������������ ������� �����
	PE_BAD_PTR,																	///< ���������� ���������
	PE_FWDORKFLOW_VIOLATION,														///< ��������� ������� ������ �������
	PE_NOT_IMPLEMENTED,															///< ������������� �������� ��� ������� �� �����������
	PE_BAD_SERIALIZE,															///< ������ ������������
	PE_BAD_DESERIALIZE,															///< ������ ��������������
	PE_INTERNAL,																///< ���������� ������ �������
	PE_EXTERN_PROCESS_START_FAIL,												///< ������ ��� ������� �������� ��������
	PE_EXTENSION_MODULE_FAIL,													///< ������ � ������������ ������ ����������

	PE_ARG_DUBLICATION,															///< ����������� �������� ������ � ���� �� ���������
	PE_ARG_DTOUT_NULL_PTR,														///< ������� ��������� �� �������� ���������
	PE_ARG_PLUGIN_INIT_NOT_FOUND,												///< �� ������ �������� ������������� �������
	PE_ARG_FAIL_OPERATION,														///< ������ ���������� �������� �� ������ ���������

	PE_DATAGRAM_NULL,															///< ������� ���������
	PE_DATAGRAM_BLOCK_INVALID,													///< ������ ��������� ����� ���������
	PE_DATAGRAM_BAD_PROCESSING,													///< ������ �������� ���������
	PE_DATAGRAM_AMBIGUITY,														///< ������������� �������

	PE_UNDEF,																	///< ������������� ������
	PE_SYSTEM_UPPER_BOUNDARY = (PE_BASE + DCF_ERROR_CODE_CLASS_RANGE),			///< ��������� ��������
	PE_USER_LOWER_BOUNDARY														///< ������ ������� ��� ������������ ������ ������������� �������
	};


/*!
��������� ���������� ��������� �� ��������� ������� ���� \ref plugin_exit_syscode_t
\param [in] code ������������� �������� ���� �������� ������ �������.
\return 1) ������ ��������� �� ������, ���� ��������������� ��������� ��� ������,
2) - �������� nullptr.
*/
const wchar_t *plugin_exit_syscode_message(const unsigned int code);

/*!
��������� ���������� ��������� �� ��������� ������� ���� \ref plugin_exit_syscode_t
\param [in] code ����������������� �������� ���� �������� ������ �������.
\return 1) ������ ��������� �� ������, ���� ��������������� ��������� ��� ������,
2) - �������� nullptr.
*/
const wchar_t *plugin_exit_syscode_message(const plugin_exit_syscode_t &code);
}
