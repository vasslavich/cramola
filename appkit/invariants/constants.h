#pragma once


#include <chrono>
#include "../build_configure/build_typeids.h"


namespace crm {

constexpr size_t serialized_prefix_payload_bits = 1;

constexpr max_serialized_length_type srlz_prefix_length_undefined = (max_serialized_length_type)(-1);


/*! ����� ��������� ������������� �������� ����� �������� */
constexpr size_t DCF_DATAGRAM_TYPE_CODE_CLASS_RANGE = 0x0000FFFF;

/*! ����� ��������� ������������� �������� ����� ������ �������� */
constexpr size_t DCF_DATAGRAM_ENTITY_CODE_CLASS_RANGE = 0x0000FFFF;

/*! ����� ��������� ������������� �������� ����� ������ */
constexpr size_t DCF_ERROR_CODE_CLASS_RANGE = 0x0000FFFF;

constexpr size_t  CBL_CORE_SRLZ_BLOB_MAX_SIZE = (1024 * 1024 * 512);

constexpr std::chrono::microseconds CBL_TIMEOUT_IO_CHANNELS = std::chrono::seconds(10);
}
