#include "../../internal.h"
#include "../../logging/logger.h"
#include "../enums.h"


using namespace crm;
using namespace crm::detail;


//
//addon_push_event_result_t::operator bool()const noexcept {
//	return code == code_t::success;
//	}
//
//addon_push_event_result_t::addon_push_event_result_t()noexcept {}
//
//addon_push_event_result_t::addon_push_event_result_t(code_t c)noexcept
//	: code(c) {}
//
//
//addon_push_event_result_t& addon_push_event_result_t::operator=(addon_push_event_result_t::code_t c)noexcept {
//	code = c;
//	return (*this);
//	}
//
//bool crm::detail:: operator==(const addon_push_event_result_t& r, addon_push_event_result_t::code_t l)noexcept {
//		return r.code == l;
//		}
//
//	bool crm::detail::operator!=(const addon_push_event_result_t& r, addon_push_event_result_t::code_t l)noexcept {
//		return !(r == l);
//		}




typedef struct plugin_exit_code_i {
	plugin_exit_syscode_t code;
	const wchar_t *msg;
	}plugin_exit_code_i;

#if (DCF_ERR_LANG == DCF_ERR_LANG_RUS)

static plugin_exit_code_i errorsDescriptions[] = {
	{plugin_exit_syscode_t::PE_SUCCESS, L"�������� ���������� ��������"},
	{plugin_exit_syscode_t::PE_NOT_FOUND, L"������ �� ������"},
	{plugin_exit_syscode_t::PE_MEM_OUT, L"������������ ������ ��� ���������� ��������"},
	{plugin_exit_syscode_t::PE_RE_ACTION, L"������� ���������� ���������� ��������"},
	{plugin_exit_syscode_t::PE_BAD_PTR, L"���������� ���������"},
	{plugin_exit_syscode_t::PE_FWDORKFLOW_VIOLATION, L"��������� ������� ������ �������"},

	{plugin_exit_syscode_t::PE_ARG_DUBLICATION, L"����������� �������� ������ � ���� �� ���������"},
	{plugin_exit_syscode_t::PE_ARG_DTOUT_NULL_PTR, L"������� ��������� �� �������� ���������"},
	{plugin_exit_syscode_t::PE_ARG_PLUGIN_INIT_NOT_FOUND, L"�� ������ �������� ������������� �������"},

	{plugin_exit_syscode_t::PE_ARG_DEVICE_ID_INVALID, L"������������ ������������� ������ ����������"},
	{plugin_exit_syscode_t::PE_ARG_DEVICE_DONT_INITILIZED, L"���������� �� ����������������"},
	{plugin_exit_syscode_t::PE_ARG_DEVICE_INVALID_STATE, L"���������� ��������� � ����������������� ��������� "},

	{plugin_exit_syscode_t::PE_DATAGRAM_NULL, L"������� ���������"},
	{plugin_exit_syscode_t::PE_DATAGRAM_BLOCK_INVALID, L"������ ��������� ����� ���������"},
	{plugin_exit_syscode_t::PE_DATAGRAM_BAD_PROCESSING, L"������ �������� ���������"},

	{plugin_exit_syscode_t::PE_UNDEF, L"������������� ������"},
	{plugin_exit_syscode_t::PE_SYSTEM_UPPER_BOUNDARY, L"��������� ��������"},
	};

#else//DCF_ERR_LANG == DCF_ERR_LANG_ENG

static plugin_exit_code_i errorsDescriptions[] = {
	{plugin_exit_syscode_t::PE_SUCCESS, L"success"},
	{plugin_exit_syscode_t::PE_NOT_FOUND, L"the object not found"},
	{plugin_exit_syscode_t::PE_MEM_OUT, L"a memory is insufficiently for the operation"},
	{plugin_exit_syscode_t::PE_RE_ACTION, L"the attempt run the operation repedeatly"},
	{plugin_exit_syscode_t::PE_BAD_PTR, L"the invalid pointer"},
	{plugin_exit_syscode_t::PE_FWDORKFLOW_VIOLATION, L"a violation of a call order of methods"},

	{plugin_exit_syscode_t::PE_ARG_DUBLICATION, L"the same argument already was passed"},
	{plugin_exit_syscode_t::PE_ARG_DTOUT_NULL_PTR, L"the pointer of the output datagram is null"},
	{plugin_exit_syscode_t::PE_ARG_PLUGIN_INIT_NOT_FOUND, L"the parametr of initialization of the plugin not found"},

	{plugin_exit_syscode_t::PE_DATAGRAM_NULL, L"the empty datagram"},
	{plugin_exit_syscode_t::PE_DATAGRAM_BLOCK_INVALID, L"the error by cause of invalid the datagram block"},
	{plugin_exit_syscode_t::PE_DATAGRAM_BAD_PROCESSING, L"the error by cause of datagram processing stage"},

	{plugin_exit_syscode_t::PE_UNDEF, L"the undefined error"},
	{plugin_exit_syscode_t::PE_SYSTEM_UPPER_BOUNDARY, L"system field"},
	};

#endif//DCF_ERR_LANG


const wchar_t *crm::plugin_exit_syscode_message( const unsigned int code ) {
	size_t index = 0;
	while( errorsDescriptions[index].code != plugin_exit_syscode_t::PE_UNDEF ) {
		if( (unsigned int)errorsDescriptions[index].code == code )return errorsDescriptions[index].msg;

		++index;
		}

	return L"";
	}

const wchar_t *crm::plugin_exit_syscode_message( const plugin_exit_syscode_t &code ) {
	return plugin_exit_syscode_message( (unsigned int)code );
	}

