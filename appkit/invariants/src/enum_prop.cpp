#include "../../internal.h"
#include "../enums.h"

using namespace crm;
using namespace crm::detail;


bool crm::detail::is_null( rtl_level_t l )noexcept{
	return l == rtl_level_t::undefined;
	}

bool crm::detail::is_null( rtl_table_t l )noexcept{
	return l == rtl_table_t::undefined;
	}
