#include "../../internal.h"
#include "../enums.h"


using namespace crm;
using namespace crm::detail;


const char* crm::detail::cvt2string(rtl_level_t l)noexcept {
	switch(l) {
		case rtl_level_t::l0:
			return "l0";
		case rtl_level_t::l1:
			return "l1";
		case rtl_level_t::undefined:
			return "undefined";

		default:
			FATAL_ERROR_FWD(nullptr);
		}
	}

const char* crm::cvt2string(async_operation_result_t value)noexcept {
	switch(value) {
		case async_operation_result_t::st_init:
			return "init";
		case async_operation_result_t::st_waiting:
			return "waiting";
		case async_operation_result_t::st_ready:
			return "ready";
		case async_operation_result_t::st_exception:
			return "exception";
		case async_operation_result_t::st_timeouted:
			return "timeouted";
		case async_operation_result_t::st_canceled:
			return "canceled";
		case async_operation_result_t::st_abandoned:
			return "abandoned";
		default:
			FATAL_ERROR_FWD(nullptr);
		}
	}

std::optional<async_operation_result_t> cvtfrom(std::string_view s, async_operation_result_t)noexcept {
	if(s == "init") {
		return std::make_optional(async_operation_result_t::st_init);
		}
	else if(s == "waiting") {
		return std::make_optional(async_operation_result_t::st_waiting);
		}
	else if(s == "ready") {
		return std::make_optional(async_operation_result_t::st_ready);
		}
	else if(s == "exception") {
		return std::make_optional(async_operation_result_t::st_exception);
		}
	else if(s == "timeouted") {
		return std::make_optional(async_operation_result_t::st_timeouted);
		}
	else if(s == "canceled") {
		return std::make_optional(async_operation_result_t::st_canceled);
		}
	else if(s == "abandoned") {
		return std::make_optional(async_operation_result_t::st_abandoned);
		}
	else
		return std::nullopt;
	}


const char* crm::detail::cvt2string(rtx_command_type_t type)noexcept {
	switch(type) {
		case rtx_command_type_t::reg_link_rdm:
			return "subscribe_rdm";
		case rtx_command_type_t::unreg_link_rdm:
			return "unsubsribe_rdm";
		case rtx_command_type_t::unreg_link_source_key:
			return "unsubsribe_rt0_link";
		case rtx_command_type_t::out_connection_req:
			return "outcoming connection request";
		case rtx_command_type_t::out_connection_stated_ready:
			return "outcoming connection stated";
		case rtx_command_type_t::exception:
			return "exception";
		case rtx_command_type_t::out_connection_closed:
			return "outcoming connection closed";
		case rtx_command_type_t::invalid_xpeer:
			return "invalid xpeer";
		case rtx_command_type_t::in_connectoin_closed:
			return "incoming connection closed";
		case rtx_command_type_t::unbind_connection:
			return "unbind connection";
		case rtx_command_type_t::sndrcv_fault:
			return "sndrcv fault";
		case rtx_command_type_t::xchannel_rt1_initial:
			return "rt1 channel initial";
		default:
			return "undefined";
		}
	}


const char* crm::detail::cvt2string( closing_reason_t value )noexcept{
	switch( value ){
		case closing_reason_t::extern_force: return "extern forcing";
		case closing_reason_t::break_connection: return "connection broken";
		case closing_reason_t::connection_procedure_timeouted: return "connection procedure timeouted";
		case closing_reason_t::ping_timeout:return "ping timeouted";
		case closing_reason_t::rw_exception:return "rw exception";
		case closing_reason_t::test_error:return "test_error";
		case closing_reason_t::destroy: return "destroy";
		default:
			FATAL_ERROR_FWD(nullptr);
		}
	}


const char * crm::detail::cvt2string( iol_types_t type ) noexcept{
	switch( type ){
		case iol_types_t::st_short:
			return "short";

		case iol_types_t::st_ping:
			return "ping";

		case iol_types_t::st_identity:
			return "identity";

		case iol_types_t::st_command:
			return "command";

		case iol_types_t::st_request:
			return "request identity";

		case iol_types_t::st_packet:
			return "packet";

		case iol_types_t::st_binary_serialized:
			return "binary serialized blob";

		case iol_types_t::st_connection:
			return "connection";

		case iol_types_t::st_user_boundary:
			return "user type";

		case iol_types_t::st_disconnection:
			return "disconnection";

		case iol_types_t::st_sendrecv_datagram:
			return "sendrecv_datagram";

		case iol_types_t::st_notification:
			return "notification";

		case iol_types_t::st_stream_header:
			return "stream header";

		case iol_types_t::st_stream_header_response:
			return "stream header response";

		case iol_types_t::st_stream_block_value:
			return "stream block value";

		case iol_types_t::st_stream_block_request:
			return "stream block request";

		case iol_types_t::st_stream_end:
			return "stream end";

		case iol_types_t::st_stream_disconnection:
			return "stream disconnection";

		case iol_types_t::st_strob_envelop:
			return "strob envelop";

		case iol_types_t::st_RZi_LX_error:
			return "RZi_LX_error";

		default:
			FATAL_ERROR_FWD(nullptr);
		}
	}

const char * crm::detail::cvt2string( iol_length_spcf_t l )noexcept{
	switch( l ){
		case iol_length_spcf_t::zeroed:
			return "zeroed";

		case iol_length_spcf_t::prefixed:
			return "prefixed";

		case iol_length_spcf_t::eof_marked_:
			return "eof marked";

		default:
			FATAL_ERROR_FWD(nullptr);
		}
	}


const char * crm::detail::cvt2string( rtl_table_t t ) noexcept{
	switch( t ){
		case rtl_table_t::undefined:
			return "undefined";
		case rtl_table_t::out:
			return "out";
		case rtl_table_t::in:
			return "in";
		case rtl_table_t::rt1_router:
			return "rt1-router";
		default:
			FATAL_ERROR_FWD(nullptr);
		}
	}


const char* crm::srlz::cvt2string(register_type_result rt)noexcept {
	switch (rt)
		{
		case crm::srlz::success:
			return "register_type_result::success";
		case crm::srlz::exists:
			return "register_type_result::exists";
		case crm::srlz::error:
			return "register_type_result::error";
		default:
			return "undefined";
		}
	}

const char* crm::cvt2string( remote_object_state_t s )noexcept{
	switch( s ){
		case remote_object_state_t::oxcn_maked:
			return "oxcn_maked";
		case remote_object_state_t::oxcn_launch:
			return "oxcn_launch";
		case remote_object_state_t::null:
			return "null";
		case remote_object_state_t::init_stage:
			return "init_stage";
		case remote_object_state_t::connect_stage:
			return "connect_stage";
		case remote_object_state_t::connect_stage_ready:
			return "connect_stage_ready";
		case remote_object_state_t::identity_stage:
			return "identity_stage";
		case remote_object_state_t::identity_stage_ready:
			return "identity_stage_ready";
		case remote_object_state_t::initialize_stage:
			return "initialize_stage";
		case remote_object_state_t::initialize_stage_ready:
			return "initialize_stage_ready";
		case remote_object_state_t::impersonated:
			return "impersonated";
		case remote_object_state_t::closed:
			return "closed";
		default:
			FATAL_ERROR_FWD(nullptr);
		}
	}

