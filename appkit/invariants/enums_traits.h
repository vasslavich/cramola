#pragma once

#include "./enums.h"
#include "../utilities/hash/predecl.h"


namespace crm{
namespace detail{

template<typename TEnum,
	typename std::enable_if_t<std::is_enum_v<TEnum>, int> = 0>
void apply_at_hash(address_hash_t& to, TEnum value)noexcept {
	using underlying_type = std::underlying_type_t<TEnum>;
	to.at< underlying_type>(0) ^= (underlying_type)value;
	}
}
}

