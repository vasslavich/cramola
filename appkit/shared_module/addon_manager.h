#pragma once


#include <string>
#include <sstream>
#include <vector>
#include <mutex>
#include <memory>
#include <map>
#include "./addon_anchor.h"
#include "./hstsrv.h"


namespace crm {
namespace addon{


/*!
����� ��������� ������� ���������� ���������� ��������.
��������� ������� �������� �� ������ � ���������.
������������:
- ������������� ��������� ������� ��� ������ �������� �������.,
- ���������� ��������� ������ �������,
- ���������� ��������� ����� ����������� �������.
*/
class addon_manager_t {
public:
	typedef addon_manager_t self_t;

private:
	typedef std::mutex lock_t;
	typedef std::unique_lock<lock_t> mutex_scope_lock_t;
	typedef std::map<addon_identifier_t, std::weak_ptr<detail::module_anchor_t> > addons_cart_t;

	/* ������������� ������ � ��������, ���������� ������������ ������������� ������ �
	�������� ���������� ������������� �������� */
	mutable lock_t _lock;
	/* ������������� ����� */
	addon_identifier_t _managerId;
	/* ����� ����������� ������� */
	addons_cart_t _map;
	/* ����� ����������� ������� ����� */
	std::shared_ptr<i_host_services_t> _hservices;
	/* ���� */
	std::shared_ptr<i_types_driver_t> _tpdrv;

protected:
	template<typename XServices,
		typename XTypeDriver,
		typename std::enable_if< (std::is_base_of<i_host_services_t, XServices>::value
		&& std::is_base_of<crm::i_types_driver_t, XTypeDriver>::value), int>::type = 0>
	addon_manager_t( typename std::shared_ptr<XServices> srv,
		typename std::shared_ptr<XTypeDriver> tpDrv ){
		
		set_services( srv, tpDrv );
		}

	addon_manager_t();

	addon_manager_t( const addon_manager_t & ) = delete;
	addon_manager_t( addon_manager_t  && ) = delete;

	addon_manager_t& operator=(const addon_manager_t &) = delete;
	addon_manager_t& operator=(addon_manager_t &&) = delete;

private:
	void _clear()noexcept;
	void _close()noexcept;

	addon_identifier_t _create_unique_addon_id()const noexcept;

public:
	virtual ~addon_manager_t();

	static std::unique_ptr<addon_manager_t> make();

	template<typename XServices,
		typename XTypeDriver,
		typename std::enable_if< (std::is_base_of<i_host_services_t, XServices>::value
		&& std::is_base_of<crm::i_types_driver_t, XTypeDriver>::value), int>::type = 0>
	static std::unique_ptr<addon_manager_t> make( typename std::shared_ptr<XServices> srv,
		typename std::shared_ptr<XTypeDriver> tpDrv ) {

		return std::unique_ptr<self_t>( new self_t( std::move( srv ), std::move( tpDrv ) ) );
		}

	std::shared_ptr<detail::module_anchor_t> __capture_by_path(const std::wstring &path)noexcept;

private:
	void erase_expired()noexcept;

	template<typename TAddon>
	typename TAddon _load( const std::wstring &path ) {
		typedef typename TAddon addon_t;

		DCF_ADDON_MANAGER_TRACE_MANAGER_FWD(_managerId.to_wstr() + L":load=" + path);

		/* ������� ������������ */
		erase_expired();

		/* ������ �� ������ ���� */
		auto amd = __capture_by_path(path);
		if( !amd ) {
			/* ����������� ������ ������������� */
			const auto addonId = _create_unique_addon_id();

			/* �������� ����� �� ������ */
			auto module = std::make_shared<detail::module_anchor_t>( path,
				std::dynamic_pointer_cast<i_host_services_t>(local_services()),
				std::dynamic_pointer_cast<i_types_driver_t>(type_driver()),
				addonId );

			/* ��������, ��� ������ � ����� ��������������� �� ��������������� */
			const auto idIt = _map.find( module->uuid() );
			if( idIt != _map.cend() ) {
				if( !idIt->second.expired() )
					CBL_CORE_SHARED_MODULE_THROW_FWD( L"addon with the same guid already loaded[path is differeces]" );
				}
			_map[module->uuid()] = module;

			amd = module;
			}
		else {
			DCF_ADDON_MANAGER_TRACE_MANAGER_FWD(_managerId.to_wstr() + L":exista=" + path);
			}

		/* �������� �������� ������ */
		return addon_t( amd );
		}

	template<typename TAddon>
	typename TAddon _get( const addon_identifier_t &id ) {
		typedef typename TAddon addon_t;

		addons_cart_t::iterator it = _map.find( id );
		if( it == _map.end() )
			return std::move( addon_t() );

		auto addonLck( it->second.lock() );
		if( !addonLck ) {
			_map.erase( it );
			return addon_t();
			}

		auto srv( services() );
		if( !srv ) {
			CBL_CORE_SHARED_MODULE_THROW_FWD(nullptr);
			}

		return addon_t( addonLck, std::move(srv) );
		}

public:
	std::shared_ptr<i_host_services_t> local_services()noexcept;
	std::shared_ptr<i_host_services_t> local_services()const noexcept;

	std::shared_ptr<i_types_driver_t> type_driver()noexcept;
	std::shared_ptr<i_types_driver_t> type_driver()const noexcept;

	template<typename XServices,
		typename XTypeDriver,
		typename std::enable_if< (std::is_base_of<i_host_services_t, XServices>::value
		&& std::is_base_of<crm::i_types_driver_t, XTypeDriver>::value), int>::type = 0 >
	void set_services( typename std::shared_ptr<XServices> srv,
		typename std::shared_ptr<XTypeDriver> tpDrv ) {

		if( srv )
			std::atomic_exchange( &_hservices, std::dynamic_pointer_cast<crm::addon::i_host_services_t>(srv) );

		if( tpDrv )
			std::atomic_exchange( &_tpdrv, std::dynamic_pointer_cast<crm::i_types_driver_t>(tpDrv) );
		}

	template<typename XServices,
		typename std::enable_if< (std::is_base_of<i_host_services_t, XServices>::value), int>::type = 0 >
	void set_host_services( typename std::shared_ptr<XServices> srv ) {
		set_services<i_host_services_t, i_types_driver_t>( srv, nullptr );
		}

	template<typename XTypeDriver,
		typename std::enable_if< (std::is_base_of<crm::i_types_driver_t, XTypeDriver>::value), int>::type = 0 >
	void set_types_driver( typename std::shared_ptr<XTypeDriver> tpDrv ) {
		set_services<i_host_services_t, i_types_driver_t>( nullptr, tpDrv );
		}

	/*! ������� ��������� �������� ������� �� ���������� ����.
	\param [in] path ���� ������������ �������
	\return ������������� ������� \ref addon_identifier_t
	*/
	template<typename TAddon>
	typename TAddon load( const std::wstring &path ) {
		mutex_scope_lock_t lock( _lock );
		return _load<TAddon>( path );
		}

	/*! ������� ���������� ������-��������� �������� �������.
	\param [in] id ������������� �������
	\return ���������� ������-��������� �������� �������,
	����� nullptr ���� ������ � ����� ��������������� �� ��������������� � �������
	*/
	template<typename TAddon>
	typename TAddon get( const addon_identifier_t &id ) {
		mutex_scope_lock_t lock( _lock );
		return _get<TAddon>( id );
		}

	/*! ��������� ��������� ���������� ��� ���� ����������� �������� */
	void clear()noexcept;

	void close()noexcept;
	};
}
}

