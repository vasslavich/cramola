#include <Windows.h>
#include <atlbase.h>
#include <string>
#include "../../internal.h"
#include "../../logging/logger.h"


using namespace crm;
using namespace crm::addon;


const std::string i_host_services_t::datagram_value_key_notification = "[crm:sm:dtg:keyname:notification]";


void pl_services_reset( platform_services_t *psrv ) {
	memset( psrv, 0, sizeof( platform_services_t ) );
	}

int pl_services_empty( const platform_services_t *psrv ) {
	const uint8_t* bPsrv = (const uint8_t*)psrv;
	for( size_t i = 0; i < sizeof( platform_services_t ); ++i )
		if( 0 != (*bPsrv++) )return 0;

	return 1;
	}

i_addon_hndl_t::~i_addon_hndl_t() {}
i_addon_instance_hndl_t::~i_addon_instance_hndl_t() {}

bool crm::addon::success_addon_function_result( std::uint32_t code ) {
	return (uint32_t)crm::plugin_exit_syscode_t::PE_SUCCESS == code;
	}



