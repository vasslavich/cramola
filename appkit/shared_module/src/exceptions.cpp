#include "../../internal.h"
#include "../../logging/logger.h"


using namespace crm;
using namespace crm::addon;


exceptions_extractor_t::exceptions_extractor_t() noexcept {}

exceptions_extractor_t::exceptions_extractor_t( std::weak_ptr<exceptions_driver_t> excdrv )noexcept
	: _drv( excdrv ) {}

//std::list<std::unique_ptr<dcf_exception_t>> exceptions_extractor_t::get_notifications( const void *poutdExc )const noexcept {
//
//	std::list<std::unique_ptr<dcf_exception_t>> result;
//
//	if( poutdExc ) {
//		auto pDrv( _drv.lock() );
//		if( pDrv ) {
//			datagram_t dExc;
//			dExc.create_from( poutdExc );
//
//			/* список уведомлений */
//			result = pDrv->get_notifications( dExc, i_host_services_t::datagram_value_key_notification );
//			}
//		}
//
//	return std::move( result );
//	}

std::list<std::unique_ptr<dcf_exception_t>> exceptions_extractor_t::get_exceptions( const void *poutdExc )const noexcept {

	std::list<std::unique_ptr<dcf_exception_t>> result;

	if( poutdExc ) {
		auto pDrv( _drv.lock() );
		if( pDrv ) {
			datagram_t dExc;
			dExc.create_from( poutdExc );

			/* список уведомлений */
			result = pDrv->get_exceptions( dExc, i_host_services_t::datagram_value_key_notification );
			}
		}

	return std::move( result );
	}


std::list<std::unique_ptr<dcf_exception_t>> exceptions_extractor_t::get( const void *poutdExc )const noexcept {

	std::list<std::unique_ptr<dcf_exception_t>> result;

	if( poutdExc ) {
		auto pDrv( _drv.lock() );
		if( pDrv ) {
			datagram_t dExc;
			dExc.create_from( poutdExc );

			/* список уведомлений */
			result = pDrv->get( dExc, i_host_services_t::datagram_value_key_notification );
			}
		}

	return result;
	}

exceptions_cather_t::exceptions_cather_t( std::shared_ptr<i_host_services_t> hsrv )noexcept
	: _hsrv(hsrv) {}

void exceptions_cather_t::create( void **ppDExc,
	const std::unique_ptr<dcf_exception_t> & ntf )noexcept {
	
	if( ntf ) {
		create( ppDExc, (*ntf) );
		}
	}

void exceptions_cather_t::create( void **ppDExc,
	const crm::dcf_exception_t & exc )noexcept {

	if( ppDExc && _hsrv ) {
		crm::datagram_t dtExc;
		exceptions_driver_t::add( dtExc, exc, i_host_services_t::datagram_value_key_notification );

		dtExc.build( _hsrv, ppDExc
			
#ifdef CBL_USE_TRACE_SM_HOST_MEMORY
			, "exceptions_cather_t::create"
#endif
			);
		}
	}

void exceptions_cather_t::add( crm::datagram_t & dt,
	const dcf_exception_t & exc )noexcept {

	exceptions_driver_t::add( dt, exc, i_host_services_t::datagram_value_key_notification );
	}



