#include "../../internal.h"
#include "../../logging/logger.h"


using namespace crm;
using namespace crm::addon;
using namespace crm::addon::detail;


/*==============================================================================
addon_manager_t
================================================================================*/


std::unique_ptr<addon_manager_t> addon_manager_t::make() {
	return std::unique_ptr<self_t>( new self_t() );
	}

void addon_manager_t::_clear()noexcept {
	DCF_ADDON_MANAGER_TRACE_MANAGER("clear:" + _managerId.to_str());

	auto ls = local_services();
	if( ls ) {
		ls->close();
		}

	auto addnIt = _map.begin();
	const auto addnEnd = _map.end();

	for( ; addnIt != addnEnd; ++addnIt )
		addnIt->second.reset();
	}

addon_identifier_t addon_manager_t::_create_unique_addon_id()const noexcept {

	addon_identifier_t addonId = addon_identifier_t::rand();
	CBL_VERIFY( addonId != addon_identifier_t::null, 0 );

	do {
		auto addnIt = _map.cbegin();
		const auto addnEnd = _map.cend();

		for( ; addnIt != addnEnd; ++addnIt ) {
			if( addnIt->first == addonId ) {

				DCF_ADDON_MANAGER_TRACE_MANAGER( "dublicate module identifiers");

				addonId = addon_identifier_t::null;
				break;
				}
			}

		} while( addonId == addon_identifier_t::null );

	return std::move( addonId );
	}

addon_manager_t::addon_manager_t()
	: _hservices( std::make_shared<host_services_t>( _managerId ) )
	, _tpdrv( std::make_shared<types_driver_t>() ) {}

addon_manager_t::~addon_manager_t() {
	_close();
	}

void addon_manager_t::close()noexcept {
	mutex_scope_lock_t lock( _lock );
	_close();
	}

void addon_manager_t::_close()noexcept {
	_clear();
	}

std::shared_ptr<i_host_services_t> addon_manager_t::local_services() noexcept {
	return std::atomic_load_explicit( &_hservices, std::memory_order::memory_order_acquire );
	}

std::shared_ptr<i_host_services_t> addon_manager_t::local_services()const noexcept {
	return std::atomic_load_explicit( &_hservices, std::memory_order::memory_order_acquire );
	}

void addon_manager_t::clear()noexcept {
	mutex_scope_lock_t lock( _lock );
	_clear();
	}

std::shared_ptr<i_types_driver_t> addon_manager_t::type_driver() noexcept {
	return std::atomic_load_explicit( &_tpdrv, std::memory_order::memory_order_acquire );
	}

std::shared_ptr<i_types_driver_t> addon_manager_t::type_driver()const noexcept {
	return std::atomic_load_explicit( &_tpdrv, std::memory_order::memory_order_acquire );
	}

void addon_manager_t::erase_expired()noexcept {

	auto it = _map.begin();
	while(it != _map.end()) {

		if(it->second.expired()) {
			it = _map.erase(it);
			}
		else {
			++it;
			}
		}
	}

std::shared_ptr<module_anchor_t> addon_manager_t::__capture_by_path(const std::wstring &path)noexcept {
	auto it = _map.begin();
	while(it != _map.end()) {

		if(auto sit = it->second.lock()) {
			if(sit->path() == path)
				return sit;
			else
				++it;
			}
		else{
			it = _map.erase(it);
			}
		}

	return nullptr;
	}

