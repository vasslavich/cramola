#include "../../internal.h"
#include "../../logging/logger.h"


using namespace crm;
using namespace addon;
using namespace addon::detail;


addon_instance_t::addon_instance_t() noexcept {}

void addon_instance_t::reset()noexcept {
	_id = DCF_INSTANCE_HANDLER_UNDEF;
	std::atomic_store_explicit(&_module, std::shared_ptr<module_anchor_t>(), std::memory_order::memory_order_release);
	}

addon_instance_t::addon_instance_t( std::shared_ptr<module_anchor_t> addon,
	notification_handler_t && nth,
	addon_instance_handler_t id,
	finalize_t && procFinalize )
	: _id( id )
	, _module( addon )
	, _finalize( std::move(procFinalize ))
	, _nth( std::move(nth)){}

addon_instance_t::addon_instance_t( std::shared_ptr<module_anchor_t> addon,
	notification_handler_t && nth,
	initialize_t && procInitialize,
	finalize_t && procFinalize )
	: _module( addon )
	, _finalize( std::move(procFinalize ))
	, _nth( std::move(nth)){

	procInitialize( &_id );
	}

void addon_instance_t::check_exception(uint32_t hcode)const {
	if(auto m = get_module())
		m->check_exception(hcode);
	}

void addon_instance_t::check_exception(const void *pOutExc, uint32_t hcode)const {
	if(auto m = get_module())
		m->check_exception(pOutExc, hcode, _nth);
	}

void addon_instance_t::ntf_hndl( std::unique_ptr<dcf_exception_t> && ntf )const noexcept {
	if( ntf && _nth) {
		_nth( std::move( ntf ) );
		}
	}

//void addon_instance_t::ntf_hndl( const dcf_notification_t & ntf )const noexcept {
//	ntf_hndl( ntf.dcpy() );
//	}

void addon_instance_t::ntf_hndl( const dcf_exception_t & exc )const noexcept {
	ntf_hndl( exc.dcpy() );
	}

std::shared_ptr<module_anchor_t> addon_instance_t::get_module() noexcept {
	return std::atomic_load_explicit(&_module, std::memory_order::memory_order_acquire);
	}

std::shared_ptr<const module_anchor_t> addon_instance_t::get_module()const noexcept {
	return std::atomic_load_explicit(&_module, std::memory_order::memory_order_acquire);
	}

std::shared_ptr<i_host_services_t> addon_instance_t::services() noexcept {
	if(auto m = get_module())
		return m->host_services();
	else
		return {};
	}

std::shared_ptr<i_host_services_t> addon_instance_t::services()const noexcept {
	if(auto m = get_module())
		return m->host_services();
	else
		return {};
	}

addon_instance_t::addon_instance_t( self_t &&o )noexcept
	: _id( std::move( o._id ) )
	, _module( std::move( o._module ) )
	, _finalize( std::move( o._finalize ) )
	, _nth( std::move(o._nth)){

	o.reset();
	}

addon_instance_t& addon_instance_t::operator = (self_t &&o) noexcept {
	_id = std::move( o._id );
	_module = std::move( o._module );
	_finalize = std::move( o._finalize );
	_nth = std::move( o._nth );

	o.reset();
	return (*this);
	}

addon_instance_t::~addon_instance_t() {
	close();
	}

const addon_instance_handler_t& addon_instance_t::id()const noexcept {
	return _id;
	}

const addon_identifier_t& addon_instance_t::addon_uuid()const noexcept {
	if(auto m = get_module())
		return m->uuid();
	else
		return addon_identifier_t::null;
	}

std::wstring addon_instance_t::get_error_message( uint32_t code )const noexcept {
	if(auto m = get_module())
		return m->get_error_message(code);
	else
		return {};
	}

void addon_instance_t::finalize() noexcept {

	// ��������� ��� �����������
	CHECK_NO_EXCEPTION_BEGIN

		CHECK_PLUGIN_EXIT_CODE_INSTANCE( (*this), _finalize, _id );
	
	CHECK_NO_EXCEPTION_END
	}

void addon_instance_t::close() noexcept {
	bool clf = false;
	if( _closedf.compare_exchange_strong( clf, true ) ) {
		if( !is_null() ) {

			/* ������� �� ������� �������� ������� */
			detail::addon_reverse_handler_table_t::erase( addon_uuid(), id() );

			/* ������� ��� ���������� ������� */
			finalize();

			/* �������� ��������� ������� � ���� */
			reset();
			}
		}
	}

bool addon_instance_t::closed()const noexcept {
	return _closedf.load( std::memory_order::memory_order_acquire );
	}

bool addon_instance_t::is_null()const noexcept {
	return DCF_INSTANCE_HANDLER_UNDEF == _id;
	}

