#include "../../internal.h"
#include "../../logging/logger.h"


using namespace crm;
using namespace crm::addon;
using namespace crm::addon::detail;


i_reverse_handler_t::~i_reverse_handler_t() {}


/*=================================================================================
addon_reverse_handler_table_t
===================================================================================*/

addon_reverse_handler_table_t::instances_table_t addon_reverse_handler_table_t::_tbl;
addon_reverse_handler_table_t::lock_t addon_reverse_handler_table_t::_tblLck;


size_t addon_reverse_handler_table_t::__size()noexcept {
	size_t c{ 0 };
	for( const auto & aid : _tbl){
		c += aid.second.size();
		}

	return c;
	}

void addon_reverse_handler_table_t::insert( const addon_identifier_t& addonId,
	const addon_instance_handler_t& instId,
	std::weak_ptr<i_reverse_handler_t> hndl ) {

	scope_lock_t lck( _tblLck );

	auto itAddon = _tbl.find( addonId );
	if( itAddon == _tbl.end() ) {

		auto insertRes = _tbl.insert( std::make_pair( addonId, instances_list_t() ) );
		if( !insertRes.second ) {
			CBL_CORE_SHARED_MODULE_THROW_FWD(nullptr);
			}

		itAddon = insertRes.first;
		}


	auto itInst = itAddon->second.find( instId );
	if( itInst != itAddon->second.end() ) {
		CBL_CORE_SHARED_MODULE_THROW_FWD(nullptr);
		}

	if( !itAddon->second.insert( std::make_pair( instId, hndl ) ).second ) {
		CBL_CORE_SHARED_MODULE_THROW_FWD(nullptr);
		}

	DCF_ADDON_MANAGER_TRACE_MANAGER("addon_reverse_handler_table_t::insert:" + std::to_string(__size()));
	}

void addon_reverse_handler_table_t::erase( const addon_identifier_t& addonId,
	const addon_instance_handler_t& instId )noexcept {

	scope_lock_t lck( _tblLck );

	auto itAddon = _tbl.find( addonId );
	if( itAddon != _tbl.end() ) {

		auto itInst = itAddon->second.find( instId );
		if( itInst != itAddon->second.end() ) {
			itAddon->second.erase( itInst );
			}

		if( itAddon->second.empty() ) {
			_tbl.erase( itAddon );
			}
		}
	}

/*! ���������� �� ������������� ������ */
unsigned int addon_reverse_handler_table_t::reverse_handler_invoke(
	const unsigned char addonUUID[16],
	addon_instance_handler_t instId,
	const char* skey_,
	void *pDtg ) noexcept {

	addon_identifier_t addonId;
	addonId.assing_line( addonUUID );

	const std::string skey( skey_ );

	DCF_ADDON_MANAGER_TRACE_SMODULE("addon_reverse_handler_table_t::rhi lookup:" + skey + ":" + std::to_string((uintptr_t)pDtg));

	scope_lock_t lck( _tblLck );

	auto itAddon = _tbl.find( addonId );
	if( itAddon == _tbl.end() ) {
		return (uint32_t)crm::plugin_exit_syscode_t::PE_NOT_FOUND;
		}

	auto itInst = itAddon->second.find( instId );
	if( itInst == itAddon->second.end() ) {
		return (uint32_t)crm::plugin_exit_syscode_t::PE_NOT_FOUND;
		}

	auto hndl = itInst->second.lock();
	lck.unlock();

	if( hndl ) {
		try {
			DCF_ADDON_MANAGER_TRACE_SMODULE("addon_reverse_handler_table_t::rhi invoke:" + skey + ":" + std::to_string((uintptr_t)pDtg));

			hndl->reverse_invoke( skey, pDtg );
			return (uint32_t)crm::plugin_exit_syscode_t::PE_SUCCESS;
			}
		catch( const dcf_exception_t & exc ) {
			crm::logger_t::logging( exc );

			auto errc = exc.code() ? (uint32_t)exc.code() : (uint32_t)crm::plugin_exit_syscode_t::PE_INTERNAL;
			if((uint32_t)crm::plugin_exit_syscode_t::PE_SUCCESS == errc) {
				return (uint32_t)crm::plugin_exit_syscode_t::PE_INTERNAL;
				}
			else
				return errc;
			}
		catch(const std::exception & e1) {
			logger_t::logging(e1);
			return (uint32_t)crm::plugin_exit_syscode_t::PE_INTERNAL;
			}
		catch( ... ) {
			logger_t::logging( "", 1, __CBL_FILEW__, __LINE__ );
			return (uint32_t)crm::plugin_exit_syscode_t::PE_INTERNAL;
			}
		}
	else
		return (uint32_t)crm::plugin_exit_syscode_t::PE_NOT_FOUND;
	}


/*==========================================================================
addon_t
============================================================================*/


addon_t::addon_t()noexcept {}

addon_t::addon_t( std::shared_ptr<module_anchor_t> module_  )
	: _module( module_ ) {}

bool addon_t::is_null()const noexcept {
	return !_module;
	}

std::wstring addon_t::path()const noexcept {
	if(_module)
		return _module->path();
	else
		return {};
	}

void addon_t::close()noexcept {}

const addon_identifier_t& addon_t::uuid()const noexcept {
	if(_module)
		return _module->uuid();
	else
		return addon_identifier_t::null;
	}

std::wstring addon_t::get_error_message( const uint32_t errorCode )const noexcept {
	if(_module)
		return _module->get_error_message(errorCode);
	else
		return {};
	}

std::shared_ptr<i_host_services_t> addon_t::host_services()const noexcept {
	if(_module)
		return _module->host_services();
	else
		return {};
	}

std::shared_ptr<crm::i_types_driver_t> addon_t::type_driver()const noexcept {
	if(_module)
		return _module->type_driver();
	else
		return {};
	}

void addon_t::check_exception( uint32_t hcode )const {
	if( _module )
		_module->check_exception( hcode );
	}

void addon_t::check_exception( const void *pOutExc, uint32_t hcode )const {
	if( _module )
		_module->check_exception( pOutExc, hcode );
	}

void addon_t::check_exception( const void *pOutExc, uint32_t hcode,
	const addon_instance_t::notification_handler_t & ntfhndl )const {

	if( _module )
		_module->check_exception( pOutExc, hcode, ntfhndl );
	}
