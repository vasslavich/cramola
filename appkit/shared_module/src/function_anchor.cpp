#include "../../internal.h"
#include "../../logging/logger.h"
#include "../../utilities/convert/cvt.h"


using namespace crm;
using namespace crm::addon::detail;


function_anchor_t::function_anchor_t( const std::shared_ptr<module_anchor_t> & module_,
	const std::string &name )
	: _iModule( module_ )
	, _pFuncAddress( module_->function_address(name) )
	, _name( name ) {

	if( !_pFuncAddress ) {
		CBL_CORE_SHARED_MODULE_THROW_FWD( module_->path() + cvt<wchar_t>( name));
		}
	}

bool function_anchor_t::is_alive()const {
	return nullptr != _pFuncAddress;
	}

void* function_anchor_t::function_address()const {
	return _pFuncAddress;
	}

const std::string& function_anchor_t::name() const {
	return _name;
	}

