#include "../../internal.h"
#include "../../logging/logger.h"


using namespace crm;
using namespace crm::addon;


/*=================================================================================
host_services_t
===================================================================================*/


host_services_t::cart_t host_services_t::_map;
host_services_t::lck_t host_services_t::_lock;
std::atomic<size_t> host_services_t::_memCounter = 0;


host_services_t::host_services_t( const addon_identifier_t & hostId ) 
	: _hostId(hostId){	
	
	reverse_handlers_t rh;
	rh.invoke = &detail::addon_reverse_handler_table_t::reverse_handler_invoke;

	_rvHd = rh;
	}

void* host_services_t::addon_allocate_memory(const unsigned char addonUUID[16],
	size_t count,
#ifdef DCF_ADDON_MANAGER_TRACE
	const char* traceid
#else
	const char*
#endif
) {

	memory_t item;
	item.uuid.assing_line(addonUUID);
	item.count = count;

	void* p = ::malloc(count);

	lck_scp_t alock(_lock);
	if (!_map.insert({ (uintptr_t)p, std::move(item) }).second) {
		::free(p);
		CBL_CORE_SHARED_MODULE_THROW_FWD(nullptr);
		}

	_memCounter.fetch_add(count);

	DCF_ADDON_MANAGER_TRACE_HOST_MEMORY((traceid ? std::string(traceid) : std::string())
		+ ":allocated memory=" + std::to_string(_memCounter.load(std::memory_order::memory_order_acquire)));

	return p;
	}

void host_services_t::addon_free_memory( void *pMemory )noexcept {

	lck_scp_t alock( _lock );
	auto it = _map.find( (uintptr_t)pMemory );
	if( it != _map.end() ) {

		::free( (void*)it->first );
		_memCounter.fetch_sub( it->second.count );
		_map.erase( it );
		}
	}

platform_services_t host_services_t::get_host_services()const noexcept {
	platform_services_t ps;
	ps.memory_services.allocate_memory = &addon_allocate_memory;
	ps.memory_services.free_memory = &addon_free_memory;
	ps.reverse_handlers = _rvHd;
	return ps;
	}

void* host_services_t::allocate( size_t count, const char * traceid ) {
	return addon_allocate_memory( _hostId.line(), count, traceid );
	}

void* host_services_t::allocate( size_t count ) {
	return addon_allocate_memory( _hostId.line(), count, nullptr );
	}

void host_services_t::free( void *p )noexcept {
	addon_free_memory( p );
	}

void host_services_t::reverse_invoke( addon_instance_handler_t instId,
	const std::string & skey,
	void *pDtg ) noexcept {

	if( _rvHd.invoke ) {
		_rvHd.invoke( _hostId.line(), instId, skey.c_str(), pDtg );
		}
	}

void host_services_t::close() noexcept {
	_clf.store( true, std::memory_order::memory_order_release );
	}

bool host_services_t::closed()const noexcept {
	return _clf.load( std::memory_order::memory_order_acquire );
	}

