#include "../../internal.h"
#include <Windows.h>


using namespace crm;
using namespace crm::addon;
using namespace crm::addon::detail;


struct module_anchor_t::hinstance_t {
	/*! ��������� ������������ ������ */
	HINSTANCE h = NULL;
	};

module_anchor_t::module_anchor_t()noexcept {}

module_anchor_t::module_anchor_t( const std::wstring & path,
	std::shared_ptr< i_host_services_t > services,
	std::shared_ptr<i_types_driver_t> tpdrv,
	const addon_identifier_t & moduleId )
	: _path( path )
	, _uuid( moduleId )
	, _services(services)
	, _tpdrv(tpdrv){

	if( !_services )
		CBL_CORE_SHARED_MODULE_THROW_FWD(nullptr);
	if( !_tpdrv || !_tpdrv->exceptions_driver() )
		CBL_CORE_SHARED_MODULE_THROW_FWD(nullptr);

	/* �������� ����������
	-------------------------------------------------*/
	_hmodule = new hinstance_t{};
	_hmodule->h = LoadLibraryW( _path.c_str() );
	if( nullptr == _hmodule->h ) {
		std::wstring errMsg( crm::get_system_error() );
		CBL_CORE_SHARED_MODULE_THROW_FWD( errMsg + L":" + _path );
		}


	/* ��������� ������������ �������
	------------------------------------------------*/
	_errorMessage = function_get_error_message_t( (*this), DCF_ADDON_PROC_NAME_GET_ERR_MSG );


	/* �������� ���������� ������� �������
	-----------------------------------------------*/
	std::string sUuid = _uuid.to_str();
	CBL_VERIFY( sUuid.size() == addon_identifier_t::str_len );

	/* ������� ����� */
	const platform_services_t platformSrv = services->get_host_services();

	/* ��������� �� �������� ��������� ���������� */
	crm::detail::memfree_with_i_services_t outdExc( _services );

	/* ����� ������� ������������� ������ */
	auto fload = function_addon_load_t( (*this), DCF_ADDON_PROC_NAME_LOAD );
	auto result = fload( &platformSrv, sUuid.c_str(), &outdExc.p );

	/* �������� �� ���������� */
	check_exception( outdExc.p, result, nullptr );


	/* ��������� ���� ������� 
	-----------------------------------------------------------------------------*/
	auto funload = function_addon_unload_t( (*this), DCF_ADDON_PROC_NAME_UNLOAD );
	_resourceCleaner = [this, funload] {

		/* ��� ����������� */
		funload();
		};

	DCF_ADDON_MANAGER_TRACE_ANCHOR_FWD(L"create=" + _path);
	}

module_anchor_t::~module_anchor_t() {
	CHECK_NO_EXCEPTION(__dctr());
	}

void module_anchor_t::__dctr()noexcept {

	crm::detail::post_scope_action_t unload([this] {
		if (nullptr != _hmodule) {
			if (NULL != _hmodule->h) {
				CHECK_NO_EXCEPTION(::FreeLibrary(_hmodule->h));
				}

			delete _hmodule;
			_hmodule = nullptr;
			}
		});

	if(_resourceCleaner) {
		CHECK_NO_EXCEPTION(_resourceCleaner());
		}

	DCF_ADDON_MANAGER_TRACE_ANCHOR_FWD(L"destroy=" + _path);
	}

std::shared_ptr<i_types_driver_t> module_anchor_t::type_driver()const noexcept {
	return std::atomic_load_explicit( &_tpdrv, std::memory_order::memory_order_acquire );
	}

std::shared_ptr<i_host_services_t> module_anchor_t::host_services()const noexcept {
	return std::atomic_load_explicit( &_services, std::memory_order::memory_order_acquire );
	}

const addon_identifier_t& module_anchor_t::uuid()const noexcept {
	return _uuid;
	}

std::wstring module_anchor_t::get_error_message( const uint32_t errorCode )const noexcept {
	return get_error_message( _errorMessage, errorCode );
	}

std::wstring module_anchor_t::get_error_message( function_get_error_message_t &functor, const uint32_t errorCode ) noexcept {
	if( functor.is_null() )
		return L"not accesseble";

	if( (uint32_t)crm::plugin_exit_syscode_t::PE_SUCCESS != errorCode ) {

		/* ������ �������� ����������������� ��������� */
		if( errorCode < (uint32_t)crm::plugin_exit_syscode_t::PE_SYSTEM_UPPER_BOUNDARY ) {
			auto pStr = plugin_exit_syscode_message( errorCode );
			if( pStr )
				return pStr;
			}
		else {

			wchar_t userMessage[1024];
			size_t widecharCount = 0;

			const uint32_t getMsgRes = functor( errorCode, userMessage, sizeof( userMessage ), &widecharCount );
			if( (uint32_t)crm::plugin_exit_syscode_t::PE_SUCCESS == getMsgRes ) {

				userMessage[(std::min)( (sizeof( userMessage ) / sizeof( wchar_t )) - 1, widecharCount )] = '\0';
				return userMessage;
				}
			}
		}
	else
		return plugin_exit_syscode_message( errorCode );

	return L"undefined";
	}

void* module_anchor_t::function_address( const std::string & name )const noexcept {
	if (_hmodule && _hmodule->h != NULL) {
		return ::GetProcAddress(_hmodule->h, name.c_str());
		}
	else {
		return nullptr;
		}
	}

void* module_anchor_t::hndl()const noexcept {
	return _hmodule->h;
	}

const std::wstring& module_anchor_t::path()const noexcept {
	return _path;
	}

void module_anchor_t::check_exception( const void *pOutExc, uint32_t hcode,
	const notification_handler_t & /*ntfhndl*/ )const {

	/* �������� �� ���������� */
	if( pOutExc ) {
		if( _tpdrv ) {
			auto excDrv( _tpdrv->exceptions_driver() );
			if( excDrv ) {

				exceptions_extractor_t excExt( excDrv );

				auto basel = excExt.get(pOutExc);
				std::unique_ptr<crm::dcf_exception_t> firstExc;

				for(auto && ni : basel) {
					if(ni) {
						if (!firstExc) {
							firstExc = std::move(ni);
							CBL_VERIFY(firstExc);
							}
						}
					}

				if( firstExc ) {
					firstExc->rethrow();
					}
				}
			}
		}

	/* �������� �� ���� �������� */
	check_exception( hcode );
	}

void module_anchor_t::check_exception( const void *pOutExc, uint32_t hcode )const {
	check_exception( pOutExc, hcode, nullptr );
	}

void module_anchor_t::check_exception( uint32_t hcode )const {
	if( (uint32_t)crm::plugin_exit_syscode_t::PE_SUCCESS != hcode ) {

#ifdef CBL_TRACE_EXCEPTION_RAISE
		file_logger_t::logging(_path + L":" + std::to_wstring(hcode), 100200300);
#endif

		/* ������ �������� ����������������� ��������� */
		if( hcode < (uint32_t)crm::plugin_exit_syscode_t::PE_SYSTEM_UPPER_BOUNDARY ) {
			const auto str = crm::plugin_exit_syscode_message( hcode );
			CBL_CORE_SHARED_MODULE_THROW_FWD( str );
			}
		else {
			const std::wstring msg = get_error_message( hcode );
			CBL_CORE_SHARED_MODULE_THROW_FWD( msg );
			}
		}
	}
