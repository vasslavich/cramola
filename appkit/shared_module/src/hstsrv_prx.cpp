#include <algorithm>
#include "../../internal.h"
#include "../../logging/logger.h"
#include "../../tableval/tableval.h"
#include "../../utilities/utilities.h"


using namespace crm;
using namespace crm::detail;
using namespace crm::addon;


/*=================================================================================
hst_services_proxy_t
===================================================================================*/

hst_services_proxy_t::hst_services_proxy_t( const platform_services_t & hsrv,
	const addon_identifier_t &addonId )
	: _hstSrv( hsrv ) 
	, _addonId(addonId){}

void* hst_services_proxy_t::allocate( size_t cnt, const char *traceid ) {
	return _hstSrv.memory_services.allocate_memory( _addonId.line(), cnt, traceid );
	}

void* hst_services_proxy_t::allocate( size_t cnt ) {
	return _hstSrv.memory_services.allocate_memory( _addonId.line(), cnt, nullptr );
	}

void hst_services_proxy_t::free( void *p )noexcept {
	_hstSrv.memory_services.free_memory( p );
	}

platform_services_t hst_services_proxy_t::get_host_services()const noexcept {
	return _hstSrv;
	}


/*! ����� ����������� �� ������ */
void hst_services_proxy_t::reverse_invoke( addon_instance_handler_t hInstance,
	const std::string & skey,
	void *pDtg )noexcept {
	
	crm::detail::post_scope_action_t freeDtg( [this, pDtg] {
		_hstSrv.memory_services.free_memory( pDtg );
		} );

	DCF_ADDON_MANAGER_TRACE_SMODULE("hst_services_proxy_t::ri:" + skey + ":" + std::to_string((uintptr_t)pDtg));

	if( !closed() ) {

		/* ����� ������� ����� */
		const auto hres = _hstSrv.reverse_handlers.invoke( _addonId.line(), hInstance, skey.c_str(), pDtg );
		if((uint32_t)crm::plugin_exit_syscode_t::PE_SUCCESS == hres) {

			DCF_ADDON_MANAGER_TRACE_SMODULE("hst_services_proxy_t::ri:" + skey + ":" + std::to_string((uintptr_t)pDtg) + ": ok");
			}
		else {
			DCF_ADDON_MANAGER_TRACE_SMODULE("hst_services_proxy_t::reverse_invoke[" + skey + "fail]:addon|instance=" +
				_addonId.to_str() + ":" + std::to_string(hInstance) +
				+":ret code=" + std::to_string(hres));
			}
		}
	else {
		DCF_ADDON_MANAGER_TRACE_SMODULE("hst_services_proxy_t::ri:" + skey + ":" + std::to_string((uintptr_t)pDtg) + ": closed");
		}
	}

void hst_services_proxy_t::close()noexcept {
	_clf.store( true, std::memory_order::memory_order_release );
	}

bool hst_services_proxy_t::closed() const noexcept {
	return _clf.load( std::memory_order::memory_order_acquire );
	}
