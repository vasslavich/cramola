#pragma once


#include <string>
#include <sstream>
#include <vector>
#include <mutex>
#include <memory>
#include <map>
#include <atomic>
#include "../resources/i_memory_services.h"
#include "./addon_anchor.h"


namespace crm{
namespace addon{


class host_services_t final : public crm::addon::i_host_services_t {
private:
	typedef std::mutex lck_t;
	typedef std::unique_lock<std::mutex> lck_scp_t;

	struct memory_t {
		addon_identifier_t uuid;
		size_t count = 0;
		};

	/*======================================================================================
	������� ������� � ��������, ���������� �� �������
	========================================================================================*/

	static void* addon_allocate_memory( const unsigned char addonUUID[16],
		size_t count,
		const char* traceid );

	static void addon_free_memory( void *pMemory )noexcept;

	typedef std::map<uintptr_t, memory_t> cart_t;

	static cart_t _map;
	static lck_t _lock;
	static std::atomic< size_t> _memCounter;

	addon_identifier_t _hostId;
	reverse_handlers_t _rvHd;
	std::atomic<bool> _clf = false;

public:
	host_services_t( const addon_identifier_t & hostId );

	void* allocate( size_t count, const char * traceid ) final;
	void* allocate( size_t count ) final;
	void reverse_invoke( addon_instance_handler_t instId,
		const std::string & skey,
		void *pDtg )noexcept final;

	void free( void *p )noexcept final;
	platform_services_t get_host_services()const noexcept final;
	void close()noexcept final;
	bool closed()const noexcept;
	};
}
}

