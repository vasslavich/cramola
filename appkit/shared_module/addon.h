#pragma once


#include <string>
#include <vector>
#include <list>
#include <mutex>
#include <atomic>
#include <memory>
#include <map>
#include "./binding.h"
#include "./addon_instance.h"
#include "./addon_anchor.h"
#include "../utilities/scopedin/memdealloc.h"


namespace crm{
namespace addon{
namespace detail{


class addon_reverse_handler_table_t {
private:
	typedef std::mutex lock_t;
	typedef std::unique_lock<lock_t> scope_lock_t;

	typedef std::map<addon_instance_handler_t, std::weak_ptr<i_reverse_handler_t>> instances_list_t;
	typedef std::map < addon_identifier_t, instances_list_t> instances_table_t;

	static instances_table_t _tbl;
	static lock_t _tblLck;

	static size_t __size() noexcept;

public:
	static void insert( const addon_identifier_t& addonId, 
		const addon_instance_handler_t& instId, 
		std::weak_ptr<i_reverse_handler_t> hndl );

	static void erase( const addon_identifier_t& addonId,
		const addon_instance_handler_t& instId )noexcept;

	static unsigned int reverse_handler_invoke(
		const unsigned char addonUUID[16],
		addon_instance_handler_t instId,
		const char* skey,
		void *pDtg )noexcept;
	};


/*!
����� �������� ��������� ��������� ��� ��������� �������������� �������.
������������:
- ������������� ��������� ������� ��� ������ �������� �������;
- ������ ���������� ��������� ������ �������.
*/
class addon_t : public i_addon_hndl_t {
private:
	typedef addon_t self_t;
	typedef std::mutex lock_t;
	typedef std::lock_guard<lock_t> mutex_scope_lock_t;

private:

	/// ��������� ������������ ������
	std::shared_ptr<detail::module_anchor_t> _module;

	template<typename ... TArgsInit>
	addon_instance_t _obj_create( uint32_t & callRes, addon_instance_t::notification_handler_t && nthndl,
		const std::string &procInitName,
		const std::string &procFinalizeName,
		typename TArgsInit && ... argsInit ) {

		// ��������� ������� ���������� �������
		auto finalize = _get_function<uint32_t( addon_instance_handler_t )>( procFinalizeName );

		// ������� ������������� ������� 
		std::function<void( addon_instance_handler_t * )> fInitialize = [&, this]( addon_instance_handler_t *id ) {

			// ���������������� �������������
			(*id) = DCF_INSTANCE_HANDLER_UNDEF;
			auto fInit = _get_function<uint32_t( addon_instance_handler_t*, TArgsInit ... )>( procInitName.c_str() );

			/* ����� ��������� ������������� */
			callRes = fInit( id, std::forward<TArgsInit>( argsInit )... );
			};

		auto instObj( addon_instance_t( _module,
			std::move( nthndl ),
			std::move( fInitialize ),
			std::move( finalize ) ) );

		if( success_addon_function_result( callRes ) ) {
			return std::move( instObj );
			}
		else
			return addon_instance_t();
		}

	template<typename ReturnType, typename ...Args >
	addon_function_t<ReturnType, Args...> _get_function( const std::string &name ) {
		return addon_function_t<ReturnType, Args...>( _module, name );
		}

public:
	addon_t()noexcept;
	addon_t( std::shared_ptr<detail::module_anchor_t> module_ );

	bool is_null()const noexcept;

	std::wstring path()const noexcept override;

	/*! ��������� ��������� ���������� ������� */
	void close()noexcept override;

	/*! �������������� ����� ������ �������
	\return ������ ������� \ref addon_instance_t
	*/
	template<typename ... TArgsInit>
	addon_instance_t create_instance( uint32_t & callRes, std::weak_ptr<i_reverse_handler_t> reverseHndl,
		addon_instance_t::notification_handler_t && nthndl,
		const std::string &procInitName,
		const std::string &procFinalizeName,
		TArgsInit && ... argsInit ) {

		auto inst = _obj_create( callRes, std::move( nthndl ),
			procInitName,
			procFinalizeName,
			std::forward<TArgsInit>( argsInit ) ... );

		/* ���� ������������� ������ ������� */
		if( success_addon_function_result( callRes ) && !inst.is_null() ) {

			/* ����������� � ������� ��������� ������ */
			addon_reverse_handler_table_t::insert( uuid(), inst.id(), reverseHndl );
			}

		return std::move( inst );
		}

	/// ��������� �������������� �������, \ref addon_identifier_t
	const addon_identifier_t& uuid()const noexcept override;

	/*! ��������� ��������� �� ���� �������� �������
	\param [in] errorCode ��� �������� ������� �������
	\return ������ ��������� �� ���� ������
	*/
	std::wstring get_error_message( uint32_t errorCode )const noexcept override;

	/*! ��������� �������-�������� ��� ��������� �������������� ������������	� �� ������� �������
	\param [in] name ��� �������
	\return ������ ���� \ref addon_function_t
	*/
	template<typename ReturnType, typename ...Args >
	addon_function_t<ReturnType, Args...> get_function( const std::string &name ) {
		return _get_function<ReturnType, Args...>( name );
		}

	/*! ��������� �������� ����� ��� ������������� �� ������� �������.
	\ref platform_services_t
	*/
	std::shared_ptr<i_host_services_t> host_services()const noexcept override;
	std::shared_ptr<i_types_driver_t> type_driver()const noexcept override;

	void check_exception( uint32_t hcode )const;
	void check_exception( const void *pOutExc, uint32_t hcode )const;
	void check_exception( const void *pOutExc, uint32_t hcode, const addon_instance_t::notification_handler_t & ntfhndl )const;
	};
}
}
}

