#pragma once


#include "../typeframe/i_tpdriver.h"
#include "../notifications/exceptions/excdrv.h"
#include "./binding.h"


namespace crm{
namespace addon{


class exceptions_cather_t {
private:
	std::shared_ptr<i_host_services_t> _hsrv;

public:
	exceptions_cather_t( std::shared_ptr<i_host_services_t> hsrv )noexcept;

	void create( void **ppDExc,
		const std::unique_ptr<dcf_exception_t> & ntf )noexcept;

	void create( void **ppDExc,
		const dcf_exception_t& exc )noexcept;

	static void add( crm::datagram_t & dt,
		const dcf_exception_t& exc )noexcept;
	};


class exceptions_extractor_t {
private:
	std::weak_ptr<exceptions_driver_t> _drv;

public:
	exceptions_extractor_t()noexcept;
	exceptions_extractor_t( std::weak_ptr<exceptions_driver_t> excdrv )noexcept;

	//std::list<std::unique_ptr<dcf_exception_t>> get_notifications( const void *outdExc )const noexcept;
	std::list<std::unique_ptr<dcf_exception_t>> get_exceptions( const void *outdExc )const noexcept;
	std::list<std::unique_ptr<dcf_exception_t>> get( const void *outdExc )const noexcept;
	//std::list<std::unique_ptr<dcf_exception_t>> get(const void *outdExc, std::initializer_list<dcf_exception_t::type_t> && )const noexcept;
	};
}
}

