#pragma once


#include <functional>
#include "./binding.h"
#include "./addon_anchor.h"


namespace crm{
namespace addon{
namespace detail{


class function_anchor_t {
private:
	std::shared_ptr<detail::module_anchor_t> _iModule;
	void *_pFuncAddress = nullptr;
	std::string _name;

public:
	/*! ������������� ������� */
	function_anchor_t( const std::shared_ptr<detail::module_anchor_t> & module_,
		const std::string &name );

	/*! ������ �������� � ������� �������
	\return true ���� ���������� �������� � �������, ����� false
	*/
	bool is_alive()const;

	/*! ��������� �� ������� */
	void *function_address()const;

	/*! ��� ������� */
	const std::string& name() const;
	};
}
}
}

