#pragma once


#include "./binding.h"
#include "./internal_terms.h"
#include "./addon_function.h"
#include "./addon_anchor.h"


namespace crm {
namespace addon{
namespace detail{


/*! ����� �������� ��������� ��� �������������� ������� � �������, � ������� �������� ������� �������.
��������� ������� ������������� �������� � �������� ��������.
�� ����� ���������� ������������� ��������� ������� ��� ������ �������� �������.
*/
class addon_instance_t : public i_addon_instance_hndl_t {
	friend class addon_t;

public:
	typedef addon_instance_t self_t;

	/*! ���������� ����������� */
	typedef module_anchor_t::notification_handler_t notification_handler_t;
	/*! ������� ������������ ������� ������ */
	typedef std::function<void( addon_instance_handler_t* )> initialize_t;
	/// ������� ���������� ��� �������� �������
	typedef addon_function_t<uint32_t( addon_instance_handler_t )> finalize_t;

private:
	/*! ������������� �������, ���������� ����� [out] �������� ������� ������� 
	��� ������������� ������� � ������� \ref initialize */
	addon_instance_handler_t _id{ DCF_INSTANCE_HANDLER_UNDEF };
	/* ������, ������� ������������� ������ */
	std::shared_ptr<detail::module_anchor_t> _module;
	/// ������� ���������� ��� �������� �������
	finalize_t _finalize;
	std::atomic<bool> _closedf = false;
	notification_handler_t _nth;

	void reset()noexcept;
	void finalize()noexcept;

	addon_instance_t( const self_t & ) = delete;
	self_t& operator=(const self_t &) = delete;

protected:
	void check_exception(uint32_t hcode)const;
	void check_exception(const void *pOutExc, uint32_t hcode)const;

	template<typename TFunctor, typename ... TArgs>
	static void CHECK_PLUGIN_EXIT_CODE_INSTANCE( const typename addon_instance_t & instance,
		typename TFunctor & functor,
		typename TArgs && ... args ) {

		const uint32_t callRes = functor( std::forward<TArgs>( args )... );
		instance.check_exception( callRes );
		}

	/*! �����������.
	\param [in] addon ������ �� �������� ����� ������������������ ������
	\param [in] services ������� �������
	\param [in] id ������������� �������
	\parma [in] procFinalize ������� ���������� �������
	*/
	addon_instance_t( std::shared_ptr<detail::module_anchor_t> addon,
		notification_handler_t && nth,
		addon_instance_handler_t id,
		finalize_t && procFinalize );

	/*! �����������.
	\param [in] addon ������ �� �������� ����� ������������������ ������
	\param [in] services ������� �������
	\param [in] procInitialize ������� ������������� �������
	\parma [in] procFinalize ������� ���������� �������
	*/ 
	addon_instance_t( std::shared_ptr<detail::module_anchor_t> addon,
		notification_handler_t && nth,
		initialize_t && procInitialize,
		finalize_t && procFinalize );

	std::shared_ptr<detail::module_anchor_t> get_module()noexcept;
	std::shared_ptr<const detail::module_anchor_t> get_module()const noexcept;

	std::shared_ptr<i_host_services_t> services()noexcept;
	std::shared_ptr<i_host_services_t> services()const noexcept;

protected:
	/*! �������� ����������� �� ���� */
	void ntf_hndl( std::unique_ptr<dcf_exception_t> && ntf )const noexcept;
	/*! �������� ���������� � ���� */
	void ntf_hndl( const dcf_exception_t & exc )const noexcept;

public:
	addon_instance_t()noexcept;

	/*! ����������� ����������� */
	addon_instance_t( self_t &&o )noexcept;

	/*! �������� ����������� */
	addon_instance_t& operator=(self_t &&o)noexcept;

	/*! ���������� */
	~addon_instance_t();

	/*! ��������� �������������� �������.
	\return \ref addon_instance_handler_t
	*/
	const addon_instance_handler_t& id()const noexcept override;

	/*! ������������� ������� */
	const addon_identifier_t& addon_uuid()const noexcept override;

	/*! ��������� ���������� ��������� �� ���� ������ */
	std::wstring get_error_message( uint32_t code )const noexcept  override;

	/*! �������� ������� */
	void close() noexcept override;

	bool closed()const noexcept;

	bool is_null()const noexcept;
	};
}
}
}

