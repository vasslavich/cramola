#pragma once


#include <list>
#include <map>
#include <mutex>
#include <atomic>
#include "../resources/i_memory_services.h"
#include "./binding.h"


namespace crm{
namespace addon{

class hst_services_proxy_t : public i_host_services_t {
private:
	platform_services_t _hstSrv;
	addon_identifier_t _addonId;
	std::atomic<bool> _clf = false;

public:
	hst_services_proxy_t( const platform_services_t & hstSrv,
		const addon_identifier_t &addonId );

	/*======================================================================================
	������� ���������� ��������� �������
	========================================================================================*/

	void* allocate( size_t cnt, const char* traceid )final;
	void* allocate( size_t cnt )final;
	void free( void *p )noexcept final;

	void reverse_invoke( addon_instance_handler_t hInstance,
		const std::string & skey,
		void *pDtg )noexcept final;

	platform_services_t get_host_services()const noexcept final;
	void close()noexcept final;
	bool closed()const noexcept;
	};
}
}

