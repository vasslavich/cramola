#pragma once


#include <functional>
#include "../notifications/internal_terms.h"
#include "../typeframe/i_tpdriver.h"
#include "./binding.h"


namespace crm{
namespace addon{
namespace detail{


struct module_anchor_t {
public:
	/*! ���������� ����������� */
	typedef std::function<void( std::unique_ptr<crm::dcf_exception_t> && ntf )> notification_handler_t;

private:
	/*! ����� ����������� ������� � ���������� ������ ����������
	*/
	template<typename ...Args>
	class internal_function_t {};

	/*! ����������� ������� �������, ������������ ������������� � ��������� ������� ������.
	\tparam ReturnType ��� ������������� �������� ��������
	\tparam ...Args ������ ����� ���������� �������
	*/
	template<typename ReturnType, typename ... Args>
	class internal_function_t<ReturnType( Args ... )> {
	public:
		/*! ��� ���������� ������� */
		typedef ReturnType result_type_t;

		/*! �������� �������������� ������� ������� */
		typedef result_type_t( DCF_ADDON_CALL *function_type_t )(Args ...);

	private:
		void *_pFuncAddress = nullptr;

	public:
		internal_function_t() noexcept {}

		/*! ������������� ������� */
		internal_function_t( const module_anchor_t &anchor, const std::string &name )
			: _pFuncAddress( anchor.function_address( name )){}

		bool is_null()const noexcept {
			return nullptr == _pFuncAddress;
			}

		/*! �������� ������ ������� �������.
		\param [in] args ������ ���������������� ����������
		\return ReturnType ������������ �������� ��������
		*/
		result_type_t operator()( Args ... args )const {
			if( !_pFuncAddress )
				CBL_CORE_SHARED_MODULE_THROW_FWD(nullptr);

			return ((function_type_t)_pFuncAddress)(args ...);
			}
		};

	typedef internal_function_t<uint32_t( char*, size_t )> function_get_uuid_t;
	typedef internal_function_t<uint32_t( uint32_t, wchar_t*, size_t, size_t* )> function_get_error_message_t;
	typedef internal_function_t<uint32_t( const platform_services_t *, const char *, void ** pOutExc )> function_addon_load_t;
	typedef internal_function_t<uint32_t()> function_addon_unload_t;

	struct hinstance_t;

	/*! ��������� ������������ ������ */
	hinstance_t* _hmodule = nullptr;
	/*! ���� �������� ������ */
	std::wstring _path;
	/*! ��������� ��������� �� ������ */
	mutable function_get_error_message_t _errorMessage;
	/*! ������������� ������� */
	addon_identifier_t _uuid;
	/*! ������� ������������ �������� */
	std::function<void()> _resourceCleaner;
	/*! ������� ������� */
	std::shared_ptr<i_host_services_t> _services;
	/*! ���� */
	std::shared_ptr<i_types_driver_t> _tpdrv;

	/*! ��������� ��������� �� ���� �������� �������
	\param [in] errorCode ��� �������� ������� �������
	\return ������ ��������� �� ���� ������
	*/
	static std::wstring get_error_message( function_get_error_message_t &functor, const uint32_t errorCode )noexcept;

	void __dctr()noexcept;

public:
	module_anchor_t()noexcept;
	module_anchor_t( const std::wstring & path, 
		std::shared_ptr< i_host_services_t> services,
		std::shared_ptr<i_types_driver_t> tpdrv,
		const addon_identifier_t & moduleId );

	~module_anchor_t();

	module_anchor_t(const module_anchor_t&) = delete;
	module_anchor_t& operator=(const module_anchor_t &) = delete;

	void* function_address( const std::string & name )const noexcept;
	void* hndl()const noexcept;
	const std::wstring& path()const noexcept;

	/*! ��������� �������������� �������, \ref addon_identifier_t */
	const addon_identifier_t& uuid()const noexcept;

	/*! ���� */
	std::shared_ptr<i_types_driver_t> type_driver()const noexcept;
	/*! ������� ������� */
	std::shared_ptr<i_host_services_t> host_services()const noexcept;

	/*! ��������� ��������� �� ���� �������� �������
	\param [in] errorCode ��� �������� ������� �������
	\return ������ ��������� �� ���� ������
	*/
	std::wstring get_error_message( const uint32_t errorCode )const noexcept;

	void check_exception( uint32_t hcode )const;
	void check_exception( const void *pOutExc, uint32_t hcode )const;
	void check_exception( const void *pOutExc, uint32_t hcode, const notification_handler_t & ntfhndl )const;
	};
}
}
}

