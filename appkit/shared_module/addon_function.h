#pragma once


#include <string>
#include <vector>
#include <list>
#include <mutex>
#include <atomic>
#include <memory>
#include <map>
#include <future>
#include <type_traits>
#include "./binding.h"
#include "./addon_anchor.h"
#include "./function_anchor.h"


namespace crm{
namespace addon{


/*! ����� ������������� ��������� ������������ ������� ������� \ref addon_function_t
�� ����� ���������� ������������� ��������� ������� ��� ������ �������� �������.
*/
template<typename TResult>
class async_result_t {
	template<typename ...Args>
	friend class addon_function_t;

public:
	typedef TResult result_t;

private:
	std::shared_future<TResult> _future;
	std::shared_ptr<detail::function_anchor_t> _anchor;

	template<typename TFutureSource>
	async_result_t( typename TFutureSource futureRes,
		const std::shared_ptr<detail::function_anchor_t> & anchor )
		: _future( futureRes )
		, _anchor( anchor ) {}

	async_result_t() = delete;

public:
	/*! �������� ���������� � ��� ������� */
	result_t get() {
		return _future.get();
		}

	/*! �������� ���������� ������� �������� ����� �����������
	\param [in] �������� ��������
	\return true - ������� ��������� ����������, ����� - false
	*/
	template<typename Rep, typename Period>
	bool wait( const std::chrono::duration<Rep, Period> & timeout ) {
		return _future.wait_for( timeout ) == std::future_status::ready;
		}
	};


/*! ����� ����������� ������� � ���������� ������ ����������
*/
template<typename ...Args>
class addon_function_t {};


/*! ������������� ������� � ��������� ������������� �������� � ������ ���������� �������.
�� ����� ���������� ������������� ��������� ������� ��� ������ �������� �������.
\tparam ReturnType ��� ������������� �������� ��������
\tparam ...Args ������ ����� ���������� �������
*/
template<typename ReturnType, typename ... Args>
class addon_function_t<ReturnType( Args ... )> {
public:
	/*! ��� ���������� ������� */
	typedef ReturnType result_type_t;

	/*! �������� �������������� ������� ������� */
	typedef ReturnType( DCF_ADDON_CALL *function_type_t )(Args ...);

private:
	std::shared_ptr<detail::function_anchor_t> _anchor;

	static ReturnType invoke( function_type_t pnt, Args ... args ) {
		CBL_VERIFY( nullptr != pnt );
		return pnt( args ... );
		}

public:
	addon_function_t() {}

	/*! ������������� ������� */
	addon_function_t( const std::shared_ptr<detail::module_anchor_t> & module_,
		const std::string &name )
		: _anchor( std::make_shared<detail::function_anchor_t>( module_, name ) ) {
		
		if( !_anchor )
			CBL_CORE_SHARED_MODULE_THROW_FWD(nullptr);
		}

	/*! �������� ������ ������� �������.
	\param [in] args ������ ���������������� ����������
	\return ReturnType ������������ �������� ��������
	*/
	ReturnType operator()( Args ... args ) {
		return invoke( (function_type_t)CHECK_PTR(_anchor)->function_address(), args ... );
		}

	/*! ��� ������� */
	const std::string& name() const {
		return CHECK_PTR( _anchor )->name();
		}

	/*! ��������� ������� �� ���������� ���������� */
	async_result_t<result_type_t> async_begin( Args ... args ) {
		std::future<result_type_t> f = std::async( std::launch::async,
			&addon_function_t::invoke, (function_type_t)CHECK_PTR( _anchor )->function_address(),
			args ... );

		async_result_t<result_type_t> r( f.share(), CHECK_PTR( _anchor ) );
		return std::move( r );
		}
	};
}
}

