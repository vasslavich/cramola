#pragma once


#include <cstdint>
#include <memory>
#include <list>
#include "./terms.h"
#include "../resources/i_memory_services.h"
#include "../typeframe/i_tpdriver.h"



/// ��� ������ �� ������
using addon_instance_handler_t = uintptr_t;

/// �������������������� �����
#define DCF_INSTANCE_HANDLER_UNDEF			(addon_instance_handler_t(-1))



/*-------------------------------------------------------------------------------------------
��������� ������� ����������� ������� �����
----------------------------------------------------------------------------------------------*/

typedef void* (DCF_ADDON_CALL *platform_service_allocate_memory_t)(
	const unsigned char addonUUID[16], 
	size_t size, 
	const char* traceLine );

typedef void(DCF_ADDON_CALL *platform_service_free_memory_t)(
	void *pMem);

typedef unsigned int(DCF_ADDON_CALL *platform_service_reverse_handler_t)(
	const unsigned char addonUUID[16],
	addon_instance_handler_t hInstance,
	const char* skey,
	void *pMem);


typedef struct reverse_handlers_t {

	/*! ���������� ��������� ������ ������ ���� */
	platform_service_reverse_handler_t invoke;

	}reverse_handlers_t;

typedef struct platform_memory_t {

	/*! ��������� ������ �� ����� */
	platform_service_allocate_memory_t allocate_memory;

	/*! ������������ ������ ����� */
	platform_service_free_memory_t free_memory;

	}platform_memory_t;


/*! ������ �������� ����� ��������� ��� ������������� �� ������� �������. */
typedef struct platform_services_t {

	/*! ������� ���������� ������� */
	platform_memory_t memory_services;

	/*! ������� ������� ��������� ������ */
	reverse_handlers_t reverse_handlers;

	}platform_services_t;

void pl_services_reset( platform_services_t *psrv );
int pl_services_empty( const platform_services_t *psrv );


/*------------------------------------------------------------------------------------------
��������� �������, ������������ ��� ���������� ��������
---------------------------------------------------------------------------------------------*/


#define DCF_ADDON_PROC_NAME_GET_ERR_MSG				"dcf_addon_get_error_message"
#define DCF_ADDON_PROC_NAME_LOAD					"dcf_addon_load"
#define DCF_ADDON_PROC_NAME_UNLOAD					"dcf_addon_unload"
#define DCF_ADDON_PROC_NAME_INSTANCE_CREATE			"dcf_addon_create_instance"
#define DCF_ADDON_PROC_NAME_INSTANCE_DESTROY		"dcf_addon_destroy_instance"


typedef unsigned int(DCF_ADDON_CALL *dcf_addon_get_uuid_t)(
	char *cstrUUID, size_t cstrBufSize);

typedef unsigned int(DCF_ADDON_CALL *dcf_addon_get_error_message_t)(
	unsigned int errorCode, void *buf, 
	size_t bufSize, 
	size_t *wcharMsgLength);

typedef unsigned int(DCF_ADDON_CALL *dcf_module_load_t)(
	platform_services_t *args,
	char *cstrUUID, 
	size_t cstrBufSize);

typedef unsigned int(DCF_ADDON_CALL *dcf_module_uload_t)(
	void *instance);

typedef unsigned int(DCF_ADDON_CALL *dcf_addon_create_instance_t)(
	void **hInstance,
	void *inDtg, 
	void **outDtg);

typedef unsigned int(DCF_ADDON_CALL *dcf_addon_destroy_instance_t)(
	void *hInstance );


#ifdef __cplusplus

namespace crm{
namespace addon{


typedef unsigned int(DCF_ADDON_CALL *dcf_host_common_handler_t)(
	void *hInstance, void *inDtg );

struct host_table_t {
	dcf_host_common_handler_t hndl;
	};

struct i_reverse_handler_t {
	virtual ~i_reverse_handler_t() = 0;

	/*! �������� ����� �� ������ */
	virtual void reverse_invoke( const std::string & skey,
		void *pDtg )noexcept = 0;
	};

struct i_host_services_t : public i_memory_services_t {

	virtual platform_services_t get_host_services()const noexcept = 0;

	static const std::string datagram_value_key_notification;

	virtual void reverse_invoke( addon_instance_handler_t instId,
		const std::string & skey,
		void *pDtg )noexcept = 0;

	virtual void close()noexcept = 0;
	};

/*! ������� ��������� ������������ ������� ������� */
struct i_addon_instance_hndl_t {
	virtual ~i_addon_instance_hndl_t();

	/*! ���������� ������� */
	virtual void close()noexcept = 0;

	/*! ��������� �������������� �������.
	\return \ref addon_instance_handler_t
	*/
	virtual const addon_instance_handler_t& id()const noexcept = 0;

	/*! ������������� ������� */
	virtual const addon_identifier_t& addon_uuid()const noexcept = 0;

	/*! ��������� ���������� ��������� �� ���� ������ */
	virtual std::wstring get_error_message( uint32_t code )const noexcept = 0;

private:

#ifdef CBL_USE_OBJECTS_COUNTER_CHECKER
	dcf::utility::object_count_checker_t<i_addon_instance_hndl_t, 1> _xDbgCounter;
#endif
	};


/*! ������� ��������� ������� */
struct i_addon_hndl_t {
	virtual ~i_addon_hndl_t();

	/*! �������� ������� �� ������� � ������� ��������� �������� */
	virtual void close() = 0;

	/*! ������������� ������� */
	virtual const addon_identifier_t& uuid()const noexcept = 0;

	/*! ��������� ���������� ��������� �� ���� ������ */
	virtual std::wstring get_error_message( uint32_t code )const noexcept = 0;

	/*! ������� ����� ��� ������������� �� ������� ������� */
	virtual std::shared_ptr<i_host_services_t> host_services()const noexcept = 0;

	/*! ���� */
	virtual std::shared_ptr<i_types_driver_t> type_driver()const noexcept = 0;

	/*! ���� ������� */
	virtual std::wstring path()const noexcept = 0;
	};

bool success_addon_function_result( std::uint32_t code );
}
}

#endif// __cplusplus

