#include "../../context/context.h"
#include "../utilities.h"


using namespace crm;
using namespace crm::detail;


#if defined( CBL_MPF_TEST_ERROR_EMULATION) || defined(CBL_MPF_MESSAGE_TRACEWAY_KEYS_BY_ID)

bool crm::is_trait_at_emulation_context( const iol_type_spcf_t &t )noexcept{
	return t.utype() > (decltype(t.utype()))iol_types_t::st_error_proun && !t.fl_report()
		&& t.utype() != (decltype(t.utype()))iol_types_t::st_stream_end
		&& t.utype() != (decltype(t.utype()))iol_types_t::st_stream_disconnection;
	}

bool crm::is_trait_at_emulation_context( const _packet_t & u )noexcept{
	bool r = is_trait_at_emulation_context( u.address() )
		&& is_trait_at_emulation_context( u.type() );

#ifdef CBL_MPF_MESSAGE_TRACEWAY_KEYS_BY_ID
	if( r && !u.is_mark_used() ){
		FATAL_ERROR_FWD(nullptr);
		}
#endif

	return r;
	}

bool crm::is_trait_at_emulation_context( const i_iol_handler_t & u )noexcept{
	return is_trait_at_emulation_context( u.address() )
		&& is_trait_at_emulation_context( u.type() );
	}

bool crm::is_trait_at_emulation_context( const message_frame_unit_t & u )noexcept{
	return is_trait_at_emulation_context( u.unit() );
	}

bool crm::is_trait_at_emulation_context( const outbound_message_unit_t  & u )noexcept{
	return is_trait_at_emulation_context( u.desc().level )
		&& is_trait_at_emulation_context( u.desc().type )
		&& !is_null( u.desc().responseTarget );
	}

bool crm::is_trait_at_emulation_context( const address_descriptor_t & ad )noexcept{
	return is_trait_at_emulation_context( ad.level() ) && is_sendrecv_request( ad );
	}

int crm::trait_at_emulation_context_cast_level( rtl_level_t l, int tag )noexcept{
	if( rtl_level_t::l0 == l ){
		CBL_VERIFY( 0x1000 > tag );
		return 0x1000 | tag;
		}
	else if( rtl_level_t::l1 == l )
		return tag;
	else{
		return std::numeric_limits<int>::max();
		}
	}

bool crm::is_trait_at_emulation_context( rtl_level_t l )noexcept{
	bool f = false;

	if( l == rtl_level_t::l0 ){
	#ifdef CBL_ENABLE_DISCARD_INBOUND_PROCESSING_L0
		f = true;
	#endif
		}
	else if( l == rtl_level_t::l1 ){
	#ifdef CBL_ENABLE_DISCARD_INBOUND_PROCESSING_L1
		f = true;
	#endif
		}

	return f;
	}


std::string crm::trait_at_emulation_context_tracex( const message_frame_unit_t & u )noexcept{
	auto & t = u.unit().address().spin_tag();
	CBL_VERIFY( !is_null( t ) );

	return t.to_str();
	}

std::string crm::trait_at_emulation_context_tracex( const keycounter_trace::key_t & k )noexcept{
	return k.to_str();
	}

std::string crm::trait_at_emulation_context_tracex( const _packet_t & u )noexcept{
	auto & t = u.address().spin_tag();
	CBL_VERIFY( !is_null( t ) );

	return t.to_str();
	}

std::string crm::trait_at_emulation_context_tracex( const i_iol_handler_t & h )noexcept{
	auto & t = h.spin_tag();
	CBL_VERIFY( !is_null( t ) );

	return t.to_str();
	}

std::string crm::trait_at_emulation_context_tracex( const outbound_message_unit_t & ou )noexcept{
	return ou.desc().spitTag.to_str();
	}

std::string  crm::trait_at_emulation_context_tracex( const rt0_x_message_t & ou )noexcept{
	return ou.package().address().spin_tag().to_str();
	}

std::string  crm::trait_at_emulation_context_tracex( const rt0_x_message_unit_t & ou )noexcept{
	return trait_at_emulation_context_tracex( ou.unit() );
	}


keycounter_trace::key_t crm::trait_at_keytrace_set( const keycounter_trace::key_t & k )noexcept{
	return k;
	}

keycounter_trace::key_t crm::trait_at_keytrace_set( const _packet_t & k )noexcept{
	return k.address().spin_tag();
	}

keycounter_trace::key_t crm::trait_at_keytrace_set( const message_frame_unit_t & k )noexcept{
	return k.unit().address().spin_tag();
	}

keycounter_trace::key_t crm::trait_at_keytrace_set( const i_iol_handler_t & k )noexcept{
	return k.spin_tag();
	}

keycounter_trace::key_t crm::trait_at_keytrace_set( const outbound_message_unit_t & k )noexcept{
	return k.desc().spitTag;
	}

keycounter_trace::key_t crm::trait_at_keytrace_set( const address_descriptor_t & k )noexcept{
	return k.spin_tag();
	}

keycounter_trace::key_t crm::trait_at_keytrace_set( const rt0_x_message_t & k )noexcept{
	return trait_at_keytrace_set( k.package() );
	}

keycounter_trace::key_t crm::trait_at_keytrace_set( const rt0_x_message_unit_t & k )noexcept{
	return trait_at_keytrace_set( k.unit() );
	}


#else


std::string trait_at_emulation_context_tracex(const detail::keycounter_trace::key_t& )noexcept { return {}; }
std::string trait_at_emulation_context_tracex(const detail::message_frame_unit_t& )noexcept { return {}; }
std::string trait_at_emulation_context_tracex(const detail::_packet_t& )noexcept { return {}; }
std::string trait_at_emulation_context_tracex(const i_iol_handler_t& )noexcept{ return {}; }
std::string trait_at_emulation_context_tracex(const detail::outbound_message_unit_t& )noexcept { return {}; }
std::string trait_at_emulation_context_tracex(const detail::rt0_x_message_t& )noexcept { return {}; }
std::string trait_at_emulation_context_tracex(const detail::rt0_x_message_unit_t& )noexcept { return {}; }


keycounter_trace::key_t trait_at_keytrace_set(const detail::keycounter_trace::key_t& )noexcept { return {}; }
keycounter_trace::key_t trait_at_keytrace_set(const detail::_packet_t& )noexcept { return {}; }
keycounter_trace::key_t trait_at_keytrace_set(const detail::message_frame_unit_t& )noexcept { return {}; }
keycounter_trace::key_t trait_at_keytrace_set(const i_iol_handler_t& )noexcept { return {}; }
keycounter_trace::key_t trait_at_keytrace_set(const detail::outbound_message_unit_t& )noexcept { return {}; }
keycounter_trace::key_t trait_at_keytrace_set(const address_descriptor_t& )noexcept { return {}; }
keycounter_trace::key_t trait_at_keytrace_set(const detail::rt0_x_message_t& )noexcept { return {}; }
keycounter_trace::key_t trait_at_keytrace_set(const detail::rt0_x_message_unit_t& )noexcept { return {}; }


bool crm::is_trait_at_emulation_context( const iol_type_spcf_t & )noexcept{ return false; }
bool crm::is_trait_at_emulation_context( const _packet_t & )noexcept{ return false; }
bool crm::is_trait_at_emulation_context( const i_iol_handler_t & )noexcept{ return false; }
bool crm::is_trait_at_emulation_context( const message_frame_unit_t & )noexcept{ return false; }
bool crm::is_trait_at_emulation_context( const outbound_message_unit_t  & )noexcept{ return false; }
bool crm::is_trait_at_emulation_context( const address_descriptor_t & )noexcept{ return false; }
int crm::trait_at_emulation_context_cast_level( rtl_level_t, int )noexcept{ return false; }
bool crm::is_trait_at_emulation_context( rtl_level_t )noexcept{ return false; }

#endif



