#include "../erremlt.h"


using namespace crm;
using namespace detail;
using namespace crm::tunetest;



void context_erremlt_increase_modeler::stop()noexcept{
	_actif.store( false );
	}

void context_erremlt_increase_modeler::enable()noexcept{
	_actif.store( true );
	}

errem_generator_mode context_erremlt_increase_modeler::mode()const noexcept{
	return errem_generator_mode::inc;
	}

bool context_erremlt_increase_modeler::modulator()noexcept{
	if( _actif.load() ){
		auto gc = ++counter;
		auto wcBase = gc / counterMaxW;
		auto bH = baseH.load( std::memory_order::memory_order_acquire );
		auto fg = (gc % bH) == 0;
		if( wcBase >= bH ){
			baseH.compare_exchange_strong( bH, bH + baseIncH );
			}

		return fg;
		}
	else
		return false;
	}

