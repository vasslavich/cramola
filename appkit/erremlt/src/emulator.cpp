#include <chrono>
#include <atomic>
#include <cstdint>
#include <iostream>
#include <map>
#include <thread>
#include <mutex>
#include "../erremlt.h"
#include "../../internal.h"


using namespace crm;
using namespace crm::detail;
using namespace crm::tunetest;


i_context_erremlt_modeler::~i_context_erremlt_modeler(){}


#ifdef CBL_MPF_USE_REVERSE_EXCEPTION_TRACE
std::wstring reverse_exception_tracer::__trace()const noexcept{
	std::wostringstream ss;

	ss << L"reverser exception trace =============== begin:" << std::endl;
	for( const auto & ik : _clst ){
		ss << ik.first << ":" << ik.second << std::endl;
		}
	ss << L"reverser exception trace ================= end" << std::endl;

	return ss.str();
	}

void reverse_exception_tracer::add( const std::wstring & key )noexcept{
	std::unique_lock<decltype(_lck)> lck( _lck );
	const auto icv = ++_clst[key];

#ifdef CBL_MPF_TRACELEVEL_EMULATOR_COUNTERS
	if( icv < 3 || (icv % 100) == 1 ){

		auto tr = __trace();
		lck.unlock();

		file_logger_t::logging( tr, CBL_MPF_TEST_EMULATION_IN_CONTEXT_TAG );
		}
#endif
	}

std::wstring reverse_exception_tracer::get_reverse_exception_trace()const noexcept{
	std::unique_lock<decltype(_lck)> lck( _lck );
	return __trace();
	}
#endif

