#include <sstream>
#include "../testerrem_counter.h"
#include "../../internal.h"


using namespace crm;
using namespace crm::tunetest;


testerrem_counter::testerrem_counter()
	: _clst(max_count + 1, 0) {}

size_t testerrem_counter::size()const noexcept {
	return _clst.size();
	}

std::string testerrem_counter::trace()const noexcept {
	std::ostringstream cl;
	for(size_t i = 0; i < _clst.size(); ++i) {
		auto iv = _clst[i];
		if(iv) {
			cl << std::hex << i << ":" << std::dec << iv << ",";
			}
		}

	return cl.str();
	}

void testerrem_counter::inc(size_t indx) {
	CBL_VERIFY(indx < _clst.size());
	auto iv = ++_clst.at(indx);

#ifdef CBL_MPF_TRACELEVEL_EMULATOR_COUNTERS
	if((iv % 10) == 1) {

		std::string cl = "testerrem-counter:==================== begin\n";
		cl += trace() + "\ntesterrem-counter:==================== end\n";

		crm::file_logger_t::logging(cl, CBL_MPF_TEST_EMULATION_IN_CONTEXT_TAG);
		}
#else
	iv;
#endif
	}
