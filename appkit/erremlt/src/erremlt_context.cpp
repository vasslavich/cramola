#include "../../internal.h"
#include "../erremlt_context.h"


using namespace crm;
using namespace crm::tunetest;
using namespace crm::detail;


err_set_t::err_set_t(errem_generator_mode mode, const errem_points_t& points)
	: _points(points)
	, _mode(mode)
	, _intervalCounter(_points.l)
	, _lastPnt(std::chrono::duration_cast<measurement_time_t>(std::chrono::system_clock::now().time_since_epoch()).count()) {}


measurement_time_base_t err_set_t::last_point()const noexcept {
	return _lastPnt.load(std::memory_order::memory_order_acquire);
	}

void err_set_t::set_last_point(measurement_time_base_t currCheck, measurement_time_base_t val)noexcept {
	_lastPnt.compare_exchange_strong(currCheck, val);
	}

measurement_time_base_t next_point_rand(measurement_time_base_t counter,
	measurement_time_base_t l,
	measurement_time_base_t u) {

	auto b = counter % u;
	if(b >= l)
		return b;
	else
		return l;
	}

bool err_set_t::next_check()noexcept {
	if(errem_generator_mode::none == _mode) {
		return false;
		}
	else {
		auto currTP = std::chrono::system_clock::now();
		auto valCnt = std::chrono::duration_cast<measurement_time_t>(currTP.time_since_epoch()).count();
		auto lastCnt = last_point();

		if(valCnt > last_point()) {

			if(errem_generator_mode::inc == _mode) {
				_intervalCounter += _points.inc;
				}
			else if(errem_generator_mode::rand == _mode) {
				auto rv = sx_uuid_t::rand().u64_2[0];
				if((rv % 3) == 0)
					_intervalCounter += (sx_uuid_t::rand().u64_2[0] + _intervalCounter) % _points.u;
				else
					_intervalCounter += _points.inc;
				}
			else {
				FATAL_ERROR_FWD(nullptr);
				}

			set_last_point(lastCnt, valCnt + next_point_rand(_intervalCounter, _points.l, _points.u));
			return true;
			}
		else {
			return false;
			}
		}
	}




std::string erremlt_context::get_errormod_counters_s()const noexcept {
	return _checkthrowCnt.trace();
	}

std::unique_ptr<i_context_erremlt_modeler> errem_modeler(errem_generator_mode m)noexcept {
	switch(m) {
			case errem_generator_mode::none:
				return nullptr;
			case errem_generator_mode::rand:
				return std::make_unique<context_erremlt_rand_modeler>();
				break;
			case errem_generator_mode::inc:
				return std::make_unique<context_erremlt_increase_modeler>();
				break;
			default:
				FATAL_ERROR_FWD(nullptr);
				break;
		}
	}

erremlt_context::erremlt_context(const distributed_ctx_policies_t& options)
	: _erremode(options.errmode())
	, _erremd(errem_modeler(options.errmode()))

#ifdef CBL_ENABLE_TEST_ERROR_EMULATION_LEVEL_0
	, _errConnection0(std::make_unique<err_set_t>(options.errmode_connection_corrupt(rtl_level_t::l0),
		options.errem_points(rtl_level_t::l0)))
#endif
#ifdef CBL_ENABLE_TEST_ERROR_EMULATION_LEVEL_1
	, _errConnection1(std::make_unique< err_set_t>(options.errmode_connection_corrupt(rtl_level_t::l1),
		options.errem_points(rtl_level_t::l1)))
#endif
	{}

bool erremlt_context::test_emulation_error()const noexcept {
	if(_erremd) {
		return _erremd->modulator();
		}
	else {
		return false;
		}
	}

void erremlt_context::disable()noexcept {
	_erremd->stop();
	}

void erremlt_context::enable()noexcept {
	_erremd->enable();
	}

void erremlt_context::chechthrow_emulation_error(int tag, rtl_level_t l, const std::wstring& file, int line)const {
	if(is_trait_at_emulation_context(l)) {
		chechthrow_emulation_error(trait_at_emulation_context_cast_level(l, tag), file, line);
		}
	}

void erremlt_context::chechthrow_emulation_error(int tag, const std::wstring& file, int line)const {
	chechthrow_emulation_error(tag, std::string{}, file, line);
	}

void erremlt_context::chechthrow_emulation_error(int tag, rtl_level_t l, std::string stracex, const std::wstring& file, int line)const {
	if(is_trait_at_emulation_context(l)) {
		chechthrow_emulation_error(trait_at_emulation_context_cast_level(l, tag), stracex, file, line);
		}
	}

void erremlt_context::chechthrow_emulation_error(int tag, std::string stracex, const std::wstring& file, int line)const {
	if(test_emulation_error()) {
		_checkthrowCnt.inc(tag);
		throw mpf_test_exception_t(std::move(stracex), file, line);
		}
	}

void erremlt_context::chechthrow_emulation_error(int tag,
	rtl_level_t l, const keycounter_trace::key_t& ktracex, const std::wstring& file, int line)const {

	if(is_trait_at_emulation_context(l)) {
		chechthrow_emulation_error(trait_at_emulation_context_cast_level(l, tag), ktracex, file, line);
		}
	}

void erremlt_context::chechthrow_emulation_error(int tag,
	const keycounter_trace::key_t& ktracex, const std::wstring& file, int line)const {

	if(test_emulation_error()) {
		_checkthrowCnt.inc(tag);

		CBL_MPF_KEYTRACE_SET(ktracex, 500);
		throw mpf_test_exception_t(std::move(ktracex.to_str()), file, line);
		}
	}

bool erremlt_context::test_emulation_error_connection(rtl_level_t l)const noexcept{
	l;

#ifdef CBL_ENABLE_TEST_ERROR_EMULATION_LEVEL_0
	if(l == rtl_level_t::l0) {
		return _errConnection0->next_check();
		}
#endif

#ifdef CBL_ENABLE_TEST_ERROR_EMULATION_LEVEL_1
	if(l == rtl_level_t::l1) {
		return _errConnection1->next_check();
		}
#endif

	return false;
	}



