#include "../../internal.h"
#include "../erremlt.h"
#include "../../utilities/identifiers/uuid.h"


using namespace crm;
using namespace crm::tunetest;
using namespace crm::detail;


const std::uintmax_t context_erremlt_rand_modeler::DistanceMax = 10000;
const std::uintmax_t context_erremlt_increase_modeler::counterMaxW = 10;
const std::uintmax_t context_erremlt_increase_modeler::baseIncH = 10;


void context_erremlt_rand_modeler::stop()noexcept{
	_actif.store( false );
	}

void context_erremlt_rand_modeler::enable()noexcept{
	_actif.store( true );
	}

errem_generator_mode context_erremlt_rand_modeler::mode()const noexcept{
	return errem_generator_mode::rand;
	}

bool context_erremlt_rand_modeler::modulator()noexcept{
	if( _actif.load() ){

		auto currBase = DistanceBase.load( std::memory_order::memory_order_acquire );
		auto hasError = (Counter.fetch_add( 1 ) % currBase) == 0;

		if( hasError ){
			const auto inc_step_rnd = sx_uuid_t::rand().u64_2[1] % DistanceMax;
			const auto inc_step = inc_step_rnd > 0 ? inc_step_rnd : 1;
			const decltype(currBase) nextBaseOff = (currBase + inc_step) % DistanceMax;
			const auto nextBase = nextBaseOff > 0 ? nextBaseOff : 1;

			DistanceBase.compare_exchange_strong(currBase, nextBase);
			}

		return hasError;
		}
	else
		return false;
	}

