#pragma once


#include "../erremlt/utilities.h"
#include "../erremlt/testerrem_counter.h"


namespace crm::tunetest {


class err_set_t {
public:
	using measurement_time_base_t = measurement_time_base_t;

private:
	const errem_points_t _points;
	const errem_generator_mode _mode{ errem_generator_mode::none };
	std::atomic<measurement_time_base_t> _intervalCounter;
	std::atomic<measurement_time_base_t> _lastPnt;

public:
	err_set_t(errem_generator_mode mode, const errem_points_t& points);

	measurement_time_base_t last_point()const noexcept;
	void set_last_point(measurement_time_base_t currCheck,
		measurement_time_base_t val) noexcept;

	bool next_check() noexcept;
	};

class erremlt_context {
private:
	errem_generator_mode _erremode{ errem_generator_mode::none };
	std::unique_ptr<i_context_erremlt_modeler> _erremd;
	mutable testerrem_counter _checkthrowCnt;

#ifdef CBL_ENABLE_TEST_ERROR_EMULATION_LEVEL_0
	std::unique_ptr<err_set_t> _errConnection0;
#endif
#ifdef CBL_ENABLE_TEST_ERROR_EMULATION_LEVEL_1
	std::unique_ptr<err_set_t> _errConnection1;
#endif

public:
	erremlt_context(const distributed_ctx_policies_t& options );

	void disable()noexcept;
	void enable()noexcept;

	bool test_emulation_error()const noexcept;
	bool test_emulation_error_connection(detail::rtl_level_t)const noexcept;

	void chechthrow_emulation_error(int tag, detail::rtl_level_t l, const std::wstring& file, int line)const;
	void chechthrow_emulation_error(int tag, const std::wstring& file, int line)const;

	void chechthrow_emulation_error(int tag, detail::rtl_level_t l, std::string stracex, const std::wstring& file, int line)const;
	void chechthrow_emulation_error(int tag, std::string stracex, const std::wstring& file, int line)const;

	void chechthrow_emulation_error(int tag, detail::rtl_level_t l, const detail::keycounter_trace::key_t& ktracex, const std::wstring& file, int line)const;
	void chechthrow_emulation_error(int tag, const detail::keycounter_trace::key_t& ktracex, const std::wstring& file, int line)const;

	std::string get_errormod_counters_s()const noexcept;
	};
}
