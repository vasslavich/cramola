#pragma once


#include <functional>
#include <atomic>
#include <mutex>
#include <map>
#include "../internal_invariants.h"


namespace crm{
namespace tunetest {


#ifdef CBL_MPF_USE_REVERSE_EXCEPTION_TRACE
class reverse_exception_tracer {
private:
	mutable std::mutex _lck;
	std::map<std::wstring, std::uintmax_t> _clst;

	std::wstring __trace()const noexcept;

public:
	void add(const std::wstring & key)noexcept;
	std::wstring get_reverse_exception_trace()const noexcept;
	};
#endif


struct i_context_erremlt_modeler {
	virtual ~i_context_erremlt_modeler() = 0;

	virtual errem_generator_mode mode()const noexcept = 0;
	virtual bool modulator()noexcept = 0;
	virtual void stop()noexcept = 0;
	virtual void enable()noexcept = 0;
	};


struct context_erremlt_rand_modeler final : public i_context_erremlt_modeler {
	static const std::uintmax_t DistanceMax;
	std::atomic<std::uintmax_t> DistanceBase{ DistanceMax };
	std::atomic<std::uintmax_t> Counter{ 1 };
	std::atomic<bool> _actif{ true };

	errem_generator_mode mode()const noexcept final;
	bool modulator()noexcept final;
	void stop()noexcept final;
	void enable()noexcept final;
	};

struct context_erremlt_increase_modeler final : public i_context_erremlt_modeler {
	static const std::uintmax_t counterMaxW;
	static const std::uintmax_t baseIncH;
	std::atomic<std::uintmax_t> baseH{ 1 };
	std::atomic<std::uintmax_t> counter{ 0 };
	std::atomic<bool> _actif{ true };

	errem_generator_mode mode()const noexcept final;
	bool modulator()noexcept final;
	void stop()noexcept final;
	void enable()noexcept final;
	};
}
}

