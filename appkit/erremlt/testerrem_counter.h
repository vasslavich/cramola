#pragma once


#include <vector>


namespace crm {
namespace tunetest {


class testerrem_counter {
	std::vector<size_t> _clst;

public:
	static constexpr size_t max_count = 8192;

	testerrem_counter();

	std::string trace()const noexcept;
	void inc(size_t indx);
	size_t size()const noexcept;
	};
}
}


