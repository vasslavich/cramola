#pragma once


#include "../build_config.h"
#include "../base_predecl.h"
#include "./erremlt.h"


namespace crm{



#ifdef CBL_MPF_TEST_EMULATION_IN_CONTEXT


#define MPF_TEST_EMULATION_IN_CONTEXT_L_COND(ctx, level, tag, cond) {\
if((cond)){\
static_assert((tag) <= tunetest::testerrem_counter::max_count, __FILE__ );\
(ctx)->chechthrow_emulation_error((tag), (level), __CBL_FILEW__, __LINE__);\
}\
}

#define MPF_TEST_EMULATION_IN_CONTEXT_COND(ctx, tag, cond) {\
if((cond)){\
static_assert((tag) <= tunetest::testerrem_counter::max_count, __FILE__ );\
(ctx)->chechthrow_emulation_error((tag), __CBL_FILEW__, __LINE__);\
}\
}


#define MPF_TEST_EMULATION_IN_CONTEXT_L_COND_TRCX(ctx, level, tag, cond, tracex) {\
if((cond)){\
static_assert((tag) <= tunetest::testerrem_counter::max_count, __FILE__ );\
(ctx)->chechthrow_emulation_error((tag), (level), (tracex), __CBL_FILEW__, __LINE__);\
}\
}

#define MPF_TEST_EMULATION_IN_CONTEXT_COND_TRCX(ctx, tag, cond, tracex) {\
if((cond)){\
static_assert((tag) <= tunetest::testerrem_counter::max_count, __FILE__ );\
(ctx)->chechthrow_emulation_error((tag), (tracex), __CBL_FILEW__, __LINE__);\
}\
}



#else

#define MPF_TEST_EMULATION_IN_CONTEXT_L_COND(ctx, level, tag, cond)						CBL_MPF_UNUSED_VARIABLE_COMPILER_MKREF((level))
#define MPF_TEST_EMULATION_IN_CONTEXT_COND(ctx, tag, cond)								CBL_MPF_UNUSED_VARIABLE_COMPILER_MKREF((tag))
#define MPF_TEST_EMULATION_IN_CONTEXT_L_COND_TRCX(ctx, level, tag, cond, tracex)		CBL_MPF_UNUSED_VARIABLE_COMPILER_MKREF((level))
#define MPF_TEST_EMULATION_IN_CONTEXT_COND_TRCX(ctx, tag, cond, tracex)					CBL_MPF_UNUSED_VARIABLE_COMPILER_MKREF((tag))
#endif


#ifdef CBL_MPF_TEST_EMULATION_IN_CONTEXT_EXPR
#define MPF_TEST_EMULATION_IN_CONTEXT_EXPR_COND(ctx, tag, cond) {\
if((cond)){\
static_assert((tag) <= tunetest::testerrem_counter::max_count, __FILE__ );\
(ctx)->chechthrow_emulation_error((tag), __CBL_FILEW__, __LINE__);\
}\
}
#else
#define MPF_TEST_EMULATION_IN_CONTEXT_EXPR_COND(ctx, tag, cond)							CBL_MPF_UNUSED_VARIABLE_COMPILER_MKREF((tag))
#endif


#ifdef CBL_MPF_TEST_EMULATION_IN_CONTEXT_O_PEER_MESSAGE
#define MPF_TEST_EMULATION_IN_CONTEXT_O_PEER_MESSAGE_COND(ctx, tag, cond) {\
if((cond)){\
static_assert((tag) <= tunetest::testerrem_counter::max_count, __FILE__ );\
(ctx)->chechthrow_emulation_error((tag), __CBL_FILEW__, __LINE__);\
}\
}
#else
#define MPF_TEST_EMULATION_IN_CONTEXT_O_PEER_MESSAGE_COND(ctx, tag, cond)				CBL_MPF_UNUSED_VARIABLE_COMPILER_MKREF((tag))
#endif


#ifdef CBL_MPF_TEST_EMULATION_IN_CONTEXT_I_PEER_MESSAGE
#define MPF_TEST_EMULATION_IN_CONTEXT_I_PEER_MESSAGE_COND(ctx, tag, cond) {\
if((cond)){\
static_assert((tag) <= tunetest::testerrem_counter::max_count, __FILE__ );\
(ctx)->chechthrow_emulation_error((tag), __CBL_FILEW__, __LINE__);\
}\
}
#else
#define MPF_TEST_EMULATION_IN_CONTEXT_I_PEER_MESSAGE_COND(ctx, tag, cond)				CBL_MPF_UNUSED_VARIABLE_COMPILER_MKREF((tag))
#endif


bool is_trait_at_emulation_context(const i_iol_handler_t& u)noexcept;
bool is_trait_at_emulation_context(const detail::_packet_t& u)noexcept;
bool is_trait_at_emulation_context(const detail::message_frame_unit_t&)noexcept;
bool is_trait_at_emulation_context(const detail::outbound_message_unit_t&)noexcept;
bool is_trait_at_emulation_context(detail::rtl_level_t l)noexcept;
bool is_trait_at_emulation_context(const iol_type_spcf_t& t)noexcept;
bool is_trait_at_emulation_context(const address_descriptor_t& ad)noexcept;
int trait_at_emulation_context_cast_level(detail::rtl_level_t l, int tag)noexcept;


std::string trait_at_emulation_context_tracex( const detail::keycounter_trace::key_t & k )noexcept;
std::string trait_at_emulation_context_tracex( const detail::message_frame_unit_t & u )noexcept;
std::string trait_at_emulation_context_tracex( const detail::_packet_t & u )noexcept;
std::string trait_at_emulation_context_tracex( const i_iol_handler_t & h )noexcept;
std::string trait_at_emulation_context_tracex( const detail::outbound_message_unit_t & ou )noexcept;
std::string trait_at_emulation_context_tracex( const detail::rt0_x_message_t & ou )noexcept;
std::string trait_at_emulation_context_tracex( const detail::rt0_x_message_unit_t & ou )noexcept;


detail::keycounter_trace::key_t trait_at_keytrace_set( const detail::keycounter_trace::key_t & k )noexcept;
detail::keycounter_trace::key_t trait_at_keytrace_set( const detail::_packet_t & k )noexcept;
detail::keycounter_trace::key_t trait_at_keytrace_set( const detail::message_frame_unit_t & k )noexcept;
detail::keycounter_trace::key_t trait_at_keytrace_set( const i_iol_handler_t & k )noexcept;
detail::keycounter_trace::key_t trait_at_keytrace_set( const detail::outbound_message_unit_t & k )noexcept;
detail::keycounter_trace::key_t trait_at_keytrace_set( const address_descriptor_t & k )noexcept;
detail::keycounter_trace::key_t trait_at_keytrace_set( const detail::rt0_x_message_t & ou )noexcept;
detail::keycounter_trace::key_t trait_at_keytrace_set( const detail::rt0_x_message_unit_t & ou )noexcept;
}

