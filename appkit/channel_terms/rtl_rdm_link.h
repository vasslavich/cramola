#pragma once


#include "./base_terms.h"
#include "./session_desc.h"


namespace crm::detail{


/*! ������� ����� �������� ����� �������������� ��������� */
struct rtl_roadmap_link_t final:
	public hf_operators_t<rtl_roadmap_link_t>{

public:
	static const rtl_roadmap_link_t null;

private:
	/*! ������� */
	identity_descriptor_t _targetId;
	/*! �������� */
	identity_descriptor_t _sourceId;
	/*! ��������� ������ */
	session_description_t _session;
	/*! ��� ������� */
	address_hash_t _hash;

public:
	using hash_t = address_hash_t;

	/*! ����� r [�� ��������� �����]*/
	bool equals_fields(const rtl_roadmap_link_t& r)const noexcept;
	/*! ������ ��� r [�� ��������� �����]*/
	bool less_fields(const rtl_roadmap_link_t& r)const noexcept;
	/*! ������ ��� r [�� ��������� �����]*/
	bool great_fields(const rtl_roadmap_link_t& r)const noexcept;
	/*! ������ ��� ����� r [�� ��������� �����]*/
	bool great_equals_fields(const rtl_roadmap_link_t& r)const noexcept;

public:
	rtl_roadmap_link_t()noexcept;
	rtl_roadmap_link_t( const identity_descriptor_t &targetId,
		const identity_descriptor_t & source,
		const session_description_t & session)noexcept;

	const address_hash_t& object_hash()const noexcept;
	const session_description_t& session()const noexcept;
	const identity_descriptor_t& source_id()const noexcept;
	const identity_descriptor_t& target_id()const noexcept;
	std::string to_str()const noexcept;
	bool is_null()const noexcept;

	template<typename Ts>
	void srlz(Ts && dest)const{
		srlz::serialize(dest, _sourceId);
		srlz::serialize(dest, _targetId);
		srlz::serialize(dest, _session);
		srlz::serialize(dest, _hash);
		}

	template<typename Ts>
	void dsrlz(Ts && src ){
		srlz::deserialize(src, _sourceId);
		srlz::deserialize(src, _targetId);
		srlz::deserialize(src, _session);
		srlz::deserialize(src, _hash);
		}

	size_t get_serialized_size()const noexcept;
	};
}