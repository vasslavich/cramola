#pragma once


#include <memory>
#include <functional>
#include "./predecl.h"
#include "../base_predecl.h"
#include "../tableval/tableval.h"
#include "./terms.h"
#include "./crc.h"
#include "./message_bitmask.h"
#include "./rtl_rdm_event_desc.h"
#include "./rtl_rdm_link.h"
#include "./message_attribute_trait.h"
#include "./message_quant.h"
#include "./stream_wait_key.h"
#include "./message_frame.h"
#include "./subscribe_terms.h"
#include "./subscribe_event_addr.h"
#include "./i_events.h"
#include "./i_stream.h"
#include "./rtl_rdm_object.h"
#include "../exceptions.h"


namespace crm::detail{


rtl_roadmap_obj_t make_message( const rt0_node_rdm_t & rt0,
	const i_iol_ohandler_t & message );
rtl_roadmap_obj_t make_message( const rt0_node_rdm_t & rt0,
	const i_iol_ihandler_t& message );


size_t get_packet_idx( const message_frame_unit_t & )noexcept;
size_t get_packet_idx( const i_iol_handler_t & )noexcept;
size_t get_packet_idx( const address_descriptor_t & )noexcept;



typedef std::function<bool( connection_stage_t stage, const std::unique_ptr<dcf_exception_t> & )> exceptions_suppress_checker_t;



/*! ��������� ������������� */
struct initialize_result_desc_t{
	std::unique_ptr<dcf_exception_t> exception;
	datagram_t responseValue;

	initialize_result_desc_t();
	initialize_result_desc_t( std::unique_ptr<dcf_exception_t> && exc )noexcept;
	initialize_result_desc_t( crm::datagram_t && value );

	initialize_result_desc_t( const initialize_result_desc_t & ) = delete;
	initialize_result_desc_t& operator=( const initialize_result_desc_t & ) = delete;

	initialize_result_desc_t( initialize_result_desc_t  && );
	initialize_result_desc_t& operator=( initialize_result_desc_t && );
	};

struct object_initialize_invoker{
	virtual ~object_initialize_invoker(){}

	virtual void operator()( std::unique_ptr<dcf_exception_t> && exc )noexcept = 0;
	virtual void operator()( crm::datagram_t && value ) = 0;

	virtual void invoke( std::unique_ptr<dcf_exception_t> && exc )noexcept = 0;
	virtual void invoke( crm::datagram_t && value ) = 0;

	virtual operator bool()const noexcept = 0;
	};

template<typename _THandler,
	typename THandler = typename std::decay_t<_THandler>,
	typename std::enable_if_t<std::is_invocable_v<THandler, typename std::add_rvalue_reference_t<initialize_result_desc_t>>, int> = 1>
	class object_initialize_result_guard_ptr_t final : public object_initialize_invoker{
	private:
		THandler _f;
		std::weak_ptr<distributed_ctx_t> _ctx;
		async_space_t _scx;
		inovked_as _invas;
		std::atomic<bool> _invokedf{ false };

		template<typename ...TResultValues>
		void __invoke( TResultValues && ... result )noexcept{

			bool isDo = false;
			if( _invokedf.compare_exchange_strong( isDo, true ) ){
				if constexpr( is_bool_cast_v<decltype(_f)> ){
					CBL_VERIFY( _f );
					}

				if( auto c = _ctx.lock() ){
					auto tailf = std::move( _f );

					if constexpr( sizeof ...(result) ){
						if( detail::inovked_as::async == _invas ){
							CHECK_NO_EXCEPTION( c->launch_async( _scx, __FILE_LINE__, std::move( tailf ),
								initialize_result_desc_t( std::forward<TResultValues>( result )... ) ) );
							}
						else{
							tailf( initialize_result_desc_t( std::forward<TResultValues>( result )... ) );
							}
						}
					else{
						c->launch_async( __FILE_LINE__, std::move( tailf ), initialize_result_desc_t( CREATE_MPF_PTR_EXC_FWD(nullptr) ) );
						}
					}
				}
			}

	public:
		template<typename _XHandler,
			typename XHandler = typename std::decay_t<_XHandler>,
			typename std::enable_if_t<std::is_invocable_v<XHandler, typename std::add_rvalue_reference_t<initialize_result_desc_t>>, int> = 1>
			object_initialize_result_guard_ptr_t( std::weak_ptr<distributed_ctx_t> ctx_,
				async_space_t scx,
				inovked_as as_,
				_XHandler && f_ )noexcept
			: _f( std::forward<_XHandler>( f_ ) )
			, _ctx( std::move( ctx_ ) )
			, _scx( scx )
			, _invas( as_ ){}

		object_initialize_result_guard_ptr_t( const object_initialize_result_guard_ptr_t & ) = delete;
		object_initialize_result_guard_ptr_t& operator=( const object_initialize_result_guard_ptr_t & ) = delete;

		template<typename XHandler>
		object_initialize_result_guard_ptr_t( object_initialize_result_guard_ptr_t<XHandler> && o )noexcept
			: _f( std::move( o._f ) )
			, _ctx( std::move( o._ctx ) )
			, _scx( o._scx )
			, _invas( o._invas )
			, _invokedf( o._invokedf.exchange( true ) ){}

		template<typename XHandler>
		object_initialize_result_guard_ptr_t& operator=( object_initialize_result_guard_ptr_t<XHandler> && o )noexcept{
			if( this != std::addressof( o ) ){
				_f = std::move( o._f );
				_ctx = std::move( o._ctx );
				_scx = o._scx;
				_invas = o._invas;
				_invokedf.store( o._invokedf.exchange( true ) );
				}

			return (*this);
			}

		~object_initialize_result_guard_ptr_t(){
			CHECK_NO_EXCEPTION( __invoke() );
			}


		void invoke( std::unique_ptr<dcf_exception_t> && exc )noexcept final{
			__invoke( std::move( exc ) );
			}

		void invoke( crm::datagram_t && value )final{
			__invoke( std::move( value ) );
			}

		void operator()( crm::datagram_t && value ) final{
			__invoke( std::move( value ) );
			}

		void operator()( std::unique_ptr<dcf_exception_t> && exc )noexcept final{
			__invoke( std::move( exc ) );
			}

		operator bool()const noexcept final{
			if constexpr( is_bool_cast_v<decltype(_f)> ){
				return !_invokedf.load( std::memory_order::memory_order_acquire ) && _f;
				}
			else{
				return !_invokedf.load( std::memory_order::memory_order_acquire );
				}
			}
	};



class object_initialize_result_guard_t{
private:
	std::unique_ptr<object_initialize_invoker> _p;

public:
	template<typename _XHandler,
		typename XHandler = typename std::decay_t<_XHandler>,
		typename std::enable_if_t<std::is_invocable_v<XHandler, typename std::add_rvalue_reference_t<initialize_result_desc_t>>, int> = 1>
		object_initialize_result_guard_t( std::weak_ptr<distributed_ctx_t> ctx_, async_space_t scx, inovked_as as_, _XHandler && f_ )noexcept
		: _p( std::make_unique < object_initialize_result_guard_ptr_t<_XHandler>>( std::move( ctx_ ), scx, as_, std::forward<_XHandler>( f_ ) ) ){}

	object_initialize_result_guard_t( const object_initialize_result_guard_t & ) = delete;
	object_initialize_result_guard_t& operator=( const object_initialize_result_guard_t & ) = delete;

	object_initialize_result_guard_t( object_initialize_result_guard_t && o )noexcept;
	object_initialize_result_guard_t& operator=( object_initialize_result_guard_t && o )noexcept;

	void invoke( std::unique_ptr<dcf_exception_t> && exc )noexcept;
	void invoke( datagram_t && value );

	void operator()( std::unique_ptr<dcf_exception_t> && exc )noexcept;
	void operator()( datagram_t && value );

	operator bool()const noexcept;
	};


/*! ������� ��������� ��������� ������������� �������� ����� */
typedef std::function<void( datagram_t && dt,
	object_initialize_result_guard_t && resultHndl )> object_initialize_handler_t;

static const std::string remote_object_initialize_response;


struct execution_bool_tail{
	virtual ~execution_bool_tail(){}
	virtual void operator()( bool )noexcept = 0;
	virtual void commite()noexcept = 0;
	};

template<typename _THandler,
	typename THandler = typename std::decay_t<_THandler>,
	typename std::enable_if_t<std::is_invocable_v<THandler, bool>, int> = 0>
	struct register_in_connection_completed_t final : execution_bool_tail{
	private:
		using self_t = register_in_connection_completed_t<THandler>;

		std::weak_ptr<distributed_ctx_t> _ctx;
		THandler _hndl;
		std::atomic<bool> _invokef = false;

		void _invoke( bool r )noexcept{
			bool passf = false;
			if( _invokef.compare_exchange_strong( passf, true ) ){
				if( auto c = _ctx.lock() ){
					CHECK_NO_EXCEPTION( c->launch_async( __FILE_LINE__, std::move( _hndl ), r ) );
					}
				}
			}

	public:
		template<typename XHandler,
			typename std::enable_if_t<std::is_invocable_v<decltype(std::declval<XHandler>()), bool>, int> = 0>
			register_in_connection_completed_t( std::shared_ptr<distributed_ctx_t> ctx,
				XHandler && xhndl )noexcept
			: _ctx( std::move( ctx ) )
			, _hndl( std::forward< XHandler>( xhndl ) ){}

		~register_in_connection_completed_t(){
			CHECK_NO_EXCEPTION( _invoke( false ) );
			}

		register_in_connection_completed_t( const register_in_connection_completed_t & ) = delete;
		register_in_connection_completed_t& operator=( const register_in_connection_completed_t & ) = delete;

		void commite()noexcept final{
			_invoke( true );
			}

		void operator()( bool r ) noexcept final{
			_invoke( r );
			}
	};


template<class T>
register_in_connection_completed_t( std::shared_ptr<distributed_ctx_t> ctx,
	T && x )->register_in_connection_completed_t<T>;

template<typename T>
decltype(auto) register_in_connection_completed_make_ptr( std::shared_ptr<distributed_ctx_t> ctx, T && x ){
	return std::make_unique< register_in_connection_completed_t<T>>( std::move( ctx ), std::forward<T>( x ) );
	}


template<typename TXPeer>
struct __outbound_connection_result_t{
public:
	using connection_result_t = crm::utility::invoke_functor_shared<void, TXPeer &&,
		async_operation_result_t,
		std::unique_ptr<dcf_exception_t> &&,
		reconnect_setup_t>;

private:
	std::weak_ptr<distributed_ctx_t> _ctx;
	connection_result_t _hndl;
	std::atomic<bool> _invokedf = false;

#ifdef CBL_MPF_TRACELEVEL_EVENT_CONNECTION
	std::wstring _tag;
#endif


	bool is_invoke()noexcept{
		bool infokedf = false;
		return _invokedf.compare_exchange_strong( infokedf, true );
		}

	auto invoker()noexcept{
		decltype(_hndl) invoker;
		std::swap( invoker, _hndl );

		return invoker;
		}

	void __invoke_ready( TXPeer && obj ){
		if( is_invoke() ){
			if( auto f = invoker() ){
				f( std::move( obj ),
					async_operation_result_t::st_ready,
					nullptr,
					detail::reconnect_setup_t::yes );
				}
			else{
				FATAL_ERROR_FWD(nullptr);
				}
			}
		}

	void __invoke_error( async_operation_result_t r,
		std::unique_ptr<dcf_exception_t> && exc,
		reconnect_setup_t rc )noexcept{

		if (is_invoke()) {
			if (auto f = invoker()) {
				if (auto c = _ctx.lock()) {
					if (!exc) {
#ifdef CBL_MPF_TRACELEVEL_EVENT_CONNECTION
						exc = CREATE_MPF_PTR_EXC_FWD(_tag);
#else
						exc = CREATE_PTR_EXC_FWD(nullptr);
#endif
						}
					else {
#ifdef CBL_MPF_TRACELEVEL_EVENT_CONNECTION
						exc->add_message_line(std::move(_tag));
#endif
						}

					c->launch_async(async_space_t::sys, __FILE_LINE__, std::move(f), TXPeer(), r, std::move(exc), rc);
					}
				}
			else {
				FATAL_ERROR_FWD(nullptr);
				}
			}
		}

public:
	__outbound_connection_result_t(std::weak_ptr<distributed_ctx_t> ctx,
		connection_result_t && hndl

#ifdef CBL_MPF_TRACELEVEL_EVENT_CONNECTION
		, std::wstring_view tag_
#endif

	)noexcept
		: _ctx(ctx)
		, _hndl(std::move(hndl))

#ifdef CBL_MPF_TRACELEVEL_EVENT_CONNECTION
		, _tag(tag_)
#endif
		{

		CBL_VERIFY(_hndl);
		}

	void close()noexcept{
		CHECK_NO_EXCEPTION( __invoke_error( async_operation_result_t::st_abandoned,
			nullptr,
			reconnect_setup_t::yes ) );
		}

	void refuse( std::unique_ptr<dcf_exception_t> && exc, reconnect_setup_t rc )noexcept{
		CHECK_NO_EXCEPTION( __invoke_error( async_operation_result_t::st_exception,
			std::move( exc ),
			rc ) );
		}

	void ready( TXPeer && obj ){
		__invoke_ready( std::move( obj ) );
		}

	~__outbound_connection_result_t(){
		CHECK_NO_EXCEPTION( close() );
		}

	__outbound_connection_result_t( const __outbound_connection_result_t & ) = delete;
	__outbound_connection_result_t& operator=( const __outbound_connection_result_t & ) = delete;

	__outbound_connection_result_t( __outbound_connection_result_t && ) = default;
	__outbound_connection_result_t& operator=( __outbound_connection_result_t && ) = default;
	};


template<typename TXPeer>
struct __inbound_connection_result_t{
public:
	using identification_success_f = crm::utility::invoke_functor_shared<void, std::unique_ptr<dcf_exception_t> &&,
		TXPeer &&,
		syncdata_list_t &&,
		std::unique_ptr<execution_bool_tail> &&>;

private:
	std::weak_ptr<distributed_ctx_t> _ctx;
	identification_success_f _hndl;
	std::atomic<bool> _invokedf = false;

	bool is_invoke()noexcept{
		bool infokedf = false;
		return _invokedf.compare_exchange_strong( infokedf, true );
		}

	auto invoker()noexcept{
		decltype(_hndl) invoker;
		std::swap( invoker, _hndl );

		return invoker;
		}

	void __invoke_success( TXPeer && peer,
		syncdata_list_t && sl,
		std::unique_ptr<execution_bool_tail> && registerCompleted ){

		if( is_invoke() ){
			if( auto f = invoker() ){
				f( nullptr,
					std::move( peer ),
					std::move( sl ),
					std::move( registerCompleted ) );
				}
			else{
				FATAL_ERROR_FWD(nullptr);
				}
			}
		}

	void __invoke_error( std::unique_ptr<dcf_exception_t> && exc )noexcept{
		if( is_invoke() ){
			if( auto c = _ctx.lock() ){
				if( !exc )
					exc = CREATE_MPF_PTR_EXC_FWD(nullptr);

				if( auto f = invoker() ){
					CHECK_NO_EXCEPTION( c->launch_async( __FILE_LINE__,
						std::move( f ), std::move( exc ), TXPeer(), syncdata_list_t(), nullptr ) );
					}
				else{
					FATAL_ERROR_FWD(nullptr);
					}
				}
			}
		}

public:
	__inbound_connection_result_t( std::weak_ptr<distributed_ctx_t> ctx,
		identification_success_f && hndl )noexcept
		: _ctx( ctx )
		, _hndl( std::move( hndl ) ){
		CBL_VERIFY( _hndl );
		}

	void ready( TXPeer && peer,
		syncdata_list_t && sl,
		std::unique_ptr<execution_bool_tail> && registerCompleted ){

		__invoke_success( std::move( peer ), std::move( sl ), std::move( registerCompleted ) );
		}

	void close()noexcept{
		CHECK_NO_EXCEPTION( __invoke_error( nullptr ) );
		}

	void refuse( std::unique_ptr<dcf_exception_t> && exc )noexcept{
		CHECK_NO_EXCEPTION( __invoke_error( std::move( exc ) ) );
		}

	~__inbound_connection_result_t(){
		CHECK_NO_EXCEPTION( close() );
		}

	__inbound_connection_result_t( const __inbound_connection_result_t & ) = delete;
	__inbound_connection_result_t& operator=( const __inbound_connection_result_t & ) = delete;

	__inbound_connection_result_t( __inbound_connection_result_t && ) = default;
	__inbound_connection_result_t& operator=( __inbound_connection_result_t && ) = default;
	};

}


