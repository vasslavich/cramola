#pragma once


#include "../internal_invariants.h"
#include "./rtl_rdm_object.h"
#include "./message_quant.h"
#include "../utilities/handlers/invoke_guard.h"
#include "./base_terms.h"
#include "./i_channel_base.h"


namespace crm::detail {


struct rt0_x_message_t  final{
	rtl_roadmap_line_t _rdm;
	_packet_t _pck;
	connection_key_t _basePeerID;

	rt0_x_message_t(rtl_roadmap_line_t && rdm,
		_packet_t && pck,
		connection_key_t && basePeerID)noexcept;

	~rt0_x_message_t();

	rt0_x_message_t()noexcept;

	static rt0_x_message_t make(rtl_roadmap_line_t && rdm,
		_packet_t && pck,
		connection_key_t && basePeerID)noexcept;

	rt0_x_message_t(const rt0_x_message_t&) = delete;
	rt0_x_message_t& operator=(const rt0_x_message_t &) = delete;

	rt0_x_message_t(rt0_x_message_t &&) = default;
	rt0_x_message_t& operator=(rt0_x_message_t &&) = default;

	const _packet_t& package()const noexcept;
	_packet_t& package() noexcept;
	_packet_t capture_package()noexcept;
	const rtl_roadmap_line_t& rdm()const noexcept;
	const connection_key_t& base_peer_id()const noexcept;

	rt0_x_message_t copy()const;

	bool is_sendrecv_request()const noexcept;
	bool is_sendrecv_response()const noexcept;
	bool is_sendrecv_cycle()const noexcept;

	template<typename Tws>
	void srlz(Tws&& dest)const {
		srlz::serialize(dest, _rdm);
		srlz::serialize(dest, _pck);
		srlz::serialize(dest, _basePeerID);
		}

	template<typename Trs>
	void dsrlz(Trs& src) {
		srlz::deserialize(src, _rdm);
		srlz::deserialize(src, _pck);
		srlz::deserialize(src, _basePeerID);
		}

	size_t get_serialized_size()const noexcept;
	};

bool is_report(const rt0_x_message_t& h)noexcept;
rtl_table_t passing_direction(const rt0_x_message_t& h)noexcept;
bool is_sendrecv_response(const rt0_x_message_t& h)noexcept;
bool is_sendrecv_request(const rt0_x_message_t& h)noexcept;
bool is_sendrecv_cycle(const rt0_x_message_t& h)noexcept;
bool check_sndrcv_key_spin_tag(const rt0_x_message_t& h)noexcept;
bool is_sndrcv_spin_tagged_invoke(const rt0_x_message_t& h)noexcept;


struct rt0_x_message_unit_t {
private:
	rt0_x_message_t _unit;
	std::unique_ptr<inbound_message_with_tail_exception_guard> _reverseExc;

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	utility::__xobject_counter_logger_t<rt0_x_message_unit_t, 10> _xDbgCounter;
#endif

public:
	~rt0_x_message_unit_t();

	rt0_x_message_unit_t()noexcept;
	rt0_x_message_unit_t(rt0_x_message_t && unit)noexcept;

	rt0_x_message_unit_t(rt0_x_message_unit_t &&o)noexcept;
	rt0_x_message_unit_t& operator=(rt0_x_message_unit_t &&)noexcept;

	rt0_x_message_unit_t(const rt0_x_message_unit_t &) = delete;
	rt0_x_message_unit_t& operator=(const rt0_x_message_unit_t &) = delete;


	const rt0_x_message_t& unit()const noexcept;
	rt0_x_message_t capture_unit()noexcept;

	bool is_sendrecv_request()const noexcept;
	bool is_sendrecv_response()const noexcept;
	bool is_sendrecv_cycle()const noexcept;
	bool is_reverse_exception_guard()const noexcept;

	inbound_message_with_tail_exception_guard reset_invoke_exception_guard()noexcept;
	inbound_message_with_tail_exception_guard reset_invoke_exception_guard(
		inbound_message_with_tail_exception_guard && g)noexcept;

	std::unique_ptr<inbound_message_with_tail_exception_guard> release_invoke_exception_guard_ptr() && noexcept;

	void invoke_exception(std::unique_ptr<dcf_exception_t> && exc)noexcept;

#ifdef CBL_MPF_ENABLE_TIMETRACE
	void timeline_set_rt1_receiver_router()noexcept;
#endif
	};


bool is_sendrecv_cycle(const rt0_x_message_unit_t & h)noexcept;
bool check_sndrcv_key_spin_tag(const rt0_x_message_unit_t& h)noexcept;
bool is_sndrcv_spin_tagged_invoke(const rt0_x_message_unit_t& h)noexcept;




struct outbound_message_desc_t final {
	rt1_streams_table_wait_key_t streamId;
	subscriber_node_id_t requestEmitter;
	subscriber_node_id_t responseTarget;
	message_spin_tag_type spitTag;
	iol_type_spcf_t type;
	channel_identity_t chid;
	rtl_level_t level{ rtl_level_t::undefined };
	bool isNull{ true };

	static const outbound_message_desc_t null;

	outbound_message_desc_t()noexcept;
	explicit outbound_message_desc_t(const i_iol_handler_t & mh)noexcept;

	bool is_sendrecv_request()const noexcept;
	bool is_sendrecv_cycle()const noexcept;
	bool is_sendrecv_response()const noexcept;
	bool is_null()const noexcept;

	void set_channel_id(const channel_identity_t & chid)noexcept;
	void set_source_rt1_stream_key(rt1_streams_table_wait_key_t && rt1sk)noexcept;


	template<typename Ts>
	void srlz(Ts && dest)const {
		srlz::serialize(dest, streamId);
		srlz::serialize(dest, requestEmitter);
		srlz::serialize(dest, responseTarget);
		srlz::serialize(dest, spitTag);
		srlz::serialize(dest, type);
		srlz::serialize(dest, chid);
		srlz::serialize(dest, level);
		srlz::serialize(dest, isNull);
		}

	template<typename Ts>
	void dsrlz(Ts && src) {
		srlz::deserialize(src, streamId);
		srlz::deserialize(src, requestEmitter);
		srlz::deserialize(src, responseTarget);
		srlz::deserialize(src, spitTag);
		srlz::deserialize(src, type);
		srlz::deserialize(src, chid);
		srlz::deserialize(src, level);
		srlz::deserialize(src, isNull);
		}

	size_t get_serialized_size()const noexcept;
	};


bool is_report(const outbound_message_desc_t& h)noexcept;
bool is_sendrecv_response(const outbound_message_desc_t& h)noexcept;
bool is_sendrecv_request(const outbound_message_desc_t& h)noexcept;
rtl_table_t passing_direction(const outbound_message_desc_t& h)noexcept;
bool is_sendrecv_cycle(const outbound_message_desc_t& h)noexcept;
bool check_sndrcv_key_spin_tag(const outbound_message_desc_t& h)noexcept;
bool is_sndrcv_spin_tagged_invoke(const outbound_message_desc_t& h)noexcept;



struct outpass_message_reverse_t {
	outbound_message_desc_t desc;
	inbound_message_with_tail_exception_guard guard;

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	utility::__xobject_counter_logger_t<outpass_message_reverse_t, 10> _xDbgCounter;
#endif

	outpass_message_reverse_t()noexcept {}
	outpass_message_reverse_t(outbound_message_desc_t && desc_,
		inbound_message_with_tail_exception_guard && guard_)noexcept;

	outpass_message_reverse_t(const outpass_message_reverse_t&) = delete;
	outpass_message_reverse_t& operator=(const outpass_message_reverse_t&) = delete;

	outpass_message_reverse_t(outpass_message_reverse_t&&)noexcept = default;
	outpass_message_reverse_t& operator=(outpass_message_reverse_t&&)noexcept = default;
	};


struct outbound_message_unit_t {
private:
	binary_vector_t _bin;
	outbound_message_desc_t _desc;
	std::unique_ptr<outpass_message_reverse_t> _reverseExc;

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	utility::__xobject_counter_logger_t<outbound_message_unit_t, 10> _xDbgCounter;
#endif

	outbound_message_unit_t(trace_tag&& tag_, 
		binary_vector_t && bin)noexcept;

	outbound_message_unit_t(trace_tag&& tag_, 
		binary_vector_t && bin,
		outbound_message_desc_t && desc,
		inbound_message_with_tail_exception_guard && excgrd)noexcept;

public:
	~outbound_message_unit_t();

	outbound_message_unit_t(const outbound_message_unit_t &) = delete;
	outbound_message_unit_t& operator=(const outbound_message_unit_t &) = delete;

	outbound_message_unit_t(outbound_message_unit_t &&)noexcept;
	outbound_message_unit_t& operator=(outbound_message_unit_t &&)noexcept;

	static outbound_message_unit_t make(trace_tag &&tag_, binary_vector_t && bin)noexcept;

	static outbound_message_unit_t make(trace_tag&& tag_, binary_vector_t && bin,
		outbound_message_desc_t && desc,
		inbound_message_with_tail_exception_guard && excgrd)noexcept;

	outbound_message_unit_t()noexcept;

	const binary_vector_t& bin()const noexcept;
	binary_vector_t capture_bin()noexcept;

	void set_source_rt1_stream_key(rt1_streams_table_wait_key_t &&)noexcept;

	const outbound_message_desc_t& desc()const & noexcept;
	outbound_message_desc_t desc() && noexcept;

	bool is_exception_tail()const noexcept;

	void reset_invoke_exception_guard_reset()noexcept;
	inbound_message_with_tail_exception_guard reset_invoke_exception_guard()noexcept;
	/*inbound_message_with_tail_exception_guard reset_invoke_exception_guard(outbound_message_desc_t &&,
		inbound_message_with_tail_exception_guard && g)noexcept;*/

	void invoke_exception(std::unique_ptr<dcf_exception_t> && exc)noexcept;

	bool is_sendrecv_cycle()const noexcept;
	bool is_sendrecv_request()const noexcept;
	bool is_sendrecv_response()const noexcept;

	/*const outbound_message_desc_t& sndrcv_desc()const & noexcept;
	outbound_message_desc_t sndrcv_desc()&& noexcept;*/

#ifdef CBL_MPF_ENABLE_TIMETRACE
	const detail::hanlder_times_stack_t& timeline()const noexcept;
	detail::hanlder_times_stack_t timeline_release() noexcept;
	void timeline_set_rt1_outbound_packet() noexcept;
#endif
	};


bool is_report(const outbound_message_unit_t& h)noexcept;
rtl_table_t passing_direction(const outbound_message_unit_t& h)noexcept;
bool is_sendrecv_response(const outbound_message_unit_t& h)noexcept;
bool is_sendrecv_request(const outbound_message_unit_t& h)noexcept;
bool is_sendrecv_cycle(const outbound_message_unit_t& h)noexcept;
bool check_sndrcv_key_spin_tag(const outbound_message_unit_t& h)noexcept;
bool is_sndrcv_spin_tagged_invoke(const outbound_message_unit_t& h)noexcept;



struct message_frame_unit_t {
private:
	_packet_t _unit;
	std::unique_ptr<inbound_message_with_tail_exception_guard> _reverseExc;

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	utility::__xobject_counter_logger_t<message_frame_unit_t, 10> _xDbgCounter;
#endif

public:
	~message_frame_unit_t();

	message_frame_unit_t()noexcept;
	message_frame_unit_t(_packet_t && unit)noexcept;
	message_frame_unit_t(rt0_x_message_unit_t && unit)noexcept;

	message_frame_unit_t(message_frame_unit_t &&)noexcept;
	message_frame_unit_t& operator=(message_frame_unit_t &&)noexcept;

	message_frame_unit_t(const message_frame_unit_t &) = delete;
	message_frame_unit_t& operator=(const message_frame_unit_t &) = delete;

	const _packet_t& unit()const noexcept;
	_packet_t capture_unit()noexcept;

	inbound_message_with_tail_exception_guard reset_invoke_exception_guard()noexcept;
	inbound_message_with_tail_exception_guard reset_invoke_exception_guard(
		inbound_message_with_tail_exception_guard && g)noexcept;

	void invoke_exception(const std::exception & exc)noexcept;
	void invoke_exception(const dcf_exception_t& exc)noexcept;
	void invoke_exception(std::unique_ptr<dcf_exception_t> && exc)noexcept;

#ifdef CBL_MPF_ENABLE_TIMETRACE
	const detail::hanlder_times_stack_t& timeline()const noexcept;
	detail::hanlder_times_stack_t timeline_release() noexcept;
	void timeline_set_rt1_receiver_router() noexcept;
#endif
	};


struct rt1_x_message_data_t final{
	rtl_roadmap_line_t _rdm;
	binary_vector_t _bin;
	outbound_message_desc_t _sndrcvHndl;


	rt1_x_message_data_t()noexcept;

	rt1_x_message_data_t(rtl_roadmap_line_t && rdm,
		binary_vector_t && bin,
		const channel_identity_t & chid_,
		outbound_message_desc_t && srh)noexcept;

	rt1_x_message_data_t(rtl_roadmap_line_t && rdm,
		binary_vector_t && bin)noexcept;

	rt1_x_message_data_t(const rt1_x_message_data_t&) = delete;
	rt1_x_message_data_t& operator=(const rt1_x_message_data_t &) = delete;

	rt1_x_message_data_t(rt1_x_message_data_t &&)noexcept = default;
	rt1_x_message_data_t& operator=(rt1_x_message_data_t &&)noexcept = default;

	rt1_x_message_data_t copy()const noexcept;

	const binary_vector_t& package()const noexcept;
	const rtl_roadmap_line_t& rdm()const & noexcept;
	rtl_roadmap_line_t rdm() && noexcept;
	binary_vector_t capture_package()noexcept;

	bool is_sendrecv_cycle()const noexcept;
	bool is_sendrecv_request()const noexcept;
	bool is_sendrecv_response()const noexcept;

	const outbound_message_desc_t& sndrcv_handler()const & noexcept;
	outbound_message_desc_t sndrcv_handler() && noexcept;


	template<typename Ts>
	void srlz(Ts&& dest)const {
		srlz::serialize(dest, _rdm);
		srlz::serialize(dest, _bin);
		srlz::serialize(dest, _sndrcvHndl);
		}

	template<typename Ts>
	void dsrlz(Ts&& src) {
		srlz::deserialize(src, _rdm);
		srlz::deserialize(src, _bin);
		srlz::deserialize(src, _sndrcvHndl);
		}

	size_t get_serialized_size()const noexcept;
	};

bool is_sendrecv_cycle(const rt1_x_message_data_t& h)noexcept;
bool is_sndrcv_spin_tagged_invoke(const rt1_x_message_data_t& h)noexcept;
bool check_sndrcv_key_spin_tag(const rt1_x_message_data_t& h)noexcept;


struct rt1_x_message_t {
private:
	rtl_roadmap_line_t _rdm;
	outbound_message_unit_t _pck;

	rt1_x_message_t(rtl_roadmap_line_t && rdm,
		outbound_message_unit_t && bin)noexcept;

public:
	rt1_x_message_t()noexcept;

	rt1_x_message_t(const rt1_x_message_t &) = delete;
	rt1_x_message_t& operator=(const rt1_x_message_t &) = delete;

	rt1_x_message_t(rt1_x_message_t &&)noexcept;
	rt1_x_message_t& operator=(rt1_x_message_t &&)noexcept;

	static rt1_x_message_t make(rtl_roadmap_line_t && rdm,
		outbound_message_unit_t && bin)noexcept;

	const rtl_roadmap_line_t& rdm()const & noexcept;
	rtl_roadmap_line_t rdm() && noexcept;

	const outbound_message_unit_t& package()const & noexcept;
	outbound_message_unit_t package() && noexcept;

	bool is_sendrecv_cycle()const noexcept;
	bool is_sendrecv_request()const noexcept;
	bool is_sendrecv_response()const noexcept;
	};


bool is_report(const rt1_x_message_t& h)noexcept;
rtl_table_t passing_direction(const rt1_x_message_t& h)noexcept;
bool is_sendrecv_request(const rt1_x_message_t& h)noexcept;
bool is_sendrecv_response(const rt1_x_message_t&)noexcept;
bool is_sendrecv_cycle(const rt1_x_message_t& h)noexcept;
bool check_sndrcv_key_spin_tag(const rt1_x_message_t& h)noexcept;
bool is_sndrcv_spin_tagged_invoke(const rt1_x_message_t& h)noexcept;
}


namespace crm {

template<>
struct null_value_t<detail::outbound_message_desc_t> {
	static const detail::outbound_message_desc_t null;
	};
}

