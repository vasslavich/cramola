#pragma once


#include <string>
#include "../internal_invariants.h"
#include "../base_predecl.h"
#include "../utilities/hash.h"
#include "../utilities/identifiers.h"
#include "./ids_types.h"


namespace crm {


message_spin_tag_type make_message_spin_tag_by_value(uint64_t val)noexcept;


struct identity_descriptor_t final{
	static const std::string null_name;
	static const global_object_identity_t null_id;
	static const identity_descriptor_t null;
	static constexpr size_t max_full_size = CRM_KB;
	static constexpr size_t max_name_length = max_full_size - 64;

private:
	identity_descriptor_id_type _id;
	std::string _name;

public:
	std::string to_str()const;
	std::wstring to_wstr()const;

	identity_descriptor_t()noexcept;
	identity_descriptor_t(identity_descriptor_id_type id,
		std::string_view name_);

	const identity_descriptor_id_type& id()const noexcept;
	const std::string& name()const noexcept;
	bool is_null()const noexcept;

	template<typename Ts>
	void srlz(Ts&& dest)const {
		srlz::serialize(dest, _id);
		srlz::serialize(dest, _name, srlz::detail::option_max_serialized_size<max_name_length>{});
		}

	template<typename Ts>
	void dsrlz(Ts&& src) {
		crm::srlz::deserialize(src, _id);
		crm::srlz::deserialize(src, _name, srlz::detail::option_max_serialized_size<max_name_length>{});
		}

	std::size_t get_serialized_size()const noexcept;
	};


void apply_at_hash( address_hash_t &, const identity_descriptor_t & d )noexcept;

bool operator>=( const identity_descriptor_t & l, const identity_descriptor_t  & r )noexcept;
bool operator>( const identity_descriptor_t & l, const identity_descriptor_t  & r )noexcept;
bool operator<( const identity_descriptor_t & l, const identity_descriptor_t  & r )noexcept;
bool operator==( const identity_descriptor_t & l, const identity_descriptor_t  & r )noexcept;
bool operator!=( const identity_descriptor_t & l, const identity_descriptor_t  & r )noexcept;

std::ostream  & operator <<( std::ostream & os, const identity_descriptor_t & uuid );
std::wostream  & operator <<( std::wostream & os, const identity_descriptor_t & uuid );

namespace detail{


struct subscribe_options{
	assign_options flag{ assign_options::insert_if_not_exists };
	bool once{ true };

	subscribe_options( assign_options a, bool once_ )
		: flag( a )
		, once( once_ ){}

	subscribe_options(){}
	};

struct handler_invoke_abandoned_marker{};
}

using binary_vector_t = std::vector<uint8_t>;

typedef crm::datagram_t syncdata_list_t;


struct identity_payload_entry_t{
	std::string key;
	binary_vector_t data;
	};

struct identity_payload_t{
	std::vector<identity_payload_entry_t> collection;

	std::unique_ptr<identity_payload_entry_t> find_by_key( const std::string & key )const noexcept;
	void add( identity_payload_entry_t && value );
	void add( const std::string & key, binary_vector_t && data );
	};

struct identity_value_t{
	identity_payload_t properties;
	identity_descriptor_t id_value;
	};


struct i_terminatable_t {
	virtual ~i_terminatable_t() {};
	};

struct i_terminatable_timeout_t : i_terminatable_t {
	virtual std::chrono::system_clock::duration timeout_interval()const noexcept = 0;
	virtual std::chrono::system_clock::time_point time_point_start()const noexcept = 0;
	virtual std::chrono::system_clock::time_point time_point_deadline()const noexcept = 0;
	virtual std::chrono::system_clock::duration default_timeline()const noexcept = 0;
	virtual std::string name()const noexcept = 0;
	virtual void do_terminate()noexcept = 0;
	};
}

