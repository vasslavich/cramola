#pragma once


#include "../typeframe/typemap/i_typemap.h"
#include "./base_terms.h"
#include "./message_bitmask.h"


namespace crm {


class iol_type_spcf_t final :
	public srlz::i_typeid_t {

public:
	typedef detail::iol_type_map_t::packed_type_t field_type_t;
	typedef std::tuple< detail::iol_type_attributes_t, field_type_t> attrb_pair_t;

	static const field_type_t undefined = 0;
	static const field_type_t utype_undefined = detail::iol_type_map_t::packed_type_undefined;
	static const field_type_t ucode_undefined = detail::iol_type_map_t::packed_type_undefined;

private:
	field_type_t _field = undefined;

	static  detail::iol_type_attributes_t get_name(const attrb_pair_t &attr) noexcept;
	static field_type_t get_value(const attrb_pair_t &attr) noexcept;

	/*! ������ �����, ��������� ��� ��������� ����� ������ ���������� */
	void _set_attribute() noexcept;

	template<typename ... tv>
	void _set_attribute(const attrb_pair_t &t, typename tv ... tlist)  noexcept {
		set(get_name(t), get_value(t));
		_set_attribute(tlist...);
		}

	iol_type_spcf_t(field_type_t field)noexcept;

public:
	iol_type_spcf_t()noexcept;
	iol_type_spcf_t(const iol_type_spcf_t &o)noexcept;
	static iol_type_spcf_t create(field_type_t field)noexcept;

	iol_type_spcf_t& operator=(const iol_type_spcf_t &o)noexcept;

	/*! ����������� � ���������� ������ ��������� ���� */
	template<typename ... tv>
	static iol_type_spcf_t build(typename tv ... attibutes)  noexcept {

		iol_type_spcf_t value;

		/* ����������� ���������� ��������� ������ ���������� � ��������� ��������� */
		value._set_attribute(attibutes...);

		return value;
		}

	template<typename T>
	static attrb_pair_t make_attrb_pair(const  detail::iol_type_attributes_t index, typename T value) noexcept {
		static_assert(std::is_integral<T>::value || std::is_enum<T>::value, __FILE__);
		static_assert(sizeof(T) <= sizeof(field_type_t), __FILE__);

		return attrb_pair_t(index, (field_type_t)value);
		}

	field_type_t get()const  noexcept;
	field_type_t get(const  detail::iol_type_attributes_t index)const  noexcept;
	static field_type_t get(field_type_t dscrp, const  detail::iol_type_attributes_t index) noexcept;

	static bool validate_attribute_value(detail::iol_type_attributes_t index, field_type_t value) noexcept;
	void set(field_type_t value) noexcept;
	void set(detail::iol_type_attributes_t index, field_type_t value)noexcept;
	static field_type_t set(field_type_t dest, detail::iol_type_attributes_t index, field_type_t value) noexcept;

	template<typename T>
	void set(const  detail::iol_type_attributes_t index, typename T value) noexcept {
		static_assert(std::is_integral<T>::value || std::is_enum<T>::value, __FILE__);
		static_assert(sizeof(T) <= sizeof(field_type_t), __FILE__);

		const field_type_t ival = (field_type_t)value;
		set(index, ival);
		}

public:
	bool is_user_type()const noexcept;
	detail::iol_types_t type()const noexcept;
	field_type_t utype()const noexcept;
	bool fl_has(detail::iol_type_attributes_t attr)const noexcept;
	bool fl_typeid()const noexcept;
	bool fl_timeline()const noexcept;
	bool fl_report()const noexcept;
	bool fl_use_crc()const noexcept;
	void setf_use_crc()noexcept;
	void setf_report()noexcept;
	void setf_length()noexcept;
	field_type_t ucode()const noexcept;
	detail::iol_length_spcf_t length()const noexcept;
	field_type_t reserved()const noexcept;
	field_type_t utypeid()const noexcept;
	detail::iol_types_ibase_t priority_value()const noexcept;

	static field_type_t utypeid(field_type_t utype, field_type_t ucode) noexcept;
	static field_type_t maxval(detail::iol_type_attributes_t name);

	std::wstring print()const;

public:
	/*! ������������ ���� */
	 type_identifier_t type_id()const noexcept final;
	/*! ������������� ����, ����� �������������� � ������, ���� ��������� �������� �� /ref id */
	 std::string type_name()const noexcept final;

	/*! ���������� ������ ���� � ������� ����� ����� */
	srlz::typemap_key_t typemap_key()const noexcept;

	friend bool operator==(const iol_type_spcf_t &l, const iol_type_spcf_t &r) noexcept;
	friend bool operator !=(const iol_type_spcf_t &l, const iol_type_spcf_t &r) noexcept;

	/*! true - ���� l ����� ������������, ��� r */
	static bool priority(const iol_type_spcf_t &l, const iol_type_spcf_t &r) noexcept;

	template<typename Ts>
	void srlz(Ts && dest)const{
		srlz::serialize(dest, _field);
		}

	template<typename Ts>
	void dsrlz(Ts && src ){
		srlz::deserialize(src, _field);
		}

	size_t get_serialized_size()const noexcept;
	};

std::string cvt2string(const iol_type_spcf_t& spc);
}
