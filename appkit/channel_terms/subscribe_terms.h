#pragma once

#include "../build_config.h"
#include "../functorapp/functorapp.h"
#include "../internal_invariants.h"
#include "../channel_terms/i_events.h"
#include "ids_types.h"
#include "./subscribe_event_addr.h"
#include "../utilities/handlers/invoke_guard.h"


namespace crm{	
namespace detail {


struct _address_hndl_t {
private:
	subscriber_node_id_t _id{ get_null<subscriber_node_id_t>() };
	std::string _key;
	iol_types_ibase_t _type{ 0 };
	rtl_level_t _level{ rtl_level_t::undefined };
	uint8_t _tagz{ 0 };
	async_space_t _scx{ async_space_t::undef };
	address_hash_t _hash;
	bool _invokeAbandoned{ true };

#if defined( CBL_OBJECTS_COUNTER_CHECKER )
	utility::__xobject_counter_logger_t<_address_hndl_t> _xDbgCounter;
#endif

	_address_hndl_t(const subscriber_node_id_t& id,
		std::string_view key,
		iol_types_ibase_t type,
		rtl_level_t level,
		async_space_t scx,
		bool invokeAbandoned = true,
		uint8_t tagz = 0)noexcept;

	_address_hndl_t(std::string_view key,
		iol_types_ibase_t type,
		rtl_level_t level,
		async_space_t scx,
		bool invokeAbandoned = true,
		uint8_t tagz = 0)noexcept;

	_address_hndl_t(const subscriber_node_id_t& id,
		std::string_view key,
		iol_types_t type,
		rtl_level_t level,
		async_space_t scx,
		bool invokeAbandoned = true,
		uint8_t tagz = 0)noexcept;

	void update_hash()noexcept;

public:
	_address_hndl_t()noexcept;

	static _address_hndl_t make(const subscriber_node_id_t& id,
		iol_types_ibase_t type,
		rtl_level_t level,
		async_space_t)noexcept;

	static _address_hndl_t make(const subscriber_node_id_t& id,
		rtl_level_t level,
		async_space_t,
		bool invokeAbandoned = true,
		uint8_t tagz = 0)noexcept;

	static _address_hndl_t make(const subscriber_node_id_t& id,
		std::string_view key,
		rtl_level_t level,
		async_space_t,
		bool invokeAbandoned = true,
		uint8_t tagz = 0)noexcept;

	static _address_hndl_t make(std::string_view key,
		rtl_level_t level,
		async_space_t,
		bool invokeAbandoned = true,
		uint8_t tagz = 0)noexcept;

	static _address_hndl_t make(const subscriber_node_id_t& id,
		iol_types_t type,
		rtl_level_t level,
		async_space_t)noexcept;

	const subscriber_node_id_t& id()const noexcept;
	iol_types_ibase_t type()const noexcept;
	rtl_level_t level()const noexcept;
	bool invoke_abandoned()const noexcept;
	uint8_t tagz()const noexcept;
	async_space_t space()const noexcept;
	const std::string& key()const noexcept;
	const address_hash_t& object_hash()const noexcept;
	};


bool operator < (const _address_hndl_t & lval, const _address_hndl_t &rval)noexcept;
bool operator >= (const _address_hndl_t & lval, const _address_hndl_t &rval)noexcept;
bool operator == (const _address_hndl_t & lval, const _address_hndl_t &rval)noexcept;
bool operator != (const _address_hndl_t & lval, const _address_hndl_t &rval)noexcept;



template<typename TCallback>
struct is_callback_subscribe_message_invokable :
	std::is_invocable<TCallback, std::unique_ptr<dcf_exception_t> &&, std::unique_ptr<i_iol_ihandler_t> &&, invoke_context_trait>{};

template<typename TCallback>
static constexpr bool is_callback_subscribe_message_invokable_v = is_callback_subscribe_message_invokable<TCallback>::value;


struct subscribe_message_handler{
	using functor_type = crm::utility::invoke_functor<void, std::unique_ptr<dcf_exception_t> &&, std::unique_ptr<i_iol_ihandler_t> &&, invoke_context_trait>;
	using functor_shared_type = crm::utility::invoke_functor_shared<void, std::unique_ptr<dcf_exception_t> &&, std::unique_ptr<i_iol_ihandler_t> &&, invoke_context_trait>;
	using functor_base_type = functor_type::base_type;

	functor_type _pf;
	inovked_as _invokeAs{ inovked_as::async };

#ifdef CBL_TRACELEVEL_TRACE_TAG_ON
	trace_tag _tag;
#endif
#ifdef CBL_OBJECTS_COUNTER_CHECKER
	crm::utility::__xobject_counter_logger_t<subscribe_message_handler> _xDbgCounter;
#endif

	subscribe_message_handler()noexcept;

	subscribe_message_handler(const subscribe_message_handler &) = delete;
	const subscribe_message_handler& operator=(const subscribe_message_handler &) = delete;

	subscribe_message_handler(subscribe_message_handler &&)noexcept = default;
	subscribe_message_handler& operator=(subscribe_message_handler &&)noexcept = default;

	template<typename TX,
		typename std::enable_if_t<(
			!std::is_base_of_v<typename std::decay_t<TX>, subscribe_message_handler> &&
			is_callback_subscribe_message_invokable_v<TX>), int> = 0 >
	subscribe_message_handler(
#ifdef CBL_TRACELEVEL_TRACE_TAG_ON
		trace_tag&& tag,
#else
		trace_tag&&,
#endif

		TX && h_ )
		: _pf( std::forward<TX>( h_ ) )
#ifdef CBL_TRACELEVEL_TRACE_TAG_ON
		, _tag(std::move(tag))
#endif
#if defined( CBL_OBJECTS_COUNTER_CHECKER) && defined(CBL_TRACELEVEL_TRACE_TAG_ON)
		, _xDbgCounter(_tag.name)
#endif
	{}


	template<typename TX,
		typename std::enable_if_t<(
			!std::is_base_of_v<typename std::decay_t<TX>, subscribe_message_handler> &&
			is_callback_subscribe_message_invokable_v<TX>), int> = 0 >
	subscribe_message_handler(
#ifdef CBL_TRACELEVEL_TRACE_TAG_ON
		trace_tag && tag, 
#else
		trace_tag&&,
#endif
		
		TX && h_, inovked_as ias_ )
		: _pf( std::forward<TX>( h_ ) )
		, _invokeAs{ ias_ }
#ifdef CBL_TRACELEVEL_TRACE_TAG_ON
		, _tag(std::move(tag))
#endif
#if defined( CBL_OBJECTS_COUNTER_CHECKER) && defined(CBL_TRACELEVEL_TRACE_TAG_ON)
		, _xDbgCounter(_tag.name)
#endif
		{}

	void operator()(std::unique_ptr<dcf_exception_t> && e, invoke_context_trait = invoke_context_trait::sync )noexcept;
	void operator()(std::unique_ptr<i_iol_ihandler_t> && v, invoke_context_trait = invoke_context_trait::sync);

	operator bool()const noexcept;

	functor_type release_as_unique() && noexcept{
		return std::move( _pf );
		}

	functor_shared_type release_as_shared() && noexcept{
		return functor_shared_type( std::move( _pf ) );
		}
	};


}
}


template<>
struct ::std::hash<crm::detail::_address_hndl_t> {
	size_t operator()(crm::detail::_address_hndl_t const& v) const noexcept {
		return crm::as_std_hash(v);
		}
	};

