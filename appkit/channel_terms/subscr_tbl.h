#pragma once


#include <unordered_map>

#if (CRM_SUBSCRIBE_TBL_INVOKE_MODE == CRM_SUBSCRIBE_TBL_INVOKE_MODE_INVAR)
#include <shared_mutex>
#elif(CRM_SUBSCRIBE_TBL_INVOKE_MODE == CRM_SUBSCRIBE_TBL_INVOKE_MODE_MUTABLE)
#include <mutex>
#else
#error Bad susbcribe table mode
#endif

#include <functional>
#include <memory>
#include <atomic>
#include "./message_subscribe_terms.h"


namespace crm::detail{


template<typename TFunctorType>
typename TFunctorType release_as(subscribe_message_handler && h)noexcept;


template<>
subscribe_message_handler::functor_type release_as<subscribe_message_handler::functor_type>(subscribe_message_handler && h)noexcept;


template<>
subscribe_message_handler::functor_shared_type release_as<subscribe_message_handler::functor_shared_type>(subscribe_message_handler && h)noexcept;


class subscribe_table_t : public std::enable_shared_from_this<subscribe_table_t> {
public:
	using line_num_t = message_subscribe_assist_t::line_num_t;
	using _addressed_link_t = message_subscribe_assist_t::message_addressed_link_t;

private:
	using line_map_t = std::unordered_map<_address_hndl_t, std::shared_ptr<message_subscribe_assist_t::message_addressed_link_t>>;

	/*! ����� �������� �������� ��������� */
	std::unordered_map <line_num_t, line_map_t > _addressBook;
	
#if (CRM_SUBSCRIBE_TBL_INVOKE_MODE == CRM_SUBSCRIBE_TBL_INVOKE_MODE_INVAR)
	using lock_type = std::shared_mutex;
#elif (CRM_SUBSCRIBE_TBL_INVOKE_MODE == CRM_SUBSCRIBE_TBL_INVOKE_MODE_MUTABLE)
	using lock_type = std::mutex;
#endif

	mutable lock_type _tableLck;

	/*! �������� */
	std::weak_ptr<distributed_ctx_t> _ctx;
	local_object_id_t _thisId;

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	mutable crm::utility::__xvalue_counter_logger_t<1> __sizeTraceer1 = __FILE_LINE__;
	mutable crm::utility::__xvalue_counter_logger_t<1> __sizeTraceer2 = __FILE_LINE__;
	crm::utility::__xobject_counter_logger_t<subscribe_table_t> _xDbgCounter;

	void __size_trace()const noexcept;
#endif

	
#ifdef CBL_SUBSCRIBE_TABLE_TRACE
	static void trace(const subscribe_table_t & obj, const _address_hndl_t & a, const std::string & pref) noexcept;

#define CBL_SUBSCRIBE_TABLE_TRACE_DO(a, pref) trace((*this), a, pref);

#else

#define CBL_SUBSCRIBE_TABLE_TRACE_DO(a, pref)
#endif

	void __regline(const line_num_t&);

	/*! ���������� ���������� ��� ������� ��������*/
	subscribe_invoker_result __add(const line_num_t&,
		async_space_t scx,
		const _address_hndl_t & address,
		subscribe_message_handler && hdl,
		subscribe_options opt);

	void unset(const line_num_t& lnId,	const _address_hndl_t& addr)noexcept;

	/*! ������� ����������� ��� ������� ������ */
	_addressed_link_t remove(const line_num_t&, const _address_hndl_t & address)noexcept;
	std::list< std::tuple<_address_hndl_t, _addressed_link_t>> remove(const line_num_t&)noexcept;

	void reset(_addressed_link_t &&)const noexcept;
	void reset(std::list<_addressed_link_t> &&)const noexcept;
	void invoke_abandoned(std::list<std::tuple<_address_hndl_t, _addressed_link_t>> &&)const noexcept;
	void invoke_abandoned(const _address_hndl_t & addr, _addressed_link_t && h)const noexcept;


	template<typename THandler, 
		typename TArg,
		typename std::enable_if_t<std::is_same_v<_addressed_link_t, std::decay_t<THandler>>, int> = 0 >
	void invoke(THandler && h, invoke_context_trait ic, TArg && arg )const{
		if( h ){
			h(std::forward<TArg>(arg), ic );
			}
		}
	
	//template<typename TArgmrk>
	//void invoke_find(const line_num_t& lnId,
	//	const _address_hndl_t & addr_,
	//	invoke_context_trait ic,
	//	TArgmrk && argmk)noexcept {

	//	CBL_NO_EXCEPTION_BEGIN

	//	lck_scp_t lck(_lck);

	//	auto itLn = _addressBook.find(lnId);
	//	if (itLn != _addressBook.end()) {

	//		auto it = itLn->second.find(addr_);
	//		if (it != itLn->second.end()) {
	//			MPF_ERR_VERIFY_FWD(it->first == addr_, 0);

	//			if (it->second->once()) {
	//				CBL_VERIFY(it->second->hndl.invoked_max_count() <= it->second->hndl.invoked_count()
	//					|| (it->second->hndl.invoked_max_count() - it->second->hndl.invoked_count()) == 1);

	//				auto ph = std::move(it->second);
	//				itLn->second.erase(it);

	//				lck.unlock();

	//				CBL_SUBSCRIBE_TABLE_TRACE_DO(addr_, "invoke");
	//				invoke((*ph), ic, argmk());
	//				}
	//			else {
	//				CBL_VERIFY(it->second->hndl.invoked_max_count() > 1 && 
	//					it->second->hndl.invoked_max_count() > it->second->hndl.invoked_count() );

	//				auto ph = it->second;
	//				lck.unlock();

	//				CBL_SUBSCRIBE_TABLE_TRACE_DO(addr_, "invoke");
	//				invoke((*ph), ic, argmk());
	//				}
	//			}
	//		else {
	//			lck.unlock();
	//			CBL_SUBSCRIBE_TABLE_TRACE_DO(addr_, "not founded 0");
	//			}
	//		}
	//	else {
	//		lck.unlock();
	//		CBL_SUBSCRIBE_TABLE_TRACE_DO(addr_, "not founded 1");
	//		}

	//	CBL_NO_EXCEPTION_END(_ctx.lock())
	//	}


	void lazy_remove_addr_hndl(const line_num_t& lnId,
		const _address_hndl_t& addr)noexcept;

	template<typename TArgmrk>
	void invoke_find(const line_num_t& lnId,
		const _address_hndl_t& addr_,
		invoke_context_trait ic,
		TArgmrk&& argmk) {

#if (CRM_SUBSCRIBE_TBL_INVOKE_MODE == CRM_SUBSCRIBE_TBL_INVOKE_MODE_INVAR)
			std::shared_lock lck(_tableLck);
#elif (CRM_SUBSCRIBE_TBL_INVOKE_MODE == CRM_SUBSCRIBE_TBL_INVOKE_MODE_MUTABLE)
			std::unique_lock lck(_tableLck);
#endif

		auto itLn = _addressBook.find(lnId);
		if(itLn != _addressBook.end()) {

			auto it = itLn->second.find(addr_);
			if(it != itLn->second.end()) {
				CBL_VERIFY(it->first == addr_);

				if (it->second) {
					if (it->second->once()) {
						CBL_VERIFY(it->second->hndl.invoked_max_count() <= it->second->hndl.invoked_count()
							|| (it->second->hndl.invoked_max_count() - it->second->hndl.invoked_count()) == 1);


						auto ph = std::atomic_exchange(&it->second, {});

#if (CRM_SUBSCRIBE_TBL_INVOKE_MODE == CRM_SUBSCRIBE_TBL_INVOKE_MODE_INVAR)

						lck.unlock();
						lazy_remove_addr_hndl(lnId, addr_);

#elif (CRM_SUBSCRIBE_TBL_INVOKE_MODE == CRM_SUBSCRIBE_TBL_INVOKE_MODE_MUTABLE)

						itLn->second.erase(it);
						lck.unlock();
#endif

						CBL_SUBSCRIBE_TABLE_TRACE_DO(addr_, "invoke:1");
						invoke((*ph), ic, argmk());
						}
					else {
						CBL_VERIFY(it->second->hndl.invoked_max_count() > 1 &&
							it->second->hndl.invoked_max_count() > it->second->hndl.invoked_count());

						auto ph = std::atomic_load_explicit(&it->second, std::memory_order::memory_order_acquire);
						lck.unlock();

						CBL_SUBSCRIBE_TABLE_TRACE_DO(addr_, "invoke:2");
						invoke((*ph), ic, argmk());
						}
					}
				else {
					lck.unlock();
					CBL_SUBSCRIBE_TABLE_TRACE_DO(addr_, "not founded 0");
					}
				}
			else {
				lck.unlock();
				CBL_SUBSCRIBE_TABLE_TRACE_DO(addr_, "not founded 1");
				}
			}
		else {
			lck.unlock();
			CBL_SUBSCRIBE_TABLE_TRACE_DO(addr_, "not founded 2");
			}
		}




	void __close()noexcept;

public:
	subscribe_table_t()noexcept;
	subscribe_table_t( std::weak_ptr<distributed_ctx_t> ctx_)noexcept;

	~subscribe_table_t();
	void close()noexcept;

	subscribe_table_t(const subscribe_table_t&) = delete;
	subscribe_table_t& operator=(const subscribe_table_t &) = delete;

	std::unique_ptr<i_iol_ihandler_t> check_address(const line_num_t&, 
		std::unique_ptr<i_iol_ihandler_t> && obj, 
		invoke_context_trait ic);

	void addressed_exception(const line_num_t&,
		const _address_hndl_t &addr,
		std::unique_ptr<dcf_exception_t>&& e,
		invoke_context_trait ic) noexcept;

	void regline(const line_num_t& lid );

	/*! �������� �� ��������� ��������� ��� ���������� ��������������.
	\param id ������� �������������
	\param once ���� ������� true, ����������� �������� ���� ���, ����� �� ��� ���,
	���� �� ����� ��������� ������ ����������� � ������� ������ /ref unsubscribe_hndl_id.
	*/
	subscribe_invoker_result subscribe(const line_num_t& lid,
		async_space_t scx,
		const _address_hndl_t & id,
		subscribe_options opt,
		subscribe_message_handler && handler);

	/*! ����� �� ��������� ����������� ��� ���������� ��������������
	\param id ������������� ����������
	*/
	void unsubscribe(const line_num_t&, const _address_hndl_t & id )noexcept;

	/*! ����� �� ��������� ����������� ��� ��������� �����
	*/
	void close(const line_num_t&)noexcept;

	template<typename TFunction, typename ... TArgs>
	void lock(const line_num_t&, TFunction && hl, TArgs && ... args) {
		lck_scp_t lck(_lck);
		hl(std::forward<TArgs>(args)...);
		}
	};


class subscribe_multitable_t : public std::enable_shared_from_this<subscribe_multitable_t> {
public:
	/*using subscribe_handler_t = subscribe_table_t::subscribe_handler_t;*/
	using line_num_t = subscribe_table_t::line_num_t;
	using handler_t = message_subscribe_assist_t::handler_t;

private:
	std::atomic<line_num_t::id_t> _lineIdCounter{ 0 };
	std::vector<std::shared_ptr<subscribe_table_t>> _table;
	std::weak_ptr<distributed_ctx_t> _ctx;

public:
	size_t h2id(const handler_t & ln)const noexcept;

	std::unique_ptr<i_iol_ihandler_t> check_address(const handler_t & ln,
		std::unique_ptr<i_iol_ihandler_t> && obj,
		invoke_context_trait ic);

	void addressed_exception(const handler_t & ln,
		const _address_hndl_t &addr,
		std::unique_ptr<dcf_exception_t>&& e,
		invoke_context_trait ic)noexcept;

	subscribe_invoker_result subscribe(const handler_t & ln,
		async_space_t scx,
		const _address_hndl_t & id,
		subscribe_options opt,
		subscribe_message_handler && handler);

	void regline(const handler_t& ln );

	void unsubscribe(const handler_t & ln, const _address_hndl_t & id)noexcept;
	void close(const handler_t & ln)noexcept;

public:
	handler_t make_handler(rtl_level_t l)noexcept;
	subscribe_multitable_t(std::weak_ptr<distributed_ctx_t> ctx_,
		size_t wide);

	template<typename THandler>
	void lock(const handler_t & ln, THandler && hl) {
		_table.at(h2id(ln))->lock(ln.lineId, std::forward<THandler>(hl));
		}
	};
}

