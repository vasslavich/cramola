#pragma once


#include "../internal_invariants.h"
#include "../typeframe/options.h"
#include "./base_terms.h"
#include "./rtl_rdm_link.h"
#include "./ids_types.h"


namespace crm {

/*! ��������� �������� ���������� ��������� */
class address_descriptor_t final :
	public hf_operators_t<address_descriptor_t> {

private:
	typedef std::uint32_t flags_utype_t;
	enum class taglst_t : flags_utype_t {
		null = 0x00000000,
		id = 0x00000001,
		dest_id = 0x00000002,
		src_id = 0x00000004,
		session = 0x00000008,
		dest_subscrb_id = 0x00000010,
		src_subscrb_id = 0x00000020,

		level = 0x000000C0,
		level_0 = (flags_utype_t)detail::rtl_level_t::l0,
		level_1 = (flags_utype_t)detail::rtl_level_t::l1,

		spin_tag = 0x00000100,
		__next = 0x00000200
		};

	/*! ������������� ��������� */
	message_identity_type _id;
	/*! ������������� ���������� */
	identity_descriptor_t _destId;
	/*! ������������� ����������� */
	identity_descriptor_t _srcId;
	/*! ������������� ������ */
	session_description_t _session{ get_null<session_description_t>() };
	/*! ������������� ���������� ���������� �� �������� ������� */
	subscriber_node_id_t _destSubscriberId{ get_null<subscriber_node_id_t>() };
	/*! ������������� ���������� �������-���������� �� ����������� */
	subscriber_node_id_t _srcSubscriberId{ get_null < subscriber_node_id_t>() };
	/*! ������� ��� */
	message_spin_tag_type _spinTag{get_null<message_spin_tag_type>()};
	/*! ��� ��������� */
	address_hash_t _hash;
	/*! ���� ��������� */
	flags_utype_t _flags{ (flags_utype_t)taglst_t::null };

	void __set_flag(const taglst_t attribute)noexcept;

	template<const taglst_t flVal>
	void set_flag()noexcept {
		static_assert(taglst_t::null != flVal, "invalid value");
		__set_flag(flVal);
		}

	flags_utype_t __get_uflags(taglst_t attribute)const noexcept;

	template<const taglst_t flVal>
	flags_utype_t get_uflags()const  noexcept {
		static_assert(taglst_t::null != flVal, "invalid value");
		return __get_uflags(flVal);
		}

	bool __has_flag(taglst_t attribute)const noexcept;

	template<const taglst_t flVal>
	bool has_flag()const noexcept {
		static_assert(taglst_t::null != flVal, "invalid value");
		return __has_flag(flVal);
		}

	void update_hash()noexcept;

public:
	using hash_t = address_hash_t;

	/*! ����� r [�� ��������� �����]*/
	bool equals_fields(const address_descriptor_t& r)const noexcept;
	/*! ������ ��� r [�� ��������� �����]*/
	bool less_fields(const address_descriptor_t& r)const noexcept;
	/*! ������ ��� r [�� ��������� �����]*/
	bool great_fields(const address_descriptor_t& r)const noexcept;
	/*! ������ ��� ����� r [�� ��������� �����]*/
	bool great_equals_fields(const address_descriptor_t& r)const noexcept;

public:
	static const address_descriptor_t null;

	address_descriptor_t()noexcept;

	bool fl_id()const noexcept;
	bool fl_destination_id()const noexcept;
	bool fl_source_id()const noexcept;
	bool fl_session()const noexcept;
	bool fl_destination_subscribe_id()const noexcept;
	bool fl_source_subscribe_id()const noexcept;
	bool fl_level()const noexcept;

	const address_hash_t& object_hash()const noexcept;

	detail::rtl_level_t level()const noexcept;
	void set_level(detail::rtl_level_t);

	void set_id(const global_object_identity_t & id);
	const global_object_identity_t& id()const noexcept;

	void set_destination_id(const identity_descriptor_t & destId);
	const identity_descriptor_t& destination_id()const noexcept;

	void set_source_id(const identity_descriptor_t  & srcId);
	const identity_descriptor_t& source_id()const noexcept;

	void set_session(const session_description_t& sid);
	const session_description_t& session()const noexcept;

	void set_destination_subscriber_id(const subscriber_node_id_t & id);
	const subscriber_node_id_t& destination_subscriber_id()const noexcept;

	void set_source_subscriber_id(const subscriber_node_id_t & id);
	const subscriber_node_id_t& source_subscriber_id()const noexcept;

	void set_spin_tag(const message_spin_tag_type& id);
	const message_spin_tag_type& spin_tag()const noexcept;

	bool is_sendrecv_request()const noexcept;
	bool is_sendrecv_response()const noexcept;
	bool is_sendrecv_cycle()const noexcept;

	void set_from(const detail::rtl_roadmap_link_t & lnk);

	using serialize_options = srlz::detail::deduction_prefix_mode_t<>;

	template<typename Ts>
	void srlz(Ts && dest)const{

		crm::srlz::serialize(dest, _flags);
		srlz::serialize(dest, _hash, serialize_options{});

		if (has_flag< taglst_t::id>())
			srlz::serialize(dest, _id, serialize_options{});

		if (has_flag< taglst_t::dest_id>())
			srlz::serialize(dest, _destId, serialize_options{});

		if (has_flag< taglst_t::src_id>())
			srlz::serialize(dest, _srcId, serialize_options{});

		if (has_flag< taglst_t::session>())
			srlz::serialize(dest, _session, serialize_options{});

		if (has_flag< taglst_t::dest_subscrb_id>())
			srlz::serialize(dest, _destSubscriberId, serialize_options{});

		if (has_flag< taglst_t::src_subscrb_id>())
			srlz::serialize(dest, _srcSubscriberId, serialize_options{});

		if (has_flag< taglst_t::spin_tag>())
			srlz::serialize(dest, _spinTag, serialize_options{});
		}

	template<typename Ts>
	void dsrlz(Ts && src){
		crm::srlz::deserialize(src, _flags);
		crm::srlz::deserialize(src, _hash, serialize_options{});

		if (has_flag<  taglst_t::id>())
			crm::srlz::deserialize(src, _id, serialize_options{});

		if (has_flag<  taglst_t::dest_id>())
			crm::srlz::deserialize(src, _destId, serialize_options{});

		if (has_flag<  taglst_t::src_id>())
			crm::srlz::deserialize(src, _srcId, serialize_options{});

		if (has_flag<  taglst_t::session>())
			crm::srlz::deserialize(src, _session, serialize_options{});

		if (has_flag<  taglst_t::dest_subscrb_id>())
			crm::srlz::deserialize(src, _destSubscriberId, serialize_options{});

		if (has_flag<  taglst_t::src_subscrb_id>())
			crm::srlz::deserialize(src, _srcSubscriberId, serialize_options{});

		if (has_flag<  taglst_t::spin_tag>())
			crm::srlz::deserialize(src, _spinTag, serialize_options{});
		}

	/*! ����� ���������������� ������������� ������� */
	std::size_t get_serialized_size()const noexcept;

	/*address_descriptor_t get_echo()const noexcept;*/
	void set_echo(address_descriptor_t & a)const noexcept;

	bool is_null()const noexcept;
	std::string to_string()const noexcept;
	};

bool is_sendrecv_request(const address_descriptor_t & ad) noexcept;
bool is_sendrecv_response(const address_descriptor_t& ad) noexcept;
bool is_sendrecv_cycle(const address_descriptor_t& ad) noexcept;
bool check_sndrcv_key_spin_tag(const address_descriptor_t& ad) noexcept;
bool is_sndrcv_spin_tagged_invoke(const address_descriptor_t& h)noexcept;



void apply_at_hash(address_hash_t &to, const address_descriptor_t & line)noexcept;
}
