#pragma once


#include "./base_terms.h"
#include "./ids_types.h"


namespace crm::detail {


/*! �������� ����� ��� ������������� ������� ������ ������ ����,
����� ��������� 0-�� � 1-�� ������� */
struct rtl_roadmap_event_t final :
	public hf_operators_t<rtl_roadmap_event_t> {

	static const rtl_roadmap_event_t null;

private:
	/*! ���������� ������������� ������� */
	identity_descriptor_t _glid;
	/*! ��������� ������������� ������� � �������� ���� */
	subscriber_event_id_t _lcid;
	/*! ��������� ������� ������������� �� ���� */
	rtl_table_t _rt{ rtl_table_t::undefined };
	/*! �������� */
	rtl_level_t _l{ rtl_level_t::undefined };
	/*! ��� ������� */
	address_hash_t _hash;

public:
	using hash_t = address_hash_t;

	/*! ����� r [�� ��������� �����]*/
	bool equals_fields(const rtl_roadmap_event_t& r)const noexcept;
	/*! ������ ��� r [�� ��������� �����]*/
	bool less_fields(const rtl_roadmap_event_t& r)const noexcept;
	/*! ������ ��� r [�� ��������� �����]*/
	bool great_fields(const rtl_roadmap_event_t& r)const noexcept;
	/*! ������ ��� ����� r [�� ��������� �����]*/
	bool great_equals_fields(const rtl_roadmap_event_t& r)const noexcept;

private:
	rtl_roadmap_event_t(const identity_descriptor_t &,
		const subscriber_event_id_t&,
		rtl_table_t rt,
		rtl_level_t l)noexcept;

public:
	rtl_roadmap_event_t()noexcept;
	static rtl_roadmap_event_t make(const identity_descriptor_t &,
		const subscriber_event_id_t&,
		rtl_table_t rt,
		rtl_level_t l)noexcept;

	const address_hash_t& object_hash()const noexcept;
	const identity_descriptor_t& glid()const noexcept;
	const subscriber_event_id_t& lcid()const noexcept;
	const rtl_table_t& rt()const noexcept;
	rtl_level_t level()const noexcept;

	/*! ������������ ������� � �������� ������ */
	template<typename Ts>
	void srlz(Ts && dest)const {
		srlz::serialize(dest, _glid);
		srlz::serialize(dest, _lcid);
		srlz::serialize(dest, _rt);
		srlz::serialize(dest, _l);
		srlz::serialize(dest, _hash);
		}

	/*! �������������� ������� �� ��������� ������� */
	template<typename Ts>
	void dsrlz(Ts && src ){
		srlz::deserialize(src, _glid);
		srlz::deserialize(src, _lcid);
		srlz::deserialize(src, _rt);
		srlz::deserialize(src, _l);
		srlz::deserialize(src, _hash);
		}

	size_t get_serialized_size()const noexcept;
	bool is_null()const noexcept;
	};
}
