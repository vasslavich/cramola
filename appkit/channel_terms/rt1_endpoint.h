#pragma once

#include "./base_terms.h"
#include "./i_endpoint.h"


namespace crm {


class rt1_endpoint_t final: public i_endpoint_object_t {
private:
	identity_descriptor_t _targetId;
	std::string _baseAddress;
	int _baseId;

	rt1_endpoint_t(std::string_view streamName,
		int streamId,
		const identity_descriptor_t & endpointId);

public:
	static const rt1_endpoint_t null;

	rt1_endpoint_t()noexcept;

	static rt1_endpoint_t make_remote(std::string_view streamName,
		int streamId,
		const identity_descriptor_t & endpointId);

	static rt1_endpoint_t make_local(int streamId,
		const identity_descriptor_t& endpointId);

	const identity_descriptor_t& endpoint_id()const  noexcept final;
	const std::string& address()const noexcept final;
	int port()const noexcept final;

	std::string print()const noexcept;
	connection_key_t to_key()const noexcept final;
	std::unique_ptr<i_endpoint_t> copy()const  noexcept final;
	std::string to_string()const noexcept final;
	address_hash_t object_hash()const noexcept final;

	/*! ������������ ������� � �������� ������ */
	template<typename Ts>
	void srlz(Ts && dest)const{
		srlz::serialize(dest, _targetId);
		srlz::serialize(dest, _baseAddress);
		srlz::serialize(dest, _baseId);
		}

	/*! �������������� ������� �� ��������� ������� */
	template<typename Ts>
	void dsrlz(Ts && src){
		srlz::deserialize(src, _targetId);
		srlz::deserialize(src, _baseAddress);
		srlz::deserialize(src, _baseId);
		}

	size_t get_serialized_size()const noexcept;
	};

bool operator < (const rt1_endpoint_t & lval, const rt1_endpoint_t &rval)noexcept;
bool operator > (const rt1_endpoint_t & lval, const rt1_endpoint_t &rval)noexcept;
bool operator >= (const rt1_endpoint_t & lval, const rt1_endpoint_t &rval)noexcept;
bool operator==(const rt1_endpoint_t &l, const rt1_endpoint_t &r)noexcept;
bool operator!=(const rt1_endpoint_t &l, const rt1_endpoint_t &r)noexcept;
}

