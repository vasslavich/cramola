#pragma once


#include "../utilities/handlers/invoke_guard.h"
#include "../counters/internal_terms.h"
#include "subscribe_terms.h"
#include "ids_types.h"


namespace crm::detail{


class message_subscribe_assist_t{
public:
	struct line_num_t{
		using id_t = std::uintmax_t;

	private:
		id_t _id{ 0 };
		rtl_level_t _level{ rtl_level_t::undefined };

		friend bool operator==( const line_num_t& l, const line_num_t& r )noexcept;
		friend bool operator!=( const line_num_t& l, const line_num_t& r )noexcept;
		friend bool operator>=( const line_num_t& l, const line_num_t& r )noexcept;
		friend bool operator<( const line_num_t& l, const line_num_t& r )noexcept;

	public:
		line_num_t()noexcept;
		line_num_t(const id_t& id_, rtl_level_t l)noexcept;

		const id_t& id()const noexcept;
		rtl_level_t level()const noexcept;
		address_hash_t object_hash()const noexcept;
		};

	struct subscribe_handler_base{
		virtual ~subscribe_handler_base(){}

		virtual void reset_handler(trace_tag&& tag_, subscribe_message_handler && )noexcept = 0;
		virtual void operator()( std::unique_ptr<dcf_exception_t> && e, invoke_context_trait ic)noexcept = 0;
		virtual void operator()( std::unique_ptr<i_iol_ihandler_t> && v, invoke_context_trait ic) = 0;
		virtual operator bool()const noexcept = 0;
		virtual void reset()noexcept = 0;
		virtual bool once()const noexcept = 0;
		virtual size_t invoked_count()const noexcept = 0;
		virtual size_t invoked_max_count()const noexcept = 0;
		};


	struct subscribe_message_pointer{
		std::unique_ptr<subscribe_handler_base> _p;

		subscribe_message_pointer()noexcept{}

		template<typename TSubscribeHandlerType,
			typename std::enable_if_t<std::is_base_of_v<subscribe_handler_base, std::decay_t<TSubscribeHandlerType>>, int> = 0>
			subscribe_message_pointer( TSubscribeHandlerType && h )noexcept
			: _p( std::make_unique<std::decay_t<TSubscribeHandlerType>>( std::forward<TSubscribeHandlerType>( h ) ) ){}

		template<typename TSubscribeHandlerType,
			typename std::enable_if_t<std::is_base_of_v<subscribe_handler_base, std::decay_t<TSubscribeHandlerType>>, int> = 0>
			subscribe_message_pointer( std::unique_ptr<TSubscribeHandlerType> && h )noexcept
			: _p( std::move( h ) ){}

		void operator()( std::unique_ptr<dcf_exception_t> && e, invoke_context_trait ic)noexcept {
			if (_p)
				(*_p)(std::move(e), ic);
			}

		void operator()( std::unique_ptr<i_iol_ihandler_t> && v, invoke_context_trait ic) {
			if (_p)
				(*_p)(std::move(v), ic);
			}

		operator bool()const noexcept {
			return _p && (*_p);
			}

		void reset_handler(trace_tag&& tag_, subscribe_message_handler && h)noexcept {
			if (_p)
				_p->reset_handler(std::move(tag_), std::move(h));
			}

		void reset()noexcept {
			if (_p)
				_p->reset();
			}

		bool once()const noexcept {
			if (_p) {
				return _p->once();
				}
			else {
				return true;
				}
			}

		size_t invoked_max_count()const noexcept {
			if (_p) {
				return _p->invoked_max_count();
				}
			else {
				return 0;
				}
			}

		size_t invoked_count()const noexcept {
			if (_p) {
				return _p->invoked_count();
				}
			else {
				return (size_t)(-1);
				}
			}
		};

private:

	template<typename TFunction>
	struct functor_wrapper;

	template<>
	struct functor_wrapper<subscribe_message_handler::functor_type> : subscribe_message_handler::functor_type{
		using base_type = subscribe_message_handler::functor_type;

		functor_wrapper()noexcept{}

		functor_wrapper( base_type && f )noexcept
			:base_type( std::move( f ) ){}

		void operator()(std::unique_ptr<dcf_exception_t>&& e, invoke_context_trait ic = {})noexcept {
			base_type::invoke( std::move( e ), nullptr, ic );
			}

		void operator()(std::unique_ptr<i_iol_ihandler_t>&& v, invoke_context_trait ic = {}) {
			base_type::invoke( nullptr, std::move( v ), ic );
			}

		void operator()(std::unique_ptr<dcf_exception_t>&& e, std::unique_ptr<i_iol_ihandler_t>&& m, invoke_context_trait ic = {}) {
			base_type::invoke( std::move( e ), std::move( m ), ic );
			}
		};

	template<>
	struct functor_wrapper<subscribe_message_handler::functor_shared_type> : subscribe_message_handler::functor_shared_type{
		using base_type = subscribe_message_handler::functor_shared_type;

		functor_wrapper()noexcept{}

		functor_wrapper( base_type && f )noexcept
			:base_type( std::move( f ) ){}

		void operator()(std::unique_ptr<dcf_exception_t>&& e, invoke_context_trait ic = {})noexcept {
			base_type::invoke( std::move( e ), nullptr, ic );
			}

		void operator()(std::unique_ptr<i_iol_ihandler_t>&& v, invoke_context_trait ic = {}) {
			base_type::invoke( nullptr, std::move( v ), ic );
			}

		void operator()(std::unique_ptr<dcf_exception_t>&& e, std::unique_ptr<i_iol_ihandler_t>&& m, invoke_context_trait ic = {}) {
			base_type::invoke( std::move( e ), std::move( m ), ic );
			}
		};

	template<typename functor_type>
	struct subscribe_handler_typeid final : subscribe_handler_base{
		using subscribe_with_tail_exception_guard = typename functor_invoke_guard_t<functor_wrapper<functor_type>, make_fault_result_t>;
		using self_type = typename subscribe_handler_typeid<functor_type>;

		subscribe_with_tail_exception_guard hndl;
		inovked_as _ias;

#ifdef CBL_OBJECTS_COUNTER_CHECKER
		utility::__xobject_counter_logger_t<self_type, 10> _xDbgCounter;
#endif


		template<typename THandler,
			typename std::enable_if_t<(
				std::is_base_of_v<subscribe_message_handler::functor_base_type, std::decay_t<THandler>> &&
				!std::is_base_of_v<subscribe_handler_base, std::decay_t<THandler>>), int> = 0>
			subscribe_handler_typeid(trace_tag && tag_,
				THandler && h,
				bool once_,
				std::weak_ptr<distributed_ctx_t> ctx_,
				inovked_as ias_,
				async_space_t scx )noexcept
			: hndl( std::move( h ),
				std::move( ctx_ ),
				!tag_.name.empty() ? tag_ : typeid(self_type).name(),
				scx,
				once_ ? subscribe_with_tail_exception_guard::invoke_once : subscribe_with_tail_exception_guard::invoke_indefinitely )
			, _ias{ ias_ }{}

		void operator()(std::unique_ptr<dcf_exception_t>&& e, invoke_context_trait ic)noexcept final {
			if (ic == invoke_context_trait::can_be_used_as_async) {
				hndl.invoke_explicit(std::move(e), nullptr, ic);
				}
			else if (_ias == inovked_as::async) {
				hndl.invoke_async(std::move(e), nullptr, ic);
				}
			else {
				hndl.invoke_explicit(std::move(e), nullptr, ic);
				}
			}

		void operator()(std::unique_ptr<i_iol_ihandler_t>&& v, invoke_context_trait ic)final {
			if (ic == invoke_context_trait::can_be_used_as_async) {
				hndl.invoke_explicit(nullptr, std::move(v), ic);
				}
			else if (_ias == inovked_as::async) {
				hndl.invoke_async(nullptr, std::move(v), ic);
				}
			else {
				hndl.invoke_explicit(nullptr, std::move(v), ic);
				}
			}

		operator bool()const noexcept final{
			return hndl;
			}

		void reset_handler(trace_tag&& tag_, subscribe_message_handler&& ph )noexcept final{
			hndl.reset_handler( std::move(tag_), release_as<functor_type>( std::move( ph ) ) );
			}

		void reset()noexcept final{
			hndl.clear();
			}

		bool once()const noexcept final{
			return hndl.is_once();
			}
		
		size_t invoked_count()const noexcept final {
			return hndl.invoked_count();
			}

		size_t invoked_max_count()const noexcept final {
			return hndl.invoked_max_count();
			}
		};

	using subscribe_handler_unique = subscribe_handler_typeid<subscribe_message_handler::functor_type>;
	using subscribe_handler_shared = subscribe_handler_typeid<subscribe_message_handler::functor_shared_type>;

public:
	static subscribe_message_pointer make_handler(trace_tag&& tag_,
		subscribe_message_handler && h,
		bool once_,
		std::weak_ptr<distributed_ctx_t> ctx_,
		async_space_t scx )noexcept;


	/*! ���������� ��������� ��������� */
	struct message_addressed_link_t{
		subscribe_message_pointer hndl;
		uint8_t tagz{ 0 };
		std::weak_ptr<distributed_ctx_t> _ctx;
		bool abandonedInvoke{ true };

#ifdef CBL_SUBSCRIBE_TABLE_OBJECTS_COUNTER
		crm::utility::__xobject_counter_logger_t<message_addressed_link_t, 10> _xDbgCounter;
#endif

		message_addressed_link_t()noexcept;

		message_addressed_link_t(trace_tag&& tag_,
			subscribe_message_handler && h,
			bool once_,
			std::weak_ptr<distributed_ctx_t> ctx_,
			async_space_t scx,
			bool abandonedInvoke_ = true,
			uint8_t tagz_ = 0 )noexcept;

		void operator()( std::unique_ptr<i_iol_ihandler_t> && o, invoke_context_trait ic );
		void operator()( std::unique_ptr<dcf_exception_t> && e, invoke_context_trait ic)noexcept;

		void reset()noexcept;
		operator bool()const noexcept;
		bool once()const noexcept;
		};


	class handler_t{
		friend class subscribe_multitable_t;

	private:
		line_num_t lineId;
		std::weak_ptr<subscribe_multitable_t> _wtbl;

	#ifdef CBL_OBJECTS_COUNTER_CHECKER
		utility::__xobject_counter_logger_t<handler_t, 1> _xDbgCounter;
	#endif

	private:
		handler_t( line_num_t && lnId, std::weak_ptr<subscribe_multitable_t> );

	public:
		~handler_t();
		handler_t()noexcept;
		void close()noexcept;

		handler_t( const handler_t& ) = delete;
		handler_t& operator=( const handler_t & ) = delete;

		handler_t( handler_t && )noexcept;
		handler_t& operator=( handler_t && )noexcept;

		std::unique_ptr<i_iol_ihandler_t> check_address( std::unique_ptr<i_iol_ihandler_t> && obj, 
			invoke_context_trait ic );

		void addressed_exception( const _address_hndl_t &addr, 
			std::unique_ptr<dcf_exception_t>&& e,
			invoke_context_trait ic )noexcept;

		subscribe_invoker_result subscribe_set_handler( const _address_hndl_t & id,
			async_space_t scx,
			subscribe_options opt,
			subscribe_message_handler && handler );
		void subscribe_unset_handler( const _address_hndl_t & id )noexcept;

		template<typename TFunction, typename ... TArgs>
		void lock( TFunction && hl, TArgs && ... args ){
			if( auto stbl = _wtbl.lock() ){
				return stbl->lock( (*this), std::forward<TFunction>( hl ), std::forward<TArgs>( args )... );
				}
			else{
				THROW_MPF_EXC_FWD(nullptr);
				}
			}
		};
	};
}


template<>
struct ::std::hash <crm::detail::message_subscribe_assist_t::line_num_t> {
	constexpr size_t operator()(crm::detail::message_subscribe_assist_t::line_num_t const& v) const noexcept {
		return crm::as_std_hash(v);
		}
	};



