#pragma once


#include <array>
#include "../internal_invariants.h"


namespace crm::detail {


class iol_type_map_t {
public:
	typedef uint64_t packed_type_t;
	typedef uint64_t packed_length_t;
	typedef uint32_t packed_crc_t;

	static const packed_type_t packed_type_undefined = ((packed_type_t)-1);

	static const uint64_t maxval_range = sizeof(packed_type_t) * 8;

	struct field_info_t {
		static const field_info_t undefined;

		uint32_t maskBits;
		uint8_t offsetBits;
		uint8_t lengthBits;

		static size_t max_value(uint8_t lengthBits) noexcept {
			return (size_t)1 << lengthBits;
			}

		template<const uint8_t offsetBits_,
			const uint8_t lengthBits_,
			const uint32_t maskBits_>
			static field_info_t make()noexcept {
			static_assert(maskBits_ > 0, "invalid value");

			return __make<offsetBits_, lengthBits_, maskBits_>();
			}

		static field_info_t make_undefined()noexcept {
			return __make<0, 0, 0>();
			}

		size_t max_value()const noexcept {
			return max_value(lengthBits);
			}

	private:
		template<const uint8_t offsetBits_,
			const uint8_t lengthBits_,
			const uint32_t maskBits_>
			static field_info_t __make()noexcept {

			static_assert((offsetBits_ + lengthBits_) <= maxval_range, "invalid value");
			static_assert((size_t)maskBits_ != ((size_t)1 << lengthBits_), "invalid value");

			field_info_t obj;
			obj.maskBits = maskBits_;
			obj.offsetBits = offsetBits_;
			obj.lengthBits = lengthBits_;

			return obj;
			}
		};

private:
	std::array<field_info_t, (size_t)iol_type_attributes_t::__count> _map;

	iol_type_map_t();
	static iol_type_map_t _instance;

public:
	static const iol_type_map_t& instance()noexcept;
	field_info_t collate(const iol_type_attributes_t index)const  noexcept;
	};
}
