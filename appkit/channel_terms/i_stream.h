#pragma once


#include "./base_terms.h"
#include "./message_frame.h"
#include "./i_requirements.h"
#include "./subscribe_terms.h"
#include "../queues/i_message_queue.h"


namespace crm {
namespace detail {


class stream_route_itable_t;


class i_stream_x_irt1_t {
public:
	virtual ~i_stream_x_irt1_t() = 0;
	};

class i_stream_x_ort1_t {
public:
	virtual ~i_stream_x_ort1_t() = 0;

	virtual void push_to_x(outbound_message_unit_t&& buf) = 0;
	};

struct i_decode_tail_stream2peer_t_x1 {
	virtual ~i_decode_tail_stream2peer_t_x1() {}

	virtual void close()noexcept = 0;
	virtual void decode(message_frame_unit_t&& p) = 0;
	virtual void addressed_exception(const _address_hndl_t& address,
		std::unique_ptr<dcf_exception_t>&& ntf,
		invoke_context_trait)noexcept = 0;
	};


struct i_xpeer_rt1_base_t;

struct i_x1_stream_address_handler {
	virtual ~i_x1_stream_address_handler();

	static const std::string null_name;
	static const std::string null_address;
	static const int null_port;

	using rdm_local_identity_t = rtl_roadmap_obj_t;
	using remote_object_endpoint_t = rt1_endpoint_t;
	using xpeer_desc_t = rdm_local_identity_t;

	struct input_value_t {
	private:
		friend class xpeer_base_rt1_t;

		std::unique_ptr<i_iol_ihandler_t> m_;
		xpeer_desc_t d_;
		std::weak_ptr< i_xpeer_rt1_base_t> _xpeerWlnk;

		input_value_t()noexcept;
		input_value_t(std::unique_ptr<i_iol_ihandler_t>&& m,
			const xpeer_desc_t& d,
			std::weak_ptr< i_xpeer_rt1_base_t> xpeerWlnk)noexcept;

	public:
		const xpeer_desc_t& desc()const noexcept;
		const std::unique_ptr<i_iol_ihandler_t>& m()const& noexcept;
		std::unique_ptr<i_iol_ihandler_t> m() && noexcept;

		template<typename TMessage,
			typename std::enable_if_t<is_out_message_requirements_v<TMessage>, int> = 0>
			void send_back(TMessage&& m) {
			if (auto sxp = _xpeerWlnk.lock()) {
				(static_cast<xpeer_base_rt1_t&>(*sxp)).push(std::forward<TMessage>(m));
			}
		}
		};

	typedef i_message_input_t<input_value_t> input_stack_t;

	using output_value_t = std::unique_ptr<i_iol_ohandler_t>;
	typedef i_message_output_t<output_value_t> output_stack_t;

	typedef remote_object_state_t state_t;

	/*! ������� �������� ������� �������� ��������� ��� �������� 0-�� ������ */
	using ctr_1_input_queue_t = std::function<std::unique_ptr<input_stack_t>()>;


	virtual const rtl_roadmap_obj_t& this_rdm()const noexcept;
	virtual const rtl_roadmap_obj_t& remote_rdm()const noexcept;
	virtual const identity_descriptor_t& remote_id()const noexcept;
	virtual const identity_descriptor_t& this_id()const noexcept;
	virtual const identity_descriptor_t& id()const noexcept;
	virtual const xpeer_desc_t& desc()const noexcept;
	virtual const rt0_node_rdm_t& node_id()const noexcept;
	virtual const local_object_id_t& local_id()const noexcept;
	virtual const local_command_identity_t& local_object_command_id()const noexcept;
	virtual const session_description_t& session()const noexcept;
	virtual const std::string& address()const noexcept;
	virtual int port()const noexcept;
	virtual remote_object_endpoint_t remote_object_endpoint()const noexcept;
	virtual connection_key_t connection_key()const noexcept;
	virtual rtl_table_t direction()const noexcept;

	const std::string& name()const noexcept;
	remote_object_state_t state()const noexcept;
	void set_name(std::string n_);
	void set_state(remote_object_state_t st)noexcept;
	rtl_level_t level()const noexcept;

private:
	std::string _name;
	std::atomic<remote_object_state_t> _state{ remote_object_state_t::null };
	};


struct i_rt1_main_router_t;

class base_stream_x_rt1_t :
	public virtual i_stream_x_irt1_t,
	public virtual i_stream_x_ort1_t {

public:
	typedef remote_object_state_t state_t;

	virtual std::shared_ptr<i_rt1_main_router_t> router()const noexcept = 0;
	virtual std::shared_ptr<i_x1_stream_address_handler> address_point()const noexcept = 0;
	virtual rtl_table_t direction()const noexcept = 0;
	virtual const rtl_roadmap_event_t& ev_rdm()const noexcept = 0;
	virtual const rtl_roadmap_obj_t& msg_rdm()const noexcept = 0;
	virtual const rtl_roadmap_link_t& msg_rdm_link()const noexcept = 0;
	virtual const rtl_roadmap_obj_t& this_rdm()const noexcept = 0;
	virtual const rtl_roadmap_obj_t& remote_rdm()const noexcept = 0;
	virtual const identity_descriptor_t& remote_id()const noexcept = 0;
	virtual const identity_descriptor_t& this_id()const noexcept = 0;
	virtual const rt0_node_rdm_t& node_id()const noexcept = 0;
	virtual const local_object_id_t& local_id()const noexcept = 0;
	virtual const session_description_t& session()const noexcept = 0;
	virtual void close_from_router()noexcept = 0;
	virtual void close()noexcept = 0;
	virtual bool closed()const noexcept = 0;
	virtual void set_state(remote_object_state_t st)noexcept = 0;
	virtual remote_object_state_t get_state()const noexcept = 0;
	rt1_streams_table_wait_key_t rt1_stream_table_key()const noexcept {
		return rt1_streams_table_wait_key_t(node_id(), msg_rdm_link(), msg_rdm().table());
		}

	virtual bool is_integrity_provided()const noexcept = 0;
	virtual bool set_tail(std::shared_ptr<i_decode_tail_stream2peer_t_x1>&& tail_) noexcept = 0;
	};

class istream_x_rt1_t :
	public base_stream_x_rt1_t {};

class ostream_x_rt1_t :
	public base_stream_x_rt1_t {

public:
	virtual const rt1_endpoint_t& endpoint()const = 0;
	virtual source_object_key_t source_key()const = 0;
	};


class i_stream_rt0_x_t {
public:
	virtual bool pop(binary_vector_t& buf,
		const std::chrono::milliseconds& wait) = 0;
	virtual void push(_packet_t&& packet) = 0;
	};
}
}

