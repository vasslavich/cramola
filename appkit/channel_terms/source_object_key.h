#pragma once


#include "./rt1_endpoint.h"
#include "./session_desc.h"


namespace crm {


class source_object_key_t final:
	public hf_operators_t<source_object_key_t> {

public:
	static const source_object_key_t null;

private:
	rt1_endpoint_t _epKey;
	identity_descriptor_t _glid;
	session_description_t _sessionId;
	address_hash_t _hash;

public:
	using hash_t = address_hash_t;

	/*! ����� r [�� ��������� �����]*/
	bool equals_fields(const source_object_key_t& r)const noexcept;
	/*! ������ ��� r [�� ��������� �����]*/
	bool less_fields(const source_object_key_t& r)const noexcept;
	/*! ������ ��� r [�� ��������� �����]*/
	bool great_fields(const source_object_key_t& r)const noexcept;
	/*! ������ ��� ����� r [�� ��������� �����]*/
	bool great_equals_fields(const source_object_key_t& r)const noexcept;

private:
	void update_hash()noexcept;

public:
	template<typename Ts>
	void srlz(Ts && dest)const{
		srlz::serialize(dest, _epKey);
		srlz::serialize(dest, _glid);
		srlz::serialize(dest, _sessionId);
		srlz::serialize(dest, _hash);
		}

	template<typename Ts>
	void dsrlz(Ts && src ){
		srlz::deserialize(src, _epKey);
		srlz::deserialize(src, _glid);
		srlz::deserialize(src, _sessionId);
		srlz::deserialize(src, _hash);
		}

	size_t get_serialized_size()const noexcept;

private:
	source_object_key_t(const rt1_endpoint_t & ck,
		const session_description_t & s,
		const identity_descriptor_t & glid )noexcept;

public:
	source_object_key_t()noexcept;

	static source_object_key_t make(const rt1_endpoint_t & ck,
		const session_description_t & s,
		const identity_descriptor_t & glid )noexcept;

	bool is_null()const noexcept;
	const address_hash_t& object_hash()const noexcept;

	const rt1_endpoint_t& connection_key()const noexcept;
	const session_description_t& session()const noexcept;
	const identity_descriptor_t& object_id()const noexcept;
	};
}

