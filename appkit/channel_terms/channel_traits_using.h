#pragma once

#include "./channel_traits_decls.h"

namespace crm::detail {

template<typename T>
constexpr bool has_event_class_id_v = has_event_class_id<std::decay_t<T>>::value;

template<typename T>
constexpr bool has_event_member_type_id_v = has_event_member_type_id<T>::value;

template<typename TBase, typename TDerived,
	typename _TBase = typename std::decay_t<TBase>,
	typename _TDerived = typename std::decay_t<TDerived>>
constexpr bool is_event_cast_v = (
		std::is_class_v<_TBase> &&
		std::is_polymorphic_v<_TDerived> &&
		std::has_virtual_destructor_v<_TBase> &&
		std::is_base_of_v<_TBase, _TDerived> &&
		std::is_final_v<_TDerived> &&
		has_event_class_id_v<_TDerived> &&
		has_event_member_type_id_v<_TDerived>
		);
}
