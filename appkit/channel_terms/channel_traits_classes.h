#pragma once

#include "./channel_traits_decls.h"

namespace crm::detail {


template<typename T>
struct has_event_class_id<T, std::void_t<
	std::enable_if_t<std::is_constructible_v<srlz::typemap_key_t, decltype(T::class_type_id())>>
	>> : std::true_type{};

template<typename T>
struct has_event_member_type_id<T, std::void_t<
	std::enable_if_t<(
		std::is_constructible_v<srlz::typemap_key_t, decltype(std::declval<const T&>().type_id())>
		)>
	>> : std::true_type {};
}
