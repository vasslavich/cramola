#pragma once


#include "../internal_invariants.h"


namespace crm::detail {

class crc_16 {
private:
	uint16_t _crc;

public:
	typedef uint16_t base_value_t;

	crc_16(base_value_t startValue = 0xFFFF);

	/*! ��������� ������������� � ������� /ref rstream_iterator_adaptor_t */
	static const bool has_action = true;

	void reset(base_value_t startValue = 0xFFFF);
	void push(uint8_t byte);
	void operator()(uint8_t byte);
	base_value_t value()const;
	base_value_t current()const;
	};

class crc_32 {
private:
	static const uint32_t start_value = 0xFFFFFFFF;
	uint32_t _crc = start_value;
	std::uintmax_t _count = 0;

public:
	typedef uint32_t base_value_t;

	crc_32(base_value_t startValue = start_value);

	/*! ��������� ������������� � ������� /ref rstream_iterator_adaptor_t */
	static const bool has_action = true;

	void reset(base_value_t startValue = start_value);
	void push(uint8_t byte);
	std::uintmax_t push_count()const;

	template<typename TInIt>
	void push(typename TInIt first, typename TInIt last/*,
		typename std::enable_if<crm::srlz::detail::is_byte<decltype(*(std::declval<typename TInIt>()))>::value>::type *= nullptr*/) {

		for(; first != last; ++first) {
			push((*first));
			}
		}

	void operator()(uint8_t byte);
	base_value_t value()const;
	base_value_t current()const;
	};

/*! ��� ����������� ����� */
typedef crc_32 crc_t;
}
