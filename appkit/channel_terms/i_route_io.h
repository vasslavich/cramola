#pragma once


#include "./i_channel_io.h"
#include "./rt0_xpeer_input.h"


namespace crm {
namespace detail {



/*! ������������� � ����� ������ 0 � �������� ������������� ������ 0 */
struct i_rt0_main_router_input_t {
public:
	virtual ~i_rt0_main_router_input_t() = 0;
	virtual void evhndl_from_peer(rt0_x_message_unit_t&& msg) = 0;
	virtual void evhndl_form_peer_rt_id(rt0_X_item_t&& obj) = 0;
	};
}


}

