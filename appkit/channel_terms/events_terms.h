#pragma once


#include <functional>
#include "./subscribe_event_addr.h"
#include "./i_events.h"
#include "../utilities/handlers/invoke_guard.h"
#include "../counters/internal_terms.h"


namespace crm::detail{


class events_multitable_t;


struct make_fault_event_result_t{
	make_fault_event_result_t()noexcept;

	std::unique_ptr<event_abandoned_t> operator()( std::string && )noexcept;
	std::unique_ptr<event_abandoned_t> operator()()noexcept;
	};


struct __event_handler_f{
	crm::utility::invoke_functor<void, std::unique_ptr<i_event_t> &&, subscribe_invoke>  _h;

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	utility::__xobject_counter_logger_t<__event_handler_f, 10> _xDbgCounter;
#endif

	__event_handler_f()noexcept;

	template<typename TX,
		typename std::enable_if_t<(
			!std::is_base_of_v<typename std::decay_t<TX>, __event_handler_f> &&
			is_event_callback_invokable_v<TX>), int> = 0 >
		__event_handler_f( TX && h_ )
		: _h( std::forward<TX>( h_ ) ){}

	void operator()( std::unique_ptr<i_event_t> && ea, subscribe_invoke as );
	void operator()( std::unique_ptr<i_event_t> && ea );

	operator bool()const noexcept;
	};


class __event_handler_point_f{
	using function_handler_type = functor_invoke_guard_t<
		crm::utility::invoke_functor_shared<void, std::unique_ptr<i_event_t> &&>,
		make_fault_event_result_t>;

	std::shared_ptr<function_handler_type> _ph;

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	utility::__xobject_counter_logger_t<__event_handler_point_f, 10> _xDbgCounter;
#endif

public:
	__event_handler_point_f()noexcept;

	template<typename TX,
		typename std::enable_if_t<is_event_point_callback_invokable_v<TX>, int> = 0 >
		__event_handler_point_f( TX && h_,
			std::weak_ptr<distributed_ctx_t> ctx_,
			async_space_t scx )
		: _ph( std::make_unique<function_handler_type>( std::forward<TX>( h_ ), ctx_, "__event_handler_point_f", scx, function_handler_type::invoke_indefinitely ) ){}

	void operator()( std::unique_ptr<i_event_t> && ea );

	operator bool()const noexcept;
	};


struct _event_handler_t{
	_address_event_t id;
	bool once = true;
	__event_handler_f hndl;

#ifdef CBL_MPF_OBJECTS_COUNTER_CHECKER_XPEER_EVENTS
	crm::utility::__xobject_counter_logger_t<_event_handler_t> _xDbgCounter;
#endif

	_event_handler_t()noexcept{}

	template<typename THandler>
	_event_handler_t( const _address_event_t & id_, bool once_,
		THandler && h_ )
		: id( id_ )
		, once( once_ )
		, hndl( std::forward<THandler>( h_ ) ){}
	};


class events_list;

class event_subscribe_assist_t{
public:

	using address_desc = _address_event_t;
	using check_condition_f = std::function<std::unique_ptr<i_event_t>( event_type_t )>;
	using subscribe_with_tail_exception_guard = functor_invoke_guard_t<__event_handler_f, make_fault_event_result_t>;
	using line_num_t = std::uintmax_t;

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	utility::__xobject_counter_logger_t<event_subscribe_assist_t, 10> _xDbgCounter;
#endif

	/*! ���������� ��������� ��������� */
	struct event_addressed_link_t{
		subscribe_with_tail_exception_guard hndl;
		address_desc subscriberId;
		bool once = true;

#ifdef CBL_OBJECTS_COUNTER_CHECKER
		utility::__xobject_counter_logger_t<event_addressed_link_t, 10> _xDbgCounter;
#endif

		event_addressed_link_t( __event_handler_f && h,
			const address_desc & id_,
			bool once_,
			std::weak_ptr<distributed_ctx_t> ctx_,
			async_space_t scx );
		};


	class event_handler_t{
		friend class events_multitable_t;

	private:
		line_num_t lineId;
		std::weak_ptr<events_multitable_t> _wtbl;

	#ifdef CBL_OBJECTS_COUNTER_CHECKER
		utility::__xobject_counter_logger_t<event_handler_t, 10> _xDbgCounter;
	#endif

	private:
		event_handler_t( line_num_t lnId, std::weak_ptr<events_multitable_t> );

		subscribe_result _subscribe( async_space_t scx,
			const address_desc & id,
			bool once,
			__event_handler_f && handler,
			check_condition_f && checkPred );

	public:
		~event_handler_t();
		event_handler_t()noexcept;
		void close()noexcept;

		event_handler_t( const event_handler_t& ) = delete;
		event_handler_t& operator=( const event_handler_t & ) = delete;

		event_handler_t( event_handler_t && )noexcept;
		event_handler_t& operator=( event_handler_t && )noexcept;

		void invoke( event_type_t type,
			std::unique_ptr<event_args_t> && args )noexcept;

		subscribe_result subscribe( async_space_t scx,
			const address_desc & id,
			bool once,
			__event_handler_f && handler,
			check_condition_f && checkPred );

		events_list release()noexcept;
		void add( events_list && el );

		void unsubscribe( const address_desc & id )noexcept;
		void unsubscribe( const channel_identity_t & chid )noexcept;

		friend bool operator==( const event_handler_t& l, const event_handler_t& r )noexcept;
		friend bool operator!=( const event_handler_t& l, const event_handler_t& r )noexcept;
		};
	};


class events_list{
	friend class events_table_t;
	friend class events_multitable_t;

	using line_map_t = std::list<std::shared_ptr<event_subscribe_assist_t::event_addressed_link_t>>;

	line_map_t lst;

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	utility::__xobject_counter_logger_t<events_list, 10> _xDbgCounter;
#endif

	events_list( line_map_t && lst_ )
		: lst( std::move( lst_ ) ){}

public:
	events_list(){}

	events_list( const events_list & ) = delete;
	events_list& operator=( const events_list & ) = delete;

	events_list( events_list && o )noexcept
		: lst( std::move( o.lst ) ){}

	events_list& operator=( events_list && o )noexcept{
		if( this != std::addressof( o ) ){
			lst = std::move( o.lst );
			}

		return (*this);
		}
	};
}
