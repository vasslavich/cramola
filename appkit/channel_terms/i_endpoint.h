#pragma once


#include <memory>
#include <string>
#include "./base_terms.h"
#include "./connection_key.h"


namespace crm {

struct i_endpoint_t {
	virtual ~i_endpoint_t() = 0;

	virtual const std::string& address()const noexcept = 0;
	virtual int port()const noexcept = 0;
	virtual connection_key_t to_key()const noexcept = 0;
	virtual std::unique_ptr<i_endpoint_t> copy()const noexcept = 0;
	virtual std::string to_string()const noexcept = 0;
	virtual address_hash_t object_hash()const noexcept = 0;
	};

struct i_endpoint_object_t : public i_endpoint_t {
	virtual const identity_descriptor_t& endpoint_id()const noexcept = 0;
	};
}
