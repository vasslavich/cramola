#pragma once


#include <memory>
#include <vector>
#include <thread>
#include <optional>
#include "../internal_invariants.h"
#include "../xml/xml.h"
#include "./timetypes.h"


namespace crm {


using measurement_time_t = std::chrono::seconds;
using measurement_time_base_t = decltype(std::declval<measurement_time_t>().count());

struct errem_points_t {
	measurement_time_base_t l{};
	measurement_time_base_t u{};
	measurement_time_base_t inc{};
	};



/*! �������� ���������� �������� ����� */
class distributed_ctx_policies_t {
private:
	size_t _threadPoolSize ;
	size_t _threadPoolExternActors;
	size_t _threadPoolStackSize;
	size_t _ioBufferMaxlen;
	size_t _ioThreadsCount;
	size_t _rt0_StackMaxLen;
	double _poolLoadingMFactor;
	std::wstring _uploadFolder;
	std::chrono::milliseconds _timeout_rt0rt1_command_execute;
	std::chrono::milliseconds _timeoutProbeAsync;
	std::chrono::milliseconds _timeoutWaitIO;
	std::chrono::milliseconds _timeoutThreadPoolWaitItem;
	std::chrono::milliseconds _timeoutInactivity;
	std::chrono::milliseconds _timeoutReconnectAttempt;
	sndrcv_timeline_periodicity_t _connectionTL;

	sndrcv_timeline_periodicity_t _timoutPing;
	sndrcv_timeline_periodicity_t _waitRequestIdentity;
	sndrcv_timeline_periodicity_t _sendrecvIdentity;
	sndrcv_timeline_periodicity_t _sendrecvOkConnection;
	sndrcv_timeline_periodicity_t _sendrecvInitConnection;
	sndrcv_timeline_periodicity_t _timeouteSndRcv;

	errem_generator_mode _errmodeConnection0{ errem_generator_mode::inc };
	errem_generator_mode _errmodeConnection1{ errem_generator_mode::inc };
	errem_generator_mode _errmode{ errem_generator_mode::rand };

	errem_points_t _erremPoints0;
	errem_points_t _erremPoints1;

	bool _pingRt;
	bool _useCrc;

protected:
	static sndrcv_timeline_t make_timeline(const sndrcv_timeline_periodicity_t & p);
	static std::optional<sndrcv_timeline_periodicity_t>  make_timeline(const sndrcv_timeline_periodicity_t::timeline_t & t,
		const sndrcv_timeline_periodicity_t::timeline_t & i);

public:

	static std::string cfg_mpf_options_root_node() noexcept;
	static std::string cfg_item_timeout_attribute_timeout() noexcept;
	static std::string cfg_item_timeout_attribute_interval() noexcept;
	static std::string cfg_netwk_settings_root() noexcept;
	static std::string cfg_threadpool_maxsize() noexcept;
	static std::string cfg_threadpool_extern_actors_maxsize() noexcept;
	static std::string cfg_threadpool_stack_size() noexcept;
	static std::string cfg_io_buffer_maxsize() noexcept;
	static std::string cfg_io_threads_count() noexcept;
	static std::string cfg_timeout_ping() noexcept;
	static std::string cfg_timeout_inactivity() noexcept;
	static std::string cfg_connection_section() noexcept;
	static std::string cfg_connection_remote_timeout() noexcept;
	static std::string cfg_connection_timeout() noexcept;
	static std::string cfg_connection_request_interval() noexcept;
	static std::string cfg_connection_identity_interval() noexcept;
	static std::string cfg_connection_initialize_interval() noexcept;
	static std::string cfg_indentity_timeout() noexcept;
	static std::string cfg_loader_section_name() noexcept;
	static std::string cfg_loader_id() noexcept;
	static std::string cfg_loader_port() noexcept;
	static std::string cfg_server_section_name() noexcept;
	static std::string cfg_server_id() noexcept;
	static std::string cfg_server_address() noexcept;
	static std::string cfg_server_port() noexcept;
	static std::string cfg_ping_rt() noexcept;
	static std::string cfg_io_stack_maxsize() noexcept;
	static std::string cfg_worknodes_processing_concurrency() noexcept;
	static std::string cfg_crc() noexcept;
	static std::string cfg_timeout_reconnect()noexcept;
	static std::string cfg_log() noexcept;
	static std::string cfg_log_log_type() noexcept;
	static std::string cfg_log_ssd_connection_string() noexcept;
	static std::string cfg_log_log_tbl_volume() noexcept;
	static std::string cfg_log_log_filepath() noexcept;
	static std::string cfg_log_log_rotation_size() noexcept;
	static std::string cfg_log_log_queue_size() noexcept;


	static std::optional<sndrcv_timeline_periodicity_t> read_timeout(const crm::xml::xml_element_t & n);

private:
	void read_local(const std::wstring & xml, const std::wstring & closeNode);
	void read_local(const crm::xml::xml_document_t & xml, const std::wstring & closeNode);

	void default_setup();

public:
	distributed_ctx_policies_t();
	distributed_ctx_policies_t(const std::wstring & xml, const std::wstring & closeNode);
	distributed_ctx_policies_t(const crm::xml::xml_document_t & xml, const std::wstring closeNode);
	virtual ~distributed_ctx_policies_t();

	virtual void read_settings(const std::wstring & xml, const std::wstring & closeNode);
	virtual void read_settings(const crm::xml::xml_document_t & xml, const std::wstring closeNode);

	virtual const std::vector<uint8_t>& sync_sequence()const  noexcept;
	virtual const std::vector<uint8_t>& eof_sequence()const  noexcept;
	virtual double pool_loading_mfactor()const  noexcept;
	virtual size_t thread_pool_maxsize()const  noexcept;
	virtual size_t thread_pool_extern_handlers_maxsize()const  noexcept;
	virtual size_t thread_pool_stack_size()const  noexcept;
	virtual size_t io_buffer_maxsize()const  noexcept;
	virtual size_t stream_block_size()const  noexcept;
	virtual size_t io_threads_count()const  noexcept;
	virtual const sndrcv_timeline_periodicity_t& timeout_ping()const  noexcept;
	virtual sndrcv_timeline_t connection_timepoints()const  noexcept;
	virtual const std::chrono::milliseconds& timeout_probe_async()const  noexcept;
	virtual const std::chrono::milliseconds& timeout_wait_io()const  noexcept;
	virtual const std::chrono::milliseconds& threadpool_wait_item()const  noexcept;
	virtual const std::chrono::milliseconds& timeout_inactivity()const  noexcept;
	virtual const std::chrono::milliseconds& timeout_rt0rt1_command_execute()const  noexcept;
	virtual const std::chrono::milliseconds& timeout_reconnect_attempt()const  noexcept;
	virtual sndrcv_timeline_t timeout_sendrecv()const noexcept;
	virtual bool ping_rt()const  noexcept;
	virtual sndrcv_timeline_periodicity_t get_preffered_inactivity_or_ping()const  noexcept;
	bool use_crc()const  noexcept;
	const std::wstring& upload_folder()const  noexcept;

	const errem_points_t& errem_points(detail::rtl_level_t)const noexcept;
	errem_generator_mode errmode_connection_corrupt(detail::rtl_level_t)const noexcept;
	errem_generator_mode errmode()const noexcept;

	static const distributed_ctx_policies_t& get_default();


	void set_threadpool_maxsize(size_t);
	void set_threadpool_user_handlers_maxsize(size_t);
	void set_io_buffer_maxsize(size_t);
	void set_io_threads_count(size_t);
	void set_ping_timeline(const sndrcv_timeline_periodicity_t&);
	void set_rt_ping(bool);
	};


	template<typename T, typename = std::void_t<>>
	struct is_context_policies : std::false_type {};

	template<typename T>
	struct is_context_policies<T, std::void_t<
		std::enable_if_t<std::is_base_of_v<distributed_ctx_policies_t, T>>
		>> : std::true_type{};

	template<typename T>
	constexpr bool is_context_policies_v = is_context_policies<std::decay_t<T>>::value;
}

