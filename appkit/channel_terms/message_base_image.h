#pragma once


#include "../internal_invariants.h"
#include "./address_desc.h"
#include "./message_type_spcf.h"
#include "./rtl_rdm_link.h"
#include "./message_quant.h"
#include "../utilities/handlers/invoke_guard.h"


namespace crm {


/*! ������� ��������� ����������� ��������� */
class i_iol_handler_t{
public:
	static const global_object_identity_t null_id;
	static const size_t eof_marker_length = 8;

	using reverse_exception_guard = detail::inbound_message_with_tail_exception_guard;

private:
	typedef i_iol_handler_t self_t;

	iol_typeid_spcf_t _typeid;
	address_descriptor_t _address;
	iol_type_spcf_t _type;
	mutable std::weak_ptr<crm::i_types_driver_t> _tpdrv;

	std::unique_ptr<reverse_exception_guard> _reverseExc;
	detail::rtl_table_t _direction{ detail::rtl_table_t::undefined };

#ifdef CBL_MESSAGES_COUNTER_TYPES_CHECKER
	utility::__xobject_counter_logger_t<self_t, 10> _messagesCounter;
#endif



#ifdef CBL_MPF_ENABLE_TIMETRACE
	mutable detail::hanlder_times_stack_t _timeline;
#endif//CBL_MPF_ENABLE_TIMETRACE

	void set_update()noexcept;

public:
	i_iol_handler_t(const i_iol_handler_t &) = delete;
	i_iol_handler_t& operator=(const i_iol_handler_t &) = delete;

	i_iol_handler_t(i_iol_handler_t &&)noexcept;
	i_iol_handler_t& operator=(i_iol_handler_t &&)noexcept;

	void set_echo_params(const i_iol_handler_t & src, bool asReport = true);
	void set_echo_params(const address_descriptor_t & src, bool asReport = true);
	void set_address(const address_descriptor_t & src);

protected:
	i_iol_handler_t(iol_type_spcf_t type, detail::rtl_table_t)noexcept;
	i_iol_handler_t(iol_type_spcf_t type, detail::rtl_table_t, const global_object_identity_t & id)noexcept;
	i_iol_handler_t(iol_typeid_spcf_t type, detail::rtl_table_t)noexcept;
	i_iol_handler_t(iol_typeid_spcf_t type, detail::rtl_table_t, const global_object_identity_t& id)noexcept;
	i_iol_handler_t(iol_type_spcf_t mtype, iol_typeid_spcf_t type, detail::rtl_table_t)noexcept;
	i_iol_handler_t(iol_type_spcf_t mtype, iol_typeid_spcf_t type, detail::rtl_table_t, const global_object_identity_t& id)noexcept;

	void set_type(const iol_type_spcf_t &type)noexcept;
	void set_id(const global_object_identity_t &id)noexcept;
	void set_destination_id(const identity_descriptor_t &id);
	void set_source_id(const identity_descriptor_t &id);
	void set_destination_subscriber_id(const subscriber_node_id_t &id);
	void set_source_subscriber_id(const subscriber_node_id_t &id);
	void set_spin_tag(const message_spin_tag_type& id);
	void set_session(const session_description_t & session);
	void set_address(const detail::rtl_roadmap_link_t & lnk);
	void set_typeid_id(const type_identifier_t  &typeId);
	void set_typeid_name(const std::string &typeName);
	void setf_use_crc()noexcept;
	void setf_report()noexcept;
	void setf_length()noexcept;

public:
	void set_level(detail::rtl_level_t l);

protected:
	template<typename T>
	void set_type_attribute(const detail::iol_type_attributes_t index, typename T value) {
		_type.set(index, value);
		}

public:
	virtual ~i_iol_handler_t();

	detail::rtl_table_t direction()const noexcept;
	const iol_type_spcf_t& type()const  noexcept;
	const address_descriptor_t& address()const  noexcept;
	const global_object_identity_t& id()const  noexcept;
	const identity_descriptor_t& destination_id()const  noexcept;
	const identity_descriptor_t& source_id()const  noexcept;
	const subscriber_node_id_t& source_subscriber_id()const  noexcept;
	const subscriber_node_id_t& destination_subscriber_id()const  noexcept;
	const message_spin_tag_type& spin_tag()const noexcept;
	const session_description_t& session()const  noexcept;
	const iol_typeid_spcf_t& type_id()const  noexcept;
	detail::iol_types_ibase_t priority_value()const  noexcept;
	void cvt_from(detail::_packet_t && cdr);

	void set_typedriver(std::weak_ptr<crm::i_types_driver_t> tpdrv);
	void set_typedriver(std::weak_ptr<crm::i_types_driver_t> tpdrv)const;
	std::shared_ptr<crm::i_types_driver_t> typedriver()const;

#ifdef CBL_MPF_ENABLE_TIMETRACE
	void set_timeline_serialize()const;
	void set_timeline(detail::hanlder_times_stack_t::stage_t st);
	void set_timeline(detail::hanlder_times_stack_t::stage_t st, const detail::hanlder_times_stack_t::time_point_t & tp);
	const detail::hanlder_times_stack_t& timeline()const;
#endif//CBL_MPF_ENABLE_TIMETRACE

	bool fl_level()const  noexcept;
	detail::rtl_level_t level()const  noexcept;

	bool is_sendrecv_cycle()const  noexcept;
	bool is_sendrecv_request()const noexcept;
	bool is_sendrecv_response()const noexcept;


	/*! true - ���� l ����� ������������, ��� r */
	static bool priority(const i_iol_handler_t &l, const i_iol_handler_t &r)  noexcept;

	void invoke_reverse_exception(std::unique_ptr<dcf_exception_t> && exc)noexcept;
	bool is_reverse_exception_guard()const noexcept;
	reverse_exception_guard reset_invoke_exception_guard()noexcept;
	reverse_exception_guard reset_invoke_exception_guard(
		reverse_exception_guard && revexc)noexcept;

	void capture_response_tail(i_iol_handler_t &, bool asReport = true)noexcept;
	};



template<typename ... AL>
constexpr bool is_all_message_handler_based_v = (std::is_base_of_v<i_iol_handler_t, std::decay_t<AL>> && ...);

template<typename ... AL>
constexpr bool is_any_message_handler_based_v = (std::is_base_of_v<i_iol_handler_t, std::decay_t<AL>> || ...);


bool is_sendrecv_cycle(const i_iol_handler_t & h)noexcept;
bool is_sendrecv_request(const i_iol_handler_t & h) noexcept;
bool is_sendrecv_response(const i_iol_handler_t & h) noexcept;
bool is_report(const i_iol_handler_t& h)noexcept;
detail::rtl_table_t passing_direction(const i_iol_handler_t& h)noexcept;


namespace detail {
bool check_sndrcv_key_spin_tag(const i_iol_handler_t& h)noexcept;
bool is_sndrcv_spin_tagged_invoke(const i_iol_handler_t& h)noexcept;
}


/*! ��������� ����������� �������� ��������� */
class i_iol_ihandler_t : public i_iol_handler_t {
	/*friend class detail::_iol_igtw_t;
	friend class detail::igtw_pck_t;*/

public:
	///*! ������� �������� � ������ /ref has_property */
	//enum properties_base_t {
	//	single,
	//	linked,
	//	addressed,
	//	__user_value = 0xF0000000
	//	};

	///*! ������� �������� �������� �������� */
	//struct iol_base_property_t {
	//	virtual ~iol_base_property_t() {}
	//	global_object_identity_t addressDestination;
	//	};

	///*! ������� �����������, ��������� ������ ����������� ���
	//�������� � ��������� ��������������� ��� ���. ��������������� �������� ������� ������������
	//� ������������ /ref properties_base_t */
	//virtual bool has_property( uint32_t propertyId, std::unique_ptr<iol_base_property_t> &out );

	/*! �������� ������ - ���������� ��������
	�������������� � ������� ������� /ref set_delete_action */
	~i_iol_ihandler_t();

protected:
	i_iol_ihandler_t(iol_type_spcf_t type)noexcept;
	i_iol_ihandler_t(iol_type_spcf_t mtype, iol_typeid_spcf_t type)noexcept;

	i_iol_ihandler_t(iol_type_spcf_t type,
		global_object_identity_t id)noexcept;

	i_iol_ihandler_t(const i_iol_ihandler_t &) = delete;
	i_iol_ihandler_t& operator=(const i_iol_ihandler_t &) = delete;

	i_iol_ihandler_t(i_iol_ihandler_t &&)noexcept;
	i_iol_ihandler_t& operator=(i_iol_ihandler_t &&)noexcept;

public:
	/*! ����� ���������� �� �������������� ����� ��������� ������� � �����,
	����� ��������� ���� ��������� ������� */
	virtual void build();

	using i_iol_handler_t::spin_tag;
	};


/*! ��������� ����������� ��������� ��������� */
class i_iol_ohandler_t : public i_iol_handler_t {
protected:
	i_iol_ohandler_t(iol_type_spcf_t type)noexcept;
	i_iol_ohandler_t(iol_type_spcf_t type, const global_object_identity_t & id)noexcept;
	i_iol_ohandler_t(iol_type_spcf_t typecd_, iol_typeid_spcf_t typeid_)noexcept;
	i_iol_ohandler_t(iol_type_spcf_t typecd_, iol_typeid_spcf_t typeid_, const global_object_identity_t& id)noexcept;

public:
	using i_iol_handler_t::set_typeid_id;
	using i_iol_handler_t::set_typeid_name;
	using i_iol_handler_t::set_destination_id;
	using i_iol_handler_t::set_source_id;
	using i_iol_handler_t::set_source_subscriber_id;
	using i_iol_handler_t::set_destination_subscriber_id;
	using i_iol_handler_t::set_session;
	using i_iol_handler_t::set_id;
	using i_iol_handler_t::set_address;
	using i_iol_handler_t::set_level;
	using i_iol_handler_t::setf_use_crc;
	using i_iol_handler_t::set_spin_tag;
	using i_iol_handler_t::setf_report;
	using i_iol_handler_t::setf_length;

	i_iol_ohandler_t(const i_iol_ohandler_t &) = delete;
	i_iol_ohandler_t& operator=(const i_iol_ohandler_t &) = delete;

	i_iol_ohandler_t(i_iol_ohandler_t &&)noexcept;
	i_iol_ohandler_t& operator=(i_iol_ohandler_t &&)noexcept;
	};


template<typename TTargetMessage,
	typename TSourceMessage,
	typename std::enable_if_t<(std::is_base_of_v<i_iol_ihandler_t, TTargetMessage> && std::is_base_of_v<i_iol_ihandler_t, TSourceMessage>)
	|| (std::is_base_of_v<i_iol_ihandler_t, TTargetMessage> && std::is_base_of_v<crm::srlz::serialized_base_rw, TSourceMessage>)
	|| (std::is_base_of_v<TSourceMessage, TTargetMessage>), int> = 0>
	std::unique_ptr<TTargetMessage> message_cast(std::unique_ptr<TSourceMessage> && pb)noexcept {
	if(pb) {
		if(auto p = std::unique_ptr<TTargetMessage>(dynamic_cast<TTargetMessage*>(pb.get()))) {
			pb.release();

			return p;
			}
		else {
			return nullptr;
			}
		}
	else {
		return nullptr;
		}
	}

template<typename TTargetMessage,
	typename TSourceMessage,
	typename std::enable_if_t<(std::is_base_of_v<i_iol_ihandler_t, TTargetMessage> && std::is_base_of_v<i_iol_ihandler_t, TSourceMessage>)
	|| (std::is_base_of_v<i_iol_ihandler_t, TTargetMessage> && std::is_base_of_v<crm::srlz::serialized_base_rw, TSourceMessage>), int> = 0>
	typename const TTargetMessage* message_cast(const std::unique_ptr<TSourceMessage> & pb)noexcept {
	if(pb) {
		if(auto p = dynamic_cast<const TTargetMessage*>(pb.get())) {
			return p;
			}
		else {
			FATAL_ERROR_FWD(nullptr);
			}
		}
	else {
		return nullptr;
		}
	}


/*! ��������� ��������� ��������� */
struct input_item_t{
	std::unique_ptr<i_iol_ihandler_t> object_;
	identity_descriptor_t sourceObjectId_;

	input_item_t() noexcept;

	input_item_t(std::unique_ptr<i_iol_ihandler_t>&& object_,
		const identity_descriptor_t& sourceObjectId_)noexcept;

	const identity_descriptor_t& desc()const noexcept;
	const std::unique_ptr<i_iol_ihandler_t>& m()const& noexcept;
	std::unique_ptr<i_iol_ihandler_t> m() && noexcept;
	bool is_null()const noexcept;
	};


bool is_normalizedx(const i_iol_handler_t& obj)noexcept;
}

