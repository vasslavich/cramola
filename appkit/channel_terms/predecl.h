#pragma once


namespace crm{
class i_iol_ihandler_t;
class i_iol_ohandler_t;
class i_iol_handler_t;

class address_descriptor_t;

namespace detail {
bool is_sndrcv_spin_tagged_off()noexcept;
bool is_sndrcv_spin_tagged_on()noexcept;
struct i_rt0_main_router_input_t;
}
};

