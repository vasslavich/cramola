#pragma once


#include "./base_terms.h"
#include "./address_desc.h"
#include "./message_frame.h"


namespace crm::detail{


_packet_t make_exception_RZi_LX( const std::shared_ptr<distributed_ctx_t> &ctx,
	const address_descriptor_t & destAddr, std::unique_ptr<dcf_exception_t> && e_ )noexcept;

bool is_exception_RZi_LX( const _packet_t & u )noexcept;

void exception_RZi_LX_extract( const std::shared_ptr<distributed_ctx_t> &ctx,
	_packet_t && p, address_descriptor_t & destAddr, std::unique_ptr<dcf_exception_t> & e_ )noexcept;
void exception_RZi_LX_extract( const distributed_ctx_t &ctx,
	_packet_t && p, address_descriptor_t & destAddr, std::unique_ptr<dcf_exception_t> & e_ )noexcept;
}

