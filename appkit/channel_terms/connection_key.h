#pragma once


#include <string>
#include "../typeframe/type_trait_base.h"


namespace crm {


struct connection_key_t{
	std::string address;
	int port{ 0 };

	std::string to_string()const noexcept;
	std::wstring to_wstring()const noexcept;
	};


template<>
struct null_value_t<connection_key_t> {
	static const connection_key_t null;
	};




bool operator == (const connection_key_t & lval, const connection_key_t &rval)noexcept;
bool operator != (const connection_key_t & lval, const connection_key_t &rval)noexcept;
bool operator < (const connection_key_t & lval, const connection_key_t &rval)noexcept;
bool operator > (const connection_key_t & lval, const connection_key_t &rval)noexcept;
bool operator >= (const connection_key_t & lval, const connection_key_t &rval)noexcept;
std::ostream  & operator <<(std::ostream & os, const connection_key_t & ck);

void apply_at_hash(address_hash_t & to, const connection_key_t & value);
}
