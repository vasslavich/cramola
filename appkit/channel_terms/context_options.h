#pragma once


#include "../notifications/base_terms.h"
#include "../streams/i_streams.h"
#include "./policies.h"


namespace crm {

class context_bindings {
private:
	channel_identity_t _domainId{ channel_identity_t::make() };
	identity_descriptor_t _idx;

public:
	context_bindings();
	context_bindings(const identity_descriptor_t& idx);
	virtual ~context_bindings(){}

	virtual void notification(std::unique_ptr<crm::dcf_exception_t>&&)noexcept;
	virtual void wstream(const stream_source_descriptor_t& h, std::shared_ptr<i_stream_whandler_t>&&);
	const channel_identity_t& domain_id()const noexcept;
	const identity_descriptor_t& identity()const noexcept;
	};

template<typename T, typename = std::void_t<>>
struct is_context_bindings : std::false_type {};

template<typename T>
struct is_context_bindings<T, std::void_t<
	std::enable_if_t<std::is_base_of_v<context_bindings, T>>
	>> : std::true_type{};

template<typename T>
constexpr bool is_context_bindings_v = is_context_bindings<std::decay_t<T>>::value;
}
