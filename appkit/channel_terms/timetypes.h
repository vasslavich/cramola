#pragma once


#include <type_traits>
#include <chrono>
#include "../internal_invariants.h"


namespace crm {


struct sndrcv_timeline_periodicity_t {
	using timeline_t = std::chrono::milliseconds;

	static const timeline_t max_duration;
	static const timeline_t null_timeline_value;

	enum class periodicity_t {
		undefined,
		cyclic_timeouted,
		cyclic,
		once_call_timeouted,
		once_call
		};

private:
	timeline_t _timeout = null_timeline_value;
	timeline_t _interval = null_timeline_value;
	periodicity_t _periodicity = periodicity_t::undefined;

public:
	static const sndrcv_timeline_periodicity_t null;

	sndrcv_timeline_periodicity_t()noexcept;

	const timeline_t& timeout()const noexcept;
	const timeline_t& interval()const noexcept;
	periodicity_t periodicity()const noexcept;
	bool is_timeouted()const noexcept;
	bool is_cyclic()const noexcept;
	bool is_once_call()const noexcept;

	std::chrono::system_clock::time_point make_deadline_point_from_now()const;
	std::chrono::system_clock::time_point make_deadline_point_from(const std::chrono::system_clock::time_point & tp)const;

private:
	template < typename TRep, typename TPeriod>
	sndrcv_timeline_periodicity_t(const std::chrono::duration<TRep, TPeriod> & timeout_,
		const std::chrono::duration<TRep, TPeriod> & interval_,
		periodicity_t  per)
		: _timeout(std::chrono::duration_cast<timeline_t>(timeout_))
		, _interval(std::chrono::duration_cast<timeline_t>(interval_))
		, _periodicity(per) {}

public:
	template < typename TRep, typename TPeriod>
	static sndrcv_timeline_periodicity_t make_cyclic_timeouted(const std::chrono::duration<TRep, TPeriod> & timeout_,
		const std::chrono::duration<TRep, TPeriod> & interval_) {

		return sndrcv_timeline_periodicity_t(timeout_, interval_, periodicity_t::cyclic_timeouted);
		}

	template < typename TRep, typename TPeriod>
	static sndrcv_timeline_periodicity_t make_once_call_timeouted(const std::chrono::duration<TRep, TPeriod> & timeout_) {

		return sndrcv_timeline_periodicity_t(std::chrono::duration_cast<timeline_t>(timeout_), max_duration, periodicity_t::once_call_timeouted);
		}

	template<typename TRep, typename TPeriod>
	static sndrcv_timeline_periodicity_t make_cyclic(const std::chrono::duration<TRep, TPeriod> & interval) {
		return sndrcv_timeline_periodicity_t(max_duration, std::chrono::duration_cast<timeline_t>(interval), periodicity_t::cyclic);
		}


	template<typename TRep, typename TPer>
	static std::chrono::system_clock::time_point make_timepoint(const std::chrono::duration<TRep, TPer> & timeout) {
		const auto nowTP = std::chrono::system_clock::now();
		const auto maxTypeDur = (std::chrono::system_clock::time_point::max)() - nowTP;
		if(maxTypeDur > (timeout * 2))
			return nowTP + timeout;
		else {
			FATAL_ERROR_FWD(nullptr);
			}
		}

	template<typename TRep, typename TPer>
	static bool validate_duration_range(const std::chrono::duration<TRep, TPer> & dur) {
		const auto nowTP = std::chrono::system_clock::now();
		const auto maxTypeDur = std::chrono::duration_cast<std::chrono::duration<TRep, TPer>>((std::chrono::system_clock::time_point::max)() - nowTP);
		if((maxTypeDur / 2) > dur )
			return true;
		else {
			return false;
			}
		}

	static std::chrono::milliseconds make_max_interval();
	static sndrcv_timeline_periodicity_t make_once_call();

	friend bool operator==(const sndrcv_timeline_periodicity_t & l, const sndrcv_timeline_periodicity_t & r) {
		return l.timeout() == r.timeout() && l.interval() == r.interval() && l.periodicity() == r.periodicity();
		}
	};


struct sndrcv_timeline_timeouted_once {
	static constexpr sndrcv_timeline_periodicity_t::timeline_t max_interval() {
		return sndrcv_timeline_periodicity_t::timeline_t{ std::chrono::hours(6) };
		}

	sndrcv_timeline_periodicity_t::timeline_t value;

	sndrcv_timeline_timeouted_once(const sndrcv_timeline_periodicity_t::timeline_t & value);
	};


struct sndrcv_timeline_t {
	using periodicity_t = sndrcv_timeline_periodicity_t::periodicity_t;
	using timeline_t = sndrcv_timeline_periodicity_t::timeline_t;

private:
	sndrcv_timeline_periodicity_t _value;
	std::chrono::system_clock::time_point _start_point;

public:
	static const sndrcv_timeline_t null;

	std::chrono::system_clock::time_point deadline_point()const;
	const timeline_t& timeout()const noexcept;
	const timeline_t& interval()const noexcept;
	periodicity_t periodicity()const noexcept;
	bool is_timeouted()const noexcept;
	bool is_cyclic()const noexcept;
	bool is_once_call()const noexcept;
	const std::chrono::system_clock::time_point& start_point()const noexcept;
	const sndrcv_timeline_periodicity_t& value()const noexcept;

private:
	template<typename TTimelinePeriodicity,
		typename TClock,
		typename std::enable_if_t< !std::is_same<sndrcv_timeline_t, typename std::decay_t<TTimelinePeriodicity>>::value, int> = 0>
	sndrcv_timeline_t(TTimelinePeriodicity && value_, TClock && clocks)
		: _value(std::forward<TTimelinePeriodicity>(value_))
		, _start_point(std::forward<TClock>(clocks)) {}

	static const std::chrono::milliseconds max_duration;

public:
	template<typename TRep, typename TPeriod>
	static sndrcv_timeline_t make_once_call_timeouted( const std::chrono::duration<TRep, TPeriod> & timeout_ ) {
		return sndrcv_timeline_t(sndrcv_timeline_periodicity_t::make_once_call_timeouted( timeout_ ), std::chrono::system_clock::now());
		}

	static sndrcv_timeline_t make_once_call();

	template<typename TRep, typename TPeriod>
	static sndrcv_timeline_t make_cyclic_timeouted( const std::chrono::duration<TRep, TPeriod> & timeout_,
		const std::chrono::duration<TRep, TPeriod> & interval_ ) {

		return sndrcv_timeline_periodicity_t::make_cyclic_timeouted(timeout_, interval_);
		}

	template<typename TRep, typename TPeriod>
	static sndrcv_timeline_t make_cyclic( const std::chrono::duration<TRep, TPeriod> & interval_ ) {
		return sndrcv_timeline_periodicity_t::make_cyclic( interval_ );
		}

	template<typename TTimelinePeriodicity,
		typename std::enable_if_t< !std::is_same<sndrcv_timeline_t, typename std::decay_t<TTimelinePeriodicity>>::value, int> = 0>
	static sndrcv_timeline_t make(TTimelinePeriodicity && value_) {
		return sndrcv_timeline_t(std::forward<TTimelinePeriodicity>(value_), std::chrono::system_clock::now());
		}

	static sndrcv_timeline_t make(const sndrcv_timeline_timeouted_once & value);

	friend bool operator==(const sndrcv_timeline_t & l, const sndrcv_timeline_t & r) {
		return l.value() == r.value() && l.start_point() == r.start_point();
		}

	friend bool operator!=(const sndrcv_timeline_t & l, const sndrcv_timeline_t & r) {
		return !(l == r);
		}
	};
}
