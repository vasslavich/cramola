#pragma once


#include "./base_terms.h"
#include "./ids_types.h"
#include "../typeframe/itypeid.h"


namespace crm{


/*! ��������� ������ */
class session_description_t final :
	public hf_operators_t<session_description_t>{

public:
	static constexpr size_t max_name_size()noexcept { return 128; }

private:
	typedef std::uint32_t flags_utype_t;
	enum class taglst_t : flags_utype_t{
		null = 0x00000000,
		id = 0x00000001,
		name = 0x00000002
		};

	/*! ������������� ������ */
	session_id_t _id{ get_null<session_id_t>() };
	/*! ��� ������ */
	std::string _name;
	/*! ��� ��������� */
	address_hash_t _hash{ get_null< address_hash_t>() };
	/*! ���� ��������� */
	flags_utype_t _flags = (flags_utype_t)taglst_t::null;


	void __set_flag(const taglst_t attribute);

	template<const taglst_t flVal>
	void set_flag(){
		static_assert(taglst_t::null != flVal, "invalid value");
		__set_flag(flVal);
		}

	flags_utype_t __get_uflags(taglst_t attribute)const noexcept;

	template<const taglst_t flVal>
	flags_utype_t get_uflags()const noexcept{
		static_assert(taglst_t::null != attribute, "invalid value");
		return __get_uflags(flVal);
		}

	bool __has_flag(taglst_t attribute)const noexcept;

	template<const taglst_t flVal>
	bool has_flag()const noexcept{
		static_assert(taglst_t::null != flVal, "invalid value");
		return __has_flag(flVal);
		}

	void update_hash()noexcept;

public:
	using hash_t = address_hash_t;

	/*! ����� r [�� ��������� �����]*/
	bool equals_fields(const session_description_t& r)const noexcept;
	/*! ������ ��� r [�� ��������� �����]*/
	bool less_fields(const session_description_t& r)const noexcept;
	/*! ������ ��� r [�� ��������� �����]*/
	bool great_fields(const session_description_t& r)const noexcept;
	/*! ������ ��� ����� r [�� ��������� �����]*/
	bool great_equals_fields(const session_description_t& r)const noexcept;

public:
	static const session_description_t null;

	session_description_t()noexcept;
	session_description_t(const session_id_t& id, std::string_view name)noexcept;

	bool is_null()const noexcept;
	const address_hash_t& object_hash()const noexcept;

	void set_id(const session_id_t & id);
	const session_id_t& id()const noexcept;

	void set_name(std::string_view name);
	const std::string& name()const noexcept;

	using serialize_options = srlz::detail::deduction_prefix_mode_t<>;

	template<typename Ts>
	void srlz(Ts && dest)const{
		srlz::serialize(dest, _flags, serialize_options{});
		srlz::serialize(dest, _hash, serialize_options{});

		if (has_flag<taglst_t::id>())
			srlz::serialize(dest, _id, serialize_options{});

		if (has_flag< taglst_t::name>())
			crm::srlz::serialize(dest, _name, srlz::detail::option_max_serialized_size<max_name_size()>{});
		}

	template<typename Ts>
	void dsrlz(Ts && src){
		srlz::deserialize(src, _flags, serialize_options{});
		srlz::deserialize(src, _hash, serialize_options{});

		if (has_flag< taglst_t::id>())
			srlz::deserialize(src, _id, serialize_options{});

		if (has_flag< taglst_t::name>())
			srlz::deserialize(src, _name, srlz::detail::option_max_serialized_size<max_name_size()>{});
		}

	size_t get_serialized_size()const noexcept;
	std::string to_str()const  noexcept;
	};
}
