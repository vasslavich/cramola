#pragma once


#include <memory>
#include "./base_terms.h"
#include "./ids_types.h"
#include "./rtl_rdm_event_desc.h"
#include "./i_channel_base.h"


namespace crm {


struct _address_event_t final :
	public hf_operators_t<_address_event_t> {
public:
	static const _address_event_t null;

private:
	subscriber_event_id_t _id{ get_null<subscriber_event_id_t>() };
	channel_identity_t _chid{ get_null<channel_identity_t>() };
	event_type_t _type{ event_type_t::st_undefined };
	detail::rtl_level_t _level{ detail::rtl_level_t::undefined };

	/*! ��� ������� */
	address_hash_t _hash;

	void update_hash()noexcept;

public:
	using hash_t = address_hash_t;

	/*! ����� r [�� ��������� �����]*/
	bool equals_fields(const _address_event_t& r)const noexcept;
	/*! ������ ��� r [�� ��������� �����]*/
	bool less_fields(const _address_event_t& r)const noexcept;
	/*! ������ ��� r [�� ��������� �����]*/
	bool great_fields(const _address_event_t& r)const noexcept;
	/*! ������ ��� ����� r [�� ��������� �����]*/
	bool great_equals_fields(const _address_event_t& r)const noexcept;

	/*! ������������ ������� � �������� ������ */
	template<typename Ts>
	void srlz(Ts && dest)const{
		srlz::serialize(dest, _id);
		srlz::serialize(dest, _type);
		srlz::serialize(dest, _level);
		srlz::serialize(dest, _chid);
		srlz::serialize(dest, _hash);
		}

	/*! �������������� ������� �� ��������� ������� */
	template<typename Ts>
	void dsrlz(Ts && src ){
		srlz::deserialize(src, _id);
		srlz::deserialize(src, _type);
		srlz::deserialize(src, _level);
		srlz::deserialize(src, _chid);
		srlz::deserialize(src, _hash);
		}

	size_t get_serialized_size()const noexcept;

private:
	_address_event_t(const subscriber_event_id_t& id,
		const channel_identity_t & chid,
		event_type_t type,
		detail::rtl_level_t level)noexcept;

public:
	_address_event_t()noexcept;

	static _address_event_t make()noexcept;

	static _address_event_t make(const subscriber_event_id_t& id,
		const channel_identity_t & chid,
		event_type_t type,
		detail::rtl_level_t level)noexcept;

	const address_hash_t& object_hash()const noexcept;
	const channel_identity_t& channel()const noexcept;
	const subscriber_event_id_t& id()const noexcept;
	event_type_t type()const noexcept;
	detail::rtl_level_t level()const noexcept;
	bool is_null()const noexcept;
	};
}
