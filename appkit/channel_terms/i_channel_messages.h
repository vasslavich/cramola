#pragma once


#include "./i_channel_base.h"
#include "./base_terms.h"
#include "./message_frame.h"


namespace crm::detail{


class io_x_irt1_messages_t : public io_x_rt1_base_t{
public:
	virtual ~io_x_irt1_messages_t() = 0;

	virtual bool pop_from_rt0( rt0_x_message_t & pck,
		const std::chrono::milliseconds & wait ) = 0;
	virtual size_t pop_from_rt0( std::list<rt0_x_message_t> & lst,
		const std::chrono::milliseconds & wait ) = 0;
	};


struct io_x_ort1_messages_t{
	virtual ~io_x_ort1_messages_t() = 0;

	[[nodiscard]]
	virtual addon_push_event_result_t push_to_rt0_message( rt1_x_message_data_t && object,
		const std::chrono::milliseconds & wait)noexcept = 0;
	};
}

