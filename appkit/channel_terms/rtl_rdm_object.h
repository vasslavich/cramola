#pragma once


#include "../internal_invariants.h"
#include "./rtl_rdm_link.h"
#include "./rt0_rdm.h"
#include "./rdm_sessional_desc.h"
#include "./source_object_key.h"
#include "./stream_wait_key.h"


namespace crm::detail {


/*! �������� ����� ��� ������������� ��������� ������ ������ ����,
����� ��������� 0-�� � 1-�� ������� */
struct rtl_roadmap_obj_t final :
	public hf_operators_t<rtl_roadmap_obj_t>  {

	static const rtl_roadmap_obj_t null;

private:
	/*! ���������� �� �������������� ����� ������ */
	rtl_roadmap_link_t _link;
	/*! ����� ������������� �� �������� ������ 0 */
	rt0_node_rdm_t _rt0;
	/*! ��� ������� ���� */
	rtl_table_t _rtt = rtl_table_t::undefined;
	/*! ��� ������� */
	address_hash_t _hash;

	void update_hash();

public:
	using hash_t = address_hash_t;

	/*! ����� r [�� ��������� �����]*/
	bool equals_fields(const rtl_roadmap_obj_t& r)const noexcept;
	/*! ������ ��� r [�� ��������� �����]*/
	bool less_fields(const rtl_roadmap_obj_t& r)const noexcept;
	/*! ������ ��� r [�� ��������� �����]*/
	bool great_fields(const rtl_roadmap_obj_t& r)const noexcept;
	/*! ������ ��� ����� r [�� ��������� �����]*/
	bool great_equals_fields(const rtl_roadmap_obj_t& r)const noexcept;

private:
	rtl_roadmap_obj_t(const rt0_node_rdm_t & rt0,
		const identity_descriptor_t & target,
		const identity_descriptor_t & source,
		const session_description_t & session,
		const rtl_table_t & rtt);

public:
	rtl_roadmap_obj_t();

	static rtl_roadmap_obj_t make_router_rt1(const identity_descriptor_t & routerId );

	static rtl_roadmap_obj_t make_rt1_ostream(const rt0_node_rdm_t & rt0,
		const identity_descriptor_t & remoteId,
		const identity_descriptor_t & thisId,
		const session_description_t & session);

	static rtl_roadmap_obj_t make_rt1_istream(const rt0_node_rdm_t & rt0,
		const identity_descriptor_t & remoteId,
		const identity_descriptor_t & thisId,
		const session_description_t & session);


	static rtl_roadmap_obj_t make(const rt0_node_rdm_t & rt0,
		const identity_descriptor_t & target,
		const identity_descriptor_t & source,
		const session_description_t & session,
		const rtl_table_t & rtt);

	void set_rt0(const rt0_node_rdm_t & rt0)noexcept;

	rt1_endpoint_t remote_object_endpoint()const noexcept;
	rdm_sessional_descriptor_t remote_object_rds()const noexcept;
	const address_hash_t& object_hash()const noexcept;
	const rt0_node_rdm_t& rt0()const noexcept;
	const session_description_t& session()const noexcept;
	const identity_descriptor_t& source_id()const noexcept;
	const identity_descriptor_t& target_id()const noexcept;
	const rtl_table_t& table()const noexcept;
	const rtl_roadmap_link_t& link()const noexcept;
	std::string to_str()const noexcept;
	rtl_roadmap_obj_t get_echo()const noexcept;
	source_object_key_t source_object_key()const noexcept;
	bool is_null()const noexcept;

	/*! ������������ ������� � �������� ������ */
	template<typename Ts>
	void srlz(Ts && dest)const{
		srlz::serialize(dest, _link);
		srlz::serialize(dest, _rt0);
		crm::srlz::serialize(dest, _rtt);
		srlz::serialize(dest, _hash);
		}

	/*! �������������� ������� �� ��������� ������� */
	template<typename Ts>
	void dsrlz(Ts && src ){
		srlz::deserialize(src, _link);
		srlz::deserialize(src, _rt0);
		srlz::deserialize(src, _rtt);
		srlz::deserialize(src, _hash);
		}

	size_t get_serialized_size()const noexcept;

	rt1_streams_table_wait_key_t rt1_stream_table_key()const noexcept;
	};

struct rtl_roadmap_line_t final {
	rtl_roadmap_obj_t points;

	const address_hash_t& object_hash()const noexcept;
	};
}
