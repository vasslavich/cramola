#include "../../internal.h"
#include "../context_options.h"

using namespace crm;
using namespace crm::detail;


static_assert(is_context_bindings_v<context_bindings>, __FILE_LINE__);

context_bindings::context_bindings()
	: context_bindings(identity_descriptor_t(
		identity_descriptor_id_type::rand(), 
		global_object_identity_t::rand().to_str())) {}

context_bindings::context_bindings(const identity_descriptor_t& idx)
	: _idx(idx) {}

void context_bindings::notification(std::unique_ptr<dcf_exception_t>&&n)noexcept {
	if(n) {
		crm::file_logger_t::logging((*n));
		}
	}

void context_bindings::wstream(const stream_source_descriptor_t& h,
	std::shared_ptr<i_stream_whandler_t>&&) {

	std::ostringstream s;
	s << "ready input stream:" << h.to_str();
	crm::file_logger_t::logging(s.str());
	}

const channel_identity_t& context_bindings::domain_id()const noexcept {
	return _domainId;
	}

const identity_descriptor_t& context_bindings::identity()const noexcept {
	return _idx;
	}

