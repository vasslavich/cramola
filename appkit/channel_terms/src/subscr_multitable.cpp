#include "../../internal.h"
#include "../../notifications/exceptions/excdrv.h"
#include "../subscr_tbl.h"
#include "../../context/context.h"
#include "../../exceptions.h"


using namespace crm;
using namespace crm::detail;


subscribe_multitable_t::subscribe_multitable_t(std::weak_ptr<distributed_ctx_t> ctx_,
	size_t wide)
	: _ctx(ctx_){

	_table.resize(wide);
	for(size_t i = 0; i < wide; ++i) {
		_table[i] = std::make_unique<subscribe_table_t>(ctx_);
		}
	}

subscribe_multitable_t::handler_t subscribe_multitable_t::make_handler(rtl_level_t l)noexcept {
	return handler_t({ _lineIdCounter++, l }, weak_from_this());
	}

size_t subscribe_multitable_t::h2id(const handler_t & ln)const noexcept {
	return ln.lineId.id() % _table.size();
	}

std::unique_ptr<i_iol_ihandler_t> subscribe_multitable_t::check_address(const handler_t & ln,
	std::unique_ptr<i_iol_ihandler_t> && obj,
	invoke_context_trait ic) {

	return _table.at(h2id(ln))->check_address(ln.lineId, std::move(obj), ic);
	}

void subscribe_multitable_t::addressed_exception(const handler_t & ln,
	const _address_hndl_t &addr,
	std::unique_ptr<dcf_exception_t>&& e,
	invoke_context_trait ic)noexcept {
	
	_table.at(h2id(ln))->addressed_exception(ln.lineId, addr, std::move(e), ic);
	}

void subscribe_multitable_t::regline(const handler_t& ln ) {
	_table.at(h2id(ln))->regline(ln.lineId);
	}

subscribe_invoker_result subscribe_multitable_t::subscribe(const handler_t & ln,
	async_space_t scx,
	const _address_hndl_t & id,
	subscribe_options opt,
	subscribe_message_handler && handler) {

	return _table.at(h2id(ln))->subscribe(ln.lineId, scx, id, opt, std::move(handler));
	}

void subscribe_multitable_t::unsubscribe(const handler_t & ln, const _address_hndl_t & id)noexcept {
	const auto cell = h2id(ln);
	const auto lineId = ln.lineId;
	
	CHECK_NO_EXCEPTION( _table.at(cell)->unsubscribe(lineId, id));
	}

void subscribe_multitable_t::close( const handler_t & ln )noexcept{
	const auto cell = h2id( ln );
	const auto lineId = ln.lineId;

	CHECK_NO_EXCEPTION( _table.at( cell )->close( lineId ) );
	}





