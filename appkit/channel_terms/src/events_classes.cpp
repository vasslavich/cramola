#include "../../internal.h"
#include "../i_events.h"

using namespace crm;
using namespace crm::detail;


const srlz::typemap_key_t event_abandoned_t::class_type_id_= srlz::typemap_key_t::make<event_abandoned_t>();

std::unique_ptr<i_event_t> event_abandoned_t::copy()const noexcept {
	return std::make_unique<event_abandoned_t>((*this));
	}

const srlz::typemap_key_t& event_abandoned_t::class_type_id()noexcept {
	return class_type_id_;
	}

const srlz::typemap_key_t& event_abandoned_t::type_id()const noexcept {
	return class_type_id();
	}

