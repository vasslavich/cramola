#include "../../internal.h"
#include "../rtl_rdm_object.h"


using namespace crm;
using namespace crm::detail;


static_assert(srlz::detail::is_i_srlzd_prefixed_v< rtl_roadmap_obj_t>, __FILE_LINE__);
static_assert(srlz::detail::is_srlzd_prefixed_v< rtl_roadmap_obj_t>, __FILE_LINE__);


/*==============================================================================
rtl_roadmap_obj_t
================================================================================*/


const rtl_roadmap_obj_t rtl_roadmap_obj_t::null;

bool rtl_roadmap_obj_t::is_null()const noexcept {
	return (*this) == null;
	}

rtl_roadmap_obj_t rtl_roadmap_obj_t::make( const rt0_node_rdm_t & rt0,
	const identity_descriptor_t & target,
	const identity_descriptor_t & source,
	const session_description_t & session,
	const rtl_table_t & rtt ) {
	
	return rtl_roadmap_obj_t( rt0, target, source, session, rtt );
	}

rtl_roadmap_obj_t rtl_roadmap_obj_t::make_router_rt1( 
	const identity_descriptor_t & routerId ) {

	return make(rt0_node_rdm_t::create_rt1_router_rdm(routerId.to_str()),
		routerId,
		identity_descriptor_t::null,
		session_description_t{},
		rtl_table_t::rt1_router );
	}

rtl_roadmap_obj_t rtl_roadmap_obj_t::make_rt1_ostream( const rt0_node_rdm_t & rt0,
	const identity_descriptor_t & remoteId,
	const identity_descriptor_t & thisId,
	const session_description_t & session_ ) {

	return make( rt0,
		remoteId,
		thisId,
		session_,
		rtl_table_t::out );
	}

rtl_roadmap_obj_t rtl_roadmap_obj_t::make_rt1_istream( const rt0_node_rdm_t & rt0,
	const identity_descriptor_t & remoteId,
	const identity_descriptor_t & thisId,
	const session_description_t & session_ ) {

	return make( rt0,
		remoteId,
		thisId,
		session_,
		rtl_table_t::in );
	}

rtl_roadmap_obj_t::rtl_roadmap_obj_t() {}

rtl_roadmap_obj_t::rtl_roadmap_obj_t( const rt0_node_rdm_t & rt0,
	const identity_descriptor_t & target_,
	const identity_descriptor_t & source_,
	const session_description_t & session_,
	const rtl_table_t & rtt_ )
	: _link( target_, source_, session_)
	, _rt0( rt0 )
	, _rtt( rtt_ ){
	
	update_hash();
	}

void rtl_roadmap_obj_t::set_rt0( const rt0_node_rdm_t & rt0 ) noexcept {
	_rt0 = rt0;
	update_hash();
	}

void rtl_roadmap_obj_t::update_hash() {
	_hash = address_hash_t();

	apply_at_hash( _hash, _link.object_hash() );
	apply_at_hash( _hash, _rtt );
	apply_at_hash( _hash, _rt0.object_hash() );
	}

const rt0_node_rdm_t& rtl_roadmap_obj_t::rt0()const noexcept {
	return _rt0;
	}

rt1_endpoint_t rtl_roadmap_obj_t::remote_object_endpoint()const noexcept {
	return rt1_endpoint_t::make_remote(rt0().connection_key().address, rt0().connection_key().port, _link.source_id());
	}

rdm_sessional_descriptor_t rtl_roadmap_obj_t::remote_object_rds()const noexcept {
	return rdm_sessional_descriptor_t( remote_object_endpoint(), session() );
	}

const rtl_roadmap_obj_t::hash_t& rtl_roadmap_obj_t::object_hash()const noexcept {
	return _hash;
	}

const session_description_t& rtl_roadmap_obj_t::session()const noexcept {
	return _link.session();
	}

const identity_descriptor_t& rtl_roadmap_obj_t::source_id()const noexcept {
	return _link.source_id();
	}

const identity_descriptor_t& rtl_roadmap_obj_t::target_id()const noexcept {
	return _link.target_id();
	}

const rtl_table_t& rtl_roadmap_obj_t::table()const noexcept {
	return _rtt;
	}

const rtl_roadmap_link_t& rtl_roadmap_obj_t::link()const noexcept {
	return _link;
	}

std::string rtl_roadmap_obj_t::to_str()const noexcept {
	std::ostringstream s;
	s << "[link=" << _link.to_str() << ":rdm0=" << _rt0.to_str() << ":direction=" << cvt2string( _rtt ) << ":hash=" + _hash.to_str() + "]";
	return s.str();
	}

rtl_roadmap_obj_t rtl_roadmap_obj_t::get_echo()const noexcept {
	return rtl_roadmap_obj_t ( rt0(),
		source_id(),
		target_id(),
		session(), 
		table() );
	}

source_object_key_t rtl_roadmap_obj_t::source_object_key()const noexcept {
	return source_object_key_t::make( remote_object_endpoint(),
		session(),

		/* �����, sourceid - ��� ���������� ��������� �� ��� �������,
		targetid - ����������� ��������� ��	���� ������� */
		target_id());
	}

size_t rtl_roadmap_obj_t::get_serialized_size()const noexcept {
	return srlz::object_trait_serialized_size( _link )
		+ srlz::object_trait_serialized_size( _rt0 )
		+ srlz::object_trait_serialized_size( _rtt )
		+ srlz::object_trait_serialized_size( _hash );
	}

rt1_streams_table_wait_key_t rtl_roadmap_obj_t::rt1_stream_table_key()const noexcept{
	return rt1_streams_table_wait_key_t(rt0(), link(), table());
	}

bool rtl_roadmap_obj_t::equals_fields( const rtl_roadmap_obj_t& r )const noexcept {

	return link() == r.link()
		&& rt0() == r.rt0()
		&& table() == r.table();
	}

bool rtl_roadmap_obj_t::less_fields( const rtl_roadmap_obj_t& r )const noexcept {

	if( link() < r.link() )
		return true;
	else if( link() > r.link() )
		return false;
	else if( rt0() < r.rt0() )
		return true;
	else if( rt0() > r.rt0() )
		return false;
	else
		return table() < r.table();
	}

bool rtl_roadmap_obj_t::great_fields( const rtl_roadmap_obj_t& r )const noexcept {

	if( link() > r.link() )
		return true;
	else if( link() < r.link() )
		return false;
	else if( rt0() > r.rt0() )
		return true;
	else if( rt0() < r.rt0() )
		return false;
	else
		return table() > r.table();
	}

bool rtl_roadmap_obj_t::great_equals_fields( const rtl_roadmap_obj_t& r )const noexcept {

	if( link() > r.link() )
		return true;
	else if( link() < r.link() )
		return false;
	else if( rt0() > r.rt0() )
		return true;
	else if( rt0() < r.rt0() )
		return false;
	else
		return table() >= r.table();
	}



/*===================================================================================
=====================================================================================*/

static_assert(srlz::detail::is_aggregate_initializable_v<rtl_roadmap_line_t>, __FILE_LINE__);
static_assert(crm::srlz::detail::is_strob_flat_serializable_v<rtl_roadmap_line_t>, __FILE_LINE__);
static_assert(crm::srlz::detail::__fields_count_hepler_t<rtl_roadmap_line_t>::count == 1, __FILE_LINE__);
static_assert(crm::srlz::detail::__fields_count_t<rtl_roadmap_line_t>::value, __FILE_LINE__);
static_assert(crm::srlz::detail::__fields_count_t<rtl_roadmap_line_t>::count == 1, __FILE_LINE__);
static_assert(crm::srlz::detail::fields_count_v<rtl_roadmap_line_t> == 1, __FILE_LINE__);
static_assert(srlz::is_serializable_v< rtl_roadmap_line_t>, __FILE_LINE__);

const address_hash_t& rtl_roadmap_line_t::object_hash()const noexcept{
	return points.object_hash();
	}

