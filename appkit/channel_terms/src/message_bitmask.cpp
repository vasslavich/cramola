#include "../message_bitmask.h"

using namespace crm;
using namespace crm::detail;


iol_type_map_t iol_type_map_t::_instance;
const iol_type_map_t::field_info_t iol_type_map_t::field_info_t::undefined = iol_type_map_t::field_info_t::make_undefined();

iol_type_map_t::iol_type_map_t() {

	_map.at((size_t)iol_type_attributes_t::type) = field_info_t::make<0, 24, 0xFFFFFF>();
	_map.at((size_t)iol_type_attributes_t::code) = field_info_t::make<24, 8, 0xFF >();
	_map.at((size_t)iol_type_attributes_t::type_id) = field_info_t::make<32, 1, 0x01 >();
	_map.at((size_t)iol_type_attributes_t::crc) = field_info_t::make<33, 1, 0x01 >();
	_map.at((size_t)iol_type_attributes_t::timeline) = field_info_t::make<34, 1, 0x01 >();
	_map.at((size_t)iol_type_attributes_t::report) = field_info_t::make<35, 1, 0x1 >();
	_map.at((size_t)iol_type_attributes_t::length) = field_info_t::make<36, 2, 0x03 >();
	_map.at((size_t)iol_type_attributes_t::reserved) = field_info_t::make<38, 26, 0x03FFFFFF >();
	}

const iol_type_map_t& iol_type_map_t::instance()noexcept {
	return _instance;
	}

iol_type_map_t::field_info_t iol_type_map_t::collate(const iol_type_attributes_t index)const  noexcept {
	if((size_t)index < _map.size())
		return _map.at((size_t)index);
	else
		return field_info_t::undefined;
	}
