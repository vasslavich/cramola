#include "../../internal.h"
#include "../rdm_sessional_desc.h"


using namespace crm;
using namespace crm::detail;


static_assert(!srlz::detail::is_serialized_trait_v<rdm_sessional_descriptor_t>, __FILE_LINE__);
static_assert(srlz::detail::is_i_srlzd_prefixed_v<rdm_sessional_descriptor_t>, __FILE_LINE__);
static_assert(srlz::is_serializable_v<rdm_sessional_descriptor_t>, __FILE_LINE__);


const rdm_sessional_descriptor_t rdm_sessional_descriptor_t::null;


const rt1_endpoint_t& rdm_sessional_descriptor_t::ep()const noexcept {
	return _ep;
	}

const session_description_t& rdm_sessional_descriptor_t::sid()const noexcept {
	return _sid;
	}

bool rdm_sessional_descriptor_t::is_null()const noexcept {
	return (*this) == null;
	}

rdm_sessional_descriptor_t::rdm_sessional_descriptor_t()noexcept {}

rdm_sessional_descriptor_t::rdm_sessional_descriptor_t( const rt1_endpoint_t & ep,
	const session_description_t & sid )noexcept
	: _ep( ep )
	, _sid( sid ) {
	
	update_hash();
	}

void rdm_sessional_descriptor_t::update_hash()noexcept {
	_hash = address_hash_t();

	apply_at_hash( _hash, _ep );
	apply_at_hash( _hash, _sid );
	}

const address_hash_t& rdm_sessional_descriptor_t::object_hash()const noexcept {
	return _hash;
	}

size_t rdm_sessional_descriptor_t::get_serialized_size()const noexcept {
	return srlz::object_trait_serialized_size( _ep )
		+ srlz::object_trait_serialized_size( _sid )
		+ srlz::object_trait_serialized_size( _hash );
	}

bool rdm_sessional_descriptor_t::equals_fields( const rdm_sessional_descriptor_t& r )const noexcept {
	return ep() == r.ep()
		&& sid() == r.sid();
	}

bool rdm_sessional_descriptor_t::less_fields( const rdm_sessional_descriptor_t& r )const noexcept {
	if( ep() < r.ep() )
		return true;
	else if( ep() > r.ep() )
		return false;
	else
		return sid() < r.sid();
	}

bool rdm_sessional_descriptor_t::great_fields( const rdm_sessional_descriptor_t& r )const noexcept {
	if( ep() > r.ep() )
		return true;
	else if( ep() < r.ep() )
		return false;
	else
		return sid() > r.sid();
	}

bool rdm_sessional_descriptor_t::great_equals_fields( const rdm_sessional_descriptor_t& r )const noexcept {
	if( ep() > r.ep() )
		return true;
	else if( ep() < r.ep() )
		return false;
	else
		return sid() >= r.sid();
	}
