#include "../../internal.h"
#include "../../notifications/exceptions/excdrv.h"
#include "../subscr_tbl.h"
#include "../../context/context.h"


using namespace crm;
using namespace crm::detail;


#ifdef CBL_SUBSCRIBE_TABLE_TRACE
void subscribe_table_t::trace(const subscribe_table_t & obj,
	const _address_hndl_t & a, const std::string & pref) noexcept {

	std::ostringstream ol;
	ol << pref << ":" << std::to_string(obj._thisId.value()) << ":" << a.id().to_str() << ":" << cvt2string(a.level());

	file_logger_t::logging(ol.str(), 100203423);
	}
#endif


#ifdef CBL_OBJECTS_COUNTER_CHECKER
void subscribe_table_t::__size_trace()const noexcept{
	CHECK_NO_EXCEPTION( __sizeTraceer1.CounterCheck( _addressBook.size() ) );

	size_t accm2 = 0;
	for(const auto & item : _addressBook) {
		accm2 += item.second.size();
		}

	CHECK_NO_EXCEPTION(__sizeTraceer2.CounterCheck(accm2));
	}
#endif


subscribe_table_t::subscribe_table_t() noexcept{}

subscribe_table_t::subscribe_table_t(std::weak_ptr<distributed_ctx_t> ctx_)noexcept
	: _ctx(ctx_)
	, _thisId(CHECK_PTR(ctx_)->make_local_object_identity()) {}

subscribe_table_t::~subscribe_table_t() {
	CHECK_NO_EXCEPTION( __close() );
	}

void subscribe_table_t::close()noexcept {
	std::lock_guard lck(_tableLck);
	CHECK_NO_EXCEPTION( __close() );
	}

void subscribe_table_t::__regline(const line_num_t& lnId ) {

	auto itLn = _addressBook.find(lnId);
	if (itLn == _addressBook.end()) {
		auto ins = _addressBook.emplace(lnId, line_map_t{});
		if (!ins.second){
			FATAL_ERROR_FWD(nullptr);
			}
		}
	else {
		THROW_EXC_FWD(nullptr);
		}
	}

subscribe_invoker_result subscribe_table_t::__add(const line_num_t& lnId,
	async_space_t scx,
	const _address_hndl_t & address_,
	subscribe_message_handler && hdl,
	subscribe_options opt) {

	auto itLn = _addressBook.find(lnId);
	if (itLn != _addressBook.end()) {

		auto it = itLn->second.find(address_);
		if (itLn->second.end() != it) {
			CBL_VERIFY(it->first == address_);

			if(auto pHndl = std::atomic_load_explicit(&it->second, std::memory_order::memory_order_acquire)) {
				if(opt.flag == assign_options::overwrite_if_exits) {
					if(static_cast<int>(pHndl->once()) != static_cast<int>(opt.flag)) {
						FATAL_ERROR_FWD(nullptr);
						}

					pHndl->hndl.reset_handler(
#ifdef CBL_TRACELEVEL_TRACE_TAG_ON
						"subscribe_table_t::__add-1:" + hdl._tag.name,
#else
						"subscribe_table_t::__add-1:",
#endif

						std::move(hdl));

					CBL_SUBSCRIBE_TABLE_TRACE_DO(address_, "add:replace");
					return subscribe_invoker_result::enum_type::success;
					}
				else if(opt.flag == assign_options::insert_if_not_exists) {

					CBL_SUBSCRIBE_TABLE_TRACE_DO(address_, "add:exists:err");
					return subscribe_invoker_result::enum_type::err_exists;
					}
				else {
					FATAL_ERROR_FWD(nullptr);
					}
				}
			else {
				if(auto pTmp = std::atomic_exchange(&it->second,
					std::make_shared<message_subscribe_assist_t::message_addressed_link_t>(

#ifdef CBL_TRACELEVEL_TRACE_TAG_ON
						"subscribe_table_t::__add-1.1:" + hdl._tag.name,
#else
						"subscribe_table_t::__add-1.1:",
#endif

						std::move(hdl),
						opt.once,
						_ctx,
						scx,
						address_.invoke_abandoned(),
						address_.tagz()))) {

					reset(std::move(*pTmp));
					}

				return subscribe_invoker_result::enum_type::success;
				}
			}
		else {
			if (!itLn->second.insert({
				address_,
				std::make_shared<message_subscribe_assist_t::message_addressed_link_t>(

#ifdef CBL_TRACELEVEL_TRACE_TAG_ON
					"subscribe_table_t::__add-2:" + hdl._tag.name,
#else
					"subscribe_table_t::__add-2:",
#endif

					std::move(hdl),
					opt.once,
					_ctx,
					scx,
					address_.invoke_abandoned(),
					address_.tagz()) }).second) {

				FATAL_ERROR_FWD(nullptr);
				}

			CBL_SUBSCRIBE_TABLE_TRACE_DO(address_, "add");

#ifdef CBL_OBJECTS_COUNTER_CHECKER
			CHECK_NO_EXCEPTION(__size_trace());
#endif

			return subscribe_invoker_result::enum_type::success;
			}
		}
	else {
		THROW_EXC_FWD(nullptr);
		}
	}

void subscribe_table_t::reset(message_subscribe_assist_t::message_addressed_link_t && h)const noexcept{
	CHECK_NO_EXCEPTION( h.reset() );
	}

void subscribe_table_t::reset( std::list<message_subscribe_assist_t::message_addressed_link_t> && hl )const noexcept{
	for( auto && h : hl ){
		reset( std::move( h ) );
		}
	}

void subscribe_table_t::__close()noexcept {

	for( auto && lnIt : _addressBook ){
		for( auto && itHndl : lnIt.second ){

			/* �������� ������� ���������� */
			if(auto pHndl = std::atomic_exchange(&itHndl.second, {})) {
				CHECK_NO_EXCEPTION(reset(std::move(*pHndl)));
				}
			}
		}

	CHECK_NO_EXCEPTION( _addressBook.clear() );

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	CHECK_NO_EXCEPTION( __size_trace() );
#endif
	}


message_subscribe_assist_t::message_addressed_link_t subscribe_table_t::remove( const line_num_t& lnId,
	const _address_hndl_t& address_ )noexcept{

	message_subscribe_assist_t::message_addressed_link_t p;

	std::lock_guard lck(_tableLck);

	auto itLn = _addressBook.find( lnId );
	if( itLn != _addressBook.end() ){
		auto it = itLn->second.find( address_ );
		if( it != itLn->second.end() ){
			if(auto pHndl = std::atomic_exchange(&it->second, {})) {

				p = std::move(*pHndl);
				itLn->second.erase(it);

				CBL_SUBSCRIBE_TABLE_TRACE_DO(address_, "remove");
				}
			}

	#ifdef CBL_OBJECTS_COUNTER_CHECKER
		CHECK_NO_EXCEPTION( __size_trace() );
	#endif
		}

	return p;
	}

subscribe_invoker_result subscribe_table_t::subscribe(const line_num_t& lid,
	async_space_t scx,
	const _address_hndl_t & id,
	subscribe_options opt,
	subscribe_message_handler && handler) {

	std::lock_guard lck(_tableLck);
	return __add(lid, scx, id, std::move(handler), opt);
	}

void subscribe_table_t::regline(const line_num_t& lid) {
	std::lock_guard lck(_tableLck);
	__regline(lid);
	}

void subscribe_table_t::unsubscribe(const line_num_t& lnId, const _address_hndl_t & id)noexcept {
	auto h = remove(lnId, id);
	if (id.invoke_abandoned()) {
		invoke_abandoned(id, std::move(h));
		}
	else {
		reset(std::move(h));
		}
	}

std::list< std::tuple<_address_hndl_t, subscribe_table_t::_addressed_link_t>> 
subscribe_table_t::remove(const line_num_t& lnId)noexcept {

	std::list< std::tuple<_address_hndl_t, _addressed_link_t>> pl;

	std::lock_guard lck(_tableLck);

	auto itLn = _addressBook.find(lnId);
	if (itLn != _addressBook.end()) {

		for (auto & it : itLn->second) {
			CBL_SUBSCRIBE_TABLE_TRACE_DO(it.first, "remove by line id");

			if(auto pHndl = std::atomic_exchange(&it.second, {})) {
				pl.push_back(std::make_tuple(std::move(it.first), std::move(*pHndl)));
				}
			}

		_addressBook.erase(itLn);

#ifdef CBL_OBJECTS_COUNTER_CHECKER
		CHECK_NO_EXCEPTION(__size_trace());
#endif
		}

	return pl;
	}

void subscribe_table_t::close( const line_num_t& lnId )noexcept {
	CHECK_NO_EXCEPTION( invoke_abandoned( remove( lnId ) ) );
	}

void subscribe_table_t::unset(const line_num_t& lnId, const _address_hndl_t& addr)noexcept {
	std::lock_guard lck(_tableLck);

	auto itLn = _addressBook.find(lnId);
	if(itLn != _addressBook.end())
		itLn->second.erase(addr);
	}

void subscribe_table_t::lazy_remove_addr_hndl(const line_num_t& lnId,
	const _address_hndl_t& addr)noexcept {

	if(auto c = _ctx.lock()) {
		c->launch_async(__FILE_LINE__, [sptr = shared_from_this(), lnId, addr]{
			sptr->unset(lnId, addr);
			});
		}
	}

void subscribe_table_t::invoke_abandoned(std::list<std::tuple<_address_hndl_t, _addressed_link_t>> && hl)const noexcept {
	for (auto && il : hl) {
		CHECK_NO_EXCEPTION(invoke_abandoned(std::get<0>(il), std::move(std::get<1>(il))));
		}
	}

void subscribe_table_t::invoke_abandoned(
#ifdef CBL_SUBSCRIBE_TABLE_TRACE
	const _address_hndl_t& addr,
#else
	const _address_hndl_t&,
#endif

	_addressed_link_t&& hl)const noexcept {

	if (hl.hndl && !hl.hndl.invoked_count()) {
		CBL_SUBSCRIBE_TABLE_TRACE_DO(addr, "abandoned");
		}

	if (hl.abandonedInvoke) {
		_addressed_link_t tmph;
		std::swap(tmph, hl);
		}
	else {
		hl.reset();
		}
	}

void subscribe_table_t::addressed_exception(const line_num_t& lnId,
	const _address_hndl_t &addr,
	std::unique_ptr<dcf_exception_t>&& e,
	invoke_context_trait ic) noexcept {

	if(addr.level() == lnId.level()) {

		// ����� ����������
		invoke_find(lnId, addr, ic, [this, &e] {

			/* ��������� */
			return std::move(e);
			});
		}
	}


std::unique_ptr<i_iol_ihandler_t> subscribe_table_t::check_address(const line_num_t& lnId,
	std::unique_ptr<i_iol_ihandler_t> && obj,
	invoke_context_trait ic)  {

	CBL_MPF_KEYTRACE_SET((*obj), 71);

	if( obj && obj->level() == lnId.level() ){

		/* ���� ��������� ����� ������� ���������� */
		if( obj && obj->address().fl_destination_subscribe_id() ){

			const auto destAddress = obj->destination_subscriber_id();
			CBL_VERIFY( !is_null( destAddress ));

			/* ������������� �������� */
			auto idHndl = _address_hndl_t::make( destAddress, obj->type().utype(), obj->level(), async_space_t::undef );

			// ����� ����������
			invoke_find( lnId, idHndl, ic, [this, &obj]{
				CBL_MPF_KEYTRACE_SET((*obj), 72);

				/* ��������� */
				return std::move( obj );
				} );

#ifdef CBL_MPF_TRACELEVEL_EVENTS_RT0RT1
			if(obj) {
				CBL_MPF_KEYTRACE_SET((*obj), 74);

				std::ostringstream sout;
				sout << "message handler not founded:";
				sout << "type=" + obj->type().utype() << ":";
				sout << "level=" << cvt2string( obj->address().level()) << ":";
				sout << "dest subscriber id=" << obj->address().destination_subscriber_id().to_str() << ":";
				sout << "source id=" << obj->address().source_id().to_str() << ":";
				sout << "dest id=" << obj->destination_id().to_str() << ":";

				CBL_SUBSCRIBE_TABLE_TRACE_DO(idHndl, sout.str());
				}
#endif

#ifdef CBL_MPF_REDIRECT_NOT_FOUNDED_SUBSCRIBED_MESSAGE_TO_BASE
#else
			/* ������������� ������� */
			CHECK_NO_EXCEPTION(obj.reset());
#endif
			}
		}

	return std::move(obj);
	}


template<>
subscribe_message_handler::functor_type crm::detail::release_as<subscribe_message_handler::functor_type>(subscribe_message_handler && h)noexcept {
	return std::move(h).release_as_unique();
	}


template<>
subscribe_message_handler::functor_shared_type crm::detail::release_as<subscribe_message_handler::functor_shared_type>(subscribe_message_handler && h)noexcept {
	return std::move(h).release_as_shared();
	}
