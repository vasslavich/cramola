#include "../../internal.h"
#include "../rtl_rdm_event_desc.h"


using namespace crm;
using namespace crm::detail;


static_assert(srlz::detail::is_i_srlzd_prefixed_v<rtl_roadmap_event_t>, __FILE_LINE__);
static_assert(srlz::is_serializable_v<rtl_roadmap_event_t>, __FILE_LINE__);


const rtl_roadmap_event_t rtl_roadmap_event_t::null;

rtl_roadmap_event_t::rtl_roadmap_event_t() noexcept{}

rtl_roadmap_event_t::rtl_roadmap_event_t( const identity_descriptor_t & glid_,
	const subscriber_event_id_t& locid_,
	rtl_table_t rt_,
	rtl_level_t l )noexcept
	: _glid( glid_ )
	, _lcid( locid_ )
	, _rt( rt_ )
	, _l( l ){

	apply_at_hash( _hash, _glid );
	apply_at_hash( _hash, _lcid );
	apply_at_hash( _hash, _rt );
	apply_at_hash( _hash, _l );
	}

rtl_roadmap_event_t rtl_roadmap_event_t::make( const identity_descriptor_t & gi,
	const subscriber_event_id_t& li,
	rtl_table_t rt,
	rtl_level_t l ) noexcept{

	return rtl_roadmap_event_t( gi, li, rt, l );
	}

bool rtl_roadmap_event_t::is_null()const noexcept{
	return (*this) == null;
	}

rtl_level_t rtl_roadmap_event_t::level()const noexcept{
	return _l;
	}

const rtl_roadmap_event_t::hash_t& rtl_roadmap_event_t::object_hash()const noexcept{
	return _hash;
	}

const	identity_descriptor_t& rtl_roadmap_event_t::glid()const noexcept{
	return _glid;
	}

const subscriber_event_id_t& rtl_roadmap_event_t::lcid()const noexcept{
	return _lcid;
	}

const rtl_table_t& rtl_roadmap_event_t::rt()const noexcept{
	return _rt;
	}

bool rtl_roadmap_event_t::equals_fields( const rtl_roadmap_event_t& r )const noexcept{
	return glid() == r.glid()
		&& lcid() == r.lcid()
		&& rt() == r.rt();
	}

bool rtl_roadmap_event_t::less_fields( const rtl_roadmap_event_t& r )const noexcept{

	if( glid() < r.glid() )
		return true;
	else if( glid() > r.glid() )
		return false;
	else if( lcid() < r.lcid() )
		return true;
	else if( lcid() > r.lcid() )
		return false;
	else if( level() < r.level() )
		return true;
	else if( level() > r.level() )
		return false;
	else
		return rt() < r.rt();
	}

bool rtl_roadmap_event_t::great_fields( const rtl_roadmap_event_t& r )const noexcept{

	if( glid() > r.glid() )
		return true;
	else if( glid() < r.glid() )
		return false;
	else if( lcid() > r.lcid() )
		return true;
	else if( lcid() < r.lcid() )
		return false;
	else if( level() > r.level() )
		return true;
	else if( level() < r.level() )
		return false;
	else
		return rt() > r.rt();
	}

bool rtl_roadmap_event_t::great_equals_fields( const rtl_roadmap_event_t& r )const noexcept{

	if( glid() > r.glid() )
		return true;
	else if( glid() < r.glid() )
		return false;
	else if( lcid() > r.lcid() )
		return true;
	else if( lcid() < r.lcid() )
		return false;
	else if( level() > r.level() )
		return true;
	else if( level() < r.level() )
		return false;
	else
		return rt() >= r.rt();
	}


size_t rtl_roadmap_event_t::get_serialized_size()const noexcept{
	return srlz::object_trait_serialized_size(_glid)
		+ srlz::object_trait_serialized_size(_lcid)
		+ srlz::object_trait_serialized_size(_rt)
		+ srlz::object_trait_serialized_size(_l)
		+ srlz::object_trait_serialized_size(_hash);
	}

