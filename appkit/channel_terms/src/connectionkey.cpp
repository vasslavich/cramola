#include "../../internal.h"
#include "../connection_key.h"
#include "../../utilities/convert/cvt.h"


using namespace crm;


const connection_key_t null_value_t<connection_key_t>::null = connection_key_t{ "", 0 };
static_assert(std::is_same_v<
	std::decay_t<decltype(null_value_t<connection_key_t>::null)>, 
	std::decay_t<decltype( null_trait<connection_key_t>::get_null_value())>>, __FILE_LINE__);


static_assert(!srlz::detail::is_serialized_trait_v<connection_key_t>, __FILE_LINE__);
static_assert(srlz::detail::is_strob_serializable_v<connection_key_t>, __FILE_LINE__);
static_assert(srlz::detail::is_strob_flat_serializable_v<connection_key_t>, __FILE_LINE__);
static_assert(srlz::is_serializable_v<connection_key_t>, __FILE_LINE__);
static_assert(has_null_value_type_specialization_v<connection_key_t>, __FILE_LINE__);


std::ostream  & crm::operator <<(std::ostream & os, const connection_key_t &ck) {
	os << ck.address << ":" << ck.port;
	return os;
	}

std::string connection_key_t::to_string()const noexcept {
	static_assert(noexcept(std::move(std::string{})), __FILE_LINE__);
	
	return address + ":" + std::to_string( port );
	}

std::wstring connection_key_t::to_wstring()const noexcept {
	return cvt<wchar_t>(address) + L":" + std::to_wstring(port);
	}

bool crm::operator == (const connection_key_t & lval, const connection_key_t &rval) noexcept {
	return lval.address == rval.address && lval.port == rval.port;
	}

bool crm::operator != (const connection_key_t & lval, const connection_key_t &rval) noexcept {
	return !(lval == rval);
	}

bool crm::operator < (const connection_key_t & lval, const connection_key_t &rval) noexcept {
	if( lval.port < rval.port )
		return true;
	else if( lval.port > rval.port )
		return false;
	else
		return lval.address < rval.address;
	}

bool crm::operator > (const connection_key_t & lval, const connection_key_t &rval)noexcept {
	if( lval.port > rval.port )
		return true;
	else if( lval.port < rval.port )
		return false;
	else
		return lval.address > rval.address;
	}

bool crm::operator >= (const connection_key_t & lval, const connection_key_t &rval)noexcept {
	if( lval.port > rval.port )
		return true;
	else if( lval.port < rval.port )
		return false;
	else
		return lval.address >= rval.address;
	}

void crm::apply_at_hash( address_hash_t & to, const connection_key_t & value ) {
	apply_at_hash( to, value.address );
	apply_at_hash( to, value.port );
	}
