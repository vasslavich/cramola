#include "../../internal.h"


using namespace crm;
using namespace crm::detail;


static_assert(!srlz::detail::is_serialized_trait_v<rt0_node_rdm_t>, __FILE_LINE__);
static_assert(srlz::detail::is_i_srlzd_prefixed_v<rt0_node_rdm_t>, __FILE_LINE__);
static_assert(srlz::is_serializable_v<rt0_node_rdm_t>, __FILE_LINE__);


/*===========================================================================================
rt0_node_rdm_t
=============================================================================================*/

const rt0_node_rdm_t rt0_node_rdm_t::null;

rt0_node_rdm_t::rt0_node_rdm_t()noexcept {
	CBL_VERIFY(::is_null(_nodeId));
	CBL_VERIFY(::is_null(_sessionId));
	CBL_VERIFY(::is_null(_cnkeyAssigned));
	CBL_VERIFY(::is_null(_cnkeyRequested));
	CBL_VERIFY(::is_null(_direction));
	CBL_VERIFY(::is_null(_hash));
	}

rt0_node_rdm_t::rt0_node_rdm_t( const identity_descriptor_t& nodeId,
	const session_description_t & nodeLocid,
	const connection_key_t& requestedCnk,
	const connection_key_t& assignedCnk,
	rtl_table_t direction )noexcept
	: _nodeId( nodeId )
	, _sessionId( nodeLocid )
	, _cnkeyAssigned( assignedCnk ) 
	, _cnkeyRequested(requestedCnk)
	, _direction( direction ){

	update_hash();
	}

rt0_node_rdm_t rt0_node_rdm_t::make_output_rdm( const identity_descriptor_t& nodeId,
	const session_description_t & nodeLocid,
	const connection_key_t& requestCK,
	const connection_key_t& assignedCK )noexcept {

	return rt0_node_rdm_t( nodeId, 
		nodeLocid, 
		requestCK, 
		assignedCK,
		rtl_table_t::out );
	}

rt0_node_rdm_t rt0_node_rdm_t::make_input_rdm( const identity_descriptor_t& nodeId,
	const session_description_t & nodeLocid,
	const connection_key_t& acceptedCK ) noexcept {

	return rt0_node_rdm_t( nodeId,
		nodeLocid, 
		connection_key_t{},
		acceptedCK,
		rtl_table_t::in );
	}

rt0_node_rdm_t rt0_node_rdm_t::create_rt1_router_rdm(const std::string& name)noexcept {
	return rt0_node_rdm_t({}, {}, {}, { name, std::numeric_limits<int>::max() }, rtl_table_t::rt1_router);
	}

void rt0_node_rdm_t::update_hash()  noexcept {
	_hash = address_hash_t();

	apply_at_hash( _hash, _cnkeyRequested );
	apply_at_hash( _hash, _cnkeyAssigned );
	apply_at_hash( _hash, _nodeId );
	apply_at_hash( _hash, _sessionId.object_hash() );
	apply_at_hash( _hash, _direction );

	CBL_VERIFY(!::is_null(_hash));
	}

void rt0_node_rdm_t::set_direction( rtl_table_t direction ) {
	_direction = direction;
	update_hash();
	}

void rt0_node_rdm_t::set_node_id( const identity_descriptor_t & id ) {
	_nodeId = id;
	update_hash();
	}

void rt0_node_rdm_t::set_session( const session_description_t & s ) {
	_sessionId = s;
	update_hash();
	}

void rt0_node_rdm_t::set_connection_key( const connection_key_t & cnk ) {
	_cnkeyAssigned = cnk;
	update_hash();
	}

const rt0_node_rdm_t::hash_t& rt0_node_rdm_t::object_hash()const noexcept {
	return _hash;
	}

const identity_descriptor_t& rt0_node_rdm_t::node_id()const noexcept {
	return _nodeId;
	}


bool rt0_node_rdm_t::is_output()const noexcept {
	return !crm::is_null( _cnkeyRequested );
	}

bool rt0_node_rdm_t::is_input()const noexcept {
	return !is_output();
	}

const session_description_t& rt0_node_rdm_t::session()const noexcept {
	return _sessionId;
	}

const connection_key_t& rt0_node_rdm_t::connection_key()const noexcept {
	return _cnkeyAssigned;
	}

const connection_key_t& rt0_node_rdm_t::requested_key()const noexcept {
	return _cnkeyRequested;
	}

rtl_table_t rt0_node_rdm_t::direction()const noexcept {
	return _direction;
	}

bool rt0_node_rdm_t::is_null()const noexcept {
	return (*this) == null;
	}

std::string rt0_node_rdm_t::to_str()const noexcept {
	std::ostringstream s;
	s << "[" << _nodeId.to_str()
		<< ":" << _sessionId.to_str()
		<< ":" << _cnkeyRequested.address << ":" << _cnkeyRequested.port
		<< ":" << _cnkeyAssigned.address << ":" << _cnkeyAssigned.port
		<< ":" << cvt2string( _direction ) << "]";
	return s.str();
	}

size_t rt0_node_rdm_t::get_serialized_size()const noexcept {
	return srlz::object_trait_serialized_size(_nodeId)
		+ srlz::object_trait_serialized_size(_sessionId)
		+ srlz::object_trait_serialized_size(_direction)
		+ srlz::object_trait_serialized_size(_cnkeyAssigned)
		+ srlz::object_trait_serialized_size(_cnkeyRequested)
		+ srlz::object_trait_serialized_size( _hash );
	}

bool rt0_node_rdm_t::equals_fields( const rt0_node_rdm_t& r )const noexcept {
	return node_id() == r.node_id()
		&& session() == r.session()
		&& connection_key() == r.connection_key()
		&& requested_key() == r.requested_key()
		&& direction() == r.direction();
	}

bool rt0_node_rdm_t::less_fields( const rt0_node_rdm_t& r )const noexcept {
	if( node_id() < r.node_id() )
		return true;
	else if( node_id() > r.node_id() )
		return false;
	else if( session() < r.session() )
		return true;
	else if( session() > r.session() )
		return false;
	else if( connection_key() < r.connection_key() )
		return true;
	else if( connection_key() > r.connection_key() )
		return false;
	else if (requested_key() < r.requested_key())
		return true;
	else if (requested_key() > r.requested_key())
		return false;
	else
		return direction() < r.direction();
	}

bool rt0_node_rdm_t::great_fields( const rt0_node_rdm_t& r )const noexcept {
	if( node_id() > r.node_id() )
		return true;
	else if( node_id() < r.node_id() )
		return false;
	else if( session() > r.session() )
		return true;
	else if( session() < r.session() )
		return false;
	else if( connection_key() > r.connection_key() )
		return true;
	else if( connection_key() < r.connection_key() )
		return false;
	else if (requested_key() > r.requested_key())
		return true;
	else if (requested_key() < r.requested_key())
		return false;
	else
		return direction() > r.direction();
	}

bool rt0_node_rdm_t::great_equals_fields( const rt0_node_rdm_t& r )const noexcept {
	if( node_id() > r.node_id() )
		return true;
	else if( node_id() < r.node_id() )
		return false;
	else if( session() > r.session() )
		return true;
	else if( session() < r.session() )
		return false;
	else if( connection_key() > r.connection_key() )
		return true;
	else if( connection_key() < r.connection_key() )
		return false;
	else if (requested_key() > r.requested_key())
		return true;
	else if (requested_key() < r.requested_key())
		return false;
	else
		return direction() >= r.direction();
	}

std::ostream  & crm::operator <<(std::ostream & os, const rt0_node_rdm_t & v) {
	os << v.node_id().to_str() << ":" << v.session().to_str();
	return os;
	}

std::wostream  & crm::operator <<(std::wostream & os, const rt0_node_rdm_t & v) {
	os << v.node_id().to_wstr() << L":" << cvt<wchar_t>( v.session().to_str() );
	return os;
	}
