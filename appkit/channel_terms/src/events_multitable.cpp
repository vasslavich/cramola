#include "../../internal.h"
#include "../events_tbl.h"


using namespace crm;
using namespace crm::detail;


events_multitable_t::events_multitable_t( std::weak_ptr<distributed_ctx_t> ctx_,
	size_t wide )
	: _ctx( ctx_ ){

	_table.resize( wide );
	for( size_t i = 0; i < wide; ++i ){
		_table[i] = std::make_unique<events_table_t>( ctx_ );
		}
	}

events_multitable_t::handler_t events_multitable_t::make_handler(){
	return handler_t( _lineIdCounter++, weak_from_this() );
	}

size_t events_multitable_t::h2id( const handler_t & ln )const noexcept{
	return ln.lineId % _table.size();
	}

void events_multitable_t::invoke( const handler_t & ln,
	event_type_t type, std::unique_ptr<event_args_t> && args )noexcept{

	CHECK_NO_EXCEPTION( _table.at( h2id( ln ) )->invoke( ln.lineId, type, std::move( args ) ) );
	}

subscribe_result events_multitable_t::subscribe( const handler_t & ln,
	async_space_t scx,
	const address_desc & id,
	const bool once,
	__event_handler_f && handler,
	check_condition_f && checkPred){

	return _table.at(h2id(ln))->subscribe(ln.lineId, scx, id, once, std::move(handler), std::move(checkPred));
	}

void events_multitable_t::unsubscribe( const handler_t & ln, const address_desc & id )noexcept{
	const auto cell = h2id( ln );
	const auto lineId = ln.lineId;
	
	CHECK_NO_EXCEPTION( _table.at( cell )->unsubscribe( lineId, id ) );
	}

void events_multitable_t::close( const handler_t & ln )noexcept{
	const auto cell = h2id( ln );
	const auto lineId = ln.lineId;

	CHECK_NO_EXCEPTION( _table.at( cell )->close( lineId ) );
	}

events_list events_multitable_t::release(const handler_t & ln)noexcept {
	return _table.at(h2id(ln))->release(ln.lineId);
	}

void events_multitable_t::add(const handler_t & ln, events_list &&el) {
	_table.at(h2id(ln))->add(ln.lineId, std::move(el));
	}

void events_multitable_t::regline(const handler_t& ln) {
	_table.at(h2id(ln))->regline(ln.lineId);
	}

