#include "../../internal.h"
#include "../../context/context.h"
#include "../../logging/logger.h"
#include "../../logging/keycounter.h"
#include "../xtract.h"


using namespace crm;
using namespace crm::detail;


_packet_t crm::detail::make_exception_RZi_LX( const std::shared_ptr<distributed_ctx_t> &ctx,
	const address_descriptor_t & destAddr, std::unique_ptr<dcf_exception_t> && e_ )noexcept{

	CBL_VERIFY( e_ && e_->code() );

	CBL_MPF_KEYTRACE_SET( destAddr, 323 );

	onotification_t epack_;
	epack_.set_typedriver( ctx->types_driver() );
	epack_.set_ntf( std::move( e_ ) );

	auto ws = srlz::wstream_constructor_t::make();
	srlz::serialize( ws, epack_ );

	CBL_MPF_KEYTRACE_SET( destAddr, 324 );

	CBL_VERIFY(!is_null(destAddr.destination_subscriber_id()));
	CBL_VERIFY(!is_null(destAddr.destination_id()));

	return _packet_t( iol_type_spcf_t::build(
		iol_type_spcf_t::make_attrb_pair( iol_type_attributes_t::type, iol_types_t::st_RZi_LX_error ) ),
		destAddr,
		ws.release() );
	}


bool crm::detail::is_exception_RZi_LX( const _packet_t & u )noexcept{
	return u.type().type() == iol_types_t::st_RZi_LX_error;
	}


void crm::detail::exception_RZi_LX_extract( const distributed_ctx_t &ctx,
	_packet_t && p, address_descriptor_t & destAddr, std::unique_ptr<dcf_exception_t> & e_ ) noexcept{

	if( is_exception_RZi_LX( p ) ){
		inotification_t epack_;
		epack_.set_typedriver( ctx.types_driver() );

		auto rs = srlz::rstream_constructor_t::make( p.data().cbegin(), p.data().cend() );
		
		static_assert(srlz::detail::is_i_srlzd_v<decltype(epack_)> && !srlz::detail::is_serialized_trait_r_v<decltype(epack_)>, __FILE_LINE__);
		srlz::deserialize( rs, epack_ );

		e_ = epack_.release_ntf();
		destAddr = p.address();

		CBL_MPF_KEYTRACE_SET( destAddr, 335 );

		CBL_VERIFY(!is_null(destAddr.destination_subscriber_id()));
		CBL_VERIFY(!is_null(destAddr.destination_id()));
		}
	}

void crm::detail::exception_RZi_LX_extract( const std::shared_ptr<distributed_ctx_t> &ctx,
	_packet_t && p, address_descriptor_t & destAddr, std::unique_ptr<dcf_exception_t> & e_ ) noexcept{

	if( ctx ){
		exception_RZi_LX_extract( (*ctx), std::move( p ), destAddr, e_ );
		}
	}

