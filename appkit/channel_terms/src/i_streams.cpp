#include "../../internal.h"
#include "../i_stream.h"

using namespace crm;
using namespace crm::detail;


const rtl_roadmap_obj_t& i_x1_stream_address_handler::this_rdm()const noexcept {
	return rtl_roadmap_obj_t::null;
	}

const rtl_roadmap_obj_t& i_x1_stream_address_handler::remote_rdm()const noexcept {
	return rtl_roadmap_obj_t::null;
	}

const identity_descriptor_t& i_x1_stream_address_handler::remote_id()const noexcept {
	return identity_descriptor_t::null;
	}

const identity_descriptor_t& i_x1_stream_address_handler::this_id()const noexcept {
	return identity_descriptor_t::null;
	}

const identity_descriptor_t& i_x1_stream_address_handler::id()const noexcept {
	return identity_descriptor_t::null;
	}

const i_x1_stream_address_handler::xpeer_desc_t& i_x1_stream_address_handler::desc()const noexcept {
	return xpeer_desc_t::null;
	}

const rt0_node_rdm_t& i_x1_stream_address_handler::node_id()const noexcept {
	return rt0_node_rdm_t::null;
	}

const local_object_id_t& i_x1_stream_address_handler::local_id()const noexcept {
	return local_object_id_t::null;
	}

const local_command_identity_t& i_x1_stream_address_handler::local_object_command_id()const noexcept {
	return local_command_identity_t::null;
	}

const session_description_t& i_x1_stream_address_handler::session()const noexcept {
	return session_description_t::null;
	}

const std::string& i_x1_stream_address_handler::address()const noexcept {
	return null_address;
	}

int i_x1_stream_address_handler::port()const noexcept {
	return null_port;
	}

i_x1_stream_address_handler::remote_object_endpoint_t i_x1_stream_address_handler::remote_object_endpoint()const noexcept {
	return remote_object_endpoint_t::null;
	}

connection_key_t i_x1_stream_address_handler::connection_key()const noexcept {
	return null_value_t<connection_key_t>::null;
	}

rtl_table_t i_x1_stream_address_handler::direction()const noexcept {
	return rtl_table_t::undefined;
	}

const std::string& i_x1_stream_address_handler::name()const noexcept {
	return _name;
	}

remote_object_state_t i_x1_stream_address_handler::state()const noexcept {
	return _state.load(std::memory_order::memory_order_acquire);
	}

void i_x1_stream_address_handler::set_name(std::string n_) {
	_name = std::move(n_);
	}

void i_x1_stream_address_handler::set_state(remote_object_state_t st)noexcept {
	if (remote_object_state_t::closed != _state.load(std::memory_order::memory_order_acquire)) {
		auto iprev = _state.exchange(st);
		ERR_VERIFY_TEXT(iprev <= st, std::string(cvt2string(iprev)) + "->" + std::string(cvt2string(st)));
		}
	}

rtl_level_t i_x1_stream_address_handler::level()const noexcept {
	return rtl_level_t::l1;
	}

