#include "../../internal.h"
#include "../events_tbl.h"


using namespace crm;
using namespace crm::detail;



#ifdef CBL_OBJECTS_COUNTER_CHECKER
void events_table_t::__size_trace()const noexcept{
	CHECK_NO_EXCEPTION( __sizeTraceer1.CounterCheck( _addressBook.size() ) );

	size_t accm2 = 0;
	for( const auto & item : _addressBook ){
		accm2 += item.second.size();
		}

	CHECK_NO_EXCEPTION( __sizeTraceer2.CounterCheck( accm2 ) );
	}
#endif


events_table_t::events_table_t()noexcept{}

events_table_t::events_table_t( std::weak_ptr<distributed_ctx_t> ctx_ )noexcept
	: _ctx( ctx_ ){}

events_table_t::~events_table_t(){
	CHECK_NO_EXCEPTION( __close() );
	}

void events_table_t::close()noexcept{
	CHECK_NO_EXCEPTION_BEGIN

	lck_scp_t lck( _lck );
	__close();

	CHECK_NO_EXCEPTION_END
	}

void events_table_t::__close()noexcept{

	for( auto && lnIt : _addressBook ){
		for( auto && itHndl : lnIt.second ){

			/* �������� ����� �������� ����������� */
			CHECK_NO_EXCEPTION( reset( std::move( itHndl ) ) );
			}
		}

	CHECK_NO_EXCEPTION( _addressBook.clear() );

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	CHECK_NO_EXCEPTION( __size_trace() );
#endif
	}

void events_table_t::__add(const line_num_t& lnId,
	async_space_t scx,
	const address_desc& address_,
	__event_handler_f&& hdl,
	bool once) {

	auto itLn = _addressBook.find(lnId);
	if (itLn != _addressBook.end()) {
		itLn->second.push_back(std::make_shared<event_addressed_link_t>(std::move(hdl), address_, once, _ctx, scx));

#ifdef CBL_OBJECTS_COUNTER_CHECKER
		CHECK_NO_EXCEPTION(__size_trace());
#endif
		}
	else {
		THROW_EXC_FWD(nullptr);
		}
	}

void events_table_t::__reg_line(const line_num_t& lnId ) {

	auto itLn = _addressBook.find(lnId);
	if (itLn == _addressBook.end()) {
		auto ins = _addressBook.emplace(lnId, line_map_t{});
		if (!ins.second) {
			FATAL_ERROR_FWD(nullptr);
			}

#ifdef CBL_OBJECTS_COUNTER_CHECKER
		CHECK_NO_EXCEPTION(__size_trace());
#endif
		}
	else {
		THROW_EXC_FWD(nullptr);
		}
	}

subscribe_result events_table_t::_subscribe( const line_num_t& lnId,
	async_space_t scx,
	const address_desc & id,
	const bool once,
	__event_handler_f && handler,
	check_condition_f && checkPred){

	if(handler) {

		lck_scp_t lck(_lck);

		decltype(checkPred(id.type())) checkf;

		if(checkPred) {
			checkf = checkPred(id.type());
			}

		if(checkf) {
			if(auto c = _ctx.lock()) {
				c->launch_async(scx, __FILE_LINE__, std::move(handler), std::move(checkf), subscribe_invoke::without_subscribtion);
				}

			return subscribe_result::enum_type::success;
			}
		else {
			__add(lnId, scx, id, std::move(handler), once);
			return subscribe_result::enum_type::success;
			}
		}
	else {
		THROW_MPF_EXC_FWD(nullptr);
		}
	}

events_list events_table_t::release(const line_num_t& lnId)noexcept {
	lck_scp_t lck(_lck);

	events_list result;

	auto itLn = _addressBook.find(lnId);
	if(itLn != _addressBook.end()) {
		result = events_list(std::move(itLn->second));
		_addressBook.erase(lnId);
		}

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	CHECK_NO_EXCEPTION(__size_trace());
#endif

	return result;
	}

void events_table_t::regline(const line_num_t& lnId) {
	lck_scp_t lck(_lck);
	__reg_line(lnId);
	}

void events_table_t::add(const line_num_t& lnId, events_list && el) {
	lck_scp_t lck(_lck);

	auto itLn = _addressBook.find(lnId);
	if (itLn != _addressBook.end()) {
		itLn->second.insert(itLn->second.end(), std::make_move_iterator(el.lst.begin()), std::make_move_iterator(el.lst.end()));

#ifdef CBL_OBJECTS_COUNTER_CHECKER
		CHECK_NO_EXCEPTION(__size_trace());
#endif
		}
	else {
		THROW_EXC_FWD(nullptr);
		}
	}


std::list<std::shared_ptr<events_table_t::event_addressed_link_t>> events_table_t::remove( const line_num_t& lnId,
	const address_desc& address_ )noexcept{

	std::list<std::shared_ptr<event_addressed_link_t>> pl;

	CHECK_NO_EXCEPTION_BEGIN

	lck_scp_t lck(_lck);

	auto itLn = _addressBook.find( lnId );
	if( itLn != _addressBook.end() ){

		auto it = itLn->second.begin();
		while( it != itLn->second.end() ){

			if( address_ == (*it)->subscriberId ){
				pl.push_back( std::move( (*it) ) );
				it = itLn->second.erase( it );
				}
			else{
				++it;
				}
			}

	#ifdef CBL_OBJECTS_COUNTER_CHECKER
		CHECK_NO_EXCEPTION( __size_trace() );
	#endif
		}

	CHECK_NO_EXCEPTION_END

	return pl;
	}

std::list<std::shared_ptr<events_table_t::event_addressed_link_t>> events_table_t::remove( const line_num_t& lnId )noexcept{
	std::list<std::shared_ptr<event_addressed_link_t>> pl;

	CHECK_NO_EXCEPTION_BEGIN

	lck_scp_t lck( _lck );

	auto itLn = _addressBook.find( lnId );
	if( itLn != _addressBook.end() ){

		for( auto & it : itLn->second ){
			pl.push_back( std::move( it ) );
			}

		_addressBook.erase( itLn );

	#ifdef CBL_OBJECTS_COUNTER_CHECKER
		CHECK_NO_EXCEPTION( __size_trace() );
	#endif
		}

	CHECK_NO_EXCEPTION_END

	return pl;
	}

void events_table_t::reset(std::shared_ptr<event_addressed_link_t> && h )noexcept{
	if(h) {
		h->hndl.reset();
		}
	}

void events_table_t::reset(std::list<std::shared_ptr<event_addressed_link_t>> && lh)noexcept {
	for (auto && x : lh) {
		reset(std::move(x));
		}
	}

void events_table_t::unsubscribe( const line_num_t& lnId, const address_desc & id )noexcept{
	CHECK_NO_EXCEPTION( reset( remove( lnId, id ) ) );
	}

void events_table_t::close( const line_num_t& lnId )noexcept{
	CHECK_NO_EXCEPTION( invoke( remove( lnId ), std::make_unique<event_abandoned_t>() ) );
	}

void events_table_t::invoke(std::shared_ptr<event_addressed_link_t> & h, std::unique_ptr<i_event_t> && iarg )noexcept{
	if(h) {
		CHECK_NO_EXCEPTION(h->hndl.invoke_async(std::move(iarg), subscribe_invoke::by_subscribion));
		}
	}

void events_table_t::invoke( std::list<std::shared_ptr<event_addressed_link_t>> && il, std::unique_ptr<event_args_t> && iarg )noexcept{
	for( auto && i : il ){
		CHECK_NO_EXCEPTION( invoke( i, iarg ? iarg->copy() : nullptr ) );
		}
	}

subscribe_result events_table_t::subscribe(const line_num_t& lkey,
	async_space_t scx,
	const address_desc & id,
	const bool once,
	__event_handler_f && handler,
	check_condition_f && checkPred) {

	return _subscribe(lkey, scx, id, once, std::move(handler), std::move(checkPred));
	}

void events_table_t::invoke( const line_num_t & ln, 
	event_type_t type, 
	std::unique_ptr<event_args_t> && iarg ) noexcept{

	std::list<std::shared_ptr<event_addressed_link_t>> esl;

	lck_scp_t lck( _lck );

	auto itLine = _addressBook.find( ln );
	if(itLine != _addressBook.end()) {

		auto itH = itLine->second.begin();
		while(itH != itLine->second.end()) {

			if((*itH)->subscriberId.type() == type) {

				/* �������, ���� ���������� */
				if((*itH)->once) {
					esl.push_back(std::move(*itH));

					itH = itLine->second.erase(itH);
					}
				else {
					esl.push_back(*itH);

					++itH;
					}
				}
			else {
				++itH;
				}
			}
		}

	lck.unlock();

	invoke(std::move(esl), std::move(iarg));
	}



