#include "../../internal.h"
#include "../../notifications/exceptions/excdrv.h"
#include "../subscribe_event_addr.h"

using namespace crm;
using namespace crm::detail;


static_assert(srlz::detail::is_i_srlzd_prefixed_v<_address_event_t>, __FILE_LINE__);


const _address_event_t _address_event_t::null;

_address_event_t _address_event_t::make()noexcept {
	return _address_event_t();
	}

_address_event_t _address_event_t::make(const subscriber_event_id_t& id,
	const channel_identity_t & chid,
	event_type_t type,
	rtl_level_t level) noexcept {

	return _address_event_t(id, chid, type, level);
	}

_address_event_t::_address_event_t()noexcept {}

_address_event_t::_address_event_t(const subscriber_event_id_t& id,
	const channel_identity_t & chid,
	event_type_t type,
	rtl_level_t level)noexcept
	: _id(id)
	, _chid(chid)
	, _type(type)
	, _level(level) {

	update_hash();
	}

size_t _address_event_t::get_serialized_size()const noexcept{
	return srlz::object_trait_serialized_size(_id)
		+ srlz::object_trait_serialized_size(_type)
		+ srlz::object_trait_serialized_size(_level)
		+ srlz::object_trait_serialized_size(_chid)
		+ srlz::object_trait_serialized_size(_hash);
	}

void _address_event_t::update_hash() noexcept {
	_hash = address_hash_t::null;

	apply_at_hash(_hash, _id);
	apply_at_hash(_hash, _type);
	apply_at_hash(_hash, _level);
	apply_at_hash(_hash, _chid);
	}

const _address_event_t::hash_t& _address_event_t::object_hash()const noexcept {
	return _hash;
	}

const channel_identity_t& _address_event_t::channel()const noexcept {
	return _chid;
	}

const subscriber_event_id_t& _address_event_t::id()const noexcept {
	return _id;
	}

event_type_t _address_event_t::type()const noexcept {
	return _type;
	}

rtl_level_t _address_event_t::level()const noexcept {
	return _level;
	}

bool _address_event_t::is_null()const noexcept {
	return (*this) == null;
	}

bool _address_event_t::equals_fields(const _address_event_t& r)const noexcept {
	return level() == r.level() && type() == r.type() && id() == r.id() && channel() == r.channel();
	}

bool _address_event_t::less_fields(const _address_event_t& r)const noexcept {
	if(level() < r.level())
		return true;
	else if(level() > r.level())
		return false;
	else {

		if(type() < r.type())
			return true;
		else if(type() > r.type())
			return false;
		else {

			if(channel() < r.channel())
				return true;
			if(channel() > r.channel())
				return false;
			else {

				return id() < r.id();
				}
			}
		}
	}

bool _address_event_t::great_fields(const _address_event_t& r)const noexcept {
	if(level() > r.level())
		return true;
	else if(level() < r.level())
		return false;
	else {

		if(type() > r.type())
			return true;
		else if(type() < r.type())
			return false;
		else {

			if(channel() > r.channel())
				return true;
			else if(channel() < r.channel())
				return false;
			else {

				return id() > r.id();
				}
			}
		}
	}

bool _address_event_t::great_equals_fields(const _address_event_t& r)const noexcept {
	if(level() < r.level())
		return false;
	else if(level() > r.level())
		return true;
	else {

		if(type() < r.type())
			return false;
		else if(type() > r.type())
			return true;
		else {

			if(channel() < r.channel())
				return false;
			else if(channel() > r.channel())
				return true;
			else {

				return id() >= r.id();
				}
			}
		}
	}

