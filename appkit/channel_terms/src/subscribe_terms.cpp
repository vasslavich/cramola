#include "../../internal.h"
#include "../../notifications/exceptions/excdrv.h"
#include "../subscribe_terms.h"
#include "../../internal.h"


using namespace crm;
using namespace crm::detail;


/*==============================================================================
_address_hndl_t
================================================================================*/


static_assert(noexcept(std::declval<_address_hndl_t>().object_hash()), __FILE_LINE__);
static_assert(std::is_same_v<std::decay_t<decltype(std::declval<_address_hndl_t>().object_hash())>, address_hash_t>, __FILE_LINE__);
static_assert(has_hashable_support_v< _address_hndl_t>, __FILE_LINE__);


_address_hndl_t _address_hndl_t::make(const subscriber_node_id_t& id,
	iol_types_ibase_t type,
	rtl_level_t level,
	async_space_t scx)noexcept {

	return _address_hndl_t(id, "", type, level, scx);
	}

_address_hndl_t _address_hndl_t::make( const subscriber_node_id_t& id,
	rtl_level_t level,
	async_space_t scx,
	bool invokeAbandoned,
	uint8_t tagz_ )noexcept{
	
	return _address_hndl_t( id, "", 0, level, scx, invokeAbandoned, tagz_ );
	}

_address_hndl_t _address_hndl_t::make(const subscriber_node_id_t& id,
	std::string_view key,
	rtl_level_t level,
	async_space_t scx,
	bool invokeAbandoned,
	uint8_t tagz)noexcept {

	return _address_hndl_t(id, key, 0, level, scx, invokeAbandoned, tagz);
	}

_address_hndl_t _address_hndl_t::make(std::string_view key,
	rtl_level_t level,
	async_space_t scx,
	bool invokeAbandoned,
	uint8_t tagz)noexcept {

	return _address_hndl_t(key, 0, level, scx, invokeAbandoned, tagz);

	}
_address_hndl_t _address_hndl_t::make(const subscriber_node_id_t& id,
	iol_types_t type,
	rtl_level_t level,
	async_space_t scx)noexcept {

#ifdef USE_DCF_CHECK
	if(iol_types_t::st_RZi_LX_error == type) {
		CBL_VERIFY(level == rtl_level_t::l1 || level == rtl_level_t::l0);
		}
#endif

	return _address_hndl_t(id, "", type, level, scx);
	}

_address_hndl_t::_address_hndl_t()noexcept {}

_address_hndl_t::_address_hndl_t(std::string_view key,
	iol_types_ibase_t type,
	rtl_level_t level,
	async_space_t scx,
	bool invokeAbandoned,
	uint8_t tagz_)noexcept
	: _key(key)
	, _type(type)
	, _level(level)
	, _tagz(tagz_)
	, _scx(scx)
	, _invokeAbandoned(invokeAbandoned) {
	update_hash();
	}

_address_hndl_t::_address_hndl_t( const subscriber_node_id_t & id,
	std::string_view key,
	iol_types_ibase_t type,
	rtl_level_t level,
	async_space_t scx,
	bool invokeAbandoned,
	uint8_t tagz_ )noexcept
	: _id( id )
	, _key(key)
	, _type( type )
	, _level( level )
	, _tagz( tagz_ )
	, _scx(scx)
	, _invokeAbandoned( invokeAbandoned ){
	update_hash();
	}

_address_hndl_t::_address_hndl_t( const subscriber_node_id_t & id,
	std::string_view key,
	iol_types_t type,
	rtl_level_t level,
	async_space_t scx,
	bool invokeAbandoned,
	uint8_t tagz_ )noexcept
	: _id( id )
	, _key(key)
	, _type( (iol_types_ibase_t)type )
	, _level( level )
	, _tagz( tagz_ )
	, _scx(scx)
	, _invokeAbandoned( invokeAbandoned ){

	if( type == iol_types_t::st_user_boundary )
		FATAL_ERROR_FWD(nullptr);

	update_hash();
	}

const std::string& _address_hndl_t::key()const noexcept {
	return _key;
	}

async_space_t _address_hndl_t::space()const noexcept {
	return _scx;
	}

const subscriber_node_id_t& _address_hndl_t::id()const noexcept {
	return _id;
	}

iol_types_ibase_t _address_hndl_t::type()const noexcept {
	return _type;
	}

rtl_level_t _address_hndl_t::level()const noexcept {
	return _level;
	}

bool _address_hndl_t::invoke_abandoned()const noexcept{
	return _invokeAbandoned;
	}

uint8_t _address_hndl_t::tagz()const noexcept{
	return _tagz;
	}

bool crm::detail::operator < (const _address_hndl_t & lval, const _address_hndl_t &rval) noexcept {
	if (lval.id() < rval.id())
		return true;
	else if (lval.id() > rval.id())
		return false;
	else if (lval.key() < rval.key())
		return true;
	else if (lval.key() > rval.key())
		return false;
	else
		return lval.level() < rval.level();
	}

bool crm::detail::operator >= (const _address_hndl_t & lval, const _address_hndl_t &rval)noexcept {
	if (lval.id() < rval.id())
		return false;
	else if (lval.id() > rval.id())
		return true;
	else if (lval.key() < rval.key())
		return false;
	else if (lval.key() > rval.key())
		return true;
	else
		return lval.level() >= rval.level();
	}

bool crm::detail::operator == (const _address_hndl_t & lval, const _address_hndl_t &rval) noexcept {
	return lval.id() == rval.id() && lval.key() == rval.key() && lval.level() == rval.level();
	}

bool crm::detail::operator != (const _address_hndl_t & lval, const _address_hndl_t &rval)noexcept {
	return !(lval == rval);
	}

void _address_hndl_t::update_hash()noexcept {
	_hash = address_hash_t{};
	apply_at_hash(_hash, _id);
	apply_at_hash(_hash, _key);
	apply_at_hash(_hash, _level);
	}

const address_hash_t& _address_hndl_t::object_hash()const noexcept {
	return _hash;
	}


subscribe_message_handler::subscribe_message_handler()noexcept{}

void subscribe_message_handler::operator()(std::unique_ptr<dcf_exception_t> && e, invoke_context_trait ic )noexcept{
	if( _pf )
		_pf( std::move( e ), nullptr, ic );
	}

void subscribe_message_handler::operator()(std::unique_ptr<i_iol_ihandler_t> && v, invoke_context_trait ic ){
	if( _pf )
		_pf( nullptr, std::move( v ), ic );
	}

subscribe_message_handler::operator bool()const noexcept{
	return _pf;
	}


static_assert(!std::is_copy_constructible_v< subscribe_message_handler>, __FILE__);
static_assert(!std::is_copy_assignable_v<subscribe_message_handler>, __FILE__);
static_assert(std::is_move_constructible_v< subscribe_message_handler>, __FILE__);
static_assert(std::is_move_assignable_v<subscribe_message_handler>, __FILE__);



