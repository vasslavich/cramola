#include "../../internal.h"
#include "../../context/context.h"
#include "../message_frame.h"


using namespace crm;
using namespace crm::detail;


static_assert(srlz::is_serializable_v<rt0_x_message_t>, __FILE_LINE__);
static_assert(srlz::is_serializable_v< outbound_message_desc_t>, __FILE_LINE__);
static_assert(srlz::is_serializable_v< rt1_x_message_data_t>, __FILE_LINE__);


static_assert(srlz::is_serializable_v< rt1_streams_table_wait_key_t>, __FILE_LINE__);
static_assert(srlz::is_serializable_v< subscriber_node_id_t>, __FILE_LINE__);
static_assert(srlz::is_serializable_v< sx_locid_t>, __FILE_LINE__);
static_assert(srlz::is_serializable_v< iol_type_spcf_t>, __FILE_LINE__);
static_assert(srlz::is_serializable_v< channel_identity_t>, __FILE_LINE__);
static_assert(srlz::is_serializable_v< rtl_level_t>, __FILE_LINE__);

static_assert(!srlz::detail::is_strob_flat_serializable_v< rt1_x_message_data_t>, __FILE_LINE__);

static_assert(has_null_value_type_specialization_v<outbound_message_desc_t>, __FILE_LINE__);

const detail::outbound_message_desc_t null_value_t<detail::outbound_message_desc_t>::null;

template<typename T>
int dmp() {
	auto ws = srlz::wstream_constructor_t::make();
	T d;
	srlz::serialize(ws, d);
	auto b = ws.release();
	auto rs = srlz::rstream_constructor_t::make(b.cbegin(), b.cend());
	srlz::deserialize(rs, d);
	}

static_assert(std::is_same_v<std::invoke_result_t<decltype(dmp<outbound_message_desc_t>)>, int>, __FILE_LINE__);
static_assert(std::is_same_v<std::invoke_result_t<decltype(dmp<rt0_x_message_t>)>, int>, __FILE_LINE__);

//static_assert(srlz::detail::is_strob_serializable_v<rt0_x_message_t>, __FILE_LINE__);
//static_assert(srlz::detail::is_strob_flat_serializable_v<rt0_x_message_t>, __FILE_LINE__);
//static_assert(srlz::detail::fields_count_v<rt0_x_message_t> == 3, __FILE_LINE__);






rt0_x_message_t::~rt0_x_message_t() {
	CBL_MPF_KEYTRACE_SET((*this), 245);
	}

rt0_x_message_t::rt0_x_message_t() noexcept{}

rt0_x_message_t::rt0_x_message_t( rtl_roadmap_line_t && rdm_,
	_packet_t && pck_,
	connection_key_t && basePeerID_ )noexcept
	: _rdm( std::move( rdm_ ) )
	, _pck( std::move( pck_ ) )
	, _basePeerID( std::move(basePeerID_)){

	CBL_VERIFY_SNDRCV_ATTRIBUTES(*this);
	CBL_VERIFY( !is_null( _rdm.points.target_id() ) && _rdm.points.table() != rtl_table_t::undefined );
	}

rt0_x_message_t rt0_x_message_t::make(rtl_roadmap_line_t && rdm,
	_packet_t && pck,
	connection_key_t && basePeerID)noexcept {

	return rt0_x_message_t(std::move(rdm), std::move(pck), std::move(basePeerID));
	}

rt0_x_message_t rt0_x_message_t::copy()const {
	rt0_x_message_t m;

	m._rdm = _rdm;
	m._pck = _pck.copy();
	m._basePeerID = _basePeerID;

	return m;
	}

const _packet_t& rt0_x_message_t::package()const noexcept {
	return _pck;
	}

_packet_t& rt0_x_message_t::package()noexcept {
	return _pck;
	}

_packet_t rt0_x_message_t::capture_package() noexcept {
	return std::move( _pck );
	}

const rtl_roadmap_line_t& rt0_x_message_t::rdm()const noexcept {
	return _rdm;
	}

const connection_key_t& rt0_x_message_t::base_peer_id()const noexcept {
	return _basePeerID;
	}

size_t rt0_x_message_t::get_serialized_size()const noexcept {
	return srlz::object_trait_serialized_size(_rdm)
		+ srlz::object_trait_serialized_size(_pck)
		+ srlz::object_trait_serialized_size(_basePeerID);
	}

bool rt0_x_message_t::is_sendrecv_cycle()const noexcept {
	return is_sendrecv_request() || is_sendrecv_response();
	}

bool rt0_x_message_t::is_sendrecv_request()const noexcept {
	return package().address().is_sendrecv_request();
	}

bool rt0_x_message_t::is_sendrecv_response()const noexcept {
	return package().address().is_sendrecv_response();
	}

bool crm::detail::is_sendrecv_response(const rt0_x_message_t& h)noexcept {
	return h.is_sendrecv_response();
	}

bool crm::detail::is_sendrecv_request(const rt0_x_message_t& h)noexcept {
	return h.is_sendrecv_request();
	}

bool crm::detail::is_sendrecv_cycle(const rt0_x_message_t& h)noexcept {
	return h.is_sendrecv_cycle();
	}

bool crm::detail::is_sndrcv_spin_tagged_invoke(const rt0_x_message_t& h)noexcept {
	return is_sendrecv_cycle(h) && check_sndrcv_key_spin_tag(h.package().address());
	}

bool crm::detail::check_sndrcv_key_spin_tag(const rt0_x_message_t& h)noexcept {
	return is_sndrcv_spin_tagged_invoke(h) || !is_sendrecv_cycle(h);
	}

bool crm::detail::is_report(const rt0_x_message_t& h)noexcept {
	return h.package().type().fl_report();
	}

rtl_table_t crm::detail::passing_direction(const rt0_x_message_t& )noexcept {
	return rtl_table_t::in;
	}







rt0_x_message_unit_t::rt0_x_message_unit_t()noexcept {}

rt0_x_message_unit_t::~rt0_x_message_unit_t() {
	CBL_MPF_KEYTRACE_SET(_unit, 247);
	}

rt0_x_message_unit_t::rt0_x_message_unit_t(rt0_x_message_unit_t&& o)noexcept
	: _unit(std::move(o._unit))
	, _reverseExc(std::move(o._reverseExc))
#ifdef CBL_OBJECTS_COUNTER_CHECKER
	, _xDbgCounter(std::move(o._xDbgCounter))
#endif
	{}

rt0_x_message_unit_t& rt0_x_message_unit_t::operator=(rt0_x_message_unit_t&& o)noexcept {
	if (this != std::addressof(o)) {
		_unit = std::move(o._unit);
		_reverseExc = std::move(o._reverseExc);

#ifdef CBL_OBJECTS_COUNTER_CHECKER
		_xDbgCounter = std::move(o._xDbgCounter);
#endif
		}

	return (*this);
	}

rt0_x_message_unit_t::rt0_x_message_unit_t(rt0_x_message_t&& unit_)noexcept
	: _unit(std::move(unit_)) {}

const rt0_x_message_t& rt0_x_message_unit_t::unit()const noexcept {
	return _unit;
	}

bool rt0_x_message_unit_t::is_sendrecv_cycle()const noexcept {
	return _unit.is_sendrecv_cycle();
	}

bool rt0_x_message_unit_t::is_sendrecv_request()const noexcept {
	return _unit.is_sendrecv_request();
	}

bool rt0_x_message_unit_t::is_sendrecv_response()const noexcept {
	return _unit.is_sendrecv_response();
	}

rt0_x_message_t rt0_x_message_unit_t::capture_unit()noexcept {
	return std::move(_unit);
	}

std::unique_ptr<inbound_message_with_tail_exception_guard> rt0_x_message_unit_t::release_invoke_exception_guard_ptr() && noexcept {
	return std::move(_reverseExc);
	}

inbound_message_with_tail_exception_guard rt0_x_message_unit_t::reset_invoke_exception_guard()noexcept {
	return _reverseExc ? std::move(*_reverseExc) : inbound_message_with_tail_exception_guard();
	}

bool rt0_x_message_unit_t::is_reverse_exception_guard()const noexcept {
	return _reverseExc && (*_reverseExc);
	}

inbound_message_with_tail_exception_guard rt0_x_message_unit_t::reset_invoke_exception_guard(
	inbound_message_with_tail_exception_guard&& g)noexcept {

	auto pTmp = std::make_unique<inbound_message_with_tail_exception_guard>(std::move(g));
	std::swap(pTmp, _reverseExc);
	return pTmp ? std::move((*pTmp)) : inbound_message_with_tail_exception_guard();
	}

void rt0_x_message_unit_t::invoke_exception(std::unique_ptr<dcf_exception_t>&& exc)noexcept {
	if (_reverseExc && (*_reverseExc)) {
		(*_reverseExc)(exc ? std::move(exc) : CREATE_MPF_PTR_EXC_FWD(L"abandoned with no specified exception"));
		}
	}

#ifdef CBL_MPF_ENABLE_TIMETRACE
void rt0_x_message_unit_t::timeline_set_rt1_receiver_router()noexcept {
	_unit.package().timeline_set_rt1_receiver_router();
	}
#endif

bool crm::detail::is_sendrecv_cycle(const rt0_x_message_unit_t& u)noexcept {
	return u.is_sendrecv_cycle();
	}

bool crm::detail::is_sndrcv_spin_tagged_invoke(const rt0_x_message_unit_t& h)noexcept {
	return is_sendrecv_cycle(h) && check_sndrcv_key_spin_tag(h.unit());
	}

bool crm::detail::check_sndrcv_key_spin_tag(const rt0_x_message_unit_t& h)noexcept {
	return is_sndrcv_spin_tagged_invoke(h) || !is_sendrecv_cycle(h);
	}









const outbound_message_desc_t outbound_message_desc_t::null;
static_assert(is_has_null_static_value_v<outbound_message_desc_t>&& is_has_null_property_v<outbound_message_desc_t>, __FILE_LINE__);


outbound_message_desc_t::outbound_message_desc_t()noexcept {}

outbound_message_desc_t::outbound_message_desc_t(const i_iol_handler_t& mh)noexcept
	: requestEmitter(mh.source_subscriber_id())
	, responseTarget(mh.destination_subscriber_id())
	, spitTag(mh.spin_tag())
	, type(mh.type())
	, level(mh.address().level())
	, isNull{ false }{

	CBL_VERIFY_SNDRCV_ATTRIBUTES(*this);
	CBL_VERIFY_SNDRCV_KEY_SPIN_TAG(*this);
	CBL_VERIFY(level == rtl_level_t::l0 || level == rtl_level_t::l1);
	}

bool outbound_message_desc_t::is_null()const noexcept {
	return isNull;
	}

void outbound_message_desc_t::set_channel_id(const channel_identity_t& chid_)noexcept {
	chid = chid_;
	}

void outbound_message_desc_t::set_source_rt1_stream_key(rt1_streams_table_wait_key_t&& rt1sk)noexcept {
	streamId = std::move(rt1sk);
	}

size_t outbound_message_desc_t::get_serialized_size()const noexcept {
	return
		srlz::object_trait_serialized_size(streamId) +
		srlz::object_trait_serialized_size(requestEmitter) +
		srlz::object_trait_serialized_size(responseTarget) +
		srlz::object_trait_serialized_size(spitTag) +
		srlz::object_trait_serialized_size(type) +
		srlz::object_trait_serialized_size(chid) +
		srlz::object_trait_serialized_size(level) +
		srlz::object_trait_serialized_size(isNull);
	}

bool outbound_message_desc_t::is_sendrecv_cycle()const noexcept {
	return is_sendrecv_request() || is_sendrecv_response();
	}

bool outbound_message_desc_t::is_sendrecv_request()const noexcept {
	return !crm::is_null(requestEmitter);
	}

bool outbound_message_desc_t::is_sendrecv_response()const noexcept {
	return !crm::is_null(responseTarget);
	}

bool crm::detail::is_sendrecv_response(const outbound_message_desc_t& h)noexcept {
	return h.is_sendrecv_response();
	}

bool crm::detail::is_sendrecv_request(const outbound_message_desc_t& h)noexcept {
	return h.is_sendrecv_request();
	}

bool crm::detail::is_report(const outbound_message_desc_t& h)noexcept {
	return h.type.fl_report();
	}

rtl_table_t crm::detail::passing_direction(const outbound_message_desc_t& )noexcept {
	return rtl_table_t::out;
	}

bool crm::detail::is_sendrecv_cycle(const outbound_message_desc_t& h)noexcept {
	return h.is_sendrecv_cycle();
	}

bool crm::detail::is_sndrcv_spin_tagged_invoke(const outbound_message_desc_t& h)noexcept {
	return is_sendrecv_cycle(h) && check_sndrcv_key_spin_tag(h.spitTag);
	}

bool crm::detail::check_sndrcv_key_spin_tag(const outbound_message_desc_t& h)noexcept {
	return is_sndrcv_spin_tagged_invoke(h) || !is_sendrecv_cycle(h);
	}













std::unique_ptr<outpass_message_reverse_t> mk_message_reverse_guarder(outbound_message_desc_t&& desc, inbound_message_with_tail_exception_guard&& g)noexcept {
	CHECK_NO_EXCEPTION_BEGIN
		if (g) {
			return std::make_unique<outpass_message_reverse_t>(outpass_message_reverse_t{ std::move(desc), std::move(g) });
			}
		else {
			return {};
			}
	CHECK_NO_EXCEPTION_END
	}

outbound_message_unit_t::~outbound_message_unit_t() {
	CBL_MPF_KEYTRACE_SET((*this), 248);
	}

outbound_message_unit_t::outbound_message_unit_t()noexcept {}

outbound_message_unit_t::outbound_message_unit_t(
#ifdef CBL_OBJECTS_COUNTER_CHECKER
	trace_tag&& tag_,
#else
	trace_tag&&,
#endif

	binary_vector_t&& bin,
	outbound_message_desc_t&& desc,
	inbound_message_with_tail_exception_guard&& excgrd)noexcept
	: _bin(std::move(bin))
	, _desc({ desc })
	, _reverseExc(mk_message_reverse_guarder(std::move(desc), std::move(excgrd)))
#ifdef CBL_OBJECTS_COUNTER_CHECKER
	, _xDbgCounter(tag_.name)
#endif
	{
	CBL_VERIFY_SNDRCV_ATTRIBUTES(*this);
	}

const binary_vector_t& outbound_message_unit_t::bin()const noexcept {
	return _bin;
	}

binary_vector_t outbound_message_unit_t::capture_bin()noexcept {
	return std::move(_bin);
	}

void outbound_message_unit_t::reset_invoke_exception_guard_reset()noexcept {
	if (_reverseExc) {
		_reverseExc->guard.reset();
		}
	}

/*inbound_message_with_tail_exception_guard outbound_message_unit_t::reset_invoke_exception_guard(
	outbound_message_desc_t && d,
	inbound_message_with_tail_exception_guard && g)noexcept {

	_desc = outbound_message_desc_t{ d };

	auto pTmp = std::make_unique<outpass_message_reverse_t>(outpass_message_reverse_t{ std::move(d), std::move(g) });
	std::swap(pTmp, _reverseExc);
	return pTmp ? std::move((*pTmp).guard) : inbound_message_with_tail_exception_guard{};
	}*/

inbound_message_with_tail_exception_guard outbound_message_unit_t::reset_invoke_exception_guard()noexcept {
	if (_reverseExc) {
		std::unique_ptr<outpass_message_reverse_t> pTmp;
		std::swap(pTmp, _reverseExc);
		return pTmp ? std::move((*pTmp).guard) : inbound_message_with_tail_exception_guard{};
		}
	else {
		return inbound_message_with_tail_exception_guard{};
		}
	}


void outbound_message_unit_t::invoke_exception(std::unique_ptr<dcf_exception_t>&& exc)noexcept {
	decltype(_reverseExc) f;
	std::swap(_reverseExc, f);

	if (f) {
		f->guard.invoke_async(std::move(exc));
		}
	}

#ifdef CBL_MPF_ENABLE_TIMETRACE
const hanlder_times_stack_t& outbound_message_unit_t::timeline()const noexcept {
	return _unit.timeline();
	}

hanlder_times_stack_t outbound_message_unit_t::timeline_release() noexcept {
	return _unit.timeline_release();
	}

void outbound_message_unit_t::timeline_set_rt1_outbound_packet() noexcept {
	_unit.timeline_set_rt1_outbound_packet();
	}
#endif


outbound_message_unit_t outbound_message_unit_t::make(trace_tag&& tag_, binary_vector_t&& bin)noexcept {
	return outbound_message_unit_t(std::move(tag_), std::move(bin));
	}

outbound_message_unit_t outbound_message_unit_t::make(trace_tag&& tag_,
	binary_vector_t&& bin,
	outbound_message_desc_t&& desc,
	inbound_message_with_tail_exception_guard&& excgrd)noexcept {

	return outbound_message_unit_t(std::move(tag_), std::move(bin), std::move(desc), std::move(excgrd));
	}

void outbound_message_unit_t::set_source_rt1_stream_key(rt1_streams_table_wait_key_t&& sk)noexcept {
	_desc.set_source_rt1_stream_key(rt1_streams_table_wait_key_t{ sk });

	if (_reverseExc) {
		_reverseExc->desc.set_source_rt1_stream_key(std::move(sk));
		}
	}

const outbound_message_desc_t& outbound_message_unit_t::desc()const& noexcept {
	return _desc;
	}

outbound_message_desc_t outbound_message_unit_t::desc() && noexcept {
	return std::move(_desc);
	}

bool outbound_message_unit_t::is_exception_tail()const noexcept {
	return nullptr != _reverseExc;
	}

outbound_message_unit_t::outbound_message_unit_t(outbound_message_unit_t&& o)noexcept
	: _bin(std::move(o._bin))
	, _desc(std::move(o._desc))
	, _reverseExc(std::move(o._reverseExc))
#ifdef CBL_OBJECTS_COUNTER_CHECKER
	, _xDbgCounter(std::move(o._xDbgCounter))
#endif
	{}

outbound_message_unit_t::outbound_message_unit_t(
#ifdef CBL_OBJECTS_COUNTER_CHECKER
	trace_tag&& tag_,
#else 
	trace_tag&&,
#endif
	binary_vector_t&& bin)noexcept
	: _bin(std::move(bin))
#ifdef CBL_OBJECTS_COUNTER_CHECKER
	, _xDbgCounter(tag_.name)
#endif
	{}

outbound_message_unit_t& outbound_message_unit_t::operator=(outbound_message_unit_t&& o)noexcept {
	if (this != std::addressof(o)) {
		_bin = std::move(o._bin);
		_desc = std::move(o._desc);
		_reverseExc = std::move(o._reverseExc);

#ifdef CBL_OBJECTS_COUNTER_CHECKER
		_xDbgCounter = std::move(o._xDbgCounter);
#endif
		}

	return (*this);
	}

bool outbound_message_unit_t::is_sendrecv_cycle()const noexcept {
	return _desc.is_sendrecv_cycle();
	}

bool outbound_message_unit_t::is_sendrecv_request()const noexcept {
	return _desc.is_sendrecv_request();
	}

bool outbound_message_unit_t::is_sendrecv_response()const noexcept {
	return _desc.is_sendrecv_response();
	}

bool crm::detail::is_sendrecv_response(const outbound_message_unit_t& h)noexcept {
	return h.is_sendrecv_response();
	}

bool crm::detail::is_sendrecv_request(const outbound_message_unit_t& h)noexcept {
	return h.is_sendrecv_request();
	}

bool crm::detail::is_sendrecv_cycle(const outbound_message_unit_t& h)noexcept {
	return h.is_sendrecv_cycle();
	}

bool crm::detail::is_sndrcv_spin_tagged_invoke(const outbound_message_unit_t& h)noexcept {
	return is_sendrecv_cycle(h) && check_sndrcv_key_spin_tag(h.desc());
	}

bool crm::detail::check_sndrcv_key_spin_tag(const outbound_message_unit_t& h)noexcept {
	return is_sndrcv_spin_tagged_invoke(h) || !is_sendrecv_cycle(h);
	}

bool crm::detail::is_report(const outbound_message_unit_t& h)noexcept {
	return is_report(h.desc());
	}

rtl_table_t crm::detail::passing_direction(const outbound_message_unit_t& h)noexcept {
	return passing_direction(h.desc());
	}





size_t rt1_x_message_data_t::get_serialized_size()const noexcept {
	return srlz::object_trait_serialized_size(_rdm) +
		srlz::object_trait_serialized_size(_bin) +
		srlz::object_trait_serialized_size(_sndrcvHndl);
	}

rt1_x_message_data_t::rt1_x_message_data_t()noexcept {}

rt1_x_message_data_t::rt1_x_message_data_t(rtl_roadmap_line_t && rdm,
	binary_vector_t && bin,
	const channel_identity_t & chid_,
	outbound_message_desc_t && srh)noexcept
	: _rdm(std::move(rdm))
	, _bin(std::move(bin))
	, _sndrcvHndl{ std::move(srh) }{

	_sndrcvHndl.set_channel_id(chid_);

	CBL_VERIFY(_sndrcvHndl.level == rtl_level_t::l1 || _sndrcvHndl.level == rtl_level_t::l0);
	CBL_VERIFY(!_rdm.points.rt0().connection_key().address.empty());
	CBL_VERIFY(!_bin.empty());
	}

rt1_x_message_data_t::rt1_x_message_data_t(rtl_roadmap_line_t && rdm,
	binary_vector_t && bin)noexcept
	: _rdm(std::move(rdm))
	, _bin(std::move(bin)){

	CBL_VERIFY(!_rdm.points.rt0().connection_key().address.empty());
	CBL_VERIFY(!_bin.empty());
	}

/*rt1_x_message_data_t::rt1_x_message_data_t(rt1_x_message_data_t && o)noexcept
	: _rdm(std::move(o._rdm))
	, _bin(std::move(o._bin))
	, _sndrcvHndl(std::move(o._sndrcvHndl)) {}

rt1_x_message_data_t& rt1_x_message_data_t::operator=(rt1_x_message_data_t && o)noexcept {

	if(this != &o) {
		_rdm = std::move(o._rdm);
		_bin = std::move(o._bin);
		_sndrcvHndl = std::move(o._sndrcvHndl);
		}

	return (*this);
	}*/

rt1_x_message_data_t rt1_x_message_data_t::copy()const noexcept {
	rt1_x_message_data_t v;

	v._rdm = _rdm;
	v._bin = _bin;
	v._sndrcvHndl = _sndrcvHndl;

	return v;
	}

const binary_vector_t& rt1_x_message_data_t::package()const noexcept {
	return _bin;
	}

const rtl_roadmap_line_t& rt1_x_message_data_t::rdm()const & noexcept {
	return _rdm;
	}

rtl_roadmap_line_t rt1_x_message_data_t::rdm()&& noexcept {
	return std::move(_rdm);
	}

binary_vector_t rt1_x_message_data_t::capture_package() noexcept {
	return std::move(_bin);
	}

const outbound_message_desc_t& rt1_x_message_data_t::sndrcv_handler()const & noexcept {
	return _sndrcvHndl;
	}

outbound_message_desc_t rt1_x_message_data_t::sndrcv_handler() && noexcept {
	return std::move(_sndrcvHndl);
	}

bool rt1_x_message_data_t::is_sendrecv_cycle()const noexcept {
	return sndrcv_handler().is_sendrecv_cycle();
	}

bool rt1_x_message_data_t::is_sendrecv_request()const noexcept {
	return sndrcv_handler().is_sendrecv_request();
	}

bool rt1_x_message_data_t::is_sendrecv_response()const noexcept {
	return sndrcv_handler().is_sendrecv_response();
	}


bool crm::detail::is_sendrecv_cycle(const rt1_x_message_data_t& h)noexcept {
	return h.is_sendrecv_cycle();
	}

bool crm::detail::is_sndrcv_spin_tagged_invoke(const rt1_x_message_data_t& h)noexcept {
	return is_sendrecv_cycle(h) && check_sndrcv_key_spin_tag(h.sndrcv_handler());
	}

bool crm::detail::check_sndrcv_key_spin_tag(const rt1_x_message_data_t& h)noexcept {
	return is_sndrcv_spin_tagged_invoke(h) || !is_sendrecv_cycle(h);
	}





rt1_x_message_t::rt1_x_message_t()noexcept {}

rt1_x_message_t::rt1_x_message_t(rt1_x_message_t && o)noexcept
	: _rdm(std::move(o._rdm))
	, _pck(std::move(o._pck)) {}

rt1_x_message_t& rt1_x_message_t::operator=(rt1_x_message_t && o) noexcept{
	if(this != &o) {
		_rdm = std::move(o._rdm);
		_pck = std::move(o._pck);
		}

	return (*this);
	}

rt1_x_message_t::rt1_x_message_t(rtl_roadmap_line_t && rdm,
	outbound_message_unit_t && bin)noexcept
	: _rdm(std::move(rdm))
	, _pck(std::move(bin)) {

	CBL_VERIFY(!is_null(_rdm.points.rt1_stream_table_key()));
	_pck.set_source_rt1_stream_key(_rdm.points.rt1_stream_table_key());

	CBL_VERIFY(!_rdm.points.rt0().connection_key().address.empty());
	CBL_VERIFY(!_pck.bin().empty());
	CBL_VERIFY_SNDRCV_ATTRIBUTES(*this);
	}

rt1_x_message_t rt1_x_message_t::make(rtl_roadmap_line_t && rdm,
	outbound_message_unit_t && bin)noexcept {

	return rt1_x_message_t(std::move(rdm), std::move(bin));
	}

const outbound_message_unit_t& rt1_x_message_t::package()const & noexcept{
	return _pck;
	}

const rtl_roadmap_line_t& rt1_x_message_t::rdm()const & noexcept{
	return _rdm;
	}

rtl_roadmap_line_t rt1_x_message_t::rdm()&& noexcept {
	return std::move(_rdm);
	}

outbound_message_unit_t rt1_x_message_t::package() && noexcept{
	return std::move( _pck );
	}

bool rt1_x_message_t::is_sendrecv_cycle()const noexcept {
	return package().is_sendrecv_cycle();
	}

bool rt1_x_message_t::is_sendrecv_request()const noexcept {
	return package().is_sendrecv_request();
	}

bool rt1_x_message_t::is_sendrecv_response()const noexcept {
	return package().is_sendrecv_response();
	}



bool crm::detail::is_report(const rt1_x_message_t& h)noexcept {
	return ::is_report(h.package());
	}

rtl_table_t crm::detail::passing_direction(const rt1_x_message_t& h)noexcept {
	return ::passing_direction(h.package());
	}

bool crm::detail::is_sendrecv_request(const rt1_x_message_t& h)noexcept {
	return h.is_sendrecv_request();
	}

bool crm::detail::is_sendrecv_response(const rt1_x_message_t& h)noexcept {
	return h.is_sendrecv_response();
	}

bool crm::detail::is_sendrecv_cycle(const rt1_x_message_t& h)noexcept {
	return h.is_sendrecv_cycle();
	}

bool crm::detail::is_sndrcv_spin_tagged_invoke(const rt1_x_message_t& h)noexcept {
	return is_sendrecv_cycle(h) && check_sndrcv_key_spin_tag(h.package());
	}

bool crm::detail::check_sndrcv_key_spin_tag(const rt1_x_message_t& h)noexcept {
	return is_sndrcv_spin_tagged_invoke(h) || !is_sendrecv_cycle(h);
	}
















outpass_message_reverse_t::outpass_message_reverse_t(outbound_message_desc_t&& desc_,
	inbound_message_with_tail_exception_guard&& guard_)noexcept
	: desc(std::move(desc_))
	, guard(std::move(guard_)) {}






message_frame_unit_t::~message_frame_unit_t() {
	CBL_MPF_KEYTRACE_SET(_unit, 249);
	}

message_frame_unit_t::message_frame_unit_t() noexcept{}

message_frame_unit_t::message_frame_unit_t(message_frame_unit_t && o)noexcept 
	: _unit(std::move(o._unit))
	, _reverseExc(std::move(o._reverseExc))
#ifdef CBL_OBJECTS_COUNTER_CHECKER
	, _xDbgCounter(std::move(o._xDbgCounter))
#endif
	{}

message_frame_unit_t& message_frame_unit_t::operator=(message_frame_unit_t && o)noexcept {
	if(this != std::addressof(o)) {
		_unit = std::move(o._unit);
		std::swap(_reverseExc, o._reverseExc);

#ifdef CBL_OBJECTS_COUNTER_CHECKER
		_xDbgCounter = std::move(o._xDbgCounter);
#endif
		}

	return (*this);
	}

message_frame_unit_t::message_frame_unit_t(_packet_t && unit_)noexcept
	: _unit(std::move(unit_)) {
	
#ifdef USE_DCF_CHECK
	if(_unit.type().type() == iol_types_t::st_RZi_LX_error) {
		CBL_VERIFY(_unit.address().level() == rtl_level_t::l1 || _unit.address().level() == rtl_level_t::l0);
		}
#endif

	_unit.mark_as_used();
	unit_.mark_as_used();
	}

message_frame_unit_t::message_frame_unit_t(rt0_x_message_unit_t && unit_)noexcept
	: _unit(unit_.capture_unit().capture_package())
	, _reverseExc(std::move(unit_).release_invoke_exception_guard_ptr()) {
	
#ifdef USE_DCF_CHECK
	if(_unit.type().type() == iol_types_t::st_RZi_LX_error) {
		CBL_VERIFY(_unit.address().level() == rtl_level_t::l1 || _unit.address().level() == rtl_level_t::l0);
		}
#endif
	}

const _packet_t& message_frame_unit_t::unit()const noexcept {
	return _unit;
	}

_packet_t message_frame_unit_t::capture_unit()noexcept {
	return std::move(_unit);
	}

inbound_message_with_tail_exception_guard message_frame_unit_t::reset_invoke_exception_guard()noexcept {
	return _reverseExc ? std::move(*_reverseExc) : inbound_message_with_tail_exception_guard();
	}

inbound_message_with_tail_exception_guard message_frame_unit_t::reset_invoke_exception_guard(
	inbound_message_with_tail_exception_guard && g)noexcept {

	auto pTmp = std::make_unique<inbound_message_with_tail_exception_guard>(std::move(g));
	std::swap(pTmp, _reverseExc);
	return pTmp ? std::move((*pTmp)) : inbound_message_with_tail_exception_guard();
	}

void message_frame_unit_t::invoke_exception(std::unique_ptr<dcf_exception_t> && exc)noexcept {
	decltype(_reverseExc) f;
	std::swap(_reverseExc, f);

	if(f && (*f)) {
		(*f)(std::move(exc));
		}
	}

void message_frame_unit_t::invoke_exception(const dcf_exception_t & exc)noexcept {
	invoke_exception(exc.dcpy());
	}

void message_frame_unit_t::invoke_exception(const std::exception& exc)noexcept {
	invoke_exception(CREATE_PTR_EXC_FWD(exc));
	}

#ifdef CBL_MPF_ENABLE_TIMETRACE
const hanlder_times_stack_t& message_frame_unit_t::timeline()const noexcept {
	return _unit.timeline();
	}

hanlder_times_stack_t message_frame_unit_t::timeline_release() noexcept {
	return _unit.timeline_release();
	}

void message_frame_unit_t::timeline_set_rt1_receiver_router() noexcept {
	_unit.timeline_set_rt1_receiver_router();
	}

#endif//CBL_MPF_ENABLE_TIMETRACE







