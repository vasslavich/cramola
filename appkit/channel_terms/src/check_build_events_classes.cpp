#include "../../internal.h"
#include "../i_events.h"


using namespace crm;
using namespace crm::detail;


static_assert(is_event_cast_v<i_event_t, event_open_connection_t<opeer_t>>, __FILE_LINE__ );
static_assert(is_event_cast_v<i_event_t, event_abandoned_t>, __FILE_LINE__);
static_assert(is_event_cast_v<i_event_t, event_close_connection_t<opeer_t>>, __FILE_LINE__);
static_assert(is_event_cast_v<i_event_t, subscribe_event_connection_t<opeer_t>>, __FILE_LINE__);

