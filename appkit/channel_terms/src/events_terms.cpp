#include "../../internal.h"
#include "../events_terms.h"
#include "../events_tbl.h"


using namespace crm;
using namespace crm::detail;

decltype(auto) check_trait_event_hndl(){
	return[](std::unique_ptr<i_event_t>&&, subscribe_invoke)noexcept {};
	}

static_assert(is_event_callback_invokable_v<decltype(check_trait_event_hndl())>, __FILE_LINE__);

event_subscribe_assist_t::event_addressed_link_t::event_addressed_link_t( __event_handler_f && h,
	const address_desc & id_,
	bool once_,
	std::weak_ptr<distributed_ctx_t> ctx_,
	async_space_t scx )
	: hndl( std::move( h ),
		std::move( ctx_ ),
		"event_addressed_link_t",
		scx,
		once_ ? subscribe_with_tail_exception_guard::invoke_once : subscribe_with_tail_exception_guard::invoke_indefinitely )
	, subscriberId( id_ )
	, once( once_ ){}



event_subscribe_assist_t::event_handler_t::event_handler_t( event_handler_t && o )noexcept
	: lineId( std::move( o.lineId ) )
	, _wtbl( std::move( o._wtbl ) ){}

event_subscribe_assist_t::event_handler_t& event_subscribe_assist_t::event_handler_t::operator=( event_handler_t && o )noexcept{
	if( this != std::addressof( o ) ){
		lineId = std::move( o.lineId );
		_wtbl = std::move( o._wtbl );
		}

	return (*this);
	}

event_subscribe_assist_t::event_handler_t::event_handler_t( line_num_t lnId, std::weak_ptr<events_multitable_t> wtbl_ )
	: lineId( lnId )
	, _wtbl( wtbl_ ){
	
	if (auto ptbl = _wtbl.lock()) {
		ptbl->regline(*this);
		}
	else {
		THROW_EXC_FWD(nullptr);
		}
	}

event_subscribe_assist_t::event_handler_t::event_handler_t()noexcept{}

void event_subscribe_assist_t::event_handler_t::invoke( event_type_t type, std::unique_ptr<event_args_t> && args )noexcept{
	if( auto stbl = _wtbl.lock() ){
		CHECK_NO_EXCEPTION( stbl->invoke( (*this), type, std::move( args ) ) );
		}
	}

subscribe_result event_subscribe_assist_t::event_handler_t::subscribe( async_space_t scx,
	const address_desc & id,
	bool once,
	__event_handler_f && handler,
	check_condition_f && checkPred ){

	return _subscribe( scx, id, once, std::move( handler ), std::move( checkPred ) );
	}

subscribe_result event_subscribe_assist_t::event_handler_t::_subscribe( async_space_t scx,
	const address_desc & id,
	bool once,
	__event_handler_f && handler,
	check_condition_f && checkPred ){

	if( auto stbl = _wtbl.lock() ){
		return stbl->subscribe( (*this), scx, id, once, std::move( handler ), std::move( checkPred ) );
		}
	else{
		THROW_MPF_EXC_FWD(nullptr);
		}
	}

void event_subscribe_assist_t::event_handler_t::unsubscribe( const address_desc & id )noexcept{
	if( auto stbl = _wtbl.lock() ){
		CHECK_NO_EXCEPTION( stbl->unsubscribe( (*this), id ) );
		}
	}

void event_subscribe_assist_t::event_handler_t::close()noexcept{
	if( auto stbl = _wtbl.lock() ){
		CHECK_NO_EXCEPTION( stbl->close( (*this) ) );
		}
	}

event_subscribe_assist_t::event_handler_t::~event_handler_t(){
	CHECK_NO_EXCEPTION( close() );
	}

events_list event_subscribe_assist_t::event_handler_t::release()noexcept{
	if( auto stbl = _wtbl.lock() ){
		return stbl->release( (*this) );
		}
	else{
		return events_list{};
		}
	}

void event_subscribe_assist_t::event_handler_t::add( events_list && el ){
	if( auto stbl = _wtbl.lock() ){
		return stbl->add( (*this), std::move( el ) );
		}
	else {
		THROW_EXC_FWD(nullptr);
		}
	}

bool crm::detail::operator==( const event_subscribe_assist_t::event_handler_t& l, const event_subscribe_assist_t::event_handler_t& r )noexcept{
	return l.lineId == r.lineId;
	}

bool crm::detail::operator!=( const event_subscribe_assist_t::event_handler_t& l, const event_subscribe_assist_t::event_handler_t& r )noexcept{
	return !(l == r);
	}




make_fault_event_result_t::make_fault_event_result_t()noexcept{}

std::unique_ptr<event_abandoned_t> make_fault_event_result_t::operator()( std::string && )noexcept{
	return std::make_unique<event_abandoned_t>();
	}

std::unique_ptr<event_abandoned_t> make_fault_event_result_t::operator()()noexcept{
	return std::make_unique<event_abandoned_t>();
	}




__event_handler_f::__event_handler_f()noexcept{}

void __event_handler_f::operator()( std::unique_ptr<i_event_t> && ea, subscribe_invoke as ){
	if( _h )
		_h( std::move( ea ), as );
	}

void __event_handler_f::operator()( std::unique_ptr<i_event_t> && ea ){
	(*this)(std::move( ea ), subscribe_invoke::with_exception);
	}

__event_handler_f::operator bool()const noexcept{
	return _h;
	}


static_assert(!std::is_copy_constructible_v< __event_handler_f>, __FILE__);
static_assert(!std::is_copy_assignable_v<__event_handler_f>, __FILE__);
static_assert(std::is_move_constructible_v< __event_handler_f>, __FILE__);
static_assert(std::is_move_assignable_v<__event_handler_f>, __FILE__);





__event_handler_point_f::__event_handler_point_f()noexcept{}

void __event_handler_point_f::operator()( std::unique_ptr<i_event_t> && ea ){
	if( _ph ){
		_ph->invoke_async( std::move( ea ) );
		}
	}

__event_handler_point_f::operator bool()const noexcept{
	return _ph && (*_ph);
	}

//static_assert(std::is_copy_constructible_v< __event_handler_point_f>, __FILE__);
//static_assert(std::is_copy_assignable_v<__event_handler_point_f>, __FILE__);
static_assert(std::is_move_constructible_v< __event_handler_point_f>, __FILE__);
static_assert(std::is_move_assignable_v<__event_handler_point_f>, __FILE__);


