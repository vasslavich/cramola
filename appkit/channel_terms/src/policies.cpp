#include <vector>
#include <string>
#include <algorithm>
#include "../../internal.h"
#include "../policies.h"
#include "../../context/context.h"
#include "../../utilities/convert/cvt.h"


using namespace crm;
using namespace crm::detail;


static_assert(is_context_policies_v < std::decay_t< decltype(distributed_ctx_policies_t::get_default())>> , __FILE_LINE__);

distributed_ctx_policies_t _Default;

/*! ���������������� ������������������ ������������ ������ ��������� */
std::vector<uint8_t> syncSequence{ 0xFF, 0xAC, 0x71, 0xC8, 0x1A, 0x52, 0x1E, 0xA9 };
/*! ���������������� ������������������ ��� ����������� ����� �������������� ����� ������ */
std::vector<uint8_t> eofMarkerSequence{ 0x11, 0xCC, 0x22, 0xDD, 0x44, 0xEE, 0x88, 0xFF };



const distributed_ctx_policies_t& distributed_ctx_policies_t::get_default() {
	return _Default;
	}

void distributed_ctx_policies_t::default_setup() {
	_threadPoolSize = (std::max)(std::thread::hardware_concurrency()/2, 2u);
	_threadPoolExternActors = 1;
	_threadPoolStackSize = 1024;
	_ioBufferMaxlen = CRM_MB;
	_ioThreadsCount = (std::max)(std::thread::hardware_concurrency() / 2, 2u);
	_rt0_StackMaxLen = 1024;
	_poolLoadingMFactor = 5.0;
	_uploadFolder = L".\\upload\\";
	_timeout_rt0rt1_command_execute = std::chrono::seconds(600);
	_timeoutProbeAsync = std::chrono::milliseconds(1000);
	_timeoutWaitIO = std::chrono::milliseconds(1000);
	_timeoutThreadPoolWaitItem = std::chrono::milliseconds(1000);
	_timeoutInactivity = std::chrono::seconds(60 * 60);
	_timeoutReconnectAttempt = std::chrono::milliseconds(2000);
	_connectionTL = sndrcv_timeline_periodicity_t::make_once_call();

	_timoutPing = sndrcv_timeline_periodicity_t::make_cyclic_timeouted(std::chrono::seconds(30), std::chrono::seconds(5));
	_waitRequestIdentity = sndrcv_timeline_periodicity_t::make_once_call_timeouted(
		std::chrono::seconds(60 * 12));
	_sendrecvIdentity = sndrcv_timeline_periodicity_t::make_once_call_timeouted(
		std::chrono::seconds(60 * 12));
	_sendrecvOkConnection = sndrcv_timeline_periodicity_t::make_once_call_timeouted(
		std::chrono::seconds(60 * 12));
	_sendrecvInitConnection = sndrcv_timeline_periodicity_t::make_once_call_timeouted(
		std::chrono::seconds(60 * 12));
	_timeouteSndRcv = sndrcv_timeline_periodicity_t::make_once_call_timeouted(
		std::chrono::seconds(60 * 12));

	_erremPoints0 = errem_points_t{
		CBL_MPF_TEST_ERROR_EMULATOR_CONNECTION_CORRUPT_RT0_MIN_SECS,
		CBL_MPF_TEST_ERROR_EMULATOR_CONNECTION_CORRUPT_RT0_MAX_SECS,
		CBL_MPF_TEST_ERROR_EMULATOR_CONNECTION_CORRUPT_RT0_INC_SECS };

	_erremPoints1 = errem_points_t{
		CBL_MPF_TEST_ERROR_EMULATOR_CONNECTION_CORRUPT_RT1_MIN_SECS,
		CBL_MPF_TEST_ERROR_EMULATOR_CONNECTION_CORRUPT_RT1_MAX_SECS,
		CBL_MPF_TEST_ERROR_EMULATOR_CONNECTION_CORRUPT_RT1_INC_SECS };

	
#ifdef CBL_MPF_TEST_ERROR_EMULATION
#if (CBL_MPF_TEST_ERROR_EMULATOR_CONNECTION_CORRUPT_INC_MODE_0 == CBL_MPF_TEST_ERROR_EMULATOR_CONNECTION_CORRUPT_INC_MODE_INC)
	_errmodeConnection0 = errem_generator_mode::inc;
#elif (CBL_MPF_TEST_ERROR_EMULATOR_CONNECTION_CORRUPT_INC_MODE_0 == CBL_MPF_TEST_ERROR_EMULATOR_CONNECTION_CORRUPT_INC_MODE_RAND)
	_errmodeConnection0 = errem_generator_mode::rand;
#else
#error invalid increment mode
#endif

#if (CBL_MPF_TEST_ERROR_EMULATOR_CONNECTION_CORRUPT_INC_MODE_1 == CBL_MPF_TEST_ERROR_EMULATOR_CONNECTION_CORRUPT_INC_MODE_INC)
	_errmodeConnection1 = errem_generator_mode::inc;
#elif (CBL_MPF_TEST_ERROR_EMULATOR_CONNECTION_CORRUPT_INC_MODE_1 == CBL_MPF_TEST_ERROR_EMULATOR_CONNECTION_CORRUPT_INC_MODE_RAND)
	_errmodeConnection1 = errem_generator_mode::rand;
#else
#error invalid increment mode
#endif

	_errmode = errem_generator_mode::rand;
#else
	_errmodeConnection0 = errem_generator_mode::none;
	_errmodeConnection1 = errem_generator_mode::none;
	_errmode = errem_generator_mode::none;
#endif

	_pingRt = true;
	_useCrc = false;
	}

distributed_ctx_policies_t::distributed_ctx_policies_t() {
	default_setup();
	}

distributed_ctx_policies_t::distributed_ctx_policies_t(
	const std::wstring & xml, const std::wstring & closeNode) {

	default_setup();
	read_local(xml, closeNode);
	}

distributed_ctx_policies_t::distributed_ctx_policies_t(
	const crm::xml::xml_document_t & xml, const std::wstring closeNode) {

	default_setup();
	read_local(xml, closeNode);
	}

distributed_ctx_policies_t:: ~distributed_ctx_policies_t() {}



std::string distributed_ctx_policies_t::cfg_mpf_options_root_node() noexcept {
	return "mpf_section";
	}

std::string distributed_ctx_policies_t::cfg_io_stack_maxsize() noexcept {
	return "io_stack_maxsize";
	}

std::string distributed_ctx_policies_t::cfg_worknodes_processing_concurrency()  noexcept {
	return "worknodes_processing_concurrency";
	}

std::string distributed_ctx_policies_t::cfg_crc() noexcept {
	return "use_crc";
	}




std::string distributed_ctx_policies_t::cfg_log() noexcept {
	return "log";
	}

std::string distributed_ctx_policies_t::cfg_log_log_type() noexcept {
	return "log_type";
	}

std::string distributed_ctx_policies_t::cfg_log_ssd_connection_string()  noexcept {
	return "ssd_connection_string";
	}

std::string distributed_ctx_policies_t::cfg_log_log_tbl_volume() noexcept {
	return "log_tbl_volume";
	}

std::string distributed_ctx_policies_t::cfg_log_log_filepath()  noexcept {
	return "log_filepath";
	}

std::string distributed_ctx_policies_t::cfg_log_log_rotation_size() noexcept {
	return "log_rotation_size";
	}

std::string distributed_ctx_policies_t::cfg_log_log_queue_size()  noexcept {
	return "log_queue_size";
	}

std::string distributed_ctx_policies_t::cfg_netwk_settings_root()  noexcept {
	return "codeblocks_netwk_root";
	}

std::string distributed_ctx_policies_t::cfg_threadpool_maxsize()  noexcept {
	return "thread_pool_maxsize";
	}

std::string distributed_ctx_policies_t::cfg_threadpool_extern_actors_maxsize() noexcept {
	return "thread_pool_extern_actors_maxsize";
	}


std::string distributed_ctx_policies_t::cfg_threadpool_stack_size() noexcept {
	return "thread_pool_stack_size";
	}

std::string distributed_ctx_policies_t::cfg_io_buffer_maxsize()  noexcept {
	return "io_buffer_maxsize";
	}

std::string distributed_ctx_policies_t::cfg_io_threads_count()  noexcept {
	return "io_threads_count";
	}

std::string distributed_ctx_policies_t::cfg_timeout_ping()  noexcept {
	return "ping_timeout";
	}

std::string distributed_ctx_policies_t::cfg_timeout_inactivity()  noexcept {
	return "inactivity_timeout";
	}

std::string distributed_ctx_policies_t::cfg_connection_section() noexcept {
	return "connection_section";
	}

std::string distributed_ctx_policies_t::cfg_connection_timeout()  noexcept {
	return "timeout";
	}

std::string distributed_ctx_policies_t::cfg_connection_request_interval() noexcept {
	return "request_interval";
	}

std::string distributed_ctx_policies_t::cfg_connection_identity_interval()  noexcept {
	return "identity_interval";
	}

std::string distributed_ctx_policies_t::cfg_connection_initialize_interval()  noexcept {
	return "initialize_interval";
	}

std::string distributed_ctx_policies_t::cfg_indentity_timeout() noexcept {
	return "indentity_timeout";
	}

std::string distributed_ctx_policies_t::cfg_connection_remote_timeout() noexcept {
	return "connection_remote_timeout";
	}

std::string distributed_ctx_policies_t::cfg_item_timeout_attribute_timeout()  noexcept {
	return "timeout";
	}

std::string distributed_ctx_policies_t::cfg_item_timeout_attribute_interval() noexcept {
	return "interval";
	}

std::string distributed_ctx_policies_t::cfg_loader_section_name() noexcept {
	return "loader";
	}

std::string distributed_ctx_policies_t::cfg_loader_id()  noexcept {
	return "id";
	}

std::string distributed_ctx_policies_t::cfg_loader_port()  noexcept {
	return "port";
	}

std::string distributed_ctx_policies_t::cfg_server_section_name()  noexcept {
	return "server";
	}

std::string distributed_ctx_policies_t::cfg_server_id()  noexcept {
	return "id";
	}

std::string distributed_ctx_policies_t::cfg_server_address() noexcept {
	return "address";
	}

std::string distributed_ctx_policies_t::cfg_server_port() noexcept {
	return "port";
	}

std::string distributed_ctx_policies_t::cfg_ping_rt() noexcept {
	return "ping_rt";
	}

std::string distributed_ctx_policies_t::cfg_timeout_reconnect()noexcept {
	return "timeout_reconnect";
	}

std::optional<sndrcv_timeline_periodicity_t> distributed_ctx_policies_t::read_timeout(
	const crm::xml::xml_element_t & n) {

	sndrcv_timeline_periodicity_t::timeline_t t;
	sndrcv_timeline_periodicity_t::timeline_t i;

	auto attr = n.find_attribute(cfg_item_timeout_attribute_timeout());
	if(attr) {
		const auto rval = crm::detail::cvt_to_i4(attr->value());
		if(rval > 0)
			t = std::chrono::seconds(rval);
		}

	attr = n.find_attribute(cfg_item_timeout_attribute_interval());
	if(attr) {
		const auto rval = crm::detail::cvt_to_i4(attr->value());
		if(rval > 0)
			i = std::chrono::seconds(rval);
		}

	if(t != sndrcv_timeline_periodicity_t::null_timeline_value || i != sndrcv_timeline_periodicity_t::null_timeline_value) {
		return make_timeline(t, i);
		}
	else {
		return std::optional<sndrcv_timeline_periodicity_t>();
		}
	}

std::optional<sndrcv_timeline_periodicity_t> distributed_ctx_policies_t::make_timeline(const sndrcv_timeline_periodicity_t::timeline_t & t,
	const sndrcv_timeline_periodicity_t::timeline_t & i) {

	if(t != sndrcv_timeline_periodicity_t::null_timeline_value) {
		if(i != sndrcv_timeline_periodicity_t::null_timeline_value) {
			return std::make_optional(sndrcv_timeline_periodicity_t::make_cyclic_timeouted(t, i));
			}
		else {
			return std::make_optional(sndrcv_timeline_periodicity_t::make_once_call_timeouted(t));
			}
		}
	else {
		if(i != sndrcv_timeline_periodicity_t::null_timeline_value) {
			return std::make_optional(sndrcv_timeline_periodicity_t::make_cyclic(i));
			}
		else {
			return std::optional<sndrcv_timeline_periodicity_t>();
			}
		}
	}

sndrcv_timeline_t distributed_ctx_policies_t::make_timeline(const sndrcv_timeline_periodicity_t & p) {
	return sndrcv_timeline_t::make(p);
	}

void distributed_ctx_policies_t::read_local(const std::wstring & xml, const std::wstring & closeNode) {

	crm::xml::xml_document_t xmlDoc;
	xmlDoc.parse(crm::cvt(xml));
	read_local(xmlDoc, closeNode);
	}

void distributed_ctx_policies_t::set_threadpool_maxsize(size_t l) {
	_threadPoolSize = l;
	}

void distributed_ctx_policies_t::set_threadpool_user_handlers_maxsize(size_t l) {
	_threadPoolExternActors = l;
	}

void distributed_ctx_policies_t::set_io_buffer_maxsize(size_t l) {
	_ioBufferMaxlen = l;
	}

void distributed_ctx_policies_t::set_io_threads_count(size_t c) {
	_ioThreadsCount = c;
	}

void distributed_ctx_policies_t::set_ping_timeline(const sndrcv_timeline_periodicity_t& t) {
	_timoutPing = t;
	}

void distributed_ctx_policies_t::set_rt_ping(bool r) {
	_pingRt = r;
	}

void distributed_ctx_policies_t::read_local(
	const crm::xml::xml_document_t & xml, const std::wstring & closeNode) {

	auto root = xml.root_element();
	auto settingsNode = root->first_child_element(crm::cvt(closeNode));
	if(settingsNode) {

		/* ������������ ������ ���� ������� ��������� ������������
		---------------------------------------------------*/
		auto n = settingsNode->first_child_element(cfg_threadpool_maxsize());
		if(n)
			set_threadpool_maxsize(crm::detail::cvt_to_i4(n->get_text()));

		/* ������������ ������ ���� ������� ������� ������������
		---------------------------------------------------*/
		n = settingsNode->first_child_element(cfg_threadpool_extern_actors_maxsize());
		if(n)
			set_threadpool_user_handlers_maxsize(crm::detail::cvt_to_i4(n->get_text()));

		/* ������������ ������ ����� ���� �������
		---------------------------------------------------*/
		n = settingsNode->first_child_element(cfg_threadpool_stack_size());
		if(n)
			_threadPoolStackSize = crm::detail::cvt_to_i4(n->get_text());

		/* ������������ ������ ������ �����/������
		---------------------------------------------------*/
		n = settingsNode->first_child_element(cfg_io_buffer_maxsize());
		if(n)
			set_io_buffer_maxsize(crm::detail::cvt_to_i4(n->get_text()));

		/* ����� ������� �����/������
		---------------------------------------------------*/
		n = settingsNode->first_child_element(cfg_io_threads_count());
		if(n)
			set_io_threads_count(crm::detail::cvt_to_i4(n->get_text()));

		/* ������� �����
		---------------------------------------------------*/
		n = settingsNode->first_child_element(cfg_timeout_ping());
		if(n) {
			auto p = read_timeout((*n));
			if(p) {
				set_ping_timeline(std::move(p).value());
				}
			}

		/* ������� �����
		---------------------------------------------------*/
		n = settingsNode->first_child_element(cfg_timeout_inactivity());
		if(n)
			_timeoutInactivity = std::chrono::seconds(crm::detail::cvt_to_i4(n->get_text()));

		/* ��������� �������������� ����������� � ��������� ����
		---------------------------------------------------*/
		auto ctp = settingsNode->first_child_element(cfg_connection_section());
		if(ctp) {
			n = ctp->first_child_element(cfg_connection_timeout());
			if(n) {
				_connectionTL = sndrcv_timeline_periodicity_t::make_once_call_timeouted(std::chrono::seconds(crm::detail::cvt_to_i4(n->get_text())));
				}
			}

		/* ����
		---------------------------------------------------*/
		n = settingsNode->first_child_element(cfg_ping_rt());
		if(n) {
			auto sval = n->get_text();
			if(sval == "true")
				set_rt_ping( true );
			else if(sval == "false")
				set_rt_ping( false );
			else {
				THROW_MPF_EXC_FWD(nullptr);
				}
			}

		/* ������������ ����������� �����
		---------------------------------------------------*/
		n = settingsNode->first_child_element(cfg_crc());
		if(n) {
			auto sval = n->get_text();
			if(sval == "true")
				_useCrc = true;
			else if(sval == "false")
				_useCrc = false;
			else {
				THROW_MPF_EXC_FWD(nullptr);
				}
			}

		n = settingsNode->first_child_element(cfg_timeout_reconnect());
		if (n) {
			auto ms = crm::detail::cvt_to_i4(n->get_text());
			_timeoutReconnectAttempt = std::chrono::milliseconds(ms);
			}
		}
	}

void distributed_ctx_policies_t::read_settings(
	const std::wstring & xml, const std::wstring & closeNode) {
	read_local(xml, closeNode);
	}

void distributed_ctx_policies_t::read_settings(
	const crm::xml::xml_document_t & xml, const std::wstring closeNode) {
	read_local(xml, closeNode);
	}

const std::vector<uint8_t>& distributed_ctx_policies_t::sync_sequence()const  noexcept {
	return syncSequence;
	}

const std::vector<uint8_t>& distributed_ctx_policies_t::eof_sequence()const  noexcept {
	return eofMarkerSequence;
	}

double distributed_ctx_policies_t::pool_loading_mfactor()const  noexcept {
	return _poolLoadingMFactor;
	}

size_t distributed_ctx_policies_t::thread_pool_maxsize()const  noexcept {
	return _threadPoolSize;
	}

size_t distributed_ctx_policies_t::thread_pool_extern_handlers_maxsize()const  noexcept {
	return _threadPoolExternActors;
	}

size_t distributed_ctx_policies_t::thread_pool_stack_size()const  noexcept {
	return _threadPoolStackSize;
	}

size_t distributed_ctx_policies_t::io_buffer_maxsize()const  noexcept {
	return _ioBufferMaxlen;
	}

size_t distributed_ctx_policies_t::stream_block_size()const  noexcept {
	return io_buffer_maxsize();
	}

size_t distributed_ctx_policies_t::io_threads_count()const  noexcept {
	return _ioThreadsCount;
	}

const sndrcv_timeline_periodicity_t& distributed_ctx_policies_t::timeout_ping()const noexcept {
	return _timoutPing;
	}

sndrcv_timeline_t distributed_ctx_policies_t::connection_timepoints()const  noexcept {
	return make_timeline(_connectionTL);
	}

const std::chrono::milliseconds& distributed_ctx_policies_t::timeout_probe_async()const  noexcept {
	return _timeoutProbeAsync;
	}

const std::chrono::milliseconds& distributed_ctx_policies_t::timeout_wait_io()const  noexcept {
	return _timeoutWaitIO;
	}

const std::chrono::milliseconds& distributed_ctx_policies_t::threadpool_wait_item()const  noexcept {
	return _timeoutThreadPoolWaitItem;
	}

const std::chrono::milliseconds& distributed_ctx_policies_t::timeout_inactivity()const noexcept {
	return _timeoutInactivity;
	}

const std::chrono::milliseconds& distributed_ctx_policies_t::timeout_rt0rt1_command_execute()const  noexcept {
	return _timeout_rt0rt1_command_execute;
	}

const std::chrono::milliseconds& distributed_ctx_policies_t::timeout_reconnect_attempt()const  noexcept {
	return _timeoutReconnectAttempt;
	}

sndrcv_timeline_t distributed_ctx_policies_t::timeout_sendrecv()const noexcept {
	return make_timeline(_timeouteSndRcv);
	}

const errem_points_t& distributed_ctx_policies_t::errem_points(rtl_level_t l)const noexcept {
	if(l == rtl_level_t::l0)
		return _erremPoints0;
	else if(l == rtl_level_t::l1)
		return _erremPoints1;
	else {
		FATAL_ERROR_FWD(nullptr);
		}
	}

bool distributed_ctx_policies_t::ping_rt()const  noexcept {
	return _pingRt;
	}

bool distributed_ctx_policies_t::use_crc()const noexcept {
	return _useCrc;
	}

errem_generator_mode distributed_ctx_policies_t::errmode()const noexcept {
	return _errmode;
	}

errem_generator_mode distributed_ctx_policies_t::errmode_connection_corrupt(rtl_level_t l)const noexcept {
	if(l == rtl_level_t::l0)
		return _errmodeConnection0;
	else if(l == rtl_level_t::l1)
		return _errmodeConnection1;
	else {
		FATAL_ERROR_FWD(nullptr);
		}
	}

const std::wstring& distributed_ctx_policies_t::upload_folder()const  noexcept {
	return _uploadFolder;
	}

sndrcv_timeline_periodicity_t distributed_ctx_policies_t::get_preffered_inactivity_or_ping()const noexcept {
	if(ping_rt())
		return timeout_ping();
	else
		return sndrcv_timeline_periodicity_t::make_cyclic_timeouted(timeout_inactivity(), timeout_inactivity());
	}



