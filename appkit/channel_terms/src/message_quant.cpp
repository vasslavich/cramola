#include "../../appkit.h"
#include "../../notifications/exceptions/excdrv.h"


using namespace crm;
using namespace crm::detail;


static_assert(srlz::is_serializable_v< _packet_t>, __FILE_LINE__);
static_assert(!srlz::detail::is_strob_serializable_v<_packet_t>, __FILE_LINE__);
static_assert(srlz::detail::is_srlzd_prefixed_v<_packet_t>, __FILE_LINE__);



/*##############################################################################################

_packet_t

################################################################################################*/



_packet_t::_packet_t(_packet_t && o)noexcept
	: _type(std::move(o._type))
	, _address(std::move(o._address))
	, _typeId(std::move(o._typeId))
	, _typeName(std::move(o._typeName))
	, _crc(std::move(o._crc))
	, _data(std::move(o._data))

#ifdef CBL_MPF_ENABLE_TIMETRACE
	, _timeline(std::move(o._timeline))
#endif//CBL_MPF_ENABLE_TIMETRACE

#ifdef CBL_MPF_MESSAGE_TRACEWAY_KEYS_BY_ID
	, _ktid(std::move(o._ktid))
#endif

#ifdef CBL_MESSAGES_COUNTER_TYPES_CHECKER
	, _ocounter(std::move(o._ocounter))
#endif

#ifdef CBL_MPF_CHECK_LOST_PACKETS
	, _lost(o._lost)
#endif

	, _error(std::move(o._error))
	, _valued(std::move(o._valued)) {

	o.reset();
	}

_packet_t& _packet_t::operator = (_packet_t && o) noexcept{
	_type = std::move( o._type );
	_address = std::move( o._address );
	_typeId = std::move( o._typeId );
	_typeName = std::move( o._typeName );
	_crc = std::move( o._crc );
	_data = std::move( o._data );

#ifdef CBL_MPF_ENABLE_TIMETRACE
	_timeline = std::move( o._timeline );
#endif//CBL_MPF_ENABLE_TIMETRACE

#ifdef CBL_MPF_MESSAGE_TRACEWAY_KEYS_BY_ID
	_ktid = std::move(o._ktid);
#endif

#ifdef CBL_MESSAGES_COUNTER_TYPES_CHECKER
	_ocounter = std::move(o._ocounter);
#endif

#ifdef CBL_MPF_CHECK_LOST_PACKETS
	_lost = o._lost;
#endif

	_error = std::move(o._error);
	_valued = std::move( o._valued );

	o.reset();

	return (*this);
	}

_packet_t::~_packet_t() {
#ifdef CBL_MPF_CHECK_LOST_PACKETS
	if(_lost) {
		CBL_MPF_KEYTRACE_SET(_ktid, 258);
		FATAL_ERROR_FWD(nullptr);
		}
	else {
		CBL_MPF_KEYTRACE_SET(_ktid, 259);
		}
#endif
	}

size_t _packet_t::get_serialized_size()const noexcept {
	return srlz::object_trait_serialized_size(_type) +
		srlz::object_trait_serialized_size(_address) +
		srlz::object_trait_serialized_size(_typeId) +
		srlz::object_trait_serialized_size(_typeName) +
		srlz::object_trait_serialized_size(_crc) +
		srlz::object_trait_serialized_size(_data) +

#ifdef CBL_MPF_ENABLE_TIMETRACE
		srlz::object_trait_serialized_size(_timeline) +
#endif//CBL_MPF_ENABLE_TIMETRACE

#ifdef CBL_MPF_MESSAGE_TRACEWAY_KEYS_BY_ID
		srlz::object_trait_serialized_size(_ktid) +
#endif

#ifdef CBL_MPF_CHECK_LOST_PACKETS
		srlz::object_trait_serialized_size(_lost) +
#endif

		srlz::object_trait_serialized_size(_error) +
		srlz::object_trait_serialized_size(_valued);
	}

void _packet_t::mark_as_used()noexcept {
#ifdef CBL_MPF_CHECK_LOST_PACKETS
	_lost = false;
#endif
	}

bool _packet_t::is_mark_used()const noexcept {
#ifdef CBL_MPF_CHECK_LOST_PACKETS
	return !_lost;
#else
	return true;
#endif
	}

bool _packet_t::error()const noexcept {
	return _error;
	}

void _packet_t::mark_as_error()noexcept {
	_error = true;
	}

_packet_t _packet_t::make_error(const address_descriptor_t & address)noexcept {
	_packet_t p(iol_type_spcf_t{}, address, {});
	p.mark_as_error();
	
	return p;
	}

_packet_t::_packet_t()noexcept{}

_packet_t::_packet_t(iol_type_spcf_t type,
	const address_descriptor_t & address_,
	binary_vector_t && data)
	: _type(type)
	, _address(address_)
	, _data(std::move(data))

#ifdef CBL_MPF_MESSAGE_TRACEWAY_KEYS_BY_ID 
	, _ktid(trait_at_keytrace_set(address_))
#endif

#ifdef CBL_MESSAGES_COUNTER_TYPES_CHECKER
	, _ocounter("cdr:" + std::to_string(type.utype()))
#endif

	, _valued(true) 	
	{}

_packet_t::_packet_t(
	iol_type_spcf_t type,
	const address_descriptor_t & address_,
	global_object_identity_t typeId,
	const std::string &typeName,
	crc_t::base_value_t crc,
	binary_vector_t && data
#ifdef CBL_MPF_ENABLE_TIMETRACE
	, crm::detail::hanlder_times_stack_t && tl
#endif//CBL_MPF_ENABLE_TIMETRACE
	)
	: _type( type )
	, _address( std::move( address_ ) )
	, _typeId( typeId )
	, _typeName( typeName )
	, _crc( crc )
	, _data( std::move( data ) )

#ifdef CBL_MPF_ENABLE_TIMETRACE
	, _timeline(std::move(tl))
#endif//CBL_MPF_ENABLE_TIMETRACE

#ifdef CBL_MPF_MESSAGE_TRACEWAY_KEYS_BY_ID 
	, _ktid(trait_at_keytrace_set(address_))
#endif

#ifdef CBL_MESSAGES_COUNTER_TYPES_CHECKER
	, _ocounter("cdr:" + std::to_string(type.utype()))
#endif

	, _valued( true )
	{

#ifdef CBL_MPF_ENABLE_TIMETRACE
	/* ��������� ����� ������� ������ */
	_timeline[crm::detail::hanlder_times_stack_t::stage_t::packet_capture] = std::chrono::system_clock::now();
#endif//CBL_MPF_ENABLE_TIMETRACE
	}

void _packet_t::reset() noexcept{
	_type = iol_type_spcf_t();
	_address = address_descriptor_t::null;
	_typeId = decltype(_typeId)::null;
	_typeName.clear();
	_crc = 0;
	_data.clear();
	_error = false;
	_valued = false;

#ifdef CBL_MPF_ENABLE_TIMETRACE
	_timeline.clear();
#endif//CBL_MPF_ENABLE_TIMETRACE

#ifdef CBL_MPF_MESSAGE_TRACEWAY_KEYS_BY_ID
	_ktid = decltype(_ktid)::null;
#endif

#ifdef CBL_MPF_CHECK_LOST_PACKETS
	_lost = false;
#endif
	}

bool _packet_t::valued()const noexcept{
	return _valued;
	}

const iol_type_spcf_t& _packet_t::type()const noexcept{
	return _type;
	}

const address_descriptor_t& _packet_t::address()const noexcept{
	return _address;
	}

const global_object_identity_t& _packet_t::type_id()const noexcept{
	return _typeId;
	}

const std::string& _packet_t::type_name()const noexcept{
	return _typeName;
	}

crc_t::base_value_t _packet_t::crc()const noexcept{
	return _crc;
	}

const binary_vector_t& _packet_t::data()const noexcept{
	return _data;
	}

binary_vector_t _packet_t::data_release() noexcept{
	return std::move( _data );
	}

_packet_t _packet_t::copy()const {
	_packet_t p;
	p._type = _type;
	p._address = _address;
	p._typeId = _typeId;
	p._typeName = _typeName;
	p._crc = _crc;
	p._data = _data;
	p._error = _error;
	p._valued = _valued;


#ifdef CBL_MPF_ENABLE_TIMETRACE
	p._timeline = _timeline;
#endif//CBL_MPF_ENABLE_TIMETRACE

#ifdef CBL_MPF_MESSAGE_TRACEWAY_KEYS_BY_ID
	p._ktid = _ktid;
#endif

#ifdef CBL_MPF_CHECK_LOST_PACKETS
	p._lost = _lost;
#endif

	return p;
	}

/*! true - ���� l ����� ������������, ��� r */
bool _packet_t::priority( const _packet_t &l, const _packet_t &r ) noexcept{
	return l.type().priority_value() >= r.type().priority_value();
	}

#ifdef CBL_MPF_ENABLE_TIMETRACE
const crm::detail::hanlder_times_stack_t& _packet_t::timeline()const {
	return _timeline;
	}

crm::detail::hanlder_times_stack_t _packet_t::timeline_release() {
	return std::move( _timeline );
	}

void _packet_t::timeline_set_rt1_receiver_router() {
	_timeline[crm::detail::hanlder_times_stack_t::stage_t::rt1_receiver_router] = std::chrono::system_clock::now();
	}
#endif//CBL_MPF_ENABLE_TIMETRACE
