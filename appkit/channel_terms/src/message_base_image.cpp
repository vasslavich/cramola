#include "../../internal.h"
#include "../message_base_image.h"


using namespace crm;
using namespace crm::detail;


input_item_t::input_item_t() noexcept {}

input_item_t::input_item_t(std::unique_ptr<i_iol_ihandler_t>&& object_,
	const identity_descriptor_t& sourceObjectId_)noexcept
	: object_(std::move(object_))
	, sourceObjectId_(sourceObjectId_) {}

const identity_descriptor_t& input_item_t::desc()const noexcept {
	return sourceObjectId_;
	}

const std::unique_ptr<i_iol_ihandler_t>& input_item_t::m()const& noexcept {
	return object_;
	}

std::unique_ptr<i_iol_ihandler_t> input_item_t::m() && noexcept {
	return std::move(object_);
	}

bool input_item_t::is_null()const noexcept { 
	return !object_; 
	}





bool crm::detail::is_sndrcv_spin_tagged_invoke(const i_iol_handler_t& h)noexcept {
	return is_sendrecv_cycle(h) && check_sndrcv_key_spin_tag(h.spin_tag());
	}

bool crm::detail::check_sndrcv_key_spin_tag(const i_iol_handler_t& h)noexcept {
	return is_sndrcv_spin_tagged_invoke(h) || !is_sendrecv_cycle(h);
	}

bool crm::is_sendrecv_cycle(const i_iol_handler_t & h)  noexcept {
	return h.is_sendrecv_cycle();
	}

bool crm::is_sendrecv_request(const i_iol_handler_t & h)  noexcept {
	return h.is_sendrecv_request();
	}

bool crm::is_sendrecv_response(const i_iol_handler_t & h)  noexcept {
	return h.is_sendrecv_response();
	}

bool crm::is_report(const i_iol_handler_t& h)noexcept {
	return h.type().fl_report();
	}

rtl_table_t crm::passing_direction(const i_iol_handler_t& h)noexcept {
	return h.direction();
	}


/*========================================================================
i_iol_handler_t
==========================================================================*/


const global_object_identity_t i_iol_handler_t::null_id = global_object_identity_t::null;

i_iol_handler_t::i_iol_handler_t(iol_type_spcf_t type_, rtl_table_t d_)noexcept
	: _direction(d_)

#ifdef CBL_MESSAGES_COUNTER_TYPES_CHECKER
	, _messagesCounter(cvt2string(type_))
#endif
	{
	set_type(type_);
	}

i_iol_handler_t::i_iol_handler_t( iol_type_spcf_t type_, rtl_table_t d_, const global_object_identity_t & id_ )noexcept
	: _direction(d_)

#ifdef CBL_MESSAGES_COUNTER_TYPES_CHECKER
	, _messagesCounter(cvt2string(type_))
#endif
	{

	set_type( type_ );
	set_id( id_ );

#ifdef CBL_MPF_ENABLE_TIMETRACE
	/* ��������� ������� � �������� ������� */
	set_timeline( hanlder_times_stack_t::stage_t::create );
#endif//CBL_MPF_ENABLE_TIMETRACE
	}


i_iol_handler_t::i_iol_handler_t(iol_typeid_spcf_t type_, detail::rtl_table_t d_)noexcept
	: _typeid(std::move(type_))
	, _direction(d_)

#ifdef CBL_MESSAGES_COUNTER_TYPES_CHECKER
	, _messagesCounter(_typeid.type_name())
#endif
{

#ifdef CBL_MPF_ENABLE_TIMETRACE
	/* ��������� ������� � �������� ������� */
	set_timeline(hanlder_times_stack_t::stage_t::create);
#endif//CBL_MPF_ENABLE_TIMETRACE
}

i_iol_handler_t::i_iol_handler_t(iol_typeid_spcf_t type_, detail::rtl_table_t d_, const global_object_identity_t& id_)noexcept
	: _typeid(std::move(type_))
	, _direction(d_)

#ifdef CBL_MESSAGES_COUNTER_TYPES_CHECKER
	, _messagesCounter(_typeid.type_name())
#endif
{
	set_id(id_);

#ifdef CBL_MPF_ENABLE_TIMETRACE
	/* ��������� ������� � �������� ������� */
	set_timeline(hanlder_times_stack_t::stage_t::create);
#endif//CBL_MPF_ENABLE_TIMETRACE
}


i_iol_handler_t::i_iol_handler_t(iol_type_spcf_t mtype_,
	iol_typeid_spcf_t type_,
	detail::rtl_table_t d_)noexcept
	: _typeid(std::move(type_))
	, _direction(d_)

#ifdef CBL_MESSAGES_COUNTER_TYPES_CHECKER
	, _messagesCounter(_typeid.type_name())
#endif
{
	set_type(mtype_);

#ifdef CBL_MPF_ENABLE_TIMETRACE
	/* ��������� ������� � �������� ������� */
	set_timeline(hanlder_times_stack_t::stage_t::create);
#endif//CBL_MPF_ENABLE_TIMETRACE
}

i_iol_handler_t::i_iol_handler_t(iol_type_spcf_t mtype_,
	iol_typeid_spcf_t type_,
	detail::rtl_table_t d_,
	const global_object_identity_t& id_)noexcept
	: _typeid(std::move(type_))
	, _direction(d_)

#ifdef CBL_MESSAGES_COUNTER_TYPES_CHECKER
	, _messagesCounter(_typeid.type_name())
#endif
{
	set_type(mtype_);
	set_id(id_);

#ifdef CBL_MPF_ENABLE_TIMETRACE
	/* ��������� ������� � �������� ������� */
	set_timeline(hanlder_times_stack_t::stage_t::create);
#endif//CBL_MPF_ENABLE_TIMETRACE
}

i_iol_handler_t::i_iol_handler_t( i_iol_handler_t && o)noexcept
	: _typeid(std::move(o._typeid))
	, _address(std::move(o._address))
	, _type(std::move(o._type))
	, _tpdrv(std::move(o._tpdrv))
	, _reverseExc(std::move(o._reverseExc))
	, _direction(o._direction)

#ifdef CBL_MESSAGES_COUNTER_TYPES_CHECKER
	, _messagesCounter(std::move(o._messagesCounter))
#endif

	
#ifdef CBL_MPF_ENABLE_TIMETRACE
	, _timeline(std::move(o._timeline))
#endif
	{}

i_iol_handler_t& i_iol_handler_t::operator=( i_iol_handler_t && o)noexcept{
	if (this != std::addressof(o)) {
		_typeid = std::move(o._typeid);
		_address = std::move(o._address);
		_type = std::move(o._type);
		_tpdrv = std::move(o._tpdrv);
		_reverseExc = std::move(o._reverseExc);
		_direction = o._direction;

#ifdef CBL_MESSAGES_COUNTER_TYPES_CHECKER
		_messagesCounter = std::move(o._messagesCounter);
#endif

#ifdef CBL_MPF_ENABLE_TIMETRACE
		_timeline = std::move(o._timeline);
#endif
		}

	return (*this);
	}

rtl_table_t i_iol_handler_t::direction()const noexcept {
	return _direction;
	}

i_iol_handler_t::~i_iol_handler_t() {
	CBL_MPF_KEYTRACE_SET((*this), 601);

	if(_reverseExc && (*_reverseExc)) {
		if (direction() == rtl_table_t::in) {
			CBL_MPF_KEYTRACE_SET((*this), 602);
			}
		else if (direction() == rtl_table_t::out) {
			CBL_MPF_KEYTRACE_SET((*this), 603);
			}

		CHECK_NO_EXCEPTION((*_reverseExc)(invoke_no_throw{}, CREATE_MPF_PTR_EXC_FWD("abandoned sndrcv message:" + spin_tag().to_str() + ":" + std::to_string(type().utype()))));
		}
	}

inbound_message_with_tail_exception_guard i_iol_handler_t::reset_invoke_exception_guard()noexcept {
	return _reverseExc ? std::move(*_reverseExc) : inbound_message_with_tail_exception_guard{};
	}

bool i_iol_handler_t::is_reverse_exception_guard()const noexcept {
	return _reverseExc && (*_reverseExc);
	}

void i_iol_handler_t::invoke_reverse_exception(std::unique_ptr<dcf_exception_t> && exc)noexcept {
	CBL_VERIFY(exc);

	if(_reverseExc) {
		(*_reverseExc)(invoke_no_throw{}, std::move(exc));
		}
	}

inbound_message_with_tail_exception_guard i_iol_handler_t::reset_invoke_exception_guard(
	inbound_message_with_tail_exception_guard && rev)noexcept {

	auto tmp(std::make_unique< inbound_message_with_tail_exception_guard>(std::move(rev)));
	std::swap(tmp, _reverseExc);

	return tmp ? std::move(*tmp) : inbound_message_with_tail_exception_guard{};
	}

void i_iol_handler_t::capture_response_tail(i_iol_handler_t & hsrc, bool asReport)noexcept {
	set_echo_params(hsrc, asReport);

	if(reset_invoke_exception_guard(hsrc.reset_invoke_exception_guard())) {
		FATAL_ERROR_FWD(nullptr);
		}

	CBL_VERIFY(is_null(source_subscriber_id()));
	}

void i_iol_handler_t::set_echo_params(const i_iol_handler_t & src, bool asReport ) {
	CBL_VERIFY(src.source_id() == src.address().source_id());
	CBL_VERIFY(src.destination_id() == src.address().destination_id());
	CBL_VERIFY(src.session() == src.address().session());
	CBL_VERIFY(src.source_subscriber_id() == src.address().source_subscriber_id());
	CBL_VERIFY(src.level() == src.address().level());

	set_echo_params(src.address(), asReport);
	}

void i_iol_handler_t::set_echo_params(const address_descriptor_t & src, bool asReport ) {
	CBL_VERIFY(!is_null(src.source_id()));
	//CBL_VERIFY(!is_null(src.destination_id()));
	//CBL_VERIFY(!is_null(src.session()));
	//CBL_VERIFY(!is_null(src.source_subscriber_id()));
	//CBL_VERIFY(!is_null(src.spin_tag()));
	//CBL_VERIFY(!is_null(src.level()));

	/*set_destination_id(src.source_id());
	set_source_id(src.destination_id());
	set_session(src.session());
	set_destination_subscriber_id(src.source_subscriber_id());
	set_spin_tag(src.spin_tag());
	set_level(src.level());*/

	src.set_echo(_address);

	if(asReport) {
		_type.setf_report();
		CBL_VERIFY_SNDRCV_ATTRIBUTES(*this);
		}
	else {
		CBL_VERIFY(type().utype() == (int)iol_types_t::st_ping || !is_sendrecv_cycle() );
		}

	CBL_VERIFY(is_null(source_subscriber_id()));
	}

void i_iol_handler_t::set_typedriver( std::weak_ptr<crm::i_types_driver_t> tpdrv ) {
	_tpdrv = tpdrv;
	}

void i_iol_handler_t::set_typedriver( std::weak_ptr<crm::i_types_driver_t> tpdrv )const {
	_tpdrv = tpdrv;
	}

std::shared_ptr<crm::i_types_driver_t> i_iol_handler_t::typedriver()const {
	return _tpdrv.lock();
	}

void i_iol_handler_t::cvt_from( _packet_t && cdr ) {

	set_type( cdr.type() );

	if( cdr.type().fl_typeid() ) {
		set_typeid_id( cdr.type_id() );
		set_typeid_name( cdr.type_name() );
		}

	set_address( cdr.address() );

#ifdef CBL_MPF_ENABLE_TIMETRACE
	/* ������ ��������� ������� �� ������� ����������� */
	_timeline = cdr.timeline_release();
	/* ������� ������� �������������� �� ������ */
	set_timeline( hanlder_times_stack_t::stage_t::deserialize_from_packet );
#endif//CBL_MPF_ENABLE_TIMETRACE
	}

void i_iol_handler_t::set_update()noexcept {
	_type.set( iol_type_attributes_t::type_id, (int)(!_typeid.is_null()) );
	}

void i_iol_handler_t::set_type( const iol_type_spcf_t &type )noexcept {
	_type = type;
	set_update();
	}

void i_iol_handler_t::set_id( const global_object_identity_t &id )noexcept {
	_address.set_id( id );
	}

void i_iol_handler_t::set_address( const address_descriptor_t & address_ ) {
	_address = address_;
	}

void i_iol_handler_t::set_destination_id( const identity_descriptor_t &id ) {
	_address.set_destination_id( id );
	}

void i_iol_handler_t::set_destination_subscriber_id( const subscriber_node_id_t &id ) {
	_address.set_destination_subscriber_id( id );
	}

void i_iol_handler_t::set_source_id( const identity_descriptor_t &id ) {
	_address.set_source_id( id );
	}

void i_iol_handler_t::set_source_subscriber_id( const subscriber_node_id_t &id ) {
	_address.set_source_subscriber_id( id );
	}

void i_iol_handler_t::set_session( const session_description_t &id ) {
	_address.set_session( id );
	}

void i_iol_handler_t::set_spin_tag( const message_spin_tag_type& tag ){
	_address.set_spin_tag( tag );
	}

void i_iol_handler_t::set_address( const rtl_roadmap_link_t & lnk ) {
	_address.set_from( lnk );
	}

void i_iol_handler_t::set_typeid_id( const type_identifier_t &typeId ) {
	_typeid.set_id( typeId );
	set_update();
	}

void i_iol_handler_t::set_typeid_name( const std::string &typeName ) {
	_typeid.set_name( typeName );
	set_update();
	}

void i_iol_handler_t::setf_use_crc()noexcept {
	_type.setf_use_crc();
	set_update();
}

void i_iol_handler_t::setf_report() noexcept {
	_type.setf_report();
	set_update();
	}

void i_iol_handler_t::setf_length() noexcept {
	_type.setf_length();
	set_update();
	}

void i_iol_handler_t::set_level( rtl_level_t l ) {
	_address.set_level( l );
	}

const address_descriptor_t& i_iol_handler_t::address()const  noexcept {
	return _address;
	}

const global_object_identity_t& i_iol_handler_t::id()const noexcept {
	return _address.id();
	}

const identity_descriptor_t& i_iol_handler_t::destination_id()const  noexcept {
	return _address.destination_id();
	}

const identity_descriptor_t& i_iol_handler_t::source_id()const  noexcept {
	return _address.source_id();
	}

const subscriber_node_id_t& i_iol_handler_t::source_subscriber_id()const noexcept {
	return _address.source_subscriber_id();
	}

const subscriber_node_id_t& i_iol_handler_t::destination_subscriber_id()const noexcept {
	return _address.destination_subscriber_id();
	}

const message_spin_tag_type& i_iol_handler_t::spin_tag()const noexcept{
	return _address.spin_tag();
	}

const session_description_t& i_iol_handler_t::session()const  noexcept {
	return _address.session();
	}

bool i_iol_handler_t::fl_level()const  noexcept {
	return _address.fl_level();
	}

rtl_level_t i_iol_handler_t::level()const  noexcept {
	return _address.level();
	}

bool i_iol_handler_t::is_sendrecv_cycle()const noexcept {
	return is_sendrecv_request() || is_sendrecv_response();
	}

bool i_iol_handler_t::is_sendrecv_request()const noexcept {
	if (type().utype() == (int)iol_types_t::st_ping) {
		return false;
		}
	else {
		return ::is_sendrecv_request(address());
		}
	}

bool i_iol_handler_t::is_sendrecv_response()const noexcept {
	if (type().utype() == (int)iol_types_t::st_ping) {
		return false;
		}
	else {
		bool isAddressResponsed = ::is_sendrecv_response(address());
		bool isFlagResponsed = type().fl_report();

		CBL_VERIFY((isAddressResponsed && isFlagResponsed) || (!isAddressResponsed && !isFlagResponsed));

		return isFlagResponsed && isAddressResponsed;
		}
	}

const iol_typeid_spcf_t& i_iol_handler_t::type_id()const noexcept {
	return _typeid;
	}

const iol_type_spcf_t& i_iol_handler_t::type()const noexcept {
	return _type;
	}

iol_types_ibase_t i_iol_handler_t::priority_value()const  noexcept {
	return type().priority_value();
	}

/*! true - ���� l ����� ������������, ��� r */
bool i_iol_handler_t::priority( const i_iol_handler_t &l, const i_iol_handler_t &r ) noexcept {
	return l.priority_value() >= r.priority_value();
	}

#ifdef CBL_MPF_ENABLE_TIMETRACE
void i_iol_handler_t::set_timeline_serialize()const {
	_timeline[hanlder_times_stack_t::stage_t::serialize] = std::chrono::system_clock::now();
	}

void i_iol_handler_t::set_timeline( hanlder_times_stack_t::stage_t st ) {

	set_timeline( st, std::chrono::system_clock::now() );
	}

void i_iol_handler_t::set_timeline( hanlder_times_stack_t::stage_t st,
	const hanlder_times_stack_t::time_point_t & tp ) {

	_timeline[st] = tp;
	}

const hanlder_times_stack_t& i_iol_handler_t::timeline()const {
	return _timeline;
	}
#endif//CBL_MPF_ENABLE_TIMETRACE


/*========================================================================
i_iol_ihandler_t
==========================================================================*/


i_iol_ihandler_t::i_iol_ihandler_t(i_iol_ihandler_t && o)noexcept
	: i_iol_handler_t(std::move(o)){}

i_iol_ihandler_t& i_iol_ihandler_t::operator=(i_iol_ihandler_t && o) noexcept {
	(*(static_cast<i_iol_handler_t*>(this))) = (std::move(o));

	return (*this);
	}

i_iol_ihandler_t::i_iol_ihandler_t(iol_type_spcf_t type )noexcept
	: i_iol_handler_t(type, rtl_table_t::in) {}

i_iol_ihandler_t::i_iol_ihandler_t(iol_type_spcf_t mtype_, iol_typeid_spcf_t type_)noexcept
	: i_iol_handler_t(mtype_, type_, rtl_table_t::in) {}

i_iol_ihandler_t::i_iol_ihandler_t( iol_type_spcf_t type, global_object_identity_t id )noexcept
	: i_iol_handler_t( type, rtl_table_t::in, id ) {}

i_iol_ihandler_t::~i_iol_ihandler_t() {}


void i_iol_ihandler_t::build() {}


/*========================================================================
i_iol_ohandler_t
==========================================================================*/


i_iol_ohandler_t::i_iol_ohandler_t(i_iol_ohandler_t && o)noexcept
	: i_iol_handler_t(std::move(o)) {}

i_iol_ohandler_t& i_iol_ohandler_t::operator=(i_iol_ohandler_t && o) noexcept {
	(*(static_cast<i_iol_handler_t*>(this))) = (std::move(o));
	return (*this);
	}

i_iol_ohandler_t::i_iol_ohandler_t(iol_type_spcf_t type_)noexcept
	: i_iol_ohandler_t(type_, global_object_identity_t::null) {}

i_iol_ohandler_t::i_iol_ohandler_t(iol_type_spcf_t typecd_, iol_typeid_spcf_t typeid_)noexcept
	: i_iol_ohandler_t(typecd_, typeid_, global_object_identity_t::null) {}

i_iol_ohandler_t::i_iol_ohandler_t( iol_type_spcf_t type_, const global_object_identity_t & id_ )noexcept
	: i_iol_handler_t( type_, rtl_table_t::out, id_ ) {

	CBL_VERIFY( type_.utype() != iol_type_spcf_t::undefined );

#ifdef CBL_MPF_ENABLE_TIMETRACE
	set_type_attribute( iol_type_attributes_t::timeline, 1 );
#endif//CBL_MPF_ENABLE_TIMETRACE
	}


i_iol_ohandler_t::i_iol_ohandler_t(iol_type_spcf_t typecd_, 
	iol_typeid_spcf_t typeid_, 
	const global_object_identity_t& id)noexcept
	: i_iol_handler_t(typecd_, typeid_, rtl_table_t::out, id) {

	CBL_VERIFY(typecd_.utype() != iol_type_spcf_t::undefined);

#ifdef CBL_MPF_ENABLE_TIMETRACE
	set_type_attribute(iol_type_attributes_t::timeline, 1);
#endif//CBL_MPF_ENABLE_TIMETRACE
	}


bool crm::is_normalizedx(const i_iol_handler_t& obj)noexcept {
	bool f =
		!is_null(obj.session()) &&
		!is_null(obj.destination_id()) &&
		!is_null(obj.source_id()) &&
		!is_null(obj.level()) &&
		!is_null(obj.id());

	if (!f) {
		auto fx1 = !is_null(obj.session());
		auto fx2 = !is_null(obj.source_subscriber_id());
		auto fx3 = !is_null(obj.destination_subscriber_id());
		auto fx4 = !is_null(obj.level());

		f = fx1 && fx2 && fx3 && fx4;

		if (!f) {
			f = fx1 && fx2;
			}
		}

	return f;
	}
