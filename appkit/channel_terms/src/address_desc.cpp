#include "../../internal.h"
#include "../../typeframe/streamcat/read/rstreams_isrlz.h"
#include "../../typeframe/internal_terms.h"
#include "../../notifications/internal_terms.h"
#include "../../exceptions.h"
#include "../../typeframe.h"
#include "../address_desc.h"


using namespace crm;
using namespace crm::detail;


static_assert(noexcept(std::declval<const address_descriptor_t>().object_hash()), __FILE_LINE__);
static_assert(has_hashable_support_v< address_descriptor_t>, __FILE_LINE__);
static_assert(!srlz::detail::is_serialized_trait_v<address_descriptor_t>, __FILE_LINE__);
static_assert(srlz::detail::is_i_srlzd_prefixed_v<address_descriptor_t>, __FILE_LINE__);
static_assert(srlz::is_serializable_v<address_descriptor_t>, __FILE_LINE__);
static_assert(srlz::is_read_accumulate_supported_v<address_descriptor_t>, __FILE_LINE__);
static_assert(srlz::detail::has_serializable_properties_v<address_descriptor_t>, __FILE_LINE__);
static_assert(srlz::serializable_properties_trait_t<address_descriptor_t>::get().serialized_size == CRM_KB, __FILE_LINE__);
static_assert(srlz::serializable_properties_trait_t<address_descriptor_t>::get().size_as == srlz::serialize_properties_length::as_maxsize, __FILE_LINE__);
static_assert(sizeof(srlz::detail::serializable_prefix_type_trait_t<address_descriptor_t>::serialized_type) == 2, __FILE_LINE__);


using address_descriptor_prefix_trait = typename srlz::detail::serializable_prefix_type_trait_t<address_descriptor_t>::prefix_trait;
static_assert(std::is_same_v<address_descriptor_prefix_trait, srlz::detail::serializable_properties_prefix_trait_nbytes<uint16_t>>, __FILE_LINE__);

static_assert(srlz::detail::is_fixed_size_serialized_v<address_descriptor_prefix_trait>, __FILE_LINE__);
static_assert(srlz::detail::has_serializable_properties_v<address_descriptor_t>, __FILE_LINE__);
static_assert((std::is_final_v<address_descriptor_prefix_trait> &&
	srlz::detail::is_strob_flat_serializable_v<address_descriptor_prefix_trait>)
	||
	!std::is_final_v<address_descriptor_prefix_trait> &&
	srlz::detail::is_strob_flat_serializable_v < address_descriptor_prefix_trait>
	, __FILE_LINE__);

static_assert((std::is_final_v<address_descriptor_prefix_trait>&&
	srlz::detail::is_i_srlzd_base_v<address_descriptor_prefix_trait>)
	||
	(!std::is_final_v<address_descriptor_prefix_trait> &&
	!srlz::detail::is_i_srlzd_base_v<address_descriptor_prefix_trait>), __FILE_LINE__);

static_assert((std::is_final_v<address_descriptor_prefix_trait> &&
	!srlz::detail::is_strob_serializable_strong_v<address_descriptor_prefix_trait>)
	||
	(!std::is_final_v<address_descriptor_prefix_trait> &&
		srlz::detail::is_strob_serializable_strong_v<address_descriptor_prefix_trait>), __FILE_LINE__);

static_assert((std::is_final_v<address_descriptor_prefix_trait> &&
	!srlz::detail::is_strob_prefixed_serialized_size_v<address_descriptor_prefix_trait>)
	||
	(!std::is_final_v<address_descriptor_prefix_trait> &&
		srlz::detail::is_strob_prefixed_serialized_size_v<address_descriptor_prefix_trait>), __FILE_LINE__);

static_assert((std::is_final_v<address_descriptor_prefix_trait>&&
	srlz::detail::is_i_srlzd_prefixed_v<address_descriptor_prefix_trait>)
	||
	(!std::is_final_v<address_descriptor_prefix_trait> &&
		!srlz::detail::is_i_srlzd_prefixed_v<address_descriptor_prefix_trait>), __FILE_LINE__);

static_assert((std::is_final_v<address_descriptor_t> && 
	!srlz::detail::is_i_srlzd_eof_v<address_descriptor_prefix_trait>)
	||
	(!std::is_final_v<address_descriptor_prefix_trait> &&
		!srlz::detail::is_i_srlzd_eof_v<address_descriptor_prefix_trait>), __FILE_LINE__);

static_assert((std::is_final_v<address_descriptor_prefix_trait> &&
	!srlz::detail::is_serialized_trait_w_v<address_descriptor_prefix_trait>)
	||
	(!std::is_final_v<address_descriptor_prefix_trait> &&
		!srlz::detail::is_serialized_trait_w_v<address_descriptor_prefix_trait>), __FILE_LINE__);

static_assert((std::is_final_v<address_descriptor_prefix_trait>&&
	srlz::detail::is_i_srlzd_v<address_descriptor_prefix_trait>)
	||
	(!std::is_final_v<address_descriptor_prefix_trait> &&
		!srlz::detail::is_i_srlzd_v<address_descriptor_prefix_trait>), __FILE_LINE__);


static_assert(std::is_same_v<size_t, decltype(srlz::object_trait_serialized_size(std::declval<address_descriptor_prefix_trait>()))>, __FILE_LINE__);
static_assert(std::is_same_v<srlz::detail::serializable_prefix_type_trait_t<address_descriptor_t>::serialized_type, uint16_t>, __FILE_LINE__);

static_assert(srlz::detail::has_serializable_properties_v<address_descriptor_t>, __FILE_LINE__);
static_assert(std::is_same_v<srlz::serialized_options_type< address_descriptor_t>, 
	srlz::serializable_properties_trait<address_descriptor_t, none_options>::serialized_options_type > , __FILE_LINE__);
static_assert(std::is_same_v < srlz::serialized_options_cond<address_descriptor_t>, 
	srlz::serializable_properties_trait<address_descriptor_t, none_options>::serialized_options_type>, __FILE_LINE__);

const address_descriptor_t address_descriptor_t::null;

void address_descriptor_t::__set_flag( taglst_t attribute )noexcept {
	CBL_VERIFY( taglst_t::null != attribute );

	_flags = (flags_utype_t)_flags | (flags_utype_t)attribute;

	/* �������� ���-�������� �������  */
	update_hash();
	}

address_descriptor_t::flags_utype_t address_descriptor_t::__get_uflags( taglst_t attribute )const noexcept{
	CBL_VERIFY( taglst_t::null != attribute );

	const auto uattr = (flags_utype_t)attribute;
	const auto result = uattr & (flags_utype_t)_flags;
	return result;
	}

bool address_descriptor_t::__has_flag( taglst_t attribute )const noexcept{
	static_assert(noexcept(__get_uflags(attribute)), __FILEW_LINE__);

	return 0 != __get_uflags(attribute);
	}

address_descriptor_t::address_descriptor_t() noexcept {
	CBL_VERIFY(::is_null(_id));
	CBL_VERIFY(::is_null(_destId));
	CBL_VERIFY(::is_null(_srcId));
	CBL_VERIFY(::is_null(_session));
	CBL_VERIFY(::is_null(_destSubscriberId));
	CBL_VERIFY(::is_null(_srcSubscriberId));
	CBL_VERIFY(::is_null(_spinTag));
	CBL_VERIFY(::is_null(_hash));
	CBL_VERIFY(::is_null(_flags));
	}

bool address_descriptor_t::fl_level()const  noexcept {
	return has_flag< taglst_t::level >();
	}

rtl_level_t address_descriptor_t::level()const noexcept{
	static_assert(noexcept(has_flag<taglst_t::level_0>()), __FILE_LINE__);

	if( has_flag<taglst_t::level_0>() )
		return rtl_level_t::l0;
	else if( has_flag<taglst_t::level_1>() )
		return rtl_level_t::l1;
	else
		return rtl_level_t::undefined;
	}

void address_descriptor_t::set_level( rtl_level_t l ) {
	if( has_flag< taglst_t::level>() )
		THROW_EXC_FWD(nullptr);

	if( l == rtl_level_t::l0 ) {
		set_flag< taglst_t::level_0 >();
		}
	else if( l == rtl_level_t::l1 ) {
		set_flag< taglst_t::level_1>( );
		}
	else
		THROW_EXC_FWD(nullptr);

	/* �������� ���-�������� �������  */
	update_hash();
	}

bool address_descriptor_t::fl_id()const  noexcept {
	return has_flag< taglst_t::id>( );
	}

bool address_descriptor_t::fl_destination_id()const  noexcept {
	return has_flag< taglst_t::dest_id>();
	}

bool address_descriptor_t::fl_source_id()const  noexcept {
	return has_flag< taglst_t::src_id>();
	}

bool address_descriptor_t::fl_session()const  noexcept {
	return has_flag< taglst_t::session>();
	}

bool address_descriptor_t::fl_destination_subscribe_id()const  noexcept {
	return has_flag< taglst_t::dest_subscrb_id>();
	}

bool address_descriptor_t::fl_source_subscribe_id()const  noexcept {
	return has_flag< taglst_t::src_subscrb_id>( );
	}

const address_descriptor_t::hash_t& address_descriptor_t::object_hash()const  noexcept {
	return _hash;
	}

void address_descriptor_t::set_id( const global_object_identity_t & id ) {
#ifdef CBL_CHECK_KEYID_VALUE_NO_REASSIGN
	if (!crm::is_null(id)) {
		if (crm::is_null(_id)) {
#endif
			_id = id;

			set_flag< taglst_t::id>();

			/* �������� ���-�������� �������  */
			update_hash();

#ifdef CBL_CHECK_KEYID_VALUE_NO_REASSIGN
			}
		else {
			THROW_EXC_FWD(nullptr);
			}
		}
#endif
	}

const global_object_identity_t& address_descriptor_t::id()const  noexcept {
	return _id;
	}

void address_descriptor_t::set_destination_id( const identity_descriptor_t & destId ) {

#ifdef CBL_CHECK_KEYID_VALUE_NO_REASSIGN
	if (!crm::is_null(destId)) {
		if (crm::is_null(_destId)) {
#endif

			_destId = destId;
			set_flag< taglst_t::dest_id>();

			/* �������� ���-�������� �������  */
			update_hash();

#ifdef CBL_CHECK_KEYID_VALUE_NO_REASSIGN
			}
		else {
			THROW_EXC_FWD(nullptr);
			}
		}
#endif
	}

const identity_descriptor_t& address_descriptor_t::destination_id()const  noexcept {
	return _destId;
	}

void address_descriptor_t::set_source_id( const identity_descriptor_t  & srcId ) {

#ifdef CBL_CHECK_KEYID_VALUE_NO_REASSIGN
	if (!crm::is_null(srcId)) {
		if (crm::is_null(_srcId)) {
#endif

			_srcId = srcId;
			set_flag< taglst_t::src_id>();

			/* �������� ���-�������� �������  */
			update_hash();

#ifdef CBL_CHECK_KEYID_VALUE_NO_REASSIGN
			}
		else {
			THROW_EXC_FWD(nullptr);
			}
		}
#endif
	}

const identity_descriptor_t& address_descriptor_t::source_id()const  noexcept {
	return _srcId;
	}

void address_descriptor_t::set_session( const session_description_t& sid ) {

#ifdef CBL_CHECK_KEYID_VALUE_NO_REASSIGN
	if (!crm::is_null(sid)) {
		if (crm::is_null(_session)) {
#endif

			_session = sid;
			set_flag< taglst_t::session>();

			/* �������� ���-�������� �������  */
			update_hash();

#ifdef CBL_CHECK_KEYID_VALUE_NO_REASSIGN
			}
		else {
			THROW_EXC_FWD(nullptr);
			}
		}
#endif
	}

const session_description_t& address_descriptor_t::session()const  noexcept {
	return _session;
	}

void address_descriptor_t::set_destination_subscriber_id( const subscriber_node_id_t & id ) {
#ifdef CBL_CHECK_KEYID_VALUE_NO_REASSIGN
	if (!crm::is_null(id)) {
		if (crm::is_null(_destSubscriberId)) {
#endif

			_destSubscriberId = id;
			set_flag< taglst_t::dest_subscrb_id>();

			/* �������� ���-�������� �������  */
			update_hash();

#ifdef CBL_CHECK_KEYID_VALUE_NO_REASSIGN
			}
		else {
			THROW_EXC_FWD(nullptr);
			}
		}
#endif
	}

const subscriber_node_id_t& address_descriptor_t::destination_subscriber_id()const  noexcept {
	return _destSubscriberId;
	}

void address_descriptor_t::set_source_subscriber_id( const subscriber_node_id_t & id ) {
#ifdef CBL_CHECK_KEYID_VALUE_NO_REASSIGN
	if (!crm::is_null(id)) {
		if (crm::is_null(_srcSubscriberId)) {
#endif

			_srcSubscriberId = id;
			set_flag< taglst_t::src_subscrb_id>();

			/* �������� ���-�������� �������  */
			update_hash();


#ifdef CBL_CHECK_KEYID_VALUE_NO_REASSIGN
			}
		else {
			THROW_EXC_FWD(nullptr);
			}
		}
#endif
	}

void address_descriptor_t::set_spin_tag( const message_spin_tag_type& id ){

#ifdef CBL_CHECK_KEYID_VALUE_NO_REASSIGN
	if (!crm::is_null(id)) {
		if (crm::is_null(_spinTag)) {
#endif

			_spinTag = id;
			set_flag< taglst_t::spin_tag>();

			/* �������� ���-�������� �������  */
			update_hash();

#ifdef CBL_CHECK_KEYID_VALUE_NO_REASSIGN
			}
		else {
			FATAL_ERROR_FWD(nullptr);
			}
		}
#endif
	}

const message_spin_tag_type& address_descriptor_t::spin_tag()const noexcept{
	return _spinTag;
	}

void address_descriptor_t::set_from( const rtl_roadmap_link_t & lnk ) {
	set_destination_id( lnk.target_id() );
	set_source_id( lnk.source_id() );
	set_session( lnk.session() );
	}

const subscriber_node_id_t& address_descriptor_t::source_subscriber_id()const  noexcept {
	return _srcSubscriberId;
	}

bool address_descriptor_t::is_sendrecv_request()const  noexcept {
	return !crm::is_null( source_subscriber_id() );
	}

bool address_descriptor_t::is_sendrecv_response()const noexcept {
	return !crm::is_null(destination_subscriber_id());
	}

bool address_descriptor_t::is_sendrecv_cycle()const noexcept {
	return is_sendrecv_request() || is_sendrecv_response();
	}


std::size_t address_descriptor_t::get_serialized_size()const noexcept{

	std::size_t accm = 0;

	accm += crm::srlz::write_size_v<decltype( _flags ), serialize_options>;
	accm += crm::srlz::object_trait_serialized_size( _hash, serialize_options{});

	if( has_flag<taglst_t::id>() )
		accm += crm::srlz::object_trait_serialized_size( _id, serialize_options{});

	if( has_flag< taglst_t::dest_id>() )
		accm += crm::srlz::object_trait_serialized_size( _destId, serialize_options{});

	if( has_flag< taglst_t::src_id>() )
		accm += crm::srlz::object_trait_serialized_size( _srcId, serialize_options{});

	if( has_flag< taglst_t::session>() )
		accm += crm::srlz::object_trait_serialized_size( _session, serialize_options{});

	if( has_flag< taglst_t::dest_subscrb_id>() )
		accm += crm::srlz::object_trait_serialized_size( _destSubscriberId, serialize_options{});

	if( has_flag< taglst_t::src_subscrb_id>() )
		accm += crm::srlz::object_trait_serialized_size( _srcSubscriberId, serialize_options{});

	if( has_flag< taglst_t::spin_tag>() )
		accm += crm::srlz::object_trait_serialized_size( _spinTag, serialize_options{});

	return accm;
	}


void address_descriptor_t::update_hash()noexcept {
	_hash = address_hash_t();

	apply_at_hash( _hash, _id );
	apply_at_hash( _hash, _destId );
	apply_at_hash( _hash, _srcId );
	apply_at_hash( _hash, _session.object_hash() );
	apply_at_hash( _hash, _destSubscriberId );
	apply_at_hash( _hash, _srcSubscriberId );
	apply_at_hash( _hash, _spinTag );
	apply_at_hash( _hash, (int)level() );
	}

void address_descriptor_t::set_echo(address_descriptor_t & a)const noexcept {
	a.set_destination_id(source_id());
	a.set_source_id(destination_id());
	a.set_session(session());
	a.set_destination_subscriber_id(source_subscriber_id());
	a.set_spin_tag(spin_tag());
	a.set_level(level());
	}


std::string address_descriptor_t::to_string()const  noexcept {
	std::string sb;
	sb += source_id().to_str() + ":";
	sb += destination_id().to_str() + ":";
	sb += source_subscriber_id().to_str() + ":";
	sb += destination_subscriber_id().to_str() + ":";
	sb += session().to_str() + ":";
	sb += cvt2string( level() );

	return sb;
	}

bool address_descriptor_t::is_null()const  noexcept {
	return (*this) == null;
	}

bool address_descriptor_t::equals_fields( const address_descriptor_t& r )const noexcept {
	return id() == r.id()
		&& destination_id() == r.destination_id()
		&& source_id() == r.source_id()
		&& session() == r.session()
		&& destination_subscriber_id() == r.destination_subscriber_id()
		&& source_subscriber_id() == r.source_subscriber_id()
		&& spin_tag() == r.spin_tag()
		&& level() == r.level();
	}

bool address_descriptor_t::less_fields( const address_descriptor_t& r )const noexcept {
	if( id() < r.id() )
		return true;
	else if( id() > r.id() )
		return false;
	else if( destination_id() < r.destination_id() )
		return true;
	else if( destination_id() > r.destination_id() )
		return false;
	else if( source_id() < r.source_id() )
		return true;
	else if( source_id() > r.source_id() )
		return false;
	else if( session() < r.session() )
		return true;
	else if( session() > r.session() )
		return false;
	else if( destination_subscriber_id() < r.destination_subscriber_id() )
		return true;
	else if( destination_subscriber_id() > r.destination_subscriber_id() )
		return false;
	else if( source_subscriber_id() < r.source_subscriber_id() )
		return true;
	else if( source_subscriber_id() > r.source_subscriber_id() )
		return false;
	else if( spin_tag() < r.spin_tag() )
		return true;
	else if( spin_tag() > r.spin_tag() )
		return false;
	else {
		return level() < r.level();
		}
	}

bool address_descriptor_t::great_fields( const address_descriptor_t& r )const noexcept {
	if( id() > r.id() )
		return true;
	else if( id() < r.id() )
		return false;
	else if( destination_id() > r.destination_id() )
		return true;
	else if( destination_id() < r.destination_id() )
		return false;
	else if( source_id() > r.source_id() )
		return true;
	else if( source_id() < r.source_id() )
		return false;
	else if( session() > r.session() )
		return true;
	else if( session() < r.session() )
		return false;
	else  if( destination_subscriber_id() > r.destination_subscriber_id() )
		return true;
	else if( destination_subscriber_id() < r.destination_subscriber_id() )
		return false;
	else if( source_subscriber_id() > r.source_subscriber_id() )
		return true;
	else if( source_subscriber_id() < r.source_subscriber_id() )
		return false;
	else  if( spin_tag() > r.spin_tag() )
		return true;
	else if( spin_tag() < r.spin_tag() )
		return false;
	else{
		return level() > r.level();
		}
	}

bool address_descriptor_t::great_equals_fields( const address_descriptor_t& r )const noexcept {
	if( id() > r.id() )
		return true;
	else if( id() < r.id() )
		return false;
	else if( destination_id() > r.destination_id() )
		return true;
	else if( destination_id() < r.destination_id() )
		return false;
	else if( source_id() > r.source_id() )
		return true;
	else if( source_id() < r.source_id() )
		return false;
	else if( session() > r.session() )
		return true;
	else if( session() < r.session() )
		return false;
	else  if( destination_subscriber_id() > r.destination_subscriber_id() )
		return true;
	else if( destination_subscriber_id() < r.destination_subscriber_id() )
		return false;
	else  if( source_subscriber_id() > r.source_subscriber_id() )
		return true;
	else if( source_subscriber_id() < r.source_subscriber_id() )
		return false;
	else  if( spin_tag() > r.spin_tag() )
		return true;
	else if( spin_tag() < r.spin_tag() )
		return false;
	else {
		return level() >= r.level();
		}
	}


void crm::apply_at_hash( address_hash_t &to, const address_descriptor_t & line )noexcept {
	apply_at_hash( to, line.object_hash() );
	}

bool crm::is_sendrecv_request(const address_descriptor_t & ad) noexcept {
	return ad.is_sendrecv_request();
	}

bool crm::is_sendrecv_response(const address_descriptor_t& ad)noexcept {
	return ad.is_sendrecv_response();
	}

bool crm::is_sendrecv_cycle(const address_descriptor_t& ad)noexcept {
	return ad.is_sendrecv_cycle();
	}

bool crm::is_sndrcv_spin_tagged_invoke(const address_descriptor_t& ad)noexcept {
	return is_sendrecv_cycle(ad) && crm::detail::check_sndrcv_key_spin_tag(ad.spin_tag());
	}

bool crm::check_sndrcv_key_spin_tag(const address_descriptor_t& ad)noexcept {
	return is_sndrcv_spin_tagged_invoke(ad) || !is_sendrecv_cycle(ad);
	}




