#include "../../internal.h"
#include "../../channel_terms/internal_terms.h"
#include "../../notifications/exceptions/excdrv.h"
#include "../stream_wait_key.h"


using namespace crm;
using namespace crm::detail;


static_assert(srlz::detail::is_i_srlzd_prefixed_v<rt1_streams_table_wait_key_t>, __FILE_LINE__);


const rt1_streams_table_wait_key_t rt1_streams_table_wait_key_t::null;

void rt1_streams_table_wait_key_t::update_hash()noexcept{
	_hash = address_hash_t::null;

	apply_at_hash( _hash, _rdml.object_hash() );
	}

bool rt1_streams_table_wait_key_t::equals_fields( const rt1_streams_table_wait_key_t& r )const noexcept{
	return rdml() == r.rdml() && rt0() == r.rt0() && table() == r.table();
	}

bool rt1_streams_table_wait_key_t::less_fields( const rt1_streams_table_wait_key_t& r )const noexcept{
	if( rdml() < r.rdml() )
		return true;
	else if( rdml() > r.rdml() )
		return false;
	else{
		if( rt0() < r.rt0() ){
			return true;
			}
		else if( rt0() > r.rt0() ){
			return false;
			}
		else{
			return table() < r.table();
			}
		}
	}

bool rt1_streams_table_wait_key_t::great_fields( const rt1_streams_table_wait_key_t& r )const noexcept{
	if( rdml() > r.rdml() )
		return true;
	else if( rdml() < r.rdml() )
		return false;
	else{
		if( rt0() > r.rt0() ){
			return true;
			}
		else if( rt0() < r.rt0() ){
			return false;
			}
		else{
			return table() > r.table();
			}
		}
	}

bool rt1_streams_table_wait_key_t::great_equals_fields( const rt1_streams_table_wait_key_t& r )const noexcept{
	if( rdml() > r.rdml() )
		return true;
	else if( rdml() < r.rdml() )
		return false;
	else{
		if( rt0() > r.rt0() ){
			return true;
			}
		else if( rt0() < r.rt0() ){
			return false;
			}
		else{
			return table() >= r.table();
			}
		}
	}

rt1_streams_table_wait_key_t::rt1_streams_table_wait_key_t()noexcept{}

rt1_streams_table_wait_key_t::rt1_streams_table_wait_key_t( const rt0_node_rdm_t & rt0,
	const rtl_roadmap_link_t &rdml,
	rtl_table_t t )noexcept
	: _rdml( rdml )
	, _rt0( rt0 )
	, _t( t ){

	update_hash();
	}

const rtl_roadmap_link_t& rt1_streams_table_wait_key_t::rdml()const noexcept{
	return _rdml;
	}

const rt0_node_rdm_t& rt1_streams_table_wait_key_t::rt0()const noexcept{
	return _rt0;
	}

rtl_table_t rt1_streams_table_wait_key_t::table()const noexcept{
	return _t;
	}

std::string rt1_streams_table_wait_key_t::to_string()const noexcept {
	return _rt0.to_str()
		+ ":" + _rdml.to_str()
		+ ":" + cvt2string(_t)
		+ ":" + _hash.to_str();
	}


const rt1_streams_table_wait_key_t::hash_t& rt1_streams_table_wait_key_t::object_hash()const noexcept{
	return _hash;
	}

bool rt1_streams_table_wait_key_t::is_null()const noexcept{
	return (*this) == null;
	}

std::size_t rt1_streams_table_wait_key_t::get_serialized_size()const noexcept{
	return 	srlz::object_trait_serialized_size( _rdml )
		+ srlz::object_trait_serialized_size( _rt0 )
		+ srlz::object_trait_serialized_size( _hash )
		+ srlz::object_trait_serialized_size( _t );
	}
