#include "../../internal.h"
#include "../../channel_terms/internal_terms.h"
#include "../session_desc.h"


using namespace crm;
using namespace crm::detail;


static_assert(srlz::detail::is_i_srlzd_prefixed_v<session_description_t>, __FILE_LINE__);
static_assert(srlz::is_serializable_v<session_description_t>, __FILE_LINE__);


const session_description_t session_description_t::null;

session_description_t::session_description_t() noexcept {}

session_description_t::session_description_t( const session_id_t&id, std::string_view name )noexcept {
	set_id( id );
	set_name( name );
	}

void session_description_t::update_hash() noexcept {
	_hash = address_hash_t();

	apply_at_hash( _hash, _id );
	apply_at_hash( _hash, _name );
	apply_at_hash( _hash, _flags );
	}

void session_description_t::__set_flag( const taglst_t attribute ) {
	CBL_VERIFY( taglst_t::null != attribute );

	_flags = (flags_utype_t)_flags | (flags_utype_t)attribute;

	update_hash();
	}

session_description_t::flags_utype_t session_description_t::__get_uflags( taglst_t attribute )const noexcept{
	CBL_VERIFY( taglst_t::null != attribute );

	const auto uattr = (flags_utype_t)attribute;
	const auto result = uattr & (flags_utype_t)_flags;
	return result;
	}

bool session_description_t::__has_flag( taglst_t attribute )const noexcept{
	return 0 != __get_uflags( attribute );
	}

const session_description_t::hash_t& session_description_t::object_hash()const noexcept {
	return _hash;
	}

bool session_description_t::is_null()const noexcept {
	return (*this) == null;
	}

void session_description_t::set_id( const session_id_t & id ) {
	if( !id.is_null() ) {

		_id = id;
		set_flag<taglst_t::id>();

		/* ���������� �������� ���� */
		update_hash();
		}
	}

const session_id_t& session_description_t::id()const noexcept {
	return _id;
	}

void session_description_t::set_name( std::string_view name ) {
	if( !name.empty() ) {
		if (name.size() > max_name_size()) {
			SERIALIZE_OVERFLOW_THROW_EXC_FWD(nullptr);
			}

		_name = name;
		set_flag< taglst_t::name>();

		/* ���������� �������� ���� */
		update_hash();
		}
	}

const std::string& session_description_t::name()const noexcept {
	return _name;
	}

std::string session_description_t::to_str()const noexcept {
	std::string s;
	s += "sd=[" + id().to_str() + ":" + name() + "]";
	return std::move( s );
	}

static_assert(srlz::detail::is_fixed_size_serialized_v<decltype(session_description_t::_flags)>, __FILE_LINE__);
static_assert(srlz::write_size_v<decltype(session_description_t::_flags)> < 10, __FILE_LINE__);

size_t session_description_t::get_serialized_size()const noexcept{

	size_t accm = 0;

	/* ����� */
	accm += crm::srlz::write_size_v<decltype(_flags), serialize_options >;
	/* ��� */
	accm += crm::srlz::object_trait_serialized_size( _hash, serialize_options{});

	if( has_flag<taglst_t::id>() )
		accm += crm::srlz::object_trait_serialized_size( _id, serialize_options{});

	if( has_flag< taglst_t::name>() )
		accm += crm::srlz::object_trait_serialized_size( _name, srlz::detail::option_max_serialized_size<max_name_size()>{});

	return accm;
	}


bool session_description_t::equals_fields( const session_description_t& r )const noexcept {
	return id() == r.id()
		&& name() == r.name();
	}

bool session_description_t::less_fields( const session_description_t& r )const noexcept {
	if( id() < r.id() )
		return true;
	else if( id() > r.id() )
		return false;
	else 
		return name() < r.name();
	}

bool session_description_t::great_fields( const session_description_t& r )const noexcept {
	if( id() > r.id() )
		return true;
	else if( id() < r.id() )
		return false;
	else
		return name() > r.name();
	}

bool session_description_t::great_equals_fields( const session_description_t& r )const noexcept {
	if( id() > r.id() )
		return true;
	else if( id() < r.id() )
		return false;
	else
		return name() >= r.name();
	}

