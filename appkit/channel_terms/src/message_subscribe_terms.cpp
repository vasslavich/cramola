#include "../../internal.h"
#include "../../context/context.h"
#include "../message_subscribe_terms.h"
#include "../subscr_tbl.h"
#include "../../logging/keycounter.h"


using namespace crm;
using namespace crm::detail;



message_subscribe_assist_t::subscribe_message_pointer message_subscribe_assist_t::make_handler(trace_tag&& tag_,
	subscribe_message_handler && h,
	bool once_,
	std::weak_ptr<distributed_ctx_t> ctx_,
	async_space_t scx )noexcept{

	if( once_ ){
		return subscribe_handler_unique( std::move(tag_), std::move( h ).release_as_unique(), once_, ctx_, h._invokeAs, scx );
		}
	else{
		return subscribe_handler_shared( std::move(tag_), std::move( h ).release_as_shared(), once_, ctx_, h._invokeAs, scx );
		}
	}




message_subscribe_assist_t::message_addressed_link_t::message_addressed_link_t()noexcept{}

message_subscribe_assist_t::message_addressed_link_t::message_addressed_link_t(trace_tag&& tag_,
	subscribe_message_handler && h,
	bool once_,
	std::weak_ptr<distributed_ctx_t> ctx_,
	async_space_t scx,
	bool abandonedInvoke_,
	uint8_t tagz_ )noexcept
	: hndl( make_handler( std::move(tag_), std::move( h ), once_, ctx_, scx ) )
	, tagz( tagz_ )
	, _ctx( ctx_ )
	, abandonedInvoke( abandonedInvoke_ ){

	CBL_VERIFY( scx != async_space_t::undef );
	}


void message_subscribe_assist_t::message_addressed_link_t::operator()( std::unique_ptr<i_iol_ihandler_t> && o, invoke_context_trait ic){
#ifdef CBL_MPF_MESSAGE_TRACEWAY_KEYS_BY_ID
	auto ktid = trait_at_keytrace_set( (*o) );
#endif
	CBL_MPF_KEYTRACE_SET( (*o), 73 );

	hndl( std::move( o ), ic );

	CBL_MPF_KEYTRACE_SET( ktid, 89 );
	}

void message_subscribe_assist_t::message_addressed_link_t::operator()( std::unique_ptr<dcf_exception_t> && e, invoke_context_trait ic) noexcept{
	hndl( std::move( e ), ic );
	}

void message_subscribe_assist_t::message_addressed_link_t::reset()noexcept{
	hndl.reset();
	}

message_subscribe_assist_t::message_addressed_link_t::operator bool()const noexcept{
	return hndl;
	}

bool message_subscribe_assist_t::message_addressed_link_t::once()const noexcept{
	return hndl.once();
	}


bool crm::detail::operator==( const message_subscribe_assist_t::line_num_t& l, const message_subscribe_assist_t::line_num_t& r )noexcept{
	return l.id() == r.id() && l.level() == r.level();
	}

bool crm::detail::operator!=( const message_subscribe_assist_t::line_num_t& l, const message_subscribe_assist_t::line_num_t& r )noexcept{
	return !(l == r);
	}

bool crm::detail::operator>=( const message_subscribe_assist_t::line_num_t& l, const message_subscribe_assist_t::line_num_t& r )noexcept{
	if( l.id() < r.id())
		return false;
	else if( l.id() > r.id())
		return true;
	else{
		return l.level() >= r.level();
		}
	}

bool crm::detail::operator<( const message_subscribe_assist_t::line_num_t& l, const message_subscribe_assist_t::line_num_t& r )noexcept{
	if( l.id() > r.id())
		return false;
	else if( l.id() < r.id())
		return true;
	else{
		return l.level() < r.level();
		}
	}





message_subscribe_assist_t::handler_t::handler_t( handler_t && o )noexcept
	: lineId( std::move( o.lineId ) )
	, _wtbl( std::move( o._wtbl ) ){}

message_subscribe_assist_t::handler_t& message_subscribe_assist_t::handler_t::operator=( handler_t && o )noexcept{
	if( this != std::addressof( o ) ){
		lineId = std::move( o.lineId );
		_wtbl = std::move( o._wtbl );
		}

	return (*this);
	}

message_subscribe_assist_t::handler_t::handler_t( line_num_t && lnId, std::weak_ptr<subscribe_multitable_t> wtbl_ )
	: lineId( std::move( lnId ) )
	, _wtbl( wtbl_ ){
	
	if (auto ptbl = _wtbl.lock()) {
		ptbl->regline((*this));
		}
	else {
		THROW_EXC_FWD(nullptr);
		}
	}

message_subscribe_assist_t::handler_t::handler_t()noexcept{}

std::unique_ptr<i_iol_ihandler_t> message_subscribe_assist_t::handler_t::check_address(
	std::unique_ptr<i_iol_ihandler_t> && obj, 
	invoke_context_trait ic){

	if( auto stbl = _wtbl.lock() ){
		return stbl->check_address( (*this), std::move( obj ), ic );
		}
	else{
		return std::move( obj );
		}
	}

void message_subscribe_assist_t::handler_t::addressed_exception( const _address_hndl_t &addr,
	std::unique_ptr<dcf_exception_t>&& e, 
	invoke_context_trait ic)noexcept{

	if( auto stbl = _wtbl.lock() ){
		return stbl->addressed_exception( (*this), addr, std::move( e ), ic );
		}
	}

subscribe_invoker_result message_subscribe_assist_t::handler_t::subscribe_set_handler( const _address_hndl_t & id,
	async_space_t scx,
	subscribe_options opt,
	subscribe_message_handler && handler ){

	if( auto stbl = _wtbl.lock() ){
		return stbl->subscribe( (*this), scx, id, opt, std::move( handler ) );
		}
	else{
		THROW_MPF_EXC_FWD(nullptr);
		}
	}

void message_subscribe_assist_t::handler_t::subscribe_unset_handler( const _address_hndl_t & id )noexcept{
	if( auto stbl = _wtbl.lock() ){
		CHECK_NO_EXCEPTION( stbl->unsubscribe( (*this), id ) );
		}
	}

void message_subscribe_assist_t::handler_t::close()noexcept{
	if( auto stbl = _wtbl.lock() ){
		CHECK_NO_EXCEPTION( stbl->close( (*this) ) );
		}
	}

message_subscribe_assist_t::handler_t::~handler_t(){
	CHECK_NO_EXCEPTION( close() );
	}


message_subscribe_assist_t::line_num_t::line_num_t()noexcept{}

message_subscribe_assist_t::line_num_t::line_num_t(const id_t& id_, rtl_level_t l)noexcept
	: _id(id_)
	, _level(l){}

const message_subscribe_assist_t::line_num_t::id_t& message_subscribe_assist_t::line_num_t::id()const noexcept {
	return _id;
	}

rtl_level_t message_subscribe_assist_t::line_num_t::level()const noexcept {
	return _level;
	}

address_hash_t message_subscribe_assist_t::line_num_t::object_hash()const noexcept {
	address_hash_t h;
	apply_at_hash(h, id());
	apply_at_hash(h, level());

	return h;
	}
