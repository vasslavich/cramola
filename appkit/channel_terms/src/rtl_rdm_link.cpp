#include "../../internal.h"
#include "../rtl_rdm_link.h"


using namespace crm;
using namespace crm::detail;


static_assert(!srlz::detail::is_serialized_trait_v<rtl_roadmap_link_t>, __FILE_LINE__);
static_assert(srlz::detail::is_i_srlzd_prefixed_v<rtl_roadmap_link_t>, __FILE_LINE__);
static_assert(srlz::is_serializable_v<rtl_roadmap_link_t>, __FILE_LINE__);

namespace {
auto check_validate() {
	return srlz::object_trait_serialized_size(identity_descriptor_t{});
	}
static_assert(std::is_same_v < size_t, decltype(check_validate())>, __FILE_LINE__);
}

const rtl_roadmap_link_t rtl_roadmap_link_t::null;

bool rtl_roadmap_link_t::is_null() const noexcept{
	return (*this) == null;
	}

rtl_roadmap_link_t::rtl_roadmap_link_t()noexcept{}

rtl_roadmap_link_t::rtl_roadmap_link_t( const identity_descriptor_t &targetId,
	const identity_descriptor_t & sourceId,
	const session_description_t & session )noexcept
	: _targetId( targetId )
	, _sourceId(sourceId)
	, _session( session ){

	apply_at_hash( _hash, _targetId );
	apply_at_hash( _hash, _sourceId );
	apply_at_hash( _hash, _session.object_hash() );
	}

const rtl_roadmap_link_t::hash_t& rtl_roadmap_link_t::object_hash()const noexcept{
	return _hash;
	}

const session_description_t& rtl_roadmap_link_t::session()const noexcept{
	return _session;
	}

const identity_descriptor_t& rtl_roadmap_link_t::source_id()const noexcept{
	return _sourceId;
	}

const identity_descriptor_t& rtl_roadmap_link_t::target_id()const noexcept{
	return _targetId;
	}

std::string rtl_roadmap_link_t::to_str()const noexcept{
	std::ostringstream s;
	s << "[s=" << _sourceId.to_str()
		<< ":t=" << _targetId.to_str() 
		<< ":sd=" << _session.to_str() << "]";
	return s.str();
	}

size_t rtl_roadmap_link_t::get_serialized_size()const noexcept{
	return srlz::object_trait_serialized_size( _sourceId )
		+ srlz::object_trait_serialized_size( _targetId )
		+ srlz::object_trait_serialized_size( _session )
		+ srlz::object_trait_serialized_size( _hash );
	}

bool rtl_roadmap_link_t::equals_fields( const rtl_roadmap_link_t& r )const noexcept{

	return source_id() == r.source_id()
		&& target_id() == r.target_id()
		&& session() == r.session();
	}

bool rtl_roadmap_link_t::less_fields( const rtl_roadmap_link_t& r )const noexcept{

	if( source_id() < r.source_id() )
		return true;
	else if( source_id() > r.source_id() )
		return false;
	else if( target_id() < r.target_id() )
		return true;
	else if( target_id() > r.target_id() )
		return false;
	else
		return session() < r.session();
	}

bool rtl_roadmap_link_t::great_fields( const rtl_roadmap_link_t& r )const noexcept{

	if( source_id() > r.source_id() )
		return true;
	else if( source_id() < r.source_id() )
		return false;
	else if( target_id() > r.target_id() )
		return true;
	else if( target_id() < r.target_id() )
		return false;
	else
		return session() > r.session();
	}

bool rtl_roadmap_link_t::great_equals_fields( const rtl_roadmap_link_t& r )const noexcept{

	if( source_id() > r.source_id() )
		return true;
	else if( source_id() < r.source_id() )
		return false;
	else if( target_id() > r.target_id() )
		return true;
	else if( target_id() < r.target_id() )
		return false;
	else
		return session() >= r.session();
	}






