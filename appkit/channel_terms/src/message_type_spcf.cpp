#include "../../internal.h"
#include "../message_type_spcf.h"
#include "../../utilities/convert/cvt.h"


using namespace crm;
using namespace crm::detail;



bool crm::operator==(const iol_type_spcf_t &l, const iol_type_spcf_t &r)  noexcept {
	return l._field == r._field;
	}

bool crm::operator !=(const iol_type_spcf_t &l, const iol_type_spcf_t &r) noexcept {
	return !(l == r);
	}




/*========================================================================
iol_type_map_t
==========================================================================*/



/*========================================================================
iol_type_spcf_t
==========================================================================*/


iol_type_spcf_t::iol_type_spcf_t()noexcept
	: _field( undefined ) {}

iol_type_spcf_t::iol_type_spcf_t( field_type_t field )noexcept
	: _field( field ) {}

iol_type_spcf_t::iol_type_spcf_t( const iol_type_spcf_t &o )noexcept
	: _field( o._field ) {}

iol_type_spcf_t iol_type_spcf_t::create( field_type_t value )noexcept{
	iol_type_spcf_t item;
	item.set( value );
	return item;
	}

iol_type_spcf_t& iol_type_spcf_t::operator = (const iol_type_spcf_t &o)noexcept{
	_field = o._field;
	return (*this);
	}

void iol_type_spcf_t::_set_attribute() noexcept {}

iol_type_spcf_t::field_type_t iol_type_spcf_t::get()const  noexcept {
	static_assert(!std::is_signed<field_type_t>::value, __FILE__);

	return _field;
	}

iol_type_spcf_t::field_type_t iol_type_spcf_t::get( field_type_t dscrp, const iol_type_attributes_t index )  noexcept {
	static_assert(!std::is_signed<field_type_t>::value, __FILE__);
	auto uval = (dscrp >> iol_type_map_t::instance().collate(index).offsetBits) & iol_type_map_t::instance().collate(index).maskBits;
	return uval;
	}

iol_type_spcf_t::field_type_t iol_type_spcf_t::get( const iol_type_attributes_t index )const  noexcept {
	static_assert(std::is_unsigned<decltype(get(_field, index))>::value, "bad type");

	return get( _field, index );
	}

iol_type_spcf_t::field_type_t iol_type_spcf_t::set(
	field_type_t dest, const iol_type_attributes_t index, field_type_t value ) noexcept {

	// ����� ��������� ��������������� ��� � ����
	const auto zeroedIndexMask = ~(
		((field_type_t)(-1) >> ((sizeof( field_type_t ) * 8) - iol_type_map_t::instance().collate(index).lengthBits))
		<< iol_type_map_t::instance().collate(index).offsetBits);

	// �������� ��������� � ���������, ���������� � ������� �����
	const auto maskedValue =
		(((field_type_t)(-1) >> ((sizeof( field_type_t ) * 8) - iol_type_map_t::instance().collate(index).lengthBits)) & value)
		<< iol_type_map_t::instance().collate(index).offsetBits;

	return (dest & zeroedIndexMask) | maskedValue;
	}

void iol_type_spcf_t::set( field_type_t value )  noexcept {
	_field = value;
	}

bool iol_type_spcf_t::validate_attribute_value( iol_type_attributes_t index, field_type_t value ) noexcept {
	if( iol_type_map_t::instance().collate(index).max_value() < value ) {
		return false;
		}
	else {
		switch( index ) {
			case iol_type_attributes_t::type:
				return true;

			case iol_type_attributes_t::code:
				return true;

			case iol_type_attributes_t::report:
				return true;

			case iol_type_attributes_t::length:
				return value <= (field_type_t)iol_length_spcf_t::eof_marked_;

			case iol_type_attributes_t::type_id:
				return true;

			case iol_type_attributes_t::timeline:
				return true;

			case iol_type_attributes_t::reserved:
				return true;

			case iol_type_attributes_t::crc:
				return true;

			default:
				FATAL_ERROR_FWD(nullptr);
			}
		}
	}

void iol_type_spcf_t::set( const iol_type_attributes_t index, field_type_t value ) noexcept{
	if( !validate_attribute_value( index, value ) ) {
		FATAL_ERROR_FWD(nullptr);
		}

	_field = set( _field, index, value );
	}

iol_types_t iol_type_spcf_t::type()const noexcept {
	const auto intval = get( iol_type_attributes_t::type );
	static_assert(std::is_unsigned<decltype(intval)>::value, "bad type");

	if( intval >= ((std::underlying_type<iol_types_t>::type)iol_types_t::st_user_boundary) )
		return iol_types_t::st_user_boundary;

	return (iol_types_t)intval;
	}

bool iol_type_spcf_t::is_user_type()const noexcept {
	const auto intval = get( iol_type_attributes_t::type );
	static_assert(std::is_unsigned<decltype(intval)>::value, "bad type");

	return(intval >= ((std::underlying_type<iol_types_t>::type)iol_types_t::st_user_boundary));
	}

iol_type_spcf_t::field_type_t iol_type_spcf_t::utype()const noexcept {
	return get( iol_type_attributes_t::type );
	}

iol_type_spcf_t::field_type_t iol_type_spcf_t::utypeid()const noexcept {
	return utypeid( utype(), ucode() );
	}

iol_types_ibase_t iol_type_spcf_t::priority_value()const noexcept {
	return ~utype();
	}

bool iol_type_spcf_t::fl_has( iol_type_attributes_t attr )const noexcept {
	const auto intval = get( attr );

	CBL_VERIFY( undefined == 0 );
	return 0 != intval;
	}

bool iol_type_spcf_t::fl_typeid()const noexcept {
	return fl_has( iol_type_attributes_t::type_id );
	}

bool iol_type_spcf_t::fl_timeline()const noexcept {
	return fl_has( iol_type_attributes_t::timeline );
	}

bool iol_type_spcf_t::fl_report()const noexcept {
	return fl_has( iol_type_attributes_t::report );
	}

bool iol_type_spcf_t::fl_use_crc()const noexcept {
	return fl_has( iol_type_attributes_t::crc );
}

void iol_type_spcf_t::setf_use_crc()noexcept {
	set( iol_type_attributes_t::crc, 1 );
}

void iol_type_spcf_t::setf_report()noexcept {
	set(iol_type_attributes_t::report, 1);
	}

void iol_type_spcf_t::setf_length()noexcept {
	set(iol_type_attributes_t::length, 1);
	}


iol_type_spcf_t::field_type_t iol_type_spcf_t::ucode()const noexcept {
	return get( iol_type_attributes_t::code );
	}

iol_length_spcf_t iol_type_spcf_t::length()const noexcept {
	const auto ival = get( iol_type_attributes_t::length );
	if( ival <= (field_type_t)iol_length_spcf_t::eof_marked_ ) {
		return (iol_length_spcf_t)ival;
		}
	else {
		FATAL_ERROR_FWD(nullptr);
		}
	}

iol_type_spcf_t::field_type_t iol_type_spcf_t::reserved()const noexcept {
	return get( iol_type_attributes_t::reserved );
	}

iol_type_spcf_t::field_type_t iol_type_spcf_t::maxval( iol_type_attributes_t name ) {
	if( iol_type_map_t::maxval_range >= iol_type_map_t::instance().collate(name).lengthBits ) {
		return (field_type_t)(-1) >> (iol_type_map_t::maxval_range - iol_type_map_t::instance().collate(name).lengthBits);
		}
	else {
		THROW_MPF_EXC_FWD(nullptr);
		}
	}

iol_type_spcf_t::field_type_t iol_type_spcf_t::utypeid( field_type_t utype, field_type_t ucode ) noexcept {
	if(utype >= maxval(iol_type_attributes_t::type) || ucode >= maxval(iol_type_attributes_t::code))
		return undefined;
	else {
		return (utype << iol_type_map_t::instance().collate(iol_type_attributes_t::type).offsetBits)
			| (ucode << iol_type_map_t::instance().collate(iol_type_attributes_t::code).offsetBits);
		}
	}

iol_type_attributes_t iol_type_spcf_t::get_name( const attrb_pair_t &attr ) noexcept {
	return std::get<0>( attr );
	}

iol_type_spcf_t::field_type_t iol_type_spcf_t::get_value( const attrb_pair_t &attr )noexcept {
	return std::get<1>( attr );
	}

std::wstring iol_type_spcf_t::print()const {

	std::wostringstream sbuf;

	if( type() == iol_types_t::st_user_boundary ) { sbuf << L"type : user, value = " << utype() << std::endl; }
	else { sbuf << L"type : " << cvt2string( type() ) << std::endl; }

	sbuf << L"code : " << std::hex << ucode() << std::endl;
	sbuf << L"report : " << (fl_report() ? L"yes" : L"no") << std::endl;
	sbuf << L"length flag : " << cvt2string( length() ) << std::endl;
	sbuf << L"reserved : " << std::hex << reserved() << std::endl;

	return std::move( sbuf.str() );
	}

type_identifier_t iol_type_spcf_t::type_id()const noexcept {
	return type_identifier_t::make_set_all(field_type_t{ utypeid(utype(), ucode()) });
	}

std::string iol_type_spcf_t::type_name()const noexcept {
	field_type_t uid( utypeid( utype(), ucode() ) );
	return crm::detail::cvt_to_str( uid );
	}

crm::srlz::typemap_key_t iol_type_spcf_t::typemap_key()const noexcept{
	return (*this);
	}

bool iol_type_spcf_t::priority( const iol_type_spcf_t &l, const iol_type_spcf_t &r ) noexcept {
	return l.priority_value() >= r.priority_value();
	}

size_t iol_type_spcf_t::get_serialized_size()const noexcept {
	return srlz::object_trait_serialized_size(_field);
	}

std::string crm::cvt2string(const iol_type_spcf_t& spc) {
	return std::to_string(spc.utype());
	}
