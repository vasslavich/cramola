#include "../../internal.h"
#include "../../channel_terms/internal_terms.h"
#include "../../notifications/exceptions/excdrv.h"
#include "../source_object_key.h"

using namespace crm;
using namespace crm::detail;



const source_object_key_t source_object_key_t::null;

source_object_key_t::source_object_key_t() noexcept{}

source_object_key_t::source_object_key_t( const rt1_endpoint_t& ck,
	const session_description_t & s,
	const identity_descriptor_t & glid )noexcept
	: _epKey( ck )
	, _glid( glid )
	, _sessionId( s ){

	update_hash();
	}

source_object_key_t source_object_key_t::make( const rt1_endpoint_t & ck,
	const session_description_t & s,
	const identity_descriptor_t & glid )noexcept{

	return source_object_key_t( ck, s, glid );
	}

void source_object_key_t::update_hash() noexcept{
	_hash = address_hash_t();

	apply_at_hash( _hash, _epKey );
	apply_at_hash( _hash, _sessionId );
	apply_at_hash( _hash, _glid );
	}

const source_object_key_t::hash_t& source_object_key_t::object_hash()const noexcept{
	return _hash;
	}

size_t source_object_key_t::get_serialized_size()const noexcept{
	return srlz::object_trait_serialized_size( _epKey )
		+ srlz::object_trait_serialized_size( _glid )
		+ srlz::object_trait_serialized_size( _sessionId )
		+ srlz::object_trait_serialized_size( _hash );
	}

bool source_object_key_t::is_null()const noexcept{
	return (*this) == null;
	}

const rt1_endpoint_t& source_object_key_t::connection_key()const noexcept{
	return _epKey;
	}

const session_description_t& source_object_key_t::session()const noexcept{
	return _sessionId;
	}

const identity_descriptor_t& source_object_key_t::object_id()const noexcept{
	return _glid;
	}

bool source_object_key_t::equals_fields( const source_object_key_t& r )const noexcept{
	return session() == r.session()
		&& object_id() == r.object_id()
		&& connection_key() == r.connection_key();
	}

bool source_object_key_t::less_fields( const source_object_key_t& r )const noexcept{
	if( session() < r.session() )
		return true;
	else if( session() > r.session() )
		return false;
	else if( connection_key() < r.connection_key() )
		return true;
	else if( connection_key() > r.connection_key() )
		return false;
	else
		return object_id() < r.object_id();
	}

bool source_object_key_t::great_fields( const source_object_key_t& r )const noexcept{
	if( session() > r.session() )
		return true;
	else if( session() < r.session() )
		return false;
	else if( connection_key() > r.connection_key() )
		return true;
	else if( connection_key() < r.connection_key() )
		return false;
	else
		return object_id() > r.object_id();
	}

bool source_object_key_t::great_equals_fields( const source_object_key_t& r )const noexcept{
	if( session() > r.session() )
		return true;
	else if( session() < r.session() )
		return false;
	else if( connection_key() > r.connection_key() )
		return true;
	else if( connection_key() < r.connection_key() )
		return false;
	else
		return object_id() >= r.object_id();
	}
