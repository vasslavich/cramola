#include "../../internal.h"
#include "../internal_terms.h"
#include "../i_stream.h"


using namespace crm;
using namespace crm::detail;


static_assert(srlz::detail::is_i_srlzd_prefixed_v< i_rt0_cmd_addlink_t>, __FILE_LINE__);
static_assert(srlz::detail::is_i_srlzd_prefixed_v< i_rt0_cmd_sublink_t>, __FILE_LINE__);
static_assert(srlz::detail::is_i_srlzd_prefixed_v< i_rt0_cmd_sublink_sourcekey_t>, __FILE_LINE__);
static_assert(srlz::detail::is_i_srlzd_prefixed_v< i_rt0_cmd_reqconnection_t>, __FILE_LINE__);
static_assert(srlz::detail::is_i_srlzd_prefixed_v< i_rt0_cmd_outcoming_oppened_t>, __FILE_LINE__);
static_assert(srlz::detail::is_i_srlzd_prefixed_v< i_rt0_exception_t>, __FILE_LINE__);
static_assert(srlz::detail::is_i_srlzd_prefixed_v< i_rt0_cmd_outcoming_closed_t>, __FILE_LINE__);
static_assert(srlz::detail::is_i_srlzd_prefixed_v< i_rt0_invalid_xpeer_t>, __FILE_LINE__);
static_assert(srlz::detail::is_i_srlzd_prefixed_v< i_rt0_cmd_incoming_closed_t>, __FILE_LINE__);
static_assert(srlz::detail::is_i_srlzd_prefixed_v< i_rt0_cmd_sndrcv_result_t>, __FILE_LINE__);
static_assert(srlz::detail::is_i_srlzd_prefixed_v< cmd_unsubscribe_connection_t>, __FILE_LINE__);
static_assert(srlz::detail::is_i_srlzd_prefixed_v< i_rt0_cmd_sndrcv_fault_t>, __FILE_LINE__);
static_assert(srlz::detail::is_i_srlzd_prefixed_v< module_channel_connection_string>, __FILE_LINE__);


i_endpoint_t::~i_endpoint_t(){}
i_rt0_command_t::~i_rt0_command_t(){}
io_x_rt1_base_t::~io_x_rt1_base_t(){}
io_x_irt1_messages_t::~io_x_irt1_messages_t(){}
io_x_irt1_commands_t::~io_x_irt1_commands_t(){}
io_x_ort1_messages_t::~io_x_ort1_messages_t(){}
io_x_ort1_commands_t::~io_x_ort1_commands_t(){}
i_rt1_command_link_t::~i_rt1_command_link_t(){}
io_x_irt1_t::~io_x_irt1_t(){}
io_x_ort1_t::~io_x_ort1_t(){}
io_irt0_x_t::~io_irt0_x_t(){}
io_ort0_x_t::~io_ort0_x_t(){}



i_is_broadcast_command_t::i_is_broadcast_command_t(const rtl_roadmap_event_t& tid)
	:i_command_from_rt02rt1_t(tid) {}



const std::chrono::milliseconds& i_rt0_command_t::wait_timeout()const noexcept{
	return _waitTimeout;
	}

void i_rt0_command_t::set_context(std::weak_ptr< distributed_ctx_t> ctx)noexcept {
	_ctx = ctx;
	}

std::shared_ptr<distributed_ctx_t> i_rt0_command_t::ctx()const noexcept {
	return _ctx.lock();
	}

size_t i_rt0_command_t::get_serialized_size()const noexcept {
	return srlz::object_trait_serialized_size(_waitTimeout);
	}

rtl_roadmap_obj_t crm::detail::make_message(const rt0_node_rdm_t & rt0,
	const i_iol_ohandler_t & message) {

	return rtl_roadmap_obj_t::make(rt0,
		message.source_id(),
		message.destination_id(),
		message.session(),
		rt0.direction());
	}

rtl_roadmap_obj_t crm::detail::make_message(const rt0_node_rdm_t & rt0,
	const i_iol_ihandler_t & message) {

	return rtl_roadmap_obj_t::make(rt0,
		message.destination_id(),
		message.source_id(),
		message.session(),
		rt0.direction());
	}

size_t crm::detail::get_packet_idx( const message_frame_unit_t & b )noexcept{
	static_assert(noexcept(get_packet_idx(b.unit().address())), __FILE_LINE__);
	return get_packet_idx( b.unit().address() );
	}

size_t crm::detail::get_packet_idx( const i_iol_handler_t & m )noexcept{
	static_assert(noexcept(get_packet_idx(m.address())), __FILE_LINE__);
	return get_packet_idx( m.address() );
	}

size_t crm::detail::get_packet_idx( const address_descriptor_t & a )noexcept{
	return get_hash_std(a.destination_id().id());
	}


channel_info_t::channel_info_t()noexcept{}

channel_info_t::channel_info_t( const channel_identity_t & id )
	: _id( id ){}

void channel_info_t::set_id( const channel_identity_t & id ){
	_id = id;
	}

const channel_identity_t& channel_info_t::id()const noexcept{
	return _id;
	}

bool crm::operator==( const channel_info_t& l, const channel_info_t& r )noexcept{
	static_assert(noexcept(l.id() == r.id()), __FILE_LINE__);
	return l.id() == r.id();
	}

bool crm::operator !=( const channel_info_t& l, const channel_info_t & r )noexcept{
	static_assert(noexcept(l != r), __FILE_LINE__);
	return !(l == r);
	}

bool crm::is_userspace_event( const std::unique_ptr<i_event_t> & ev )noexcept{
	static_assert(noexcept(dynamic_cast<const i_userspace_event_t*>(ev.get())), __FILE_LINE__);
	return nullptr != dynamic_cast<const i_userspace_event_t*>(ev.get());
	}

bool crm::is_syspace_event( const std::unique_ptr<i_event_t> & ev )noexcept{
	static_assert(noexcept(dynamic_cast<const i_syspace_event_t*>(ev.get())), __FILE_LINE__);
	return nullptr != dynamic_cast<const i_syspace_event_t*>(ev.get());
	}



initialize_result_desc_t::initialize_result_desc_t(){}

initialize_result_desc_t::initialize_result_desc_t( std::unique_ptr<dcf_exception_t> && exc )noexcept
	: exception( std::move( exc ) ){}

initialize_result_desc_t::initialize_result_desc_t( crm::datagram_t && value )
	: responseValue( std::move( value ) ){}

initialize_result_desc_t::initialize_result_desc_t( initialize_result_desc_t  && o )
	: exception( std::move( o.exception ) )
	, responseValue( std::move( o.responseValue ) ){}

initialize_result_desc_t& initialize_result_desc_t::operator = ( initialize_result_desc_t && o ){
	if (this != std::addressof(o)) {
		exception = std::move(o.exception);
		responseValue = std::move(o.responseValue);
		}

	return (*this);
	}







object_initialize_result_guard_t::object_initialize_result_guard_t( object_initialize_result_guard_t && o )noexcept
	: _p( std::move( o._p ) ){
	
	static_assert(noexcept(decltype(_p)( std::move(o._p))), __FILE_LINE__);
	}

object_initialize_result_guard_t& object_initialize_result_guard_t::operator=( object_initialize_result_guard_t && o )noexcept{
	static_assert(noexcept(_p = std::move(o._p)), __FILE_LINE__);

	if( this != std::addressof( o ) ){
		_p = std::move( o._p );
		}

	return (*this);
	}

void object_initialize_result_guard_t::invoke( std::unique_ptr<dcf_exception_t> && exc )noexcept{
	static_assert(noexcept(_p->invoke(std::move(exc))), __FILE_LINE__);

	if( _p ){
		_p->invoke( std::move( exc ) );
		}
	}

void object_initialize_result_guard_t::invoke( crm::datagram_t && value ){
	if( _p ){
		_p->invoke( std::move( value ) );
		}
	}

void object_initialize_result_guard_t::operator()( std::unique_ptr<dcf_exception_t> && exc )noexcept{
	static_assert(noexcept(invoke(std::move(exc))), __FILE_LINE__);
	invoke( std::move( exc ) );
	}

void object_initialize_result_guard_t::operator()( crm::datagram_t && value ){
	invoke( std::move( value ) );
	}

object_initialize_result_guard_t::operator bool()const noexcept{
	//static_assert(noexcept(static_cast<bool>((const object_initialize_invoker&)(*_p))), __FILE_LINE__);
	return _p && (*_p);
	}



i_x1_stream_address_handler::~i_x1_stream_address_handler(){}
const std::string i_x1_stream_address_handler::null_address;
const int i_x1_stream_address_handler::null_port = 0;
const std::string i_x1_stream_address_handler::null_name;



i_x1_stream_address_handler::input_value_t::input_value_t()noexcept {}
i_x1_stream_address_handler::input_value_t::input_value_t(std::unique_ptr<i_iol_ihandler_t>&& m,
	const xpeer_desc_t& d,
	std::weak_ptr<i_xpeer_rt1_base_t> xpeerWlnk)noexcept
	: m_(std::move(m))
	, d_(d) 
	, _xpeerWlnk(std::move(xpeerWlnk)){}

const i_x1_stream_address_handler::xpeer_desc_t& i_x1_stream_address_handler::input_value_t::desc()const noexcept {
	return d_;
	}

const std::unique_ptr<i_iol_ihandler_t>& i_x1_stream_address_handler::input_value_t::m()const& noexcept {
	return m_;
	}

std::unique_ptr<i_iol_ihandler_t> i_x1_stream_address_handler::input_value_t::m() && noexcept {
	return std::move(m_);
	}


