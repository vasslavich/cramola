#include "../../internal.h"
#include "../rt1_endpoint.h"


using namespace crm;
using namespace crm::detail;


const rt1_endpoint_t rt1_endpoint_t::null;

rt1_endpoint_t::rt1_endpoint_t() noexcept {}

rt1_endpoint_t::rt1_endpoint_t( std::string_view streamName,
	int streamId,
	const identity_descriptor_t & endpointId )
	: _targetId( endpointId ) 
	, _baseAddress( streamName )
	, _baseId( streamId ){}

rt1_endpoint_t rt1_endpoint_t::make_local(int streamId,
	const identity_descriptor_t & endpointId) {

	return rt1_endpoint_t("", streamId, endpointId);
	}

rt1_endpoint_t rt1_endpoint_t::make_remote(std::string_view streamName,
	int streamId,
	const identity_descriptor_t& endpointId) {

	return rt1_endpoint_t(streamName, streamId, endpointId);
	}

const std::string& rt1_endpoint_t::address()const noexcept {
	return _baseAddress;
	}

const identity_descriptor_t& rt1_endpoint_t::endpoint_id()const noexcept {
	return _targetId;
	}

int rt1_endpoint_t::port()const noexcept {
	return _baseId;
	}

bool crm::operator == (const rt1_endpoint_t &l, const rt1_endpoint_t &r)noexcept {
	return l.address() == r.address() && l.endpoint_id() == r.endpoint_id();
	}

bool crm::operator != (const rt1_endpoint_t &l, const rt1_endpoint_t &r) noexcept {
	return !(l == r);
	}

bool crm::operator < (const rt1_endpoint_t & lval, const rt1_endpoint_t &rval)noexcept {
	if( lval.address() < rval.address() )
		return true;
	else if( lval.address() > rval.address() )
		return false;
	else if( lval.port() < rval.port() )
		return true;
	else if( lval.port() > rval.port() )
		return false;
	else
		return lval.endpoint_id() < rval.endpoint_id();
	}

bool crm::operator > (const rt1_endpoint_t & lval, const rt1_endpoint_t &rval)noexcept {
	return rval < lval;
	}

bool crm::operator >= (const rt1_endpoint_t & lval, const rt1_endpoint_t &rval) noexcept {
	if( lval.address() > rval.address() )
		return true;
	else if( lval.address() < rval.address() )
		return false;
	else if( lval.port() > rval.port() )
		return true;
	else if( lval.port() < rval.port() )
		return false;
	else
		return lval.endpoint_id() >= rval.endpoint_id();
	}

std::string rt1_endpoint_t::print()const noexcept{
	return _baseAddress + ":" + std::to_string(_baseId);
	}

connection_key_t rt1_endpoint_t::to_key()const noexcept{
	connection_key_t ckey;
	ckey.address = address();
	ckey.port = port();
	return ckey;
	}

std::string rt1_endpoint_t::to_string()const noexcept{
	return address() + ":" + std::to_string( port() ) + ":" + endpoint_id().to_str();
	}

address_hash_t rt1_endpoint_t::object_hash()const noexcept{
	address_hash_t h;
	apply_at_hash( h, _targetId );
	apply_at_hash( h, _baseAddress );
	apply_at_hash( h, _baseId );

	return h;
	}

std::unique_ptr<i_endpoint_t> rt1_endpoint_t::copy()const noexcept{
	return std::make_unique<rt1_endpoint_t>( rt1_endpoint_t( *this ) );
	}

size_t rt1_endpoint_t::get_serialized_size()const noexcept{
	return srlz::object_trait_serialized_size( _targetId )
		+ srlz::object_trait_serialized_size( _baseAddress )
		+ srlz::write_size_v<decltype(_baseId)>;
	}

