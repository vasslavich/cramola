#include "../../internal.h"
#include "../timetypes.h"
#include "../../exceptions.h"


using namespace crm;
using namespace crm::detail;


const sndrcv_timeline_periodicity_t sndrcv_timeline_periodicity_t::null;
const sndrcv_timeline_periodicity_t::timeline_t sndrcv_timeline_periodicity_t::max_duration = timeline_t::max();
const sndrcv_timeline_periodicity_t::timeline_t sndrcv_timeline_periodicity_t::null_timeline_value = timeline_t(0);
const sndrcv_timeline_t::timeline_t sndrcv_timeline_t::max_duration = sndrcv_timeline_periodicity_t::max_duration;
const sndrcv_timeline_t::timeline_t null_timeline_value = sndrcv_timeline_periodicity_t::timeline_t();



sndrcv_timeline_timeouted_once::sndrcv_timeline_timeouted_once(const sndrcv_timeline_periodicity_t::timeline_t & t){
	if(sndrcv_timeline_periodicity_t::validate_duration_range(t)) {
		value = t;
		}
	else{
		FATAL_ERROR_FWD(nullptr);
		}
	}

sndrcv_timeline_periodicity_t::sndrcv_timeline_periodicity_t()noexcept {}

const sndrcv_timeline_periodicity_t::timeline_t& sndrcv_timeline_periodicity_t::timeout()const noexcept {
	return _timeout;
	}

const sndrcv_timeline_periodicity_t::timeline_t& sndrcv_timeline_periodicity_t::interval()const noexcept {
	return _interval;
	}

sndrcv_timeline_periodicity_t::periodicity_t sndrcv_timeline_periodicity_t::periodicity()const noexcept {
	return _periodicity;
	}

bool sndrcv_timeline_periodicity_t::is_timeouted()const noexcept {
	return _periodicity == periodicity_t::cyclic_timeouted || _periodicity == periodicity_t::once_call_timeouted;
	}

bool sndrcv_timeline_periodicity_t::is_cyclic()const noexcept {
	return _periodicity == periodicity_t::cyclic || _periodicity == periodicity_t::cyclic_timeouted;
	}

bool sndrcv_timeline_periodicity_t::is_once_call()const noexcept {
	return _periodicity == periodicity_t::once_call || _periodicity == periodicity_t::once_call_timeouted;
	}

sndrcv_timeline_periodicity_t sndrcv_timeline_periodicity_t::make_once_call() {
	return sndrcv_timeline_periodicity_t(max_duration, max_duration, periodicity_t::once_call);
	}

std::chrono::milliseconds sndrcv_timeline_periodicity_t::make_max_interval() {
	return std::chrono::duration_cast<std::chrono::milliseconds>((std::chrono::system_clock::time_point::max)() - std::chrono::system_clock::now()) / 3;
	}

std::chrono::system_clock::time_point sndrcv_timeline_periodicity_t::make_deadline_point_from_now()const {
	return make_deadline_point_from(std::chrono::system_clock::now());
	}

std::chrono::system_clock::time_point sndrcv_timeline_periodicity_t::make_deadline_point_from(const std::chrono::system_clock::time_point & tp)const {
	switch(_periodicity) {

		case periodicity_t::cyclic_timeouted:
			return tp + _timeout;
		case periodicity_t::cyclic:
			return std::chrono::system_clock::time_point::max();
		case periodicity_t::once_call_timeouted:
			return tp + _timeout;
		case periodicity_t::once_call:
			return std::chrono::system_clock::time_point::max();
		case periodicity_t::undefined:
			THROW_MPF_EXC_FWD(nullptr);
		default:
			FATAL_ERROR_FWD(nullptr);
		}
	}


const sndrcv_timeline_t sndrcv_timeline_t::null = sndrcv_timeline_t(sndrcv_timeline_periodicity_t::null, std::chrono::system_clock::time_point{});

sndrcv_timeline_t sndrcv_timeline_t::make_once_call() {
	return sndrcv_timeline_t(sndrcv_timeline_periodicity_t::make_once_call(), std::chrono::system_clock::now());
	}

const sndrcv_timeline_t::timeline_t& sndrcv_timeline_t::timeout()const noexcept {
	return _value.timeout();
	}

const sndrcv_timeline_t::timeline_t& sndrcv_timeline_t::interval()const noexcept {
	return _value.interval();
	}

sndrcv_timeline_t::periodicity_t sndrcv_timeline_t::periodicity()const noexcept {
	return _value.periodicity();
	}

std::chrono::system_clock::time_point sndrcv_timeline_t::deadline_point()const {
	return _value.make_deadline_point_from(_start_point);
	}

bool sndrcv_timeline_t::is_timeouted()const noexcept {
	return _value.is_timeouted();
	}

bool sndrcv_timeline_t::is_cyclic()const noexcept {
	return _value.is_cyclic();
	}

bool sndrcv_timeline_t::is_once_call()const noexcept {
	return _value.is_once_call();
	}

sndrcv_timeline_t sndrcv_timeline_t::make(const sndrcv_timeline_timeouted_once & value) {
	if(sndrcv_timeline_periodicity_t::validate_duration_range(value.value)) {
		return make_once_call_timeouted(value.value);
		}
	else {
		FATAL_ERROR_FWD(nullptr);
		}
	}

const std::chrono::system_clock::time_point& sndrcv_timeline_t::start_point()const noexcept {
	return _start_point;
	}

const sndrcv_timeline_periodicity_t& sndrcv_timeline_t::value()const noexcept {
	return _value;
	}
