#pragma once


#include "../utilities/identifiers/predecl.h"
#include "../utilities/hash/predecl.h"


namespace crm {


#ifdef CBL_MESSAGE_ADDRESS_TYPES_SHORT

/*! Global object identity type */
using global_object_identity_t = sx_glob_64;

/*! Message spin-tag type for send-receive operations */
using message_spin_tag_type = sx_loc_64;

/*! Subscriber instance identity type */
using subscriber_node_id_t = sx_loc_64;

/*! Subscriber instance identity type */
using local_object_id_t = sx_loc_64;

/*! Session unique identity type */
using session_id_t = sx_loc_64;


#else

/*! Global object identity type */
using global_object_identity_t = sx_uuid_t;

/*! Message spin-tag type for send-receive operations */
using message_spin_tag_type = sx_uuid_t;

/*! Subscriber instance identity type */
using subscriber_node_id_t = sx_locid_t;

/*! Subscriber instance identity type */
using local_object_id_t = sx_locid_t;

/*! Session unique identity type */
using session_id_t = sx_locid_t;


#endif

using stream_sequence_position_t = sx_uuid_t;
using stream_source_descriptor_t = sx_uuid_t;

using identity_descriptor_id_type = global_object_identity_t;

/*! Subscriber instance identity type */
using subscriber_event_id_t = sx_locid_t;

/*! Command identity type */
using local_command_identity_t = sx_locid_t;

/*! Global command identity type */
using global_command_identity_t = sx_uuid_t;

/*! Message Identity Type*/
using message_identity_type = global_object_identity_t;

/*! Channel unique identity type */
using channel_identity_t = sx_locid_t;
}

