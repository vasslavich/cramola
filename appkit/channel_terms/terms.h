#pragma once


#include <type_traits>
#include <functional>
#include <vector>
#include <memory>
#include "./base_terms.h"
#include "./ids_types.h"
#include "./i_channel_base.h"
#include "./i_channel_io.h"
#include "../internal_invariants.h"
#include "../protocol/i_encoding.h"
#include "./i_events.h"
#include "./connection_key.h"
#include "./session_desc.h"
#include "./i_endpoint.h"
#include "./rt1_endpoint.h"
#include "./source_object_key.h"
#include "./rt0_rdm.h"
#include "./address_desc.h"
#include "./message_base_image.h"
#include "./rtl_rdm_object.h"
#include "./policies.h"
#include "ids_types.h"

namespace crm{
namespace detail{


/*! �������� ���������� ����������� � ��������� ���� */
class i_outcoming_peer_t {
public:
	virtual ~i_outcoming_peer_t() {}
	};

/*! ��������� ������� ����������� */
struct i_connection_factory_t {
public:
	virtual ~i_connection_factory_t() {}
	};







/*class i_iol_write_iterator_t {
public:
	virtual ~i_iol_write_iterator_t() {}

	typedef const uint8_t *citerator_t;
	virtual citerator_t cbegin()const = 0;
	virtual citerator_t cend()const = 0;
	};


class i_iol_write_contiguous_t {
public:
	virtual ~i_iol_write_contiguous_t() {}

	typedef const uint8_t *citerator_t;
	virtual citerator_t ptr()const = 0;
	virtual size_t size()const = 0;
	};*/




//
///*! ��� �������� ������������������� ��������� ��������� */
//class i_iol_linked_t {
//public:
//	/*! ������� ���������� ��������� ������� */
//	enum class finalize_reason_t {
//		eof, ///< ������� �����
//		canceled, ///< ������
//		exc ///< ����������
//		};
//
//	virtual ~i_iol_linked_t() {}
//
//	/*! ����������� � ������� ���������� ��������� �������.
//	\param reason ������� ���������� �������, �� ������ /ref finalize_reason_t */
//	virtual void ntf_fnlz( finalize_reason_t /*reason*/ ) {}
//
//	/*! ���������� ������� � ������ ��������� ������� ����������.
//	\param obj ������� ������
//	\param selfId ������������� ��������
//	\param prevId ������������� ����������� �������� */
//	virtual void app(
//		std::unique_ptr<i_iol_ihandler_t> && obj,
//		global_object_identity_t prevId,
//		global_object_identity_t selfId ) = 0;
//
//	/*! �������, �����������, ��� ������ �������� �������� ��������� ��������.
//	� ������, ���� ��� ������ �������� �������� ��������-�����������, ������������
//	�������� ��������� selfId ������������� ������������� �������� ������ /ref recipient_id.
//	\param selfId ������������� ������� ������� */
//	virtual bool is_recipient( global_object_identity_t &selfId )const = 0;
//
//	/*! ������������� ���������� (��������� ������� ������).
//	� ������, ���� ��� ������ �������� ��������-�����������, ������������
//	�������� ������������� ��������� �������� ��������� selfId ������ /ref is_recipient. */
//	virtual global_object_identity_t recipient_id()const = 0;
//
//	/*! �������, ��� ������ �������� ��������� ������ ��������� �������.
//	\param prevId ������������� ����������� �������� � ������� */
//	virtual bool is_following( global_object_identity_t &prevId )const = 0;
//
//	/*! ������� ����� ������� ��������� ��������.
//	\param recipientId ������������� ������ */
//	virtual bool is_end()const = 0;
//	};
//


/*using event_handler_f = std::function<void(std::unique_ptr<i_event_t> &&, subscribe_invoke)>;
using event_handler_1_f = std::function<void(std::unique_ptr<i_event_t> &&)>;*/






}
}





