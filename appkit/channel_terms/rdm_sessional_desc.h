#pragma once


#include "./base_terms.h"
#include "./rt1_endpoint.h"
#include "./session_desc.h"


namespace crm {



struct rdm_sessional_descriptor_t final:
	public hf_operators_t<rdm_sessional_descriptor_t>  {

private:
	rt1_endpoint_t _ep;
	session_description_t _sid;

	/*! ��� ��������� */
	address_hash_t _hash;

	void update_hash()noexcept;

public:
	using hash_t = address_hash_t;

	/*! ����� r [�� ��������� �����]*/
	bool equals_fields(const rdm_sessional_descriptor_t& r)const noexcept;
	/*! ������ ��� r [�� ��������� �����]*/
	bool less_fields(const rdm_sessional_descriptor_t& r)const noexcept;
	/*! ������ ��� r [�� ��������� �����]*/
	bool great_fields(const rdm_sessional_descriptor_t& r)const noexcept;
	/*! ������ ��� ����� r [�� ��������� �����]*/
	bool great_equals_fields(const rdm_sessional_descriptor_t& r)const noexcept;

	template<typename Ts>
	void srlz(Ts && dest)const{
		srlz::serialize(dest, _ep);
		srlz::serialize(dest, _sid);
		srlz::serialize(dest, _hash);
		}

	template<typename Ts>
	void dsrlz(Ts && src ){
		srlz::deserialize(src, _ep);
		srlz::deserialize(src, _sid);
		srlz::deserialize(src, _hash);
		}

	size_t get_serialized_size()const noexcept;

	static const rdm_sessional_descriptor_t null;

	const address_hash_t& object_hash()const noexcept;
	const rt1_endpoint_t& ep()const noexcept;
	const session_description_t& sid()const noexcept;

	rdm_sessional_descriptor_t()noexcept;
	rdm_sessional_descriptor_t(const rt1_endpoint_t & ep, const session_description_t & sid)noexcept;

	bool is_null()const noexcept;
	};
}
