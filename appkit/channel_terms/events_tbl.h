#pragma once


#include <unordered_map>
#include <functional>
#include <memory>
#include <atomic>
#include "./events_terms.h"
#include "../counters/internal_terms.h"


namespace crm{
namespace detail{


class events_table_t {
	friend class events_multitable_t;

public:
	using line_num_t = std::uintmax_t;
	using event_addressed_link_t = event_subscribe_assist_t::event_addressed_link_t;
	using handler_t = event_subscribe_assist_t::event_handler_t;

private:
	using lck_t = std::mutex;
	using lck_scp_t = std::unique_lock<lck_t>;

public:
	using address_desc = event_subscribe_assist_t::address_desc;
	using check_condition_f = std::function<std::unique_ptr<i_event_t>(event_type_t)>;

private:
	using subscribe_with_tail_exception_guard = event_subscribe_assist_t::subscribe_with_tail_exception_guard;
	using line_map_t = std::list<std::shared_ptr<event_subscribe_assist_t::event_addressed_link_t>>;

	/*! ����� �������� �������� ��������� */
	std::unordered_map<line_num_t, line_map_t > _addressBook;
	mutable lck_t _lck;
	/*! �������� */
	std::weak_ptr<distributed_ctx_t> _ctx;

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	mutable crm::utility::__xvalue_counter_logger_t<1> __sizeTraceer1 = __FILE_LINE__;
	mutable crm::utility::__xvalue_counter_logger_t<1> __sizeTraceer2 = __FILE_LINE__;
	crm::utility::__xobject_counter_logger_t<events_table_t> _xDbgCounter;

	void __size_trace()const noexcept;
#endif

	/*! ���������� ���������� ��� ������� ��������*/
	void __add( const line_num_t&,
		async_space_t scx,
		const address_desc & address,
		__event_handler_f && hdl,
		bool once );

	/*! ���������� ���������� ��� ������� ��������*/
	void __reg_line(const line_num_t& );

	/*! ������� ����������� ��� ������� ������ */
	std::list<std::shared_ptr<event_subscribe_assist_t::event_addressed_link_t>> remove(const line_num_t&, const address_desc & address)noexcept;
	std::list<std::shared_ptr<event_subscribe_assist_t::event_addressed_link_t>> remove(const line_num_t&)noexcept;

	static void reset(std::shared_ptr<event_subscribe_assist_t::event_addressed_link_t> &&)noexcept;
	static void reset(std::list<std::shared_ptr<event_subscribe_assist_t::event_addressed_link_t>> &&)noexcept;

	static void invoke(std::shared_ptr<event_subscribe_assist_t::event_addressed_link_t> & il, std::unique_ptr<i_event_t> && iarg)noexcept;
	static void invoke(std::list<std::shared_ptr<event_subscribe_assist_t::event_addressed_link_t>> && il, std::unique_ptr<event_args_t> && iarg)noexcept;

	void __close()noexcept;

	subscribe_result _subscribe( const line_num_t&,
		async_space_t scx,
		const address_desc & id,
		const bool once,
		__event_handler_f && handler,
		check_condition_f && checkPred );

public:


	events_table_t()noexcept;
	events_table_t(std::weak_ptr<distributed_ctx_t> ctx_)noexcept;

	~events_table_t();
	void close()noexcept;

	events_table_t(const events_table_t&) = delete;
	events_table_t& operator=(const events_table_t &) = delete;

	void invoke(const line_num_t & ln,
		event_type_t type,
		std::unique_ptr<event_args_t> && args)noexcept;

	void regline(const line_num_t& ln);

	/*! �������� �� ��������� ��������� ��� ���������� ��������������.
	\param id ������� �������������
	\param once ���� ������� true, ����������� �������� ���� ���, ����� �� ��� ���,
	���� �� ����� ��������� ������ ����������� � ������� ������ /ref unsubscribe_hndl_id.
	*/
	subscribe_result subscribe(const line_num_t& lkey,
		async_space_t scx,
		const address_desc & id,
		const bool once,
		__event_handler_f && handler,
		check_condition_f && checkPred);

	/*! ����� �� ��������� ����������� ��� ���������� ��������������
	\param id ������������� ����������
	*/
	void unsubscribe(const line_num_t&, const address_desc & id)noexcept;

	/*! ����� �� ��������� ����������� ��� ���������� ��������������
	\param id ������������� ����������
	*/
	void unsubscribe(const line_num_t&, const channel_identity_t & chid)noexcept;

	/*! ����� �� ��������� ����������� ��� ��������� �����
	*/
	void close(const line_num_t&)noexcept;

	events_list release(const line_num_t&)noexcept;
	void add(const line_num_t&, events_list &&);

	template<typename TFunction, typename ... TArgs>
	void lock( const line_num_t&, TFunction && hl, TArgs && ... args ){
		lck_scp_t lck( _lck );
		hl( std::forward<TArgs>( args )... );
		}
	};


class events_multitable_t :
	public std::enable_shared_from_this<events_multitable_t> {
public:
	using check_condition_f = event_subscribe_assist_t::check_condition_f;
	using address_desc = event_subscribe_assist_t::address_desc;
	using line_num_t = event_subscribe_assist_t::line_num_t;
	using handler_t = event_subscribe_assist_t::event_handler_t;

private:
	std::atomic<line_num_t> _lineIdCounter{ 0 };
	std::vector<std::unique_ptr<events_table_t>> _table;
	std::weak_ptr<distributed_ctx_t> _ctx;

	size_t h2id(const handler_t & ln)const noexcept;

public:
	void invoke(const handler_t & ln,
		event_type_t type,
		std::unique_ptr<event_args_t> && args)noexcept;

	subscribe_result subscribe(const handler_t & ln,
		async_space_t scx,
		const address_desc & id,
		const bool once,
		__event_handler_f && handler,
		check_condition_f && checkPred);

	events_list release(const handler_t & ln)noexcept;
	void add(const handler_t & ln, events_list &&);
	void regline(const handler_t& ln);

	void unsubscribe(const handler_t & ln, const address_desc & id)noexcept;
	void unsubscribe(const handler_t & ln, const channel_identity_t & chid)noexcept;
	void close(const handler_t & ln)noexcept;

public:
	handler_t make_handler();
	events_multitable_t(std::weak_ptr<distributed_ctx_t> ctx_,
		size_t wide);
	};
}
}
