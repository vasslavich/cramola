#pragma once


#include "./i_channel_base.h"
#include "./i_channel_messages.h"
#include "../cmdrt/i_channel_commands.h"


namespace crm::detail{

enum class channel_state{
	ready,
	fault
	};

class io_x_irt1_t :
	public io_x_irt1_messages_t,
	public io_x_irt1_commands_t{
public:
	virtual ~io_x_irt1_t() = 0;
	};


struct io_x_ort1_t :
	public io_x_ort1_messages_t,
	public io_x_ort1_commands_t{

	virtual ~io_x_ort1_t() = 0;
	};



class io_x_rt1_channel_messages_t :
	public io_x_irt1_messages_t,
	public io_x_ort1_messages_t{};

class io_x_rt1_channel_commands_t :
	public io_x_irt1_commands_t,
	public io_x_ort1_commands_t{};

class io_x_rt1_channel_t :
	public io_x_rt1_channel_messages_t,
	public io_x_rt1_channel_commands_t{
	};





class io_irt0_x_t{
public:
	virtual ~io_irt0_x_t() = 0;
	};


class io_ort0_x_t{
public:
	virtual ~io_ort0_x_t() = 0;
	virtual bool push_to_rt1( rt0_x_message_t && object, const std::chrono::milliseconds & wait )noexcept = 0;
	virtual bool push_to_rt1( command_rdm_t && ev)noexcept = 0;
	virtual void set_channel_state( channel_state st )noexcept = 0;
	};



class io_rt0_x_channel_t :
	public io_irt0_x_t,
	public io_ort0_x_t{

public:
	virtual std::shared_ptr<distributed_ctx_t> ctx()const noexcept = 0;
	virtual const channel_info_t& get_channel_info()const = 0;
	virtual void subscribe( const rtl_roadmap_event_t & id, const std::list<rtx_command_type_t> & evlst ) = 0;
	virtual void unsubscribe( const rtl_roadmap_event_t & id )noexcept = 0;
	//virtual void notify( rtx_command_type_t ev, rt0_x_command_pack_t&& cmd )noexcept = 0;
	
	template<typename _TxCommand,
		typename C = typename std::decay_t<_TxCommand>,
		typename std::enable_if_t<std::is_base_of_v<i_command_from_rt02rt1_t, C>&& std::is_final_v<C>, int> = 0>
	bool push_command_to_rt1(_TxCommand && cmd)noexcept {

		command_rdm_t rdm;
		rdm.target = cmd.command_target_receiver_id();
		rdm.cmd = std::forward<_TxCommand>(cmd);

		return push_to_rt1(std::move(rdm));
		}

	virtual void close()noexcept = 0;
	virtual bool closed()const noexcept = 0;
	virtual void open(){}

protected:
	virtual std::list<rtl_roadmap_event_t> find_event_targets(rtx_command_type_t ev)const noexcept = 0;

public:
	template<typename _TxCommand,
		typename C = typename std::decay_t<_TxCommand>,
		typename std::enable_if_t<std::is_base_of_v<i_command_from_rt02rt1_t, C>&& std::is_final_v<C>, int> = 0>
	void notify_to_rt1(rtx_command_type_t ev, _TxCommand && cmd)noexcept {
		if (!closed()) {
			CBL_NO_EXCEPTION_BEGIN

			auto evTargets(find_event_targets(ev));
			all_send(evTargets, std::forward<_TxCommand>(cmd));

			CBL_NO_EXCEPTION_END(ctx())
			}
		}

protected:
	template<typename _TxCommand,
		typename C = typename std::decay_t<_TxCommand>,
		typename std::enable_if_t<std::is_base_of_v<i_command_from_rt02rt1_t, C>&& std::is_final_v<C>, int> = 0>
	void all_send(const std::list<rtl_roadmap_event_t>& lst, _TxCommand  && cmd)noexcept {
		CBL_NO_EXCEPTION_BEGIN

		if (!lst.empty()) {
			const auto wt = cmd.wait_timeout();

			auto it = lst.begin();
			for (size_t i = 0; (i + 1) < lst.size(); ++i, ++it) {
				command_rdm_t rdm;
				rdm.target = (*it);
				rdm.cmd = rt0_x_command_pack_t(cmd);

				if (!push_to_rt1(std::move(rdm))) {
					if (auto c = ctx()) {
						c->exc_hndl(CREATE_PTR_EXC_FWD(nullptr));
						}
					}
				}

			command_rdm_t rdm;
			rdm.target = (*it);
			rdm.cmd = rt0_x_command_pack_t(std::forward<_TxCommand>(cmd));

			if (!push_to_rt1(std::move(rdm))) {
				if (auto c = ctx()) {
					c->exc_hndl(CREATE_PTR_EXC_FWD(nullptr));
					}
				}
			}

		CBL_NO_EXCEPTION_END(ctx())
		}
	};


class unique_channel_manager : public io_x_rt1_channel_t, public io_rt0_x_channel_t {};

class i_rt0_channel_route_t{
public:
	virtual ~i_rt0_channel_route_t(){}

	/*! �������� ��������� � ������ 1 � ����� X �� ������� 0 */
	[[nodiscard]]
	virtual addon_push_event_result_t push_to_rt0_xpeer_XBN( rt1_x_message_data_t && object )noexcept = 0;

	/*! �������� ������� � ������ 1 � ����� X �� ������� 0 */
	[[nodiscard]]
	virtual addon_push_event_result_t push_command_pack_to_rt0( rt0_x_command_pack_t && cmd)noexcept = 0;

	template<typename _TxCommand,
		typename C = typename std::decay_t<_TxCommand>,
		typename std::enable_if_t<std::is_base_of_v<i_command_from_rt1_t, C>&& std::is_final_v<C>, int> = 0>
		[[nodiscard]]
	addon_push_event_result_t push_to_rt0_command(_TxCommand && cmd)noexcept {
		return push_command_pack_to_rt0(rt0_x_command_pack_t{ std::forward<_TxCommand>(cmd) });
		}

	virtual void register_rt1_channel( std::weak_ptr<io_rt0_x_channel_t> channel ) = 0;
	virtual void unregister_rt1_channel( const channel_info_t& chId )noexcept = 0;
	};


typedef bool(DCF_ADDON_CALL *dcf_addon_image_push_message_to_rt0_f)(
	const sx_locid_t & target, const rt1_x_message_data_t *obj, long long millisecondsWait);
typedef addon_push_event_result_t( DCF_ADDON_CALL *dcf_addon_image_push_event_to_rt0_f )(
	const sx_locid_t & target, const rt0_x_command_pack_t *obj, long long millisecondsWait );


struct rt0_x_channel_link_functors_table_rt0_x_desc_t{
	local_object_id_t locid;

	std::function<bool(const local_object_id_t & target,
		const rt1_x_message_data_t &obj, 
		const std::chrono::milliseconds & wait )> fmsg;

	std::function<addon_push_event_result_t(
		const local_object_id_t & target,
		const rt0_x_command_pack_t & obj,
		const std::chrono::milliseconds & wait)> fcmd;
	};

class i_rt1_leaf_backlink_t : public io_x_irt1_t, public io_x_ort1_t{};

struct unregister_rt1_stream_t{
	std::function<void()> _f;
	std::weak_ptr<distributed_ctx_t> _ctx;

	unregister_rt1_stream_t()noexcept;
	unregister_rt1_stream_t( std::weak_ptr<distributed_ctx_t> ctx, std::function<void()> && f )noexcept;

	~unregister_rt1_stream_t();
	void reset()noexcept;
	void invoke()noexcept;
	bool valid()const noexcept;

	unregister_rt1_stream_t( const unregister_rt1_stream_t & ) = delete;
	unregister_rt1_stream_t& operator=( const unregister_rt1_stream_t & ) = delete;

	unregister_rt1_stream_t( unregister_rt1_stream_t && )noexcept;
	unregister_rt1_stream_t& operator=( unregister_rt1_stream_t && )noexcept;
	};



struct i_rt0_peer_handler_t{
	virtual ~i_rt0_peer_handler_t() = 0;

	virtual void push( rt1_x_message_t && package ) = 0;
	virtual bool is_opened()const noexcept = 0;
	virtual rtl_table_t direction()const noexcept = 0;
	};


struct i_rt0_main_router_registrator_t{
	virtual ~i_rt0_main_router_registrator_t() = 0;

	virtual void unbind_out_connection( std::vector<source_object_key_t> && )noexcept = 0;
	virtual void unbind_out_connection( const connection_key_t & ck )noexcept = 0;

	[[nodiscard]]
	virtual addon_push_event_result_t push_command_pack_to_rt0( rt0_x_command_pack_t && c )noexcept = 0;
	};



}


namespace crm {

struct channel_config_base {
	virtual ~channel_config_base() {}
	virtual const channel_info_t& info()const noexcept = 0;
	virtual xchannel_type_t type()const noexcept = 0;
	};
}
