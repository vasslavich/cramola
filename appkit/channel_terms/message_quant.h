#pragma once


#include "../internal_invariants.h"
#include "./address_desc.h"
#include "./crc.h"
#include "./message_type_spcf.h"
#include "../counters/internal_terms.h"


namespace crm::detail {

/*! ���� �������� ��������� */
struct _packet_t final{

	iol_type_spcf_t _type;
	address_descriptor_t _address;
	type_identifier_t _typeId = type_identifier_t::null;
	std::string _typeName;
	crc_t::base_value_t _crc = 0;
	binary_vector_t _data;

#ifdef CBL_MPF_ENABLE_TIMETRACE
	hanlder_times_stack_t _timeline;
#endif//CBL_MPF_ENABLE_TIMETRACE

#ifdef CBL_MPF_MESSAGE_TRACEWAY_KEYS_BY_ID
	keycounter_trace::key_t _ktid;
#endif

#ifdef CBL_MESSAGES_COUNTER_TYPES_CHECKER
	crm::utility::__xobject_counter_logger_t<_packet_t> _ocounter{"_packet"};
#endif


#ifdef CBL_MPF_CHECK_LOST_PACKETS
	bool _lost{ true };
#endif

	bool _error{ false };
	bool _valued{ false };

	void reset()noexcept;


	~_packet_t();

	_packet_t(const _packet_t &) = delete;
	_packet_t& operator=(const _packet_t &) = delete;

	_packet_t(_packet_t &&)noexcept;
	_packet_t& operator=(_packet_t &&)noexcept;

	_packet_t()noexcept;

	_packet_t(iol_type_spcf_t type,
		const address_descriptor_t & address,
		type_identifier_t typeId,
		const std::string & typeName,
		crc_t::base_value_t crc,
		binary_vector_t && data
#ifdef CBL_MPF_ENABLE_TIMETRACE
		, hanlder_times_stack_t && tl
#endif//CBL_MPF_ENABLE_TIMETRACE
	);

	_packet_t(iol_type_spcf_t type,
		const address_descriptor_t & address,
		binary_vector_t && data);

	static _packet_t make_error(const address_descriptor_t & address)noexcept;

	bool valued()const noexcept;
	const iol_type_spcf_t& type()const noexcept;
	const address_descriptor_t& address()const noexcept;
	const type_identifier_t& type_id()const noexcept;
	const std::string& type_name()const noexcept;
	crc_t::base_value_t crc()const noexcept;
	const binary_vector_t& data()const noexcept;
	binary_vector_t data_release() noexcept;
	_packet_t copy()const;

	bool error()const noexcept;
	void mark_as_error()noexcept;

	void mark_as_used()noexcept;
	bool is_mark_used()const noexcept;

#ifdef CBL_MPF_ENABLE_TIMETRACE
	const hanlder_times_stack_t& timeline()const noexcept;
	hanlder_times_stack_t timeline_release() noexcept;
	void timeline_set_rt1_receiver_router() noexcept;
#endif//CBL_MPF_ENABLE_TIMETRACE

	/*! true - ���� l ����� ������������, ��� r */
	static bool priority(const _packet_t &l, const _packet_t &r) noexcept;


	template<typename Tws>
	void srlz(Tws&& dest)const {

		srlz::serialize(dest, _type);
		srlz::serialize(dest, _address);
		srlz::serialize(dest, _typeId);
		srlz::serialize(dest, _typeName);
		srlz::serialize(dest, _crc);
		srlz::serialize(dest, _data);

#ifdef CBL_MPF_ENABLE_TIMETRACE
		srlz::serialize(dest, _timeline);
#endif//CBL_MPF_ENABLE_TIMETRACE

#ifdef CBL_MPF_MESSAGE_TRACEWAY_KEYS_BY_ID
		srlz::serialize(dest, _ktid);
#endif

#ifdef CBL_MPF_CHECK_LOST_PACKETS
		srlz::serialize(dest, _lost);
#endif

		srlz::serialize(dest, _error);
		srlz::serialize(dest, _valued);
		}

	template<typename Trs>
	void dsrlz(Trs& src) {
		srlz::deserialize(src, _type);
		srlz::deserialize(src, _address);
		srlz::deserialize(src, _typeId);
		srlz::deserialize(src, _typeName);
		srlz::deserialize(src, _crc);
		srlz::deserialize(src, _data);

#ifdef CBL_MPF_ENABLE_TIMETRACE
		srlz::deserialize(src, _timeline);
#endif//CBL_MPF_ENABLE_TIMETRACE

#ifdef CBL_MPF_MESSAGE_TRACEWAY_KEYS_BY_ID
		srlz::deserialize(src, _ktid);
#endif

#ifdef CBL_MPF_CHECK_LOST_PACKETS
		srlz::deserialize(src, _lost);
#endif

		srlz::deserialize(src, _error);
		srlz::deserialize(src, _valued);
		}

	size_t get_serialized_size()const noexcept;
	};
}

