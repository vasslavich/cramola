#pragma once


#include "../internal_invariants.h"
#include "./rtl_rdm_link.h"
#include "./rt0_rdm.h"


namespace crm::detail {

struct rt1_streams_table_wait_key_t final:
	hf_operators_t<rt1_streams_table_wait_key_t> {

public:
	static const rt1_streams_table_wait_key_t null;

private:
	rtl_roadmap_link_t _rdml;
	rt0_node_rdm_t _rt0;
	address_hash_t _hash;
	rtl_table_t _t{ rtl_table_t::undefined };

	void update_hash()noexcept;

public:
	template<typename Ts>
	void srlz(Ts && dest)const{
		srlz::serialize(dest, _rdml);
		srlz::serialize(dest, _rt0);
		srlz::serialize(dest, _hash);
		srlz::serialize(dest, _t);
		}

	template<typename Ts>
	void dsrlz(Ts && src){
		srlz::deserialize(src, _rdml);
		srlz::deserialize(src, _rt0);
		srlz::deserialize(src, _hash);
		srlz::deserialize(src, _t);
		}

	std::size_t get_serialized_size()const noexcept;

	using hash_t = address_hash_t;

	/*! ����� r [�� ��������� �����]*/
	bool equals_fields(const rt1_streams_table_wait_key_t& r)const noexcept;
	/*! ������ ��� r [�� ��������� �����]*/
	bool less_fields(const rt1_streams_table_wait_key_t& r)const noexcept;
	/*! ������ ��� r [�� ��������� �����]*/
	bool great_fields(const rt1_streams_table_wait_key_t& r)const noexcept;
	/*! ������ ��� ����� r [�� ��������� �����]*/
	bool great_equals_fields(const rt1_streams_table_wait_key_t& r)const noexcept;

	rt1_streams_table_wait_key_t()noexcept;
	rt1_streams_table_wait_key_t(const rt0_node_rdm_t &, const rtl_roadmap_link_t &rdml, rtl_table_t t)noexcept;

	const rtl_roadmap_link_t& rdml()const noexcept;
	const rt0_node_rdm_t& rt0()const noexcept;
	const address_hash_t& object_hash()const noexcept;
	rtl_table_t table()const noexcept;
	std::string to_string()const noexcept;
	bool is_null()const noexcept;
	};
}
