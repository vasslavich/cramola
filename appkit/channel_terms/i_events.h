#pragma once


#include <memory>
#include "../counters/internal_terms.h"
#include "../base_predecl.h"
#include "./subscribe_event_addr.h"
#include "./channel_traits_using.h"
#include "./channel_traits_classes.h"


namespace crm{


struct i_event_t {
	virtual ~i_event_t() {}
	virtual const srlz::typemap_key_t& type_id()const noexcept = 0;
	};

struct i_stream_end_t {
	virtual ~i_stream_end_t() = 0;
	};


template<typename TDerivedEvent,
	typename _TDerivedEvent = typename std::decay_t<TDerivedEvent>,
	typename std::enable_if_t<detail::is_event_cast_v<i_event_t, _TDerivedEvent>, int> = 0>
std::optional<_TDerivedEvent> event_cast(std::unique_ptr<i_event_t> && e)noexcept {
	if(e && e->type_id() == _TDerivedEvent::class_type_id()) {
		if(auto uD = std::unique_ptr<_TDerivedEvent>(static_cast<_TDerivedEvent*>(e.get()))) {
			e.release();

			return std::optional(std::move(*uD));
			}
		else {
			return {};
			}
		}
	else {
		return {};
		}
	}

struct i_userspace_event_t : public i_event_t{};
struct i_syspace_event_t : public i_event_t{};


bool is_userspace_event( const std::unique_ptr<i_event_t> & ev )noexcept;
bool is_syspace_event( const std::unique_ptr<i_event_t> & ev )noexcept;


struct event_args_t : public i_event_t{
	virtual std::unique_ptr<i_event_t> copy()const noexcept = 0;
	};

struct event_abandoned_t final : public event_args_t{
	static const srlz::typemap_key_t class_type_id_;

	std::unique_ptr<i_event_t> copy()const noexcept final;
	static const srlz::typemap_key_t& class_type_id()noexcept;
	const srlz::typemap_key_t& type_id()const noexcept;
	};



template<typename TPeer>
struct event_open_connection_t final : public event_args_t{
	using self_t = typename event_open_connection_t<TPeer>;
	static const srlz::typemap_key_t class_type_id_;

	TPeer peer;

	event_open_connection_t( const TPeer & peer_ )
		: peer( peer_ ){
		CBL_VERIFY( peer_.state() >= remote_object_state_t::impersonated );
		}

	std::unique_ptr<i_event_t> copy()const noexcept final{
		return std::make_unique<event_open_connection_t>( (*this) );
		}

	static const srlz::typemap_key_t& class_type_id()noexcept {
		return class_type_id_;
		}

	const srlz::typemap_key_t& type_id()const noexcept {
		return class_type_id();
		}
	};

template<typename TPeer>
const srlz::typemap_key_t event_open_connection_t<TPeer>::class_type_id_ = srlz::typemap_key_t::make<event_open_connection_t<TPeer>>();

template<typename TPeer>
struct event_close_connection_t final : public event_args_t{
	using xpeer_desc_t = typename TPeer::xpeer_desc_t;
	using self_t = typename event_close_connection_t<TPeer>;
	static const srlz::typemap_key_t class_type_id_;

private:
	xpeer_desc_t desc;

public:
	event_close_connection_t( const xpeer_desc_t & desc_ )
		: desc( desc_ ){}

	std::unique_ptr<i_event_t> copy()const noexcept final{
		return std::make_unique<event_close_connection_t>( (*this) );
		}

	static const srlz::typemap_key_t& class_type_id()noexcept {
		return class_type_id_;
		}

	const srlz::typemap_key_t& type_id()const noexcept {
		return class_type_id();
		}
	};

template<typename TPeer>
const srlz::typemap_key_t event_close_connection_t<TPeer>::class_type_id_ = srlz::typemap_key_t::make<event_close_connection_t<TPeer>>();


template<typename TPeer>
struct subscribe_event_connection_t final : i_event_t{
	using handler_f = std::function<void( std::unique_ptr<dcf_exception_t> && exc, TPeer && xp, bool subscribed )>;
	using self_t = typename subscribe_event_connection_t<TPeer>;
	static const srlz::typemap_key_t class_type_id_;

private:
	std::weak_ptr<distributed_ctx_t> _ctx;
	_address_event_t _subscriberRdm;
	handler_f _handler;
	std::atomic<bool> _invokedf = false;

#ifdef CBL_TRACELEVEL_TRACE_TAG_ON
	trace_tag _tag;
#endif

#ifdef CBL_MPF_OBJECTS_COUNTER_CHECKER_XPEER_EVENTS
	crm::utility::__xobject_counter_logger_t<self_t, 1> _xDbgCounter;
#endif

	void _invoke( bool dctr, std::unique_ptr<dcf_exception_t> && exc, TPeer && xp, bool subscribed )noexcept{
		bool invf = false;
		if( _invokedf.compare_exchange_strong( invf, true ) ){
			if( _handler ){
				if( auto c = _ctx.lock() ){

					if( dctr )
						exc = CREATE_MPF_PTR_EXC_FWD(nullptr);

					c->launch_async( __FILE_LINE__, std::move( _handler ), std::move( exc ), std::move( xp ), subscribed );
					}
				}
			}
		}

public:
	subscribe_event_connection_t( const subscribe_event_connection_t & ) = delete;
	subscribe_event_connection_t& operator=( const subscribe_event_connection_t & ) = delete;

	template<typename XHandler>
	subscribe_event_connection_t(
#ifdef CBL_TRACELEVEL_TRACE_TAG_ON
		trace_tag&& tag,
#else
		trace_tag&&,
#endif
		std::weak_ptr<distributed_ctx_t> ctx,
		const _address_event_t& subscriberRdm,
		XHandler&& hndl)noexcept
		: _ctx(ctx)
		, _subscriberRdm(subscriberRdm)
		, _handler(std::forward<XHandler>(hndl))
#ifdef CBL_TRACELEVEL_TRACE_TAG_ON
		, _tag(std::move(tag))
#endif
		{}

	~subscribe_event_connection_t(){
		CHECK_NO_EXCEPTION( _invoke( true, nullptr, TPeer(), true ) );
		}

	void operator()( std::unique_ptr<dcf_exception_t> && exc, TPeer && xp, bool subscribed )noexcept{
		_invoke( false, std::move( exc ), std::move( xp ), subscribed );
		}

	const _address_event_t& subscriber_id()const noexcept{
		return _subscriberRdm;
		}

	static const srlz::typemap_key_t& class_type_id()noexcept {
		return class_type_id_;
		}

	const srlz::typemap_key_t& type_id()const noexcept {
		return class_type_id();
		}
	};

template<typename TPeer>
const srlz::typemap_key_t subscribe_event_connection_t<TPeer>::class_type_id_ = srlz::typemap_key_t::make<subscribe_event_connection_t<TPeer>>();


template<typename TEventCallback>
struct is_event_callback_invokable :
	std::is_nothrow_invocable<TEventCallback, std::unique_ptr<i_event_t>&&, subscribe_invoke> {};

template<typename TEventCallback>
struct is_event_point_callback_invokable :
	std::is_invocable<TEventCallback, std::unique_ptr<i_event_t>&&> {};


template<typename TECallback>
constexpr bool is_event_callback_invokable_v = is_event_callback_invokable<std::decay_t<TECallback>>::value;

template<typename TECallback>
constexpr bool is_event_point_callback_invokable_v = is_event_point_callback_invokable<std::decay_t<TECallback>>::value;
}

