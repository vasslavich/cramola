#pragma once


#include "../internal_invariants.h"
#include "./ids_types.h"


namespace crm{


struct channel_info_t{
private:
	channel_identity_t _id;

public:
	channel_info_t()noexcept;
	explicit channel_info_t( const channel_identity_t & id );

	void set_id( const channel_identity_t & id );
	const channel_identity_t& id()const noexcept;
	};

bool operator==( const channel_info_t& l, const channel_info_t& r )noexcept;
bool operator !=( const channel_info_t& l, const channel_info_t & r )noexcept;

namespace detail{
struct io_x_rt1_base_t{
	virtual ~io_x_rt1_base_t() = 0;

	virtual const channel_info_t& get_channel_info()const = 0;
	};
}
}

