#pragma once


#include "./base_terms.h"
#include "./connection_key.h"
#include "./session_desc.h"


namespace crm {

/*! ����� ������������� �� �������� ������ 0 */
struct rt0_node_rdm_t final :
	public hf_operators_t<rt0_node_rdm_t> {

private:
	/*! ������������� ���� */
	identity_descriptor_t _nodeId;
	/*! ������������� ������ */
	session_description_t _sessionId;
	/*! ��������� ����������� */
	connection_key_t _cnkeyAssigned;
	/*! ������������� ����� */
	connection_key_t _cnkeyRequested;

	/*! ��� ���� */
	detail::rtl_table_t _direction = detail::rtl_table_t::undefined;
	/*! ��� ������� */
	address_hash_t _hash;

	void update_hash()noexcept;

public:
	using hash_t = address_hash_t;

	/*! ����� r [�� ��������� �����]*/
	bool equals_fields(const rt0_node_rdm_t& r)const noexcept;
	/*! ������ ��� r [�� ��������� �����]*/
	bool less_fields(const rt0_node_rdm_t& r)const noexcept;
	/*! ������ ��� r [�� ��������� �����]*/
	bool great_fields(const rt0_node_rdm_t& r)const noexcept;
	/*! ������ ��� ����� r [�� ��������� �����]*/
	bool great_equals_fields(const rt0_node_rdm_t& r)const noexcept;

private:
	rt0_node_rdm_t(const identity_descriptor_t& nodeId,
		const session_description_t & nodeLocid,
		const connection_key_t& requestedCnk,
		const connection_key_t& assignedCnk,
		detail::rtl_table_t direction)noexcept;

public:
	static const rt0_node_rdm_t null;

	rt0_node_rdm_t()noexcept;

	static rt0_node_rdm_t make_output_rdm(const identity_descriptor_t& nodeId,
		const session_description_t & nodeLocid,
		const connection_key_t& requestedCnk,
		const connection_key_t& cnk)noexcept;

	static rt0_node_rdm_t make_input_rdm(const identity_descriptor_t& nodeId,
		const session_description_t & nodeLocid,
		const connection_key_t& cnk)noexcept;

	static rt0_node_rdm_t create_rt1_router_rdm(const std::string& name)noexcept;

	void set_connection_key(const connection_key_t & cnk);
	void set_direction(detail::rtl_table_t direction);
	void set_node_id(const identity_descriptor_t & id);
	void set_session(const session_description_t & s);
	
	bool is_output()const noexcept;
	bool is_input()const noexcept;
	const address_hash_t& object_hash()const noexcept;
	const connection_key_t& connection_key()const noexcept;
	const connection_key_t& requested_key()const noexcept;
	const identity_descriptor_t& node_id()const noexcept;
	const session_description_t& session()const noexcept;

	std::string to_str()const noexcept;
	detail::rtl_table_t direction()const noexcept;
	bool is_null()const noexcept;

	template<typename Ts>
	void srlz(Ts && dest)const{
		srlz::serialize(dest, _nodeId);
		srlz::serialize(dest, _sessionId);
		crm::srlz::serialize(dest, _direction);
		srlz::serialize(dest, _cnkeyAssigned);
		srlz::serialize(dest, _cnkeyRequested);
		srlz::serialize(dest, _hash);
		}

	template<typename Ts>
	void dsrlz(Ts && src){
		crm::srlz::deserialize(src, _nodeId);
		crm::srlz::deserialize(src, _sessionId);
		crm::srlz::deserialize(src, _direction);
		crm::srlz::deserialize(src, _cnkeyAssigned);
		crm::srlz::deserialize(src, _cnkeyRequested);
		crm::srlz::deserialize(src, _hash);
		}

	size_t get_serialized_size()const noexcept;
	};


std::ostream  & operator <<(std::ostream & os, const rt0_node_rdm_t &uuid);
std::wostream  & operator <<(std::wostream & os, const rt0_node_rdm_t &uuid);
}
