#pragma once

#include "../internal_invariants.h"
#include "./message_type_spcf.h"


namespace crm::detail {
struct iol_type_trait_t {
	iol_type_attributes_t name;
	iol_type_spcf_t::field_type_t value;

	iol_type_trait_t(iol_type_attributes_t name_, iol_type_spcf_t::field_type_t value_)
		: name(name_)
		, value(value_) {}

	template<typename T>
	iol_type_trait_t(iol_type_attributes_t name_, typename T value_)
		: name(name_) {
		static_assert(std::is_integral<T>::value || std::is_enum<T>::value, __FILE__);
		static_assert(sizeof(T) <= sizeof(iol_type_spcf_t::field_type_t), __FILE__);

		value = (iol_type_spcf_t::field_type_t)value_;
		}
	};
}

