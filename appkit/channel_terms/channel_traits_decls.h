#pragma once


#include <type_traits>


namespace crm::detail {

template<typename T, typename = void>
struct rand_generator{};

template<typename T, typename = std::void_t<>>
struct has_event_class_id : std::false_type {};

template<typename T, typename = std::void_t<>>
struct has_event_member_type_id : std::false_type{};
}

