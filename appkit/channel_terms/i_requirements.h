#pragma once

#include "../base_predecl.h"
#include "../internal_invariants.h"

namespace crm {
	template<typename O>
	constexpr bool is_out_message_requirements_v = (
		std::is_final_v<std::decay_t<O>> && 
		srlz::is_serializable_v<O> && 
		std::is_base_of_v<i_iol_ohandler_t, std::decay_t<O>>);
}

