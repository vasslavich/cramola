#pragma once


#include <functional>
#include <atomic>
#include <memory>
#include <queue>
#include <deque>
#include <condition_variable>
#include <array>
#include <vector>
#include <type_traits>
#include "../typeframe/options.h"
#include "../typeframe/streams.h"
#include "../internal_invariants.h"
#include "../channel_terms/internal_terms.h"
#include "./i_bincntr.h"
#include "../utilities/filesys/fio.h"
#include "../typeframe/options.h"


namespace crm {


namespace detail {


/*! �������� ����������� ���������� ������ */
class _packet_accmulator_t {
public:
	typedef srlz::detail::_sync_sequence_t<uint64_t> sync_line_t;

	enum class state_t {
		sync,
		type, 
		address,
		type_id, 
		timeline, 
		length, 
		ldata, 
		mdata, 
		crc, 
		__count };

private:
	srlz::detail::pod_accumulate_trait_t<iol_type_map_t::packed_type_t> _typeField;
	srlz::read_from_stream_accumulate_options_t<address_descriptor_t> _address;
	srlz::read_from_stream_accumulate_options_t<iol_typeid_spcf_t> _typeid;

#ifdef CBL_MPF_ENABLE_TIMETRACE
	srlz::read_from_stream_accumulate_options_t<detail::hanlder_times_stack_t> _timeline;
#endif//CBL_MPF_ENABLE_TIMETRACE

	srlz::detail::pod_accumulate_trait_t<iol_type_map_t::packed_length_t> _prefLength;
	srlz::detail::pod_accumulate_trait_t<iol_type_map_t::packed_crc_t> _crc;
	crc_t _crcCalculated;
	sync_line_t _eofFactor;
	sync_line_t _syncLine;
	binary_vector_t _data;
	state_t _state = state_t::sync;

public:
	_packet_accmulator_t()noexcept;

	_packet_accmulator_t(_packet_accmulator_t &&o)noexcept;
	_packet_accmulator_t& operator=(_packet_accmulator_t &&o)noexcept;
	void assign(_packet_accmulator_t &&o)noexcept;

	void set_sync_line(const std::vector<uint8_t> & line);
	bool sync()const;

	void set_state(state_t st);
	state_t state()const;

	void typefield_setmark();

private:
	template<typename TInIt>
	size_t accm_ldata(typename TInIt first, typename TInIt last) {

		if (type().length() == iol_length_spcf_t::prefixed) {
			if (_state == state_t::ldata) {

				if (pref_length() > _data.size()) {
					if (first < last) {
						const auto addCount = std::min<size_t>((pref_length() - _data.size()), std::distance(first, last));

						const auto off = _data.size();
						_data.resize(_data.size() + addCount);
						std::copy(first, first + addCount, _data.begin() + off);

						if (type().fl_use_crc())
							_crcCalculated.push(first, first + addCount);

						return addCount;
						}
					else
						return 0;
					}
				else
					return 0;
				}
			else
				FATAL_ERROR_FWD(nullptr);
			}
		else
			FATAL_ERROR_FWD(nullptr);
		}

	template<typename TInIt>
	size_t accm_mdata(typename TInIt first_, typename TInIt last_) {

		if (type().length() == iol_length_spcf_t::eof_marked_) {
			if (_state == state_t::mdata) {
				if (first_ < last_) {

					/* �������������� ����� ������ */
					_data.reserve(std::distance(first_, last_));

					/* ���������, ���� �� ����������� ������� ��������� */
					auto it = first_;
					while (!eof() && it != last_) {
						auto byte((*it));
						++it;

						_eofFactor.checkline_shifft(byte);

						/* �������� ������� ���������������� ������������������, ����������� �� ����� ����� */
						if (_eofFactor.checkline() == _syncLine.syncmask())
							THROW_MPF_EXC_FWD(nullptr);

						_data.push_back(byte);
						}

					if (type().fl_use_crc())
						_crcCalculated.push(first_, it);

					return (size_t)std::distance(first_, it);
					}
				else
					return 0;
				}
			else
				FATAL_ERROR_FWD(nullptr);
			}
		else
			FATAL_ERROR_FWD(nullptr);
		}

	template<typename TInIt>
	size_t tadd(const typename TInIt first, const typename TInIt last) {

		if (first < last) {
			auto it = first;
			size_t readcnt = 0;

			switch (_state) {

					case state_t::type:
						{
						readcnt = _typeField.add(it, last);
						_crcCalculated.push(it, it + readcnt);
						}
						break;

					case state_t::address:
						{
						readcnt = _address.add(it, last);

						if (type().fl_use_crc())
							_crcCalculated.push(it, it + readcnt);
						}
						break;

					case state_t::type_id:
						{
						readcnt = _typeid.add(it, last);

						if (type().fl_use_crc())
							_crcCalculated.push(it, it + readcnt);
						}
						break;

					case state_t::timeline:
						{
#ifdef CBL_MPF_ENABLE_TIMETRACE
						readcnt = _timeline.add(it, last);

						if (type().fl_use_crc())
							_crcCalculated.push(it, it + readcnt);
#endif//CBL_MPF_ENABLE_TIMETRACE
				}
						break;

					case state_t::length:
						{
						readcnt = _prefLength.add(it, last);

						if (type().fl_use_crc())
							_crcCalculated.push(it, it + readcnt);
						}
						break;

					case state_t::mdata:
						readcnt = accm_mdata(it, last);
						break;

					case state_t::ldata:
						readcnt = accm_ldata(it, last);
						break;

					case state_t::crc:
						readcnt = _crc.add(it, last);
						break;

					case state_t::sync:
						readcnt = _syncLine.checkline_shifft_until(it, last, [this](const sync_line_t & current) {

							/* ��������� ������ ��������������� ������������������ */
							return (_syncLine.syncmask() == current.checkline());
							});
						break;

					default:
						THROW_MPF_EXC_FWD(nullptr);
			}

			return std::distance(it, it + readcnt);
		}
		else
			return 0;
	}

public:
	void add(uint8_t byte);
	size_t add(const uint8_t* first, const uint8_t* last);
	size_t add(const std::vector<uint8_t> & line);
	size_t add(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last);

	bool type_ready()const;
	iol_type_spcf_t type()const;

	bool typeid_ready()const;
	const iol_typeid_spcf_t& typeid_()const;
	type_identifier_t typeid_id()const;
	std::string typeid_name()const;

	bool pref_length_ready()const;
	iol_type_map_t::packed_length_t pref_length()const;

	bool address_ready()const;
	const address_descriptor_t& address()const&;
	address_descriptor_t address()&&;

	void set_eof(const std::vector<uint8_t> & mask);
	bool eof()const;

	bool crc_ready()const;
	iol_type_map_t::packed_crc_t crc()const;
	iol_type_map_t::packed_crc_t crc_calculated()const;

	void data_reserve(size_t capacity);
	bool data_ready()const;
	size_t data_length()const;
	const binary_vector_t& data()const;
	binary_vector_t data();

	void reset();

	bool ready()const;
	_packet_t release();

	bool timeline_ready()const;

#ifdef CBL_MPF_ENABLE_TIMETRACE
	const detail::hanlder_times_stack_t& timeline()const&;
	detail::hanlder_times_stack_t timeline()&&;
#endif//CBL_MPF_ENABLE_TIMETRACE

	std::string info()const;
};
}

/*! �������� ����������� ������������������ ������� */
class _flow_packet_listener_t final /*:
	public _flow_packet_listener_t*/{

public:
	typedef detail::_packet_accmulator_t::sync_line_t sync_line_t;

private:
	static const uint64_t undefined_length = (uint64_t)(-1);

	class _binary_t{
	public:
		typedef binary_vector_t blob_t;
		typedef blob_t::const_iterator citerator_t;

	private:
		const blob_t * _pBuf;
		size_t _offset = 0;
		size_t _count = 0;

	public:
		_binary_t()noexcept;
		_binary_t( const binary_vector_t & blob, size_t )noexcept;

		_binary_t( _binary_t && )noexcept;
		_binary_t& operator=( _binary_t && )noexcept;

		bool eof()const noexcept;

		template<typename TApplyFunctor,
			typename std::enable_if_t<std::is_invocable_r_v<size_t, TApplyFunctor, decltype((*_pBuf).cbegin()), decltype((*_pBuf).cend())>, int> = 0>
			void apply( TApplyFunctor && applyf ){

			if( !eof() ){

				const size_t addoff = applyf( (*_pBuf).cbegin() + _offset, (*_pBuf).cbegin() + _count );
				_offset += addoff;
				}
			else{
				FATAL_ERROR_FWD(nullptr);
				}
			}

		_binary_t( const _binary_t & ) = delete;
		_binary_t& operator=( const _binary_t & ) = delete;
		};

	typedef std::unique_ptr<_binary_t> _binary_ptr_t;
	typedef std::function<void( _binary_ptr_t & bin )> flow_operation_t;

	std::queue<detail::_packet_t> _syncPackets;
	std::vector<flow_operation_t> _opflowStack;

	/*! ����������� ������ ������ */
	detail::_packet_accmulator_t _packAccm;
	std::weak_ptr<distributed_ctx_t> _ctx;

#ifdef CBL_MPF_OBJECTS_COUNTER_CHECKER_BIN_ACCEPTOR
	crm::utility::__xobject_counter_logger_t<_flow_packet_listener_t, 1> _xDbgCounter;
#endif

	/*! ���������� ������������������� ������ � ������ */
	void packet_accm_move_to_list()noexcept;

	/*! ����� ����� ���������� �������� */
	void op_hndl( flow_operation_t & op, _binary_ptr_t &  bin )noexcept;
	/*! ����������� ���������������� ������������������ */
	void op_sync_hndl( _binary_ptr_t & bin );
	/*! ����������� ���� */
	void op_type_hndl( _binary_ptr_t & bin );
	/*! ����������� �������� ���������� */
	void op_address_hndl( _binary_ptr_t & bin );
	/*! ����������� �������������� ���� */
	void op_typeid_hndl( _binary_ptr_t & bin );
	/*! ����������� ����� */
	void op_length_hndl( _binary_ptr_t & bin );
	/*! ���������� ��������� ������� */
	void op_timeline_hndl( _binary_ptr_t & bin );
	/*! ���������� �������� ������ ������ */
	void op_ldata_hndl( _binary_ptr_t & bin );
	/*! ���������� �������� ������ ������ */
	void op_mdata_hndl( _binary_ptr_t& bin );
	/*! ����������� ����������� ����� */
	void op_crc_hndl( _binary_ptr_t & bin );

	void _hndl_endop()noexcept;

	void ntf_hndl( std::unique_ptr<dcf_exception_t> && e )const noexcept;

public:
	~_flow_packet_listener_t();

	_flow_packet_listener_t( std::weak_ptr<distributed_ctx_t> ctx,
		const std::vector<uint8_t> & sync,
		const std::vector<uint8_t> & eof )noexcept;

	void push( const binary_vector_t & blob, size_t ) noexcept ;
	bool has_packet()const noexcept ;
	std::list<detail::_packet_t> capture_packets() noexcept ;
	};


class message_serializer_v0_t/*: public message_serializer_v0_t*/{
private:
	std::weak_ptr<distributed_ctx_t> _ctx;
	std::vector<uint8_t> _sync;
	std::vector<uint8_t> _eof;

#ifdef CBL_MPF_OBJECTS_COUNTER_CHECKER_BIN_EMITTER
	crm::utility::__xobject_counter_logger_t<message_serializer_v0_t, 1> _xDbgCounter;
#endif

	class _state_t{
	public:
		detail::crc_t crc;
		binary_vector_t bin;
		};

	template<typename O,
		typename S,
		typename std::enable_if_t < is_out_message_requirements_v<O>, int> = 0>
	void serialize_object( O && obj, S && s)const {
		static_assert(std::is_final_v<std::decay_t<O>>, __FILE_LINE__);

		srlz::serialize(std::forward<S>(s), std::forward<O>(obj));
	}

	template<typename _O,
		typename S,
		typename O = std::decay_t<_O>,
		typename std::enable_if_t < is_out_message_requirements_v<O>, int> = 0>
		void serialize_header(_state_t& st, _O&& obj, S&& w)const {


		/*#### ����� �������������: ������� ������ ������
		-----------------------------------------------------------*/
		srlz::serialize_raw(w, _sync);

		/*#### ������ ���������� ����������� ����� */
		if (obj.type().fl_use_crc())
			st.crc.reset();

		/*#### ��������� ���������
		----------------------------------------------------------*/
		if (obj.type().get() == 0)
			THROW_MPF_EXC_FWD(nullptr);

		/* ��� ��������� */
		if (is_null(obj.type_id())) {
			if constexpr (srlz::has_static_typeid_trait_v<O>) {
				obj.set_typeid_id(O::typeid_key().type_id());
				obj.set_typeid_name(O::typeid_key().type_name());

				CBL_VERIFY(obj.type().fl_typeid());
				}
			else if constexpr (is_i_typeid_based_v<O>) {
				obj.set_typeid_id(obj.type_id());
				obj.set_typeid_name(obj.type_name());

				CBL_VERIFY(obj.type().fl_typeid());
				}
			else {
				obj.set_typeid_id(srlz::make_type<O>::id());
				obj.set_typeid_name(srlz::make_type<O>::name());

				CBL_VERIFY(obj.type().fl_typeid());
				}
			}

		static_assert(std::is_fundamental_v<decltype(obj.type().get())>, __FILE_LINE__);
		srlz::serialize(w, (detail::iol_type_map_t::packed_type_t)obj.type().get());

		/* �������� ���������� */
		srlz::serialize(w, obj.address());

		/* ������������� ����[�����������] */
		if (obj.type().fl_typeid()) {

			/* �������� �������� �������������� */
			srlz::serialize(w, obj.type_id());
			}

#ifdef CBL_MPF_ENABLE_TIMETRACE
		srlz::serialize(w, obj.timeline());
#endif//CBL_MPF_ENABLE_TIMETRACE

		// ����� ����� ������[�����������]
		if constexpr (srlz::detail::is_srlzd_prefixed_v<O>) {
			CBL_VERIFY(obj.type().length() == detail::iol_length_spcf_t::prefixed);
			srlz::serialize(w, (detail::iol_type_map_t::packed_length_t) srlz::object_trait_serialized_size(obj));
			}
		else {
			CBL_VERIFY(obj.type().length() != detail::iol_length_spcf_t::prefixed);
			}
		}

	template<typename O,
		typename S,
		typename std::enable_if_t < is_out_message_requirements_v<O>, int> = 0>
	void serialize_body(_state_t &, O && obj, S && w)const {

		/* ������ ������������ �������� ����� � �������� */
		bool hasPackageLength_{ false };
		std::uintmax_t packageLength = 0;
		if constexpr(srlz::detail::is_i_srlzd_prefixed_v<O>) {
			hasPackageLength_ = true;
			packageLength = srlz::object_trait_serialized_size(obj);
		}

		/* ��������� ��������� � �������� ����� � ������������ ���������� ������������ ���� */
		size_t wrcnt = w.offset();

		/* ������������ ���� ��������� */
		serialize_object(obj, w);

		/* �������� ������������ ����� */
		if (hasPackageLength_) {

			wrcnt = w.offset() - wrcnt;
			if (wrcnt != packageLength) {
				std::string log;
				log += "_iol_ogtw_t::_nsync_slot_write_body: error , " + crm::detail::cvt_to_str(wrcnt) + ":" + crm::detail::cvt_to_str(packageLength);
				FATAL_ERROR_FWD(std::move(log));
			}
		}
	}

	template<typename O,
		typename S,
		typename std::enable_if_t < is_out_message_requirements_v<O>, int> = 0>
	void serialize_tail(_state_t & st, O && obj, S && w)const {

		/* ������� ����� ����� ������[�����������] */
		if constexpr (srlz::detail::is_i_srlzd_eof_v<O>) {
			srlz::serialize_raw(w, _eof);
		}

		/*#### ����������� �����
		----------------------------------------------------------*/
		if (obj.type().fl_use_crc())
			srlz::serialize(w, (detail::iol_type_map_t::packed_crc_t)st.crc.value());
	}

	template<typename O,
		typename S,
		typename std::enable_if_t < is_out_message_requirements_v<O>, int> = 0>
	void __serialize(S && os, _state_t & st, O && obj)const {
#ifdef CBL_MPF_ENABLE_TIMETRACE
		obj.set_timeline_serialize();
#endif//CBL_MPF_ENABLE_TIMETRACE

		/*#### ��������� ���������
		----------------------------------------------------------*/
		serialize_header(st, std::forward<O>(obj), std::forward<S>(os));

		/*#### ���� ���������
		----------------------------------------------------------*/
		serialize_body(st, std::forward<O>(obj), std::forward<S>(os));

		/*#### ����������� ���������
		----------------------------------------------------------*/
		serialize_tail(st, std::forward<O>(obj), std::forward<S>(os));
	}

	template<typename O,
		typename std::enable_if_t < is_out_message_requirements_v<O>, int> = 0>
	void __serialize_crc( _state_t & st, O && obj )const {

		/* ��������������� ������ ��� ������������ �������� */
		auto wstream = crm::srlz::wstream_constructor_t::make([&st](uint8_t byte) {
			st.crc.push(byte);
			});

		__serialize(wstream, st, obj);

		st.bin = wstream.release();
		}

	template<typename O,
		typename std::enable_if_t < is_out_message_requirements_v<O>, int> = 0>
	void __serialize_no_crc( _state_t & st, O && obj )const {

		/* ��������������� ������ ��� ������������ �������� */
		auto wstream = crm::srlz::wstream_constructor_t::make();
		__serialize(wstream, st, obj);

		st.bin = wstream.release();
		}


public:
	message_serializer_v0_t( std::weak_ptr<distributed_ctx_t> tdrv,
		const std::vector<uint8_t> & sync,
		const std::vector<uint8_t> & eof );

	template<typename O,
		typename std::enable_if_t <is_out_message_requirements_v<O>, int > = 0>
	binary_vector_t push(O && obj)const {
		CBL_VERIFY(obj.type().type() != detail::iol_types_t::st_notification ||( is_null(obj.source_subscriber_id()) && !is_sendrecv_request(obj.address())));

#pragma message( CBL_VAR_NAME_VALUE(CBL_BUILD_VAR_MESSAGE_ROADMAP_MARKER_OUTBOUND, 3))

		auto ctx(_ctx.lock());
		if (ctx) {
			if (auto  tDrv = ctx->types_driver()) {
				obj.set_typedriver(tDrv);

				if constexpr (srlz::detail::is_i_srlzd_prefixed_v<O>) {
					obj.setf_length();
					}

				_state_t st;

				if (obj.type().fl_use_crc()) {
					__serialize_crc(st, obj);
					}
				else {
					__serialize_no_crc(st, obj);
					}

				/* ��������������� ������������� ������� */
				return std::move(st.bin);
				}
			else {
				THROW_MPF_EXC_FWD(nullptr);
				}
			}
		else {
			THROW_MPF_EXC_FWD(nullptr);
			}
		}
	};


class default_binary_protocol_handler_t /*: public default_binary_protocol_handler_t*/{
private:
	std::weak_ptr<distributed_ctx_t> _ctx;
	std::vector<uint8_t> _sync;
	std::vector<uint8_t> _eof;

public:
	default_binary_protocol_handler_t( std::weak_ptr<distributed_ctx_t> ctx_ );
	default_binary_protocol_handler_t(std::weak_ptr<distributed_ctx_t> ctx_ ,
		const std::vector<uint8_t> & sync,
		const std::vector<uint8_t> & eof );

	std::unique_ptr<_flow_packet_listener_t> create_acceptor()const;
	std::unique_ptr<message_serializer_v0_t> create_emitter()const;

	std::unique_ptr< default_binary_protocol_handler_t> copy()const;
	};
}


