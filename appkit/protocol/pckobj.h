#pragma once


#include "../internal_invariants.h"
#include "./i_bincntr.h"
#include "./ipckobj.h"
#include "./opckobj.h"


namespace crm::detail{

class gtw_rt1_x_t : public i_iol_gtw_rt1_x_t {
private:
	/* �������� ���������� ������������� ����� */
	std::weak_ptr<distributed_ctx_t> _ctx;
	//! ������ ������
	std::atomic<bool> _cancel{false};
	/// ���� �������� ���������
	igtw_rt1_x_t _inGtw;
	/// ���� ��������� ���������
	ogtw_rt1_x_t _outGtw;


#ifdef CBL_MPF_OBJECTS_COUNTER_CHECKER_BIN_PACKAGE2OBJ_TRANSLATOR
	crm::utility::__xobject_counter_logger_t<gtw_rt1_x_t,1> _xDbgCounter;
#endif

	std::shared_ptr<distributed_ctx_t> ctx()noexcept;
	std::shared_ptr<const distributed_ctx_t> ctx()const noexcept;

	/*! ������� ��������� */
	bool canceled()const noexcept;

public:
	/*! �������� �������.
	\param channels ��������� ��������� �����/������
	*/
	gtw_rt1_x_t( std::weak_ptr<distributed_ctx_t> ctx,
		std::unique_ptr<message_serializer_v0_t> protocol )noexcept;

	gtw_rt1_x_t( const gtw_rt1_x_t & ) = delete;
	gtw_rt1_x_t& operator=(const gtw_rt1_x_t &) = delete;

	/*! �������� ��������� */
	void close()noexcept override;

	/*! ������ ����� ��������� */
	void start(detail::reverse_handlers&& r);

	~gtw_rt1_x_t();

	/*-------------------------------------------------------------------------
	i_iol_gtw_t
	----------------------------------------------------------------------------*/

	/*! �������� ������� �������� �������� */
	void set_input_queue( std::shared_ptr<i_iprocessed_handler_t> obj ) override;

	/*! ������ �� ������� �������� �������� */
	std::shared_ptr<i_iprocessed_handler_t> get_input_queue()noexcept override;

	/*! ������ �� ������� �������� �������� */
	std::shared_ptr<const i_iprocessed_handler_t> get_input_queue()const noexcept override;

	/*! �������� �� ��������� ��������� ��� ���������� ��������������.
	\param id ������� �������������
	\param once ���� ������� true, ����������� �������� ���� ���, ����� �� ��� ���,
	���� �� ����� ��������� ������ ����������� � ������� ������ /ref unsubscribe_hndl_id.
	*/
	subscribe_invoker_result subscribe_hndl_id(async_space_t scx,
		const _address_hndl_t & id, 
		subscribe_options opt,
		subscribe_message_handler && handler )final;

	/*! ����� �� ��������� ����������� ��� ���������� ��������������
	\param id ������������� ����������
	*/
	void unsubscribe_hndl_id( const _address_hndl_t & id )noexcept final;

	/* -------------------------------------------------------------------------
	i_iol_ogtw_t
	-----------------------------------------------------------------------------*/

	template<typename O,
		typename std::enable_if_t < is_out_message_requirements_v<O>, int> = 0>
	outbound_message_unit_t serialize_gateout(O && obj)const {
		return _outGtw.serialize_gateout(std::forward<O>(obj));
		}

	 void push(message_frame_unit_t && pck) final;

	 void addressed_exception(const _address_hndl_t & address,
		 std::unique_ptr<dcf_exception_t> && e,
		 invoke_context_trait)noexcept final;

#ifdef CBL_USE_SMART_OBJECTS_COUNTING
	 crm::log::counter_t subcounter(const std::string & name)override;
#endif//CBL_USE_SMART_OBJECTS_COUNTING
	};
}
