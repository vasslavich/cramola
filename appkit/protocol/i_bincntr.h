#pragma once


#include "../internal_invariants.h"
#include "../channel_terms/internal_terms.h"
#include "../queues/i_message_queue.h"


namespace crm::detail {


class i_gtw_ostream_t {
public:
	virtual ~i_gtw_ostream_t() = 0;

	virtual void push(binary_vector_t && bin) = 0;
	};


class i_iol_igtw_base_t {
public:
	virtual ~i_iol_igtw_base_t() = 0;

	/*! ��������� ��������� ��������� */
	typedef input_item_t iprocessed_t;

	/*! ���������������� ������� �������������� ������� �������� */
	typedef i_message_input_t<iprocessed_t> i_iprocessed_handler_t;
	};

using inbound_message_handler = std::function<void( std::unique_ptr<ifunctor_package_t> && mf )>;

class reverse_handlers {
	inbound_message_with_tail_exception_guard_handler_by_address _exch;
	inbound_message_handler _fh;

public:
	reverse_handlers()noexcept;

	reverse_handlers(inbound_message_with_tail_exception_guard_handler_by_address&& exch_,
		inbound_message_handler&& fh_);

	void operator()(const address_descriptor_t& addr, std::unique_ptr<dcf_exception_t>&&)noexcept;
	void operator()(std::unique_ptr<ifunctor_package_t>&& mf);

	const inbound_message_with_tail_exception_guard_handler_by_address& exception_handler()const noexcept;
	};


class i_iol_gtw_t : public virtual i_iol_igtw_base_t {
public:
	/*! ������ ���������. */
	virtual void start(reverse_handlers && rh) = 0;

	virtual void close()noexcept = 0;

	/*! �������� �� ��������� ��������� ��� ���������� ��������������.
	\param id ������� �������������
	\param once ���� ������� true, ����������� �������� ���� ���, ����� �� ��� ���,
	���� �� ����� ��������� ������ ����������� � ������� ������ /ref unsubscribe_hndl_id.
	*/
	virtual subscribe_invoker_result subscribe_hndl_id(async_space_t scx, const _address_hndl_t & id, subscribe_options opt,
		subscribe_message_handler && handler ) = 0;

	/*! ����� �� ��������� ����������� ��� ���������� ��������������
	\param id ������������� ����������
	*/
	virtual void unsubscribe_hndl_id(const _address_hndl_t & id)noexcept = 0;

	/*! �������� ������� �������� �������� */
	virtual void set_input_queue(std::shared_ptr<i_iprocessed_handler_t> obj) = 0;

	/*! ������ �� ������� �������� �������� */
	virtual std::shared_ptr<i_iprocessed_handler_t> get_input_queue()noexcept = 0;

	/*! ������ �� ������� �������� �������� */
	virtual std::shared_ptr<const i_iprocessed_handler_t> get_input_queue()const noexcept = 0;
	
	virtual void push( const binary_vector_t & blob, size_t)noexcept = 0;
	virtual 	std::vector<std::unique_ptr<i_iol_ihandler_t>> decode(const binary_vector_t& blob, size_t) = 0;

#ifdef CBL_USE_SMART_OBJECTS_COUNTING
	virtual crm::log::counter_t subcounter(const std::string & name) = 0;
#endif//CBL_USE_SMART_OBJECTS_COUNTING
	};


class i_iol_gtw_rt1_x_t : public virtual i_iol_igtw_base_t {
public:
	/*! �������� �� ��������� ��������� ��� ���������� ��������������.
	\param id ������� �������������
	\param once ���� ������� true, ����������� �������� ���� ���, ����� �� ��� ���,
	���� �� ����� ��������� ������ ����������� � ������� ������ /ref unsubscribe_hndl_id.
	*/
	virtual subscribe_invoker_result subscribe_hndl_id(async_space_t scx, const _address_hndl_t & id, subscribe_options opt,
		subscribe_message_handler && handler ) = 0;

	/*! ����� �� ��������� ����������� ��� ���������� ��������������
	\param id ������������� ����������
	*/
	virtual void unsubscribe_hndl_id( const _address_hndl_t & id )noexcept = 0;

	/*! �������� ������� �������� �������� */
	virtual void set_input_queue(std::shared_ptr<i_iprocessed_handler_t> obj) = 0;

	/*! ������ �� ������� �������� �������� */
	virtual std::shared_ptr<i_iprocessed_handler_t> get_input_queue()noexcept = 0;

	/*! ������ �� ������� �������� �������� */
	virtual std::shared_ptr<const i_iprocessed_handler_t> get_input_queue() const noexcept  = 0;

	/*! ������ ���������. */
	virtual void start(reverse_handlers &&) = 0;

	virtual void push(message_frame_unit_t && pck) = 0;

	virtual void addressed_exception(const _address_hndl_t & address,
		std::unique_ptr<dcf_exception_t> && e,
		invoke_context_trait )noexcept = 0;

#ifdef CBL_USE_SMART_OBJECTS_COUNTING
	virtual crm::log::counter_t subcounter(const std::string & name) = 0;
#endif//CBL_USE_SMART_OBJECTS_COUNTING

	virtual void close()noexcept = 0;
	};
}


