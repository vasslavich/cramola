#pragma once

#include <functional>
#include <memory>
#include "../internal_invariants.h"
#include "./i_bincntr.h"


namespace crm::detail{


/*! ���� ��������� ��������� */
class _iol_ogtw_t : public virtual i_iol_igtw_base_t {
private:
	std::unique_ptr<message_serializer_v0_t> _emitter;
	std::weak_ptr<distributed_ctx_t> _ctx;

#ifdef CBL_MPF_OBJECTS_COUNTER_CHECKER_BIN_OBJECT2BIN_TRANSLATOR
	crm::utility::__xobject_counter_logger_t<_iol_ogtw_t, 1> _xDbgCounter;
#endif

	template<typename _O,
		typename O = typename std::decay_t<_O>,
		typename std::enable_if_t <is_out_message_requirements_v<O> && std::is_base_of_v<obinary_vector_t,O> , int > = 0>
		outbound_message_unit_t write_obin(O && obj)const {
#pragma message( CBL_VAR_NAME_VALUE(CBL_BUILD_VAR_MESSAGE_ROADMAP_MARKER_OUTBOUND, 21))

		if (obj.type().type() != iol_types_t::st_binary_serialized)
			THROW_MPF_EXC_FWD(nullptr);

#pragma message( CBL_VAR_NAME_VALUE(CBL_BUILD_VAR_MESSAGE_ROADMAP_MARKER_OUTBOUND, 22))

		if (is_sendrecv_cycle(obj)) {
			CBL_VERIFY(is_sendrecv_request(obj) || is_sendrecv_response(obj));
			CBL_VERIFY(CBL_IS_SNDRCV_KEY_SPIN_TAGGED_ON(obj));

			return outbound_message_unit_t::make("_iol_ogtw_t::write_obin-1", std::move(obj).blob(), obj, obj.reset_invoke_exception_guard());
			}
		else {
			CBL_VERIFY(!(is_sendrecv_request(obj) || is_sendrecv_response(obj)));
			CBL_VERIFY(!obj.is_reverse_exception_guard());
			CBL_VERIFY(CBL_IS_SNDRCV_KEY_SPIN_TAGGED_OFF(obj) || obj.type().type() == iol_types_t::st_ping);

			return outbound_message_unit_t::make("_iol_ogtw_t::write_obin-2", std::move(obj).blob());
			}
		}

		template<typename O,
		typename std::enable_if_t <is_out_message_requirements_v<O>, int > = 0>
			outbound_message_unit_t write_obj(O && obj)const {

#pragma message( CBL_VAR_NAME_VALUE(CBL_BUILD_VAR_MESSAGE_ROADMAP_MARKER_OUTBOUND, 23))

			if (is_sendrecv_cycle(obj)) {
				CBL_VERIFY_SNDRCV_KEY_SPIN_TAG(obj);
				CBL_VERIFY(is_sendrecv_request(obj) || is_sendrecv_response(obj));
				CBL_VERIFY(CBL_IS_SNDRCV_KEY_SPIN_TAGGED_ON(obj));

				outbound_message_desc_t mdesc(obj);
				auto mresponseGuard = obj.reset_invoke_exception_guard();
				auto mbinary = _emitter->push(std::forward<O>(obj));

				return outbound_message_unit_t::make("_iol_ogtw_t::write_obj-1", std::move(mbinary), std::move(mdesc), std::move(mresponseGuard));
				}
			else {
				CBL_VERIFY(!(is_sendrecv_request(obj) || is_sendrecv_response(obj)));
				CBL_VERIFY(!obj.is_reverse_exception_guard());
				CBL_VERIFY(CBL_IS_SNDRCV_KEY_SPIN_TAGGED_OFF(obj) || obj.type().type() == iol_types_t::st_ping);

				return outbound_message_unit_t::make("_iol_ogtw_t::write_obj-2", _emitter->push(std::forward<O>(obj)));
				}
			}

public:
	_iol_ogtw_t(const _iol_ogtw_t &) = delete;
	_iol_ogtw_t& operator=(const _iol_ogtw_t &) = delete;

	_iol_ogtw_t(std::weak_ptr<distributed_ctx_t> ctx,
		std::unique_ptr<message_serializer_v0_t> && emitter );

	/*! ������� ������������� �� ������� �������� ������ ������ */
	rtl_level_t level()const;

	template<typename _O,
		typename O = typename std::decay_t<_O>,
		typename std::enable_if_t <is_out_message_requirements_v<O>, int > = 0>
	outbound_message_unit_t serialize_gateout( _O && obj )const {
		CBL_VERIFY(obj.type().type() != iol_types_t::st_notification || (is_null(obj.source_subscriber_id()) && !is_sendrecv_request(obj.address())));

#pragma message( CBL_VAR_NAME_VALUE(CBL_BUILD_VAR_MESSAGE_ROADMAP_MARKER_OUTBOUND, 1))

		MPF_TEST_EMULATION_IN_CONTEXT_COND_TRCX(_ctx.lock(), 403,
			is_trait_at_emulation_context(obj),
			trait_at_keytrace_set(obj));

		if constexpr (std::is_base_of_v<obinary_vector_t, O>) {
			CBL_VERIFY(obj.type().type() == iol_types_t::st_binary_serialized);
			return write_obin(std::forward<_O>(obj));
			}
		else {
			return write_obj(std::forward<_O>(obj));
			}
		}
	};
}
