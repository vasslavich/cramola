#include "../../internal.h"
#include "../i_bincntr.h"
#include "../../exceptions.h"



using namespace crm;
using namespace crm::detail;


i_gtw_ostream_t::~i_gtw_ostream_t(){}
i_iol_igtw_base_t::~i_iol_igtw_base_t(){}
//_flow_packet_listener_t::~_flow_packet_listener_t(){}
//default_binary_protocol_handler_t::~default_binary_protocol_handler_t(){}
//message_serializer_v0_t::~message_serializer_v0_t(){}


reverse_handlers::reverse_handlers()noexcept {}

reverse_handlers::reverse_handlers(inbound_message_with_tail_exception_guard_handler_by_address&& exch_,
	inbound_message_handler&& fh_)
	: _exch(std::move(exch_))
	, _fh(std::move(fh_)){}

void reverse_handlers::operator()(const address_descriptor_t& addr, std::unique_ptr<dcf_exception_t>&& e)noexcept {
	if (_exch) {
		_exch(std::move(addr), std::move(e));
		}
	}

void reverse_handlers::operator()(std::unique_ptr<ifunctor_package_t>&& mf) {
	if (_fh) {
		_fh(std::move(mf));
		}
	}

const inbound_message_with_tail_exception_guard_handler_by_address& reverse_handlers::exception_handler()const noexcept {
	return _exch;
	}
