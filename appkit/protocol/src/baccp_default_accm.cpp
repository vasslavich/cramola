#include "../../internal.h"
#include "../baccp_default.h"
#include "../../utilities/utilities.h"


using namespace crm;
using namespace crm::detail;


static_assert(srlz::detail::is_i_srlzd_prefixed_v<address_descriptor_t>, __FILE_LINE__);
static_assert(srlz::detail::is_i_srlzd_prefixed_v<iol_typeid_spcf_t>, __FILE_LINE__);
static_assert(srlz::is_read_accumulate_supported_v<address_descriptor_t>, __FILE_LINE__);
static_assert(srlz::is_read_accumulate_supported_v<iol_typeid_spcf_t>, __FILE_LINE__);


/*##############################################################################################

_packet_accmulator_t

################################################################################################*/


_packet_accmulator_t::_packet_accmulator_t()noexcept {}

_packet_accmulator_t::_packet_accmulator_t( _packet_accmulator_t  &&o )noexcept {
	assign( std::move( o ) );
	}

_packet_accmulator_t& _packet_accmulator_t::operator=(_packet_accmulator_t &&o)noexcept {
	if(this != std::addressof(o)) {
		assign(std::move(o));
		}

	return (*this);
	}

void _packet_accmulator_t::assign( _packet_accmulator_t &&o ) noexcept{
	_typeField = std::move( o._typeField );
	_address = std::move( o._address );
	_typeid = std::move( o._typeid );

#ifdef CBL_MPF_ENABLE_TIMETRACE
	_timeline = std::move( o._timeline );
#endif//CBL_MPF_ENABLE_TIMETRACE

	_prefLength = std::move( o._prefLength );
	_crc = std::move( o._crc );
	_eofFactor = std::move( o._eofFactor );
	_crcCalculated = std::move( o._crcCalculated );
	_data = std::move( o._data );
	_state = std::move( o._state );
	_syncLine = std::move( o._syncLine );

	o.reset();
	}

void _packet_accmulator_t::set_state( state_t st ) {
	if( st == state_t::sync )
		reset();

	_state = st;
	}

void _packet_accmulator_t::set_sync_line( const std::vector<uint8_t> & line ) {
	_syncLine.assign( line.cbegin(), line.cend() );
	}

bool _packet_accmulator_t::sync()const {
	return _syncLine.checkline() == _syncLine.syncmask();
	}

_packet_accmulator_t::state_t _packet_accmulator_t::state()const {
	return _state;
	}

size_t _packet_accmulator_t::add( const uint8_t* first, const uint8_t* last ) {
	return tadd( first, last );
	}

size_t _packet_accmulator_t::add( const std::vector<uint8_t> & line ) {
	return tadd( line.cbegin(), line.cend() );
	}

size_t _packet_accmulator_t::add( std::vector<uint8_t>::const_iterator first, 
	std::vector<uint8_t>::const_iterator last ) {

	return tadd( first, last );
	}

void _packet_accmulator_t::add( uint8_t byte ) {
	tadd( (&byte), (&byte) + 1 );
	}

bool _packet_accmulator_t::type_ready()const {
	return _typeField.ready();
	}

iol_type_spcf_t _packet_accmulator_t::type()const {
	CBL_VERIFY(type_ready());

	iol_type_spcf_t value;
	value.set( _typeField.get() );
	return value;
	}

bool _packet_accmulator_t::typeid_ready()const {
	return _typeid.ready();
	}

const iol_typeid_spcf_t& _packet_accmulator_t::typeid_()const {
	return _typeid.get();
	}

type_identifier_t _packet_accmulator_t::typeid_id()const{
	return _typeid.get().type_id();
	}

std::string _packet_accmulator_t::typeid_name()const {
	return _typeid.get().type_name();
	}

bool _packet_accmulator_t::pref_length_ready()const {
	return _prefLength.ready();
	}

iol_type_map_t::packed_length_t _packet_accmulator_t::pref_length()const {
	return _prefLength.get();
	}

bool _packet_accmulator_t::address_ready()const {
	return _address.ready();
	}

const address_descriptor_t& _packet_accmulator_t::address()const & {
	return _address.get();
	}

address_descriptor_t _packet_accmulator_t::address()&& {
	return std::move(_address).get();
	}

void _packet_accmulator_t::set_eof( const std::vector<uint8_t> & line ) {
	_eofFactor.assign( line.cbegin(), line.cend() );
	}

bool _packet_accmulator_t::eof()const {
	return _eofFactor.syncmask() == _eofFactor.checkline();
	}

bool _packet_accmulator_t::crc_ready()const {
	return _crc.ready();
	}

iol_type_map_t::packed_crc_t _packet_accmulator_t::crc()const {
	return _crc.get();
	}

iol_type_map_t::packed_crc_t _packet_accmulator_t::crc_calculated()const {
	return _crcCalculated.value();
	}

void _packet_accmulator_t::data_reserve( size_t capacity ) {
	_data.reserve( capacity );
	}

const binary_vector_t& _packet_accmulator_t::data()const {
	return _data;
	}

binary_vector_t _packet_accmulator_t::data() {
	return std::move(_data);
	}

bool _packet_accmulator_t::data_ready()const {
	return (type().length() == iol_length_spcf_t::zeroed
		|| (type().length() == iol_length_spcf_t::prefixed && pref_length() == data_length())
		|| (type().length() == iol_length_spcf_t::eof_marked_ && eof()));
	}

size_t _packet_accmulator_t::data_length()const {
	return _data.size();
	}

void _packet_accmulator_t::reset() {
	_typeField.reset();
	_address.reset();
	_typeid.reset();

#ifdef CBL_MPF_ENABLE_TIMETRACE
	_timeline.reset();
#endif//CBL_MPF_ENABLE_TIMETRACE

	_prefLength.reset();
	_crc.reset();

	_crcCalculated.reset();
	_syncLine.checkline_reset();
	_eofFactor.checkline_reset();
	_data.clear();

	_state = state_t::sync;
	}

std::string _packet_accmulator_t::info()const {
	std::string sbuf;

	if( _typeField.ready() ) {
		sbuf += "typefield " + cvt_to_str( _typeField.get()) + ";";
		}

	if( _prefLength.ready() ) {
		sbuf += "length type " +  cvt_to_str(_prefLength.get()) + ";";
		}

	if( _typeid.ready() ) {
		sbuf += "typeid " + _typeid.get().type_id().to_str() + ":" + _typeid.get().type_name() + ";";
		}

	if( !_data.empty() ) {
		sbuf += "data size " + cvt_to_str(_data.size()) + ";";
		}

	if( _crc.ready() ) {
		sbuf += "crc " + cvt_to_str(_crc.get()) + ": calculated " + cvt_to_str(_crcCalculated.value()) + ";";
		}

	return std::move( sbuf );
	}

bool _packet_accmulator_t::ready()const {
	return _crc.ready();
	}

void _packet_accmulator_t::typefield_setmark(){
	if( !type().fl_use_crc() ){
		_crcCalculated.reset();
	}
}

bool _packet_accmulator_t::timeline_ready()const {
#ifdef CBL_MPF_ENABLE_TIMETRACE
	return _timeline.ready();
#else
	return true;
#endif//CBL_MPF_ENABLE_TIMETRACE
	}

#ifdef CBL_MPF_ENABLE_TIMETRACE
const hanlder_times_stack_t& _packet_accmulator_t::timeline()const &{
	return _timeline.get();
	}
hanlder_times_stack_t _packet_accmulator_t::timeline()&& {
	return std::move(_timeline).get();
	}
#endif//CBL_MPF_ENABLE_TIMETRACE

_packet_t _packet_accmulator_t::release() {
	CBL_VERIFY(type().type() != iol_types_t::st_notification ||
		(!address_ready() || !is_sendrecv_request(address())));

	return _packet_t(
		type(),
		address_ready() ? address() : address_descriptor_t(),
		typeid_ready() ? typeid_id() : type_identifier_t::null,
		typeid_ready() ? typeid_name() : "",
		type().fl_use_crc() ? crc() : 0,
		data()
#ifdef CBL_MPF_ENABLE_TIMETRACE
		, timeline()
#endif//CBL_MPF_ENABLE_TIMETRACE
		);
	}

