#include "../../internal.h"
#include "../baccp_default.h"
#include "../../context/context.h"
#include "../../utilities/convert/cvt.h"


using namespace crm;
using namespace crm::detail;


message_serializer_v0_t::message_serializer_v0_t( std::weak_ptr<distributed_ctx_t> tdrv,
	const std::vector<uint8_t> & sync,
	const std::vector<uint8_t> & eof )
	: _ctx(tdrv)
	, _sync( sync )
	, _eof( eof ) {}
