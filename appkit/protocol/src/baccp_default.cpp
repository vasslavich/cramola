#include "../../internal.h"
#include "../baccp_default.h"
#include "../../context/context.h"


using namespace crm;
using namespace crm::detail;


default_binary_protocol_handler_t::default_binary_protocol_handler_t( std::weak_ptr<distributed_ctx_t> ctx_ )
	: default_binary_protocol_handler_t( ctx_,
		CHECK_PTR( ctx_ )->policies().sync_sequence(),
		CHECK_PTR( ctx_ )->policies().eof_sequence() ) {}

default_binary_protocol_handler_t::default_binary_protocol_handler_t(std::weak_ptr<distributed_ctx_t> ctx_ ,
	const std::vector<uint8_t> & sync,
	const std::vector<uint8_t> & eof )
	: _ctx(ctx_)
	, _sync( sync )
	, _eof( eof ) {}

std::unique_ptr<_flow_packet_listener_t> default_binary_protocol_handler_t::create_acceptor()const {
	return std::make_unique<_flow_packet_listener_t>( _ctx, _sync, _eof );
	}

std::unique_ptr<message_serializer_v0_t> default_binary_protocol_handler_t::create_emitter()const{
	return std::make_unique<message_serializer_v0_t>(_ctx, _sync, _eof );
	}

std::unique_ptr< default_binary_protocol_handler_t> default_binary_protocol_handler_t::copy()const {
	return std::make_unique< default_binary_protocol_handler_t>(*this);
	}

