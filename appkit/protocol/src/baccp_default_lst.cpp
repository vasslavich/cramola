#include "../../internal.h"
#include "../baccp_default.h"
#include "../i_bincntr.h"
#include "../../context/context.h"


using namespace crm;
using namespace crm::detail;



/*##############################################################################################

_flow_packet_listener_t

################################################################################################*/


_flow_packet_listener_t::_flow_packet_listener_t(std::weak_ptr<distributed_ctx_t> ctx_,
	const std::vector<uint8_t> & sync,
	const std::vector<uint8_t> & eof )noexcept
	: _ctx(ctx_){

	_opflowStack.resize( (size_t)_packet_accmulator_t::state_t::__count );

	_opflowStack[(size_t)_packet_accmulator_t::state_t::sync] = 
		std::bind( &_flow_packet_listener_t::op_sync_hndl, this, std::placeholders::_1 );

	_opflowStack[(size_t)_packet_accmulator_t::state_t::type] =
		std::bind(&_flow_packet_listener_t::op_type_hndl, this, std::placeholders::_1);

	_opflowStack[(size_t)_packet_accmulator_t::state_t::address] =
		std::bind(&_flow_packet_listener_t::op_address_hndl, this, std::placeholders::_1);

	_opflowStack[(size_t)_packet_accmulator_t::state_t::type_id] =
		std::bind(&_flow_packet_listener_t::op_typeid_hndl, this, std::placeholders::_1);

	_opflowStack[(size_t)_packet_accmulator_t::state_t::timeline] =
		std::bind(&_flow_packet_listener_t::op_timeline_hndl, this, std::placeholders::_1);

	_opflowStack[(size_t)_packet_accmulator_t::state_t::length] =
		std::bind(&_flow_packet_listener_t::op_length_hndl, this, std::placeholders::_1);

	_opflowStack[(size_t)_packet_accmulator_t::state_t::ldata] =
		std::bind(&_flow_packet_listener_t::op_ldata_hndl, this, std::placeholders::_1);

	_opflowStack[(size_t)_packet_accmulator_t::state_t::mdata] =
		std::bind(&_flow_packet_listener_t::op_mdata_hndl, this, std::placeholders::_1);

	_opflowStack[(size_t)_packet_accmulator_t::state_t::crc] =
		std::bind(&_flow_packet_listener_t::op_crc_hndl, this, std::placeholders::_1);

	_packAccm.set_sync_line( sync );
	_packAccm.set_eof( eof );
	}

void _flow_packet_listener_t::ntf_hndl(std::unique_ptr<dcf_exception_t> && e)const noexcept {
	if(auto c = _ctx.lock()) {
		c->exc_hndl(std::move(e));
		}
	}

_flow_packet_listener_t::~_flow_packet_listener_t() {
	CBL_VERIFY(_syncPackets.empty());
	}

void _flow_packet_listener_t::packet_accm_move_to_list()noexcept {
	CBL_MPF_KEYTRACE_SET(_packAccm.address(), 31);

	bool errorf{ false };

	if(_packAccm.type().fl_use_crc()) {

		if(!_packAccm.ready()) {
			CBL_MPF_KEYTRACE_SET(_packAccm.address(), 370);
			errorf = true;
			}

		if(_packAccm.crc_calculated() != _packAccm.crc()) {
			CBL_MPF_KEYTRACE_SET(_packAccm.address(), 371);
			errorf = true;
			}
		}

	if(!errorf) {
		_syncPackets.push(_packAccm.release());
		}
	else {
		_syncPackets.push(_packet_t::make_error(_packAccm.address()));
		}

	_packAccm.reset();
	}

void _flow_packet_listener_t::op_hndl(flow_operation_t & op, _binary_ptr_t & bin)noexcept {

	bool errf = true;
	try {
		op(bin);
		errf = false;
		}
	catch(const dcf_exception_t & e0) {
		ntf_hndl(e0.dcpy());
		}
	catch(const std::exception & e1) {
		ntf_hndl(CREATE_MPF_PTR_EXC_FWD(e1));
		}
	catch(...) {
		ntf_hndl(CREATE_MPF_PTR_EXC_FWD(nullptr));
		}

	if(errf) {
		_syncPackets.push(_packet_t::make_error(_packAccm.address()));

		_packAccm.reset();
		_packAccm.set_state(_packet_accmulator_t::state_t::sync);
		}
	}

void _flow_packet_listener_t::op_sync_hndl( _binary_ptr_t & bin ) {

	CBL_VERIFY( _packAccm.state() == _packet_accmulator_t::state_t::sync );
	while( !bin->eof() ) {

		bin->apply( [this]( binary_vector_t::const_iterator first, binary_vector_t::const_iterator last ) {
			return _packAccm.add( first, last );
			} );

		if( _packAccm.sync() ) {

			// ������� � ���������� �����������
			_packAccm.set_state( _packet_accmulator_t::state_t::type );
			break;
			}
		}
	}

void _flow_packet_listener_t::op_type_hndl(	_binary_ptr_t & bin ) {

	CBL_VERIFY( _packAccm.state() == _packet_accmulator_t::state_t::type );
	while( !bin->eof() ) {

		bin->apply( [this]( binary_vector_t::const_iterator first, binary_vector_t::const_iterator last ) {
			return _packAccm.add( first, last );
			} );

		if( _packAccm.type_ready() ) {

			/* ������� � ����������� ���� ������������� */
			_packAccm.typefield_setmark();

			// ������� � ���������� �����������
			_packAccm.set_state( _packet_accmulator_t::state_t::address );
			break;
			}
		}
	}

void _flow_packet_listener_t::op_address_hndl( _binary_ptr_t & bin ) {

	CBL_VERIFY( _packAccm.state() == _packet_accmulator_t::state_t::address );
	while( !bin->eof() ) {

		bin->apply( [this]( binary_vector_t::const_iterator first, binary_vector_t::const_iterator last ){
			return _packAccm.add( first, last );
			} );

		if( _packAccm.address_ready() ) {

			CBL_MPF_KEYTRACE_SET(_packAccm.address(), 30);

			// ������� � ���������� �����������
			_packAccm.set_state( _packet_accmulator_t::state_t::type_id );
			break;
			}
		}
	}

void _flow_packet_listener_t::op_typeid_hndl(	_binary_ptr_t & bin ) {

	CBL_VERIFY( _packAccm.state() == _packet_accmulator_t::state_t::type_id );
	/* ���� ������������ ������� �������������� ���� */
	if( _packAccm.type().fl_typeid() ) {

		while( !bin->eof() ) {

			/* ����������� �������� ����� */
			bin->apply( [this]( binary_vector_t::const_iterator first, binary_vector_t::const_iterator last ) {
				return _packAccm.add( first, last );
				} );

			if( _packAccm.typeid_ready() ) {
				// ������� � ���������� �����������
				_packAccm.set_state( _packet_accmulator_t::state_t::timeline );
				break;
				}
			}
		}
	else {

		// ������� � ���������� �����������
		_packAccm.set_state( _packet_accmulator_t::state_t::timeline );
		}
	}

void _flow_packet_listener_t::op_timeline_hndl(_binary_ptr_t & bin ) {

	CBL_VERIFY( _packAccm.state() == _packet_accmulator_t::state_t::timeline );
	/* ���� ������������ ������� �������������� ��������� ������� */
	if( _packAccm.type().fl_timeline() ) {

		while( !bin->eof() ) {

			/* ����������� �������� ����� */
			bin->apply( [this]( binary_vector_t::const_iterator first, binary_vector_t::const_iterator last ) {
				return _packAccm.add( first, last );
				} );

			if( _packAccm.timeline_ready() ) {

				// ������� � ���������� �����������
				_packAccm.set_state( _packet_accmulator_t::state_t::length );
				break;
				}
			}
		}
	else {

		// ������� � ���������� �����������
		_packAccm.set_state( _packet_accmulator_t::state_t::length );
		}
	}

void _flow_packet_listener_t::op_length_hndl(_binary_ptr_t & bin ) {

	CBL_VERIFY( _packAccm.state() == _packet_accmulator_t::state_t::length );

	/* ��������� ������������������ ������ � ������� ��������� � �������� */
	if( _packAccm.type().length() == iol_length_spcf_t::prefixed ) {

		while( !bin->eof() ) {

			/* ����������� �������� ����� */
			bin->apply( [this]( binary_vector_t::const_iterator first, binary_vector_t::const_iterator last ) {
				return _packAccm.add( first, last );
				} );

			if( _packAccm.pref_length_ready() ) {

				/* ��������� ������� ������ �� ���������� �������� � �������� */
				_packAccm.data_reserve( _packAccm.pref_length() );

				// ������� � ���������� �����������
				_packAccm.set_state( _packet_accmulator_t::state_t::ldata );
				break;
				}
			}
		}

	/* ��������� eof-������������� ������(� ��������� ����� ����� ������) */
	else if( _packAccm.type().length() == iol_length_spcf_t::eof_marked_ ) {

		_packAccm.set_state( _packet_accmulator_t::state_t::mdata );
		}

	/* ��������� ������ ������� ����� */
	else if( _packAccm.type().length() == iol_length_spcf_t::zeroed ) {

		if( _packAccm.type().fl_use_crc() )
			_packAccm.set_state( _packet_accmulator_t::state_t::crc );
		else
			_hndl_endop();
		}

	else {
		THROW_MPF_EXC_FWD(nullptr);
		}
	}

void _flow_packet_listener_t::op_ldata_hndl(_binary_ptr_t & bin ) {

	CBL_VERIFY( _packAccm.state() == _packet_accmulator_t::state_t::ldata );
	if( _packAccm.type().length() == iol_length_spcf_t::prefixed ) {
		
		bin->apply( [this]( binary_vector_t::const_iterator first, binary_vector_t::const_iterator last ) {
			return _packAccm.add( first, last );
			} );

		if( _packAccm.data_length() == _packAccm.pref_length() ) {

			// ������� � ���������� �����������
			if( _packAccm.type().fl_use_crc() )
				_packAccm.set_state( _packet_accmulator_t::state_t::crc );
			else
				_hndl_endop();
			}
		}
	else {
		THROW_MPF_EXC_FWD(nullptr);
		}
	}

void _flow_packet_listener_t::op_mdata_hndl(_binary_ptr_t & bin ) {

	CBL_VERIFY( _packAccm.state() == _packet_accmulator_t::state_t::mdata );
	if( _packAccm.type().length() == iol_length_spcf_t::eof_marked_ ) {

		if( _packAccm.state() != _packet_accmulator_t::state_t::mdata )
			THROW_MPF_EXC_FWD(nullptr);
		if( !_packAccm.type_ready() )
			THROW_MPF_EXC_FWD(nullptr);

		while( !bin->eof() ) {

			bin->apply( [this]( binary_vector_t::const_iterator first, binary_vector_t::const_iterator last ) {
				return _packAccm.add( first, last );
				} );

			/*! �������� ������� ����� ����� ������ */
			if( _packAccm.eof() ) {

				// ������� � ���������� �����������
				if( _packAccm.type().fl_use_crc() )
					_packAccm.set_state( _packet_accmulator_t::state_t::crc );
				else
					_hndl_endop();

				break;
				}
			}
		}
	else {
		THROW_MPF_EXC_FWD(nullptr);
		}
	}

void _flow_packet_listener_t::op_crc_hndl(_binary_ptr_t & bin) {

	CBL_VERIFY(_packAccm.state() == _packet_accmulator_t::state_t::crc);

	if(_packAccm.type().fl_use_crc()) {
		while(!bin->eof()) {

			/* ����������� �������� ����� */
			bin->apply([this](binary_vector_t::const_iterator first, binary_vector_t::const_iterator last) {
				return _packAccm.add(first, last);
				});

			if(_packAccm.crc_ready()) {

				_hndl_endop();
				break;
				}
			}
		}
	else {
		_hndl_endop();
		}
	}

void _flow_packet_listener_t::_hndl_endop()noexcept{

	// ��������� ����� � ������ ������������������
	packet_accm_move_to_list();

	// ������� � ������ ���������
	_packAccm.set_state( _packet_accmulator_t::state_t::sync );
}

/* [INPUT:������]:
��������� �������� ������������������ � ������� ����� ������������ */
void _flow_packet_listener_t::push( const binary_vector_t & blob, size_t count ) noexcept{

	_binary_ptr_t raw = std::make_unique<_binary_t>( blob, count );

	/* ������ �� ��������� ������������:
	��������� ������������� ������ ��� ���������� ����� ������� ������������������,
	������������� ���������� ����������� ���������� ������ �������� ����������� */
	while( !raw->eof() ) {
		const size_t opId = (size_t)_packAccm.state();
		if( opId >= _opflowStack.size() )
			FATAL_ERROR_FWD(nullptr);

		op_hndl( _opflowStack[opId], raw );
		}
	}

/* [INPUT:������]: 
������ ��������������� ������������������� ������ */
std::list<_packet_t> _flow_packet_listener_t::capture_packets()noexcept {
	std::list<_packet_t> result;
	while( !_syncPackets.empty() ) {
		result.push_back( std::move( _syncPackets.front() ) );
		_syncPackets.pop();
		}

	return result;
	}

bool _flow_packet_listener_t::has_packet()const noexcept{
	return !_syncPackets.empty();
	}


/*##############################################################################################

_flow_packet_listener_t::_binary_t

################################################################################################*/


_flow_packet_listener_t::_binary_t::_binary_t()noexcept
	: _offset( 0 ) {}

_flow_packet_listener_t::_binary_t::_binary_t( const binary_vector_t & blob, size_t count )noexcept
	: _pBuf( &blob )
	, _offset( 0 )
	, _count((std::min)(blob.size(), count)){}

_flow_packet_listener_t::_binary_t::_binary_t( _binary_t && o )noexcept
	: _pBuf( std::move( o._pBuf) )
	, _offset( std::move( o._offset ) )
	, _count(std::move(o._count)){}

_flow_packet_listener_t::_binary_t& _flow_packet_listener_t::_binary_t::operator=(_binary_t && o)noexcept {
	_pBuf = std::move( o._pBuf);
	_offset = std::move( o._offset );
	_count = std::move(o._count);

	return (*this);
	}

bool _flow_packet_listener_t::_binary_t::eof()const noexcept {
	return _offset == _count;
	}


