#pragma once


#include <functional>
#include <memory>
#include "./i_encoding.h"
#include "./i_bincntr.h"
#include "./ibinpck.h"
#include "../utilities/check_validate/checker_seqcall.h"


namespace crm::detail{


/*! ���� �������� ��������� */
class _iol_igtw_t : public igtw_pck_t {
public:
	typedef igtw_pck_t base_type_t;

private:
	//! ���������� �������� �������
	std::unique_ptr<_flow_packet_listener_t> _binpckScan;

#ifdef CBL_USE_SEQUENTIAL_CALL_CHECKER
	checker_seqcall_t _seqCallChecker;
#endif

	void tail_lost_packets()noexcept;
	void reverse_exception(_packet_t && u, std::unique_ptr<dcf_exception_t> && e)const noexcept;
	message_frame_unit_t transform(_packet_t && p)const noexcept;

public:
	~_iol_igtw_t();

	_iol_igtw_t(std::weak_ptr<distributed_ctx_t> ctx,
		std::unique_ptr<_flow_packet_listener_t> && binAccpt );

	_iol_igtw_t(const _iol_igtw_t &) = delete;
	_iol_igtw_t& operator=(const _iol_igtw_t &) = delete;

	/*! ���������� ���������� /ref i_iol_slot_callback_rt0_t :
	���������� ��������� ��������� ��������� ������ */
	void push(const binary_vector_t & blob, size_t)noexcept;

	std::vector<std::unique_ptr<i_iol_ihandler_t>> decode(const binary_vector_t& blob, size_t);

	/*! �������� �� ��������� ��������� ��� ���������� ��������������.
	\param id ������� �������������
	\param once ���� ������� true, ����������� �������� ���� ���, ����� �� ��� ���,
	���� �� ����� ��������� ������ ����������� � ������� ������ /ref unsubscribe_hndl_id.
	*/
	subscribe_invoker_result subscribe_hndl_id(async_space_t scx,
		const _address_hndl_t & id,
		subscribe_options opt,
		subscribe_message_handler && handler );

	/*! ����� �� ��������� ����������� ��� ���������� ��������������
	\param id ������������� ����������
	*/
	void unsubscribe_hndl_id( const _address_hndl_t & id )noexcept;

	/*! �������� ������� �������� �������� */
	 void set_input_queue( std::shared_ptr<i_iprocessed_handler_t> obj ) ;

	 /*! ������ �� ������� �������� �������� */
	 std::shared_ptr<i_iprocessed_handler_t> get_input_queue()noexcept;

	 /*! ������ �� ������� �������� �������� */
	 std::shared_ptr<const i_iprocessed_handler_t> get_input_queue()const noexcept;
	};
}

