#pragma once


#include "../base_predecl.h"
#include "../internal_invariants.h"
#include "../channel_terms/internal_terms.h"
#include "../channel_terms/message_quant.h"


namespace crm {


class _flow_packet_listener_t;
class message_serializer_v0_t;
class default_binary_protocol_handler_t;


///*! ��������� ������������ ��������� ������ */
//struct _flow_packet_listener_t {
//	virtual ~_flow_packet_listener_t();
//
//	virtual void push( const binary_vector_t & blob, size_t count )noexcept = 0;
//	virtual bool has_packet()const noexcept = 0;
//	virtual std::list<detail::_packet_t> capture_packets()noexcept = 0;
//
//private:
//#ifdef CBL_MPF_OBJECTS_COUNTER_CHECKER_BIN_ACCEPTOR
//	crm::utility::__xobject_counter_logger_t<_flow_packet_listener_t, 1> _xDbgCounter;
//#endif
//	};
//
//
//struct message_serializer_v0_t {
//	virtual ~message_serializer_v0_t();
//
//	virtual binary_vector_t push( i_iol_ohandler_t & obj )const = 0;
//
//private:
//#ifdef CBL_MPF_OBJECTS_COUNTER_CHECKER_BIN_EMITTER
//	crm::utility::__xobject_counter_logger_t<message_serializer_v0_t, 1> _xDbgCounter;
//#endif
//	};


///*! ��������� ����������� ��������� */
//struct default_binary_protocol_handler_t {
//	virtual ~default_binary_protocol_handler_t();
//
//	virtual std::unique_ptr<_flow_packet_listener_t> create_acceptor(std::weak_ptr<distributed_ctx_t> ctx_) = 0;
//	virtual std::unique_ptr<message_serializer_v0_t> create_emitter(std::weak_ptr<distributed_ctx_t> ctx_) = 0;
//
//private:
//#ifdef CBL_MPF_OBJECTS_COUNTER_CHECKER_BIN_PROTOCOL_HANDLER
//	crm::utility::__xobject_counter_logger_t<default_binary_protocol_handler_t, 1> _xDbgCounter;
//#endif
//	};
}


