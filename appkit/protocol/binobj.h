#pragma once


#include <functional>
#include <memory>
#include "../internal_invariants.h"
#include "./ibinobj.h"
#include "./obinobj.h"
#include "./i_bincntr.h"


namespace crm::detail{


class iol_gtw_t final : public i_iol_gtw_t {
private:
	/* �������� ���������� ������������� ����� */
	std::weak_ptr<distributed_ctx_t> _ctx;
	/*! ���� �������� ��������� */
	_iol_igtw_t _inGtw;
	/*! ���� ��������� ��������� */
	_iol_ogtw_t _outGtw;
	/*! ������ ������ */
	std::atomic<bool> _cancel{ false };

#ifdef CBL_MPF_OBJECTS_COUNTER_CHECKER_BIN_B2OBJECT_TRANSLATOR
	crm::utility::__xobject_counter_logger_t<iol_gtw_t, 1> _xDbgCounter;
#endif

	std::shared_ptr<distributed_ctx_t> ctx()noexcept;
	std::shared_ptr<const distributed_ctx_t> ctx()const noexcept;

	/*! ������� ��������� */
	bool canceled()const noexcept;
	/*! ��������� */
	void cancel()noexcept;

public:
	/*! �������� �������.
	\param channels ��������� ��������� �����/������
	*/
	iol_gtw_t( std::weak_ptr<distributed_ctx_t> ctx ,
		std::shared_ptr<default_binary_protocol_handler_t> protocol );

	iol_gtw_t( const iol_gtw_t & ) = delete;
	iol_gtw_t& operator=(const iol_gtw_t &) = delete;

#ifdef CBL_USE_SMART_OBJECTS_COUNTING
	crm::log::counter_t subcounter(const std::string & name);
#endif//CBL_USE_SMART_OBJECTS_COUNTING

	/*! �������� ��������� */
	void close()noexcept;

	/*! ������ ����� ��������� */
	void start(detail::reverse_handlers && exch)final;

	~iol_gtw_t();

	/*-------------------------------------------------------------------------
	i_iol_gtw_t
	----------------------------------------------------------------------------*/

	void push(const binary_vector_t & blob, size_t)noexcept override;
	std::vector<std::unique_ptr<i_iol_ihandler_t>> decode(const binary_vector_t& blob, size_t) final;

	/*! �������� ������� �������� �������� */
	void set_input_queue( std::shared_ptr<i_iprocessed_handler_t> obj ) final;

	/*! ������ �� ������� �������� �������� */
	std::shared_ptr<i_iprocessed_handler_t> get_input_queue()noexcept final;

	std::shared_ptr<const i_iprocessed_handler_t> get_input_queue()const noexcept final;

	/*! �������� �� ��������� ��������� ��� ���������� ��������������.
	\param id ������� �������������
	\param once ���� ������� true, ����������� �������� ���� ���, ����� �� ��� ���,
	���� �� ����� ��������� ������ ����������� � ������� ������ /ref unsubscribe_hndl_id.
	*/
	subscribe_invoker_result subscribe_hndl_id(async_space_t scx,
		const _address_hndl_t & id, 
		subscribe_options opt,
		subscribe_message_handler && handler )final;

	/*! ����� �� ��������� ����������� ��� ���������� ��������������
	\param id ������������� ����������
	*/
	void unsubscribe_hndl_id( const _address_hndl_t & id )noexcept final;

	/* -------------------------------------------------------------------------
	i_iol_ogtw_t
	-----------------------------------------------------------------------------*/

	template<typename O,
		typename std::enable_if_t < is_out_message_requirements_v<O>, int> = 0>
	outbound_message_unit_t serialize_gateout(O && obj)const {
		if constexpr(!std::is_same_v<oping_id_t, std::decay_t<O>>) {
			CBL_VERIFY(is_normalizedx(obj));
			}

		return _outGtw.serialize_gateout(std::forward<O>(obj));
		}
	};
}


