#include <list>
#include <limits>
#include "../../context/context.h"
#include "../binobj.h"


using namespace crm;
using namespace crm::detail;


iol_gtw_t::iol_gtw_t( std::weak_ptr<distributed_ctx_t> ctx_,
	std::shared_ptr<default_binary_protocol_handler_t> protocol)
	: _ctx( ctx_ )
	, _inGtw( ctx_, CHECK_PTR(protocol)->create_acceptor() )
	, _outGtw( ctx_, CHECK_PTR(protocol)->create_emitter() ){}

void iol_gtw_t::start(reverse_handlers&& r) {

	/* ������ ��������� ����������� */
	_inGtw.start(std::move(r));
	}

subscribe_invoker_result iol_gtw_t::subscribe_hndl_id(async_space_t scx, const _address_hndl_t & id, subscribe_options opt,
	subscribe_message_handler && hndl) {

	return _inGtw.subscribe_hndl_id(scx, id, opt, std::move(hndl));
	}

void iol_gtw_t::unsubscribe_hndl_id( const _address_hndl_t & id )noexcept {
	_inGtw.unsubscribe_hndl_id( id );
	}


void iol_gtw_t::cancel()noexcept {
	_cancel.store( true, std::memory_order::memory_order_release );
	}

bool iol_gtw_t::canceled()const noexcept {
	return _cancel.load( std::memory_order::memory_order_acquire );
	}

void iol_gtw_t::close()noexcept {
	cancel();

	_inGtw.close();
	}

iol_gtw_t::~iol_gtw_t() {
	close();
	}

void iol_gtw_t::push(const binary_vector_t & blob, size_t c)noexcept {
#pragma message( CBL_VAR_NAME_VALUE(CBL_BUILD_VAR_SNDRCV_REVERSE_GUARD, 1))
	_inGtw.push(blob, c);
	}

std::vector<std::unique_ptr<i_iol_ihandler_t>> iol_gtw_t::decode(const binary_vector_t& blob, size_t c) {
#pragma message( CBL_VAR_NAME_VALUE(CBL_BUILD_VAR_SNDRCV_REVERSE_GUARD, 1))
	return _inGtw.decode(blob, c);
	}

void iol_gtw_t::set_input_queue( std::shared_ptr<i_iprocessed_handler_t> obj ) {
	_inGtw.set_input_queue(std::move(obj));
	}

std::shared_ptr<iol_gtw_t::i_iprocessed_handler_t> iol_gtw_t::get_input_queue()noexcept {
	return _inGtw.get_input_queue();
	}

std::shared_ptr<const iol_gtw_t::i_iprocessed_handler_t> iol_gtw_t::get_input_queue()const noexcept {
	return _inGtw.get_input_queue();
	}

std::shared_ptr<distributed_ctx_t> iol_gtw_t::ctx()noexcept {
	return _ctx.lock();
	}

std::shared_ptr<const distributed_ctx_t> iol_gtw_t::ctx()const noexcept {
	return _ctx.lock();
	}



