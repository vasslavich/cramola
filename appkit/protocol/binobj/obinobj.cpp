#include <list>
#include <limits>
#include "../../context/context.h"
#include "../baccp_default.h"
#include "../obinobj.h"
#include "../../messages/binary_vector.h"


using namespace crm;
using namespace crm::detail;


_iol_ogtw_t::_iol_ogtw_t(std::weak_ptr<distributed_ctx_t> ctx_,
	std::unique_ptr<message_serializer_v0_t> && emitter )
	: _emitter( std::move( emitter ) )
	, _ctx(ctx_){
	
	CBL_VERIFY(_emitter);
	}

rtl_level_t _iol_ogtw_t::level()const {
	return rtl_level_t::l0;
	}


