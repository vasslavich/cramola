#include <list>
#include <limits>
#include <sstream>
#include "../../context/context.h"
#include "../ibinobj.h"
#include "../xcvt.h"
#include "../../channel_terms/xtract.h"


using namespace crm;
using namespace crm::detail;


void  _iol_igtw_t::reverse_exception(_packet_t && pck_, std::unique_ptr<dcf_exception_t> && e)const noexcept {
	CBL_MPF_KEYTRACE_SET(pck_, 310);

	/* ��������� ��������� request-��������� */
	if (is_sendrecv_request(pck_.address())) {
		CBL_MPF_KEYTRACE_SET(pck_.address(), 312);

		CBL_VERIFY(!is_null(pck_.address()));
		CBL_VERIFY(!is_null(pck_.address().session()) &&
			!is_null(pck_.address().source_subscriber_id()) &&
			!is_null(pck_.address().source_id()));

		get_reverse_handler()(pck_.address(), std::move(e));
		}

	/* ��������� ��������� response-��������� */
	else if (pck_.type().fl_report()) {
		CBL_MPF_KEYTRACE_SET(pck_.address(), 313);

		if (!crm::is_null(pck_.address())) {
			CBL_VERIFY(!is_null(pck_.address().destination_subscriber_id()));
			CBL_VERIFY(is_null(pck_.address().source_subscriber_id()));

			if (auto c = ctx()) {
				accept_report(make_exception_RZi_LX(c, pck_.address(), std::move(e)));
				}
			}
		}

	pck_.mark_as_used();
	}

void _iol_igtw_t::tail_lost_packets()noexcept {
	CHECK_NO_EXCEPTION_BEGIN

		if (_binpckScan) {
			if (auto c = ctx()) {

				/* ������ ��������� �������������� �������� ������� */
				if (_binpckScan->has_packet()) {

					auto plst(_binpckScan->capture_packets());
					for (auto && pck_ : plst) {
						reverse_exception(std::move(pck_), CREATE_MPF_PTR_EXC_FWD(nullptr));
						}
					}
				}

			CBL_VERIFY(!_binpckScan->has_packet());
			}

	CHECK_NO_EXCEPTION_END
	}

_iol_igtw_t::~_iol_igtw_t() {
	tail_lost_packets();
	}

message_frame_unit_t _iol_igtw_t::transform(_packet_t && p)const noexcept {

	CBL_VERIFY(!p.error());
	message_frame_unit_t unit(std::move(p));

	CHECK_NO_EXCEPTION_BEGIN

		CBL_VERIFY(unit.unit().is_mark_used());
	CBL_VERIFY(p.is_mark_used());

	/* ��������� ��������� request-��������� */
	if (is_sendrecv_request(unit.unit().address()) && unit.unit().type().utype() != (int)iol_types_t::st_ping) {
		CBL_VERIFY(unit.unit().type().utype() != (int)iol_types_t::st_ping);
		CBL_VERIFY(unit.unit().type().type() != iol_types_t::st_notification);

#pragma message(CBL_MESSAGE_ROADMAP_FOOTMARK(CBL_MESSAGE_ROADMAP,RQi_L0))

		auto targetAddr = unit.unit().address();
		CBL_VERIFY(!is_null(targetAddr));

		CBL_VERIFY(!is_null(targetAddr.session()) &&
			!is_null(targetAddr) &&
			!is_null(targetAddr.source_subscriber_id()) &&
			!is_null(targetAddr.source_id()));

#pragma message( CBL_VAR_NAME_VALUE(CBL_BUILD_VAR_SNDRCV_REVERSE_SETUP_FUNCTOR, 0))
#pragma message( CBL_VAR_NAME_VALUE(CBL_BUILD_VAR_SNDRCV_REVERSE_GUARD, 2))

		/* � ������ ������, ��������� response-��������� �� ������ */
		auto frev = get_reverse_handler();
		if (unit.reset_invoke_exception_guard(inbound_message_with_tail_exception_guard([frev, targetAddr](std::unique_ptr<dcf_exception_t>&& exc) noexcept {

			CHECK_NO_EXCEPTION_BEGIN

				std::wostringstream what;
			what << L"sndrcv-guard(gtw-trfm:rq)";
			what << L":s=" << targetAddr.source_id();
			what << L":t=" << targetAddr.destination_id();
			what << L":st=" << targetAddr.spin_tag();

			if (!exc) {
				exc = CREATE_MPF_PTR_EXC_FWD(what.str());
				}
			else {
				exc->add_message_line(what.str());
				}

			exc->set_code(RQi_L0);

#ifdef CBL_MPF_TRACELEVEL_EVENT_SNDRCV_GUARD
			GLOBAL_STOUT_FWDRITE_STR(L"sndrcv-guard(gtw-trfm:rq):" + exc->msg());
#endif

			CBL_MPF_KEYTRACE_SET(targetAddr, 503);

			frev(targetAddr, std::move(exc));

			CHECK_NO_EXCEPTION_END

			}, ctx(),
				std::string(__FILE_LINE__) + ":" + std::to_string(unit.unit().type().utype()),
				async_space_t::sys))) {

			FATAL_ERROR_FWD(nullptr);
			}
		}

	/* ��������� ��������� response-��������� */
	else if (unit.unit().type().fl_report() && unit.unit().type().utype() != (int)iol_types_t::st_ping) {
		CBL_VERIFY(unit.unit().type().utype() != (int)iol_types_t::st_ping);

#pragma message(CBL_MESSAGE_ROADMAP_FOOTMARK(CBL_MESSAGE_ROADMAP,RZi_L0))

		auto targetAddr = unit.unit().address();
		CBL_VERIFY((unit.unit().type().fl_report() && !is_null(targetAddr.destination_subscriber_id()))
			|| !unit.unit().type().fl_report()
			|| unit.unit().type().type() == iol_types_t::st_ping);
		CBL_VERIFY(targetAddr.level() == rtl_level_t::l1 || targetAddr.level() == rtl_level_t::l0);

		/* � ������ ������, �������� ������, ������ ����������� response-���������, ��������� �� ������ */
		if (unit.reset_invoke_exception_guard(inbound_message_with_tail_exception_guard(
			[actor = get_push_actor(), ctx_ = ctx(), targetAddr](std::unique_ptr<dcf_exception_t> && exc)noexcept {

			CHECK_NO_EXCEPTION_BEGIN

				std::wostringstream what;
			what << L"sndrcv-guard(gtw-trfm:rp)";
			what << L":s=" << targetAddr.source_id();
			what << L":t=" << targetAddr.destination_id();
			what << L":st=" << targetAddr.spin_tag();

			if (!exc) {
				exc = CREATE_MPF_PTR_EXC_FWD(what.str());
				}
			else {
				exc->add_message_line(what.str());
				}

			exc->set_code(RZi_L0);

#ifdef CBL_MPF_TRACELEVEL_EVENT_SNDRCV_GUARD
			GLOBAL_STOUT_FWDRITE_STR(L"sndrcv-guard(gtw-trfm:rp):" + exc->msg());
#endif

			CBL_MPF_KEYTRACE_SET(targetAddr, 502);

			CBL_VERIFY(!is_null(targetAddr.destination_subscriber_id()));
			actor(make_exception_RZi_LX(ctx_, targetAddr, std::move(exc)));

			CHECK_NO_EXCEPTION_END

			}, ctx(),
				std::string(__FILE_LINE__) + ":" + std::to_string(unit.unit().type().utype()),
				async_space_t::sys))) {

			FATAL_ERROR_FWD(nullptr);
			}
		}

	CHECK_NO_EXCEPTION_END

		return unit;
	}

_iol_igtw_t::_iol_igtw_t(std::weak_ptr<distributed_ctx_t> ctx,
	std::unique_ptr<_flow_packet_listener_t> && binAccpt)

	: base_type_t(ctx, rtl_level_t::l0)
	/* ���������� ��������� ��������� */
	, _binpckScan(std::move(binAccpt)) {
	}

void _iol_igtw_t::push(const binary_vector_t & blob, size_t count)noexcept {

	CHECK_NO_EXCEPTION_BEGIN

#ifdef CBL_USE_SEQUENTIAL_CALL_CHECKER
		auto seqChecker(_seqCallChecker.inc_scope());
#endif

	if (!canceled()) {

		/* �������� ��������� ������� � ������ ������������
		������������������ �������� ������� */
		_binpckScan->push(blob, count);

		/* ������ ��������� �������������� �������� ������� */
		if (_binpckScan->has_packet()) {

			auto plst(_binpckScan->capture_packets());
			for (auto && pck_ : plst) {
				CBL_MPF_KEYTRACE_SET(pck_, 32);

				if (!pck_.error()) {
					base_type_t::push(transform(std::move(pck_)));
					}
				else {
					reverse_exception(std::move(pck_), CREATE_MPF_PTR_EXC_FWD(nullptr));
					}
				}
			}
		}

	CHECK_NO_EXCEPTION_END
	}

std::vector<std::unique_ptr<i_iol_ihandler_t>> _iol_igtw_t::decode(const binary_vector_t& blob, size_t count) {
	std::vector<std::unique_ptr<i_iol_ihandler_t>> r;

	/* �������� ��������� ������� � ������ ������������
	������������������ �������� ������� */
	_binpckScan->push(blob, count);

	/* ������ ��������� �������������� �������� ������� */
	if (_binpckScan->has_packet()) {

		auto plst(_binpckScan->capture_packets());

		r.reserve(plst.size());
		for (auto&& pck_ : plst) {
			CBL_MPF_KEYTRACE_SET(pck_, 32);

			if (!pck_.error()) {
				r.push_back(base_type_t::decode(transform(std::move(pck_))));
				}
			else {
				THROW_EXC_FWD(nullptr);
				}
			}
		}

	return r;
	}

subscribe_invoker_result _iol_igtw_t::subscribe_hndl_id(async_space_t scx, const _address_hndl_t & id, subscribe_options opt,
	subscribe_message_handler && handler) {

	return base_type_t::subscribe_hndl_id(scx, id, opt, std::move(handler));
	}

void _iol_igtw_t::unsubscribe_hndl_id(const _address_hndl_t & id) noexcept {
	base_type_t::unsubscribe_hndl_id(id);
	}

void _iol_igtw_t::set_input_queue(std::shared_ptr<i_iprocessed_handler_t> obj) {
	base_type_t::set_input_queue(obj);
	}

std::shared_ptr<_iol_igtw_t::i_iprocessed_handler_t> _iol_igtw_t::get_input_queue()noexcept {
	return base_type_t::get_input_queue();
	}

std::shared_ptr<const _iol_igtw_t::i_iprocessed_handler_t> _iol_igtw_t::get_input_queue()const noexcept {
	return base_type_t::get_input_queue();
	}

