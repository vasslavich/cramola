#pragma once


#include <functional>
#include <memory>
#include "../internal_invariants.h"
#include "./i_bincntr.h"


namespace crm::detail{


class igtw_pck_impl_t;

/*! ���� �������� �������� ��������� */
class igtw_pck_t : public i_iol_igtw_base_t{
protected:
	typedef igtw_pck_t self_t;

private:
	//! ������� ������������������ �������� �������
	std::shared_ptr<igtw_pck_impl_t> _impl;

#ifdef CBL_MPF_OBJECTS_COUNTER_CHECKER_BIN_B2PACKET_TRANSLATOR
	utility::__xobject_counter_logger_t<igtw_pck_t, 1> _xDbgCounter;
#endif

public:
	std::shared_ptr<distributed_ctx_t> ctx()const noexcept;
	void set_input_queue( std::shared_ptr<i_iprocessed_handler_t> obj );
	std::shared_ptr<i_iprocessed_handler_t> get_input_queue()noexcept;
	std::shared_ptr<const i_iprocessed_handler_t> get_input_queue()const noexcept;

	rtl_level_t level()const noexcept;

	/*! ������ ��������� ���������� ������ */
	void start(reverse_handlers && r);

	inbound_message_with_tail_exception_guard_handler_by_address get_reverse_handler()const noexcept;

	/*! �������� ��������� */
	void close()noexcept;

	/*! ��������� ��������� */
	bool canceled()const noexcept;

	igtw_pck_t(std::weak_ptr<distributed_ctx_t> ctx,
		rtl_level_t l);

	~igtw_pck_t();

	/*! ���������� ���������� /ref i_iol_packet_slot_callback_t :
	���������� ��������� ��������� ��������� ������ */
	void push(message_frame_unit_t && lst)const noexcept;
	std::unique_ptr<i_iol_ihandler_t> decode(message_frame_unit_t&& lst)const;
	std::function<void(message_frame_unit_t && u)> get_push_actor()const noexcept;
	void accept_report(message_frame_unit_t && u)const noexcept;
	void addressed_exception(const _address_hndl_t & address,
		std::unique_ptr<dcf_exception_t> && e,
		invoke_context_trait)noexcept;

	/*! �������� �� ��������� ��������� ��� ���������� ��������������.
	\param id ������� �������������
	\param once ���� ������� true, ����������� �������� ���� ���, ����� �� ��� ���,
	���� �� ����� ��������� ������ ����������� � ������� ������ /ref unsubscribe_hndl_id.
	*/
	subscribe_invoker_result subscribe_hndl_id(async_space_t scx, 
		const _address_hndl_t & id,
		subscribe_options opt,
		subscribe_message_handler && handler );

	/*! ����� �� ��������� ����������� ��� ���������� ��������������
	\param id ������������� ����������
	*/
	void unsubscribe_hndl_id( const _address_hndl_t & id )noexcept;

	void ntf_hndl(std::unique_ptr<dcf_exception_t> && e)const noexcept;
	};
}
