#pragma once


#include <functional>
#include <memory>
#include "../internal_invariants.h"
#include "../channel_terms/internal_terms.h"


namespace crm {


struct i_xpeer_source_t {
	typedef std::function<void(std::unique_ptr<dcf_exception_t> && exc)> end_execute_f;
	using read_ready_f = std::function<void(const std::vector<uint8_t> & buf, size_t count)>;
	using make_ping_sequence_f = std::function<detail::outbound_message_unit_t()>;

	virtual ~i_xpeer_source_t() {};
	virtual void close()noexcept = 0;

	virtual void async_connect(const std::unique_ptr<i_endpoint_t> & endPoint,
		end_execute_f && onConnect) = 0;

	virtual std::string saddress()const noexcept = 0;
	virtual int port() const noexcept = 0;
	virtual connection_key_t connecition_key()const noexcept = 0;
	virtual bool is_integrity_provided()const noexcept =0;

	virtual void write( detail::outbound_message_unit_t && buf) = 0;
	virtual void start(read_ready_f && r, end_execute_f && eof, make_ping_sequence_f && pingout, const sndrcv_timeline_t & pingTimeline) = 0;

	virtual bool ping_emit_interval_exceeded()const noexcept = 0;
	virtual bool ping_accept_interval_exceeded()const noexcept = 0;
	};


struct i_xpeer_source_factory_t {
	virtual ~i_xpeer_source_factory_t() {};

	typedef std::function<void(std::unique_ptr<i_xpeer_source_t> && source)> evhndl_source_connection_t;

	virtual void open(evhndl_source_connection_t && evhndlConnection_,
		const std::unique_ptr<i_endpoint_t> & endpoint) = 0;
	virtual bool closed()const noexcept = 0;
	virtual void close()noexcept = 0;
	virtual std::unique_ptr<i_xpeer_source_t> create() = 0;
	};
}


