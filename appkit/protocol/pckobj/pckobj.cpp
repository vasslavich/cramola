#include <list>
#include <limits>
#include "../../context/context.h"
#include "../pckobj.h"


using namespace crm;
using namespace crm::detail;


gtw_rt1_x_t::gtw_rt1_x_t(std::weak_ptr<distributed_ctx_t> ctx_,
	std::unique_ptr<message_serializer_v0_t> protocol )noexcept
	: _ctx( ctx_ )
	, _inGtw( ctx_, rtl_level_t::l1 )
	, _outGtw( ctx_, std::move(protocol) ){}

subscribe_invoker_result gtw_rt1_x_t::subscribe_hndl_id(async_space_t scx, const _address_hndl_t & id, subscribe_options opt,
	subscribe_message_handler && hndl ) {

	return _inGtw.subscribe_hndl_id(scx, id, opt, std::move(hndl));
	}

void gtw_rt1_x_t::unsubscribe_hndl_id( const _address_hndl_t & id )noexcept {
	_inGtw.unsubscribe_hndl_id(id);
	}

bool gtw_rt1_x_t::canceled()const noexcept {
	return _cancel.load( std::memory_order::memory_order_acquire );
	}

void gtw_rt1_x_t::close()noexcept {
	bool cancelf = false;
	if( _cancel.compare_exchange_strong( cancelf, true ) ) {

		_inGtw.close();
		}
	}

gtw_rt1_x_t::~gtw_rt1_x_t() {
	close();
	}

void gtw_rt1_x_t::set_input_queue( std::shared_ptr<i_iprocessed_handler_t> obj ) {
	_inGtw.set_input_queue(std::move(obj));
	}

std::shared_ptr<gtw_rt1_x_t::i_iprocessed_handler_t> gtw_rt1_x_t::get_input_queue()noexcept {
	return _inGtw.get_input_queue();
	}

std::shared_ptr<const gtw_rt1_x_t::i_iprocessed_handler_t> gtw_rt1_x_t::get_input_queue()const noexcept {
	return _inGtw.get_input_queue();
	}

std::shared_ptr<distributed_ctx_t> gtw_rt1_x_t::ctx()noexcept {
	return _ctx.lock();
	}

std::shared_ptr<const distributed_ctx_t> gtw_rt1_x_t::ctx()const noexcept {
	return _ctx.lock();
	}

void gtw_rt1_x_t::start(reverse_handlers&& r) {

	/* ������ ��������� ����������� */
	_inGtw.start(std::move(r));
	}

void gtw_rt1_x_t::push(message_frame_unit_t && pck) {

#pragma message( CBL_VAR_NAME_VALUE(CBL_BUILD_VAR_SNDRCV_REVERSE_GUARD, 102))
	_inGtw.push(std::move(pck));
	}

void gtw_rt1_x_t::addressed_exception(const _address_hndl_t & address,
	std::unique_ptr<dcf_exception_t> && e,
	invoke_context_trait ic)noexcept {
	
	_inGtw.addressed_exception(address, std::move(e), ic);
	}

#ifdef CBL_USE_SMART_OBJECTS_COUNTING
crm::log::counter_t gtw_rt1_x_t::subcounter(const std::string & name) {
	return _objectCounter.subcounter(name);
	}
#endif//CBL_USE_SMART_OBJECTS_COUNTING


