#include <list>
#include <limits>
#include "../../internal.h"
#include "../../context/context.h"
#include "../opckobj.h"


using namespace crm;
using namespace crm::detail;


ogtw_rt1_x_t::ogtw_rt1_x_t(std::weak_ptr<distributed_ctx_t> ctx,
	std::unique_ptr<message_serializer_v0_t> && emitter )
	: _emitter( std::move(emitter)){
	
	CBL_VERIFY(_emitter);
	}

rtl_level_t ogtw_rt1_x_t::level()const {
	return rtl_level_t::l1;
	}



