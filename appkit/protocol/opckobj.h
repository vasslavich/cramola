#pragma once


#include <functional>
#include <memory>
#include <map>
#include "../internal_invariants.h"
#include "./i_bincntr.h"


namespace crm::detail {


/*! ���� ��������� ��������� */
class ogtw_rt1_x_t : public virtual i_iol_igtw_base_t {
private:
	std::unique_ptr<message_serializer_v0_t> _emitter;

#ifdef CBL_MPF_OBJECTS_COUNTER_CHECKER_BIN_OBJECT2BIN_TRANSLATOR
	crm::utility::__xobject_counter_logger_t<ogtw_rt1_x_t, 1> _xDbgCounter;
#endif

public:
	ogtw_rt1_x_t(const ogtw_rt1_x_t &) = delete;
	ogtw_rt1_x_t& operator=(const ogtw_rt1_x_t &) = delete;

	ogtw_rt1_x_t(std::weak_ptr<distributed_ctx_t> ctx,
		std::unique_ptr<message_serializer_v0_t> && emitter);

	/*! ������� �������������, �� ������� �������� ������ ������ */
	rtl_level_t level()const;

	template<typename O,
		typename std::enable_if_t < is_out_message_requirements_v<O>, int> = 0>
	outbound_message_unit_t serialize_gateout(O && obj)const {
		CBL_VERIFY(obj.type().type() != iol_types_t::st_notification || (is_null(obj.source_subscriber_id()) && !is_sendrecv_request(obj.address())));

#pragma message( CBL_VAR_NAME_VALUE(CBL_BUILD_VAR_MESSAGE_ROADMAP_MARKER_OUTBOUND, 1))

		if (is_sendrecv_cycle(obj)) {
			CBL_VERIFY_SNDRCV_KEY_SPIN_TAG(obj);
			CBL_VERIFY(is_sendrecv_request(obj) || is_sendrecv_response(obj));
			CBL_VERIFY(CBL_IS_SNDRCV_KEY_SPIN_TAGGED_ON(obj));

			outbound_message_desc_t mdesc(obj);
			auto mresponseGuard = obj.reset_invoke_exception_guard();
			auto mbinary = _emitter->push(std::forward<O>(obj));

 			return outbound_message_unit_t::make("ogtw_rt1_x_t::serialize_gateout-1", std::move(mbinary), std::move(mdesc), std::move(mresponseGuard));
			}
		else {
			CBL_VERIFY(!(is_sendrecv_request(obj) || is_sendrecv_response(obj)));
			CBL_VERIFY(!obj.is_reverse_exception_guard());
			CBL_VERIFY(CBL_IS_SNDRCV_KEY_SPIN_TAGGED_OFF(obj));

			return outbound_message_unit_t::make("ogtw_rt1_x_t::serialize_gateout-2", _emitter->push(std::forward<O>(obj)));
			}
		}
	};
}

