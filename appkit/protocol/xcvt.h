#pragma once


#include "./binobj.h"
#include "./pckobj.h"


namespace crm::detail {

using binary_packet_cvt_t = gtw_rt1_x_t;
using binary_object_cvt_t = iol_gtw_t;

}

