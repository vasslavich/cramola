#include <list>
#include <limits>
#include <sstream>
#include "../../context/context.h"
#include "../ibinpck.h"
#include "../../queues/qdef.h"


namespace crm::detail {


/*! ���� �������� �������� ��������� */
class igtw_pck_impl_t :
	public std::enable_shared_from_this<igtw_pck_impl_t>,
	public virtual i_iol_igtw_base_t {

	typedef igtw_pck_impl_t self_t;

private:
	//! �������� �����
	std::weak_ptr<distributed_ctx_t> _ctx;
	//! ���� �������������� ��������
	std::shared_ptr<i_iprocessed_handler_t> _inQueue;
	/*! ������� �������������, �� ������� �������� ������ ����� */
	rtl_level_t _level = rtl_level_t::undefined;
	reverse_handlers _revh;
	distributed_ctx_t::subscribe_line_t _subscribeLine;
	//! ������ 
	std::atomic<bool> _cancel{ false };

#ifdef CBL_MPF_OBJECTS_COUNTER_CHECKER_BIN_B2PACKET_TRANSLATOR
	crm::utility::__xobject_counter_logger_t<self_t, 1> _xDbgCounter;
#endif

	void tail_lost_packets()noexcept {
		}

	void cancel()noexcept {
		_cancel.store(true, std::memory_order::memory_order_release);
		}

	void ntf_hndl(std::unique_ptr<crm::dcf_exception_t>&& ntf) noexcept {

		auto ctx_(_ctx.lock());
		if (ctx_)
			ctx_->exc_hndl(std::move(ntf));
		}

	void ntf_hndl(const dcf_exception_t& exc)noexcept {
		ntf_hndl(exc.dcpy());
		}

	void dsrlz_placeout(std::unique_ptr<i_iol_ihandler_t>&& obj) {

		if (obj) {
			auto sourceId = obj->source_id();

			input_item_t item{ std::move(obj), sourceId };
			auto stck(get_input_queue());
			if (stck)
				stck->push(std::move(item));
			}
		else
			THROW_MPF_EXC_FWD(nullptr);
		}

	void send_outbox(global_object_identity_t /*target*/,
		global_object_identity_t /*source*/,
		std::unique_ptr<dcf_exception_t> /*pExc_*/) {

		crm::file_logger_t::logging(L"unimplemented operation", 1, __CBL_FILEW__, __LINE__);
		}

	std::unique_ptr<i_iol_ihandler_t> dsrlz_postbuild_addressed(
		std::unique_ptr<i_iol_ihandler_t>&& obj, invoke_context_trait ic) {

		if (obj) {
			return _subscribeLine.check_address(std::move(obj), ic);
			}
		else
			return nullptr;
		}

	std::unique_ptr<i_iol_ihandler_t> dsrlz_postbuild_as_handler(
		std::unique_ptr<i_iol_ihandler_t>&& obj) {

		if (obj && obj->type().type() == iol_types_t::st_functor_package && obj->level() == level()) {
			try {
				_revh(message_cast<ifunctor_package_t>(std::move(obj)));
				}
			catch (const dcf_exception_t& e1) {
				if (obj)
					obj->reset_invoke_exception_guard().invoke_async(e1.dcpy());
				}
			catch (const std::exception& e0) {
				if (obj)
					obj->reset_invoke_exception_guard().invoke_async(
						CREATE_PTR_EXC_FWD(e0));
				}
			catch (...) {
				if (obj)
					obj->reset_invoke_exception_guard().invoke_async(
						CREATE_PTR_EXC_FWD("undefined exception at arguments extract or handling"));
				}
			}

		return std::move(obj);
		}

	void dsrlz_postbuild(std::unique_ptr<i_iol_ihandler_t>&& obj, invoke_context_trait ic) {

		/* ��������� ��������� ������� */
		if (obj) {
			CBL_MPF_KEYTRACE_SET((*obj), 70);
			obj = dsrlz_postbuild_addressed(std::move(obj), ic);
			}

		/* ����� ������������ ����������� */
		if (obj) {
			CBL_MPF_KEYTRACE_SET((*obj), 80);
			obj = dsrlz_postbuild_as_handler(std::move(obj));
			}

		/* ���� �� ������������� �����, ��������� � ����� ���� �������� ��������� */
		if (obj) {
			CBL_MPF_KEYTRACE_SET((*obj), 90);
			dsrlz_placeout(std::move(obj));
			}
		}

public:
	igtw_pck_impl_t(std::weak_ptr<distributed_ctx_t> ctx_, rtl_level_t l)
		: /* �������� ����� */
		_ctx(ctx_)
		/* ������� ������������� */
		, _level(l)
		, _subscribeLine(CHECK_PTR(ctx_)->subscribe_make_line(l)) {}

	rtl_level_t level()const noexcept {
		return _level;
		}

	bool canceled()const noexcept {
		return _cancel.load(std::memory_order::memory_order_acquire);
		}

	std::weak_ptr<distributed_ctx_t> ctx()const noexcept {
		return _ctx;
		}

	void start(reverse_handlers&& revh) {
		using qtype = std::decay_t<decltype(CHECK_PTR(ctx())->queue_manager().create_rt0_input_queue(
			__FILE__, queue_options_t::message_stack_node))>;

		_revh = std::move(revh);

		if (!_inQueue)
			_inQueue = std::make_shared< qtype>(CHECK_PTR(ctx())->queue_manager().create_rt0_input_queue(
				__FILE__,
				queue_options_t::message_stack_node));
		}

	inbound_message_with_tail_exception_guard_handler_by_address get_reverse_handler()const noexcept {
		return _revh.exception_handler();
		}

	void close()noexcept {
		CHECK_NO_EXCEPTION_BEGIN

			cancel();
		_subscribeLine.close();

		CHECK_NO_EXCEPTION_END
		}

	~igtw_pck_impl_t() {
		close();
		}

	std::unique_ptr<i_iol_ihandler_t> decode(message_frame_unit_t&& pck) {
		CBL_MPF_KEYTRACE_SET(pck, 533);
		if (auto c = _ctx.lock()) {
			auto l = level();

			return c->deserialize(l, std::move(pck));
			}
		else {
			THROW_EXC_FWD(nullptr);
			}
		}

	void push(message_frame_unit_t&& pck)noexcept {
		if (auto c = _ctx.lock()) {
			try {

				CBL_MPF_KEYTRACE_SET(pck, 33);

#ifdef CRM_INCOMING_MESSAGES_SPLIT_IO_FROM_MESSAGES_STOCK
				auto l = level();

#ifdef CRM_INCOMING_MESSAGES_ORDERED_BY_TARGET_ID
				auto idx = get_packet_idx(pck);

				c->launch_async_ordered(async_space_t::sys, idx, __FILE_LINE__, [wptr = weak_from_this(), c, l](message_frame_unit_t&& u){
					if (auto sptr = wptr.lock()) {
						sptr->dsrlz_postbuild(c->deserialize(l, std::move(u)), invoke_context_trait::can_be_used_as_async);
						}
					}, std::move(pck));

#else
				c->launch_async(__FILE_LINE__, [wptr = weak_from_this(), c, l](message_frame_unit_t&& u){
					if (auto sptr = wptr.lock()) {
						sptr->dsrlz_postbuild(c->deserialize(l, std::move(u)), invoke_context_trait::can_be_used_as_async);
						}
					}, std::move(pck));
#endif

#else
				dsrlz_postbuild(c->deserialize(level(), std::move(pck)), invoke_context_trait::can_be_used_as_async);
#endif

				}
			catch (const crm::dcf_exception_t& ex0) {
				pck.invoke_exception(ex0);
				}
			catch (const std::exception& ex1) {
				pck.invoke_exception(ex1);
				}
			catch (...) {
				pck.invoke_exception(CREATE_PTR_EXC_FWD(nullptr));
				}
			}
		}

	void push(std::unique_ptr<i_iol_ihandler_t> && h, invoke_context_trait ic) {
		dsrlz_postbuild(std::move(h), ic);
		}

	void addressed_exception(const _address_hndl_t &addr,
		std::unique_ptr<dcf_exception_t>&& e,
		invoke_context_trait ic)noexcept {

		_subscribeLine.addressed_exception(addr, std::move(e), ic);
		}


	[[nodiscard]]
	subscribe_invoker_result subscribe_hndl_id(async_space_t scx, const _address_hndl_t & id, subscribe_options opt, subscribe_message_handler && handler) {
		if (!canceled()) {
			return _subscribeLine.subscribe_set_handler(id, scx, opt, std::move(handler));
			}
		else {
			return subscribe_invoker_result::enum_type::exception;
			}
		}

	void unsubscribe_hndl_id( const _address_hndl_t & id )noexcept{
		_subscribeLine.subscribe_unset_handler(id);
		}

	std::shared_ptr<igtw_pck_t::i_iprocessed_handler_t> get_input_queue()noexcept {
		return std::atomic_load_explicit(&_inQueue, std::memory_order::memory_order_acquire);
		}

	std::shared_ptr<const igtw_pck_t::i_iprocessed_handler_t> get_input_queue()const noexcept {
		return std::atomic_load_explicit(&_inQueue, std::memory_order::memory_order_acquire);
		}

#ifdef CBL_USE_SMART_OBJECTS_COUNTING
	crm::log::counter_t subcounter(const std::string & name) {
		return _THIS_COUNTER.subcounter(name);
		}
#endif///CBL_USE_SMART_OBJECTS_COUNTING

	void set_input_queue(std::shared_ptr<i_iprocessed_handler_t> obj) {
		auto oldq(std::atomic_exchange(&_inQueue, obj));
		}
	};
}



using namespace crm;
using namespace crm::detail;


void igtw_pck_t::set_input_queue(std::shared_ptr<i_iprocessed_handler_t> obj) {
	CHECK_PTR(_impl)->set_input_queue(obj);
	}

std::shared_ptr<igtw_pck_t::i_iprocessed_handler_t> igtw_pck_t::get_input_queue()noexcept {
	if(_impl)
		return _impl->get_input_queue();
	else
		return nullptr;
	}

std::shared_ptr<const igtw_pck_t::i_iprocessed_handler_t> igtw_pck_t::get_input_queue()const noexcept {
	if(_impl)
		return 	_impl->get_input_queue();
	else
		return nullptr;
	}

#ifdef CBL_USE_SMART_OBJECTS_COUNTING
crm::log::counter_t igtw_pck_t::subcounter(const std::string & name) {
	return 	CHECK_PTR(_impl)->subcounter(name);
	}
#endif//CBL_USE_SMART_OBJECTS_COUNTING

rtl_level_t igtw_pck_t::level()const noexcept {
	if(_impl)
		return 	_impl->level();
	else
		return rtl_level_t::undefined;
	}

void igtw_pck_t::start(reverse_handlers&& hndl) {
	CHECK_PTR(_impl)->start(std::move(hndl));
	}

inbound_message_with_tail_exception_guard_handler_by_address igtw_pck_t::get_reverse_handler()const noexcept {
	return CHECK_PTR(_impl)->get_reverse_handler();
	}

void igtw_pck_t::close() noexcept {
	if(_impl)
		_impl->close();
	}

igtw_pck_t::igtw_pck_t(std::weak_ptr<distributed_ctx_t> ctx,
	rtl_level_t l)
	: _impl(std::make_shared<igtw_pck_impl_t>(ctx, l)) {}

igtw_pck_t::~igtw_pck_t() {
	close();
	}

std::function<void(message_frame_unit_t && h)> igtw_pck_t::get_push_actor()const noexcept {
	return[p = _impl](message_frame_unit_t && h){
		p->push(std::move(h));
		};
	}

void igtw_pck_t::accept_report(message_frame_unit_t && u)const noexcept {
	push(std::move(u));
	}

std::unique_ptr<i_iol_ihandler_t> igtw_pck_t::decode(message_frame_unit_t&& u)const {
#pragma message( CBL_VAR_NAME_VALUE(CBL_BUILD_VAR_SNDRCV_REVERSE_GUARD, 3))
	if (_impl) {
		return _impl->decode(std::move(u));
		}
	else {
		THROW_EXC_FWD(nullptr);
		}
	}

void igtw_pck_t::push(message_frame_unit_t && u)const noexcept {

#pragma message( CBL_VAR_NAME_VALUE(CBL_BUILD_VAR_SNDRCV_REVERSE_GUARD, 3))
	if (_impl) {
		_impl->push(std::move(u));
		}
	}

void igtw_pck_t::addressed_exception(const _address_hndl_t & address,
	std::unique_ptr<dcf_exception_t> && e,
	invoke_context_trait ic )noexcept {
	
	if(_impl)
		_impl->addressed_exception(address, std::move(e), ic);
	}

subscribe_invoker_result igtw_pck_t::subscribe_hndl_id(async_space_t scx, const _address_hndl_t & id, subscribe_options opt,
	subscribe_message_handler && handler) {

	return CHECK_PTR(_impl)->subscribe_hndl_id(scx, id, opt, std::move(handler));
	}

void igtw_pck_t::unsubscribe_hndl_id(const _address_hndl_t & id) noexcept{
	if(_impl)
		_impl->unsubscribe_hndl_id(id);
	}

bool igtw_pck_t::canceled()const noexcept {
	if(_impl)
		return _impl->canceled();
	else
		return true;
	}

std::shared_ptr<distributed_ctx_t> igtw_pck_t::ctx()const noexcept {
	if(_impl)
		return _impl->ctx().lock();
	else
		return {};
	}

void igtw_pck_t::ntf_hndl(std::unique_ptr<dcf_exception_t> && e)const noexcept {
	if(auto c = ctx()) {
		c->exc_hndl(std::move(e));
		}
	}


