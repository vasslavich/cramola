#include <list>
#include <mutex>
#include <atomic>
#include "../../internal.h"
#include "../glblst.h"
#include "../../exceptions.h"
#include "../../logging/logger.h"
#include "../../exceptions.h"


using namespace crm;


typedef std::mutex lck_t;
typedef std::unique_lock<lck_t> lck_scp_t;


static std::list<std::function<void()>> lst;
static lck_t lstLck;

std::atomic<bool> terminatef = false;


void crm::subscribe_terminate_list( std::function<void()> && terminateHndl ) {
	if( !terminatef ) {
		lck_scp_t lck( lstLck );
		lst.push_back( std::move( terminateHndl ) );
		}
	}

void crm::terminate_list_execute()noexcept {
	terminatef.store(true);

	lck_scp_t lck(lstLck);

	for (auto && h : lst) {
		try {
			if (h) {
				h();
				}
			}
		catch (const dcf_exception_t & exc0) {
			crm::file_logger_t::logging(exc0);
			}
		catch (const std::exception & exc1) {
			crm::file_logger_t::logging(exc1);
			}
		catch (...) {
			crm::file_logger_t::logging("undefined exception", 1111, __CBL_FILEW__, __LINE__);
			}
		}
	}

void crm::setup_environement() {
	std::set_terminate([] {
		fatal_error_t::break_on_fatal_error("call terminate", L"-", 0);
		});
	}

