#pragma once


#include <functional>
#include "../internal_invariants.h"


namespace crm{

void setup_environement();
void subscribe_terminate_list(std::function<void()> && terminateHndl);
void terminate_list_execute()noexcept;
}


