#include <cstdio>
#include <cwchar>
#include "../cvt.h"
#include "../../../internal.h"
#include "../../../notifications/internal_terms.h"


using namespace crm;
using namespace crm::detail;



static const char* scan_mask_bigend[] = {
	/*0 ����*/				0,
	/*1 ���� uint8_t*/		"%2hhx",
	/*2 ����� uint16_t*/	"%4hx",
	/*3 �����*/				0,
	/*4 ����� uint32_t*/	"%8x",
	/*5 ����*/				0,
	/*6 ����*/				0,
	/*7 ����*/				0,
	/*8 ���� uint64_t*/		"%16llx"
	};


static const wchar_t* wscan_mask_bigend[] = {
	/*0 ����*/				0,
	/*1 ���� uint8_t*/		L"%2hhx",
	/*2 ����� uint16_t*/	L"%4hx",
	/*3 �����*/				0,
	/*4 ����� uint32_t*/	L"%8x",
	/*5 ����*/				0,
	/*6 ����*/				0,
	/*7 ����*/				0,
	/*8 ���� uint64_t*/		L"%16llx"
	};


template<typename integer_type>
static inline bool scan_integer( const char *cstr, const char *format, typename integer_type *ivalue ) {
	return 1 == sscanf_s( cstr, format, ivalue, sizeof(integer_type)*2 );
	}


template<typename integer_type>
static inline bool scan_integer( const wchar_t *cstr, const wchar_t *format, typename integer_type *ivalue ) {
	return 1 == swscanf_s( cstr, format, ivalue, sizeof(integer_type)* 2 );
	}


static inline size_t tstrlen( const char *str ) { return strlen( str ); }
static inline size_t tstrlen( const wchar_t *str ) { return wcslen( str ); }


static inline const char* integer_format( const char /*stub*/, const size_t sizeofInteger ) {
	return scan_mask_bigend[sizeofInteger];
	}

static inline const wchar_t* integer_format( const wchar_t /*stub*/, const size_t sizeofInteger ) {
	return wscan_mask_bigend[sizeofInteger];
	}


template<typename TChar, typename value_type>
static inline typename value_type cvt_hex_to_value_STR_BIG_TO_LITTLE( const typename TChar *str, cvt_err_t *errc ) {
	static_assert(sizeof(typename value_type) <= 8 && !(sizeof(typename value_type) & (sizeof(typename value_type) - 1)), "");

	uint64_t hexval = 0;
	if( tstrlen( str ) < sizeof(value_type)* 2 
		|| !scan_integer( str, integer_format( str[0], sizeof(value_type) ), &hexval ) ) {

		if( nullptr != errc )(*errc) = cvt_err_t::read_str_hex_pair;
		return (value_type)0;
		}

	if( nullptr != errc )(*errc) = cvt_err_t::success;
	return (value_type)hexval;
	}


template<typename TChar, typename value_type>
static inline typename value_type cvt_hex_to_value_STR_LITTLE_TO_BIG( const typename TChar *str, cvt_err_t *errc ) {

	if( tstrlen( str ) < sizeof(value_type)* 2 ) {
		if( nullptr != errc )(*errc) = cvt_err_t::read_str_hex_pair;
		return (value_type)0;
		}

	const typename TChar *pHexStr = str;
	typename value_type hexVal = 0;
	uint8_t* pHexValue = (uint8_t*)&hexVal;

	for( size_t ib = 0; ib < sizeof(value_type); ++ib, pHexStr += 2, ++pHexValue ) {

		if( !scan_integer( pHexStr, integer_format( str[0], sizeof(uint8_t) ), pHexValue ) ) {
			if( nullptr != errc )(*errc) = cvt_err_t::read_str_hex_pair;
			return (value_type)0;
			}
		}

	return hexVal;
	}


template<typename TChar, typename value_type>
static inline typename value_type cvt_hex_to_value_type( const TChar *cstr, const cvt_endian_t mode, cvt_err_t *errc ) {
	switch( mode ) {
			case cvt_endian_t::str_as_big_to_little:
				return cvt_hex_to_value_STR_BIG_TO_LITTLE<TChar, uint8_t>( cstr, errc );

			case cvt_endian_t::str_as_little_to_big:
				return cvt_hex_to_value_STR_LITTLE_TO_BIG<TChar, uint8_t>( cstr, errc );

			default:
				THROW_EXC_FWD( L"undefined mode" );
		}
	}


uint8_t crm::detail::cvt_hex_to_uint8( const char *cstr, cvt_err_t *errc ) {
	return cvt_hex_to_value_STR_BIG_TO_LITTLE<char, uint8_t>( cstr, errc );
	}

uint8_t crm::detail::cvt_hex_to_uint8( const wchar_t *cstr, cvt_err_t *errc ) {
	return cvt_hex_to_value_STR_BIG_TO_LITTLE<wchar_t, uint8_t>( cstr, errc );
	}

uint16_t crm::detail::cvt_hex_to_uint16( const char *cstr, const cvt_endian_t mode, cvt_err_t *errc ) {
	return cvt_hex_to_value_type<char, uint16_t>( cstr, mode, errc );
	}

uint16_t crm::detail::cvt_hex_to_uint16( const wchar_t *cstr, const cvt_endian_t mode, cvt_err_t *errc ) {
	return cvt_hex_to_value_type<wchar_t, uint16_t>( cstr, mode, errc );
	}

uint32_t crm::detail::cvt_hex_to_uint32( const char *cstr, const cvt_endian_t mode, cvt_err_t *errc ) {
	return cvt_hex_to_value_type<char, uint32_t>( cstr, mode, errc );
	}

uint32_t crm::detail::cvt_hex_to_uint32( const wchar_t *cstr, const cvt_endian_t mode, cvt_err_t *errc ) {
	return cvt_hex_to_value_type<wchar_t, uint32_t>( cstr, mode, errc );
	}

uint64_t crm::detail::cvt_hex_to_uint64( const char *cstr, const cvt_endian_t mode, cvt_err_t *errc ) {
	return cvt_hex_to_value_type<char, uint64_t>( cstr, mode, errc );
	}

uint64_t crm::detail::cvt_hex_to_uint64( const wchar_t *cstr, const cvt_endian_t mode, cvt_err_t *errc ) {
	return cvt_hex_to_value_type<wchar_t, uint64_t>( cstr, mode, errc );
	}


/*std::string crm::cvt( std::wstring_view src, cvt_err_t *errc, std::locale loc )noexcept {
	std::string buf( src.size(), '\0' );

	auto i = src.cbegin(), ie = src.cend();
	auto j = buf.begin();

	for( ; i != ie; ++i, ++j )
		*j = std::use_facet< std::ctype< char > >( loc ).narrow( *i );

	if( nullptr != errc )
		(*errc) = cvt_err_t::success;

	return std::move( buf );
	}*/


/*std::wstring crm::cvt( std::string_view src, cvt_err_t *errc, std::locale loc )noexcept {
	std::wstring buf( src.size(), '\0' );

	auto i = src.cbegin(), ie = src.cend();
	auto j = buf.begin();

	for( ; i != ie; ++i, ++j )
		*j = std::use_facet< std::ctype< wchar_t > >( loc ).widen( *i );

	if( nullptr != errc )
		(*errc) = cvt_err_t::success;

	return std::move( buf );
	}*/

template<typename Tstr>
static int cvt_tstr_to_i4w( const typename Tstr &str, int base, cvt_err_t *errc, std::locale /*loc*/ ) {

	static_assert(std::is_integral<Tstr::value_type>::value, __FILE__);
	
	size_t i = 0;
	auto val = std::stoi( str, &i, base );
	if( i < str.size() ) {
		if( nullptr != errc ) {
			(*errc) = cvt_err_t::bad_format;
			return 0;
			}
		else {
			THROW_EXC_FWD(nullptr);
			}
		}

	return val;
	}

template<typename Tstr>
static std::uint64_t cvt_tstr_to_ui8w( const typename Tstr &str, int base, cvt_err_t *errc, std::locale /*loc*/ ) {

	static_assert(std::is_integral<Tstr::value_type>::value, __FILE__);

	size_t i = 0;
	auto val = std::stoull( str, &i, base );
	if( i < str.size() ) {
		if( nullptr != errc ) {
			(*errc) = cvt_err_t::bad_format;
			return 0;
			}
		else {
			THROW_EXC_FWD(nullptr);
			}
		}

	return val;
	}

int crm::detail::cvt_to_i4( const std::wstring &wstr, int base, cvt_err_t *errc, std::locale loc ) {
	return cvt_tstr_to_i4w( wstr, base, errc, loc );
	}

int crm::detail::cvt_to_i4( const std::string &wstr, int base, cvt_err_t *errc, std::locale loc ) {
	return cvt_tstr_to_i4w( wstr, base, errc, loc );
	}

std::uint64_t crm::detail::cvt_to_ui8( const std::string &wstr, int base,
	cvt_err_t *errc, std::locale loc ) {

	return cvt_tstr_to_ui8w( wstr, base, errc, loc );
	}

std::string crm::detail::cvt_to_str( uint64_t value ) {
	static_assert(sizeof( unsigned long long ) == sizeof( uint64_t ), __FILE__);
	return std::to_string( value );
	}

std::string crm::detail::to_hexstr( size_t value ) {
	std::ostringstream s;
	s << std::hex << value;
	return s.str();
	}

std::string crm::detail::cvt_bin_to_hex(const std::vector<uint8_t> &data) {
	std::stringstream ss;
	ss << std::hex;
	for (size_t i = 0; i < data.size(); ++i)
		ss << (int)data[i];
	return std::move(ss.str());	
}

