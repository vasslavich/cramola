#pragma once


#include <string>
#include <vector>
#include <sstream>
#include "../../internal_invariants.h"


namespace crm {
namespace detail{



enum class cvt_endian_t {
	/*! ������ ���������� ������ �� �������� ����� � �������� "AABBCCDD", ����������
	� ������ [������� ���� ����� �� ��������(DD) � ��������(AA)] DD:CC:BB:AA*/
	str_as_big_to_little,

	/*! ������ ���������� ������ �� �������� ����� � �������� "AABBCCDD", ����������
	� ������ [������� ���� ����� �� ��������(AA) � ��������(DD)] AA:BB:CC:DD*/
	str_as_little_to_big
	};


//std::wstring cvt( std::string_view src, cvt_err_t *errc = nullptr, std::locale loc = std::locale() );

int cvt_to_i4( const std::wstring &wstr, int base = 10,
	cvt_err_t *errc = nullptr, std::locale loc = std::locale() );

int cvt_to_i4( const std::string &wstr, int base = 10,
	cvt_err_t *errc = nullptr, std::locale loc = std::locale() );

std::uint64_t cvt_to_ui8( const std::string &wstr, int base = 10,
	cvt_err_t *errc = nullptr, std::locale loc = std::locale() );

std::string cvt_to_str( uint64_t value );

std::string to_hexstr( size_t value );

uint8_t cvt_hex_to_uint8( const char *cstr, cvt_err_t *errc = nullptr );
uint8_t cvt_hex_to_uint8( const wchar_t *cstr, cvt_err_t *errc = nullptr );

uint16_t cvt_hex_to_uint16( const char *cstr, const cvt_endian_t mode, cvt_err_t *errc = nullptr );
uint16_t cvt_hex_to_uint16( const wchar_t *cstr, const cvt_endian_t mode, cvt_err_t *errc = nullptr );

uint32_t cvt_hex_to_uint32( const char *cstr, const cvt_endian_t mode, cvt_err_t *errc = nullptr );
uint32_t cvt_hex_to_uint32( const wchar_t *cstr, const cvt_endian_t mode, cvt_err_t *errc = nullptr );

uint64_t cvt_hex_to_uint64( const char *cstr, const cvt_endian_t mode, cvt_err_t *errc = nullptr );
uint64_t cvt_hex_to_uint64( const wchar_t *cstr, const cvt_endian_t mode, cvt_err_t *errc = nullptr );


template<typename Tstring>
std::vector<uint8_t> cvt_to_binary( const Tstring &hexStr, cvt_err_t *errc = nullptr ) {

	if( hexStr.size() % 2 )THROW_EXC_FWD( L"hex string bad format" );

	std::vector<uint8_t> buf( hexStr.size() / 2 );
	for( size_t i = 0; i < buf.size(); ++i ) {
		buf[i] = cvt_hex_to_uint8( &hexStr[i * 2], errc );
		if( nullptr != errc && (*errc) != cvt_err_t::success )return std::vector<uint8_t>();
		}

	return std::move( buf );
	}

std::string cvt_bin_to_hex(const std::vector<uint8_t> &data);
}
}
