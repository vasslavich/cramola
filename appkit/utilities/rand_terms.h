#pragma once

#include "../internal.h"

namespace crm::detail{

template<>
struct rand_generator< session_id_t>{
	static auto get(){
		return session_id_t::rand();
	}
};

template<>
struct rand_generator< sx_locid_t>{
	static auto get(){
		return sx_locid_t::rand();
	}

};
template<>
struct rand_generator< identity_descriptor_id_type>{
	static auto get(){
		return identity_descriptor_id_type::rand();
	}
};

template<typename T>
struct rand_generator<T, std::enable_if_t<std::is_integral_v<T>>>{
	static auto get(){
		return identity_descriptor_id_type::rand().at<T>(0);
	}
};

template<typename T>
struct rand_generator<T, std::enable_if_t<srlz::detail::is_string_type_v<T>>>{
	static auto get(){
		return identity_descriptor_id_type::rand().to_string<typename T::value_type>();
	}
};

template<>
struct rand_generator<binary_vector_t>{
	static auto get(){
		return binary_vector_t((sx_uuid_t::rand().at<uint8_t>(0) % 1024) + 1, 1);
	}
};

template<>
struct rand_generator< session_description_t>{
	static auto get(){
		return session_description_t(
			rand_generator<session_id_t>::get(),
			rand_generator<std::string>::get());
	}
};

template<>
struct rand_generator< connection_key_t>{
	static auto get(){
		return connection_key_t{
			rand_generator<std::string>::get(),
			(int)rand_generator<int>::get()};
	}
};

template<>
struct rand_generator< identity_descriptor_t>{
	static auto get(){
		return identity_descriptor_t(
			rand_generator<identity_descriptor_id_type>::get(),
			rand_generator<std::string>::get());
	}
};

template<>
struct rand_generator< rt0_node_rdm_t>{
	static auto get(){
		return rt0_node_rdm_t::make_output_rdm(
			rand_generator<identity_descriptor_t>::get(),
			rand_generator<session_description_t>::get(),
			rand_generator<connection_key_t>::get(),
			rand_generator<connection_key_t>::get());
	}
};

template<>
struct rand_generator< rtl_roadmap_obj_t>{
	static auto get(){
		return rtl_roadmap_obj_t::make_rt1_ostream(
			rand_generator<rt0_node_rdm_t>::get(),
			rand_generator<identity_descriptor_t>::get(),
			rand_generator<identity_descriptor_t>::get(),
			rand_generator<session_description_t>::get());
	}
};

template<>
struct rand_generator< rtl_roadmap_link_t>{
	static auto get(){
		return rtl_roadmap_link_t(
			rand_generator<identity_descriptor_t>::get(),
			rand_generator<identity_descriptor_t>::get(),
			rand_generator<session_description_t>::get());
	}
};


template<>
struct rand_generator< rtl_roadmap_line_t>{
	static auto get(){
		rtl_roadmap_line_t r;
		r.points = rand_generator<rtl_roadmap_obj_t>::get();
		return r;
	}
};

template<>
struct rand_generator< rt1_streams_table_wait_key_t>{
	static auto get(){
		return rt1_streams_table_wait_key_t(
			rand_generator<rt0_node_rdm_t>::get(),
			rand_generator<rtl_roadmap_link_t>::get(),
			rtl_table_t::rt1_router);
	}
};

template<>
struct rand_generator< iol_type_spcf_t>{
	static auto get(){
		iol_type_spcf_t v;
		v.set(sx_uuid_t::rand().at< iol_type_spcf_t::field_type_t>(0));

		return v;
	}
};


template<>
struct rand_generator< outbound_message_desc_t>{
	static auto get(){
		outbound_message_desc_t v;
		v.chid = rand_generator<channel_identity_t>::get();
		v.isNull = false;
		v.level = rtl_level_t::l1;
		v.requestEmitter = rand_generator<subscriber_node_id_t>::get();
		v.responseTarget = rand_generator<subscriber_node_id_t>::get();
		v.spitTag = rand_generator<message_spin_tag_type>::get();
		v.streamId = rand_generator<rt1_streams_table_wait_key_t>::get();
		v.type = rand_generator< iol_type_spcf_t>::get();

		return v;
	}
};

template<>
struct rand_generator< rt1_x_message_data_t>{
	static auto get(){
		return rt1_x_message_data_t(
			rand_generator<rtl_roadmap_line_t>::get(),
			rand_generator<binary_vector_t>::get(),
			rand_generator<channel_identity_t>::get(),
			rand_generator<outbound_message_desc_t>::get());

	}
};
}

