#include <fstream>
#include <iterator>
#include "../../internal.h"
#include "./fio.h"


void crm::filesys::fio_rbin( const std::wstring &filename,
	std::vector<uint8_t> & vec,
	size_t offset,
	std::intmax_t count ) {

	std::basic_ifstream<uint8_t> file( filename, std::ios::binary );
	if( file ) {
		try {
			file.exceptions( std::ios_base::failbit | std::ios_base::badbit );
			file.unsetf( std::ios::skipws );

			// get its size:
			file.seekg( 0, std::ios::end );
			const auto fileSize = file.tellg();
			CBL_VERIFY( fileSize >= 0 );
			file.seekg( offset, std::ios::beg );

			if( fileSize >= 0 ) {
				if( -1 == count ) {
					vec.clear();
					vec.reserve( fileSize );

					// read the data:
					std::istreambuf_iterator<uint8_t> iter( file );
					std::copy( iter, (std::istreambuf_iterator<uint8_t>()), std::back_inserter( vec ) );
					}
				else {
					if( (size_t)fileSize < (offset + count) ) {
						THROW_EXC_FWD(nullptr);
						}

					const auto xcount_ = count >= 0 ? count : (size_t)fileSize - offset;
					if( xcount_ > 0 ) {

						vec.resize( xcount_ );

						file.read( vec.data(), xcount_ );
						}
					else {
						THROW_EXC_FWD(nullptr);
						}
					}
				}
			else {
				THROW_EXC_FWD(nullptr);
				}
			}
		catch( const std::exception & exc ) {
			EXTERN_CODE_THROW_EXC_FWD( exc );
			}
		}
	else {
		THROW_EXC_FWD(nullptr);
		}
	}


std::vector<uint8_t> crm::filesys::fio_rbin( const std::wstring &filename, 
	size_t offset, 
	std::intmax_t count ) {

	std::vector<uint8_t> vec;
	fio_rbin( filename, vec, offset, count );

	return vec;
	}

void crm::filesys::fio_wbin( const std::vector<uint8_t> &src, const std::wstring &filename ) {

	if( !src.empty() ) {
		std::basic_ofstream<uint8_t> of( filename, std::ios::binary );
		if( of ) {
			try {
				of.exceptions( std::ios_base::badbit );
				of.unsetf( std::ios::skipws );

				of.write( !src.empty() ? (const uint8_t*)src.data() : nullptr, src.size() );
				of.close();
				}
			catch( const std::exception & exc ) {
				EXTERN_CODE_THROW_EXC_FWD( exc );
				}
			}
		else {
			EXTERN_CODE_THROW_EXC_FWD(nullptr);
			}
		}
	}


void crm::filesys::fio_wtxt( const std::string &src, const std::wstring &filename ) {
	if( !src.empty() ) {
		std::ofstream of( filename );
		if( of ) {
			try {

				of.exceptions( std::ios_base::badbit );
				of.unsetf( std::ios::skipws );

				of.write( src.c_str(), src.size() );
				of.close();
				}
			catch( const std::exception & exc ) {
				EXTERN_CODE_THROW_EXC_FWD( exc );
				}
			}
		else {
			EXTERN_CODE_THROW_EXC_FWD(nullptr);
			}
		}
	}

