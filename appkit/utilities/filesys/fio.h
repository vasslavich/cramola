#ifndef DCF_UTILITY_FILE_IO_HEADER
#define DCF_UTILITY_FILE_IO_HEADER

#include <vector>
#include <string>
#include <iterator>

namespace crm{
namespace filesys{

std::vector<uint8_t> fio_rbin( const std::wstring &filename, size_t offset = 0, std::intmax_t count = -1 );
void fio_rbin( const std::wstring &filename, std::vector<uint8_t> & buf, size_t offset = 0, std::intmax_t count = -1 );

void fio_wbin( const std::vector<uint8_t> &src, const std::wstring &filename );
void fio_wtxt( const std::string &src, const std::wstring &filename );

template<typename Xstring>
typename Xstring fio_rtxt( const std::wstring &filename ) {

	typedef typename Xstring::value_type value_type;

	std::basic_ifstream<value_type> file( filename );

	// �� ���������� �������
	file.unsetf( std::ios::skipws );

	// ������ �����
	std::streampos fileSize;
	file.seekg( 0, std::ios::end );
	fileSize = file.tellg();
	file.seekg( 0, std::ios::beg );

	if( fileSize ) {
		// reserve capacity
		typename Xstring dest;
		dest.reserve( fileSize );

		// read the data:
		std::istreambuf_iterator<value_type> iter( file );
		std::copy( iter, (std::istreambuf_iterator<value_type>()), std::back_inserter( dest ) );

		return std::move( dest );
		}
	else
		return Xstring();
	}

template<typename Xstring>
typename Xstring fio_rtxt( const std::string &filename ) {
	return std::move( fio_rtxt<Xstring>( cbl::typecast::cvt( filename ) ) );
	}
}
}

#endif//DCF_UTILITY_FILE_IO_HEADER
