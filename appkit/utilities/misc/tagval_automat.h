#ifndef DCF_UTILITY_TAG_VALUE_AUTOMAT_HEADER
#define DCF_UTILITY_TAG_VALUE_AUTOMAT_HEADER

#include <functional>
#include <memory>
#include <map>
#include <mutex>
#include <list>
#include <type_traits>

namespace cbl{
namespace aux{

template<typename Xval>
class tagval_automat_t {
	static_assert(std::is_enum<typename Xval>::value || std::is_integral<typename Xval>::value, __FILE__);

public:
	typedef typename Xval value_type_t;

private:
	typedef std::mutex lck_t;
	typedef std::unique_lock<lck_t> lck_scp_t;

	struct link_t {
	private:
		value_type_t _value;
		std::weak_ptr<link_t> _upper;
		std::list<std::weak_ptr<link_t>> _next;

	public:
		static std::weak_ptr<link_t> null() {
			return std::move( std::weak_ptr<link_t>() );
			}

		link_t( const value_type_t & value_, std::weak_ptr<link_t> parent )
			: _value( value_ )
			, _upper( parent ) {}

		const value_type_t& value()const {
			return _value;
			}

		std::weak_ptr<link_t> parent()const {
			return _upper;
			}

		void add_next( std::weak_ptr<link_t> link ) {
			_next.push_back( std::move( link ) );
			}

		std::weak_ptr<link_t> find_next( const value_type_t &value_ )const {
			auto it = _next.cbegin();
			const auto end = _next.cend();
			for( ; it != end; ++it ) {
				auto lck( (*it).lock() );
				if( lck && value_ == lck->value() )
					return (*it);
				}

			return null();
			}
		};

	std::map<value_type_t,std::shared_ptr<link_t>> _set;
	std::weak_ptr<link_t> _root;
	std::weak_ptr<link_t> _current;
	std::atomic<bool> _valid{true};
	lck_t _lck;

	template<typename TxvalSequence>
	void _add( const value_type_t & to, const typename TxvalSequence & lst ) {

		/* �������� ������������� ������� ������������� ���� (to) */
		auto itPrnt = _set.find( to );
		if( itPrnt == _set.end() )
			FATAL_ERROR_C( 0 );

		/* ��������� �� ���� */
		auto pPrnt = itPrnt->second;
		if( pPrnt ) {

			/* ���������� ������ */
			auto itLst = lst.cbegin();
			const auto itLstEnd = lst.cend();
			for( ; itLst != itLstEnd; ++itLst ) {

				/* ����� ������� � ������ ���-����� */
				auto pNxt = pPrnt->find_next( (*itLst) ).lock();

				/* ���� �� ���������� - �������� */
				if( !pNxt ) {

					/* ����� � ������� ����� */
					auto itSet = _set.find( (*itLst) );
					/* ���� ���������� - �������� ��� ������ �� ���� ���� */
					if( itSet != _set.end() ) {
						pPrnt->add_next( itSet->second );
						}
					/* ���� �� ���������� - �������� */
					else {
						std::shared_ptr<link_t> ptr( new link_t( (*itLst), pPrnt ) );
						pPrnt->add_next( ptr );

						/* ���� �� ���������� � ������� */
						_set.insert( std::make_pair( (*itLst), std::move( ptr ) ) );
						}
					}
				}
			}
		else
			FATAL_ERROR_C( 0 );
		}

	bool _shifft( const value_type_t & to ) {
		if( _current.expired() ) {
			set_fault();
			return false;
			}

		auto lckCurr( _current.lock() );
		if( lckCurr )
			_current = lckCurr->find_next( to );
		/* �������������� ��������� */
		else
			FATAL_ERROR_C( 0 );

		/* �� �������� ��������� */
		if( _current.expired() ) {
			set_fault();
			return false;
			}

		return true;
		}

	bool _try_shifft( const value_type_t & to, bool & nonValid )CBL_NO_EXCEPTION{
		nonValid = true;

		if( _current.expired() ) {
			set_fault();
			return false;
			}

		auto lckCurr( _current.lock() );
		if( lckCurr ) {
			auto next = lckCurr->find_next( to );
			if( next.expired() )
				return false;

			_current = next;

			nonValid = false;
			return true;
			}
		/* �������������� ��������� */
		else {
			FATAL_TERMINATE_C( 0 );
			return false;
			}
		}

	void set_fault()CBL_NO_EXCEPTION{
		_valid.store( false, std::memory_order::memory_order_release );
		}

public:
	tagval_automat_t( const value_type_t & root ){

		std::shared_ptr<link_t> ptr( new link_t( root, link_t::null() ) );
		_root = ptr;
		_set.insert( std::make_pair( root, std::move( ptr ) ) );
		}

	void reset() {
		lck_scp_t lck( _lck );
		_valid.store( true, std::memory_order::memory_order_release );
		_current = _root;
		}

	bool valid()const {
		return _valid.load( std::memory_order::memory_order_acquire );
		}

	value_type_t value()const {
		if( !valid() )
			FATAL_ERROR_C( 0 );

		auto lck( _current.lock() );
		if( lck )
			return lck->value();

		FATAL_ERROR_C( 0 );
		}

	template<typename TxvalSequence>
	void add( const value_type_t & to, const typename TxvalSequence & lst ) {
		if( !valid() )
			FATAL_ERROR_C( 0 );

		lck_scp_t lck( _lck );
		_add( to, lst );
		}

	void add_self( const value_type_t & to ) {
		if( !valid() )
			FATAL_ERROR_C( 0 );

		lck_scp_t lck( _lck );
		_add( to, std::list<value_type_t>( {to} ) );
		}

	bool shifft( const value_type_t & to ) {
		if( !valid() )
			FATAL_ERROR_C( 0 );

		lck_scp_t lck( _lck );
		return _shifft( to );
		}

	bool try_shifft( const value_type_t & to, bool & nonValid ) CBL_NO_EXCEPTION{
		if( !valid() ) {
			nonValid = true;
			return false;
			}

		lck_scp_t lck( _lck );
		return _try_shifft( to, nonValid );
		}
	};

}
}

#endif///DCF_UTILITY_TAG_VALUE_AUTOMAT_HEADER
