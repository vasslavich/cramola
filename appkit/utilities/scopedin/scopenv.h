#pragma once


#include <string>


namespace crm::detail {

bool sys_is_big_endian();
bool sys_is_little_endian();

std::string get_this_thread_info(const std::string & tag = "")noexcept;
}


