#include <thread>
#include <sstream>
#include "../../../internal.h"
#include "../scopenv.h"


using namespace crm;


bool crm::detail::sys_is_big_endian() {
	return (*(uint16_t*)"\0\xff") < (uint16_t)0x100;
	}

bool crm::detail::sys_is_little_endian() {
	return !sys_is_big_endian();
	}

std::string crm::detail::get_this_thread_info(const std::string & tag)noexcept {
	std::ostringstream log;
	log << "thread-info=" << tag << ":" << std::this_thread::get_id();
	return log.str();
	}
