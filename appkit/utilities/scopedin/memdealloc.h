#pragma once


namespace crm::detail {

template<typename Tdeallocator>
struct deallocator_scope_t {
	Tdeallocator deallocator;

	void *dtRaw;

	deallocator_scope_t(const typename Tdeallocator &deallocator_, void *dtRaw_)
		: deallocator(deallocator_), dtRaw(dtRaw_) {}

	deallocator_scope_t(typename Tdeallocator &&deallocator_, void *dtRaw_)
		: deallocator(std::move(deallocator_)), dtRaw(dtRaw_) {}

	~deallocator_scope_t() {
		clear();
		}

	void clear() {
		if(nullptr != dtRaw)
			deallocator(dtRaw);

		dtRaw = nullptr;
		}
	};
}

