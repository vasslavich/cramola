#pragma once


#include <type_traits>
#include <atomic>
#include "../../exceptions.h"


namespace crm::detail {


template<typename _TEx,
	typename TEx = typename std::decay_t<_TEx>,
	typename = std::enable_if_t<std::is_invocable_v<_TEx>>>
class post_scope_action_t {
	public:
		using self_type = typename post_scope_action_t<_TEx>;

	private:
		TEx _f;
		std::atomic<bool> _executedf{ false };

		post_scope_action_t(const self_type &o) = delete;
		post_scope_action_t& operator=(const self_type &o) = delete;

		void __execute() {
			bool executed = false;
			if(_executedf.compare_exchange_strong(executed, true)) {

				if constexpr (!is_lambda_v<TEx>) {
					if (_f) {
						_f();
						}
					}
				else {
					_f();
					}
				}
			}

		void __execute_no_throw()noexcept {
			if(!_executedf.load(std::memory_order::memory_order_acquire)) {
				CHECK_NO_EXCEPTION_BEGIN

					__execute();
				
				CHECK_NO_EXCEPTION_END
				}
			}

	public:
		template<typename _XEx,
			typename XEx = typename std::decay_t<_XEx>,
			typename std::enable_if_t<(
				std::is_invocable_v<XEx> &&
				!std::is_same_v<self_type, XEx> &&
				std::is_nothrow_constructible_v<TEx, _XEx>
				), int> = 0>
			post_scope_action_t(_XEx && xf_)noexcept
			: _f(std::forward<_XEx>(xf_)) {}

		template<typename _XEx,
			typename XEx = typename std::decay_t<_XEx>,
			typename std::enable_if_t<(
				std::is_invocable_v<XEx> &&
				!std::is_same_v<self_type, XEx> &&
				!std::is_nothrow_constructible_v<TEx, _XEx>
				), int> = 0>
			post_scope_action_t(_XEx && xf_)
			: _f(std::forward<_XEx>(xf_)) {}

		~post_scope_action_t() {
			__execute_no_throw();
			}


		post_scope_action_t(self_type && o) noexcept
			: _f(std::move(o._f))
			, _executedf(o._executedf.exchange(true)) {}

		post_scope_action_t& operator=(self_type && o) noexcept{
			if(this != &o) {
				_f = std::move(o._f);
				_executedf.store(o._executedf.exchange(true));
				}

			return (*this);
			}

		template<typename _OEx,
			typename OEx = typename std::decay_t<_OEx>,
			typename std::enable_if_t<(
				std::is_invocable_v<OEx> &&
				!std::is_same_v<OEx, TEx> &&
				std::is_nothrow_constructible_v<TEx, std::decay_t<_OEx>>
				), int> = 0>
		post_scope_action_t(post_scope_action_t<_OEx> &&o)noexcept
			: _f(std::forward<decltype(o._f)>(o._f))
			, _executedf(o._executedf.exchange(true)) {}


		template<typename _OEx,
			typename OEx = typename std::decay_t<_OEx>,
			typename std::enable_if_t<(
				std::is_invocable_v<OEx> &&
				!std::is_same_v<OEx, TEx> &&
				std::is_nothrow_constructible_v<TEx, std::decay_t<_OEx>>
				), int> = 0>
		post_scope_action_t& operator=(post_scope_action_t<_OEx> &&o)noexcept {
			if(this != std::addressof(o)) {
				_f = std::forward<decltype(o._f)>(o._f);
				_executedf.store(o._executedf.exchange(true));
				}

			return (*this);
			}

		void reset()noexcept {
			_executedf.store(true);
			}

		void execute() { __execute(); }
		void operator()() { __execute(); }
	};


template<typename T>
post_scope_action_t(T &&)->post_scope_action_t<T>;

template<typename T,
	typename std::enable_if_t<std::is_invocable_v<T>, int> = 0>
auto make_postscope_action(T&& t) {
	return post_scope_action_t(std::forward<T>(t));
	}
}

