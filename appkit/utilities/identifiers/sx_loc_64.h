#pragma once


#include "../../internal_invariants.h"


namespace crm {


/*! ���������� ������������� ���������� � �������� ���� */
struct sx_loc_64 final {
	typedef std::size_t address_id_t;
private:
	address_id_t _aid{ 0 };

	sx_loc_64(address_id_t aid)noexcept;
	sx_loc_64(void* vaid)noexcept;

	constexpr address_id_t aid()const  noexcept {
		return _aid;
		}


public:
	static const sx_loc_64 null;

	template<typename Opt = none_options>
	static constexpr size_t serialized_size()noexcept {
		return srlz::write_size_v<decltype(_aid), Opt>;
		}

	constexpr sx_loc_64()noexcept;

	static sx_loc_64 make();
	static sx_loc_64 make(address_id_t aid);
	//static sx_loc_64 make(std::string_view s);
	static sx_loc_64 make_with_key(address_id_t aid);


	constexpr address_id_t xoffer()const  noexcept {
		return aid();
		}

	std::string to_str()const noexcept;
	std::wstring to_wstr()const noexcept;
	bool is_null()const noexcept;

	/*! ������������ ������� � �������� ������ */
	template<typename Tws>
	void srlz(Tws&& dest)const {
		srlz::serialize(dest, _aid);
		}

	/*! �������������� ������� �� ��������� ������� */
	template<typename Trs>
	void dsrlz(Trs&& src) {
		srlz::deserialize(src, _aid);
		}

	/*! ������ ���������������� �������, � ������ */
	size_t get_serialized_size()const noexcept;

	friend bool operator == (const sx_loc_64& lval, const sx_loc_64& rval)noexcept;
	friend bool operator != (const sx_loc_64& lval, const sx_loc_64& rval)noexcept;
	friend bool operator < (const sx_loc_64& lval, const sx_loc_64& rval)noexcept;
	friend bool operator > (const sx_loc_64& lval, const sx_loc_64& rval)noexcept;
	friend bool operator >= (const sx_loc_64& lval, const sx_loc_64& rval)noexcept;
	};


std::ostream& operator <<(std::ostream& os, const sx_loc_64& uuid);
std::wostream& operator <<(std::wostream& os, const sx_loc_64& uuid);



namespace detail {
bool check_sndrcv_key_spin_tag(const sx_loc_64& h)noexcept;
bool is_sndrcv_spin_tagged_invoke(const sx_loc_64& h)noexcept;
}


constexpr std_hash_type as_std_hash(const sx_loc_64& sx)noexcept {
	return sx.xoffer();
	}
}

template<>
struct ::std::hash<crm::sx_loc_64> {
	size_t operator()(crm::sx_loc_64 const& v) const noexcept {
		return crm::as_std_hash(v);
		}
	};


