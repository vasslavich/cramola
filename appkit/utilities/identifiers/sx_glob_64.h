#pragma once


#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <exception>
#include <functional>
#include "../../internal_invariants.h"
#include "../../typeframe/base_typetrait_decls.h"
#include "../../typeframe/base_typetrait_using.h"
#include "../../notifications/base_macrodef.h"


namespace crm {


struct sx_glob_64 final {
	using max_uint_type = std::uint64_t;
	using serialized_type = max_uint_type;

	template<typename T, typename Options, typename Enabled>
	friend struct srlz::serializable_properties_trait;

private:
	union {
		uint64_t u64_1;
		uint32_t u32_2[2];
		uint16_t u16_4[4];
		uint8_t u8_8[8];
		};

	template<typename T, typename = std::void_t<>>
	struct value_type_detect {};

	template<typename T>
	struct value_type_detect<T, std::enable_if_t<sizeof(T) == sizeof(uint64_t)>> { using type = uint64_t; };
	template<typename T>
	struct value_type_detect<T, std::enable_if_t<sizeof(T) == sizeof(uint32_t)>> { using type = uint32_t; };
	template<typename T>
	struct value_type_detect<T, std::enable_if_t<sizeof(T) == sizeof(uint16_t)>> { using type = uint16_t; };
	template<typename T>
	struct value_type_detect<T, std::enable_if_t<sizeof(T) == sizeof(uint8_t)>> { using type = uint8_t; };

	template<typename T, typename = std::void_t<>>
	struct index_bound {};
	template<typename T>
	struct index_bound<T, std::enable_if_t<sizeof(T) == sizeof(uint64_t)>> { static constexpr size_t bound = 1; };
	template<typename T>
	struct index_bound<T, std::enable_if_t<sizeof(T) == sizeof(uint32_t)>> { static constexpr size_t bound = 2; };
	template<typename T>
	struct index_bound<T, std::enable_if_t<sizeof(T) == sizeof(uint16_t)>> { static constexpr size_t bound = 4; };
	template<typename T>
	struct index_bound<T, std::enable_if_t<sizeof(T) == sizeof(uint8_t)>> { static  constexpr size_t bound = 8; };

public:
	static size_t const plane_size = sizeof(serialized_type);
	static size_t const str_len = sizeof(serialized_type);

public:
	template<typename Opt = none_options>
	static constexpr size_t serialized_size()noexcept {
		return srlz::write_size_v<decltype(u64_1), Opt>;
		}

	template<typename Tsequence>
	void from_sequence(const typename Tsequence& dest, size_t count) {
		if (dest.size() < plane_size || count < plane_size)
			throw std::out_of_range(__FILE_LINE__);

		from(&dest[0]);
		}

	template<typename Tsequence>
	void to_sequence(typename Tsequence& dest)const {

		if (dest.size())
			dest.resize(plane_size);

		to(&dest[0]);
		}

	constexpr const sx_glob_64& object_hash()const noexcept;

private:
	template<typename T>
	struct dependent_false : std::false_type {};

	template<typename _TElem,
		const size_t EN = sizeof(_TElem)>
		constexpr auto _at(size_t ind)noexcept -> typename value_type_detect<_TElem>::type& {
		CBL_VERIFY(ind < index_bound<_TElem>::bound);

		if constexpr (EN == sizeof(u64_1)) {
			ind;

			return u64_1;
			}
		else {
			if constexpr (EN == sizeof(u32_2[0])) {
				return u32_2[ind];
				}
			else {
				if constexpr (EN == sizeof(u16_4[0])) {
					return u16_4[ind];
					}
				else {
					if constexpr (EN == sizeof(u8_8[0])) {
						return u8_8[ind];
						}
					else {
						static_assert(dependent_false<_TElem>::value, __FILE__);
						}
					}
				}
			}
		}

	template<typename _TElem,
		const size_t EN = sizeof(_TElem)>
		constexpr auto _at(size_t ind)const noexcept-> const typename value_type_detect<_TElem>::type& {
		CBL_VERIFY(ind < index_bound<_TElem>::bound);

		if constexpr (EN == sizeof(u64_1)) {
			return u64_1;
			}
		else {
			if constexpr (EN == sizeof(u32_2[0])) {
				return u32_2[ind];
				}
			else {
				if constexpr (EN == sizeof(u16_4[0])) {
					return u16_4[ind];
					}
				else {
					if constexpr (EN == sizeof(u8_8[0])) {
						return u8_8[ind];
						}
					else {
						static_assert(dependent_false<_TElem>::value, __FILE__);
						}
					}
				}
			}
		}

	template<typename _TElem>
	constexpr auto _at()noexcept-> typename value_type_detect<_TElem>::type& {
		return u64_1;
		}

	template<typename _TElem>
	constexpr auto _at()const noexcept->const typename value_type_detect<_TElem>::type& {
		return u64_1;
		}

public:
	template<typename _TElem,
		const size_t EN = sizeof(_TElem)>
		constexpr auto at(size_t ind) noexcept-> typename value_type_detect<_TElem>::type& {
		static_assert(!std::is_const_v<std::remove_reference_t<decltype(_at<_TElem>(0))>> && std::is_lvalue_reference_v<decltype(_at<_TElem>(0))>, __FILE__);
		return _at<_TElem>(ind);
		}

	template<typename _TElem,
		const size_t EN = sizeof(_TElem)>
		constexpr auto at(size_t ind)const noexcept -> const typename value_type_detect<_TElem>::type& {
		static_assert(std::is_const_v<std::remove_reference_t<decltype(_at<_TElem>(0))>> && std::is_lvalue_reference_v<decltype(_at<_TElem>(0))>, __FILE__);
		return _at<_TElem>(ind);
		}

	template<typename _TElem>
	constexpr auto at()noexcept  -> typename value_type_detect<_TElem>::type& {
		static_assert(!std::is_const_v<std::remove_reference_t<decltype(_at<_TElem>())>> && std::is_lvalue_reference_v<decltype(_at<_TElem>())>, __FILE__);
		return _at<_TElem>();
		}

	template<typename _TElem>
	constexpr auto at()const noexcept  -> const typename value_type_detect<_TElem>::type& {
		static_assert(std::is_const_v<std::remove_reference_t<decltype(_at<_TElem>())>> && std::is_lvalue_reference_v<decltype(_at<_TElem>())>, __FILE__);
		return _at<_TElem>();
		}

	template<typename _TElem,
		typename TElem = std::enable_if_t<sizeof(typename std::decay_t<_TElem>) <= sizeof(u64_1), std::decay_t<_TElem>>,
		const size_t EN = sizeof(TElem)>
		static constexpr size_t count_of_as()noexcept {
		if constexpr (EN == sizeof(u64_1)) {
			return plane_size / sizeof(u64_1);
			}
		else {
			if constexpr (EN == sizeof(u32_2[0])) {
				return plane_size / sizeof(u32_2[0]);
				}
			else {
				if constexpr (EN == sizeof(u16_4[0])) {
					return plane_size / sizeof(u16_4[0]);
					}
				else {
					if constexpr (EN == sizeof(u8_8[0])) {
						return plane_size / sizeof(u8_8[0]);
						}
					else {
						static_assert(dependent_false<TElem>::value, __FILE__);
						}
					}
				}
			}
		}

	constexpr sx_glob_64()noexcept;

	sx_glob_64(size_t)noexcept;

	template<typename TInIt,
		typename TInItValue = decltype(*(std::declval<TInIt>())),
		typename std::enable_if_t<crm::srlz::detail::is_byte_v<TInItValue>, int> = 0>
		explicit sx_glob_64(typename TInIt first, typename TInIt last) {
		set(first, last);
		}

	template<typename Binary16,
		typename std::enable_if_t<srlz::detail::is_container_of_bytes_v<Binary16>, int> = 0>
		explicit sx_glob_64(Binary16&& bin) {
		set(bin.cbegin(), bin.cend());
		}

	explicit sx_glob_64(std::initializer_list<uint8_t> lst);

	template<typename TInIt,
		typename TInItValue = decltype(*(std::declval<TInIt>())),
		typename std::enable_if_t<crm::srlz::detail::is_byte_v<TInItValue>, int> = 0>
		void set(typename TInIt first, typename TInIt last) {
		if (std::distance(first, last) == plane_size) {
			for (size_t i = 0; i < plane_size; ++first, ++i)
				u8_8[i] = (*first);
			}
		else {
			throw std::out_of_range(__FILE_LINE__);
			}
		}

	template<typename _TInteger,
		typename TInteger = std::decay_t<_TInteger>,
		typename std::enable_if_t<std::is_integral_v<TInteger>, int> = 0>
	static sx_glob_64 make_set_all(_TInteger iv)noexcept {
		sx_glob_64 result;

		for (size_t i = 0; i < count_of_as<TInteger>(); ++i) {
			result.at<TInteger>(i) = iv;
			}

		return result;
		}

	template<typename TString>
	static sx_glob_64 as_hash(TString&& str)noexcept {
		static_assert(0 < address_hash_t::count_of_as<decltype(u64_1)>(), __FILE_LINE__);

		address_hash_t h;
		apply_at_hash(h, std::forward<TString>(str));

		sx_glob_64 r;
		for (size_t i = 0; i < address_hash_t::count_of_as<decltype(r.u64_1)>(); ++i) {
			r.u64_1 ^= h.at<decltype(r.u64_1)>(i);
			}

		return r;
		}

	template<class _Elem = char>
	std::basic_string<_Elem> to_string()const {
		std::basic_ostringstream<_Elem> os;
		os << u64_1;
		return os.str();
		}

	std::string to_str()const;
	std::wstring to_wstr()const;

	static sx_glob_64 rand()noexcept;
	bool is_null()const noexcept;

	template<typename Ts>
	void srlz(Ts& dest)const {
		srlz::serialize(dest, u64_1);
		}

	template<typename Ts>
	void dsrlz(Ts& src) {
		srlz::deserialize(src, u64_1);
		}

	std::size_t get_serialized_size()const noexcept;

	static const sx_glob_64 null;
	static const sx_glob_64 maxvalue;
	static const sx_glob_64 minvalue;

	friend bool operator == (const sx_glob_64& lval, const sx_glob_64& rval)noexcept;
	friend bool operator != (const sx_glob_64& lval, const sx_glob_64& rval)noexcept;
	friend bool operator < (const sx_glob_64& lval, const sx_glob_64& rval)noexcept;
	friend bool operator > (const sx_glob_64& lval, const sx_glob_64& rval)noexcept;
	friend bool operator >= (const sx_glob_64& lval, const sx_glob_64& rval)noexcept;
	friend bool operator <= (const sx_glob_64& lval, const sx_glob_64& rval)noexcept;

	friend sx_glob_64 operator ^ (const sx_glob_64& lval, const sx_glob_64& rval)noexcept;
	friend sx_glob_64 operator & (const sx_glob_64& lval, const sx_glob_64& rval)noexcept;
	friend sx_glob_64 operator | (const sx_glob_64& lval, const sx_glob_64& rval)noexcept;

	template<class T = uint8_t, const size_t N = plane_size>
	constexpr auto to_bytes()const->std::enable_if_t<(count_of_as<T>() > 0), std::array<T, N>>{
		std::array<T, N> r{ 0 };
		for (size_t i = 0; i < (std::min<size_t>)(N, count_of_as<T>()); ++i) {
			r.at(i) = at<T>(i);
			}

		return r;
		}
	};


std::ostream& operator <<(std::ostream& os, const sx_glob_64& uuid);
std::wostream& operator <<(std::wostream& os, const sx_glob_64& uuid);

constexpr std_hash_type as_std_hash(const sx_glob_64& sx)noexcept {
	static_assert(sx.count_of_as<std_hash_type>() == 1, __FILE_LINE__);
	return sx.at<std_hash_type>();
	}
}


template<>
struct ::std::hash<crm::sx_glob_64> {
	size_t operator()(const crm::sx_glob_64 & v) const noexcept {
		return crm::as_std_hash(v);
		}
	};

