#pragma once


#include <string>
#include "../../internal_invariants.h"


namespace crm {


/*! ���������� ������������� ���������� � �������� ���� */
struct sx_locid_t final{

	typedef std::size_t address_id_t;
	typedef std::uint64_t process_id_t;
	typedef std::uint64_t thread_id_t;

private:
	process_id_t _pid = 0;
	address_id_t _aid = 0;

	sx_locid_t(address_id_t aid);
	sx_locid_t(void* vaid);
	sx_locid_t(process_id_t pid, address_id_t aid)noexcept;
	sx_locid_t(process_id_t pid, void* vaid)noexcept;

	address_id_t aid()const noexcept;
	process_id_t pid()const noexcept;

public:
	template<typename Opt = none_options>
	static constexpr size_t serialized_size()noexcept {
		return srlz::write_size_v<decltype(_pid), Opt> +
			srlz::write_size_v<decltype(_aid), Opt>;
		}

	static const sx_locid_t null;

	sx_locid_t()noexcept;

	static sx_locid_t make();
	static sx_locid_t make(process_id_t b, address_id_t x);
	static sx_locid_t as_hash(std::string_view s);
	static sx_locid_t make_with_key(address_id_t x);

	address_id_t base()const noexcept;
	process_id_t xoffer()const noexcept;
	std::string to_str()const noexcept;
	std::wstring to_wstr()const noexcept;
	bool is_null()const noexcept;

	/*! ������������ ������� � �������� ������ */
	template<typename Tws>
	void srlz(Tws && dest)const{
		srlz::serialize(dest, _pid);
		srlz::serialize(dest, _aid);
		}

	/*! �������������� ������� �� ��������� ������� */
	template<typename Trs>
	void dsrlz(Trs && src){
		srlz::deserialize(src, _pid);
		srlz::deserialize(src, _aid);
		}

	/*! ������ ���������������� �������, � ������ */
	size_t get_serialized_size()const noexcept;

	static process_id_t current_pid();
	static thread_id_t current_tid();
	static sx_locid_t rand();

	friend bool operator == (const sx_locid_t & lval, const sx_locid_t &rval)noexcept;
	friend bool operator != (const sx_locid_t & lval, const sx_locid_t &rval)noexcept;
	friend bool operator < (const sx_locid_t & lval, const sx_locid_t &rval)noexcept;
	friend bool operator > (const sx_locid_t & lval, const sx_locid_t &rval)noexcept;
	friend bool operator >= (const sx_locid_t & lval, const sx_locid_t &rval)noexcept;
	};


std::ostream  & operator <<(std::ostream & os, const sx_locid_t & uuid);
std::wostream  & operator <<(std::wostream & os, const sx_locid_t & uuid);


namespace detail {
bool check_sndrcv_key_spin_tag(const sx_locid_t& h)noexcept;
bool is_sndrcv_spin_tagged_invoke(const sx_locid_t& h)noexcept;
}
}
