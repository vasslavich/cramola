#pragma once


#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <exception>
#include <algorithm>
#include "../../internal_invariants.h"
#include "../../typeframe/base_terms.h"
#include "../../notifications/base_macrodef.h"
#include "../../typeframe/base_typetrait_decls.h"
#include "../../typeframe/base_typetrait_using.h"


namespace crm{


struct sx_uuid_t final{
public:
	static size_t const plane_size = 16;
	static size_t const str_len = plane_size * 2 + 4;

public:
	void to_raw(uint8_t buf[plane_size])const;

	template<typename Opt = none_options>
	static constexpr size_t serialized_size()noexcept {
		return srlz::write_size_v<decltype(u64_2[0]), Opt> * 2;
		}

	template<typename Tsequence>
	void from_sequence( const typename Tsequence &dest, size_t count ){
		if( dest.size() < plane_size || count < plane_size )
			throw std::out_of_range(__FILE_LINE__);

		from( &dest[0] );
		}

	template<typename Tsequence>
	void to_sequence( typename Tsequence &dest )const{

		if( dest.size() )
			dest.resize( plane_size );

		to( &dest[0] );
		}

	union{
		struct{
			uint32_t	time_low;
			uint16_t	time_mid;
			uint16_t	time_hi_and_version;
			uint8_t		clock_seq_hi_and_reserved;
			uint8_t		clock_seq_low;
			uint8_t		node[6];
			};

		uint64_t u64_2[2];
		uint32_t u32_4[4];
		uint16_t u16_8[8];
		uint8_t u8_16[16];
		};


	template<typename T, typename = std::void_t<>>
	struct value_type_detect {};

	template<typename T>
	struct value_type_detect<T, std::enable_if_t<sizeof(T) == sizeof(uint64_t)>> { using type = uint64_t; };
	template<typename T>
	struct value_type_detect<T, std::enable_if_t<sizeof(T) == sizeof(uint32_t)>> { using type = uint32_t; };
	template<typename T>
	struct value_type_detect<T, std::enable_if_t<sizeof(T) == sizeof(uint16_t)>> { using type = uint16_t; };
	template<typename T>
	struct value_type_detect<T, std::enable_if_t<sizeof(T) == sizeof(uint8_t)>> { using type = uint8_t; };

	template<typename T, typename = std::void_t<>>
	struct index_bound {};
	template<typename T>
	struct index_bound<T, std::enable_if_t<sizeof(T) == sizeof(uint64_t)>> { static constexpr size_t bound = 2; };
	template<typename T>
	struct index_bound<T, std::enable_if_t<sizeof(T) == sizeof(uint32_t)>> { static constexpr size_t bound = 4; };
	template<typename T>
	struct index_bound<T, std::enable_if_t<sizeof(T) == sizeof(uint16_t)>> { static constexpr size_t bound = 8; };
	template<typename T>
	struct index_bound<T, std::enable_if_t<sizeof(T) == sizeof(uint8_t)>> { static  constexpr size_t bound = 16; };

private:
	template<typename T>
	struct dependent_false : std::false_type{};

	template<typename _TElem,
		const size_t EN = sizeof( _TElem )>
		constexpr auto _at( size_t ind )noexcept  -> typename value_type_detect<_TElem>::type& {
		CBL_VERIFY(ind < index_bound<_TElem>::bound);

		if constexpr( EN == sizeof( u64_2[0] ) ){
			return u64_2[ind];
			}
		else{
			if constexpr( EN == sizeof( u32_4[0] ) ){
				return u32_4[ind];
				}
			else{
				if constexpr( EN == sizeof( u16_8[0] ) ){
					return u16_8[ind];
					}
				else{
					if constexpr( EN == sizeof( u8_16[0] ) ){
						return u8_16[ind];
						}
					else{
						static_assert(dependent_false<_TElem>::value, __FILE__);
						}
					}
				}
			}
		}


	template<typename _TElem,
		const size_t EN = sizeof(_TElem)>
		constexpr auto _at(size_t ind)const noexcept -> const typename value_type_detect<_TElem>::type& {
		CBL_VERIFY(ind < index_bound<_TElem>::bound);

		if constexpr (EN == sizeof(u64_2[0])) {
			return u64_2[ind];
			}
		else {
			if constexpr (EN == sizeof(u32_4[0])) {
				return u32_4[ind];
				}
			else {
				if constexpr (EN == sizeof(u16_8[0])) {
					return u16_8[ind];
					}
				else {
					if constexpr (EN == sizeof(u8_16[0])) {
						return u8_16[ind];
						}
					else {
						static_assert(dependent_false<_TElem>::value, __FILE__);
						}
					}
				}
			}
		}

public:
	template<typename _TElem>
	constexpr auto at( size_t ind )noexcept -> typename value_type_detect<_TElem>::type& {
		static_assert(!std::is_const_v<std::remove_reference_t<decltype(_at<_TElem>(0))>> && std::is_lvalue_reference_v<decltype(_at<_TElem>( 0 ))>, __FILE__);
		return _at<_TElem>( ind );
		}

	template<typename _TElem>
	constexpr auto at( size_t ind )const noexcept -> const typename value_type_detect<_TElem>::type& {
		static_assert(std::is_const_v<std::remove_reference_t<decltype(_at<_TElem>( 0 ))>> && std::is_lvalue_reference_v<decltype(_at<_TElem>( 0 ))>, __FILE__);
		return _at<_TElem>( ind );
		}

	template<typename _TElem,
		const size_t EN = sizeof( _TElem )>
	static constexpr size_t count_of_as()noexcept{
		if constexpr( EN == sizeof( u64_2[0] ) ){
			return plane_size / sizeof( u64_2[0] );
			}
		else{
			if constexpr( EN == sizeof( u32_4[0] ) ){
				return plane_size / sizeof( u32_4[0] );
				}
			else{
				if constexpr( EN == sizeof( u16_8[0] ) ){
					return plane_size / sizeof( u16_8[0] );
					}
				else{
					if constexpr( EN == sizeof( u8_16[0] ) ){
						return plane_size / sizeof( u8_16[0] );
						}
					else{
						static_assert(dependent_false<_TElem>::value, __FILE__);
						}
					}
				}
			}
		}

	sx_uuid_t()noexcept;

	template<typename TContainerOfInts,
		typename std::enable_if_t<srlz::detail::is_container_of_integral_v<TContainerOfInts>, int> = 0>
	explicit sx_uuid_t(TContainerOfInts && ic)noexcept {
		assign(std::forward<TContainerOfInts>(ic));
		}

	template<typename TInIt,
		typename std::enable_if_t<srlz::detail::is_iterator_of_integral_v<TInIt>,int> = 0>
	explicit sx_uuid_t( TInIt first, TInIt last )noexcept{
		assign(first, last);
		}

	template<typename TIntegral,
		typename std::enable_if_t<std::is_integral_v<TIntegral> && !srlz::detail::is_iterator_of_integral_v<TIntegral>, int> = 0>
	explicit sx_uuid_t(std::initializer_list<TIntegral> ints)noexcept {
		assign(ints);
		}

	template<typename TInIt,
		typename std::enable_if_t<srlz::detail::is_iterator_of_integral_v<TInIt>, int> = 0>
	void assign( TInIt first, TInIt last )noexcept{

		using integral_type = std::decay_t<decltype(*(std::declval<TInIt>()))>;
		const auto typedCount = count_of_as<integral_type>();
		const auto rangeCount = std::distance(first, last);
		const auto iterationCount = (std::min<size_t>)(typedCount, rangeCount);

		for (size_t i = 0; i < iterationCount; ++i) {
			at<integral_type>(i) = (*first++);
			}
		}

	template<typename TIntegral,
		typename std::enable_if_t<std::is_integral_v<TIntegral> && !srlz::detail::is_iterator_of_integral_v<TIntegral>, int> = 0>
	void assign(std::initializer_list<TIntegral> ints)noexcept {
		assign(ints.begin(), ints.end());
		}

	template<typename TContainerOfInts,
		typename std::enable_if_t<srlz::detail::is_container_of_integral_v<TContainerOfInts>, int> = 0>
	void assign(TContainerOfInts&& ic)noexcept {
		assign(ic.cbegin(), ic.cend());
		}

	static sx_uuid_t as_uuid(std::string_view str);
	static sx_uuid_t as_uuid(std::wstring_view str);

	template<typename TString>
	static sx_uuid_t as_hash(TString&& str)noexcept {
		sx_uuid_t h;
		apply_at_hash(h, std::forward<TString>(str));

		return h;
		}

	static sx_uuid_t from_string(std::string_view s);
	static sx_uuid_t from_string(std::wstring_view s);

	void assing_line(const uint8_t buf[plane_size]);
	const uint8_t* line()const noexcept;
	const void* pmobject()const noexcept;
	void* pmobject()noexcept;
	size_t pmsize()const noexcept;

	template<class _Elem = char>
	std::basic_string<_Elem> to_string()const{
		uint8_t plane[plane_size];
		to_raw( plane );

		char symb[3] = { 0 };

		std::basic_stringstream<_Elem> ss;
		ss << std::hex << std::setw( 2 );

		sprintf( symb, "%02x", plane[0] ); ss << symb;
		sprintf( symb, "%02x", plane[1] ); ss << symb;
		sprintf( symb, "%02x", plane[2] ); ss << symb;
		sprintf( symb, "%02x", plane[3] ); ss << symb << "-";
		sprintf( symb, "%02x", plane[4] ); ss << symb;
		sprintf( symb, "%02x", plane[5] ); ss << symb << "-";
		sprintf( symb, "%02x", plane[6] ); ss << symb;
		sprintf( symb, "%02x", plane[7] ); ss << symb << "-";
		sprintf( symb, "%02x", plane[8] ); ss << symb;
		sprintf( symb, "%02x", plane[9] ); ss << symb << "-";
		sprintf( symb, "%02x", plane[10] ); ss << symb;
		sprintf( symb, "%02x", plane[11] ); ss << symb;
		sprintf( symb, "%02x", plane[12] ); ss << symb;
		sprintf( symb, "%02x", plane[13] ); ss << symb;
		sprintf( symb, "%02x", plane[14] ); ss << symb;
		sprintf( symb, "%02x", plane[15] ); ss << symb;

		return ss.str();
		}

	std::string to_str()const;
	std::wstring to_wstr()const;

	static sx_uuid_t rand()noexcept;
	bool is_null()const noexcept;

	template<typename Ts>
	void srlz(Ts & dest)const{
		srlz::serialize(dest, u64_2[0]);
		srlz::serialize(dest, u64_2[1]);
		}

	template<typename Ts>
	void dsrlz(Ts & src ){
		srlz::deserialize(src, u64_2[0]);
		srlz::deserialize(src, u64_2[1]);
		}

	std::size_t get_serialized_size()const noexcept;

	static const sx_uuid_t null;
	static const sx_uuid_t maxvalue;
	static const sx_uuid_t minvalue;

	template<typename _TInteger,
		typename TInteger = std::decay_t<_TInteger>,
		typename std::enable_if_t<std::is_integral_v<TInteger>, int> = 0>
	static sx_uuid_t make_set_all(_TInteger iv)noexcept {
		sx_uuid_t result;

		for (size_t i = 0; i < count_of_as<TInteger>(); ++i) {
			result.at<TInteger>(i) = iv;
			}

		return result;
		}

	template<class T, size_t N>
	constexpr auto to_bytes()const->std::enable_if_t<(count_of_as<T>() > 0), std::array<T, N>>{
		std::array<T, N> r{ 0 };
		for (size_t i = 0; i < (std::min<size_t>)(N, count_of_as<T>()); ++i) {
			r.at(i) = at<T>(i);
			}

		return r;
		}
	};

bool operator == (const sx_uuid_t & lval, const sx_uuid_t &rval)noexcept;
bool operator != (const sx_uuid_t & lval, const sx_uuid_t &rval)noexcept;
bool operator < (const sx_uuid_t & lval, const sx_uuid_t &rval)noexcept;
bool operator > (const sx_uuid_t & lval, const sx_uuid_t &rval)noexcept;
bool operator >= (const sx_uuid_t & lval, const sx_uuid_t &rval)noexcept;
bool operator <= (const sx_uuid_t & lval, const sx_uuid_t &rval)noexcept;

sx_uuid_t operator ^ (const sx_uuid_t & lval, const sx_uuid_t &rval)noexcept;
sx_uuid_t operator & (const sx_uuid_t & lval, const sx_uuid_t &rval)noexcept;
sx_uuid_t operator | (const sx_uuid_t & lval, const sx_uuid_t &rval)noexcept;

std::ostream  & operator <<(std::ostream & os, const sx_uuid_t & uuid);
std::wostream  & operator <<(std::wostream & os, const sx_uuid_t & uuid);


constexpr std_hash_type as_std_hash(const sx_uuid_t& sx)noexcept {
	static_assert(sx.count_of_as<std_hash_type>() == 2, __FILE_LINE__);
	return sx.at<std_hash_type>(0) ^ sx.at<std_hash_type>(1);
	}
}


template<>
struct ::std::hash<crm::sx_uuid_t> {
	size_t operator()(crm::sx_uuid_t const& v) const noexcept {
		return crm::as_std_hash(v);
		}
	};






