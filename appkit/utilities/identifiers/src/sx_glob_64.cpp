#include "../../../internal.h"
#include "../sx_glob_64.h"
#include "../uuid.h"


using namespace crm;



static_assert(sizeof(decltype(std::declval<std::hash<sx_glob_64>>()(std::declval<sx_glob_64>()))) >= sizeof(size_t), __FILE_LINE__);
static_assert(detail::has_std_hash_support_v<sx_glob_64>, __FILE_LINE__);
static_assert(sizeof(decltype(get_hash_std(std::declval<sx_glob_64>()))) == sizeof(std_hash_type), __FILE_LINE__);
//static_assert(as_std_hash(sx_glob_64()) == 0, __FILE_LINE__);


const sx_glob_64 sx_glob_64::null;

constexpr const sx_glob_64& sx_glob_64::object_hash()const noexcept {
	return (*this);
	}

constexpr sx_glob_64::sx_glob_64()noexcept
	: u64_1{ 0 } {}

sx_glob_64::sx_glob_64(size_t value)noexcept
	: u64_1{ value } {}

sx_glob_64::sx_glob_64(std::initializer_list<uint8_t> lst)
	: sx_glob_64(lst.begin(), lst.end()) {}


std::string sx_glob_64::to_str()const {
	return std::to_string(u64_1);
	}

std::wstring sx_glob_64::to_wstr()const {
	return std::to_wstring(u64_1);
	}

bool sx_glob_64::is_null()const noexcept {
	return 0 == u64_1;
	}

sx_glob_64 sx_glob_64::rand()noexcept {
	const auto u16 = sx_uuid_t::rand();
	
	sx_glob_64 r;
	r.u64_1 = u16.at<decltype(r.u64_1)>(0) ^ u16.at<decltype(r.u64_1)>(1);

	return r;
	}

std::size_t sx_glob_64::get_serialized_size()const noexcept {
	return serialized_size<>();
	}

bool crm::operator == (const sx_glob_64& lval, const sx_glob_64& rval)noexcept {
	return	lval.u64_1 == rval.u64_1;
	}

bool crm::operator != (const sx_glob_64& lval, const sx_glob_64& rval)noexcept {
	return !(lval == rval);
	}

/*! ��������� ����������� � �������, ���,
������� 64 ������ ������ �������� ����� � ������� u64_2 �� ������� 0. */
bool crm::operator < (const sx_glob_64& lval, const sx_glob_64& rval)noexcept {
	return lval.u64_1 < rval.u64_1;
	}

/*! ��������� ����������� � �������, ���,
������� 64 ������ ������ �������� ����� � ������� u64_2 �� ������� 0. */
bool crm::operator > (const sx_glob_64& lval, const sx_glob_64& rval) noexcept {
	return lval.u64_1 > rval.u64_1;
	}

/*! ��������� ����������� � �������, ���,
������� 64 ������ ������ �������� ����� � ������� u64_2 �� ������� 0. */
bool crm::operator >= (const sx_glob_64& lval, const sx_glob_64& rval) noexcept {
	return lval.u64_1 >= rval.u64_1;
	}

/*! ��������� ����������� � �������, ���,
������� 64 ������ ������ �������� ����� � ������� u64_2 �� ������� 0. */
bool crm::operator <= (const sx_glob_64& lval, const sx_glob_64& rval)noexcept {
	return lval.u64_1 <= rval.u64_1;
	}

std::ostream& crm::operator <<(std::ostream& os, const sx_glob_64& uuid) {
	os << uuid.to_str();
	return os;
	}

std::wostream& crm::operator <<(std::wostream& os, const sx_glob_64& uuid) {
	os << uuid.to_wstr();
	return os;
	}


sx_glob_64 crm::operator ^ (const sx_glob_64& lval, const sx_glob_64& rval)noexcept {
	return lval.u64_1 ^ rval.u64_1;
	}
sx_glob_64 crm::operator & (const sx_glob_64& lval, const sx_glob_64& rval) noexcept {
	return lval.u64_1 & rval.u64_1;
	}
sx_glob_64 crm::operator | (const sx_glob_64& lval, const sx_glob_64& rval) noexcept {
	return lval.u64_1 | rval.u64_1;
	}



