#include <atomic>
#include "../../../internal.h"
#include "../uuid_based_inc.h"


using namespace crm;


static uint64_t make_rand_u64()noexcept {
	auto ruuid = crm::sx_uuid_t::rand();
	return ruuid.u64_2[0] ^ ruuid.u64_2[1];
	}

uuid_based_inc::value_type uuid_based_inc::next()noexcept {
	static const uint64_t base = make_rand_u64();
	static std::atomic<uint64_t> inc{ 0 };

	value_type v;
	v.u64_2[0] = base;
	v.u64_2[1] = ++inc;

	return v;
	}
