#include <Windows.h>
#include "../../../internal.h"


using namespace crm;
using namespace crm::detail;


static_assert(srlz::detail::is_i_srlzd_base_v<sx_locid_t>, __FILE_LINE__);
static_assert(srlz::detail::is_i_srlzd_prefixed_v<sx_locid_t>, __FILE_LINE__);
static_assert(is_null_trait_v<sx_locid_t>, __FILE_LINE__);

size_t sx_locid_t::get_serialized_size()const noexcept {
	return serialized_size<>();
	}


const sx_locid_t sx_locid_t::null = sx_locid_t();

sx_locid_t::sx_locid_t()noexcept {}

sx_locid_t::sx_locid_t( address_id_t aid_ )
	: sx_locid_t(current_pid(), aid_){}

sx_locid_t::sx_locid_t( void * vaid_ )
	: sx_locid_t( (address_id_t)vaid_ ) {}

sx_locid_t::sx_locid_t( process_id_t pid_, address_id_t aid_ )noexcept
	: _pid( pid_ )
	, _aid( aid_ ) {}

sx_locid_t::sx_locid_t( process_id_t pid_, void * vaid_ )noexcept
	: sx_locid_t( pid_, (address_id_t)vaid_ ) {}

sx_locid_t::process_id_t sx_locid_t::current_pid() {
	return ::GetCurrentProcessId();
	}

sx_locid_t::thread_id_t sx_locid_t::current_tid() {
	return ::GetCurrentThreadId();
	}

sx_locid_t sx_locid_t::rand() {
	const auto randValue = sx_uuid_t::rand();
	return sx_locid_t(current_pid(), randValue.at<size_t>(0) ^ randValue.at<size_t>(1));
	}

sx_locid_t::address_id_t sx_locid_t::aid()const  noexcept {
	return _aid;
	}

sx_locid_t::process_id_t sx_locid_t::pid()const  noexcept {
	return _pid;
	}

bool sx_locid_t::is_null()const  noexcept {
	return (*this) == null;
	}

std::string sx_locid_t::to_str()const  noexcept {
	std::string sbld;
	sbld += crm::detail::cvt_to_str( pid() ) + ":" + crm::detail::cvt_to_str(aid());
	return std::move( sbld );
	}

std::wstring sx_locid_t::to_wstr()const  noexcept {
	std::wstring sbld;
	sbld += std::to_wstring( pid() ) + L":" + std::to_wstring( aid() );
	return std::move( sbld );
	}

sx_locid_t sx_locid_t::make() {
	auto rndv = sx_uuid_t::rand();

	sx_locid_t v;
	v._pid = rndv.u64_2[0];
	v._aid = rndv.u64_2[1];

	return std::move( v );
	}

sx_locid_t sx_locid_t::make( process_id_t pid, address_id_t aid ) {
	sx_locid_t v;
	v._pid = pid;
	v._aid = aid;

	return std::move( v );
	}

sx_locid_t sx_locid_t::make_with_key(address_id_t aid) {
	return make(0, aid);
	}

sx_locid_t sx_locid_t::as_hash(std::string_view s) {
	auto u = crm::sx_uuid_t::as_hash(s);
	return make(u.at<decltype(_pid)>(0), u.at<decltype(_aid)>(1));
	}

sx_locid_t::address_id_t sx_locid_t::base()const  noexcept {
	return pid();
	}

sx_locid_t::process_id_t sx_locid_t::xoffer()const  noexcept {
	return aid();
	}

bool crm::operator == (const sx_locid_t & lval, const sx_locid_t &rval)  noexcept {
	return lval.pid() == rval.pid() && lval.aid() == rval.aid();
	}

bool crm:: operator != (const sx_locid_t & lval, const sx_locid_t &rval)  noexcept {
	return !(lval == rval);
	}

bool crm::operator < (const sx_locid_t & lval, const sx_locid_t &rval)  noexcept {
	if( lval.pid() < rval.pid() )
		return true;
	else if( lval.pid() > rval.pid() )
		return false;
	else
		return lval.aid() < rval.aid();
	}

bool crm::operator >( const sx_locid_t & lval, const sx_locid_t &rval )  noexcept {
	if( lval.pid() > rval.pid() )
		return true;
	else if( lval.pid() < rval.pid() )
		return false;
	else
		return lval.aid() > rval.aid();
	}

bool crm::operator >= (const sx_locid_t & lval, const sx_locid_t &rval)  noexcept {
	if( lval.pid() > rval.pid() )
		return true;
	else if( lval.pid() < rval.pid() )
		return false;
	else
		return lval.aid() >= rval.aid();
	}

std::ostream  & crm::operator <<(std::ostream & os, const sx_locid_t & uuid) {
	os << uuid.to_str();
	return os;
	}

std::wostream  & crm::operator <<(std::wostream & os, const sx_locid_t & uuid) {
	os << cvt<wchar_t>( uuid.to_str() );
	return os;
	}

bool crm::detail::is_sndrcv_spin_tagged_invoke(const sx_locid_t& h)noexcept {
	return !is_null(h);
	}

bool crm::detail::check_sndrcv_key_spin_tag(const sx_locid_t& h)noexcept {
	return is_sndrcv_spin_tagged_invoke(h);
	}

