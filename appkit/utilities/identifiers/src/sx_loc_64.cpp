#include "../../../internal.h"
#include "../sx_loc_64.h"


using namespace crm;
using namespace crm::detail;


const sx_loc_64 sx_loc_64::null;


static_assert(srlz::detail::is_i_srlzd_base_v<sx_loc_64>, __FILE_LINE__);
static_assert(srlz::detail::is_i_srlzd_prefixed_v<sx_loc_64>, __FILE_LINE__);
static_assert(is_null_trait_v<sx_loc_64>, __FILE_LINE__);

size_t sx_loc_64::get_serialized_size()const noexcept {
	return serialized_size<>();
	}

constexpr sx_loc_64::sx_loc_64()noexcept {}

sx_loc_64::sx_loc_64(address_id_t aid_)noexcept
	: _aid(aid_) {}

sx_loc_64::sx_loc_64(void* vaid_)noexcept
	: _aid( (address_id_t)vaid_) {}

bool sx_loc_64::is_null()const  noexcept {
	return (*this) == null;
	}

std::string sx_loc_64::to_str()const  noexcept {
	return std::to_string(aid());
	}

std::wstring sx_loc_64::to_wstr()const  noexcept {
	return std::to_wstring(aid());
	}

sx_loc_64 sx_loc_64::make() {
	auto rndv = sx_uuid_t::rand();

	sx_loc_64 v;
	v._aid = rndv.u64_2[0] ^ rndv.u64_2[1];

	return std::move(v);
	}

sx_loc_64 sx_loc_64::make(address_id_t aid) {
	sx_loc_64 v;
	v._aid = aid;

	return std::move(v);
	}

sx_loc_64 sx_loc_64::make_with_key(address_id_t aid) {
	return make( aid);
	}
//
//sx_loc_64 sx_loc_64::make(std::string_view s) {
//	auto u = crm::sx_uuid_t::from_str(s);
//	return make(u.at<decltype(_aid)>(0) ^ u.at<decltype(_aid)>(1));
//	}


bool crm::operator == (const sx_loc_64& lval, const sx_loc_64& rval)  noexcept {
	return lval.aid() == rval.aid();
	}

bool crm:: operator != (const sx_loc_64& lval, const sx_loc_64& rval)  noexcept {
	return !(lval == rval);
	}

bool crm::operator < (const sx_loc_64& lval, const sx_loc_64& rval)  noexcept {
	return lval.aid() < rval.aid();
	}

bool crm::operator >(const sx_loc_64& lval, const sx_loc_64& rval)  noexcept {
	return lval.aid() > rval.aid();
	}

bool crm::operator >= (const sx_loc_64& lval, const sx_loc_64& rval)  noexcept {
	return lval.aid() >= rval.aid();
	}

std::ostream& crm::operator <<(std::ostream& os, const sx_loc_64& uuid) {
	os << uuid.to_str();
	return os;
	}

std::wostream& crm::operator <<(std::wostream& os, const sx_loc_64& uuid) {
	os << cvt<wchar_t>(uuid.to_str());
	return os;
	}

bool crm::detail::is_sndrcv_spin_tagged_invoke(const sx_loc_64& h)noexcept {
	return !is_null(h);
	}

bool crm::detail::check_sndrcv_key_spin_tag(const sx_loc_64& h)noexcept {
	return is_sndrcv_spin_tagged_invoke(h);
	}

