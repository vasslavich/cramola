#include <memory.h>
#include <iostream>
#include <chrono>
#include <Windows.h>
#include "../../../internal.h"
#include "../uuid.h"
#include "../../convert/cvt.h"



using namespace crm;
using namespace crm::srlz;
using namespace crm::detail;


static_assert(sizeof(decltype(std::declval<std::hash<sx_uuid_t>>()(std::declval<sx_uuid_t>()))) >= sizeof(size_t), __FILE_LINE__);
static_assert(has_std_hash_support_v<sx_uuid_t>, __FILE_LINE__);
static_assert(sizeof(decltype(get_hash_std(std::declval<sx_uuid_t>()))) == sizeof(std_hash_type), __FILE_LINE__);


const auto dmp1 = sx_uuid_t(std::array<size_t, 2>{0});
const auto dmp2 = sx_uuid_t({ 10ull, 12ull });



static_assert(srlz::detail::is_i_srlzd_prefixed_v<sx_uuid_t>, __FILE_LINE__);

/*static_assert(srlz::detail::is_strob_serializable_v<sx_uuid_t>, __FILE_LINE__);
static_assert(srlz::detail::is_strob_flat_serializable_v<sx_uuid_t>, __FILE_LINE__);
static_assert(srlz::detail::__fields_count_hepler_t<sx_uuid_t>::count > 1, __FILE_LINE__);*/


static_assert(sx_uuid_t::count_of_as<uint8_t>() == 16, __FILE__);
static_assert(sx_uuid_t::count_of_as<uint16_t>() == 8, __FILE__);
static_assert(sx_uuid_t::count_of_as<uint32_t>() == 4, __FILE__);
static_assert(sx_uuid_t::count_of_as<uint64_t>() == 2, __FILE__);


static_assert(std::is_lvalue_reference_v<decltype(std::declval<sx_uuid_t>().at<uint8_t>(0))>,__FILE__);
static_assert(std::is_lvalue_reference_v<decltype(std::declval<sx_uuid_t>().at<uint16_t>(0))>, __FILE__);
static_assert(std::is_lvalue_reference_v<decltype(std::declval<sx_uuid_t>().at<uint32_t>(0))>, __FILE__);
static_assert(std::is_lvalue_reference_v<decltype(std::declval<sx_uuid_t>().at<uint64_t>(0))>, __FILE__);


static void uuid_unpack( sx_uuid_t &plan, const void *in );

#ifdef CBL_USE_BIG_LITTLE_ENDIAN
static void uuid_unpack_littleendian( sx_uuid_t &plan, const void *in );
#endif//CBL_USE_BIG_LITTLE_ENDIAN

/*static*/ void uuid_unpack_raw( sx_uuid_t &plan, const void *in );
static std::string uuid_to_str( const sx_uuid_t &uuid );
static sx_uuid_t uuid_from_str(std::string_view str);
static sx_uuid_t uuid_from_str(std::wstring_view str);



std::size_t sx_uuid_t::get_serialized_size()const noexcept {
	return serialized_size<>();
	}

static sx_uuid_t create_max_uuid()noexcept{
	sx_uuid_t value;

	value.u64_2[0] = std::numeric_limits<std::decay_t<decltype(value.u64_2[0])>>::max();
	value.u64_2[1] = value.u64_2[0];

	return value;
	}



const sx_uuid_t sx_uuid_t::null;
const sx_uuid_t sx_uuid_t::maxvalue = create_max_uuid();
const sx_uuid_t sx_uuid_t::minvalue = sx_uuid_t::null;


sx_uuid_t::sx_uuid_t()noexcept{
	u64_2[0] = 0;
	u64_2[1] = 0;
	}

/*sx_uuid_t::sx_uuid_t( const uint8_t plainData[plane_size] ) {
	uuid_unpack( (*this), plainData );
	}*/

std::string sx_uuid_t::to_str()const {
	auto s = uuid_to_str( (*this) );

#ifdef USE_DCF_CHECK
	auto idFromS = from_string( s );
	if( idFromS != (*this) )
		THROW_EXC_FWD(nullptr);
#endif//USE_DCF_CHECK

	return s;
	}

std::wstring sx_uuid_t::to_wstr()const{
	return cvt<wchar_t>( uuid_to_str( (*this) ) );
	}

bool sx_uuid_t::is_null()const noexcept{
	return 0 == u64_2[0] && 0 == u64_2[1];
	}

const void* sx_uuid_t::pmobject()const noexcept{
	return (const void*)u8_16;
	}

void* sx_uuid_t::pmobject()noexcept{
	return (void*)u8_16;
	}

size_t sx_uuid_t::pmsize()const noexcept{
	return sizeof( u8_16 );
	}

const uint8_t* sx_uuid_t::line()const noexcept{
	return u8_16;
	}

void sx_uuid_t::assing_line(const uint8_t buf[plane_size]) {
	for (size_t i = 0; i < plane_size; ++i)
		u8_16[i] = buf[i];
	}

sx_uuid_t sx_uuid_t::rand()noexcept {

	static_assert(sizeof( std::chrono::high_resolution_clock::now().time_since_epoch().count() ) == sizeof( decltype(std::declval<sx_uuid_t>().u64_2[0]) ), __FILE__);
	static_assert(sizeof( decltype(std::rand()) ) == sizeof( decltype(std::declval<sx_uuid_t>().u32_4[0]) ), __FILE__);

	sx_uuid_t plane;

	GUID sysuuid;
	sysuuid.Data1 = 0;
	sysuuid.Data2 = 0;
	sysuuid.Data3 = 0;
	memset( sysuuid.Data4, 0, sizeof( sysuuid.Data4 ) );

	if( S_OK == ::CoCreateGuid( &sysuuid ) ) {

		plane.time_low = sysuuid.Data1;
		plane.time_mid = sysuuid.Data2;
		plane.time_hi_and_version = sysuuid.Data3;
		plane.clock_seq_low = sysuuid.Data4[0];
		plane.clock_seq_hi_and_reserved = sysuuid.Data4[1];
		for( size_t i = 0; i < 6; ++i ) {
			plane.node[i] = sysuuid.Data4[2 + i];
			}
		}
	else {

		std::srand( (uint32_t)::time( nullptr ) );
		for( int i = 0; i < 4; ++i )
			plane.u32_4[i] ^= ::rand();
		}

	/* Set the two most significant bits (bits 6 and 7) of the
      clock_seq_hi_and_reserved to zero and one, respectively, RFC 4122 */
	plane.clock_seq_hi_and_reserved &= ~(uint8_t)0x40;
	plane.clock_seq_hi_and_reserved |= (uint8_t)0x80;

	/* Set the four most significant bits (bits 12 through 15) of the
      time_hi_and_version field to the 4-bit version number from
      Section 4.1.3, RFC 4122: ���������� ������ 4.  */
	plane.time_hi_and_version &= ~(uint16_t)0xF000;
	plane.time_hi_and_version |= 0x4000;

	return plane;
	}

/*static*/ void uuid_unpack_raw( sx_uuid_t &plan, const void *in ){

	const uint8_t *ptr = (const uint8_t *)in;
	uint32_t tmpu4 = 0;
	uint16_t tmpu2 = 0;

	tmpu4 = *ptr++;
	tmpu4 = (tmpu4 << 8) | *ptr++;
	tmpu4 = (tmpu4 << 8) | *ptr++;
	tmpu4 = (tmpu4 << 8) | *ptr++;
	plan.time_low = tmpu4;

	tmpu2 = *ptr++;
	tmpu2 = (tmpu2 << 8) | *ptr++;
	plan.time_mid = tmpu2;

	tmpu2 = *ptr++;
	tmpu2 = (tmpu2 << 8) | *ptr++;
	plan.time_hi_and_version = tmpu2;

	plan.clock_seq_hi_and_reserved = *ptr++;
	plan.clock_seq_low = *ptr++;

	memcpy( plan.node, ptr, sizeof(plan.node) );
	}

static void uuid_pack_raw( const sx_uuid_t &plan, const void *out ) {

	uint8_t *ptr = (uint8_t *)out;
	uint32_t tmpu4 = 0;
	uint16_t tmpu2 = 0;

	tmpu4 = plan.time_low;
	(*ptr++) = (tmpu4 >> 24) & 0xFF;
	(*ptr++) = (tmpu4 >> 16) & 0xFF;
	(*ptr++) = (tmpu4 >> 8) & 0xFF;
	(*ptr++) = tmpu4 & 0xFF;

	tmpu2 = plan.time_mid;
	(*ptr++) = (tmpu2 >> 8) & 0xFF;
	(*ptr++) = tmpu2 & 0xFF;

	tmpu2 = plan.time_hi_and_version;
	(*ptr++) = (tmpu2 >> 8) & 0xFF;
	(*ptr++) = tmpu2 & 0xFF;

	(*ptr++) = plan.clock_seq_hi_and_reserved;
	(*ptr++) = plan.clock_seq_low;

	memcpy( ptr, plan.node, sizeof( plan.node ) );
	}

#ifdef CBL_USE_BIG_LITTLE_ENDIAN
static void uuid_unpack_littleendian( sx_uuid_t &plan, const void *in ) {

	const uint8_t *ptr = (const uint8_t*)in;
	uint32_t tmpu4 = 0;
	uint16_t tmpu2 = 0;

	ptr += 3;
	tmpu4 = *ptr--;
	tmpu4 = (tmpu4 << 8) | *ptr--;
	tmpu4 = (tmpu4 << 8) | *ptr--;
	tmpu4 = (tmpu4 << 8) | *ptr;
	plan.time_low = tmpu4;
	ptr += 4;

	tmpu2 = *(ptr + 1);
	tmpu2 = (tmpu2 << 8) | *ptr;
	plan.time_mid = tmpu2;
	ptr += 2;

	tmpu2 = *(ptr + 1);
	tmpu2 = (tmpu2 << 8) | *ptr;
	plan.time_hi_and_version = tmpu2;
	ptr += 2;

	plan.clock_seq_hi_and_reserved = *ptr++;
	plan.clock_seq_low = *ptr++;

	memcpy( plan.node, ptr, sizeof(plan.node) );
	}
#endif///CBL_USE_BIG_LITTLE_ENDIAN

#ifdef CBL_USE_BIG_LITTLE_ENDIAN
static void uuid_pack_littleendian( const sx_uuid_t &plan, void *out ) {

	uint8_t *ptr = (uint8_t*)out;
	uint32_t tmpu4 = 0;
	uint16_t tmpu2 = 0;

	tmpu4 = plan.time_low;
	(*ptr++) = tmpu4 & 0xFF;
	(*ptr++) = (tmpu4 >> 8) & 0xFF;
	(*ptr++) = (tmpu4 >> 16) & 0xFF;
	(*ptr++) = (tmpu4 >> 24) & 0xFF;

	tmpu2 = plan.time_mid;
	(*ptr++) = tmpu2 & 0xFF;
	(*ptr++) = (tmpu2 >> 8) & 0xFF;

	tmpu2 = plan.time_hi_and_version;
	(*ptr++) = tmpu2 & 0xFF;
	(*ptr++) = (tmpu2 >> 8) & 0xFF;

	(*ptr++) = plan.clock_seq_hi_and_reserved;
	(*ptr++) = plan.clock_seq_low;

	memcpy( ptr, plan.node, sizeof( plan.node ) );
	}
#endif//CBL_USE_BIG_LITTLE_ENDIAN

static void uuid_unpack( sx_uuid_t &plan, const void *in ) {

#ifdef CBL_USE_BIG_LITTLE_ENDIAN
	if( crm::sysenv::sys_is_little_endian() )
		uuid_unpack_littleendian( plan, in );
	else
		uuid_unpack_raw( plan, in );

#else//CBL_USE_BIG_LITTLE_ENDIAN

	uuid_unpack_raw( plan, in );
#endif//CBL_USE_BIG_LITTLE_ENDIAN
	}

static void uuid_pack( const sx_uuid_t &plan, void *out ) {

#ifdef CBL_USE_BIG_LITTLE_ENDIAN
	if( crm::sysenv::sys_is_little_endian() )
		uuid_pack_littleendian( plan, out );
	else
		uuid_pack_raw( plan, out );

#else//CBL_USE_BIG_LITTLE_ENDIAN

	uuid_pack_raw( plan, out );
#endif///CBL_USE_BIG_LITTLE_ENDIAN


#ifdef USE_DCF_CHECK
	sx_uuid_t tmp;
	uuid_unpack( tmp, out );

	if( tmp != plan )
		THROW_EXC_FWD(nullptr);
#endif//
	}

static std::string uuid_to_str( const sx_uuid_t &uuid ) {

	char buf[4 * sizeof(sx_uuid_t)];
	
	uint8_t plane[sx_uuid_t::plane_size];
	uuid.to_raw( plane );

	sprintf_s( buf, sizeof(buf),
		"%02x%02x%02x%02x-%02x%02x-%02x%02x-%02x%02x-%02x%02x%02x%02x%02x%02x\0",
		plane[0],
		plane[1],
		plane[2],
		plane[3],
		plane[4],
		plane[5],
		plane[6],
		plane[7],
		plane[8],
		plane[9],
		plane[10],
		plane[11],
		plane[12],
		plane[13],
		plane[14],
		plane[15] );

	std::string s( buf );

#ifdef USE_DCF_CHECK
	auto tmp = uuid_from_str( s );
	if( tmp != uuid )
		THROW_EXC_FWD(nullptr);

#endif//USE_DCF_CHECK

	return std::move( s );
	}

static sx_uuid_t uuid_from_str(std::string_view str) {

	if( str.size() != 36 )THROW_EXC_FWD(nullptr);
	sx_uuid_t u;

	uint8_t buffer[sx_uuid_t::plane_size];
	uint16_t prebuffer[sx_uuid_t::plane_size];

	//������ ������ UUID: F8E30BB6-724D-47F4-B40B-69A361623762
	sscanf_s( str.data(), "%02hx%02hx%02hx%02hx-%02hx%02hx-%02hx%02hx-%02hx%02hx-%02hx%02hx%02hx%02hx%02hx%02hx",
		&prebuffer[0],
		&prebuffer[1],
		&prebuffer[2],
		&prebuffer[3],
		&prebuffer[4],
		&prebuffer[5],
		&prebuffer[6],
		&prebuffer[7],
		&prebuffer[8],
		&prebuffer[9],
		&prebuffer[10],
		&prebuffer[11],
		&prebuffer[12],
		&prebuffer[13],
		&prebuffer[14],
		&prebuffer[15] );

	for( size_t i = 0; i < sx_uuid_t::plane_size; i++ )
		buffer[i] = (unsigned char)prebuffer[i];

	uuid_unpack( u, (const void*)buffer );
	return u;
	}

static sx_uuid_t uuid_from_wstr(std::wstring_view str) {

	if(str.size() != 36)THROW_EXC_FWD(nullptr);
	sx_uuid_t u;

	uint8_t buffer[sx_uuid_t::plane_size];
	uint16_t prebuffer[sx_uuid_t::plane_size];

	//������ ������ UUID: F8E30BB6-724D-47F4-B40B-69A361623762
	swscanf_s(str.data(), L"%02hx%02hx%02hx%02hx-%02hx%02hx-%02hx%02hx-%02hx%02hx-%02hx%02hx%02hx%02hx%02hx%02hx",
		&prebuffer[0],
		&prebuffer[1],
		&prebuffer[2],
		&prebuffer[3],
		&prebuffer[4],
		&prebuffer[5],
		&prebuffer[6],
		&prebuffer[7],
		&prebuffer[8],
		&prebuffer[9],
		&prebuffer[10],
		&prebuffer[11],
		&prebuffer[12],
		&prebuffer[13],
		&prebuffer[14],
		&prebuffer[15]);

	for(size_t i = 0; i < sx_uuid_t::plane_size; i++)
		buffer[i] = (unsigned char)prebuffer[i];

	uuid_unpack(u, (const void*)buffer);
	return u;
	}

sx_uuid_t sx_uuid_t::from_string(std::string_view s) {
	return uuid_from_str(s);
	}

sx_uuid_t sx_uuid_t::from_string(std::wstring_view s) {
	return uuid_from_wstr(s);
	}

sx_uuid_t sx_uuid_t::as_uuid(std::string_view str) {
	return uuid_from_str(str);
	}

sx_uuid_t sx_uuid_t::as_uuid(std::wstring_view str) {
	return uuid_from_wstr(str);
	}

void sx_uuid_t::to_raw( uint8_t buf[plane_size] )const {
	uuid_pack( (*this), buf );
	}

bool crm::operator == (const sx_uuid_t & lval, const sx_uuid_t &rval)noexcept{
	return	lval.u64_2[0] == rval.u64_2[0] && lval.u64_2[1] == rval.u64_2[1];
	}

bool crm::operator != (const sx_uuid_t & lval, const sx_uuid_t &rval)noexcept{
	return !(lval == rval);
	}

/*! ��������� ����������� � �������, ���,
������� 64 ������ ������ �������� ����� � ������� u64_2 �� ������� 0. */
bool crm::operator < (const sx_uuid_t & lval, const sx_uuid_t &rval)noexcept{
	if( lval.u64_2[0] < rval.u64_2[0] )
		return true;
	else if( lval.u64_2[0] > rval.u64_2[0] )
		return false;
	else	
		return lval.u64_2[1] < rval.u64_2[1];
	}

/*! ��������� ����������� � �������, ���,
������� 64 ������ ������ �������� ����� � ������� u64_2 �� ������� 0. */
bool crm::operator > (const sx_uuid_t & lval, const sx_uuid_t &rval) noexcept{
	if( lval.u64_2[0] > rval.u64_2[0] )
		return true;
	else if( lval.u64_2[0] < rval.u64_2[0] )
		return false;
	else
		return lval.u64_2[1] > rval.u64_2[1];
	}

/*! ��������� ����������� � �������, ���,
������� 64 ������ ������ �������� ����� � ������� u64_2 �� ������� 0. */
bool crm::operator >= (const sx_uuid_t & lval, const sx_uuid_t &rval) noexcept{
	if( lval.u64_2[0] > rval.u64_2[0] )
		return true;
	else  if( lval.u64_2[0] < rval.u64_2[0] )
		return false;
	else
		return lval.u64_2[1] >= rval.u64_2[1];
	}

/*! ��������� ����������� � �������, ���,
������� 64 ������ ������ �������� ����� � ������� u64_2 �� ������� 0. */
bool crm::operator <= (const sx_uuid_t & lval, const sx_uuid_t &rval)noexcept{
	if( lval.u64_2[0] < rval.u64_2[0] )
		return true;
	else if( lval.u64_2[0] > rval.u64_2[0] )
		return false;
	else
		return lval.u64_2[1] <= rval.u64_2[1];
	}

std::ostream  & crm::operator <<(std::ostream & os, const sx_uuid_t &uuid) {
	os << uuid.to_str();
	return os;
	}

std::wostream  & crm::operator <<(std::wostream & os, const sx_uuid_t &uuid) {
	std::string uuid_str = uuid.to_str();
	std::wstring uuid_wstr( uuid_str.begin(), uuid_str.end() );

	os << uuid_wstr.c_str();
	return os;
	}



template<typename T64Op>
static sx_uuid_t all_value_op( const sx_uuid_t & lval, const sx_uuid_t &rval, typename T64Op op )noexcept{
	sx_uuid_t result;
	result.u64_2[0] = op( lval.u64_2[0], rval.u64_2[0] );
	result.u64_2[1] = op( lval.u64_2[1], rval.u64_2[1] );
	return std::move( result );
	}

sx_uuid_t crm::operator ^ (const sx_uuid_t & lval, const sx_uuid_t &rval)noexcept{
	return all_value_op( lval, rval, []( uint64_t lval, uint64_t rval ) {return lval ^ rval; } );
	}
sx_uuid_t crm::operator & (const sx_uuid_t & lval, const sx_uuid_t &rval) noexcept{
	return all_value_op( lval, rval, []( uint64_t lval, uint64_t rval ) {return lval & rval; } );
	}
sx_uuid_t crm::operator | (const sx_uuid_t & lval, const sx_uuid_t &rval) noexcept{
	return all_value_op( lval, rval, []( uint64_t lval, uint64_t rval ) {return lval | rval; } );
	}

