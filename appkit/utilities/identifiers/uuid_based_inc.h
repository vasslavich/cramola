#pragma once


#include "../../internal_invariants.h"
#include "./uuid.h"


namespace crm {

struct uuid_based_inc {
	using value_type = crm::sx_uuid_t;

	static value_type next()noexcept;
	};
}
