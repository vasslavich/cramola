#include "../../internal.h"


using namespace crm;
using namespace crm::detail;

static auto lxx = [] { return 0; };
static_assert(std::is_invocable_v<decltype(make_postscope_action(std::move(lxx)))>, __FILE_LINE__);
static_assert(std::is_invocable_v<decltype(make_postscope_action(std::declval<std::function<int()>>()))>, __FILE_LINE__);


