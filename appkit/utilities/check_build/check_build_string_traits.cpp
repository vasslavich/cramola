#include <string>
#include "../../internal.h"
#include "../stringscoped/string_trait_using.h"

using namespace crm;

static_assert(std::is_same_v<std::string, decltype(crm::cvt(std::declval<std::string_view>()))>, __FILE_LINE__);
static_assert(std::is_same_v<std::string, decltype(crm::cvt(std::declval<std::wstring_view>()))>, __FILE_LINE__);
static_assert(std::is_same_v<std::wstring, decltype(crm::cvt<wchar_t>(std::declval<std::string_view>()))>, __FILE_LINE__);
static_assert(std::is_same_v<std::wstring, decltype(crm::cvt<wchar_t>(std::declval<std::wstring_view>()))>, __FILE_LINE__);

static_assert(std::is_same_v < std::string, decltype(crm::format_message(std::declval<std::string_view>()))>, __FILE_LINE__);
static_assert(std::is_same_v < std::string, decltype(crm::format_message(std::declval<std::string>()))>, __FILE_LINE__);
static_assert(std::is_same_v < std::string, decltype(crm::format_message(std::declval<const char*>()))>, __FILE_LINE__);
static_assert(std::is_same_v < std::string, decltype(crm::format_message(std::declval<std::nullptr_t>()))>, __FILE_LINE__);

static_assert(std::is_same_v < std::string, decltype(crm::format_message(std::declval<std::wstring_view>()))>, __FILE_LINE__);
static_assert(std::is_same_v < std::string, decltype(crm::format_message(std::declval<std::wstring>()))>, __FILE_LINE__);
static_assert(std::is_same_v < std::string, decltype(crm::format_message(std::declval<const wchar_t*>()))>, __FILE_LINE__);


