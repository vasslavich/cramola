#pragma once


#include <string>
#include <type_traits>
#include "../../build_config.h"
#include "../../exceptions.h"

namespace crm::detail {


template<typename TString, typename TValue, typename = std::void_t<>>
struct make_string_t_ : std::false_type {};


template<typename TString, typename TValue>
struct make_string_t_ < TString, TValue,
	std::void_t <
	std::enable_if_t<(
		std::is_constructible_v<std::decay_t<TString>, TValue> && 
		!std::is_same_v<std::decay_t<TString>, std::decay_t<TValue>>)>
	>> : std::true_type{

	using string_type = std::decay_t<TString>;

	template<typename XValue>
	static string_type get( XValue && v )
		noexcept(std::is_nothrow_constructible_v<string_type, XValue> && 
			std::is_nothrow_move_constructible_v<string_type> && 
			std::is_nothrow_move_assignable_v<string_type>){
		static_assert(std::is_same_v<std::decay_t<TValue>, std::decay_t<XValue>>, __FILE_LINE__);

		return { std::forward<XValue>( v ) };
		}
	};

template<typename TString, typename TValue>
struct make_string_t_ < TString, TValue,
	std::void_t <
		std::enable_if_t<std::is_same_v<std::decay_t<TString>, std::decay_t<TValue>>>
	>> : std::true_type{

	template<typename XValue>
	static decltype(auto) get( XValue && v )noexcept{
		static_assert(std::is_same_v<std::decay_t<TValue>, std::decay_t<XValue>>, __FILE_LINE__);
		static_assert(std::is_reference_v<decltype(std::forward<XValue>(v))>, __FILE_LINE__);

		return std::forward<XValue>( v );
		}

	static_assert(!std::is_reference_v< decltype(get(std::declval<std::decay_t<TString>>()))> ||
		std::is_rvalue_reference_v < decltype(get( std::declval<std::decay_t<TString>>() ))>, __FILE_LINE__);
	static_assert(std::is_reference_v <decltype(get( std::declval<std::decay_t<TString>&>() ))>, __FILE_LINE__);
	static_assert(std::is_reference_v < decltype(get( std::declval<std::decay_t<TString>&&>() ))>, __FILE_LINE__);
	static_assert(std::is_reference_v < decltype(get( std::declval<std::decay_t<const TString>&>() ))>, __FILE_LINE__);
	};

template<typename TString, typename TValue>
struct make_string_t_ < TString, TValue,
	std::void_t<
		std::enable_if_t<std::is_same_v<std::decay_t<TString>, std::string>>, 
		decltype(std::to_string( std::declval<TValue>() ))>>
	: std::true_type {

	template<typename XValue>
	static decltype(auto) get( XValue && v )
		noexcept(std::is_nothrow_invocable_v <decltype(std::to_string( std::declval<XValue>() )), XValue > &&
			std::is_nothrow_move_constructible_v<TString> &&
			std::is_nothrow_move_assignable_v<TString> ){
		static_assert(std::is_same_v<std::decay_t<TValue>, std::decay_t<XValue>>, __FILE_LINE__);

		return std::to_string( std::forward<TValue>( v ) );
		}
	};


template<typename TString, typename TValue>
struct make_string_t_ < TString, TValue,
	std::void_t<
		std::enable_if_t<std::is_same_v<std::decay_t<TString>, std::wstring>>,
		decltype(std::to_wstring( std::declval<TValue>() ))>>
	: std::true_type {

	template<typename XValue>
	static decltype(auto) get( XValue && v )
		noexcept(std::is_nothrow_invocable_v <decltype(std::to_wstring( std::declval<XValue>() )), XValue > &&
			std::is_nothrow_move_constructible_v<TString> &&
			std::is_nothrow_move_assignable_v<TString>){
		static_assert(std::is_same_v<std::decay_t<TValue>, std::decay_t<XValue>>, __FILE_LINE__);

		return std::to_wstring( std::forward<TValue>( v ) );
		}
	};



template<typename TString>
struct make_string{
	using string_type = std::decay_t<TString>;

	template<typename TValue>
	static decltype(auto) make( TValue && v )
		/*noexcept( std::is_nothrow_invocable_r_v<string_type, make_string_t_<string_type, TValue>::get<TValue>, std::declval<TValue>()> )*/{

		return make_string_t_<string_type, TValue>::get( std::forward<TValue>( v ) );
		}
	};



template<typename TString, typename TValue, typename = std::void_t<>>
struct is_make_string_invokable :std::false_type{};

template<typename TString, typename TValue>
struct is_make_string_invokable<TString,
	TValue,
	std::void_t<decltype(make_string<TString>::make( std::declval<TValue>() ))>> :std::true_type{};

template<typename TString, typename TValue>
constexpr bool is_make_string_invokable_v = is_make_string_invokable<TString, TValue>::value;


template<typename TString, typename TValue>
constexpr bool is_to_string_invokable_v = is_make_string_invokable_v<TString, TValue>;





template<typename TString>
class stringcat_t{
public:
	using string_type = std::decay_t<TString>;

public:
	template<typename ... TSubstrings>
	static string_type stringcat( TSubstrings && ... vl )noexcept
		/*noexcept(std::conjunction_v<std::is_nothrow_invocable_v< decltype(&make_string_t_<string_type, TSubstrings>::get<TSubstrings>), std::declval<TSubstrings>()>...>)*/
		/*noexcept(make_string<string_type>::make( std::declval<TSubstrings>() ) + ...)*/
		/*noexcept()*/{

		static_assert(std::is_nothrow_move_assignable_v<string_type> && std::is_nothrow_move_constructible_v<string_type> , __FILE_LINE__);

		//if constexpr (std::conjunction_v<std::is_nothrow_invocable_v< decltype(&make_string_t_<string_type, TSubstrings>::get<TSubstrings>), std::declval<TSubstrings>()>...>) {
		//	}
		//else {
			CHECK_NO_EXCEPTION_BEGIN

				return (make_string_t_<string_type, TSubstrings>::get(std::forward<TSubstrings>(vl)) + ...);
			//return (make_string<string_type>::make( std::forward<TSubstrings>( vl ) ) + ...);

			CHECK_NO_EXCEPTION_END
			//}
		}
	};



template<typename ... TSubstrings>
decltype(auto) wstringcat(TSubstrings && ... subargs)noexcept {
	return stringcat_t<std::wstring>::stringcat( std::forward<TSubstrings>(subargs)...);
	}

template<typename ... TSubstrings>
decltype(auto) stringcat(TSubstrings && ... subargs)noexcept {
	return stringcat_t<std::string>::stringcat(std::forward<TSubstrings>(subargs)...);
	}
}

