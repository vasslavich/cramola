#pragma once

#include "./strcat.h"
#include "./string_trait_decls.h"
#include "./string_trait_classes.h"
#include "./string_trait_using.h"
