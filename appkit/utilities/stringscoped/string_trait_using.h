#pragma once

#include <locale>
#include "./string_trait_decls.h"

namespace crm {

template<typename S>
constexpr bool is_string_class_v = is_string_class<std::decay_t<S>>::value;


template<typename T>
constexpr bool is_string_constructible_v = std::is_constructible_v<std::string, T> || std::is_constructible_v<std::wstring, T>;


//std::string cvt2string(std::wstring_view);
//std::string cvt2string(std::string_view);
//
//std::wstring cvt2wstring(std::wstring_view);
//std::wstring cvt2wstring(std::string_view);


template<typename TCharResult = char,
	typename TCharSource,
	typename = std::enable_if_t<(
		std::is_same_v<TCharResult, char> ||
		std::is_same_v<TCharResult, wchar_t>
		)>>
	std::basic_string<TCharResult> cvt(std::basic_string_view<TCharSource> src,
		cvt_err_t* errc = nullptr,
		std::locale loc = std::locale()) {

	if constexpr (std::is_same_v<TCharSource, TCharResult>) {
		return src;
		}
	else {
		if constexpr (std::is_same_v<TCharResult, char>) {
			std::string buf1(src.size(), '\0');

			auto i = src.cbegin(), ie = src.cend();
			auto j = buf1.begin();

			for (; i != ie; ++i, ++j)
				*j = std::use_facet< std::ctype< char > >(loc).narrow(*i);

			if (nullptr != errc)
				(*errc) = cvt_err_t::success;

			return buf1;
			}
		else if constexpr (std::is_same_v<TCharResult, wchar_t>) {
			std::wstring buf2(src.size(), '\0');

			auto i = src.cbegin(), ie = src.cend();
			auto j = buf2.begin();

			for (; i != ie; ++i, ++j)
				*j = std::use_facet< std::ctype< wchar_t > >(loc).widen(*i);

			if (nullptr != errc)
				(*errc) = cvt_err_t::success;

			return buf2;
			}
		else {
			static_assert(fault_instantiaion_detect<TCharResult>, __FILE_LINE__);
			}
		}
	}


template<typename TCharResult = char,
	typename TString,
	typename = std::enable_if_t<(
		std::is_same_v<TCharResult, char> ||
		std::is_same_v<TCharResult, wchar_t>
		) && (
			is_string_class_v<TString>
			)>>
	std::basic_string<TCharResult> cvt(TString && src,
		cvt_err_t* errc = nullptr,
		std::locale loc = std::locale()) {

	return cvt<TCharResult, typename std::decay_t<TString>::value_type>(src.c_str(), errc, loc);
	}

template<typename TCharTarget = char, 
	typename TString,
	typename = std::enable_if_t<(
		(is_string_class_v<TString> ||
			is_string_constructible_v<TString>) &&
		(std::is_same_v<TCharTarget, char> || std::is_same_v<TCharTarget, wchar_t>)
		)>>
std::basic_string<TCharTarget> format_message(TString&& s) {
	if constexpr (is_string_class_v<TString>) {
		using source_value_type = typename std::decay_t<TString>::value_type;

		if constexpr (std::is_same_v<TCharTarget, source_value_type>) {
			return std::forward<TString>(s);
			}
		else {
			return cvt<TCharTarget, source_value_type>(std::forward<TString>(s));
			}
		}
	else if constexpr (std::is_pointer_v<std::decay_t<TString>>) {
		using source_value_type = typename std::decay_t<decltype(*std::declval<TString>())>;

		if constexpr (std::is_same_v<TCharTarget, source_value_type>) {
			return std::basic_string<TCharTarget>(std::forward<TString>(s));
			}
		else {
			return cvt<TCharTarget>(std::basic_string_view< source_value_type>(std::forward<TString>(s)));
			}
		}
	else if constexpr (std::is_null_pointer_v<std::decay_t<TString>>) {
		return std::basic_string<TCharTarget> {};
		}
	else {
		static_assert(fault_instantiaion_detect<TString>::value, __FILE_LINE__);
		}
	}
}
