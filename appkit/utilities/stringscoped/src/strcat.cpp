#include "../../../internal.h"
#include "../strcat.h"


using namespace crm;
using namespace crm::detail;


auto s0 = make_string<std::string>::make( (int)0 );

std::string tostr(char)noexcept;

//auto s10 = TValue2StringRef<std::string&, int>{ 1 } + TValue2StringRef<std::string&, double>{ 1.0 };


static_assert(std::is_nothrow_invocable_r_v < std::string, decltype(tostr), char>, __FILE_LINE__);
static_assert(!std::is_nothrow_invocable_v <decltype(std::to_string(std::declval<int>())), int>, __FILE_LINE__);
static_assert(is_to_string_invokable_v<std::string, std::string>, __FILE_LINE__);
static_assert(is_to_string_invokable_v<std::string, int>, __FILE_LINE__);


const char* pc = 0;
std::string s;
char cbuf[10];

static_assert(std::is_constructible_v < std::string, decltype(pc)>, __FILE_LINE__);
static_assert(std::is_constructible_v < std::string, decltype(s)>, __FILE_LINE__);
static_assert(std::is_constructible_v < std::string, decltype(cbuf)>, __FILE_LINE__);



static_assert(is_to_string_invokable_v<std::string, int>, __FILE_LINE__);
static_assert(is_to_string_invokable_v<std::string, double>, __FILE_LINE__);
static_assert(is_to_string_invokable_v<std::string, decltype(pc)>, __FILE_LINE__);
static_assert(is_to_string_invokable_v<std::string, decltype(s)>, __FILE_LINE__);
static_assert(is_to_string_invokable_v<std::string, decltype(cbuf)>, __FILE_LINE__);

static_assert(is_to_string_invokable_v<std::string, decltype("0")>, __FILE_LINE__);
static_assert(is_to_string_invokable_v<std::string, decltype(pc)>, __FILE_LINE__);
static_assert(is_to_string_invokable_v<std::string, int>, __FILE_LINE__);
static_assert(is_to_string_invokable_v<std::string, long>, __FILE_LINE__);

//static_assert(noexcept(stringcat("1,", std::string("2,"), "3,", 4)), __FILE_LINE__);
//static_assert(noexcept(wstringcat(L"1,", std::wstring(L"2,"), L"3,", 4)), __FILE_LINE__);

auto s1 = stringcat("1,", std::string("2,"), "3,", 4);
auto ws = wstringcat(L"1,", std::wstring(L"2,"), L"3,", 4);
