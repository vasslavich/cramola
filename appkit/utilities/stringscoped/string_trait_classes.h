#pragma once

#include "./string_trait_decls.h"

namespace crm {

template<typename S>
struct is_string_class<S, std::void_t<
	typename S::value_type,
	decltype(std::declval<S>().c_str())
	>> : std::true_type {};
}
