#pragma once


#include <type_traits>


namespace crm {

template<typename S, typename = std::void_t<>>
struct is_string_class : std::false_type {};

}
