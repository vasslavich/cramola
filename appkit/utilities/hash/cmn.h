#pragma once


#include <array>
#include "../../internal_invariants.h"
#include "./md5.h"


namespace crm::hash{


template<typename TContainer>
std::array<std::uint8_t, 16> hash_128(const TContainer & data) {
	return md5(data);
	}
}

