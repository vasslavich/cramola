#pragma once


#include "../../internal_invariants.h"
#include "../identifiers/sx_glob_64.h"
#include "../identifiers/uuid.h"


namespace crm {


#ifdef CBL_MESSAGE_ADDRESS_TYPES_SHORT

/*! Identifier of a address hash */
using address_hash_t = sx_glob_64;

#else

/*! Identifier of a address hash */
using address_hash_t = sx_uuid_t;

#endif


size_t constexpr address_hash_size = address_hash_t::plane_size;
}



