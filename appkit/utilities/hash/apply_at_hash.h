#pragma once


#include "../../typeframe/base_typetrait_decls.h"
#include "../../typeframe/base_typetrait_using.h"
#include "./object_hash.h"


namespace crm {

template<typename T,
	typename std::enable_if_t<(
		detail::has_hashable_support_v<T> &&
		!std::is_same_v<std::decay_t<T>, sx_glob_64> &&
		!std::is_same_v<std::decay_t<T>, sx_uuid_t>
		), int> = 0>
	constexpr std_hash_type as_std_hash(T&& v)noexcept {
	return as_std_hash(std::forward<T>(v).object_hash());
	}


address_hash_t object_hash(const sx_glob_64& o)noexcept;
address_hash_t object_hash(const sx_uuid_t& o)noexcept;
address_hash_t object_hash(const sx_loc_64& o)noexcept;
address_hash_t object_hash(const sx_locid_t& o)noexcept;


void apply_at_hash(address_hash_t& to, const sx_glob_64& value)noexcept;
void apply_at_hash(address_hash_t& to, const sx_loc_64& value)noexcept;
void apply_at_hash(address_hash_t& to, const sx_uuid_t& value)noexcept;
void apply_at_hash(address_hash_t& to, const sx_locid_t& value)noexcept;


template<typename THash>
void apply_at_hash(THash& to, std::string_view s)noexcept {
	apply_at_hash<THash, std::string_view>(to, s);
	}

template<typename THash>
void apply_at_hash(THash& to, std::wstring_view s)noexcept {
	apply_at_hash<THash, std::wstring_view>(to, s);
	}

void apply_at_hash(address_hash_t& t0, const std::chrono::system_clock::time_point& tp)noexcept;


namespace detail {
template<typename T>
struct uuid_integral_trait_dependent_false : std::false_type {};
}


template<typename THash,
	typename _TIntegral,
	typename TIntegral = typename std::decay_t<_TIntegral>,
	typename std::enable_if_t<std::is_integral_v<TIntegral> || std::is_enum_v<TIntegral>, int> = 0>
	void apply_at_hash(THash& to, _TIntegral value)noexcept {
	static_assert(std::is_lvalue_reference_v<decltype(to.at<TIntegral>(0))>, __FILE__);

	auto idx = static_cast<size_t>(value) % THash::count_of_as<TIntegral>();
	to.at<TIntegral>(idx) ^= static_cast<std::decay_t<decltype(to.at<TIntegral>(idx))>>(value);
	}


template<typename THash,
	typename TContainerIntegral,
	typename std::enable_if_t<(
		srlz::detail::is_container_of_integral_v<TContainerIntegral>
		), int> = 0>
	void apply_at_hash(THash& to, const TContainerIntegral& value)noexcept {
	using value_type = typename srlz::detail::contaiter_value_type_t<TContainerIntegral>;
	static_assert(std::is_lvalue_reference_v<decltype(to.at<value_type>(0))>, __FILE__);

	static constexpr size_t blockSize = THash::count_of_as<value_type>();

	const size_t blks = value.size() / blockSize;
	const size_t taill = value.size() % blockSize;

	for(size_t iblk = 0; iblk < blks; ++iblk) {
		for(size_t i = 0; i < blockSize; ++i) {
			to.at<value_type>(i) ^= value.at(iblk * blockSize + i);
			}
		}

	for(size_t i = 0; i < taill; ++i) {
		to.at<value_type>(i) ^= value.at(blks * blockSize + i);
		}
	}


template<typename THash,
	typename TContainerHashableValues,
	typename std::enable_if_t<(
		srlz::detail::is_container_of_containers_v<TContainerHashableValues>
		), int> = 0>
	void apply_at_hash(THash& to, const TContainerHashableValues& value)noexcept {
	auto itEnd = value.cend();
	for(auto it = value.cbegin(); it != itEnd; ++it) {
		apply_at_hash(to, (*it));
		}
	}


template<typename T>
struct bad_apply_to_hash_noexcept : std::false_type {};


template<typename THash,
	typename T,
	typename std::enable_if_t<detail::has_hashable_support_v<T, THash> && !std::is_same_v<std::decay_t<T>, std::decay_t<THash>>, int> = 0>
	void apply_at_hash(THash& to, T&& hashableObject)noexcept {
	if constexpr(noexcept(std::declval<T>().object_hash())) {
		apply_at_hash(to, object_hash(std::forward<T>(hashableObject)));
		}
	else {
		static_assert(bad_apply_to_hash_noexcept<T>::value, "noexcept specification");
		}
	}


template<typename T,
	typename std::enable_if_t<detail::has_hashable_support_v<T>, int> = 0>
	bool operator <(const T& l, const T& r)noexcept {
	return object_hash(l) < object_hash(r);
	}

template<typename T,
	typename std::enable_if_t<detail::has_hashable_support_v<T>, int> = 0>
	bool operator >(const T& l, const T& r)noexcept {
	return object_hash(l) > object_hash(r);
	}

template<typename T,
	typename std::enable_if_t<detail::has_hashable_support_v<T>, int> = 0>
	bool operator >=(const T& l, const T& r)noexcept {
	return object_hash(l) >= object_hash(r);
	}

template<typename T,
	typename std::enable_if_t<detail::has_hashable_support_v<T>, int> = 0>
	bool operator ==(const T& l, const T& r)noexcept {
	return object_hash(l) == object_hash(r);
	}
}

