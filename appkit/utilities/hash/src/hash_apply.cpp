#include "../../../internal.h"

using namespace crm;
using namespace crm::detail;


#ifdef CBL_MESSAGE_ADDRESS_TYPES_SHORT
sx_glob_64 crm::object_hash(const sx_glob_64& o)noexcept {
	return o;
	}

sx_glob_64 crm::object_hash(const sx_uuid_t& o)noexcept {
	return sx_glob_64( o.at<size_t>(0) ^ o.at<size_t>(1) );
	}

sx_glob_64 crm::object_hash(const sx_loc_64& o)noexcept {
	return sx_glob_64( o.xoffer() );
	}

sx_glob_64 crm::object_hash(const sx_locid_t& o)noexcept {
	return sx_glob_64( o.xoffer() ^ o.base() );
	}

#else
sx_uuid_t crm::object_hash(const sx_glob_64& o)noexcept {
	return { o.at<size_t>(), o.at<size_t>() };
	}

sx_uuid_t crm::object_hash(const sx_uuid_t& o)noexcept {
	return o;
	}
sx_uuid_t crm::object_hash(const sx_loc_64& o)noexcept {
	return { o.key(), o.value() };
	}
sx_uuid_t crm::object_hash(const sx_locid_t& o)noexcept {
	return { o.key(), o.key() };
	}
#endif

#ifdef CBL_MESSAGE_ADDRESS_TYPES_SHORT
void crm::apply_at_hash(sx_glob_64& to, const sx_glob_64& value)noexcept {
	to.at<size_t>() ^= value.at<size_t>();
	}
void crm::apply_at_hash(sx_glob_64& to, const sx_loc_64& value)noexcept {
	to.at<size_t>() ^= value.xoffer();
	}
void crm::apply_at_hash(sx_glob_64& to, const sx_uuid_t& value)noexcept {
	to.at<size_t>() ^= value.at<size_t>(0);
	to.at<size_t>() ^= value.at<size_t>(1);
	}
void crm::apply_at_hash(sx_glob_64& to, const sx_locid_t& value)noexcept {
	to.at<size_t>() ^= value.base();
	to.at<size_t>() ^= value.xoffer();
	}
#else
void crm::apply_at_hash(sx_uuid_t& to, const sx_glob_64& value)noexcept {
	to.at<size_t>() ^= value.at<size_t>();
	}
void crm::apply_at_hash(sx_uuid_t& to, const sx_loc_64& value)noexcept {
	to.at<size_t>() ^= value.xoffer();
	}
void crm::apply_at_hash(sx_uuid_t& to, const sx_uuid_t& value)noexcept {
	to.at<size_t>(0) ^= value.at<size_t>(0);
	to.at<size_t>(1) ^= value.at<size_t>(1);
	}
void crm::apply_at_hash(sx_uuid_t& to, const sx_locid_t& value)noexcept {
	to.at<size_t>(0) ^= value.base();
	to.at<size_t>(1) ^= value.xoffer();
	}
#endif

void crm::apply_at_hash(address_hash_t& t0, const std::chrono::system_clock::time_point& tp)noexcept {
	apply_at_hash(t0, tp.time_since_epoch().count());
	}




