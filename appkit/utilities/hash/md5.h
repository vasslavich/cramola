﻿#pragma once


#include <vector>
#include <string>
#include <array>
#include "../../internal_invariants.h"


namespace crm{
namespace hash{


std::array<uint8_t,16> md5( const std::vector<uint8_t> & data );
std::array<uint8_t, 16> md5( const std::string & s );
}
}

