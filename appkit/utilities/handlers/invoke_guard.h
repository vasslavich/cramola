#pragma once


#include <type_traits>
#include "../../base_predecl.h"
#include "../../internal_invariants.h"
#include "../../exceptions.h"
#include "../../functorapp/functorapp.h"
#include "./base_terms.h"


namespace crm::detail {


template<typename T, typename TExpression, typename Enable = void>
struct is_invoke_with_expression_t : public std::false_type {};


template<typename _Tx, typename _TxExpression,
	typename T = std::decay_t<_Tx>,
	typename TExpression = typename std::decay_t<_TxExpression>>
	struct is_invoke_with_expression_cond_t {
	typedef typename std::decay_t<decltype(std::declval<T>()(std::declval<TExpression>()(std::string{}))) > invoke_result_t;
	static constexpr bool is_noexcept_expression = noexcept(TExpression{}) && noexcept(std::declval<TExpression>()(std::string{}));
	};

template<typename T, typename TExpression>
struct is_invoke_with_expression_t<T, TExpression,
	std::void_t< typename is_invoke_with_expression_cond_t<T, TExpression>::invoke_result_t,
	typename std::enable_if_t<is_invoke_with_expression_cond_t<T, TExpression>::is_noexcept_expression>
	>> : public std::true_type{};

template<typename T, typename TExpression>
constexpr bool is_invoke_with_expression_v = is_invoke_with_expression_t<T, TExpression>::value;


template<typename _THandler, typename _TFaultResult,
	typename THandler = typename std::decay_t<_THandler>,
	typename TFatulResult = typename std::decay_t<_TFaultResult>,
	typename std::enable_if_t<is_invoke_with_expression_v<THandler, TFatulResult>, int> = 0>
	class functor_invoke_guard_t {
	public:
		using handler_t = typename THandler;
		using fault_maker_t = typename TFatulResult;
		using self_t = functor_invoke_guard_t<_THandler, _TFaultResult>;

		static constexpr std::uintmax_t invoke_indefinitely = (std::numeric_limits<std::uintmax_t>::max)();
		static constexpr std::uintmax_t invoke_once = 1;

	private:
		handler_t _hf;

#ifdef CBL_TRACELEVEL_TRACE_TAG_ON
		trace_tag _tag;
#endif

		std::weak_ptr<distributed_ctx_t> _ctx;
		std::atomic<std::uintmax_t> _invokedCnt{ 0 };
		async_space_t _scx{ async_space_t::undef };
		std::uintmax_t _invokedMaxCount{ 0 };

#ifdef CBL_TRACELEVEL_TRACE_TAG_ON
#define functor_invoke_guard_t_TRACE_LINE		(!_tag.name.empty() ? _tag.name : __FILE_LINE__)
#else
#define functor_invoke_guard_t_TRACE_LINE		__FILE_LINE__
#endif

#ifdef CBL_OBJECTS_COUNTER_CHECKER
		utility::__xobject_counter_logger_t<self_t, 10> _xDbgCounter;
#endif

		bool is_invoke_ready()const noexcept {
			if constexpr (is_functional_check_empty_v<handler_t>) {
				return _hf && _invokedCnt < _invokedMaxCount;
				}
			else {
				return  _invokedCnt < _invokedMaxCount;
				}
			}

		template<typename ...Values>
		struct args_count_of_v {
			static constexpr size_t value = sizeof ... (Values);
			};

		bool do_invoke()noexcept {
			return _invokedCnt.fetch_add(1) < _invokedMaxCount;
			}

		void __invoke_async_r(handler_t&& h, std::shared_ptr<distributed_ctx_t>& c)noexcept {

			if constexpr (is_functional_check_empty_v<handler_t>) {
				CBL_VERIFY(h);
				}

			fault_maker_t fm;
			CHECK_NO_EXCEPTION(c->launch_async(_scx, functor_invoke_guard_t_TRACE_LINE, std::move(h), fm(functor_invoke_guard_t_TRACE_LINE)));
			}

		template<typename ...TResultValues>
		void __invoke_async_r(handler_t&& h, std::shared_ptr<distributed_ctx_t>& c, TResultValues&& ... result)noexcept {
			if constexpr (is_functional_check_empty_v<handler_t>) {
				CBL_VERIFY(h);
				}

			CHECK_NO_EXCEPTION(c->launch_async(_scx, functor_invoke_guard_t_TRACE_LINE, std::move(h), std::forward<TResultValues>(result)...));
			}

		template<typename ...TResultValues>
		void __invoke_async(TResultValues&& ... result)noexcept {
			CHECK_NO_EXCEPTION_BEGIN

				if (do_invoke()) {
					if (_hf) {
						if (auto c = _ctx.lock()) {
							try {
								if constexpr (std::is_copy_assignable_v<decltype(_hf)>) {
									auto h = (is_once()) ? std::move(_hf) : _hf;
									constexpr auto pps = sizeof...(result);
									if (pps > 0) {
										__invoke_async_r(std::move(h), c, std::forward<TResultValues>(result)...);
										}
									else {
										__invoke_async_r(std::move(h), c);
										}
									}
								else {
									static_assert(std::is_move_assignable_v<decltype(_hf)>, __FILE__);

									CBL_VERIFY(is_once());

									constexpr auto pps = sizeof...(result);
									if (pps > 0) {
										__invoke_async_r(std::move(_hf), c, std::forward<TResultValues>(result)...);
										}
									else {
										__invoke_async_r(std::move(_hf), c);
										}
									}
								}
							catch (const dcf_exception_t & exc0) {
								CHECK_NO_EXCEPTION(c->exc_hndl(exc0));
								}
							catch (const std::exception & exc1) {
								CHECK_NO_EXCEPTION(c->exc_hndl(CREATE_MPF_EXC_FWD(exc1)));
								}
							catch (...) {
								CHECK_NO_EXCEPTION(c->exc_hndl(CREATE_MPF_EXC_FWD(nullptr)));
								}
							}
						}
					}

			CHECK_NO_EXCEPTION_END
			}

		template<typename ...TResultValues>
		auto __invoke_explicit(TResultValues&& ... result) {
			if (do_invoke()) {
				if (_hf) {
					if (is_once()) {
						decltype(_hf) h;
						std::swap(h, _hf);
						CBL_VERIFY(empty());

						CBL_VERIFY(h);
						return h(std::forward<TResultValues>(result)...);
						}
					else {
						CBL_VERIFY(!empty());
						return _hf(std::forward<TResultValues>(result)...);
						}
					}
				else {
					THROW_MPF_EXC_FWD(nullptr);
					}
				}
			else {
				THROW_MPF_EXC_FWD(nullptr);
				}
			}

	public:
		functor_invoke_guard_t()noexcept
#if defined(CBL_OBJECTS_COUNTER_CHECKER) || defined(CBL_TRACELEVEL_TRACE_TAG_ON)
			:
#endif

#ifdef CBL_TRACELEVEL_TRACE_TAG_ON
			_tag("functor_invoke_guard_t::empty"),
#endif

#ifdef CBL_OBJECTS_COUNTER_CHECKER
			_xDbgCounter(functor_invoke_guard_t_TRACE_LINE)
#endif
			{}

		//template<typename _XHandler/*,
		//	typename THandler = typename std::decay_t<_THandler>,
		//	typename std::enable_if_t<((std::is_lvalue_reference_v<_XHandler> && std::is_nothrow_copy_constructible_v<THandler>) ||
		//	(std::is_rvalue_reference_v< _XHandler> && std::is_nothrow_move_constructible_v<THandler>)), int> = 0*/>
		//	explicit functor_invoke_guard_t(_XHandler&& h_,
		//		std::weak_ptr<distributed_ctx_t> ctx_,
		//		async_space_t scx,
		//		std::uintmax_t invokedMaxCount = 1)noexcept
		//	: functor_invoke_guard_t(std::forward<_XHandler>(h_),
		//		ctx_,
		//		{},
		//		scx,
		//		invokedMaxCount) {}

		template<typename _XHandler/*,
			typename THandler = typename std::decay_t<_THandler>,
			typename std::enable_if_t<((std::is_lvalue_reference_v<_XHandler> && std::is_nothrow_copy_constructible_v<THandler>) ||
			(std::is_rvalue_reference_v< _XHandler> && std::is_nothrow_move_constructible_v<THandler>)), int> = 0*/>
			explicit functor_invoke_guard_t(_XHandler&& h_,
				std::weak_ptr<distributed_ctx_t> ctx_,

#ifdef CBL_TRACELEVEL_TRACE_TAG_ON
				trace_tag&& tag_,
#else
				trace_tag&&,
#endif

				async_space_t scx,
				std::uintmax_t invokedMaxCount = 1)noexcept
			: _hf(std::forward<_XHandler>(h_))

#ifdef CBL_TRACELEVEL_TRACE_TAG_ON
			, _tag(std::move(tag_))
#endif

			, _ctx(ctx_)
			, _scx(scx)
			, _invokedMaxCount{ invokedMaxCount }

#ifdef CBL_OBJECTS_COUNTER_CHECKER
			, _xDbgCounter(functor_invoke_guard_t_TRACE_LINE)
#endif
			{

			CBL_VERIFY(std::is_copy_assignable_v<decltype(_hf)> || invokedMaxCount == 1);
			CBL_VERIFY(invokedMaxCount > 0);
			CBL_VERIFY(invokedMaxCount > 1 || is_once());

#ifdef CBL_TRACELEVEL_TRACE_TAG_ON
			CBL_VERIFY(!_tag.name.empty());
#endif

			}

		functor_invoke_guard_t(const functor_invoke_guard_t&) = delete;
		functor_invoke_guard_t& operator=(const functor_invoke_guard_t&) = delete;

		functor_invoke_guard_t(functor_invoke_guard_t&& o)noexcept
			: _hf(std::move(o._hf))

#ifdef CBL_TRACELEVEL_TRACE_TAG_ON
			, _tag(std::move(o._tag))
#endif

			, _ctx(std::move(o._ctx))
			, _invokedCnt(o._invokedCnt.exchange(o._invokedMaxCount))
			, _scx(o._scx)
			, _invokedMaxCount(o._invokedMaxCount)

#ifdef CBL_OBJECTS_COUNTER_CHECKER
			, _xDbgCounter(std::move(o._xDbgCounter))
#endif
			{

			CBL_VERIFY(!o.do_invoke());
			}

		functor_invoke_guard_t& operator=(functor_invoke_guard_t&& o)noexcept {
			if (this != std::addressof(o)) {
				_hf = std::move(o._hf);

#ifdef CBL_TRACELEVEL_TRACE_TAG_ON
				_tag = std::move(o._tag);
#endif

				_ctx = std::move(o._ctx);
				_invokedCnt.store(o._invokedCnt.exchange(o._invokedMaxCount));
				_scx = o._scx;
				_invokedMaxCount = o._invokedMaxCount;

#ifdef CBL_OBJECTS_COUNTER_CHECKER
				_xDbgCounter = std::move(o._xDbgCounter);
#endif

				CBL_VERIFY(!o.do_invoke());
				}

			return (*this);
			}

		~functor_invoke_guard_t() {
			if (is_invoke_ready()) {
				CHECK_NO_EXCEPTION(__invoke_async());
				}
			}

		bool is_once()const noexcept {
			const auto invokedCnt = _invokedCnt.load(std::memory_order::memory_order_acquire);
			return invokedCnt >= _invokedMaxCount || (_invokedMaxCount - invokedCnt) == 1;
			}

		async_space_t space()const noexcept {
			return _scx;
			}

		std::shared_ptr<distributed_ctx_t> ctx()const noexcept {
			return _ctx.lock();
			}

		template<typename ...TResultValues>
		auto operator()(TResultValues&& ... result) {
			return invoke_explicit(std::forward<TResultValues>(result)...);
			}

		template<typename ...TResultValues>
		void invoke_async(TResultValues&& ... result)noexcept {
			__invoke_async(std::forward<TResultValues>(result)...);
			}

		template<typename ...TResultValues>
		auto invoke_explicit(TResultValues&& ... result) {
			return __invoke_explicit(std::forward<TResultValues>(result)...);
			}

		template<typename ...TResultValues>
		void operator()(invoke_no_throw, TResultValues&& ... result) {
			CHECK_NO_EXCEPTION(invoke_async(std::forward<TResultValues>(result)...));
			}

		bool empty()const noexcept {
			return !_hf;
			}

		size_t invoked_max_count()const noexcept {
			return _invokedMaxCount;
			}

		operator bool()const noexcept {
			return !empty() && _invokedCnt.load(std::memory_order::memory_order_acquire) < _invokedMaxCount;
			}

		void reset()noexcept {
			_invokedCnt.store(_invokedMaxCount, std::memory_order::memory_order_release);
			}

		size_t invoked_count()const noexcept {
			return _invokedCnt.load(std::memory_order::memory_order_acquire);
			}

		void clear()noexcept {
			reset();

			if (!empty()) {
				CHECK_NO_EXCEPTION_BEGIN

					if (auto c = _ctx.lock()) {
						c->launch_async(__FILE_LINE__, [](decltype(_hf)&&) {}, std::move(_hf));
						}

				CHECK_NO_EXCEPTION_END
				}
			}

		template<typename THandler>
		auto reset_handler(
			trace_tag&& tag_,
			THandler&& h) {

			handler_t hNext(std::forward<THandler>(h));
			std::swap(_hf, hNext);

			if (hNext) {
				return functor_invoke_guard_t(std::move(hNext), _ctx, std::move(tag_), _scx);
				}
			else {
				return functor_invoke_guard_t{};
				}
			}
	};

#undef functor_invoke_guard_t_TRACE_LINE


using inbound_message_with_tail_exception_guard_handler_by_address = std::function<void(const address_descriptor_t & addr, std::unique_ptr<dcf_exception_t>&&)>;
using inbound_message_with_tail_exception_guard_handler = crm::utility::invoke_functor<void, std::unique_ptr<dcf_exception_t>&&>;


using inbound_message_with_tail_exception_guard =
functor_invoke_guard_t<inbound_message_with_tail_exception_guard_handler, make_fault_result_t>;
}

