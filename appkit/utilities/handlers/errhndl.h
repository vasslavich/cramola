#pragma once


#include "../../internal_invariants.h"
#include "./invoke_guard.h"
#include "../../functorapp/invoker_ptr.h"
#include "./base_terms.h"


namespace crm{


class __error_handler_point_f{
	using function_handler_type = detail::functor_invoke_guard_t<
		utility::invoke_functor<void, std::unique_ptr<dcf_exception_t> &&>,
		make_fault_result_t>;

	function_handler_type _h;

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	utility::__xobject_counter_logger_t<__error_handler_point_f, 10> _xDbgCounter;
#endif

public:
	__error_handler_point_f()noexcept;

	template<typename TX,
		typename std::enable_if_t < std::is_invocable_v<TX, std::unique_ptr<dcf_exception_t> &&>, int > = 0 >
		__error_handler_point_f( TX && h_,
			std::weak_ptr<distributed_ctx_t> ctx_,
			async_space_t scx )
		: _h( std::forward<TX>( h_ ), ctx_, "__error_handler_point_f", scx, function_handler_type::invoke_once ){}

	void operator()( std::unique_ptr<dcf_exception_t> && e )noexcept;

	operator bool()const noexcept;
	async_space_t space()const noexcept;
	std::shared_ptr<distributed_ctx_t> ctx()const noexcept;
	};
}

