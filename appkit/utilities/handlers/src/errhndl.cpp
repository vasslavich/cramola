#include "../../../internal.h"
#include "../errhndl.h"


using namespace crm;
using namespace crm::detail;



__error_handler_point_f::__error_handler_point_f()noexcept{}

void __error_handler_point_f::operator()( std::unique_ptr<dcf_exception_t> && e )noexcept{
	_h.invoke_async( e ? e->dcpy() : nullptr );
	}

__error_handler_point_f::operator bool()const noexcept{
	return _h;
	}

async_space_t __error_handler_point_f::space()const noexcept{
	return _h.space();
	}

std::shared_ptr<distributed_ctx_t> __error_handler_point_f::ctx()const noexcept{
	return _h.ctx();
	}


static_assert(!std::is_copy_constructible_v< __error_handler_point_f>, __FILE__);
static_assert(!std::is_copy_assignable_v<__error_handler_point_f>, __FILE__);
static_assert(std::is_move_constructible_v< __error_handler_point_f>, __FILE__);
static_assert(std::is_move_assignable_v<__error_handler_point_f>, __FILE__);
