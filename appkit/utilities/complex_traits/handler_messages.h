#pragma once


#include <type_traits>
#include "../../functorapp/function_meta.h"
#include "../../messages/strob_envelop.h"


namespace crm::detail {


template<typename THandler>
struct handler_message_trait {
	using meta_type = typename function_meta2<THandler>;
	using tuple_type = typename meta_type::params_tuple;

	static constexpr size_t tuple_size_v = std::tuple_size_v<tuple_type>;

	template<typename tuple_type, std::size_t ... I>
	static decltype(auto) get_im(tuple_type&&, std::index_sequence<I...>)noexcept {
		using imessage_type = istrob_message_envelop_t<std::tuple_element_t<I, std::decay_t<tuple_type>>...>;
		return imessage_type{};
		}

	template<typename tuple_type, std::size_t ... I>
	static decltype(auto) get_om(tuple_type&&, std::index_sequence<I...>)noexcept {
		using omessage_type = ostrob_message_envelop_t<std::tuple_element_t<I, std::decay_t<tuple_type>>...>;
		return omessage_type{};
		}

	using imessage_type = typename std::decay_t<decltype(get_im(std::declval<tuple_type>(), std::make_index_sequence<std::tuple_size_v<tuple_type>>{})) > ;
	using omessage_type = typename std::decay_t<decltype(get_om(std::declval<tuple_type>(), std::make_index_sequence<std::tuple_size_v<tuple_type>>{})) > ;
	};
}
