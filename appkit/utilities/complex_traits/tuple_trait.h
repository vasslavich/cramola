#pragma once


#include <tuple>


namespace crm::detail {


struct detect_fail_self {};


template<typename ... AL>
class tuple_envelop_t final : detect_fail_self {
	static_assert(srlz::detail::is_all_srlzd_v<AL...>, __FILE_LINE__);

	using tuple_type = std::tuple<std::decay_t<AL>...>;

	tuple_type _ts{};

public:
	using base_type = detect_fail_self;
	using self_type = typename tuple_envelop_t<AL...>;

	template<typename S>
	void srlz(S&& dest)const {
		srlz::serialize(std::forward<S>(dest), _ts);
		}

	template<typename S>
	void dsrlz(S&& src) {
		srlz::deserialize(std::forward<S>(src), _ts);
		}

	std::enable_if_t< srlz::detail::is_all_srlzd_prefixed_v<AL...>, size_t> get_serialized_size()const noexcept {
		return srlz::object_trait_serialized_size(_ts);
		}

public:
	tuple_envelop_t()noexcept {}

	template<typename ... XAL_,
		typename std::enable_if_t<(
			!srlz::detail::is_has_inherited_from_v<detect_fail_self, XAL_...> &&
			!srlz::detail::exists_type_v<self_type, XAL_...> &&
			srlz::detail::is_all_srlzd_v<XAL_...> &&
			(sizeof...(XAL_)) >= 1
			), int> = 0>
		tuple_envelop_t(XAL_&& ... al)
		:  _ts(std::forward<XAL_>(al)...) {}

	static constexpr size_t elements_count()noexcept {
		return std::tuple_size_v<tuple_type>;
	}

	template<const size_t I>
	constexpr decltype(auto) value() && noexcept {
		return std::get<I>(move(_ts));
		}

	template<const size_t I>
	constexpr decltype(auto) value() & noexcept {
		return std::get<I>(_ts);
		}

	template<const size_t I>
	constexpr decltype(auto) value()const& noexcept {
		return std::get<I>(_ts);
		}

	constexpr tuple_type&& values_tuple() && noexcept {
		return std::move(_ts);
		}

	constexpr const tuple_type& values_tuple()const& noexcept {
		return _ts;
		}
	};


template<typename ... AL>
decltype(auto) make_tuple_envelop(AL&& ... al) {
	return tuple_envelop_t<AL...>(std::forward<AL>(al)...);
	}

template<typename ... AL>
tuple_envelop_t(AL&& ...)->tuple_envelop_t<AL...>;
}


