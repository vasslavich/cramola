#include "../../../internal.h"
#include "../checker_seqcall.h"


using namespace crm;
using namespace crm::detail;


#ifdef CBL_USE_SEQUENTIAL_CALL_CHECKER
void checker_seqcall_t::__this_value_t::inc()noexcept {
	if( ++counter > 1 )
		FATAL_ERROR_FWD(nullptr);
	}

void checker_seqcall_t::__this_value_t::dec()noexcept {
	if( --counter < 0 )
		FATAL_ERROR_FWD(nullptr);
	}

checker_seqcall_t::ref_to_this_t::ref_to_this_t()
	: ref( std::make_shared<__this_value_t>() ) {}

void checker_seqcall_t::ref_to_this_t::inc() const noexcept{
	if( ref )
		ref->inc();
	}

void checker_seqcall_t::ref_to_this_t::dec() const noexcept{
	if( ref )
		ref->dec();
	}

void checker_seqcall_t::ref_to_this_t::reset() noexcept{
	ref.reset();
	}

void checker_seqcall_t::inc()const noexcept{
	_value.inc();
	}

void checker_seqcall_t::dec()const noexcept{
	_value.dec();
	}

void checker_seqcall_t::decrement_t::dec()noexcept {
	bool f{ false };
	if(_inf.compare_exchange_strong(f, true)) {
		refThis.dec();
		}
	}

void checker_seqcall_t::decrement_t::reset()noexcept {
	_inf.store(true, std::memory_order::memory_order_release);
	}

checker_seqcall_t::decrement_t::decrement_t( ref_to_this_t obj )noexcept
	: refThis( std::move( obj ) ) {}

checker_seqcall_t::decrement_t::~decrement_t() {
	dec();
	}

checker_seqcall_t::decrement_t::decrement_t( decrement_t && obj )noexcept
	: refThis( std::move( obj.refThis) ) {

	obj.refThis.reset();
	}

checker_seqcall_t::decrement_t& checker_seqcall_t::decrement_t::operator=(decrement_t && obj) noexcept{
	refThis = std::move( obj.refThis);
	obj.refThis.reset();

	return (*this);
	}

checker_seqcall_t::decrement_t checker_seqcall_t::inc_scope()const noexcept{
	_value.inc();
	return decrement_t( _value );
	}
#endif

