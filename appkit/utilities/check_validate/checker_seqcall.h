#pragma once


#include <atomic>
#include <memory>
#include "../../counters/internal_terms.h"
#include "../../internal_invariants.h"


namespace crm{
namespace detail{


#ifdef CBL_USE_SEQUENTIAL_CALL_CHECKER
struct checker_seqcall_t {
	struct __this_value_t : public std::enable_shared_from_this<__this_value_t> {
		typedef __this_value_t self_t;

	private:
		std::atomic<std::intmax_t> counter = 0;

#ifdef CBL_MPF_OBJECTS_COUNTER_CHECKER_SEQCALL
		crm::utility::__xobject_counter_logger_t<self_t> _xDbgCounter;
#endif

	public:
		void inc()noexcept;
		void dec()noexcept;
		};

	struct decrement_t;

	struct ref_to_this_t {
		friend struct decrement_t;
	private:
		std::shared_ptr<__this_value_t> ref;

		void reset()noexcept;

	public:
		ref_to_this_t();

		void inc()const noexcept;
		void dec()const noexcept;
		};

	void inc()const noexcept;
	void dec()const noexcept;

public:
	struct decrement_t {
	private:
		ref_to_this_t refThis;
		std::atomic<bool> _inf{ false };

		void reset()noexcept;

	public:
		decrement_t( ref_to_this_t obj )noexcept;
		~decrement_t();

		decrement_t( const decrement_t & ) = delete;
		decrement_t& operator=(const decrement_t &) = delete;

		decrement_t( decrement_t && )noexcept;
		decrement_t& operator=(decrement_t &&)noexcept;

		void dec()noexcept;
		};

	decrement_t inc_scope()const noexcept;

private:
	ref_to_this_t _value;
	};

#else//CBL_USE_SEQUENTIAL_CALL_CHECKER

struct checker_seqcall_t{
	struct decrement_t{};

	void inc()const noexcept{}
	void dec()const noexcept{}
	decrement_t inc_scope()const noexcept{ return decrement_t(); }
	};

#endif//CBL_USE_SEQUENTIAL_CALL_CHECKER
}
}
