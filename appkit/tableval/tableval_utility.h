#pragma once


#include <functional>
#include "../resources/i_memory_services.h"
#include "../typeframe/streamcat/interators/pod_iterator.h"


namespace crm{
namespace detail{

class memfree_with_i_services_t {
private:
	std::shared_ptr<i_memory_services_t> _srv;
	
public:
	void *p = nullptr;

	virtual ~memfree_with_i_services_t();
	void deallocate();
	void clear();

	template < typename TAllocator,
		typename std::enable_if<std::is_base_of<i_memory_services_t, TAllocator>::value, int>::type = 0 >
	memfree_with_i_services_t( typename std::shared_ptr<TAllocator> srv_ )
		: _srv( std::dynamic_pointer_cast<i_memory_services_t>( srv_ ) ) {

		if( !_srv )
			THROW_EXC_FWD(nullptr);
		}

	template<typename TContainer,
		typename std::enable_if<crm::srlz::detail::is_range_bytes_t<TContainer>::value, int>::type = 0 >
	void copy2host( const typename TContainer & data, void **ppHostBuffer, 
		const char* traceid ) {

		if( !ppHostBuffer )
			THROW_EXC_FWD(nullptr);
		
		/* ��������� ������ ����� ��� �������� ����� */
		allocate( data.size(), traceid );

		/* ���������� */
		std::copy( std::cbegin( data ), std::cend( data ),
			crm::srlz::make_binary_iterator_t::create( (std::decay_t<decltype(*std::cbegin( data ))>*)p, std::size( data ), 0 ) );

		(*ppHostBuffer) = p;
		p = nullptr;
		}

	template<typename TContainer,
		typename std::enable_if<crm::srlz::detail::is_range_bytes_t<TContainer>::value, int>::type = 0 >
	void copy2host( const typename TContainer & data, void **ppHostBuffer ) {
		copy2host( data, ppHostBuffer, nullptr );
		}

	void allocate( size_t bytes, const char * traceid );
	void allocate( size_t bytes );
	};

class deallocator_with_functor_t {
public:
	typedef std::function<void( void *pDt_ )> deallocator_hndl_t;
	void *pDt = nullptr;

private:
	deallocator_hndl_t _deallocator;

public:
	void clear();
	void reset();

	deallocator_with_functor_t( deallocator_hndl_t da_ );
	deallocator_with_functor_t( void *p, deallocator_hndl_t da_ );
	~deallocator_with_functor_t();
	};
}
}

