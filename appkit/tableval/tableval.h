#pragma once


#include <optional>
#include <map>
#include <memory>
#include <vector>
#include <list>
#include <cstdint>
#include "../typeframe/isrlz.h"
#include "../typeframe/streams.h"
#include "./tableval_utility.h"
#include "../resources/i_memory_services.h"


namespace crm{

class __datagram_impl_t;

/*! ���� ������ ��������� */
class datagram_entry_t {
	friend class __datagram_impl_t;

public:
	typedef std::vector<uint8_t> bin_t;
	using type_type_t = uint64_t ;

private:
	/// ��� �����
	type_type_t _utype{0};
	/// ��� �����
	std::string _name;
	/*! �������� ������ ����� */
	bin_t _data;

public:
	static const datagram_entry_t null;

	const bin_t& binary()const;
	type_type_t utype()const;
	const std::string& name()const;
	size_t packed_size()const;
	bool is_empty()const;

	/*! �������� ���������������� ������������� ������� ��������� � ������ */
	template<typename Tws>
	void pack(Tws && sout)const{

		// ��� �����
		srlz::serialize(sout, utype());
		// ��� �����
		srlz::serialize(sout, _name);
		// ���� ������
		srlz::serialize(sout, _data);
		}

	template<typename TValue>
	static type_type_t type_value() {
		auto tval = typeid(TValue).hash_code();
		static_assert(sizeof( tval ) == sizeof( type_type_t ), __FILE__);
		return tval;
		}

private:
	/// �������� ����� ���������
	datagram_entry_t( const type_type_t type_,
		const std::string &name, const void *pData, size_t bCnt );

	/// �������� ����� ���������
	template<typename Tsequence>
	datagram_entry_t( const type_type_t type_,
		const std::string &name, Tsequence && bin )
		: _utype( type_ )
		, _name( name )
		, _data( std::forward<Tsequence>( bin ) ) {}

	/// �������� ����� ���������
	template<typename TInIt>
	datagram_entry_t( const type_type_t type_,
		const std::string &name, TInIt first, TInIt last )
		: _utype( type_ )
		, _name( name )
		, _data( first, last ) {}

	template < typename T, typename std::enable_if_t<(!srlz::is_serializable_v<T>), int> = 0 >
	static datagram_entry_t create_value( const std::string & name, const T & value ) {
		static_assert(false, __FILE__);
		}

	template < typename T, typename std::enable_if_t<srlz::is_serializable_v<T>, int> = 0 >
	static datagram_entry_t create_value( const std::string & name, const T & value ) {

		auto ws( crm::srlz::wstream_constructor_t::make() );
		srlz::serialize( ws, value );

		return datagram_entry_t( type_value<T>(), name, ws.release() );
		}

public:
	datagram_entry_t();

	template<typename T, typename std::enable_if_t<!(srlz::is_serializable_v<T>), int> = 0>
	typename T get_value()const {
		static_assert(false, __FILE__);
		}

	template<typename T, typename std::enable_if_t<srlz::is_serializable_v<T>, int> = 0>
	typename T get_value()const {

		auto rstream( crm::srlz::rstream_constructor_t::make( binary().cbegin(), binary().cend() ) );

		typename T result;
		srlz::deserialize( rstream, result );

		return std::move( result );
		}
	};

/*!
����� �������������� ������ ���������.
��� ���������� ������ � ������ ���������� ��-�������� ����������� ������ �����.
��� ������������ ����������, ���������� ������� ���� �� ������� #build.
��� ����, ���������� ����������� ������ ������ � ���������� ������� ������ (������������ ������ ���������).
*/
class __datagram_impl_t {
	friend class datagram_t;

public:
	enum class build_class_t : uint8_t {
		lower_bound,
		undefined,
		plane,
		plane_with_validate,
		upper_bound
		};

	using datagram_type_t = uint64_t;
	typedef uint64_t length_type_t;

	typedef datagram_entry_t entity_t;
	typedef std::shared_ptr<entity_t> ref_entity_t;
	typedef std::shared_ptr<const entity_t> cref_entity_t;

	typedef std::vector<ref_entity_t> entities_vector_t;
	typedef std::map<entity_t::type_type_t, entities_vector_t> entities_type_map_t;

	typedef entities_vector_t::const_iterator entity_const_iterator;
	typedef entities_vector_t::iterator entity_iterator;

public:
	/// NULL - ���������
	static const __datagram_impl_t NULL_DATAGRAM;

private:
	/// ������� ��������� ������������� ���������
	static const entity_t::bin_t binary_blocks_prefix;
	static const entity_t::bin_t binary_blocks_postfix;

	/// ����� � ������ ��������� ���������� ������������� ������ ����������
	static const length_type_t build_class_length;

	/// ����� ��������� ��������� ����������� ��� ���������
	static const length_type_t type_length;
	static const length_type_t type_offset;

	/// ����� ��������� ��������� ����������� ������ ������
	static const length_type_t size_length;
	static const length_type_t size_offset;

	/// ����� � ������ ������������ ������ (���������) ���������
	static const length_type_t prefix_length;

	static size_t binary_blocks_prefix_size();
	static size_t binary_blocks_postfix_size();
	static bool validate_blocks_prefix( const entity_t::bin_t& s );
	static bool validate_blocks_postfix( const entity_t::bin_t& s );

	void add( ref_entity_t && entr );

	template<typename TValue>
	static entity_t::type_type_t type_value() {
		return entity_t::type_value<TValue>();
		}

	template<typename TType,
		typename std::enable_if<(!srlz::is_serializable_v<TType>), int>::type = 0>
		bool tget_item_by_index( typename TType & result,
		const std::string &name,
		const size_t index,
		std::function<TType( cref_entity_t It )> fcast )const {

		static_assert(false, "type serializable is not supported");
		}

	template<typename TType,
		typename std::enable_if<srlz::is_serializable_v<TType>, int>::type = 0>
	bool tget_item_by_index( typename TType & result,
		const std::string &name,
		const size_t index,
		std::function<TType( cref_entity_t It )> fcast )const {

		const auto type = type_value<TType>();
		auto pItem( find( type, name, index ) );
		if( pItem ) {
			if( pItem->utype() != type )
				SERIALIZE_THROW_EXC_FWD(nullptr);

			if( !pItem->is_empty() ) {
				result = fcast( pItem );
				return true;
				}
			}

		return false;
		}

	template<typename TType,
		typename std::enable_if<srlz::is_serializable_v<TType>, int>::type = 0>
	std::optional<TType> tget_item_by_index_2(const std::string &name,
		const size_t index,
		std::function<TType(cref_entity_t It)> fcast)const {

		const auto type = type_value<TType>();
		auto pItem(find(type, name, index));
		if(pItem) {
			if(pItem->utype() != type)
				SERIALIZE_THROW_EXC_FWD(nullptr);

			if(!pItem->is_empty()) {
				return std::make_optional(fcast(pItem));
				}
			}

		return std::nullopt;
		}

	template<typename TType,
		typename std::enable_if<(!srlz::is_serializable_v<TType>), int>::type = 0>
		typename std::list<TType> tget_collection( std::function<TType( cref_entity_t It )> fcast )const {

		static_assert(false, "type serializable is not supported");
		}

	/// ����� ���� ������ ��������� ��������� ����
	template<typename TType,
		typename std::enable_if<srlz::is_serializable_v<TType>, int>::type = 0>
		typename std::list<TType> tget_collection( std::function<TType( cref_entity_t It )> fcast )const {

		const auto type = type_value<TType>();
		std::list<TType> lst;

		auto it = get_begin( type );
		const auto itEnd = get_end( type );
		for( ; it != itEnd; ++it ) {

			if( (*it)->utype() != type )
				SERIALIZE_THROW_EXC_FWD(nullptr);

			if( !(*it)->is_empty() ) {
				auto value( fcast( *it ) );
				lst.emplace_back( std::move( value ) );
				}
			}

		return std::move( lst );
		}

	template<typename TType,
		typename std::enable_if<(!srlz::is_serializable_v<TType>), int>::type = 0>
		typename std::list<TType> tget_collection_by_name( const std::string & name,
		std::function<TType( cref_entity_t It )> fcast )const {

		static_assert(false, "type serializable is not supported");
		}

	/// ����� ���� ������ ��������� ��������� ����
	template<typename TType,
		typename std::enable_if<srlz::is_serializable_v<TType>, int>::type = 0>
		typename std::list<TType> tget_collection_by_name( const std::string & name,
		std::function<TType( cref_entity_t It )> fcast )const {

		const auto type = type_value<TType>();
		size_t index = 0;
		cref_entity_t itr;
		std::list<TType> lst;
		do {
			itr = find( type, name, index );
			if( itr ) {
				if( itr->utype() != type || itr->name() != name )
					SERIALIZE_THROW_EXC_FWD(nullptr);

				if( !itr->is_empty() ) {
					auto value( fcast( itr ) );
					lst.emplace_back( std::move( value ) );
					}
				}

			++index;

			} while( itr );

			return std::move( lst );
		}

public:
	/// ���������� �������� ������������������ � ���������
	void add( const entity_t::type_type_t type,
		const void *pData, const size_t dataSize );

	/// ���������� �������� ������������������ � ���������
	void add( const entity_t::type_type_t type,
		const std::string &name, const void *pData, const size_t dataSize );

	template<typename Tsequence>
	void add( const entity_t::type_type_t type,
		const std::string &name, typename Tsequence && data ) {

		ref_entity_t entry( new entity_t( type, name, std::forward<Tsequence>( data ) ) );
		add( std::move( entry ) );
		}

	template<typename TInIt>
	void add( const entity_t::type_type_t type,
		const std::string &name, typename TInIt first, typename TInIt last ) {

		ref_entity_t entry( new entity_t( type, name, first, last ) );
		add( std::move( entry ) );
		}


	static entity_t::bin_t cpy( const void *dt );
	static size_t max_mem_size();

	/// ����� ������� ����� ��������� ��������� ����
	ref_entity_t __find( entity_t::type_type_t type, size_t index );
	cref_entity_t __find( entity_t::type_type_t type, size_t index )const;

	/// ����� ������� ����� ��������� ��������� ���� � �����
	ref_entity_t __find( entity_t::type_type_t type,
		const std::string &name, size_t index );
	cref_entity_t __find( entity_t::type_type_t type,
		const std::string &name, size_t index )const;

protected:
	template<typename Ts>
	void srlz(Ts && dest)const{
		auto bin{ build() };
		srlz::serialize(std::forward<Ts>(dest), bin);
		}

	template<typename Ts>
	void dsrlz(Ts && src ){
		entity_t::bin_t val;
		srlz::deserialize(std::forward<Ts>(src), val);

		if (!val.empty())
			create_from(val.data());
		}

public:
	template<typename T,
		typename std::enable_if<srlz::is_serializable_v<T>, int>::type = 0>
		void add_value( const std::string &name, const typename T & value ) {

		ref_entity_t entry( new entity_t( entity_t::create_value( name, value ) ) );
		add( std::move( entry ) );
		}

	template<typename T,
		typename std::enable_if<(!srlz::is_serializable_v<T>), int>::type = 0>
		void add_value( const std::string &name, const typename T & value ) {
		static_assert(false, "type serializable is not supported");
		}

	void add( entity_const_iterator first, entity_const_iterator last );
	void add( const __datagram_impl_t & dtg );

	/// ����� ������� ����� ��������� ��������� ����
	ref_entity_t find( const entity_t::type_type_t type, const size_t index );
	cref_entity_t find( const entity_t::type_type_t type, const size_t index )const;

	/// ����� ������� ����� ��������� ��������� ���� � �����
	ref_entity_t find( const entity_t::type_type_t type,
		const std::string &name, const size_t index );
	cref_entity_t find( const entity_t::type_type_t type,
		const std::string &name, const size_t index )const;

	template<typename T, typename std::enable_if<!(srlz::is_serializable_v<T>), int>::type = 0>
	std::list<T> get_values( const std::string & name )const {
		static_assert(false, "type serializable is not supported");
		}

	template<typename T, typename std::enable_if<!(srlz::is_serializable_v<T>), int>::type = 0>
	std::list<T> get_values()const {
		static_assert(false, "type serializable is not supported");
		}

	/// ����� ������� ����� ��������� ��������� ���� � �����
	template<typename T, typename std::enable_if<!(srlz::is_serializable_v<T>), int>::type = 0>
	bool try_get_value( typename T & result, const std::string &name, const size_t index )const {
		static_assert(false, "type serializable is not supported");
		}

	/// ����� ������� ����� ��������� ��������� ���� � �����
	template<typename T, typename std::enable_if<(!srlz::is_serializable_v<T>), int>::type = 0>
	typename T get_value( const std::string &name, const size_t index )const {
		static_assert(false, "type serializable is not supported");
		}

	/// ����� ������� ����� ��������� ��������� ���� � �����
	template<typename T, typename std::enable_if<srlz::is_serializable_v<T>, int>::type = 0>
	bool try_get_value( typename T & result, const std::string &name, const size_t index )const {
		return tget_item_by_index<T>( result, name, index, []( cref_entity_t pItem ) {return pItem->get_value<T>(); } );
		}

	/// ����� ������� ����� ��������� ��������� ���� � �����
	template<typename T, typename std::enable_if<srlz::is_serializable_v<T>, int>::type = 0>
	std::optional<T> get_value_2(const std::string &name, const size_t index)const {
		return tget_item_by_index_2<T>(name, index, [](cref_entity_t pItem) {return pItem->get_value<T>(); });
		}

	/// ����� ������� ����� ��������� ��������� ���� � �����
	template<typename T>
	bool try_get_value( typename T & result, const std::string &name )const {
		return try_get_value<T>( result, name, 0 );
		}

	/// ����� ������� ����� ��������� ��������� ���� � �����
	template<typename T>
	std::optional<T> get_value_2(const std::string &name)const {
		return get_value_2<T>(name, 0);
		}

	/// ����� ������� ����� ��������� ��������� ���� � �����
	template<typename T>
	bool try_get_value( typename T & result, const size_t index )const {
		return try_get_value<T>( result, "", index );
		}

	/// ����� ������� ����� ��������� ��������� ���� � �����
	template<typename T>
	std::optional<T> get_value_2(const size_t index = 0)const {
		return get_value_2<T>("", index);
		}

	/// ����� ������� ����� ���������
	template<typename T>
	bool try_get_value( typename T & result )const {
		return try_get_value<T>( result, "", 0 );
		}

	/// ����� ������� ����� ��������� ��������� ���� � �����
	template<typename T, typename std::enable_if<srlz::is_serializable_v<T>, int>::type = 0>
	typename T get_value( const std::string &name, const size_t index )const {

		typename T value;
		if( try_get_value<T>( value, name, index ) )
			return std::move( value );
		else {
			SERIALIZE_THROW_EXC_FWD(nullptr);
			}
		}

	/// ����� ������� ����� ��������� ��������� ���� � �����
	template<typename T>
	typename T get_value( const std::string &name )const {
		return get_value<T>( name, 0 );
		}

	/// ����� ������� ����� ��������� ��������� ���� � �����
	template<typename T>
	typename T get_value( const size_t index )const {
		return get_value<T>( "", index );
		}

	/// ����� ���� ������ ��������� ��������� ����
	template<typename T, typename std::enable_if<srlz::is_serializable_v<T>, int>::type = 0>
	typename std::list<T> get_values()const {

		return tget_collection<T>( []( cref_entity_t pItem ) {return pItem->get_value<T>(); } );
		}

	/// ����� ���� ������ ��������� ��������� ����
	template<typename T, typename std::enable_if<srlz::is_serializable_v<T>, int>::type = 0>
	typename std::list<T> get_values( const std::string & name )const {

		return tget_collection_by_name<T>( name, []( cref_entity_t pItem ) {return pItem->get_value<T>(); } );
		}

private:
	entity_iterator get_begin();
	entity_iterator get_begin( const entity_t::type_type_t type );
	entity_const_iterator get_begin()const;
	entity_const_iterator get_begin( const entity_t::type_type_t type )const;

	entity_iterator get_end();
	entity_iterator get_end( const entity_t::type_type_t type );
	entity_const_iterator get_end()const;
	entity_const_iterator get_end( const entity_t::type_type_t type )const;

public:
	/// ����� ������ � ���������
	size_t count()const;

	/// ����� ������ � ���������, ��������� ����
	size_t count( const entity_t::type_type_t entityType )const;
	/// ����� ������ � ���������, ��������� ���� � �����
	size_t count( const entity_t::type_type_t entityType, const std::string &name )const;

	template<typename T, typename std::enable_if<!(srlz::is_serializable_v<T>), int>::type = 0>
	size_t count()const {
		static_assert(false, "type serializable is not supported");
		}

	/// ����� ������ � ���������, ��������� ����
	template<typename T, typename std::enable_if<srlz::is_serializable_v<T>, int>::type = 0>
	size_t count()const {
		return count( type_value<T>() );
		}

	template<typename T, typename std::enable_if<!(srlz::is_serializable_v<T>), int>::type = 0>
	size_t count( const std::string &name )const {
		static_assert(false, "type serializable is not supported");
		}

	/// ����� ������ � ���������, ��������� ���� � �����
	template<typename T, typename std::enable_if<srlz::is_serializable_v<T>, int>::type = 0>
	size_t count( const std::string &name )const {
		return count( type_value<T>(), name );
		}

	/// �������� ������� ��������� �� ���������������� �������������
	void create_from( const void *pDt );

	/// �������� ������� ��������� ��������� ����
	void create( const datagram_type_t type );

	/// �������� ����������� ���������
	void clear();


	explicit __datagram_impl_t( datagram_type_t type = 0 );
	explicit __datagram_impl_t( const void *pDt );
	explicit __datagram_impl_t( const std::vector<uint8_t> & dt );


	__datagram_impl_t( const __datagram_impl_t & dt ) = delete;
	__datagram_impl_t( __datagram_impl_t  && dt ) = delete;
	__datagram_impl_t& operator=(const __datagram_impl_t & dt) = delete;
	__datagram_impl_t& operator=(__datagram_impl_t && dt) = delete;


	~__datagram_impl_t();

	/*! �������� ���������������� ������������� ������� ��������� � ������� ���������� ����������
	\param [in] obj ��������� ����������
	\param [in] ppSerialData ����� ���������� ������ ������ �������� ��������������� ������������� ���������
	\param [in] createCRC ������� �������� ����������� �����
	*/
	template<typename TAllocator,
		typename std::enable_if<std::is_base_of<i_memory_services_t, TAllocator>::value, int>::type = 0 >
		void build( std::shared_ptr<TAllocator> allocator, void **ppSerialData,
		const char * traceid )const {

		/* ��������� ��������� */
		auto bin( build() );
		/* ������ ����� */
		detail::memfree_with_i_services_t scope( allocator );
		/* ���������� */
		scope.copy2host( bin, ppSerialData, traceid );
		}

	template<typename TAllocator,
		typename std::enable_if<std::is_base_of<i_memory_services_t, TAllocator>::value, int>::type = 0 >
		void build( std::shared_ptr<TAllocator> allocator, void **ppSerialData )const {
		build( allocator, ppSerialData, nullptr );
		}

	/*! �������� ���������������� ������������� ������� ��������� � �������� ������ */
	entity_t::bin_t build()const;

	/*! �������� ���������������� ������������� ������� ��������� � ������ */
	template<typename Ts>
	void build(Ts && sout)const{
#ifdef CBL_DATAGRAM_CHECK_SRLZLAYOUT
		auto sOut = sout.ready();
#endif

		/* ����� ����������
		--------------------------------------------------*/
#ifdef CBL_USE_DATAGRAM_VALIDATE_PREFIX
		srlz::serialize(sout, build_class_t::plane_with_validate);
#else
		srlz::serialize(sout, build_class_t::plane);
#endif

		/* ���
		--------------------------------------------------*/
		srlz::serialize(sout, _dtgType);

		/* �����
		-------------------------------------------------*/
		const auto l = datagram_length();
		if (l <= (std::numeric_limits<max_serialized_length_type>::max)()) {
			srlz::serialize(sout, static_cast<max_serialized_length_type>(l));
			}
		else {
			THROW_EXC_FWD(nullptr);
			}

#ifdef CBL_DATAGRAM_CHECK_SRLZLAYOUT
		if ((sout.ready() - sOut) != prefix_length)
			SERIALIZE_THROW_EXC_FWD(L"datagram: bad layout(prefix length)");
		sOut = sout.ready();
#endif

		/* �������������� ���������� �������
		--------------------------------------------------*/
#ifdef CBL_USE_DATAGRAM_VALIDATE_PREFIX
		srlz::serialize(sout, binary_blocks_prefix);
#endif


		/* �����
		--------------------------------------------------*/
		auto itType = _entitiesMap.cbegin();
		const auto endType = _entitiesMap.cend();
		for (; itType != endType; ++itType){

			auto itBlock = itType->second.cbegin();
			const auto endBlocks = itType->second.cend();
			for (; itBlock != endBlocks; ++itBlock)
				(*itBlock)->pack(sout);
			}

		/* �������������� ���������� ��������
		--------------------------------------------------*/
#ifdef CBL_USE_DATAGRAM_VALIDATE_PREFIX
		srlz::serialize(sout, binary_blocks_postfix);
#endif

#ifdef CBL_DATAGRAM_CHECK_SRLZLAYOUT
		if ((sout.ready() - sOut) != datagram_length())
			SERIALIZE_THROW_EXC_FWD(L"datagram: bad layout");
#endif
		}

private:
	// ����� ���� ����������� ��� ���������� ��������������� ���������
	size_t size_packed()const;
	// ����� ���������
	size_t datagram_length()const;

	/// ��� ���������, \ref crm::datagram_type_t
	datagram_type_t type()const;

public:
	bool empty()const;

private:

	datagram_type_t _dtgType{ 0 };
	size_t _sizeBlocks{ 0 };
	entities_type_map_t _entitiesMap;
	entities_vector_t _fullLst;

	/* ������� ������.
	��������� ��� �������� ��������� �� ������ ������ ��������, ����� ��
	��������� ��������� ������ ������� ��������.
	*/
	entities_vector_t _nullList;
	};


	/*=======================================================================
	datagram_t - front end
	=========================================================================*/


class datagram_t final : public srlz::serialized_base_rw {
private:
	std::unique_ptr<__datagram_impl_t> _pdt;

public:
	using datagram_type_t = __datagram_impl_t::datagram_type_t;

	typedef __datagram_impl_t::build_class_t build_class_t;
	typedef __datagram_impl_t::length_type_t length_type_t;

	typedef __datagram_impl_t::entity_t entity_t;
	typedef __datagram_impl_t::ref_entity_t ref_entity_t;
	typedef __datagram_impl_t::cref_entity_t cref_entity_t;

	typedef __datagram_impl_t::entities_vector_t entities_vector_t;
	typedef __datagram_impl_t::entities_type_map_t entities_type_map_t;

	typedef __datagram_impl_t::entity_const_iterator entity_const_iterator;
	typedef __datagram_impl_t::entity_iterator entity_iterator;

	bool empty()const;

	/// ����� ������ � ���������
	size_t count()const;

	/// ����� ������ � ���������, ��������� ����
	size_t count( const entity_t::type_type_t entityType )const;

	/// ����� ������ � ���������, ��������� ���� � �����
	size_t count( const entity_t::type_type_t entityType, const std::string &name )const;

	template<typename T,
		typename std::enable_if<!(srlz::is_serializable_v<T>), int>::type = 0>
		size_t count()const {
		static_assert(false, "type serializable is not supported");
		}

	/// ����� ������ � ���������, ��������� ����
	template<typename T,
		typename std::enable_if<srlz::is_serializable_v<T>, int>::type = 0>
		size_t count()const {
		if( _pdt )
			return _pdt->count<T>();
		else
			return 0;
		}

	template<typename T,
		typename std::enable_if<!(srlz::is_serializable_v<T>), int>::type = 0>
		size_t count( const std::string &name )const {
		static_assert(false, "type serializable is not supported");
		}

	/// ����� ������ � ���������, ��������� ���� � �����
	template<typename T,
		typename std::enable_if<srlz::is_serializable_v<T>, int>::type = 0>
		size_t count( const std::string &name )const {
		if( _pdt )
			return _pdt->count<T>( name );
		else
			return 0;
		}

	/// �������� ������� ��������� �� ���������������� �������������
	void create_from( const void *pDt );

	/// �������� ������� ��������� ��������� ����
	void create( const datagram_type_t type );

	/// �������� ����������� ���������
	void clear();

	explicit datagram_t( datagram_type_t type = 0 );
	explicit datagram_t( const void *pDt );
	explicit datagram_t( const std::vector<uint8_t> & dt );

	datagram_t( const datagram_t & dt ) = delete;
	datagram_t( datagram_t  && dt )noexcept;

	datagram_t& operator=(const datagram_t & dt) = delete;
	datagram_t& operator=(datagram_t && dt)noexcept;


	/*! �������� ���������������� ������������� ������� ��������� � ������� ���������� ����������
	\param [in] obj ��������� ����������
	\param [in] ppSerialData ����� ���������� ������ ������ �������� ��������������� ������������� ���������
	\param [in] createCRC ������� �������� ����������� �����
	*/
	template<typename TAllocator,
		typename std::enable_if<std::is_base_of<i_memory_services_t, TAllocator>::value, int>::type = 0 >
		void build( std::shared_ptr<TAllocator> allocator, void **ppSerialData,
		const char * traceid )const {

		if( _pdt )
			_pdt->build( allocator, ppSerialData, traceid );
		else
			THROW_EXC_FWD(nullptr);
		}

	template<typename TAllocator,
		typename std::enable_if<std::is_base_of<i_memory_services_t, TAllocator>::value, int>::type = 0 >
		void build( std::shared_ptr<TAllocator> allocator, void **ppSerialData )const {

		if( _pdt )
			_pdt->build( allocator, ppSerialData );
		else
			THROW_EXC_FWD(nullptr);
		}

	/*! �������� ���������������� ������������� ������� ��������� � �������� ������ */
	entity_t::bin_t build()const;

	/*! �������� ���������������� ������������� ������� ��������� � ������ */
	template<typename Ts>
	void build(Ts && dest)const{
		if (_pdt)
			_pdt->build(std::forward<Ts>(dest));
		else
			THROW_EXC_FWD(nullptr);
		}

	/// ���������� �������� ������������������ � ���������
	void add( const entity_t::type_type_t type,
		const void *pData, const size_t dataSize );

	/// ���������� �������� ������������������ � ���������
	void add( const entity_t::type_type_t type,
		const std::string &name, const void *pData, const size_t dataSize );

	template<typename Tsequence>
	void add( const entity_t::type_type_t type,
		const std::string &name, typename Tsequence && data ) {

		if( _pdt )
			_pdt->add( type, name, data );
		else
			THROW_EXC_FWD(nullptr);
		}

	template<typename TInIt>
	void add( const entity_t::type_type_t type,
		const std::string &name, typename TInIt first, typename TInIt last ) {

		if( _pdt )
			_pdt->add( type, name, first, last );
		else
			THROW_EXC_FWD(nullptr);
		}


	static entity_t::bin_t cpy( const void *dt );
	static size_t max_mem_size();

	/*! ������������ ������� � �������� ������ */
	template<typename Ts>
	void srlz(Ts && dest)const{
		if (_pdt)
			_pdt->srlz(std::forward<Ts>(dest));
		}

	/*! �������������� ������� �� ��������� ������� */
	template<typename Ts>
	void dsrlz(Ts && src){
		if (_pdt)
			_pdt->dsrlz(std::forward<Ts>(src));
		else{
			_pdt = std::make_unique<__datagram_impl_t>();
			_pdt->dsrlz(std::forward<Ts>(src));
			}
		}

	void deserialize(srlz::detail::rstream_base_handler & src) {
		dsrlz(src);
	}

	void serialize(srlz::detail::wstream_base_handler & dest)const {
		srlz(dest);
	}

	template<typename T,
		typename std::enable_if<srlz::is_serializable_v<T>, int>::type = 0>
		void add_value( const std::string &name, const typename T & value ) {

		if( _pdt )
			_pdt->add_value( name, value );
		else
			THROW_EXC_FWD(nullptr);
		}

	template<typename T,
		typename std::enable_if<(!srlz::is_serializable_v<T>), int>::type = 0>
	void add_value( const std::string &name, const typename T & value ) {
		static_assert(false, "type serializable is not supported");
		}

	void add( entity_const_iterator first, entity_const_iterator last );

	void add( const datagram_t & dtg );

	/// ����� ������� ����� ��������� ��������� ����
	ref_entity_t find( const entity_t::type_type_t type, const size_t index );

	cref_entity_t find( const entity_t::type_type_t type, const size_t index )const;

	/// ����� ������� ����� ��������� ��������� ���� � �����
	ref_entity_t find( const entity_t::type_type_t type,
		const std::string &name, const size_t index );

	cref_entity_t find( const entity_t::type_type_t type,
		const std::string &name, const size_t index )const;

	template<typename T, typename std::enable_if<!(srlz::is_serializable_v<T>), int>::type = 0>
	std::list<T> get_values( const std::string & name )const {
		static_assert(false, "type serializable is not supported");
		}

	template<typename T, typename std::enable_if<!(srlz::is_serializable_v<T>), int>::type = 0>
	std::list<T> get_values()const {
		static_assert(false, "type serializable is not supported");
		}

	/// ����� ������� ����� ��������� ��������� ���� � �����
	template<typename T, typename std::enable_if<!(srlz::is_serializable_v<T>), int>::type = 0>
	bool try_get_value( typename T & result, const std::string &name, const size_t index )const {
		static_assert(false, "type serializable is not supported");
		}

	/// ����� ������� ����� ��������� ��������� ���� � �����
	template<typename T, typename std::enable_if<(!srlz::is_serializable_v<T>), int>::type = 0>
	typename T get_value( const std::string &name, const size_t index )const {
		static_assert(false, "type serializable is not supported");
		}

	/// ����� ������� ����� ��������� ��������� ���� � �����
	template<typename T, typename std::enable_if<srlz::is_serializable_v<T>, int>::type = 0>
	bool try_get_value( typename T & result, const std::string &name, const size_t index )const {
		if( _pdt )
			return _pdt->try_get_value( result, name, index );
		else
			return false;
		}

	/// ����� ������� ����� ��������� ��������� ���� � �����
	template<typename T>
	bool try_get_value( typename T & result, const std::string &name )const {

		if( _pdt )
			return _pdt->try_get_value( result, name );
		else
			return false;
		}

	/// ����� ������� ����� ��������� ��������� ���� � �����
	template<typename T>
	bool try_get_value( typename T & result, const size_t index )const {

		if( _pdt )
			return _pdt->try_get_value( result, index );
		else
			return false;
		}

	/// ����� ������� ����� ��������� ��������� ���� � �����
	template<typename T>
	std::optional<T> get_value_2(const size_t index)const {

		if(_pdt)
			return _pdt->get_value_2<T>(index);
		else
			return std::nullopt;
		}

	/// ����� ������� ����� ���������
	template<typename T>
	bool try_get_value( typename T & result )const {

		if( _pdt )
			return _pdt->try_get_value( result );
		else
			return false;
		}

	/// ����� ������� ����� ��������� ��������� ���� � �����
	template<typename T, typename std::enable_if<srlz::is_serializable_v<T>, int>::type = 0>
	typename T get_value( const std::string &name, const size_t index )const {

		if( _pdt )
			return _pdt->get_value<T>( name, index );
		else
			THROW_EXC_FWD(nullptr);
		}

	/// ����� ������� ����� ��������� ��������� ���� � �����
	template<typename T>
	typename T get_value( const std::string &name )const {
		if( _pdt )
			return _pdt->get_value<T>( name );
		else
			THROW_EXC_FWD(nullptr);
		}

	/// ����� ������� ����� ��������� ��������� ���� � �����
	template<typename T>
	typename T get_value( const size_t index )const {
		if( _pdt )
			return _pdt->get_value<T>( index );
		else
			THROW_EXC_FWD(nullptr);
		}

	/// ����� ���� ������ ��������� ��������� ����
	template<typename T, typename std::enable_if<srlz::is_serializable_v<T>, int>::type = 0>
	typename std::list<T> get_values()const {

		if( _pdt )
			return _pdt->get_values<T>();
		else
			return std::list<T>();
		}

	/// ����� ���� ������ ��������� ��������� ����
	template<typename T, typename std::enable_if<srlz::is_serializable_v<T>, int>::type = 0>
	typename std::list<T> get_values( const std::string & name )const {

		if( _pdt )
			return _pdt->get_values<T>( name );
		else
			return std::list<T>();
		}
	};
}

