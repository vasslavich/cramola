#include "../../internal.h"
#include "../tableval_utility.h"


using namespace crm;
using namespace crm::detail;


/*=====================================================================
deallocator_with_functor_t
=======================================================================*/


void deallocator_with_functor_t::clear() {
	if( pDt ) {
		CBL_VERIFY( nullptr != _deallocator );
		_deallocator( pDt );
		pDt = nullptr;
		}
	}

void deallocator_with_functor_t::reset() {
	pDt = nullptr;
	}

deallocator_with_functor_t::deallocator_with_functor_t( deallocator_hndl_t da_ )
	: _deallocator( std::move( da_ ) ) {}

deallocator_with_functor_t::deallocator_with_functor_t( void *p, deallocator_hndl_t da_ )
	: pDt( p ) 
	, _deallocator( std::move( da_ ) ){}

deallocator_with_functor_t::~deallocator_with_functor_t() {
	clear();
	}


/*=====================================================================
memfree_with_i_services_t
=======================================================================*/


void memfree_with_i_services_t::allocate( size_t bytes, const char *traceid ) {
	if( !p ) {
		if( _srv ) {
			p = _srv->allocate( bytes, traceid );
			if( !p )
				THROW_EXC_FWD(nullptr);
			}
		else
			THROW_EXC_FWD(nullptr);
		}
	else {
		THROW_EXC_FWD(nullptr);
		}
	}

void memfree_with_i_services_t::allocate( size_t bytes ) {
	if( !p ) {
		if( _srv ) {
			p = _srv->allocate( bytes, nullptr );
			if( !p )
				THROW_EXC_FWD(nullptr);
			}
		else
			THROW_EXC_FWD(nullptr);
		}
	else {
		THROW_EXC_FWD(nullptr);
		}
	}

void memfree_with_i_services_t::deallocate() {
	if( p )
		_srv->free( p );
	p = nullptr;
	}

void memfree_with_i_services_t::clear() {
	deallocate();
	}

memfree_with_i_services_t::~memfree_with_i_services_t() {
	clear();
	}

