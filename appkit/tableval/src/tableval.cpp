#include <cassert>
#include "../../internal.h"
#include "../tableval.h"


using namespace crm;
using namespace crm::detail;


static_assert(srlz::detail::is_serialized_trait_v<datagram_t>, __FILE_LINE__);
static_assert(srlz::is_serializable_v<datagram_t>, __FILE_LINE__);


namespace {
auto checkvalidate_0() {
	auto ws = srlz::wstream_constructor_t::make();
	srlz::serialize(ws, datagram_t{});
	return ws.release();
	}
static_assert(srlz::detail::is_container_of_bytes_v<decltype(checkvalidate_0())>, __FILE_LINE__);
}

const datagram_entry_t::bin_t& datagram_entry_t::binary()const {
	return _data;
	}

datagram_entry_t::type_type_t datagram_entry_t::utype()const {
	return _utype;
	}

const std::string& datagram_entry_t::name()const {
	return _name;
	}

bool datagram_entry_t::is_empty()const {
	return _data.empty();
	}

size_t datagram_entry_t::packed_size()const {
	return crm::srlz::write_size_v<decltype(_utype)>
		+ crm::srlz::object_trait_serialized_size( _name )
		+ crm::srlz::object_trait_serialized_size( _data );
	}

datagram_entry_t::datagram_entry_t(const type_type_t type_,
	const std::string &name, const void *pData, const size_t bCnt )
	: _utype( type_ )
	, _name( name )
	, _data( (const uint8_t*)pData, (const uint8_t*)pData + bCnt ) {}

datagram_entry_t::datagram_entry_t() {}


const datagram_entry_t datagram_entry_t::null;


/*----------------------------------------------------------
__datagram_impl_t
--------------------------------------------------------------*/


const __datagram_impl_t::length_type_t __datagram_impl_t::build_class_length = srlz::write_size_v<build_class_t>;
const __datagram_impl_t::length_type_t __datagram_impl_t::type_length = srlz::write_size_v<entity_t::type_type_t>;
const __datagram_impl_t::length_type_t __datagram_impl_t::type_offset = build_class_length;
const __datagram_impl_t::length_type_t __datagram_impl_t::size_length = srlz::write_size_v<length_type_t>;
const __datagram_impl_t::length_type_t __datagram_impl_t::size_offset = (type_offset + type_length);
const __datagram_impl_t::length_type_t __datagram_impl_t::prefix_length = (build_class_length + type_length + size_length);


const __datagram_impl_t __datagram_impl_t::NULL_DATAGRAM;

const __datagram_impl_t::entity_t::bin_t __datagram_impl_t::binary_blocks_prefix = {0x3C, 0x3C, 0x3C, 0x3C, 0x7E, 0x7E, 0x7E, 0x7E, 0x11, 0x22, 0x33, 0x44};
const __datagram_impl_t::entity_t::bin_t __datagram_impl_t::binary_blocks_postfix = {0x7C, 0x7C, 0x7C, 0x7C, 0x3E, 0x3E, 0x3E, 0x3E, 0x44, 0x33, 0x22, 0x11};

size_t __datagram_impl_t::binary_blocks_prefix_size() {
	return crm::srlz::object_trait_serialized_size( binary_blocks_prefix );
	}

size_t __datagram_impl_t::binary_blocks_postfix_size() {
	return crm::srlz::object_trait_serialized_size( binary_blocks_postfix );
	}

__datagram_impl_t::entity_t::bin_t __datagram_impl_t::cpy( const void *ptr ) {
	if( 0 == ptr )
		SERIALIZE_THROW_EXC_FWD(nullptr);

	// ������
	size_t size = 0;

	auto pb0 = (const uint8_t*)ptr + size_offset;
	auto length = prefix_length - size_offset;
	auto sr = crm::srlz::rstream_constructor_t::make( pb0, pb0 + length );

	// �����
	srlz::deserialize( sr, size );
	if( size > max_mem_size() )
		SERIALIZE_THROW_EXC_FWD( L"datagram: bad binary length" );

	/* ���������� ���� ���������� */
	std::vector<uint8_t> bin( (const uint8_t*)ptr, (const uint8_t*)ptr + prefix_length + size );
	return std::move( bin );
	}

bool cmp( const __datagram_impl_t::entity_t::bin_t& l, const __datagram_impl_t::entity_t::bin_t& r ) {
	if( l.size() != r.size() )
		return false;

	for( size_t i = 0; i < l.size(); ++i )
		if( l.at( i ) != r.at( i ) )
			return false;

	return true;
	}

bool __datagram_impl_t::validate_blocks_prefix( const entity_t::bin_t& s ) {
	return cmp( s, binary_blocks_prefix );
	}

bool __datagram_impl_t::validate_blocks_postfix( const entity_t::bin_t& s ) {
	return cmp( s, binary_blocks_postfix );
	}

size_t __datagram_impl_t::max_mem_size() {
	return std::numeric_limits<std::uint32_t>::max();
	}

size_t __datagram_impl_t::size_packed()const{
	return prefix_length + datagram_length();
	}

size_t __datagram_impl_t::datagram_length()const {

#ifdef CBL_USE_DATAGRAM_VALIDATE_PREFIX
	return _sizeBlocks + binary_blocks_prefix_size() + binary_blocks_postfix_size();
#else
	return _sizeBlocks;
#endif
	}

__datagram_impl_t::datagram_type_t __datagram_impl_t::type()const{
    return _dtgType; }

bool __datagram_impl_t::empty()const {
	return _entitiesMap.empty();
	}

__datagram_impl_t::cref_entity_t __datagram_impl_t::__find(entity_t::type_type_t type,
	const size_t index)const {

    /* ������ ������ ������� ���� */
    entities_type_map_t::const_iterator itType = _entitiesMap.find( type );
	if( itType == _entitiesMap.end() )
		return nullptr;

    /* � ������ ���������� ������ */
	if( index >= itType->second.size() )
		return nullptr;

    return itType->second[ index ];
    }

__datagram_impl_t::cref_entity_t __datagram_impl_t::find( entity_t::type_type_t type,
	const size_t index )const {

	return __find( type, index );
	}

__datagram_impl_t::cref_entity_t __datagram_impl_t::__find(
	entity_t::type_type_t type, 
	const std::string &name,
	const size_t index)const {

	/* ������ ������ ������� ���� */
	entities_type_map_t::const_iterator itType = _entitiesMap.find( type );
	if( itType == _entitiesMap.end() )
		return nullptr;

	/* � ������ ���������� ������ */
	if( index >= itType->second.size() )
		return nullptr;

	size_t findNum = 0;
	for( auto it : itType->second ) {
		if( it->name() == name && index == findNum++ )
			return it;
		}

	return nullptr;
	}

__datagram_impl_t::cref_entity_t __datagram_impl_t::find(
	entity_t::type_type_t type,
	const std::string &name, 
	const size_t index )const {

	if( !name.empty() )
		return __find( type, name, index );
	else
		return __find( type, index );
	}

__datagram_impl_t::ref_entity_t __datagram_impl_t::__find( entity_t::type_type_t type, const size_t index ) {

	/* ������ ������ ������� ���� */
	entities_type_map_t::iterator itType = _entitiesMap.find( type );
	if( itType == _entitiesMap.end() )
		return nullptr;

	/* � ������ ���������� ������ */
	if( index >= itType->second.size() )
		return nullptr;

	return itType->second[index];
	}

__datagram_impl_t::ref_entity_t __datagram_impl_t::find( entity_t::type_type_t type,
	const size_t index ) {

	return __find( type, index );
	}

__datagram_impl_t::ref_entity_t __datagram_impl_t::__find( entity_t::type_type_t type, 
	const std::string &name,
	const size_t index ) {

	/* ������ ������ ������� ���� */
	entities_type_map_t::iterator itType = _entitiesMap.find( type );
	if( itType == _entitiesMap.end() )
		return nullptr;

	/* � ������ ���������� ������ */
	if( index >= itType->second.size() )
		return nullptr;

	size_t findNum = 0;
	for( auto it : itType->second ) {
		if( it->name() == name && index == findNum++ )
			return it;
		}

	return nullptr;
	}


__datagram_impl_t::ref_entity_t __datagram_impl_t::find( entity_t::type_type_t type,
	const std::string &name, 
	const size_t index ) {

	if( !name.empty() )
		return __find( type, name, index );
	else
		return __find( type, index );
	}

__datagram_impl_t::entity_iterator __datagram_impl_t::get_begin() {
	return _fullLst.begin();
	}

__datagram_impl_t::entity_const_iterator __datagram_impl_t::get_begin()const {
	return _fullLst.cbegin();
	}

__datagram_impl_t::entity_iterator __datagram_impl_t::get_end() {
	return _fullLst.end();
	}

__datagram_impl_t::entity_const_iterator __datagram_impl_t::get_end()const {
	return _fullLst.cend();
	}

__datagram_impl_t::entity_iterator __datagram_impl_t::get_begin(const entity_t::type_type_t type) {
	/* ������ ������ ������� ���� */
	entities_type_map_t::iterator itType = _entitiesMap.find( type );
	if( itType == _entitiesMap.end() )
		return _nullList.begin();

	return itType->second.begin();
	}

__datagram_impl_t::entity_const_iterator __datagram_impl_t::get_begin(const entity_t::type_type_t type)const {
	/* ������ ������ ������� ���� */
	entities_type_map_t::const_iterator itType = _entitiesMap.find( type );
	if( itType == _entitiesMap.end() )
		return _nullList.begin();

	return itType->second.begin();
	}

__datagram_impl_t::entity_iterator __datagram_impl_t::get_end(const entity_t::type_type_t type) {
	/* ������ ������ ������� ���� */
	entities_type_map_t::iterator itType = _entitiesMap.find( type );
	if( itType == _entitiesMap.end() )
		return _nullList.end();

	return itType->second.end();
	}

__datagram_impl_t::entity_const_iterator __datagram_impl_t::get_end(const entity_t::type_type_t type)const {
	/* ������ ������ ������� ���� */
	entities_type_map_t::const_iterator itType = _entitiesMap.find( type );
	if( itType == _entitiesMap.end() )
		return _nullList.end();

	return itType->second.end();
	}

size_t __datagram_impl_t::count(const entity_t::type_type_t type)const {
        
    /* ������ ������ ������� ���� */
    entities_type_map_t::const_iterator itType = _entitiesMap.find( type );
    if( itType == _entitiesMap.end() )return 0;

    /* � ������ ���������� ������ */
    return itType->second.size();
    }

size_t __datagram_impl_t::count()const {
	return _entitiesMap.size();
	}

size_t __datagram_impl_t::count(const entity_t::type_type_t type, const std::string &name)const {

	/* ������ ������ ������� ���� */
	entities_type_map_t::const_iterator itType = _entitiesMap.find( type );
	if( itType == _entitiesMap.end() )return 0;

	/* � ������ ���������� ������ */
	size_t cnt = 0;
	for( auto it : itType->second )
		if( it->name() == name )++cnt;

	return cnt;
	}

void __datagram_impl_t::add( const __datagram_impl_t & dtg ) {
	add( dtg.get_begin(), dtg.get_end() );
	}

void __datagram_impl_t::add( entity_const_iterator first, entity_const_iterator last ) {
	for( ; first != last; ++first ) {
		add(std::make_shared<entity_t>(*(*first)));
		}
	}

void __datagram_impl_t::add( std::shared_ptr<entity_t> && entr ) {
	if( entr ) {
		const auto type = entr->utype();
		const auto lpckd = entr->packed_size();

		auto lnk2( entr );

		/* ������ ������ ������� ���� */
		_entitiesMap[type].emplace_back( std::move(entr) );
		_fullLst.push_back( std::move( lnk2 ) );

		/* ������ ���������������� ����� */
		_sizeBlocks += lpckd;
		}
	}

void __datagram_impl_t::add(const entity_t::type_type_t type,
	const void *pData, const size_t dataSize ) {
	add( type, "", pData, dataSize );
	}

void __datagram_impl_t::add( const entity_t::type_type_t type,
	const std::string &name, const void *pData, const size_t dataSize) {

	std::shared_ptr<entity_t> entry( new entity_t( type, name, pData, dataSize ) );
	add( std::move( entry ) );
    }


__datagram_impl_t::entity_t::bin_t __datagram_impl_t::build()const {
	auto sout( crm::srlz::wstream_constructor_t::make() );
	build( sout );

#ifdef CBL_DATAGRAM_CHECK_SRLZLAYOUT
	/* ��������
	----------------------------------------------------*/
	if( sout.ready() != size_packed() )
		SERIALIZE_THROW_EXC_FWD( L"datagram: bad layout" );
#endif

	return std::move( sout.release() );
    }


void __datagram_impl_t::create( const datagram_type_t type ) {

	clear();

	_dtgType = type;
	_sizeBlocks = 0;
	}


void __datagram_impl_t::create_from( const void *ptr ) {

	if( ptr ) {

		clear();

		/* ������������ ��������� �� ���������� ��������� �����
		----------------------------------------------------------------*/
		const uint8_t *pb = (uint8_t*)ptr;
		auto sr = crm::srlz::rstream_constructor_t::make( pb, pb + prefix_length );

		/* ����� ����������
		--------------------------------------------------------------*/
		auto buildCl = build_class_t::undefined;
		srlz::deserialize( sr, buildCl );
		if( buildCl <= build_class_t::undefined || buildCl >= build_class_t::upper_bound )
			SERIALIZE_THROW_EXC_FWD( L"datagram: bad build_class_t" );

		/* ���
		---------------------------------------------------------------*/
		srlz::deserialize( sr, _dtgType );

		/* �����
		---------------------------------------------------------------*/
		size_t varsize = 0;
		srlz::deserialize( sr, varsize );

		if( varsize >= max_mem_size() )
			SERIALIZE_THROW_EXC_FWD( "datagram: bad length:" + std::to_string(varsize) + ":" + std::to_string(max_mem_size()) );

		/* ������������� ��������� ������ ������
		-----------------------------------------------------------------*/
		if( sr.offset() != prefix_length )
			SERIALIZE_THROW_EXC_FWD( L"datagram: bad length" );

		auto pb1 = pb + prefix_length;
		sr = crm::srlz::rstream_constructor_t::make( pb1, pb1 + varsize );

		/* �������������� ����������� �������
		-----------------------------------------------------------------*/
		if( buildCl == build_class_t::plane_with_validate ) {
			entity_t::bin_t prefix;
			srlz::deserialize( sr, prefix );
			if( !validate_blocks_prefix( prefix ) ) {
				SERIALIZE_THROW_EXC_FWD( L"datagram: bad prefix" );
				}
			}

		/* ���������� ����� ������
		--------------------------------------*/
		size_t countb = sr.length();
		if( datagram_t::not_available_value == countb ){
			SERIALIZE_THROW_EXC_FWD( L"datagram: bad stream type" );
			}

		if( buildCl == build_class_t::plane_with_validate ) {
			if( countb < binary_blocks_postfix_size() )
				SERIALIZE_THROW_EXC_FWD( L"datagram: bad prefix" );
			countb -= binary_blocks_postfix_size();
			}

		while( sr.offset() < countb ) {

			entity_t::type_type_t typeBlock{ 0 };
			std::string name;

			/* ��� ����� */
			srlz::deserialize( sr, typeBlock );

			/* ��� ����� */
			srlz::deserialize( sr, name );

			/* ����� ������ */
			entity_t::bin_t buf;
			srlz::deserialize( sr, buf );

			// �������� � ���������
			add( typeBlock, name, std::move( buf ) );
			}

		/* �������������� ����������� ��������
		-----------------------------------------------------------------*/
		if( buildCl == build_class_t::plane_with_validate ) {
			entity_t::bin_t postfix;
			srlz::deserialize( sr, postfix );
			if( !validate_blocks_postfix( postfix ) ) {
				SERIALIZE_THROW_EXC_FWD( L"datagram: bad postfix" );
				}
			}
		}
	else {
		SERIALIZE_THROW_EXC_FWD( L"datagram: null pointer" );
		}
	}

void __datagram_impl_t::clear(){
    _entitiesMap.clear();
	_fullLst.clear();

    _dtgType = 0;
    _sizeBlocks = 0;
    }

__datagram_impl_t::~__datagram_impl_t(){
    clear();
    }

__datagram_impl_t::__datagram_impl_t( datagram_type_t type )
	: _dtgType( type ) {}

__datagram_impl_t::__datagram_impl_t( const void *pDt ) {
	create_from( pDt );
	}

__datagram_impl_t::__datagram_impl_t( const std::vector<uint8_t> & dt )
	: __datagram_impl_t( dt.empty() ? nullptr : dt.data() ) {}


/* ===========================================================================
datagram_t
==============================================================================*/


bool datagram_t::empty()const {
	if( _pdt )
		return _pdt->empty();
	else
		return true;
	}

size_t datagram_t::count()const {
	if( _pdt )
		return _pdt->count();
	else
		return 0;
	}

size_t datagram_t::count( const entity_t::type_type_t entityType )const {
	if( _pdt )
		return _pdt->count( entityType );
	else
		return 0;
	}

size_t datagram_t::count( const entity_t::type_type_t entityType, const std::string &name )const {
	if( _pdt )
		return _pdt->count( entityType, name );
	else
		return 0;
	}

void datagram_t::create_from( const void *pDt ) {
	_pdt = std::make_unique<__datagram_impl_t>();
	_pdt->create_from( pDt );
	}

void datagram_t::create( const datagram_type_t type ) {
	_pdt = std::make_unique<__datagram_impl_t>();
	_pdt->create( type );
	}

void datagram_t::clear() {
	if( _pdt )
		_pdt->clear();
	}

datagram_t::datagram_t( datagram_type_t type )
	: _pdt( std::make_unique<__datagram_impl_t>( type ) ) {}

datagram_t::datagram_t( const void *pDt )
	: _pdt( std::make_unique<__datagram_impl_t>( pDt ) ) {}

datagram_t::datagram_t( datagram_t  && dt )noexcept
	: _pdt( std::move( dt._pdt ) ) {}

datagram_t& datagram_t::operator=(datagram_t && dt)noexcept {
	_pdt = std::move( dt._pdt );
	return (*this);
	}

datagram_t::datagram_t( const std::vector<uint8_t> & dt )
	: _pdt( std::make_unique<__datagram_impl_t>( dt ) ) {}

datagram_t::entity_t::bin_t datagram_t::build()const {
	if( _pdt )
		return _pdt->build();
	else
		return datagram_t::entity_t::bin_t();
	}

void datagram_t::add( const entity_t::type_type_t type,
	const void *pData, const size_t dataSize ) {

	if( !_pdt )
		_pdt = std::make_unique<__datagram_impl_t>();
	_pdt->add( type, pData, dataSize );
	}

void datagram_t::add( const entity_t::type_type_t type,
	const std::string &name, const void *pData, const size_t dataSize ) {

	if( !_pdt )
		_pdt = std::make_unique<__datagram_impl_t>();
	_pdt->add( type, name, pData, dataSize );
	}

datagram_t::entity_t::bin_t datagram_t::cpy( const void *dt ) {
	return __datagram_impl_t::cpy( dt );
	}

size_t datagram_t::max_mem_size() {
	return __datagram_impl_t::max_mem_size();
	}

void datagram_t::add( entity_const_iterator first, entity_const_iterator last ) {
	if( !_pdt )
		_pdt = std::make_unique<__datagram_impl_t>();
	_pdt->add( first, last );
	}

void datagram_t::add( const datagram_t & dtg ) {
	if( !_pdt )
		_pdt = std::make_unique<__datagram_impl_t>();

	if( dtg._pdt )
		_pdt->add( (*dtg._pdt) );
	}

datagram_t::ref_entity_t datagram_t::find( const entity_t::type_type_t type, const size_t index ) {
	if( _pdt )
		return _pdt->find( type, index );
	else
		return datagram_t::ref_entity_t();
	}

datagram_t::cref_entity_t datagram_t::find( const entity_t::type_type_t type, const size_t index )const {
	if( _pdt )
		return _pdt->find( type, index );
	else
		return datagram_t::cref_entity_t();
	}

datagram_t::ref_entity_t datagram_t::find( const entity_t::type_type_t type,
	const std::string &name, const size_t index ) {
	if( _pdt )
		return _pdt->find( type, name, index );
	else
		return datagram_t::ref_entity_t();
	}

datagram_t::cref_entity_t datagram_t::find( const entity_t::type_type_t type,
	const std::string &name, const size_t index )const {
	if( _pdt )
		return _pdt->find( type, name, index );
	else
		return datagram_t::cref_entity_t();
	}
