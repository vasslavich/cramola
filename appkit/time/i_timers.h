#pragma once


#include <chrono>
#include "../internal_invariants.h"
#include "../functorapp/functorapp.h"
#include "../logging/trace_terms.h"

namespace crm {



struct i_xtimer_base_t {
	virtual ~i_xtimer_base_t() = 0;

	virtual void stop()noexcept = 0;
	virtual async_space_t space()const noexcept = 0;
	virtual bool is_alived()const noexcept = 0;

	struct make_fault_result_t {
		make_fault_result_t()noexcept;
		async_operation_result_t operator()(std::string&&)noexcept;
		};
	};


struct i_xtimer_t : public i_xtimer_base_t {
	class timer_out_t {
		using function_handler_type = crm::utility::invoke_functor_shared<bool/* ������ */, async_operation_result_t>;

		function_handler_type _ph;

	public:
		timer_out_t()noexcept;

		template<typename TX,
			typename std::enable_if_t<!std::is_base_of_v<timer_out_t, std::decay_t<TX>> && std::is_invocable_r_v<bool, TX, async_operation_result_t>, int> = 0 >
			timer_out_t(TX && h_ )
			: _ph(std::forward<TX>(h_)) {}

		bool operator()(async_operation_result_t r);
		operator bool()const noexcept;
		};

	virtual void start(std::function<void()> && first,
		timer_out_t && timerOut,
		const std::chrono::milliseconds & interval,
		bool nextIfExc = true) = 0;

	virtual void start(timer_out_t && timerOut,
		const std::chrono::milliseconds & interval,
		bool nextIfExc = true) = 0;

	virtual void start(std::function<void()> && first,
		timer_out_t && timerOut,
		bool nextIfExc = true) = 0;

	virtual void start(timer_out_t && timerOut,
		bool nextIfExc = true) = 0;

	virtual bool canceled()const noexcept = 0;
	virtual std::chrono::milliseconds interval()const noexcept = 0;
	};


struct i_xtimer_once_called_t : public i_xtimer_base_t {
	class timer_out_t {
		using function_handler_type = crm::utility::invoke_functor<void, async_operation_result_t>;

		function_handler_type _ph;

	public:
		timer_out_t()noexcept;

		template<typename TX,
			typename std::enable_if_t<!std::is_base_of_v<timer_out_t, std::decay_t<TX>> && std::is_invocable_v<TX, async_operation_result_t>, int> = 0 >
			timer_out_t(TX && h_)
			: _ph(std::forward<TX>(h_)) {}

		void operator()(async_operation_result_t r);
		operator bool()const noexcept;
		};
	};


struct i_xtimer_oncecall_t : public i_xtimer_once_called_t {
	virtual void start(timer_out_t && timerOut, const std::chrono::milliseconds & timeout) = 0;
	virtual std::chrono::system_clock::time_point deadline_point()const noexcept = 0;
	};

struct i_xdeffered_action_timer_t : public i_xtimer_once_called_t {
	virtual invoke_result_t invoke(async_operation_result_t) = 0;
	virtual std::chrono::system_clock::time_point deadline_point()const noexcept = 0;
	};

struct i_timers_factory_t {
	virtual ~i_timers_factory_t() = 0;

	virtual std::unique_ptr<i_xtimer_oncecall_t> create_timer_oncecall() = 0;
	virtual std::unique_ptr<i_xtimer_oncecall_t> create_timer_oncecall(
		i_xtimer_oncecall_t::timer_out_t && hndl,
		const std::chrono::milliseconds & timeout) = 0;
	virtual std::unique_ptr<i_xtimer_t> create_timer(std::chrono::milliseconds interval) = 0;
	virtual std::unique_ptr<i_xtimer_t> create_timer() = 0;
	virtual std::unique_ptr<i_xdeffered_action_timer_t> create_timer_deffered_action(
		trace_tag &&,
		i_xdeffered_action_timer_t::timer_out_t && hndl,
		const std::chrono::milliseconds & timeout) = 0;
	};
}


