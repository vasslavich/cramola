#pragma once


#include <functional>
#include <chrono>
#include <atomic>
#include <memory>
#include "../base_predecl.h"
#include "./i_timers.h"
#include "../internal.h"


namespace crm{




namespace detail {




class timer_oncecall_impl_t;
class timer_impl_t;
class deffered_action_timer_impl_t;

class __xtimer_oncecall_t : public i_xtimer_oncecall_t {
private:
	std::shared_ptr<timer_oncecall_impl_t> _impl;
	
	invoke_result_t invoke(async_operation_result_t)noexcept;

public:
	__xtimer_oncecall_t( const __xtimer_oncecall_t& ) = delete;
	__xtimer_oncecall_t& operator=(const __xtimer_oncecall_t&) = delete;

	__xtimer_oncecall_t( __xtimer_oncecall_t && )noexcept;
	__xtimer_oncecall_t& operator=(__xtimer_oncecall_t &&)noexcept;

	__xtimer_oncecall_t( std::weak_ptr<distributed_ctx_t> ctx, async_space_t aspc );
	__xtimer_oncecall_t( std::weak_ptr<distributed_ctx_t> ctx_,
		timer_out_t && hndl_,
		const std::chrono::milliseconds & timeout,
		async_space_t aspc );

	~__xtimer_oncecall_t();

	void start( timer_out_t && timerOut, const std::chrono::milliseconds & timeout ) final;
	void stop()noexcept final;
	bool is_alived()const noexcept final;
	bool canceled()const noexcept;
	std::chrono::system_clock::time_point deadline_point()const noexcept final;
	async_space_t space()const noexcept final;
	};


class __xtimer_t : public i_xtimer_t {
private:
	std::shared_ptr<timer_impl_t> _impl;

public:
	__xtimer_t( const __xtimer_t& ) = delete;
	__xtimer_t& operator=(const __xtimer_t&) = delete;

	__xtimer_t( __xtimer_t  && )noexcept;
	__xtimer_t& operator=(__xtimer_t &&)noexcept;

	__xtimer_t( std::weak_ptr<distributed_ctx_t> ctx, async_space_t aspc );
	__xtimer_t( std::weak_ptr<distributed_ctx_t> ctx, std::chrono::milliseconds interval, async_space_t aspc );

	~__xtimer_t();

	void start( std::function<void()> && first,
		timer_out_t && timerOut,
		const std::chrono::milliseconds & interval,
		bool nextIfExc = true)final;

	void start( timer_out_t && timerOut,
		const std::chrono::milliseconds & interval,
		bool nextIfExc = true)final;

	void start(std::function<void()> && first, 
		timer_out_t && timerOut,
		bool nextIfExc = true)final;

	void start( timer_out_t && timerOut,
		bool nextIfExc = true)final;

	void stop()noexcept final;
	bool is_alived()const noexcept final;
	bool canceled()const noexcept;
	std::chrono::milliseconds interval()const noexcept final;
	async_space_t space()const noexcept final;
	};


class __xdeffered_action_timer_t : public i_xdeffered_action_timer_t {
private:
	std::shared_ptr<deffered_action_timer_impl_t> _impl;

public:
	__xdeffered_action_timer_t( __xdeffered_action_timer_t && o )noexcept;
	__xdeffered_action_timer_t& operator=(__xdeffered_action_timer_t && o)noexcept;

	__xdeffered_action_timer_t( const __xdeffered_action_timer_t& ) = delete;
	__xdeffered_action_timer_t& operator=(const __xdeffered_action_timer_t&) = delete;

	__xdeffered_action_timer_t( std::weak_ptr<distributed_ctx_t> ctx_,
		trace_tag&& tag,
		timer_out_t && hndl_,
		const std::chrono::milliseconds & timeout,
		async_space_t aspc );

	~__xdeffered_action_timer_t();

	void stop()noexcept final;
	bool is_alived()const noexcept final;
	bool canceled()const noexcept;
	async_space_t space()const noexcept final;
	invoke_result_t invoke(async_operation_result_t)noexcept final;
	std::chrono::system_clock::time_point deadline_point()const noexcept final;

	static void launch( std::weak_ptr<distributed_ctx_t> ctx_,
		trace_tag && tag_,
		timer_out_t && hndl_,
		const std::chrono::milliseconds & timeout,
		async_space_t aspc );
	};

class timer_oncecall_sys_t : public __xtimer_oncecall_t {
public:
	timer_oncecall_sys_t( const timer_oncecall_sys_t& ) = delete;
	timer_oncecall_sys_t& operator=(const timer_oncecall_sys_t&) = delete;


	timer_oncecall_sys_t( timer_oncecall_sys_t  && )noexcept;
	timer_oncecall_sys_t& operator=(timer_oncecall_sys_t &&)noexcept;

	timer_oncecall_sys_t( std::weak_ptr<distributed_ctx_t> ctx );
	timer_oncecall_sys_t(std::weak_ptr<distributed_ctx_t> ctx_,
		timer_out_t && hndl_,
		const std::chrono::milliseconds & timeout);
	};

class timer_sys_t : public __xtimer_t {
public:
	timer_sys_t( const timer_sys_t& ) = delete;
	timer_sys_t& operator=(const timer_sys_t&) = delete;

	timer_sys_t( timer_sys_t  && )noexcept;
	timer_sys_t& operator=(timer_sys_t &&)noexcept;

	timer_sys_t( std::weak_ptr<distributed_ctx_t> ctx );
	timer_sys_t( std::weak_ptr<distributed_ctx_t> ctx, std::chrono::milliseconds interval );
	};

class deffered_action_timer_sys_t : public __xdeffered_action_timer_t {
public:
	deffered_action_timer_sys_t( deffered_action_timer_sys_t && o )noexcept;
	deffered_action_timer_sys_t& operator=(deffered_action_timer_sys_t && o)noexcept;

	deffered_action_timer_sys_t( const deffered_action_timer_sys_t& ) = delete;
	deffered_action_timer_sys_t& operator=(const deffered_action_timer_sys_t&) = delete;

	deffered_action_timer_sys_t( std::weak_ptr<distributed_ctx_t> ctx_,
		trace_tag&& tag,
		timer_out_t && hndl_,
		const std::chrono::milliseconds & timeout );

	static void launch( std::weak_ptr<distributed_ctx_t> ctx_,
		trace_tag&& tag,
		timer_out_t && hndl_,
		const std::chrono::milliseconds & timeout );
	};
}


class timer_oncecall_t : public detail::__xtimer_oncecall_t {
public:
	timer_oncecall_t( const timer_oncecall_t& ) = delete;
	timer_oncecall_t& operator=(const timer_oncecall_t&) = delete;

	timer_oncecall_t( timer_oncecall_t  && )noexcept;
	timer_oncecall_t& operator=(timer_oncecall_t &&)noexcept;

	timer_oncecall_t( std::weak_ptr<distributed_ctx_t> ctx );
	timer_oncecall_t( std::weak_ptr<distributed_ctx_t> ctx_,
		timer_out_t && hndl_,
		const std::chrono::milliseconds & timeout );
	};

class timer_t : public detail::__xtimer_t {
public:
	timer_t( const timer_t& ) = delete;
	timer_t& operator=(const timer_t&) = delete;

	timer_t( timer_t  && )noexcept;
	timer_t& operator=(timer_t &&)noexcept;

	timer_t( std::weak_ptr<distributed_ctx_t> ctx );
	timer_t( std::weak_ptr<distributed_ctx_t> ctx, std::chrono::milliseconds interval );
	};

class deffered_action_timer_t : public detail::__xdeffered_action_timer_t {
public:
	deffered_action_timer_t( deffered_action_timer_t && o )noexcept;
	deffered_action_timer_t& operator=(deffered_action_timer_t && o)noexcept;

	deffered_action_timer_t( const deffered_action_timer_t& ) = delete;
	deffered_action_timer_t& operator=(const deffered_action_timer_t&) = delete;

	deffered_action_timer_t( std::weak_ptr<distributed_ctx_t> ctx_,
		trace_tag && tag,
		timer_out_t && hndl_,
		const std::chrono::milliseconds & timeout );

	static void launch(std::weak_ptr<distributed_ctx_t> ctx_,
		trace_tag&& tag,
		timer_out_t&& hndl_,
		const std::chrono::milliseconds& timeout);
	};
}

