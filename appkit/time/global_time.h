#pragma once


#include <chrono>
#include <string>


namespace crm {


struct distributed_time_t {
public:
	typedef std::chrono::system_clock::time_point timepoint_t;

private:
	timepoint_t _tp = std::chrono::system_clock::now();

public:
	const timepoint_t& timepoint()const;
	std::string to_str()const;
	};
}

