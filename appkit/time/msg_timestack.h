#pragma once


#include <chrono>
#include <array>
#include "../internal.h"


namespace crm::detail{


class hanlder_times_stack_t final {
public:
	enum class stage_t {
		create,
		rt1_sender,
		rt0_sender,
		serialize,
		packet_capture,
		deserialize_from_packet,
		rt0_receiver,
		rt1_receiver_router,
		rt1_receiver,
		__COUNT__
		};

	typedef std::chrono::system_clock::time_point time_point_t;

	struct clocks_t {
		std::array<std::chrono::milliseconds, (size_t)stage_t::__COUNT__> clocks;
		};

private:
	typedef std::array<time_point_t, (size_t)stage_t::__COUNT__> time_line_t;
	time_line_t _line;

	/*! ������������ ������� � �������� ������ */
	template<typename S>
	void srlz(S && dest)const {
		std::vector<streamed_time_t> line(_line.size());

		for (size_t i = 0; i < _line.size(); ++i) {
			line[i] = _line[i].time_since_epoch().count();
		}

		crm::srlz::serialize(dest, line);
	}

	/*! �������������� ������� �� ��������� ������� */
	template<typename S>
	void dsrlz(S && src ) {

		std::vector<streamed_time_t> line;
		crm::srlz::deserialize(src, line);

		if (line.size() != _line.size())
			FATAL_ERROR_FWD(nullptr);

		for (size_t i = 0; i < _line.size(); ++i) {

			std::chrono::system_clock::time_point tp;
			tp += std::chrono::system_clock::duration(line[i]);

			_line.at(i) = tp;
		}
	}

	/*! ���������� ����� ����, ���������� �������� � ��������������� �������������.
	������ ��� ������ length_info() == length_info_t::prefixed, ��� ��������� �������
	�������� � ��������� ���������� */
	std::size_t get_serialized_size()const noexcept;

public:
	hanlder_times_stack_t();

	hanlder_times_stack_t( const hanlder_times_stack_t & );
	hanlder_times_stack_t& operator=(const hanlder_times_stack_t &);

	hanlder_times_stack_t( hanlder_times_stack_t && );
	hanlder_times_stack_t& operator=(hanlder_times_stack_t &&);

	time_point_t& operator[]( stage_t stIndex );
	const time_point_t& operator[]( stage_t stIndex )const;

	clocks_t clocks()const;
	void clear();
	};
}

