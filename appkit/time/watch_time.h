#pragma once


#include "../internal_invariants.h"
#include <chrono>


namespace crm{


class stop_watcher_t {
private:
	std::chrono::high_resolution_clock::time_point _startTP;

public:
	typedef uint64_t counter_t;

	stop_watcher_t();
	void start();

	std::chrono::high_resolution_clock::duration now()const;
	counter_t now_millisec()const;
	counter_t now_microsec()const;
	};
}

