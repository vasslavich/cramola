#include "../../internal.h"


using namespace crm;
using namespace crm::detail;


i_xtimer_base_t:: ~i_xtimer_base_t() {}



i_xtimer_base_t::make_fault_result_t::make_fault_result_t()noexcept {}

async_operation_result_t i_xtimer_base_t::make_fault_result_t::operator()(std::string&&)noexcept {
	return async_operation_result_t::st_abandoned;
	}




i_xtimer_t::timer_out_t::timer_out_t()noexcept {}

bool i_xtimer_t::timer_out_t::operator()(async_operation_result_t r) {
	if (_ph) {
		return _ph(r);
		}
	else {
		THROW_EXC_FWD(nullptr);
		}
	}

i_xtimer_t::timer_out_t::operator bool()const noexcept {
	return _ph;
	}




i_xtimer_once_called_t::timer_out_t::timer_out_t()noexcept {}

void i_xtimer_once_called_t::timer_out_t::operator()(async_operation_result_t r) {
	if (_ph) {
		_ph(r);
		}
	else {
		THROW_EXC_FWD(nullptr);
		}
	}

i_xtimer_once_called_t::timer_out_t::operator bool()const noexcept {
	return _ph;
	}




i_timers_factory_t:: ~i_timers_factory_t() {}

