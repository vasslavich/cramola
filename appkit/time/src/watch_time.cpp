#include "../../internal.h"
#include "../watch_time.h"
#include "../../exceptions.h"


using namespace crm;
using namespace crm::detail;


stop_watcher_t::stop_watcher_t() {
	start();
	}

void stop_watcher_t::start() { 
	_startTP = std::chrono::high_resolution_clock::now();
	}

std::chrono::high_resolution_clock::duration stop_watcher_t::now()const {
	return std::chrono::high_resolution_clock::now() - _startTP;
	}

stop_watcher_t::counter_t stop_watcher_t::now_millisec()const {
	return std::chrono::duration_cast<std::chrono::microseconds>(now()).count();
	}

stop_watcher_t::counter_t stop_watcher_t::now_microsec()const {
	return std::chrono::duration_cast<std::chrono::microseconds>(now()).count();
	}