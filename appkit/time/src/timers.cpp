#include "../../internal.h"
#include "../../io_services/src/ios_boost_impl.h"
#include "../../utilities/check_validate/checker_seqcall.h"
#include "../../context/context.h"
#include "../../utilities/utilities.h"
#include "../timers.h"


namespace crm {
namespace detail {


static std::unique_ptr<boost::asio::deadline_timer> create_timer(std::weak_ptr<distributed_ctx_t> ctx_) {
	auto ctx(ctx_.lock());
	if (ctx) {
		return std::make_unique<boost::asio::deadline_timer>(CHECK_PTR(ctx->io_services().detail())->services());
		}
	else
		THROW_MPF_EXC_FWD(nullptr);
	}


/*================================================================================
timer_oncecall_impl_t
==================================================================================*/


class timer_oncecall_impl_t : public std::enable_shared_from_this<timer_oncecall_impl_t>{
	typedef timer_oncecall_impl_t self_t;

public:
	using timer_out_t = __xtimer_oncecall_t::timer_out_t;
	using timer_out_exception_guard = functor_invoke_guard_t<timer_out_t, i_xtimer_base_t::make_fault_result_t>;

private:
	std::weak_ptr<distributed_ctx_t> _ctx;
	std::unique_ptr<boost::asio::deadline_timer> _timer;
	timer_out_exception_guard _timerCallback;
	std::chrono::system_clock::time_point _deadlinePoint;
	async_space_t _scpf = async_space_t::undef;
	std::atomic<bool> _invokedfl{ false };
	std::atomic<bool> _cancel{ false };
	std::atomic<bool> _startf{false};

#ifdef CBL_MPF_OBJECTS_COUNTER_CHECKER_TIMERS
	utility::__xobject_counter_logger_t<self_t> _xDbgCounter;
#endif

	bool invoke_test_and_set()noexcept {
		return !_invokedfl.exchange(true);
		}

	void invoke_reset()noexcept {
		if(invoke_test_and_set()) {
			_timerCallback = timer_out_exception_guard{};
			}
		}

public:
	timer_oncecall_impl_t( std::weak_ptr<distributed_ctx_t> ctx, async_space_t spcf )
		: _ctx(ctx)
		, _timer(create_timer(ctx)) 
		, _scpf(spcf){}

	~timer_oncecall_impl_t() {
		CHECK_NO_EXCEPTION( close() );
		CBL_VERIFY(_timerCallback.empty());
		}

	void close()noexcept {
		CHECK_NO_EXCEPTION(invoke(async_operation_result_t::st_abandoned));
		CHECK_NO_EXCEPTION(stop());
		}

	async_space_t space()const noexcept {
		return _scpf;
		}
	
	invoke_result_t invoke( async_operation_result_t result )noexcept {
		if( invoke_test_and_set() ) {
			if(auto h = std::move(_timerCallback)) {
				if(auto ctx_ = _ctx.lock()) {
					if(h) {
						CHECK_NO_EXCEPTION(ctx_->launch_async_timer_handler(_scpf, __FILE_LINE__, std::move(h), result));
						}
					}
				}

			return invoke_result_t::st_ok;
			}
		else {
			return invoke_result_t::st_already_invoked;
			}
		}

	void start(timer_out_t && timerOut, const std::chrono::milliseconds & timeout) {
		bool stf = false;
		if( _startf.compare_exchange_strong( stf, true ) ) {

			if( !canceled() ) {
				CBL_VERIFY(!_timerCallback);
				_timerCallback = timer_out_exception_guard( std::move( timerOut ), _ctx, __FILE_LINE__, _scpf, timer_out_exception_guard::invoke_once );
				_deadlinePoint = sndrcv_timeline_periodicity_t::make_timepoint( timeout);

				_timer->expires_from_now( boost::posix_time::millisec( timeout.count() ) );
				_timer->async_wait( [wptr = weak_from_this()]( const boost::system::error_code& error ) {

					auto sptr( wptr.lock() );
					if( sptr ) {
						if( !error )
							sptr->invoke( async_operation_result_t::st_ready );
						else {
							if( sptr->canceled() )
								sptr->invoke( async_operation_result_t::st_canceled );
							else
								sptr->invoke( async_operation_result_t::st_exception );
							}
						}
					} );
				}
			}
		else {
			THROW_EXC_FWD(nullptr);
			}
		}

	void stop()noexcept {
		bool clf = false;
		if(_cancel.compare_exchange_strong(clf, true)) {
			CHECK_NO_EXCEPTION(_timer->cancel());
			CHECK_NO_EXCEPTION(invoke_reset());
			}
		}

	bool canceled()const noexcept{
		return _cancel.load(std::memory_order::memory_order_acquire);
		}

	std::chrono::system_clock::time_point deadline_point()const noexcept {
		return _deadlinePoint;
		}
	};


/*================================================================================
timer_impl_t
==================================================================================*/


class timer_impl_t : public std::enable_shared_from_this<timer_impl_t> {
	typedef timer_impl_t self_t;

public:
	using timer_out_t = __xtimer_t::timer_out_t;
	using timer_out_exception_guard = functor_invoke_guard_t<timer_out_t, i_xtimer_base_t::make_fault_result_t>;

private:
	struct __extscp_t {
		timer_out_exception_guard timerCallback;
		std::chrono::milliseconds interval;

		__extscp_t(timer_out_exception_guard && timerCallback_,
			const std::chrono::milliseconds & interval_)
			: timerCallback(std::move(timerCallback_))
			, interval(interval_) {
			
			if(!sndrcv_timeline_periodicity_t::validate_duration_range(interval)) {
				FATAL_ERROR_FWD(nullptr);
				}
			}
		};

	std::weak_ptr<distributed_ctx_t> _ctx;
	std::shared_ptr<boost::asio::deadline_timer> _timerd;
	async_space_t _scpf = async_space_t::undef;
	std::shared_ptr<__extscp_t> _hndl;
	std::atomic<bool> _cancel{ false };
	std::atomic<bool> _startedf{ false };
	bool _nextIfUserExc = true;

	static const std::chrono::milliseconds null_interval;

#ifdef CBL_USE_SEQUENTIAL_CALL_CHECKER
	checker_seqcall_t _orderCallChecker;
#endif

#ifdef CBL_MPF_OBJECTS_COUNTER_CHECKER_TIMERS
	utility::__xobject_counter_logger_t<self_t> _xDbgCounter;
#endif

	std::shared_ptr<boost::asio::deadline_timer> get_timerd()const noexcept {
		return std::atomic_load_explicit(&_timerd, std::memory_order::memory_order_acquire);
		}

	std::shared_ptr<boost::asio::deadline_timer> get_timerd_release() noexcept {
		return std::atomic_exchange(&_timerd, std::shared_ptr<boost::asio::deadline_timer>());
		}

	void ntf_hndl(std::unique_ptr<dcf_exception_t> && n)const noexcept {
		auto ctx_(_ctx.lock());
		if(ctx_) {
			ctx_->exc_hndl(std::move(n));
			}
		}

	std::shared_ptr<__extscp_t> get_hndl() const noexcept{
		return std::atomic_load_explicit(&_hndl, std::memory_order::memory_order_acquire);
		}

	std::shared_ptr<__extscp_t> release_hndl()noexcept {
		return std::atomic_exchange(&_hndl, std::shared_ptr<__extscp_t>());
		}

	std::shared_ptr<__extscp_t> pack(timer_out_t && timerCallback,
		const std::chrono::milliseconds & interval) {

		return std::make_shared<__extscp_t>(
			 timer_out_exception_guard( std::move(timerCallback), _ctx, "timer_impl_t", _scpf, timer_out_exception_guard::invoke_indefinitely),
			interval );
		};

	void set_hndl(timer_out_t && timerCallback,
		const std::chrono::milliseconds & interval) {

		if(interval == null_interval) {
			set_hndl(std::move(timerCallback));
			}
		else {
			std::atomic_exchange(&_hndl, pack(std::move(timerCallback), interval));
			}
		}

	void set_hndl(timer_out_t && timerCallback) {
		auto hndl(get_hndl());
		if(hndl) {
			if(auto oh = std::atomic_exchange(&_hndl, pack(std::move(timerCallback), hndl->interval))) {
				if(oh) {
					if(oh && oh->timerCallback) {
						oh->timerCallback.invoke_async(async_operation_result_t::st_abandoned);
						}
					}
				}
			}
		else {
			THROW_EXC_FWD(nullptr);
			}
		}

	void invoke(std::shared_ptr<__extscp_t> pHndl,
		async_operation_result_t result) noexcept {

		if(pHndl) {

			/* ����� ����������������� ����������� */
			bool nextBasef = _nextIfUserExc;

			auto uinvoker = [wptr = weak_from_this(), pHndl, result, nextBasef] {
				if(auto sptr = wptr.lock()) {

					auto exch = [&sptr](std::unique_ptr<dcf_exception_t> && exc)noexcept {
						if(sptr) {
							CHECK_NO_EXCEPTION(sptr->ntf_hndl(std::move(exc)));
							}
						};

					/* ���� return:[true] - ������ ��������� �������� */
					bool next = nextBasef;
					try {
						next = pHndl->timerCallback.invoke_explicit(result);
						}
					catch(const dcf_exception_t & exc0) {
						exch(exc0.dcpy());
						}
					catch(const std::exception & exc1) {
						exch(CREATE_MPF_PTR_EXC_FWD(exc1));
						}
					catch(...) {
						exch(CREATE_PTR_EXC_FWD(nullptr));
						}

					if(next) {
						if(!sptr->canceled()) {
							sptr->timer_start();
							}
						}
					else {
						CHECK_NO_EXCEPTION(sptr->stop());
						}
					}
				};

			/* ����� ����������������� ����������� � ��������� ��������� */
			if(auto ctx_ = _ctx.lock()) {
				CHECK_NO_EXCEPTION(ctx_->launch_async_timer_handler(_scpf, __FILE_LINE__, std::move(uinvoker)));
				}
			}
		}

	void invoke_abandoned() noexcept{
		invoke( release_hndl(), async_operation_result_t::st_abandoned );
		}

	void timer_start() {
		if(!canceled()) {

#ifdef CBL_USE_SEQUENTIAL_CALL_CHECKER
			auto t0(_orderCallChecker.inc_scope());
#endif

			if(auto pHndl = get_hndl()) {
				auto nextTimer = get_timerd();
				if(nextTimer) {

					nextTimer->expires_from_now(boost::posix_time::millisec(pHndl->interval.count()));
					nextTimer->async_wait([wptr = weak_from_this(), pHndl](const boost::system::error_code& error) {

						auto sptr(wptr.lock());
						if(sptr && ! sptr->canceled()) {
							if(!error)
								sptr->invoke(pHndl, async_operation_result_t::st_ready);
							else {
								if(sptr->canceled())
									sptr->invoke(pHndl, async_operation_result_t::st_canceled);
								else
									sptr->invoke(pHndl, async_operation_result_t::st_exception);
								}
							}
						});
					}
				}
			else
				THROW_EXC_FWD(nullptr);
			}
		}

public:
	timer_impl_t(std::weak_ptr<distributed_ctx_t> ctx, async_space_t scpf)
		: _ctx(ctx)
		, _timerd(create_timer(ctx))
		, _scpf(scpf) {}

	timer_impl_t(std::weak_ptr<distributed_ctx_t> ctx,
		std::chrono::milliseconds interval, async_space_t scpf)
		: _ctx(ctx)
		, _timerd(create_timer(ctx))
		, _scpf(scpf)
		, _hndl(pack({}, interval)) {}

	~timer_impl_t() {
		CHECK_NO_EXCEPTION(close());
		}

	void close()noexcept {
		CHECK_NO_EXCEPTION(invoke_abandoned());
		CHECK_NO_EXCEPTION(stop());
		}

	async_space_t space()const noexcept {
		return _scpf;
		}

	void start(std::function<void()> && first, 
		timer_out_t && timerOut,
		const std::chrono::milliseconds & interval,
		bool nextIfExc ) {

		bool stf = false;
		if( _startedf.compare_exchange_strong( stf, true ) ) {
			_nextIfUserExc = nextIfExc;

			if(first) {
				auto _start = crm::utility::bind_once_call({ "timer_impl_t::start" }, [wptr = weak_from_this()]
				(std::function<void()> && f, timer_out_t && timerOut, const std::chrono::milliseconds & interval) {

					if(auto s = wptr.lock()) {
						try {
							f();

							s->set_hndl(std::move(timerOut), interval);
							s->timer_start();
							}
						catch(const dcf_exception_t & exc0) {
							CHECK_NO_EXCEPTION(s->ntf_hndl(exc0.dcpy()));
							CHECK_NO_EXCEPTION(s->stop());
							}
						catch(const std::exception & exc1) {
							CHECK_NO_EXCEPTION(s->ntf_hndl(CREATE_MPF_PTR_EXC_FWD(exc1)));
							CHECK_NO_EXCEPTION(s->stop());
							}
						catch(...) {
							CHECK_NO_EXCEPTION(s->ntf_hndl(CREATE_PTR_EXC_FWD(nullptr)));
							CHECK_NO_EXCEPTION(s->stop());
							}
						}
					}, std::move(first), std::move(timerOut), interval);

				if(auto c = _ctx.lock()) {
					c->launch_async(_scpf, __FILE_LINE__, std::move(_start), std::move(_start));
					}
				}
			else {
				set_hndl(std::move(timerOut), interval);
				timer_start();
				}
			}
		else {
			THROW_EXC_FWD(nullptr);
			}
		}

	void start(std::function<void()> && first, 
		timer_out_t && timerOut,
		bool nextIfExc ) {

		start( std::move(first), std::move( timerOut ), null_interval, nextIfExc );
		}

	void stop() noexcept {
		bool cancelf = false;
		if(_cancel.compare_exchange_strong(cancelf, true)) {

			if(auto t = get_timerd_release()) {
				CHECK_NO_EXCEPTION(t->cancel());
				}
			}
		}

	bool canceled()const noexcept{
		return _cancel.load(std::memory_order::memory_order_acquire);
		}

	std::chrono::milliseconds interval()const {
		auto hndl( get_hndl() );
		if( hndl )
			return hndl->interval;
		else
			THROW_EXC_FWD(nullptr);
		}
	};

const std::chrono::milliseconds timer_impl_t::null_interval = std::chrono::milliseconds(0);


/*==================================================================================
deffered_action_timer_impl_t
====================================================================================*/


class deffered_action_timer_impl_t :
	public std::enable_shared_from_this<deffered_action_timer_impl_t> {

	typedef deffered_action_timer_impl_t self_t;

public:
	using timer_out_t = __xdeffered_action_timer_t::timer_out_t;
	using timer_out_exception_guard = functor_invoke_guard_t<timer_out_t, i_xtimer_base_t::make_fault_result_t>;

private:
	typedef deffered_action_timer_impl_t self_t;

	std::weak_ptr<distributed_ctx_t> _ctx;
#ifdef CBL_TRACELEVEL_TRACE_TAG_ON
	trace_tag _tag;
#endif
	timer_out_exception_guard _hndlCallback;
	std::unique_ptr<boost::asio::deadline_timer> _expiredTimer;
	std::chrono::system_clock::time_point _deadlinePoint;
	std::atomic<bool> _invokedfl{ false };
	std::atomic<bool> _cancelf{ false };
	async_space_t _scpf{ async_space_t::undef };

#ifdef CBL_MPF_OBJECTS_COUNTER_CHECKER_TIMERS
	utility::__xobject_counter_logger_t<self_t> _xDbgCounter;
#endif

public:
	deffered_action_timer_impl_t(std::weak_ptr<distributed_ctx_t> ctx_,
#ifdef CBL_TRACELEVEL_TRACE_TAG_ON
		trace_tag&& tag,
#else
		trace_tag&&,
#endif

		timer_out_t && hndl_,
		async_space_t scpf )
		: _ctx(ctx_)
#ifdef CBL_TRACELEVEL_TRACE_TAG_ON
		, _tag(std::move(tag))
#endif
		, _hndlCallback( std::move( hndl_ ), 
			ctx_,
#ifdef CBL_TRACELEVEL_TRACE_TAG_ON
			_tag.name + 
#endif
			":deffered-timer-ctr", 
			scpf, 
			timer_out_exception_guard::invoke_once )
		, _expiredTimer(create_timer(ctx_))
		, _scpf(scpf)
#if defined( CBL_MPF_OBJECTS_COUNTER_CHECKER_TIMERS) && defined(CBL_TRACELEVEL_TRACE_TAG_ON)
		, _xDbgCounter(_tag.name)
#endif
	
	{}

private:
	void timer_callback(const boost::system::error_code& error) {
		if(!canceled()) {
			try {
				/* ���������� ������ �� ���������� ������� �������� */
				if(!error) {
					invoke(async_operation_result_t::st_timeouted);
					}
				/* ������ �������� */
				else {
					if(canceled())
						invoke(async_operation_result_t::st_canceled);
					else
						invoke(async_operation_result_t::st_exception);
					}
				}
			catch(const dcf_exception_t & e0) {
				if(auto ctx_ = _ctx.lock()) {
					ctx_->exc_hndl(e0);
					}
				}
			catch(const std::exception & e1) {
				if(auto ctx_ = _ctx.lock()) {
					ctx_->exc_hndl(CREATE_MPF_PTR_EXC_FWD(e1));
					}
				}
			catch(...) {
				if(auto ctx_ = _ctx.lock()) {
					ctx_->exc_hndl(CREATE_MPF_PTR_EXC_FWD(nullptr));
					}
				}
			}
		}

	template<typename Rep, typename Duration>
	void start_timer( bool standalone, const std::chrono::duration<Rep, Duration> & timeout_ ) {
		using time_line_type = typename std::chrono::duration<Rep, Duration>;

		_deadlinePoint = sndrcv_timeline_periodicity_t::make_timepoint( timeout_ );

		/* ����� ��������� �������� ������� ���������� */
		const auto millisecVal = std::chrono::duration_cast<std::chrono::milliseconds>(timeout_);
		_expiredTimer->expires_from_now( boost::posix_time::millisec( millisecVal.count() ) );

		/* ������ ������� �������� */
		if(standalone) {
			_expiredTimer->async_wait([sptr = shared_from_this()](const boost::system::error_code& error) {
				sptr->timer_callback(error);
				});
			}
		else {
			_expiredTimer->async_wait([wptr = weak_from_this()](const boost::system::error_code& error) {
				if(auto sptr = wptr.lock()) {
					sptr->timer_callback(error);
					}
				});
			}
		}

	void invoke_abandoned()noexcept {
		CHECK_NO_EXCEPTION(invoke(async_operation_result_t::st_abandoned));
		}

	bool invoke_test_and_set()noexcept {
		return !_invokedfl.exchange( true);
		}

	void invoke_reset()noexcept {
		if(invoke_test_and_set()) {
			_hndlCallback = timer_out_exception_guard{};
			}
		}

public:
	virtual ~deffered_action_timer_impl_t() {
		CHECK_NO_EXCEPTION(invoke_abandoned());
		CHECK_NO_EXCEPTION(stop());
		CBL_VERIFY(_hndlCallback.empty());
		}

	async_space_t space()const noexcept {
		return _scpf;
		}

	template<typename Rep, typename Duration>
	static std::shared_ptr<deffered_action_timer_impl_t> launch(bool standalone, 
		std::weak_ptr<distributed_ctx_t> ctx_,
		trace_tag && tag,
		timer_out_t && hndl_,
		const std::chrono::duration<Rep, Duration> & timeout,
		async_space_t scpf ) {

		auto actor = std::make_shared<deffered_action_timer_impl_t>(ctx_, std::move(tag), std::move( hndl_ ), scpf );
		actor->start_timer(standalone, timeout);

		return actor;
		}

	void stop()noexcept {
		bool clf = false;
		if( _cancelf.compare_exchange_strong( clf, true ) ) {
			CHECK_NO_EXCEPTION( _expiredTimer->cancel() );
			CHECK_NO_EXCEPTION( invoke_reset() );
			}
		}

	bool canceled()const noexcept {
		return _cancelf.load( std::memory_order::memory_order_acquire );
		}

	invoke_result_t invoke( async_operation_result_t result )noexcept {
		if( invoke_test_and_set() ) {
			if(auto h = std::move(_hndlCallback)) {

				post_scope_action_t deleteTimer([this] {
					_expiredTimer->cancel();
					});

				if(auto ctx_ = _ctx.lock()) {
					if(h) {
						CHECK_NO_EXCEPTION(ctx_->launch_async_timer_handler(_scpf, __FILE_LINE__, std::move(h), result));
						}
					}
				}

			return invoke_result_t::st_ok;
			}
		else
			return invoke_result_t::st_already_invoked;
		}

	std::chrono::system_clock::time_point deadline_point()const noexcept {
		return _deadlinePoint;
		}
	};
}
}


using namespace crm;
using namespace crm::detail;


/*==================================================================================
__xtimer_oncecall_t
====================================================================================*/


__xtimer_oncecall_t::~__xtimer_oncecall_t() {
	CHECK_NO_EXCEPTION(stop());
	}

__xtimer_oncecall_t::__xtimer_oncecall_t( std::weak_ptr<distributed_ctx_t> ctx, 
	async_space_t scpf )
	: _impl(std::make_shared<timer_oncecall_impl_t>(ctx, scpf )) {}

__xtimer_oncecall_t::__xtimer_oncecall_t( __xtimer_oncecall_t  && o )noexcept
	: _impl( std::move( o._impl ) ) {}

__xtimer_oncecall_t::__xtimer_oncecall_t( std::weak_ptr<distributed_ctx_t> ctx_,
	timer_out_t && hndl_,
	const std::chrono::milliseconds & timeout,
	async_space_t aspc )
	: _impl( std::make_shared<timer_oncecall_impl_t>( ctx_, aspc ) ) {
	
	_impl->start( std::move( hndl_ ), timeout );
	}

__xtimer_oncecall_t& __xtimer_oncecall_t::operator=(__xtimer_oncecall_t && o)noexcept {
	if( this != std::addressof(o) ) {
		_impl = std::move( o._impl );	
		}
	
	return (*this);
	}

void __xtimer_oncecall_t::start( timer_out_t && timerOut, 
	const std::chrono::milliseconds & timeout ) {

	if( _impl ) {
		_impl->start( std::move( timerOut ), timeout );
		}
	else
		THROW_EXC_FWD(nullptr);
	}

void __xtimer_oncecall_t::stop()noexcept {
	if( _impl ) {
		CHECK_NO_EXCEPTION( _impl->stop() );
		}
	}

bool __xtimer_oncecall_t::canceled()const noexcept {
	if( _impl )
		return _impl->canceled();
	else
		return true;
	}

invoke_result_t __xtimer_oncecall_t::invoke( async_operation_result_t result )noexcept {
	if( _impl ) {
		return _impl->invoke( result );
		}
	else
		return invoke_result_t::st_null_handler;
	}

async_space_t __xtimer_oncecall_t::space()const noexcept {
	if(_impl)
		return _impl->space();
	else {
		return async_space_t::undef;
		}
	}

bool __xtimer_oncecall_t::is_alived()const noexcept {
	return nullptr != _impl;
	}

std::chrono::system_clock::time_point __xtimer_oncecall_t::deadline_point()const noexcept {
	if(_impl)
		return _impl->deadline_point();
	else {
		return std::chrono::system_clock::time_point{};
		}
	}


/*==================================================================================
__xtimer_t
====================================================================================*/


__xtimer_t::~__xtimer_t() {
	CHECK_NO_EXCEPTION(stop());
	}

__xtimer_t::__xtimer_t( std::weak_ptr<distributed_ctx_t> ctx, 
	async_space_t scpf)
	: _impl(std::make_shared<timer_impl_t>(ctx, scpf)) {}

__xtimer_t::__xtimer_t( std::weak_ptr<distributed_ctx_t> ctx,
	std::chrono::milliseconds interval, 
	async_space_t scpf)
	: _impl(std::make_shared<timer_impl_t>(ctx, interval, scpf)) {}

__xtimer_t::__xtimer_t( __xtimer_t  && o )noexcept
	: _impl( std::move( o._impl ) ) {}

__xtimer_t& __xtimer_t::operator=(__xtimer_t && o)noexcept {
	if( this != std::addressof(o) ) {
		_impl = std::move( o._impl );
		}
	
	return (*this);
	}

bool __xtimer_t::canceled()const noexcept {
	if( _impl )
		return _impl->canceled();
	else
		return true;
	}

std::chrono::milliseconds __xtimer_t::interval()const noexcept {
	if(_impl)
		return _impl->interval();
	else
		return std::chrono::milliseconds{};
	}

void __xtimer_t::stop() noexcept {
	if( _impl ) {
		CHECK_NO_EXCEPTION( _impl->stop() );
		}
	}

void __xtimer_t::start(std::function<void()> && first, 
	timer_out_t && timerOut,
	const std::chrono::milliseconds & interval,
	bool nextIfExc) {

	if(_impl) {
		_impl->start(std::move(first), std::move(timerOut), interval, nextIfExc);
		}
	else
		THROW_EXC_FWD(nullptr);
	}

void __xtimer_t::start(timer_out_t && timerOut,
	const std::chrono::milliseconds & interval,
	bool nextIfExc) {

	if( _impl ) {
		_impl->start( nullptr, std::move( timerOut ), interval, nextIfExc );
		}
	else
		THROW_EXC_FWD(nullptr);
	}

void __xtimer_t::start(std::function<void()> && first, 
	timer_out_t && timerOut,
	bool nextIfExc) {

	if(_impl) {
		_impl->start(std::move(first) , std::move(timerOut), nextIfExc);
		}
	else
		THROW_EXC_FWD(nullptr);
	}

void __xtimer_t::start(timer_out_t && timerOut,
	bool nextIfExc ) {

	if( _impl ) {
		_impl->start( nullptr, std::move( timerOut ), nextIfExc );
		}
	else
		THROW_EXC_FWD(nullptr);
	}

async_space_t __xtimer_t::space()const noexcept {
	if(_impl)
		return _impl->space();
	else {
		return async_space_t::undef;
		}
	}

bool __xtimer_t::is_alived()const noexcept {
	return nullptr != _impl;
	}


/*==================================================================================
deffered_action_timer_impl_t
====================================================================================*/

__xdeffered_action_timer_t::~__xdeffered_action_timer_t() {
	CHECK_NO_EXCEPTION(stop());
	}

__xdeffered_action_timer_t::__xdeffered_action_timer_t(std::weak_ptr<distributed_ctx_t> ctx_,
	trace_tag&& tag,
	timer_out_t && hndl_,
	const std::chrono::milliseconds & timeout,
	async_space_t scpf )
	: _impl( deffered_action_timer_impl_t::launch( false, ctx_, std::move(tag), std::move( hndl_ ), timeout, scpf ) ) {}

void __xdeffered_action_timer_t::launch(std::weak_ptr<distributed_ctx_t> ctx_,
	trace_tag&& tag,
	timer_out_t && hndl_,
	const std::chrono::milliseconds & timeout,
	async_space_t scpf) {

	deffered_action_timer_impl_t::launch(true, ctx_, std::move(tag), std::move(hndl_), timeout, scpf);
	}

bool __xdeffered_action_timer_t::canceled()const noexcept {
	if( _impl )
		return _impl->canceled();
	else
		return true;
	}

void __xdeffered_action_timer_t::stop() noexcept {
	if( _impl ) {
		CHECK_NO_EXCEPTION( _impl->stop() );
		}
	}

invoke_result_t __xdeffered_action_timer_t::invoke( async_operation_result_t rslt )noexcept {
	if( _impl ) {
		return _impl->invoke( rslt );
		}
	else
		return invoke_result_t::st_null_handler;
	}

__xdeffered_action_timer_t::__xdeffered_action_timer_t(__xdeffered_action_timer_t && o)noexcept
	: _impl(std::move(o._impl)) {}

__xdeffered_action_timer_t& __xdeffered_action_timer_t::operator=(__xdeffered_action_timer_t && o)noexcept {
	if (this != std::addressof(o)) {
		_impl = std::move(o._impl);
		}

	return (*this);
	}

async_space_t __xdeffered_action_timer_t::space()const noexcept {
	if(_impl)
		return _impl->space();
	else {
		return async_space_t::undef;
		}
	}

bool __xdeffered_action_timer_t::is_alived()const noexcept {
	return nullptr != _impl;
	}

std::chrono::system_clock::time_point __xdeffered_action_timer_t::deadline_point()const noexcept {
	if(_impl)
		return _impl->deadline_point();
	else {
		return std::chrono::system_clock::time_point{};
		}
	}

