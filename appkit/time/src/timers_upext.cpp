#include "../../internal.h"
#include "../timers.h"


using namespace crm;
using namespace crm::detail;


timer_oncecall_t::timer_oncecall_t( std::weak_ptr<distributed_ctx_t> ctx )
	: detail::__xtimer_oncecall_t( ctx, async_space_t::ext ) {}

timer_oncecall_t::timer_oncecall_t( std::weak_ptr<distributed_ctx_t> ctx_,
	timer_out_t && hndl_,
	const std::chrono::milliseconds & timeout )
	: detail::__xtimer_oncecall_t( ctx_, std::move(hndl_), timeout, async_space_t::ext ) {}

timer_oncecall_t::timer_oncecall_t( timer_oncecall_t  && o )noexcept
	: detail::__xtimer_oncecall_t( std::move( o ) ) {}

timer_oncecall_t& timer_oncecall_t::operator = (timer_oncecall_t && o)noexcept {
	detail::__xtimer_oncecall_t::operator=(std::move( o ));
	return (*this);
	}




timer_t::timer_t( timer_t  && o )noexcept
	: detail::__xtimer_t( std::move( o ) ) {}

timer_t& timer_t::operator = (timer_t && o)noexcept {
	detail::__xtimer_t::operator=(std::move( o ));
	return (*this);
	}

timer_t::timer_t( std::weak_ptr<distributed_ctx_t> ctx )
	: detail::__xtimer_t( ctx, async_space_t::ext ) {}

timer_t::timer_t( std::weak_ptr<distributed_ctx_t> ctx, std::chrono::milliseconds interval )
	: detail::__xtimer_t( ctx, interval, async_space_t::ext ) {}



deffered_action_timer_t::deffered_action_timer_t( deffered_action_timer_t && o )noexcept
	: detail::__xdeffered_action_timer_t( std::move( o ) ) {}

deffered_action_timer_t& deffered_action_timer_t::operator = (deffered_action_timer_t && o)noexcept {
	detail::__xdeffered_action_timer_t::operator=(std::move( o ));
	return (*this);
	}


deffered_action_timer_t::deffered_action_timer_t( std::weak_ptr<distributed_ctx_t> ctx_,
	trace_tag&& tag,
	timer_out_t && hndl,
	const std::chrono::milliseconds & timeout )
	: detail::__xdeffered_action_timer_t( ctx_, std::move( tag ), std::move( hndl ), timeout, async_space_t::ext ) {}


void deffered_action_timer_t::launch(std::weak_ptr<distributed_ctx_t> ctx_,
	trace_tag&& tag,
	timer_out_t&& hndl_,
	const std::chrono::milliseconds& timeout) {

	detail::__xdeffered_action_timer_t::launch(ctx_, std::move(tag), std::move(hndl_), timeout, async_space_t::sys);
	}

