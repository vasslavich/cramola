#include "../../context/context.h"
#include "../msg_timestack.h"


using namespace crm;
using namespace crm::detail;


typedef decltype(std::declval<std::chrono::system_clock::time_point>().time_since_epoch()) _xstreamed_time_t;
typedef decltype(std::declval<_xstreamed_time_t>().count()) streamed_time_t;


hanlder_times_stack_t::hanlder_times_stack_t( hanlder_times_stack_t && o )
	:_line( std::move( o._line ) ) {}

hanlder_times_stack_t& hanlder_times_stack_t::operator=(hanlder_times_stack_t && o) {
	_line = std::move( o._line );
	return (*this);
	}

hanlder_times_stack_t::hanlder_times_stack_t( const hanlder_times_stack_t & o )
	:_line( o._line ) {}

hanlder_times_stack_t& hanlder_times_stack_t::operator=(const hanlder_times_stack_t & o) {
	_line = o._line;
	return (*this);
	}

std::size_t hanlder_times_stack_t::get_serialized_size()const noexcept{
	return crm::srlz::object_trait_serialized_size( _line.size() );
	}

hanlder_times_stack_t::hanlder_times_stack_t() {}

hanlder_times_stack_t::time_point_t& hanlder_times_stack_t::operator[]( stage_t stIndex ) {
	return _line.at( (size_t)stIndex );
	}

const hanlder_times_stack_t::time_point_t& hanlder_times_stack_t::operator[]( stage_t stIndex )const {
	return _line.at( (size_t)stIndex );
	}

hanlder_times_stack_t::clocks_t hanlder_times_stack_t::clocks()const {
	static const std::chrono::system_clock::time_point null_value = std::chrono::system_clock::time_point();

	clocks_t clk;

	size_t lastReper = 0;
	for( size_t i = 0; i < _line.size(); ++i ) {

		if( _line.at( i ) > _line.at( lastReper ) && _line.at( lastReper ) != null_value ) {

			auto interval = std::chrono::duration_cast<std::chrono::milliseconds>(_line.at( i ) - _line.at( lastReper ));
			clk.clocks.at( i ) = interval;

			lastReper = i;
			}
		}

	return std::move( clk );
	}

void hanlder_times_stack_t::clear() {
	for( size_t i = 0; i < _line.size(); ++i ) {
		_line.at( i ) = std::chrono::system_clock::time_point();
		}
	}
