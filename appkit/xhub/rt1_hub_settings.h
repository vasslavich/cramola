#pragma once


#include "../tableval/tableval.h"
#include "../xpeer/i_rt1_xpeer.h"
#include "../channel_terms/terms.h"
#include "../cmdrt/terms.h"
#include "../queues/i_message_queue.h"
#include "../messages/terms.h"
#include "../xpeer/xpeer_mkprereq.h"
#include "../xpeer/i_input_handlers.h"


namespace crm{


class rt1_hub_settings_t{
private:
	std::weak_ptr<distributed_ctx_t> _ctx;
	identity_descriptor_t _thisId;
	std::unique_ptr<i_input_messages_stock_t> _inputStackCtr;
	std::unique_ptr<default_binary_protocol_handler_t> _protocol;

	void assign( rt1_hub_settings_t && o )noexcept;

public:
	virtual ~rt1_hub_settings_t(){}

	rt1_hub_settings_t()noexcept;

	rt1_hub_settings_t(std::shared_ptr<distributed_ctx_t> ctx,
		const identity_descriptor_t & thisId,
		std::unique_ptr<default_binary_protocol_handler_t> protocol,
		std::unique_ptr<i_input_messages_stock_t> && inputStackCtr);

	rt1_hub_settings_t(std::shared_ptr<distributed_ctx_t> ctx,
		const identity_descriptor_t& thisId,
		std::unique_ptr<i_input_messages_stock_t>&& inputStackCtr);

	template<typename H,
		typename std::enable_if_t<is_rt1_hub_input_handler_v<H>, int> = 0>
		rt1_hub_settings_t(std::shared_ptr<distributed_ctx_t> ctx,
			const identity_descriptor_t& thisId,
			H&& h)
		: rt1_hub_settings_t(ctx,
			thisId,
			std::make_unique<hub_input2handler_rt1_t>(std::forward<H>(h))) {}

	rt1_hub_settings_t( const rt1_hub_settings_t  & ) = delete;
	rt1_hub_settings_t& operator=( const rt1_hub_settings_t & ) = delete;

	rt1_hub_settings_t( rt1_hub_settings_t  && o )noexcept;
	rt1_hub_settings_t& operator=( rt1_hub_settings_t && o )noexcept;

	std::shared_ptr<distributed_ctx_t> ctx()const noexcept;
	const identity_descriptor_t& this_id()const noexcept;

	virtual void connection_handler( ipeer_t &&, datagram_t && );
	virtual void disconnection_handler( const detail::i_xpeer_rt1_t::xpeer_desc_t &, std::shared_ptr<i_peer_statevalue_t> && );

	virtual syncdata_list_t local_object_syncdata();
	remote_object_connection_value_t local_object_identity();
	virtual bool remote_object_identity_validate( const identity_value_t &,
		std::unique_ptr<i_peer_remote_properties_t> & );
	virtual datagram_t remote_object_initialize( syncdata_list_t && );
	virtual void local_object_initialize( datagram_t && /*dt*/,
		detail::object_initialize_result_guard_t && resultHndl );
	virtual std::unique_ptr<i_peer_statevalue_t> state_value();
	std::unique_ptr<i_input_messages_stock_t> input_stack();
	virtual void event_handler( std::unique_ptr<i_event_t> && ev );
	virtual void opeer_fail( const remote_connection_desc_t & /*opeer*/,
		std::unique_ptr<dcf_exception_t> && /*exc*/ );
	void ntf_hndl(std::unique_ptr<dcf_exception_t> && n)const noexcept;
	const std::unique_ptr<default_binary_protocol_handler_t>& protocol()const noexcept;

	void set_protocol(std::unique_ptr<default_binary_protocol_handler_t> &&);
	void set_input_stack(std::unique_ptr<i_input_messages_stock_t> &&);
	void set_context(std::shared_ptr<distributed_ctx_t> ctx);
	void set_this_id(const identity_descriptor_t & thisId);


	template<typename H,
		typename std::enable_if_t<is_rt1_hub_input_handler_v<H>, int> = 0>
	void set_input_stack(H&& h) {
		set_input_stack(std::make_unique<hub_input2handler_rt1_t>(std::forward<H>(h)));
		}
	};

template<typename S, typename = void>
struct is_hub_settings_rt1 : std::false_type {};

template<typename S>
struct is_hub_settings_rt1 < S,
	typename std::enable_if_t<std::is_base_of_v<rt1_hub_settings_t, S>>> : std::true_type {};

template<typename S>
constexpr bool is_hub_settings_rt1_v = is_hub_settings_rt1<S>::value;
}


