#pragma once


#include "../tableval/tableval.h"
#include "../protocol/i_ssource.h"
#include "../protocol/i_encoding.h"
#include "../xpeer/rt0_xpeer.h"
#include "../channel_terms/terms.h"
#include "../queues/i_message_queue.h"
#include "../messages/terms.h"
#include "../xpeer/xpeer_mkprereq.h"
#include "../xpeer/rt0_end_ipeer.h"
#include "../channel_terms/rt0_xpeer_input.h"
#include "../xpeer/i_input_handlers.h"


namespace crm {


class rt0_hub_settings_t {
private:
	std::weak_ptr<distributed_ctx_t> _ctx;
	identity_descriptor_t _thisId;
	std::shared_ptr<i_endpoint_t> _ep;
	std::shared_ptr<i_xpeer_source_factory_t> _ipeerFactory;
	std::shared_ptr<i_message_input_t<input_message_knop_l0>> _inputStack;
	std::shared_ptr<default_binary_protocol_handler_t> _p;

public:
	virtual ~rt0_hub_settings_t() {}

	template<typename TEndPoint,
		typename TPeerFactory,
		typename TBinaryProtocol,
		typename TInputMessageHandler,

		typename _TEndPoint = std::decay_t<TEndPoint>,
		typename _TPeerFactory = std::decay_t<TPeerFactory>, 
		typename _TBinaryProtocol = std::decay_t<TBinaryProtocol>,
		typename _TInputMessageHandler = std::decay_t<TInputMessageHandler>,
		typename std::enable_if_t<(
			std::is_base_of_v<i_endpoint_t, _TEndPoint> &&
			std::is_base_of_v< i_xpeer_source_factory_t, _TPeerFactory> &&
			std::is_base_of_v< default_binary_protocol_handler_t, _TBinaryProtocol>&&
			std::is_base_of_v<i_message_input_t<input_message_knop_l0>, _TInputMessageHandler>
			), int> = 0>
	rt0_hub_settings_t(std::shared_ptr<distributed_ctx_t> ctx,
		const identity_descriptor_t& thisId,
		TEndPoint&& ep,
		TPeerFactory && ipeerFactory,
		TInputMessageHandler && inputStack,
		TBinaryProtocol && ph)
		: _ctx(ctx)
		, _thisId(thisId)
		, _ep(std::make_unique<_TEndPoint>(std::forward<TEndPoint>(ep)))
		, _ipeerFactory(std::make_unique< _TPeerFactory>(std::forward<TPeerFactory>( ipeerFactory)))
		, _inputStack(std::make_unique< _TInputMessageHandler>(std::forward<TInputMessageHandler>(inputStack)))
		, _p(std::make_unique<_TBinaryProtocol>(std::forward<TBinaryProtocol>(ph))){}

	template<typename TEndPoint,
		typename TInputMessageHandler,

		typename _TEndPoint = std::decay_t<TEndPoint>,
		typename _TInputMessageHandler = std::decay_t<TInputMessageHandler>,
		typename std::enable_if_t<(
			std::is_base_of_v<i_endpoint_t, _TEndPoint>&&
			std::is_base_of_v<i_message_input_t<input_message_knop_l0>, _TInputMessageHandler>
			), int> = 0>
	rt0_hub_settings_t(std::shared_ptr<distributed_ctx_t> ctx,
		const identity_descriptor_t& thisId,
		TEndPoint && ep,
		TInputMessageHandler && inputStack)
		: rt0_hub_settings_t(ctx,
			thisId,
			std::forward<TEndPoint>(ep),
			crm::detail::tcp::tcp_source_factory_t::create(ctx),
			std::forward<TInputMessageHandler>(inputStack),
			default_binary_protocol_handler_t(ctx)) {}

	template<typename TEndPoint,
		typename TInputMessageHandler,

		typename _TEndPoint = std::decay_t<TEndPoint>,
		typename std::enable_if_t<(
			std::is_base_of_v<i_endpoint_t, _TEndPoint>&&
			is_rt0_hub_input_handler_v<TInputMessageHandler>
			), int> = 0>
	rt0_hub_settings_t(std::shared_ptr<distributed_ctx_t> ctx,
		const identity_descriptor_t& thisId,
		TEndPoint&& ep,
		TInputMessageHandler && h)
		: rt0_hub_settings_t(ctx,
			thisId,
			std::forward<TEndPoint>(ep),
			input2handler_rt0_t<TInputMessageHandler>(std::forward<TInputMessageHandler>(h))) {}

	const std::shared_ptr<default_binary_protocol_handler_t>& protocol()const noexcept;
	const identity_descriptor_t& this_id()const noexcept;
	std::unique_ptr<i_endpoint_t> endpoint()const;
	std::shared_ptr<distributed_ctx_t> ctx()const noexcept;
	std::shared_ptr<i_message_input_t<message_stamp_t>> input_stack()const;
	std::shared_ptr<i_xpeer_source_factory_t> ipeer_factory()const;
	virtual syncdata_list_t local_object_syncdata();
	remote_object_connection_value_t local_object_identity();
	virtual void event_handler(std::unique_ptr<i_event_t>&& ev);
	virtual bool remote_object_identity_validate(const identity_value_t&,
		std::unique_ptr<i_peer_remote_properties_t>&);

	virtual datagram_t remote_object_initialize(syncdata_list_t&&);

	virtual void local_object_initialize(datagram_t&& /*dt*/,
		detail::object_initialize_result_guard_t&& resultHndl);

	virtual void ipeer_connection(ipeer_l0&& /*lnk*/, datagram_t&& /*dt*/);

	virtual void ipeer_disconnection(const detail::xpeer_base_rt0_t::xpeer_desc_t& /*desc*/, std::shared_ptr<i_peer_statevalue_t>&&);
	virtual std::unique_ptr<i_peer_statevalue_t> state_value()const;

	virtual void run();
	};

template<typename S, typename = void>
struct is_hub_settings_rt0 : std::false_type {};

template<typename S>
struct is_hub_settings_rt0 < S,
	typename std::enable_if_t<std::is_base_of_v<rt0_hub_settings_t, S>>> : std::true_type {};

template<typename S>
constexpr bool is_hub_settings_rt0_v = is_hub_settings_rt0<S>::value;

}
