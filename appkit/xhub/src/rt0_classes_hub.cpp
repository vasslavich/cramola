#include "../../internal.h"
#include "../../functorapp/functorapp.h"
#include "../rt0_hub_settings.h"


using namespace crm;
using namespace crm::detail;


const identity_descriptor_t& rt0_hub_settings_t::this_id()const noexcept{
	return _thisId;
	}

void rt0_hub_settings_t::event_handler(std::unique_ptr<i_event_t>&& ) {}

std::unique_ptr<i_endpoint_t> rt0_hub_settings_t::endpoint()const{
	return _ep->copy();
	}

std::shared_ptr<distributed_ctx_t> rt0_hub_settings_t::ctx()const noexcept{
	return _ctx.lock();
	}

std::shared_ptr<i_message_input_t<rt0_X_item_t>> rt0_hub_settings_t::input_stack()const{
	return _inputStack;
	}

std::shared_ptr<i_xpeer_source_factory_t> rt0_hub_settings_t::ipeer_factory()const{
	return _ipeerFactory;
	}

syncdata_list_t rt0_hub_settings_t::local_object_syncdata(){
	return syncdata_list_t();
	}

remote_object_connection_value_t rt0_hub_settings_t::local_object_identity(){
	identity_value_t identity;
	identity.id_value = _thisId;

	return remote_object_connection_value_t( std::move( identity ), local_object_syncdata() );
	}

bool rt0_hub_settings_t::remote_object_identity_validate( const identity_value_t &,
											  std::unique_ptr<i_peer_remote_properties_t> & ){

	return true;
	}

datagram_t rt0_hub_settings_t::remote_object_initialize( syncdata_list_t && ){
	return datagram_t();
	}

void rt0_hub_settings_t::local_object_initialize( datagram_t && /*dt*/,
									  object_initialize_result_guard_t && resultHndl ){

	resultHndl( datagram_t() );
	}

void rt0_hub_settings_t::ipeer_connection(ipeer_l0&& /*lnk*/, datagram_t && /*dt*/ ){}

void rt0_hub_settings_t::ipeer_disconnection( const ipeer_l0::xpeer_desc_t & /*desc*/, std::shared_ptr<i_peer_statevalue_t> && ){}

std::unique_ptr<i_peer_statevalue_t> rt0_hub_settings_t::state_value()const{
	return nullptr;
	}

const std::shared_ptr<default_binary_protocol_handler_t>& rt0_hub_settings_t::protocol()const noexcept {
	return _p;
	}

void rt0_hub_settings_t::run() {}

