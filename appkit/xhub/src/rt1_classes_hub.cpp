#include "../../internal.h"
#include "../rt1_hub_settings.h"


using namespace crm;
using namespace crm::detail;


void rt1_hub_settings_t::assign( rt1_hub_settings_t && o )noexcept{
	_ctx = std::move( o._ctx );
	_thisId = std::move( o._thisId );
	_protocol = std::move( o._protocol );
	_inputStackCtr = std::move( o._inputStackCtr );
	}

rt1_hub_settings_t::rt1_hub_settings_t()noexcept{}

rt1_hub_settings_t::rt1_hub_settings_t( std::shared_ptr<distributed_ctx_t> ctx,
	const identity_descriptor_t & thisId,
	std::unique_ptr<default_binary_protocol_handler_t> protocol,
	std::unique_ptr<i_input_messages_stock_t> && inputStackCtr )
	: _ctx( ctx )
	, _thisId( thisId )
	, _inputStackCtr( std::move( inputStackCtr ) )
	, _protocol( std::move(protocol)) {}

rt1_hub_settings_t::rt1_hub_settings_t(std::shared_ptr<distributed_ctx_t> ctx,
	const identity_descriptor_t& thisId,
	std::unique_ptr<i_input_messages_stock_t>&& inputStackCtr)
	: rt1_hub_settings_t( ctx,
		thisId,
		std::make_unique<default_binary_protocol_handler_t>(ctx),
		std::move( inputStackCtr ) ){}

rt1_hub_settings_t::rt1_hub_settings_t( rt1_hub_settings_t  && o )noexcept{
	assign( std::move( o ) );
	}

rt1_hub_settings_t& rt1_hub_settings_t::operator=( rt1_hub_settings_t && o )noexcept{
	assign( std::move( o ) );
	return ( *this );
	}

std::shared_ptr<distributed_ctx_t> rt1_hub_settings_t::ctx()const noexcept{
	return _ctx.lock();
	}

const identity_descriptor_t& rt1_hub_settings_t::this_id()const noexcept{
	return _thisId;
	}

const std::unique_ptr<default_binary_protocol_handler_t>& rt1_hub_settings_t::protocol()const noexcept{
	return _protocol;
	}

void rt1_hub_settings_t::connection_handler( ipeer_t &&, datagram_t && ){}
void rt1_hub_settings_t::disconnection_handler( const i_xpeer_rt1_t::xpeer_desc_t &, std::shared_ptr<i_peer_statevalue_t> && ){}

syncdata_list_t rt1_hub_settings_t::local_object_syncdata(){
	return syncdata_list_t();
	}

remote_object_connection_value_t rt1_hub_settings_t::local_object_identity(){
	identity_value_t identity;
	identity.id_value = _thisId;

	return remote_object_connection_value_t( std::move( identity ), local_object_syncdata() );
	}

bool rt1_hub_settings_t::remote_object_identity_validate( const identity_value_t &,
														  std::unique_ptr<i_peer_remote_properties_t> & ){

	return true;
	}

datagram_t rt1_hub_settings_t::remote_object_initialize( syncdata_list_t && ){
	return crm::datagram_t();
	}

void rt1_hub_settings_t::local_object_initialize( datagram_t && /*dt*/,
	object_initialize_result_guard_t && resultHndl ){

	resultHndl( datagram_t() );
	}

std::unique_ptr<i_peer_statevalue_t> rt1_hub_settings_t::state_value(){
	return nullptr;
	}

std::unique_ptr<i_input_messages_stock_t> rt1_hub_settings_t::input_stack(){
	return std::move( _inputStackCtr );
	}

void rt1_hub_settings_t::event_handler( std::unique_ptr<i_event_t> &&  ){}

void rt1_hub_settings_t::opeer_fail( const remote_connection_desc_t & /*opeer*/,
									 std::unique_ptr<dcf_exception_t> && /*exc*/ ){}


void rt1_hub_settings_t::set_protocol(std::unique_ptr<default_binary_protocol_handler_t> && p) {
	_protocol = std::move(p);
	}

void rt1_hub_settings_t::set_input_stack(std::unique_ptr<i_input_messages_stock_t> && ic) {
	_inputStackCtr = std::move(ic);
	}

void rt1_hub_settings_t::set_context(std::shared_ptr<distributed_ctx_t> ctx) {
	_ctx = ctx;
	}

void rt1_hub_settings_t::set_this_id(const identity_descriptor_t & thisId) {
	_thisId = thisId;
	}



