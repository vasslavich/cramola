#pragma once


#include "./base_terms.h"
#include "./notification_utility.h"
#include "./base_exception_terms.h"


namespace crm {


//std::unique_ptr<dcf_exception_t> cast2exc(std::unique_ptr<dcf_exception_t> && isrlz)noexcept;

//
//bool cstr_ptr_is_null(const char* p);
//bool cstr_ptr_is_null(const wchar_t* p);
//bool cstr_ptr_is_null(int p);


template<typename T>
const std::unique_ptr<T>& CHECK_PTR_STRONG(const std::unique_ptr<T> & ptr) {
	if (ptr)
		return ptr;
	else {
		FATAL_ERROR_FWD(typeid(T).name());
		}
	}

template<typename T>
std::shared_ptr<T> CHECK_PTR_STRONG(const std::shared_ptr<T> & ptr) {
	if (ptr)
		return ptr;
	else {
		FATAL_ERROR_FWD(typeid(T).name());
		}
	}

template<typename T>
std::shared_ptr<T> CHECK_PTR(std::shared_ptr<T> && ptr) {
	if (ptr)
		return std::move(ptr);
	else {
		THROW_EXC_FWD(typeid(T).name());
		}
	}

template<typename T>
std::shared_ptr<const T> CHECK_PTR(std::shared_ptr<const T> && ptr) {
	if (ptr)
		return std::move(ptr);
	else {
		THROW_EXC_FWD(typeid(T).name());
		}
	}

template<typename T>
std::shared_ptr<T> CHECK_PTR(const std::shared_ptr<T> & ptr) {
	if (ptr)
		return ptr;
	else {
		THROW_EXC_FWD(typeid(T).name());
		}
	}

template<typename T>
std::shared_ptr<T> CHECK_PTR(const std::weak_ptr<T> & ptr) {
	auto obj(ptr.lock());
	if (obj)
		return std::move(obj);
	else
		THROW_EXC_FWD(typeid(T).name());
	}

template<typename T>
T* CHECK_PTR(const std::unique_ptr<T> & ptr) {
	if (ptr) {
		return ptr.get();
		}
	else
		THROW_EXC_FWD(typeid(T).name());
	}


template<typename T>
typename T* CHECK_PTR(T * ptr) {
	if (ptr) {
		return ptr;
		}
	else
		THROW_EXC_FWD(typeid(T).name());
	}
}



#define CHECK_NO_EXCEPTION_BEGIN {\
try{\

#define CHECK_NO_EXCEPTION_END }\
	catch (const crm::mpf_test_exception_t& exc_x2__) {\
		FATAL_ERROR_FWD(exc_x2__);\
		}\
	catch( const crm::dcf_exception_t& exc_x0__ ){\
		FATAL_ERROR_FWD(exc_x0__) \
		}\
	catch( const std::exception & exc_x1__ ){\
		FATAL_ERROR_FWD(exc_x1__) \
		}\
	catch(...){\
		FATAL_ERROR_FWD(nullptr) \
		}\
	}


#define CBL_NO_EXCEPTION_BEGIN {\
try{\

#define CBL_NO_EXCEPTION_END(spCtx_)\
 }\
	catch (const crm::mpf_test_exception_t& exc_x2__) {\
		FATAL_ERROR_FWD(exc_x2__);\
		}\
	catch( const crm::dcf_exception_t& exc_x0__ ){\
		if(auto pctx = (spCtx_)){\
			pctx->exc_hndl(exc_x0__.dcpy());\
			}\
		}\
	catch( const std::exception & exc_x1__ ){\
		if(auto pctx = (spCtx_)){\
			pctx->exc_hndl(CREATE_MPF_PTR_EXC_FWD( exc_x1__ ));\
			}\
		}\
	catch(...){\
		if(auto pctx = (spCtx_)){\
			pctx->exc_hndl(CREATE_MPF_PTR_EXC_FWD(nullptr));\
			}\
		}\
	}



#define CHECK_NO_EXCEPTION(expression){ \
	try{ \
		(expression);\
		}\
	catch( const crm::dcf_exception_t& exc_x0__ ){\
		FATAL_ERROR_FWD(exc_x0__) \
		}\
	catch( const std::exception & exc_x1__ ){\
		FATAL_ERROR_FWD(exc_x1__) \
		}\
	catch(...){\
		FATAL_ERROR_FWD(nullptr) \
		}\
	}

