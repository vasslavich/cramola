#pragma once


#include "./base_exception_terms.h"


namespace crm{


struct i_notification_provider_t {
	virtual ~i_notification_provider_t() = 0;

	/*! �������� ����������� � ���� */
	virtual void ntf_hndl(std::unique_ptr<dcf_exception_t> && ntf)noexcept = 0;
	/*! �������� ���������� � ���� */
	virtual void ntf_hndl(dcf_exception_t && exc)noexcept = 0;
	/*! �������� ���������� � ���� */
	virtual void ntf_hndl(const dcf_exception_t & exc)noexcept = 0;
	};
}
