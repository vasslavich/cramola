#pragma once


#include <string>
#include "../internal_invariants.h"


namespace crm {

std::wstring get_system_error()noexcept;
std::wstring get_system_error(sys_error_code_t code)noexcept;
}

