#pragma once


#include <string>
#include <memory>
#include "../internal_invariants.h"


namespace crm {

class dcf_exception_t;

class fatal_error_t {
public:
	[[noreturn]]
	static void break_on_fatal_error()noexcept;

	[[noreturn]]
	static void break_on_fatal_error(
		const std::wstring& msg, const std::wstring& file, const int line)noexcept;

	[[noreturn]]
	static void break_on_fatal_error(
		const std::string& msg, const std::wstring& file, const int line)noexcept;

	[[noreturn]]
	static void break_on_fatal_error(
		const char* msg, const std::wstring& file, const int line)noexcept;

	[[noreturn]]
	static void break_on_fatal_error(
		const wchar_t* msg, const std::wstring& file, const int line)noexcept;

	[[noreturn]]
	static void break_on_fatal_error(
		std::nullptr_t, const std::wstring& file, const int line)noexcept;

	[[noreturn]]
	static void break_on_fatal_error(const dcf_exception_t& exc)noexcept;

	[[noreturn]]
	static void break_on_fatal_error(const std::exception& exc,
		const std::wstring& file, const int line)noexcept;

	[[noreturn]]
	static void break_on_fatal_error(const crm::dcf_exception_t& exc,
		const std::wstring& file, const int line)noexcept;

	static void global_stout(
		std::wstring_view msg, const int type, std::wstring_view file, const int line)noexcept;
	static void global_stout(
		std::string_view msg, const int type, std::wstring_view file, const int line)noexcept;
	};

bool is_has_canceled(const std::unique_ptr<dcf_exception_t>& p)noexcept;
bool is_has_exception(const std::unique_ptr<dcf_exception_t>& p)noexcept;
}

#define FATAL_ERROR_FWD(v){\
	crm::fatal_error_t::break_on_fatal_error((v), __CBL_FILEW__, __LINE__);\
	}	

//
//#define FATAL_TERMINATE_(msg){\
//	crm::fatal_error_t::break_on_fatal_error((msg), __CBL_FILEW__, __LINE__);\
//	}
//
//#define FATAL_TERMINATE_FWD(msg){\
//	crm::fatal_error_t::break_on_fatal_error((msg), __CBL_FILEW__, __LINE__);\
//	}
//
//#define FATAL_TERMINATE_STR(msg){\
//	crm::fatal_error_t::break_on_fatal_error((msg), __CBL_FILEW__, __LINE__);\
//	}
//
//#define FATAL_TERMINATE_FWDSTR(msg){\
//	crm::fatal_error_t::break_on_fatal_error((msg), __CBL_FILEW__, __LINE__);\
//	}
//
//#define FATAL_ERROR_FWD(exc){\
//	crm::fatal_error_t::break_on_fatal_error((exc),(exc).file(), (exc).line() );\
//	}
//
//#define FATAL_ERROR_FWD(exc){\
//	crm::fatal_error_t::break_on_fatal_error((exc), __CBL_FILEW__, __LINE__);\
//	}
//
//#define FATAL_ERROR_FWD(msg){\
//	crm::fatal_error_t::break_on_fatal_error((msg), __CBL_FILEW__, __LINE__);\
//	}	
//
//#define FATAL_ERROR_FWD(msg){\
//	crm::fatal_error_t::break_on_fatal_error((msg), __CBL_FILEW__, __LINE__);\
//	}
//
//#define FATAL_ERROR_FWD(msg){\
//	crm::fatal_error_t::break_on_fatal_error((msg), __CBL_FILEW__, __LINE__);\
//	}
//
//#define FATAL_ERROR_FWD(msg){\
//	crm::fatal_error_t::break_on_fatal_error((msg), __CBL_FILEW__, __LINE__);\
//	}





#ifdef USE_DCF_CHECK

#define CBL_VERIFY(expression){\
	if( !(expression) ){\
		FATAL_ERROR_FWD(nullptr);\
		}\
	}

#define ERR_VERIFY_TEXT(expression,msg){\
	if( !(expression) ){\
		FATAL_ERROR_FWD( (msg) );\
		}\
	}

//#define ERR_VERIFY_C(expression,msg){\
//	if( !(expression) ){\
//	FATAL_ERROR_FWD( (msg) );\
//		}\
//	}
//
//#define ERR_VERIFY_STR(expression,msg){\
//	if( !(expression) ){\
//	FATAL_TERMINATE_STR( (msg) );\
//				}\
//		}

#else///USE_DCF_CHECK

#define CBL_VERIFY(expression)
#define ERR_VERIFY_TEXT(expression,msg)

#endif///USE_DCF_CHECK


#define THROW_EXC_FWD(v){ \
	throw crm::dcf_exception_t((v), __CBL_FILEW__, __LINE__);\
	}


#define CREATE_EXC_FWD(v) (::crm::dcf_exception_t((v), __CBL_FILEW__, __LINE__))
#define CREATE_PTR_EXC_FWD(v) (std::make_unique<crm::dcf_exception_t>((v), __CBL_FILEW__, __LINE__))







