#pragma once


#include <cerrno>
#include <string>
#include <chrono>
#include <vector>
#include <memory>
#include <exception>
#include "../typeframe/i_tpdriver.h"
#include "../typeframe/isrlz.h"
#include "../typeframe/typeid.h"
#include "../typeframe/base_terms.h"
#include "./base_macrodef.h"
#include "../utilities/stringscoped/string_trait_classes.h"
#include "../utilities/stringscoped/string_trait_using.h"

namespace crm {

template<typename T>
constexpr bool is_string_error_constructible_v = std::is_constructible_v<std::string, T> || std::is_constructible_v<std::wstring, T>;

class dcf_exception_t :
	public srlz::serialized_base_rw,
	public std::runtime_error{

	static std::atomic<std::uintmax_t> IdGen;

public:
	using hash_type = std::uintmax_t;
	using key_t = std::uintmax_t;

private:
	/*! ������������ ����� � ������ ������������ �������� ������� */
	static const size_t time_point_length = 32;

	/// the text of a message
	std::wstring _msg;
	mutable std::string _cmsgCache;
	/// the name of a file that was a source of error
	std::wstring _file;
	/// ����� ��������� ����������
	std::chrono::system_clock::time_point _timePoint = std::chrono::system_clock::now();
	/// the line number in file that was a source of error
	int _line = 0;
	/// ��� ������
	int _code = 0;
	/* ������������� ������� */
	hash_type _id;
	key_t _key;

	void update_state();

	template<typename T>
	struct fail_detect : std::false_type {};

	void serialize(srlz::detail::wstream_base_handler& dest)const override;
	void deserialize(srlz::detail::rstream_base_handler& src)override;

public:
	dcf_exception_t()noexcept;

	dcf_exception_t(const std::exception& o)noexcept;
	dcf_exception_t(const std::exception& o, 
		const std::wstring& fileSource, 
		const int lineSource, 
		const int code = -1)noexcept;

	template<typename TString,
		typename = std::enable_if_t<(
			is_string_class_v<TString> ||
			is_string_error_constructible_v<TString>
			)>>
	explicit dcf_exception_t( TString && msg)noexcept
		: dcf_exception_t(std::forward<TString>(msg), __FILEW__, __LINE__){}

	template<typename TString,
		typename = std::enable_if_t<(
			is_string_class_v<TString> ||
			is_string_error_constructible_v<TString>
			)>>
	explicit dcf_exception_t( TString && msg,
		std::wstring_view fileSource_, 
		const int lineSource_, 
		const int code_ = -1)noexcept
		: std::runtime_error(format_message(msg))
		, _msg(format_message<wchar_t>(std::forward<TString>(msg)))
		, _file(fileSource_)
		, _line(lineSource_)
		, _code(code_)
		, _id(++IdGen) {

		update_state();

#ifdef CBL_TRACE_EXCEPTION_RAISE
		file_logger_t::logging(_msg, 0, _file, _line);
#endif
		}

	const char* what()const noexcept override;
	/// the c-string of message
	const std::wstring& msg()const noexcept;
	/// the c-string of a file name of source error
	const std::wstring& file()const noexcept;
	/// the line number of a source of error
	int line()const noexcept;
	/// ��� ������
	int code()const noexcept;
	/// ����� ��������� ����������
	const std::chrono::system_clock::time_point& time()const noexcept;

	const hash_type& hash()const noexcept;

	void add_message_line(std::wstring&& msg)noexcept;
	void set_message(std::wstring&& msg)noexcept;
	void set_code(int tag)noexcept;

	key_t key()const noexcept;

	/*! ����� ������� */
	virtual std::unique_ptr<dcf_exception_t> dcpy()const noexcept;

	/*! ������������� ���� */
	virtual const srlz::i_typeid_t& type_id()const noexcept;

	[[noreturn]]
	virtual void rethrow()const;

	template<typename Tws>
	void srlz(Tws& sout)const {

		/* ����
		-------------------------------------------------*/
		srlz::serialize(sout, _file);

		/* ������
		-----------------------------------------------------*/
		srlz::serialize(sout, _line);

		/* ���
		-----------------------------------------------------*/
		srlz::serialize(sout, _code);

		/* �������������
		-----------------------------------------------------*/
		srlz::serialize(sout, _id);

		/* ���������
		-----------------------------------------------------*/
		srlz::serialize(sout, _msg);

		/* �����
		---------------------------------------------------- */
		streamed_time_t tl = _timePoint.time_since_epoch().count();
		srlz::serialize(sout, tl);

		/* ���� */
		srlz::serialize(sout, _key);
		}

	template<typename Trs>
	void dsrlz(Trs& sin) {

		/* ����
		----------------------------------------------*/
		srlz::deserialize(sin, _file);


		/* ������
		----------------------------------------------*/
		srlz::deserialize(sin, _line);

		/* ���
		-----------------------------------------------------*/
		srlz::deserialize(sin, _code);

		/* ������������� */
		srlz::deserialize(sin, _id);

		/* ���������
		----------------------------------------------*/
		srlz::deserialize(sin, _msg);

		/* �����
		---------------------------------------------------- */
		streamed_time_t tl;
		srlz::deserialize(sin, tl);
		_timePoint = std::chrono::system_clock::time_point(std::chrono::system_clock::duration(tl));

		/* ���� */
		srlz::deserialize(sin, _key);
		}

	template<typename Tws>
	static bool serialize(const i_types_driver_t& tdrv, Tws&& dest, const dcf_exception_t& e)noexcept {

		auto excDrv(tdrv.exceptions_driver());
		if (excDrv) {
			excDrv->serialize(std::forward<Tws>(dest), e);
			return true;
			}
		else {
			return false;
			}
		}

	template<typename Trs>
	static std::unique_ptr<dcf_exception_t> deserialize(const i_types_driver_t& tdrv, Trs&& src)noexcept {
		auto excDrv(tdrv.exceptions_driver());
		if (excDrv) {
			return excDrv->deserialize(std::forward<Trs>(src));
			}
		else {
			return nullptr;
			}
		}
	};


bool is_mpsys_exception(const std::unique_ptr<dcf_exception_t>& e)noexcept;
bool is_terminate_operation_exception(const std::unique_ptr<dcf_exception_t>& e)noexcept;
bool is_extern_code_exception(const std::unique_ptr<crm::dcf_exception_t>& e)noexcept;
}

