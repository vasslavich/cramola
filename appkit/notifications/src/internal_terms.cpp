#include "../../internal.h"
#include "../../logging/logger.h"


using namespace crm;
using namespace crm::detail;

//
//std::unique_ptr<dcf_notification_t> crm::cast2ntf(std::unique_ptr<dcf_exception_t> && isrlz)noexcept {
//
//	if(isrlz) {
//		auto pNtf = dynamic_cast<dcf_notification_t*>(isrlz.get());
//		if(pNtf) {
//			isrlz.release();
//			std::unique_ptr<dcf_notification_t> pBase(pNtf);
//			return std::move(pBase);
//			}
//		else {
//			return {};
//			}
//		}
//	else {
//		return {};
//		}
//	}
//
//std::unique_ptr<dcf_exception_t> crm::cast2exc(std::unique_ptr<dcf_exception_t> && isrlz)noexcept {
//
//	if(isrlz) {
//		auto pNtf = dynamic_cast<dcf_exception_t*>(isrlz.get());
//		if(pNtf) {
//			isrlz.release();
//			std::unique_ptr<dcf_exception_t> pBase(pNtf);
//			return std::move(pBase);
//			}
//		else {
//			return {};
//			}
//		}
//	else {
//		return {};
//		}
//	}
//

void fatal_error_t::global_stout(
	std::wstring_view msg, const int type, std::wstring_view file, const int line)noexcept {

	file_logger_t::global_stout(msg, type, file, line );
	}

void fatal_error_t::global_stout(
	std::string_view msg, const int type, std::wstring_view file, const int line)noexcept {

	file_logger_t::global_stout(msg, type, file, line);
	}

void fatal_error_t::break_on_fatal_error()noexcept {
	break_on_fatal_error("", __CBL_FILEW__, __LINE__);
	}

void fatal_error_t::break_on_fatal_error(const dcf_exception_t & exc) noexcept {
	break_on_fatal_error(exc.msg(), exc.file(), exc.line());
	}

void fatal_error_t::break_on_fatal_error(
	const std::wstring &msg, const std::wstring &file, const int line)noexcept {

	break_on_fatal_error(msg.c_str(), file, line);
	}

void fatal_error_t::break_on_fatal_error(
	const std::string & msg, const std::wstring &file, const int line)noexcept {

	break_on_fatal_error(msg.c_str(), file, line);
	}

void fatal_error_t::break_on_fatal_error(
	const char* msg, const std::wstring &file, const int line) noexcept {

	std::string text("FATAL ERROR:");
	if (msg)
		text += msg;

	file_logger_t::logging(text, 0x1, file, line);

	__debugbreak();
	std::exit(EXIT_FAILURE);
	}


void fatal_error_t::break_on_fatal_error(
	const wchar_t* msg, const std::wstring &file, const int line)noexcept {

	std::wstring text(L"FATAL ERROR:");
	if (msg)
		text += msg;

	file_logger_t::logging(text, 0x1, file, line);

	__debugbreak();
	std::exit(EXIT_FAILURE);
	}

void fatal_error_t::break_on_fatal_error(
	std::nullptr_t, const std::wstring& file, const int line)noexcept {

	break_on_fatal_error((const wchar_t*)(nullptr), file, line);
	}

void fatal_error_t::break_on_fatal_error(
	const std::exception & exc, const std::wstring &file, const int line)noexcept {

	break_on_fatal_error(exc.what(), file, line);
	}

void fatal_error_t::break_on_fatal_error(
	const dcf_exception_t & exc, const std::wstring &file, const int line)noexcept {

	break_on_fatal_error(exc.msg(), file, line);
	}

//bool crm::cstr_ptr_is_null(const char* p) {
//	if(p)
//		return false;
//	else
//		return true;
//	}
//
//bool crm::cstr_ptr_is_null(const wchar_t* p) {
//	if(p)
//		return false;
//	else
//		return true;
//	}
//
//bool crm::cstr_ptr_is_null(int p) {
//	if(p)
//		return false;
//	else
//		return true;
//	}
