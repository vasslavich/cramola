#include <atomic>
#include "../../internal.h"
#include "../../typeframe.h"
#include "../../utilities/convert/cvt.h"
#include "../base_terms.h"
#include "../../notifications/exceptions/excdrv.h"


using namespace crm;


static_assert(std::is_base_of_v<srlz::serialized_base_rw, dcf_exception_t>, __FILE_LINE__);
static_assert(std::is_default_constructible_v<dcf_exception_t>, __FILE_LINE__);
static_assert(srlz::detail::is_serialized_trait_v<dcf_exception_t>, __FILE_LINE__);
static_assert(srlz::is_serializable_v<dcf_exception_t>, __FILE_LINE__);


/*=============================================================================================
			dcf_exception_t
===============================================================================================*/


std::unique_ptr<dcf_exception_t> dcf_exception_t::dcpy()const noexcept {
	return std::make_unique<dcf_exception_t>(*this);
	}


typedef crm::srlz::fram_typectr_default_t<dcf_exception_t > typectr_exception_t;
static typectr_exception_t tctrExc;

const srlz::i_typeid_t& dcf_exception_t::type_id()const noexcept {
	return tctrExc;
	}

void dcf_exception_t::rethrow()const {
	throw (*this);
	}

std::atomic<std::uintmax_t> dcf_exception_t::IdGen{ 0 };

void dcf_exception_t::serialize(srlz::detail::wstream_base_handler & dest)const {
	srlz(dest);
	}

void dcf_exception_t::deserialize(srlz::detail::rstream_base_handler & src) {
	dsrlz(src);
	}


dcf_exception_t::dcf_exception_t()noexcept
	:dcf_exception_t(std::string{}) {}

dcf_exception_t::dcf_exception_t(const std::exception& o)noexcept
	: dcf_exception_t(std::string(o.what())) {}

dcf_exception_t::dcf_exception_t(const std::exception& o,
	const std::wstring& fileSource,
	const int lineSource,
	const int code)noexcept
	: dcf_exception_t(std::string(o.what()), fileSource, lineSource, code) {}

dcf_exception_t::key_t dcf_exception_t::key()const noexcept {
	return _key;
	}

const char* dcf_exception_t::what()const noexcept {
	if (_cmsgCache.empty()) {
		_cmsgCache = cvt(_msg);
		}

	return _cmsgCache.c_str();
	}

const std::wstring& dcf_exception_t::msg()const noexcept {
	return _msg;
	}

const std::wstring& dcf_exception_t::file()const noexcept {
	return _file;
	}

int dcf_exception_t::line()const noexcept {
	return _line;
	}

int dcf_exception_t::code()const noexcept {
	return _code;
	}

const std::chrono::system_clock::time_point& dcf_exception_t::time()const noexcept {
	return _timePoint;
	}

void dcf_exception_t::set_message(std::wstring && msg) noexcept {
	_msg = std::move(msg);
	update_state();
	}

void dcf_exception_t::add_message_line(std::wstring && msg) noexcept {
	_msg += std::move(msg);
	update_state();
	}

void dcf_exception_t::set_code(int c)noexcept {
	_code = c;
	update_state();
	}

void dcf_exception_t::update_state() {
	address_hash_t h0;

	apply_at_hash(h0, _msg);
	apply_at_hash(h0, _file);
	//apply_at_hash(h0, _timePoint);
	apply_at_hash(h0, _line);
	apply_at_hash(h0, _code);
	//apply_at_hash(h0, _id);

	_key = get_hash_std(h0);
	}







