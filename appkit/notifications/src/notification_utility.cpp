#include <Windows.h>
#include <atlbase.h>
#include "../../internal.h"
#include "../notification_utility.h"


using namespace crm;


std::wstring crm::get_system_error() noexcept {
	return get_system_error(GetLastError());
	}

std::wstring crm::get_system_error(sys_error_code_t code)noexcept {
	TCHAR buf[1024];
	std::wstring msg;

	const DWORD nwr = FormatMessage(
		FORMAT_MESSAGE_FROM_SYSTEM,
		NULL,
		code,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)buf,
		1024,
		NULL
	);

	if(nwr) {
		msg = ((CT2WEX<1024>(buf)));
		return std::move(msg);
		}
	else {
		return std::wstring(std::to_wstring(code) + L":" + std::to_wstring(nwr));
		}
	}

