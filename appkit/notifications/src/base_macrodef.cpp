#include "../../internal.h"

using namespace crm;

bool crm::is_has_canceled(const std::unique_ptr<dcf_exception_t>& p)noexcept {
	return canceled_notification::type_code == p->code();
	}

bool crm::is_has_exception(const std::unique_ptr<dcf_exception_t>& p)noexcept {
	return p != nullptr;
	}

bool crm::is_mpsys_exception(const std::unique_ptr<crm::dcf_exception_t>& e)noexcept {
	return nullptr != dynamic_cast<const mpf_exception_t*>(e.get());
	}

bool crm::is_terminate_operation_exception(const std::unique_ptr<dcf_exception_t>& e)noexcept {
	return nullptr != dynamic_cast<const terminate_operation_exception_t*>(e.get());
	}

bool crm::is_extern_code_exception(const std::unique_ptr<crm::dcf_exception_t>& e)noexcept {
	return nullptr != dynamic_cast<const extern_code_exception_t*>(e.get());
	}



