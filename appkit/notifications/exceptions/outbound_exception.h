#pragma once

#include "../base_exception_terms.h"
#include "../base_predecl.h"

namespace crm {


class outbound_exception_t : public mpf_exception_t {
public:
	using base_type_t = mpf_exception_t ;

public:
	outbound_exception_t()noexcept;

	template<typename T,
		typename = std::enable_if_t<std::is_constructible_v<base_type_t>, T>>
		explicit outbound_exception_t(T&& t)noexcept
		: base_type_t(std::forward<T>(t)) {}

	template<typename T,
		typename = std::enable_if_t<std::is_constructible_v<base_type_t, T, std::wstring, int, int>>>
		explicit outbound_exception_t(
			T&& msg, const std::wstring& fileSource, const int lineSource, const int code = -1)noexcept
		: base_type_t(std::forward<T>(msg), fileSource, lineSource, code) {}
	
	std::unique_ptr<dcf_exception_t> dcpy()const noexcept final;
	const srlz::i_typeid_t & type_id()const noexcept final;	
	void rethrow()const final;
	};
}


#define CREATE_OUTBOUND_EXC_FWD(v)					(crm::outbound_exception_t((v), __CBL_FILEW__, __LINE__))
#define CREATE_OUTBOUND_PTR_EXC_FWD(v)				(std::make_unique<crm::outbound_exception_t>((v), __CBL_FILEW__, __LINE__))


#define OUTBOUND_THROW_EXC_FWD(v) {	\
	throw crm::outbound_exception_t((v), __CBL_FILEW__, __LINE__); \
									}		


