#pragma once


#include <memory>
#include "../internal_terms.h"


namespace crm {


class mpf_exception_t : public crm::dcf_exception_t {
public:
	typedef crm::dcf_exception_t base_type_t;

public:
	mpf_exception_t()noexcept;

	template<typename T,
		typename = std::enable_if_t<std::is_constructible_v<base_type_t>, T>>
		explicit mpf_exception_t(T&& t)noexcept
		: base_type_t(std::forward<T>(t)) {}

	template<typename T,
		typename = std::enable_if_t<std::is_constructible_v<base_type_t, T, std::wstring, int, int>>>
		explicit mpf_exception_t(
			T&& msg, const std::wstring& fileSource, const int lineSource, const int code = -1)noexcept
		: base_type_t(std::forward<T>(msg), fileSource, lineSource, code) {}

	std::unique_ptr<dcf_exception_t> dcpy()const noexcept override;
	const crm::srlz::i_typeid_t& type_id()const noexcept override;

private:
	[[noreturn]]
	void rethrow()const override;
	};
}



#define MPF_THROW_EXC_FWD(v)		{ \
	throw crm::mpf_exception_t((v), __CBL_FILEW__, __LINE__); \
									}


#define THROW_MPF_EXC_FWD(v) {throw crm::mpf_exception_t((v), __CBL_FILEW__, __LINE__);}
#define CREATE_MPF_EXC_FWD(v) (crm::mpf_exception_t((v), __CBL_FILEW__, __LINE__))
#define CREATE_MPF_PTR_EXC_FWD(v)	(std::make_unique<crm::mpf_exception_t>((v), __CBL_FILEW__, __LINE__))


//#define MPF_TRY_LOCK_EXCEPTION(expression){ \
//	try{ \
//		(expression);\
//								}\
//	catch( const crm::dcf_exception_t& exc ){\
//		logger_t::logging(exc.msg(), 100100100, (int)exc.type_class(), __FILE_LINE__); \
//						}\
//	catch( const std::exception & exc1 ){\
//		logger_t::logging(exc1.what(), 100100100, 10, __FILE_LINE__) \
//						}\
//	catch(...){\
//		logger_t::logging("", 100100100, 10, __FILE_LINE__) \
//						}\
//					}


//#define EXCEPTION_SCOPED_LOCK(expression){ \
//	try{ \
//		(expression);\
//		}\
//	catch( const crm::dcf_exception_t& e0 ){\
//		if(auto c = ctx()){\
//			CHECK_NO_EXCEPTION(c->ntf_hndl(e0.dcpy()));\
//			}\
//		}\
//	catch( const std::exception & e1 ){\
//		if(auto c = ctx()){\
//			CHECK_NO_EXCEPTION(c->ntf_hndl(CREATE_PTR_EXC_FWD(e1)));\
//			}\
//		}\
//	catch(...){\
//		if(auto c = ctx()){\
//			CHECK_NO_EXCEPTION(c->ntf_hndl(CREATE_PTR_EXC_FWD(nullptr)));\
//			}\
//		}\
//	}
//
//
//#define EXCEPTION_SCOPED_LOCK_PCTX(pctx_, expression){ \
//	auto pctx = (pctx_);\
//	try{ \
//		(expression);\
//		}\
//	catch( const crm::dcf_exception_t& e0 ){\
//		if(pctx){\
//			CHECK_NO_EXCEPTION(pctx->ntf_hndl(e0.dcpy()));\
//			}\
//		}\
//	catch( const std::exception & e1 ){\
//		if(pctx){\
//			CHECK_NO_EXCEPTION(pctx->ntf_hndl(CREATE_PTR_EXC_FWD(e1)));\
//			}\
//		}\
//	catch(...){\
//		if(pctx){\
//			CHECK_NO_EXCEPTION(pctx->ntf_hndl(CREATE_PTR_EXC_FWD(nullptr)));\
//			}\
//		}\
//	}
//

namespace crm {
struct invoke_no_throw {};
}







