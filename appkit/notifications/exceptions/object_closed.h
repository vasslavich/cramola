#pragma once


#include "../base_exception_terms.h"


namespace crm{


class object_closed_t : public dcf_exception_t{
public:
	typedef dcf_exception_t base_type_t;

public:
	object_closed_t()noexcept;

	template<typename T,
		typename = std::enable_if_t<std::is_constructible_v<base_type_t>, T>>
		explicit object_closed_t(T&& t)noexcept
		: base_type_t(std::forward<T>(t)) {}

	template<typename T,
		typename = std::enable_if_t<std::is_constructible_v<base_type_t, T, std::wstring, int, int>>>
		explicit object_closed_t(
			T&& msg, const std::wstring& fileSource, const int lineSource, const int code = -1)noexcept
		: base_type_t(std::forward<T>(msg), fileSource, lineSource, code) {}

	std::unique_ptr<dcf_exception_t> dcpy()const noexcept override;
	const crm::srlz::i_typeid_t& type_id()const noexcept override;

private:
	[[noreturn]]
	void rethrow()const override;
	};
}


#define CREATE_OBJECT_CLOSED_EXC_FWD(v)					(crm::object_closed_t((v), __CBL_FILEW__, __LINE__))
#define CREATE_OBJECT_CLOSED_PTR_EXC_FWD(v)				(std::make_unique<crm::object_closed_t>((v), __CBL_FILEW__, __LINE__))


#define OBJECT_CLOSED_THROW_EXC_FWD(v)		{ \
	throw crm::object_closed_t((v), __CBL_FILEW__, __LINE__); \
											}




