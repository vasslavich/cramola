#include "../../../internal.h"
#include "../timeout_exception.h"


using namespace crm;


timeout_exception_t::timeout_exception_t()noexcept{}

std::unique_ptr<dcf_exception_t> timeout_exception_t::dcpy()const noexcept{
	return std::make_unique<timeout_exception_t>( *this );
	}

void timeout_exception_t::rethrow()const {
	throw (*this);
	}

/*! ��� �������� ������ */
typedef srlz::fram_typectr_default_t<timeout_exception_t > typectr_this_t;
static typectr_this_t tctrNtf;

const srlz::i_typeid_t& timeout_exception_t::type_id()const noexcept{
	return tctrNtf;
	}



