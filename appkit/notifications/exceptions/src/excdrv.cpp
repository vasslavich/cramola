#include "../../../internal.h"
#include "../../../logging/logger.h"
#include "../../../typeframe.h"
#include "../../../exceptions.h"
#include "../excdrv.h"


using namespace crm;



static_assert(srlz::is_serializable_v<dcf_exception_t>, __FILE_LINE__);
static_assert(srlz::detail::is_serialized_trait_v<dcf_exception_t>, __FILE_LINE__);



exceptions_driver_t::exceptions_driver_t() {
	auto check_result_add_type = [](auto errc) {
		if(errc) {
			THROW_EXC_FWD(srlz::cvt2string(errc));
			}
		};

	check_result_add_type(add_drv_type<dcf_exception_t>());
	check_result_add_type(add_drv_type<srlz::serialize_exception_t>());
	check_result_add_type(add_drv_type<srlz::serialize_overflow_t>());
	check_result_add_type(add_drv_type<object_closed_t>());
	check_result_add_type(add_drv_type<timeout_exception_t>());
	check_result_add_type(add_drv_type<abandoned_exception_t>());
	check_result_add_type(add_drv_type<extern_code_exception_t>());
	check_result_add_type(add_drv_type<refuse_operation_exception_t>());
	check_result_add_type(add_drv_type<duplicate_object_exception_t>());
	check_result_add_type(add_drv_type<not_found_exception_t>());
	check_result_add_type(add_drv_type<types_mismatching_exception_t>());
	check_result_add_type(add_drv_type<mpf_exception_t>());
	check_result_add_type(add_drv_type<outbound_exception_t>());
	check_result_add_type(add_drv_type<terminate_operation_exception_t>());
	check_result_add_type(add_drv_type<inconsistency_exchange_protocol_exception_t>());
	check_result_add_type(add_drv_type<mpf_test_exception_t>());
	}

void exceptions_driver_t::add( datagram_t & dtg,
	const std::unique_ptr<dcf_exception_t> & ntf,
	const std::string & name )noexcept {

	if( ntf ) {
		add( dtg, (*ntf), name );
		}
	}

void exceptions_driver_t::add( datagram_t & dtg,
	const dcf_exception_t& ntf,
	const std::string & name ) noexcept {

	auto ws = srlz::wstream_constructor_t::make();
	serialize( ws, ntf );

	dtg.add_value( name, ws.release() );
	}

std::list<std::unique_ptr<dcf_exception_t>> exceptions_driver_t::get( const datagram_t & dt,
	const std::string & name )const noexcept {

		std::list<std::unique_ptr<dcf_exception_t>> rlst;

		/* ������������ ��������� �� ������� ����������� */
		auto ntfBins = dt.get_values<std::vector<uint8_t>>(name);
		if(!ntfBins.empty()) {
			for(auto & ntfItem : ntfBins) {

				/* �������������� ������� */
				auto rs = srlz::rstream_constructor_t::make(ntfItem.cbegin(), ntfItem.cend());
				auto obj(deserialize(rs));
				if(obj) {
					rlst.emplace_back(std::move(obj));
					}
				}
			}

		return std::move(rlst);
	}

std::list<std::unique_ptr<dcf_exception_t>> exceptions_driver_t::get_exceptions( const datagram_t & dtg,
	const std::string & name )const noexcept {

	return get(dtg, name);
	}

std::list<std::unique_ptr<dcf_exception_t>> exceptions_driver_t::get_base(const datagram_t & dtg,
	const std::string & name )const noexcept {
	
	return get(dtg, name);
	}

