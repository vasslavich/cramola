#include "../../../internal.h"
#include "../../../typeframe/typemap.h"
#include "../abandoned_exception.h"


using namespace crm;


abandoned_exception_t::abandoned_exception_t()noexcept{}

std::unique_ptr<dcf_exception_t> abandoned_exception_t::dcpy()const noexcept{
	return std::make_unique<abandoned_exception_t>( *this );
	}

void abandoned_exception_t::rethrow()const {
	throw (*this);
	}

/*! ��� �������� ������ */
typedef srlz::fram_typectr_default_t<abandoned_exception_t > typectr_this_t;
static typectr_this_t tctrNtf;

const srlz::i_typeid_t& abandoned_exception_t::type_id()const noexcept{
	return tctrNtf;
	}
