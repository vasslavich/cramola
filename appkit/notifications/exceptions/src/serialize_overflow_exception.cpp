#include "../../../internal.h"
#include "../../../typeframe/typemap.h"
#include "../serialize_overflow.h"


using namespace crm;
using namespace crm::srlz;


serialize_overflow_t::serialize_overflow_t() noexcept {}

std::unique_ptr<dcf_exception_t> serialize_overflow_t::dcpy()const noexcept {
	return std::make_unique<serialize_overflow_t>(*this);
	}

void serialize_overflow_t::rethrow() const {
	throw (*this);
	}

/*! ��� �������� ������ */
typedef srlz::fram_typectr_default_t<serialize_overflow_t > typectr_this_t;
static typectr_this_t tctrNtf;

const srlz::i_typeid_t& serialize_overflow_t::type_id()const noexcept {
	return tctrNtf;
	}
