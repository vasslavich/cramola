#include "../../../internal.h"
#include "../types_mismatching_exception.h"


using namespace crm;
using namespace crm::srlz;


types_mismatching_exception_t::types_mismatching_exception_t() noexcept{}

std::unique_ptr<dcf_exception_t> types_mismatching_exception_t::dcpy()const noexcept{
	return std::make_unique<types_mismatching_exception_t>( *this );
	}

void types_mismatching_exception_t::rethrow()const {
	throw (*this);
	}

/*! ��� �������� ������ */
typedef srlz::fram_typectr_default_t<types_mismatching_exception_t > typectr_this_t;
static typectr_this_t tctrNtf;

const srlz::i_typeid_t& types_mismatching_exception_t::type_id()const noexcept{
	return tctrNtf;
	}


