#include "../../../internal.h"
#include "../../../typeframe/typemap.h"
#include "../object_closed.h"


using namespace crm;


object_closed_t::object_closed_t()noexcept{}

std::unique_ptr<dcf_exception_t> object_closed_t::dcpy()const noexcept{
	return std::make_unique<object_closed_t>( *this );
	}

void object_closed_t::rethrow()const {
	throw (*this);
	}

/*! ��� �������� ������ */
typedef srlz::fram_typectr_default_t<object_closed_t > typectr_this_t;
static typectr_this_t tctrNtf;

const srlz::i_typeid_t& object_closed_t::type_id()const noexcept{
	return tctrNtf;
	}

