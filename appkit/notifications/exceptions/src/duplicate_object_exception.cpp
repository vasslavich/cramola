#include "../../../internal.h"
#include "../../../typeframe/typemap.h"
#include "../duplicate_object_exception.h"


using namespace crm;
using namespace crm::srlz;


duplicate_object_exception_t::duplicate_object_exception_t()noexcept {}

void duplicate_object_exception_t::rethrow()const {
	throw (*this);
	}

std::unique_ptr<dcf_exception_t> duplicate_object_exception_t::dcpy()const noexcept {
	return std::make_unique<duplicate_object_exception_t>(*this);
	}

/*! ��� �������� ������ */
typedef srlz::fram_typectr_default_t<duplicate_object_exception_t > typectr_this_t;
static typectr_this_t tctrNtf;

const srlz::i_typeid_t& duplicate_object_exception_t::type_id()const noexcept {
	return tctrNtf;
	}

