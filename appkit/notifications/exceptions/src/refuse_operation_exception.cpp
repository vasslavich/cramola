#include "../../../internal.h"
#include "../../../typeframe/typemap.h"
#include "../refuse_operation_exception.h"


using namespace crm;
using namespace crm::srlz;


refuse_operation_exception_t::refuse_operation_exception_t() noexcept{}

std::unique_ptr<dcf_exception_t> refuse_operation_exception_t::dcpy()const noexcept{
	return std::make_unique<refuse_operation_exception_t>( *this );
	}

void refuse_operation_exception_t::rethrow()const {
	throw (*this);
	}

/*! ��� �������� ������ */
typedef srlz::fram_typectr_default_t<refuse_operation_exception_t > typectr_this_t;
static typectr_this_t tctrNtf;

const srlz::i_typeid_t& refuse_operation_exception_t::type_id()const noexcept{
	return tctrNtf;
	}

