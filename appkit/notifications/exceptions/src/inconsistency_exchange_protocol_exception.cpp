#include "../../../internal.h"
#include "../inconsistency_exchange_protocol_exception.h"


using namespace crm;


inconsistency_exchange_protocol_exception_t::inconsistency_exchange_protocol_exception_t() noexcept {}

std::unique_ptr<dcf_exception_t> inconsistency_exchange_protocol_exception_t::dcpy()const noexcept {
	return std::make_unique<inconsistency_exchange_protocol_exception_t>(*this);
	}

void inconsistency_exchange_protocol_exception_t::rethrow() const {
	throw (*this);
	}

/*! ��� �������� ������ */
typedef srlz::fram_typectr_default_t<inconsistency_exchange_protocol_exception_t > typectr_this_t;
static typectr_this_t tctrNtf;

const srlz::i_typeid_t& inconsistency_exchange_protocol_exception_t::type_id()const noexcept {
	return tctrNtf;
	}

