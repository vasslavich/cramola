#pragma once


#include "./base_channel_terms.h"


namespace crm {


class inconsistency_exchange_protocol_exception_t : public mpf_exception_t {
public:
	typedef mpf_exception_t base_type_t;

private:
	[[noreturn]]
	void rethrow()const override;

public:
	inconsistency_exchange_protocol_exception_t()noexcept;


	template<typename T,
		typename = std::enable_if_t<std::is_constructible_v<base_type_t>, T>>
		explicit inconsistency_exchange_protocol_exception_t(T&& t)noexcept
		: base_type_t(std::forward<T>(t)) {}

	template<typename T,
		typename = std::enable_if_t<std::is_constructible_v<base_type_t, T, std::wstring, int, int>>>
		explicit inconsistency_exchange_protocol_exception_t(
			T&& msg, const std::wstring& fileSource, const int lineSource, const int code = -1)noexcept
		: base_type_t(std::forward<T>(msg), fileSource, lineSource, code) {}

	std::unique_ptr<dcf_exception_t> dcpy()const noexcept override;
	const crm::srlz::i_typeid_t& type_id()const noexcept override;
	};



#define CREATE_INCONSISTENCY_EXCHANGE_PROTOCOL_EXC_FWD(v)					(crm::inconsistency_exchange_protocol_exception_t((v), __CBL_FILEW__, __LINE__))
#define CREATE_INCONSISTENCY_EXCHANGE_PROTOCOL_PTR_EXC_FWD(v)				(std::make_unique<crm::inconsistency_exchange_protocol_exception_t>((v), __CBL_FILEW__, __LINE__))


#define INCONSISTENCY_EXCHANGE_PROTOCOL_THROW_EXC_FWD(v)		{ \
	throw crm::inconsistency_exchange_protocol_exception_t((v), __CBL_FILEW__, __LINE__); \
														}
}

