#pragma once 


#include "../../typeframe/typemap.h"
#include "../base_exception_terms.h"
#include "../../tableval/predecl.h"


namespace crm{


class exceptions_driver_t : public srlz::dcf_typemap_t{
public:
	exceptions_driver_t();

	template<typename TNotification,
		typename std::enable_if_t<std::is_base_of<dcf_exception_t, TNotification>::value, int> = 0>
	void add_driver_type() {
		add_drv_type<TNotification>();
		}

	template<typename Ts>
	static void serialize(Ts & d, const std::unique_ptr<dcf_exception_t> & exc)noexcept{
		if (exc){
			return serialize(d, (*exc));
			}
		}

	template<typename Ts>
	static void serialize(Ts & d, const dcf_exception_t& exc)noexcept{

		/* ��� */
		const auto typeId_ = exc.type_id().type_id();
		const auto typeNm_ = exc.type_id().type_name();

		srlz::serialize(d, typeId_);
		srlz::serialize(d, typeNm_);

		/* ���������� ������������� */
		srlz::serialize(d, exc);
		}

	std::unique_ptr<dcf_exception_t> cast2base(std::unique_ptr<srlz::serialized_base_r> && isrlz)const noexcept{

		if (isrlz){
			auto pNtf = dynamic_cast<dcf_exception_t*>(isrlz.get());
			if (pNtf){
				isrlz.release();
				std::unique_ptr<dcf_exception_t> pBase(pNtf);
				return std::move(pBase);
				}
			else{
				return {};
				}
			}
		else{
			return {};
			}
		}

	template<typename S>
	std::unique_ptr<dcf_exception_t> deserialize(S && s, std::shared_ptr<srlz::i_type_ctr_i_srlzd_t<dcf_exception_t>> ctr) const{
		if (auto pBase = ctr->create()) {
			pBase->decode(std::forward<S>(s));
			return std::move(pBase->pObj);
			}
		else {
			SERIALIZE_THROW_EXC_FWD(nullptr);
			}
		}

	template<typename S>
	std::unique_ptr<dcf_exception_t> deserialize(S && s, std::shared_ptr<srlz::i_type_ctr_serialized_trait_t> ctr) const {
		static_assert(std::is_base_of_v<srlz::serialized_base_r, dcf_exception_t>, __FILE_LINE__);

		if (auto pBase = ctr->create()) {
			pBase->deserialize( std::forward<S>(s));
			return cast2base(std::move(pBase));
			}
		else {
			SERIALIZE_THROW_EXC_FWD(nullptr);
			}
		}

	template<typename Ts>
	std::unique_ptr<dcf_exception_t> deserialize(Ts && s)const {

		/* ��������� ����
		---------------------------------------------*/
		type_identifier_t typeId;
		srlz::deserialize(std::forward<Ts>(s), typeId);

		std::string typeNm;
		srlz::deserialize(std::forward<Ts>(s), typeNm);

		/* ���
		---------------------------------------------*/
		if (auto ictr = find(srlz::typemap_key_t(typeId, typeNm))) {

			if (auto srlzAsTrait = std::dynamic_pointer_cast<srlz::i_type_ctr_serialized_trait_t>(ictr)) {
				return deserialize(std::forward<Ts>(s), srlzAsTrait);
				}
			else if (auto srlzAsI = std::dynamic_pointer_cast<srlz::i_type_ctr_i_srlzd_t<dcf_exception_t>>(ictr)) {
				return deserialize(std::forward<Ts>(s), srlzAsI);
				}
			else {
				SERIALIZE_THROW_EXC_FWD(nullptr);
				}
			}
		else{
#ifdef CBL_TRACE_TYPEMAP_FAIL
			FATAL_ERROR_FWD(typeNm + typeId.to_str());
#else
			SERIALIZE_THROW_EXC_FWD(typeNm + typeId.to_str());
#endif
			}
		}


	static void add( datagram_t & dtg,
		const std::unique_ptr<dcf_exception_t> & ntf,
		const std::string & name )noexcept;

	static void add( datagram_t & dtg,
		const dcf_exception_t& ntf,
		const std::string & name )noexcept;

	std::list<std::unique_ptr<dcf_exception_t>> get( const datagram_t & dtg,
		const std::string & name )const noexcept;

	//std::list<std::unique_ptr<dcf_exception_t>> get( const datagram_t & dtg,
	//	const std::string & name,
	//	std::initializer_list<dcf_exception_t::type_t> && typeclass )const noexcept;

	//std::list<std::unique_ptr<dcf_exception_t>> get_notifications( const datagram_t & dtg,
	//	const std::string & name )const noexcept;

	std::list<std::unique_ptr<dcf_exception_t>> get_exceptions( const datagram_t & dtg,
		const std::string & name )const noexcept;

	std::list<std::unique_ptr<dcf_exception_t>> get_base(const datagram_t & dtg,
		const std::string & name/*,
		std::initializer_list<dcf_exception_t::type_t> && tl */)const noexcept;
	};
}
