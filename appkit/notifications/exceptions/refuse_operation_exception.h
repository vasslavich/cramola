#pragma once


#include "../base_exception_terms.h"
#include "extern_exception.h"


namespace crm{


class refuse_operation_exception_t : public extern_code_exception_t {
public:
	typedef extern_code_exception_t base_type_t;

public:
	refuse_operation_exception_t()noexcept;

	template<typename T,
		typename = std::enable_if_t<std::is_constructible_v<base_type_t>, T>>
		explicit refuse_operation_exception_t(T&& t)noexcept
		: base_type_t(std::forward<T>(t)) {}


	template<typename T,
		typename = std::enable_if_t<std::is_constructible_v<base_type_t, T, std::wstring, int, int>>>
		explicit refuse_operation_exception_t(
			T&& msg, const std::wstring& fileSource, const int lineSource, const int code = -1)noexcept
		: base_type_t(std::forward<T>(msg), fileSource, lineSource, code) {}

	std::unique_ptr<dcf_exception_t> dcpy()const noexcept override;
	const crm::srlz::i_typeid_t& type_id()const noexcept override;

private:
	[[noreturn]]
	void rethrow()const override;
	};
}

#define CREATE_REFUSE_OPERATION_EXC_FWD(v)		(crm::refuse_operation_exception_t((v), __CBL_FILEW__, __LINE__))
#define CREATE_REFUSE_OPERATION_PTR_EXC_FWD(v)		(std::make_unique<crm::refuse_operation_exception_t>((v), __CBL_FILEW__, __LINE__))

#define REFUSE_OPERATION_THROW_EXC_FWD(v)		{ \
	throw crm::refuse_operation_exception_t((v), __CBL_FILEW__, __LINE__); \
												}




