#pragma once


#include "../base_exception_terms.h"


namespace crm{


class duplicate_object_exception_t : public dcf_exception_t {
public:
	typedef dcf_exception_t base_type_t;

public:
	duplicate_object_exception_t()noexcept;

	template<typename T,
		typename = std::enable_if_t<std::is_constructible_v<base_type_t>, T>>
	explicit duplicate_object_exception_t(T&& t)noexcept
		: base_type_t(std::forward<T>(t)) {}

	template<typename T,
		typename = std::enable_if_t<std::is_constructible_v<base_type_t, T, std::wstring, int, int>>>
	explicit duplicate_object_exception_t(
			T&& msg, const std::wstring& fileSource, const int lineSource, const int code = -1)noexcept
		: base_type_t(std::forward<T>(msg), fileSource, lineSource, code) {}

	std::unique_ptr<dcf_exception_t> dcpy()const noexcept override;
	const crm::srlz::i_typeid_t& type_id()const noexcept override;

private:
	[[noreturn]]
	void rethrow()const override;
	};
}

#define CREATE_DUPLICATE_OBJECT_EXC_FWD(v)		(crm::duplicate_object_exception_t((v), __CBL_FILEW__, __LINE__))
#define CREATE_DUPLICATE_OBJECT_PTR_EXC_FWD(v)		(std::make_unique<crm::duplicate_object_exception_t>((v), __CBL_FILEW__, __LINE__))

#define DUPLICATE_OBJECT_THROW_EXC_FWD(v)		{ \
	throw crm::duplicate_object_exception_t((v), __CBL_FILEW__, __LINE__); \
	}

