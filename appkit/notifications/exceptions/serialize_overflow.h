#pragma once


#include <memory>
#include "../base_terms.h"
#include "../base_exception_terms.h"
#include "./serialize_exception.h"


namespace crm {
namespace srlz {


class serialize_overflow_t : public serialize_exception_t {
public:
	typedef serialize_exception_t base_type_t;

public:
	serialize_overflow_t()noexcept;

	template<typename T,
		typename = std::enable_if_t<std::is_constructible_v<base_type_t>, T>>
		explicit serialize_overflow_t(T&& t)noexcept
		: base_type_t(std::forward<T>(t)) {}


	template<typename T,
		typename = std::enable_if_t<std::is_constructible_v<base_type_t, T, std::wstring, int, int>>>
		explicit serialize_overflow_t(
			T&& msg, const std::wstring& fileSource, const int lineSource, const int code = -1)noexcept
		: base_type_t(std::forward<T>(msg), fileSource, lineSource, code) {}

	std::unique_ptr<dcf_exception_t> dcpy()const noexcept override;
	const i_typeid_t& type_id()const noexcept override;

private:
	[[noreturn]]
	void rethrow()const override;
	};
}
}

