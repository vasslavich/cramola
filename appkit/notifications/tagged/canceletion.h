#pragma once


#include "../base_exception_terms.h"


namespace crm {


class canceled_notification : public crm::dcf_exception_t {
public:
	typedef crm::dcf_exception_t base_type_t;
	static constexpr int type_code = 0x1000;

	void rethrow()const final;

public:
	canceled_notification()noexcept;

	template<typename T,
		typename = std::enable_if_t<std::is_constructible_v<base_type_t>, T>>
		explicit canceled_notification(
			T&& msg)noexcept
		: base_type_t(std::forward<T>(msg)) {
		set_code(type_code);
		}

	template<typename T,
		typename = std::enable_if_t<std::is_constructible_v<base_type_t, T, std::wstring, int, int>>>
	explicit canceled_notification(
		T && msg, const std::wstring& fileSource, const int lineSource, const int code = -1)noexcept
		: base_type_t(std::forward<T>(msg), fileSource, lineSource, code) {}

	std::unique_ptr<dcf_exception_t> dcpy()const noexcept override;
	const crm::srlz::i_typeid_t& type_id()const noexcept override;
	};
}


#define CREATE_CANCELETION_PTR_FWD(v)	(std::make_unique<crm::canceled_notification>((v), __CBL_FILEW__, __LINE__, canceled_notification::type_code))


