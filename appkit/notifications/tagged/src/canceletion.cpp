#include "../../../internal.h"
#include "../canceletion.h"


using namespace crm;


canceled_notification::canceled_notification()noexcept {}

std::unique_ptr<dcf_exception_t> canceled_notification::dcpy()const noexcept {
	return std::make_unique<canceled_notification>(*this);
	}

/*! ��� �������� ������ */
typedef srlz::fram_typectr_default_t<canceled_notification > typectr_this_t;
static typectr_this_t tctrNtf;

const srlz::i_typeid_t& canceled_notification::type_id()const noexcept {
	return tctrNtf;
	}

void canceled_notification::rethrow()const {
	throw (*this);
	}

