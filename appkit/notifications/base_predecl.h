#pragma once


namespace crm {
class dcf_exception_t;
class mpf_exception_t;

namespace srlz {
class serialize_exception_t;
class serialize_overflow_t;
}
}


#define CREATE_SERIALIZE_EXC_FWD(v)		(crm::srlz::serialize_exception_t((v), __CBL_FILEW__, __LINE__))
#define CREATE_SERIALIZE_PTR_EXC_FWD(v)		(std::make_unique<crm::srlz::serialize_exception_t>((v), __CBL_FILEW__, __LINE__))


#if (CBL_SERIALIZE_ERROR_ACTION == CBL_SERIALIZE_THROW_ON_ERROR)
#define SERIALIZE_THROW_EXC_FWD(v)		{ \
	throw crm::srlz::serialize_exception_t((v), __CBL_FILEW__, __LINE__); \
														}

#elif( CBL_SERIALIZE_ERROR_ACTION == CBL_SERIALIZE_FATAL_ON_ERROR)

#define SERIALIZE_THROW_EXC_FWD(v)		FATAL_ERROR_FWD(v)

#else
#error "undefined verify mode"
#endif


#ifdef USE_DCF_CHECK

#if (CBL_VERIFY_ACTION_MODE == CBL_VERIFY_ACTION_THROW)
#define SERIALIZE_VERIFY_TEXT(expression,msg){\
	if( !(expression) ){\
	SERIALIZE_THROW_EXC_FWD( (msg) );\
				}\
		}

#define SERIALIZE_VERIFY(expression){\
	if( !(expression) ){\
	SERIALIZE_THROW_EXC_FWD( nullptr );\
				}\
		}

#elif( CBL_VERIFY_ACTION_MODE == CBL_VERIFY_ACTION_FATAL)
#define SERIALIZE_VERIFY_TEXT(expression,msg){\
	if( !(expression) ){\
		FATAL_ERROR_FWD( (msg) );\
				}\
		}

#define SERIALIZE_VERIFY(expression){\
	if( !(expression) ){\
		FATAL_ERROR_FWD( nullptr );\
				}\
		}

#else
#error "undefined verify mode"
#endif

#else

#define SERIALIZE_VERIFY_TEXT(expression,msg)
#define SERIALIZE_VERIFY(expression)
#endif







#define CREATE_SERIALIZE_OVERFLOW_EXC_FWD(v)		(crm::srlz::serialize_overflow_t((v), __CBL_FILEW__, __LINE__))
#define CREATE_SERIALIZE_OVERFLOW_PTR_EXC_FWD(v)		(std::make_unique<crm::srlz::serialize_overflow_t>((v), __CBL_FILEW__, __LINE__))


#if (CBL_SERIALIZE_ERROR_ACTION == CBL_SERIALIZE_THROW_ON_ERROR)
#define SERIALIZE_OVERFLOW_THROW_EXC_FWD(v)		{ \
	throw crm::srlz::serialize_overflow_t((v), __CBL_FILEW__, __LINE__); \
			}
#elif( CBL_SERIALIZE_ERROR_ACTION == CBL_SERIALIZE_FATAL_ON_ERROR)

#define SERIALIZE_OVERFLOW_THROW_EXC_FWD(v)		FATAL_ERROR_FWD(v)

#else
#error "undefined verify mode"
#endif


#ifdef USE_DCF_CHECK

#if (CBL_VERIFY_ACTION_MODE == CBL_VERIFY_ACTION_THROW)
#define SERIALIZE_OVERFLOW_VERIFY_TEXT(expression,msg){\
	if( !(expression) ){\
	SERIALIZE_OVERFLOW_THROW_EXC_FWD( (msg) );\
				}\
		}

#define SERIALIZE_OVERFLOW_VERIFY(expression){\
	if( !(expression) ){\
	SERIALIZE_OVERFLOW_THROW_EXC_FWD( nullptr );\
				}\
		}

#elif( CBL_VERIFY_ACTION_MODE == CBL_VERIFY_ACTION_FATAL)
#define SERIALIZE_OVERFLOW_VERIFY_TEXT(expression,msg){\
	if( !(expression) ){\
	FATAL_ERROR_FWD( (msg) );\
				}\
		}

#define SERIALIZE_OVERFLOW_VERIFY(expression){\
	if( !(expression) ){\
	FATAL_ERROR_FWD( nullptr );\
				}\
		}

#else
#error "undefined verify mode"
#endif

#else

#define SERIALIZE_OVERFLOW_VERIFY_TEXT(expression,msg)
#define SERIALIZE_OVERFLOW_VERIFY(e)
#endif


