#pragma once


#include "./i_tpdriver.h"


namespace crm{


class exceptions_driver_t;


class types_driver_t : public crm::i_types_driver_t {
private:
	std::shared_ptr<exceptions_driver_t> _excDrv;

protected:
	std::shared_ptr<exceptions_driver_t> get_excdrv();
	std::shared_ptr<const exceptions_driver_t> get_excdrv()const;

public:
	std::shared_ptr<exceptions_driver_t> exceptions_driver()final;
	std::shared_ptr<const exceptions_driver_t> exceptions_driver()const final;

public:
	types_driver_t();
	};
}

