#pragma once

#include "./type_trait_base.h"


namespace crm {

template<typename THashableSTD>
struct has_std_hash_support<THashableSTD,
	std::void_t<
	decltype(std::hash<std::decay_t<THashableSTD>>()(std::declval<THashableSTD>()))
	>> : std::true_type {};
}

namespace crm::detail {


template<typename T, typename THash>
struct has_hashable_support <T, THash, std::void_t<
	typename std::enable_if_t<(
		std::is_same_v<std::decay_t<decltype(std::declval<T>().object_hash())>, std::decay_t<THash>>&&
		std::is_nothrow_invocable_r_v<std::decay_t<THash>, decltype(&T::object_hash), T>
		)>
	>> : std::true_type{};


template<typename T>
struct is_functorable_t<T, std::void_t<decltype(std::declval<T>().is_functor())>> : std::true_type {};

template<typename T>
struct is_smart_pointer_t<T,
	std::void_t<
	typename T::pointer,
	typename T::element_type,
	typename T::deleter_type,
	decltype(std::declval<T>().release()),
	decltype(std::declval<T>().reset()),
	decltype(std::declval<const T>().get()),
	decltype(std::declval<const T>().get_deleter()),
	decltype(std::declval<const T>().operator*()),
	decltype(std::declval<const T>().operator->()),
	typename std::enable_if_t<std::is_same_v< typename T::element_type, std::decay_t<decltype(*std::declval<const T>().get())> >>
	>> : std::true_type{};
}


