#pragma once


#include "./base_typetrait_decls.h"
#include "./base_typetrait_using.h"
#include "./type_trait_prefix.h"
#include "./options.h"


namespace crm::srlz {
namespace detail {


template<typename TInIt,
	typename Options = none_options,
	typename std::enable_if_t<(
		is_iterator_serialized_value_v<TInIt> &&
		!is_iterator_fixed_sized_v<TInIt>
		), int> = 0>
std::size_t serialized_size_of_container_iterators(TInIt first, TInIt last, Options = {})noexcept {

	static_assert(is_serializable_v<std::decay_t<decltype(*(std::declval<TInIt>()))>>, "bad consistency 1");
	static_assert(!is_mono_serializable_v<decltype(*(std::declval<TInIt>()))>, "bad consistency 2");
	static_assert(!is_fixed_size_serialized_v<decltype(*(std::declval<TInIt>())), Options>, "bad consistency 3");
	using length_type = option_trait_length_type<Options>;

	const auto count_ = std::distance(first, last);
	if(count_ >= 0) {
		length_type accm{ 0 };

		// �����
		accm += sizeof(length_type);

		// ��������
		for(auto it = first; it != last; ++it) {
			accm += object_trait_serialized_size((*it), serialized_options_cond<decltype(*it), Options>{});
			}

		return accm;
		}
	else {
		return 0;
		}
	}

template<typename TInIt,
	typename Options = none_options,
	typename std::enable_if_t<(
		is_iterator_serialized_value_v<TInIt>&&
		is_iterator_fixed_sized_v<TInIt> &&
		!is_iterator_mono_v<TInIt>
		), int> = 0>
std::size_t serialized_size_of_container_iterators(TInIt first, TInIt last, Options = {})noexcept {
	static_assert(is_fixed_size_serialized_v<decltype(*(std::declval<TInIt>())), Options>, __FILE_LINE__);

	using value_type = std::decay_t<decltype(*(std::declval<TInIt>()))>;
	using length_type = option_trait_length_type<Options>;

	const auto count_ = std::distance(first, last);
	if(count_ >= 0) {
		size_t range_serialized_size = write_size_v<value_type, Options> * static_cast<size_t>(std::distance(first, last));
		size_t length_size = sizeof(length_type);

		return length_size + range_serialized_size;
		}
	else {
		return 0;
		}
	}


template<typename TInIt,
	typename Options = none_options,
	typename std::enable_if_t<(
		is_iterator_serialized_value_v<TInIt>&&
		is_iterator_fixed_sized_v<TInIt> &&
		is_iterator_mono_v<TInIt>
		), int> = 0>
std::size_t serialized_size_of_container_iterators(TInIt first, TInIt last, Options = {})noexcept {
	static_assert(is_fixed_size_serialized_v<decltype(*(std::declval<TInIt>())), Options>, __FILE_LINE__);

	using value_type = std::decay_t<decltype(*(std::declval<TInIt>()))>;
	using length_type = option_trait_length_type<Options>;

	const auto count_ = std::distance(first, last);
	if(count_ >= 0) {
		size_t range_serialized_size = sizeof(value_type) * static_cast<size_t>(std::distance(first, last));
		size_t length_size = sizeof(length_type);

		return length_size + range_serialized_size;
		}
	else {
		return 0;
		}
	}

template<typename T,
	typename Options = none_options,
	typename std::enable_if_t<is_fixed_size_serialized_v<T, Options>, int> = 0>
constexpr size_t serialized_size_of_container_count(size_t count_, Options = {})noexcept {
	using value_type = std::decay_t<T>;
	using sequence_trait = sequence_serialized_size_t<T, Options>;

	size_t range_serialized_size = sequence_trait::value_serialized_size * count_;
	size_t length_size = detect_serializable_properties_prefix_bytes_arranged(range_serialized_size + 8);

	return length_size + range_serialized_size;
	}

template<typename _TxSequence,
	typename TOptions = none_options,
	typename Tsequence = std::decay_t<_TxSequence>,
	typename std::enable_if_t<!is_fixed_size_array_v<Tsequence>, int> = 0 >
std::size_t serialized_size_of_container_iterators(const _TxSequence& seq, TOptions = {})noexcept {
	return serialized_size_of_container_iterators(seq.cbegin(), seq.cend(), TOptions{});
	}

template<typename _TxSequence,
	typename Options = none_options,
	typename Tsequence = typename std::decay_t<_TxSequence>,
	typename std::enable_if_t<is_fixed_size_serialized_array_v<Tsequence, Options>, int> = 0 >
constexpr std::size_t serialized_size_of_container(const _TxSequence& c, Options = {}) noexcept {
	using value_type = fixed_size_array_value_type_t<Tsequence>;
	return serialized_size_of_container_count<value_type, Options>(std::size(c));
	}

template<typename _TxSequence,
	typename Options = none_options,
	typename Tsequence = std::decay_t<_TxSequence>,
	typename std::enable_if_t<!is_fixed_size_serialized_array_v<Tsequence, Options>, int> = 0 >
std::size_t serialized_size_of_container(const _TxSequence& c, Options = {}) noexcept {
	return serialized_size_of_container_iterators(c.cbegin(), c.cend(), Options{});
	}
}


template<typename _TxContainer,
	typename Options = none_options,
	typename TContainer = std::decay_t<_TxContainer>,
	typename std::enable_if_t<(
		detail::is_container_v<TContainer>
		&& !detail::is_dictionary_v<TContainer>
		), int> = 0>
constexpr size_t object_trait_serialized_size(const _TxContainer& xcontainer, Options = {})noexcept {
	static_assert(is_trait_as_options_v<Options>, __FILE_LINE__);

	return detail::serialized_size_of_container( xcontainer,
		serialized_options_cond<decltype(xcontainer), Options>{});
	}
}
