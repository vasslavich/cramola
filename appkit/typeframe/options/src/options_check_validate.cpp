#include "../../../internal.h"


using namespace crm;
using namespace crm::srlz;
using namespace crm::srlz::detail;


static_assert(is_fixed_size_serialized_v<int>, __FILE_LINE__);
static_assert(is_fixed_size_serialized_v<std::array<int, 10>>, __FILE_LINE__);
static_assert(has_max_serialized_value_v<option_max_serialized_size<100>>, __FILE_LINE__);
static_assert(sizeof(option_trait_length_type<option_max_serialized_size<100>>) == sizeof(char), __FILE_LINE__);
static_assert(sizeof(option_trait_length_type<none_options>) == sizeof(uint64_t), __FILE_LINE__);


struct a_with_options {};
template<typename Options>
struct serializable_properties_trait<a_with_options, Options> {
	static constexpr auto get()noexcept {
		return serializable_properties{
			8, serialize_properties_length::as_fixedsize
			};
		}
	};

static_assert(has_serializable_properties_v<a_with_options>, __FILE_LINE__);


struct a_none_options {};
static_assert(!has_serializable_properties_v<a_none_options>, __FILE_LINE__);


