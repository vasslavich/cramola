#pragma once


#include <type_traits>
#include "../base_typetrait_decls.h"
#include "../base_typetrait_using.h"


namespace crm::srlz {


template<typename T, typename DefaultOptions>
struct serialized_options_trait<T, DefaultOptions,
	std::enable_if_t<detail::has_serializable_properties_v<T>>> : std::true_type {
	using type = typename serializable_properties_trait_t<std::decay_t<T>, DefaultOptions>::serialized_options_type;
	};

template<typename T,
	typename UpperOptions = none_options>
using serialized_options_cond = std::conditional_t<!std::is_same_v<std::decay_t<UpperOptions>, crm::none_options>,
	UpperOptions,
	serialized_options_type<T, UpperOptions>>;


namespace detail {


template<size_t N>
struct option_max_serialized_size : base_options {
	static constexpr size_t max_serialized_size()noexcept { return N; }
	};

template<typename T>
struct has_max_serialized_value<T,
	std::void_t<decltype(typename std::decay_t<T>::max_serialized_size())>> : std::true_type {};


template<typename T>
struct has_flag_without_prefix<T,
	std::enable_if_t< std::is_base_of_v<without_prefix, std::decay_t<T>> >> : std::true_type{};



#if (CBL_SRLZ_OPTIONS_DEDUCTION_PREFIX_MODE == CBL_SRLZ_OPTIONS_DEDUCTION_PREFIX_ON)

template<typename Options = none_options>
using deduction_prefix_mode_t = typename Options;

#elif (CBL_SRLZ_OPTIONS_DEDUCTION_PREFIX_MODE == CBL_SRLZ_OPTIONS_DEDUCTION_PREFIX_OFF)

template<typename Options>
struct deduction_prefix_mode_ : Options, without_prefix {};

template<typename Options = none_options>
using deduction_prefix_mode_t = typename deduction_prefix_mode_<Options>;

#else
#error wrong message addressing descritptors serialization mode
#endif
}
}

