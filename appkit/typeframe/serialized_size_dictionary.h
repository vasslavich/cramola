#pragma once


#include "./base_typetrait_decls.h"
#include "./base_typetrait_using.h"
#include "./type_trait_prefix.h"
#include "./options.h"


namespace crm::srlz {
namespace detail {


template<typename _TxDictionary,
	typename Options = none_options,
	typename TDictionary = typename std::decay_t<_TxDictionary>>
size_t serialized_size_of_dictionary(const _TxDictionary& c, Options = {}) {

	using length_type = option_trait_length_type<Options>;
	using mapped_key_t = dictionary_key_type_t<TDictionary>;
	using mapped_value_t = dictionary_mapped_type_t<TDictionary>;

	length_type accm{ 0 };

	// ����� ���������
	accm += sizeof(length_type);

	// ��������
	for(const auto& e : c) {
		accm += object_trait_serialized_size(e.first, serialized_options_cond<decltype(e.first), Options>{});
		accm += object_trait_serialized_size(e.second, serialized_options_cond<decltype(e.second), Options>{});
		}

	return accm;
	}
}


template<typename _TxDictionary,
	typename Options = none_options,
	typename TDictionary = std::decay_t<_TxDictionary>,
	typename std::enable_if_t<detail::is_dictionary_v<TDictionary>, int> = 0>
size_t object_trait_serialized_size(_TxDictionary&& xcontainer, Options = {}) noexcept {
	static_assert(is_trait_as_options_v<Options>, __FILE_LINE__);

	return detail::serialized_size_of_dictionary(
		std::forward<_TxDictionary>(xcontainer),
		serialized_options_cond<decltype(xcontainer), Options>{});
	}
}

