#pragma once


#include "./base_typetrait_decls.h"


namespace crm {


template<typename T, typename TResult>
constexpr bool has_avaitable_requirements_v = has_avaitable_requirements<T, TResult>::value;

template<typename THandler>
constexpr bool has_cancel_v = has_cancel_t<THandler>::value;

template<typename T>
constexpr bool has_null_value_type_specialization_v = has_null_value_type_specialization<T>::value;

template<typename T>
constexpr bool is_null_trait_v = null_trait<T>::value;

template<typename T>
bool constexpr is_has_null_static_value_v = is_has_null_static_value<T>::value;

template<typename T>
bool constexpr is_has_null_property_v = is_has_null_property<T>::value;

template<typename T>
constexpr bool is_invoke_with_async_operation_result_v = is_invoke_with_async_operation_result_t<T>::value;

template<class T>
constexpr bool is_bool_cast_v = is_bool_cast<T>::value;

template<typename T>
constexpr bool is_functor_v = is_functor<T>::value;

template<class T>
constexpr bool is_check_nullptr_v = is_check_nullptr<T>::value;

template<typename T>
constexpr bool is_lambda_v = std::is_invocable_v<T> && std::is_copy_constructible_v<T>;


namespace detail {


template<typename THashableSTD>
constexpr bool has_std_hash_support_v = has_std_hash_support<THashableSTD>::value;

template<typename T, typename THash = address_hash_t>
constexpr bool has_hashable_support_v = has_hashable_support<std::decay_t<T>, THash>::value;

template<typename T>
constexpr bool is_functorable_v = is_functorable_t<T>::value;

template<typename T>
constexpr bool is_smart_pointer_v = is_smart_pointer_t<T>::value;
}


namespace srlz {


template<typename T, typename Options = none_options>
constexpr bool is_sequence_serialized_size_v = sequence_serialized_size_t<T, Options>::value;

namespace detail {
template<typename T, typename Opt>
using is_fixed_size_serialized_t = is_fixed_size_serialized<T, Opt>;
}

template<typename T, typename Options = none_options>
using serializable_properties_trait_t = serializable_properties_trait<T, Options>;

template<typename Options>
constexpr bool is_trait_as_options_v = std::is_base_of_v<base_options, Options>;


template<typename T, typename Options = none_options>
constexpr bool is_read_accumulate_supported_v = read_from_stream_accumulate_t<T, Options>::value;

template<typename T, typename DefaultOptions = none_options>
using serialized_options_type = typename serialized_options_trait<T, DefaultOptions>::type;

template<typename T, typename Options = none_options>
constexpr std::size_t write_size_v = write_size<T, Options>::size;

template<typename TObject>
constexpr bool is_serialized_as_blob_v = is_serialized_as_blob_t<TObject>::value;



namespace detail {

/*template<typename TObject, typename TDecoder>
constexpr bool is_decoder_stream2object_v = is_decoder_stream2object<TObject, TDecoder>::value;*/

template<typename Iter>
using iterator_category = typename std::iterator_traits<Iter>::iterator_category;

template<typename Iter>
constexpr bool is_random_access_iterator_v = std::bool_constant<std::is_same_v<iterator_category<std::decay_t<Iter>>, std::random_access_iterator_tag>>::value;

template<typename T>
constexpr size_t has_max_serialized_value_v = has_max_serialized_value<std::decay_t<T>>::value;

template<typename T>
constexpr bool has_flag_without_prefix_v = has_flag_without_prefix<T>::value;

template<typename T, typename Options = none_options>
constexpr bool type_prefix_serialize_cond_v = !has_flag_without_prefix_v<std::decay_t<Options>> && is_serialized_with_prefix_v<T>;

template<typename T>
constexpr bool is_aggregate_initializable_v = is_aggregate_initializable_t<T>::value;

template<typename T>
constexpr bool is_fixed_size_array_v = is_fixed_size_array<T>::value;

template<typename T, typename Opt = none_options>
constexpr bool is_fixed_size_serialized_v = is_fixed_size_serialized<T, Opt>::value > 0;

template<typename T>
constexpr bool is_i_srlzd_base_v = std::is_final_v<std::decay_t<T>> && is_i_srlzd_base<std::decay_t<T>>::value;

template <class T>
constexpr bool is_mono_serializable_v = is_mono_serializable<T>::value;

template <class T>
constexpr bool is_strob_serializable_v = is_strob_serializable<std::decay_t<T>>::value;

template<typename T>
constexpr bool is_i_srlzd_prefixed_v = is_i_srlzd_prefixed<std::decay_t<T>>::value && std::is_final_v<std::decay_t<T>>;

template<typename T>
constexpr bool is_i_srlzd_eof_v = !is_i_srlzd_prefixed_v<T> && is_i_srlzd_eof<std::decay_t<T>>::value && std::is_final_v<std::decay_t<T>>;

template<typename T>
constexpr bool is_i_srlzd_v = is_i_srlzd_eof_v<T> || is_i_srlzd_prefixed_v<T>;

template<typename T>
constexpr bool has_serializable_size_v = has_serializable_size<std::decay_t<T>>::value;

template<typename T>
constexpr bool is_srlzd_prefixed_v = is_i_srlzd_prefixed_v<T> || is_strob_prefixed_serialized_size_v<T> || is_mono_serializable_v<T> || has_serializable_size_v<T>;

template<typename T>
constexpr bool has_serializable_properties_v = has_serializable_properties<std::decay_t<T>>::value;

template<typename T>
constexpr bool is_headed_serialized_trait_v = is_headed_serialized_trait<T>::value;

template<typename T>
constexpr bool is_serialized_trait_r_v = is_serialized_trait_r<T>::value;

template<typename T>
constexpr bool is_serialized_trait_w_v = is_serialized_trait_w<T>::value;

template<typename T>
constexpr bool is_serialized_trait_v = is_serialized_trait<T>::value;

template<typename T, typename Opt = none_options>
constexpr bool is_strob_fixed_serialized_size_v = is_strob_fixed_serialized_size<T, Opt>::value;

template<typename T>
constexpr size_t fixe_size_array_count_v = fixed_size_array_count<T>::value;

template<typename T, typename Opt = crm::none_options>
constexpr bool is_fixed_size_serialized_array_v = is_fixed_size_serialized_array<T, Opt>::value;

template<typename T>
using fixed_size_array_value_type_t = typename fixed_size_array_value_type<T>::type;

template<typename T>
using fixed_size_array_type_t = typename fixed_size_array_type<T>::type;

template<typename T>
constexpr fixed_array_type fixed_size_array_v = fixed_size_array_type<T>::value;


template<typename TStrob, typename Options>
constexpr size_t strob_fields_serialized_size_v = strob_fields_serialized_size<TStrob, Options>::value;

template<typename T, typename Opt>
constexpr bool strob_fields_trait_as_fixed_serialized_v = strob_fields_trait_as_fixed_serialized<T, Opt>::detect();

template<typename T, typename Opt = none_options>
constexpr size_t fixed_serialized_size_v = fixed_serialized_size<T, Opt>::value;

template<typename T, typename Opt = none_options>
using fixed_serialized_length_type_t = typename fixed_serialized_length_type<T, Opt>::type;

template<typename T>
constexpr bool is_container_of_isrlzd_v = is_container_of_isrlzd<T>::value;

template<typename T>
constexpr bool is_container_of_isrlzd_eof_v = is_container_of_isrlzd_eof<T>::value;

template<typename T>
constexpr bool is_container_of_isrlzd_prefixed_v = is_container_of_isrlzd_prefixed<T>::value;

template<typename T>
constexpr bool is_invoke_with_byte_argument_v = is_invoke_with_byte_argument<T>::value;

template<typename T>
bool constexpr is_serialization_stream_v = is_serialization_stream<T>::value;

template<typename T>
bool constexpr is_serialization_wstream_v = is_serialization_wstream<T>::value;

template<typename T>
bool constexpr is_serialization_rstream_v = is_serialization_rstream<T>::value;

template<typename T>
constexpr bool is_chrono_duration_v = is_chrono_duration_t<std::decay_t<T>>::value;

template<typename T>
constexpr bool is_chrono_time_point_v = is_chrono_time_point_t<T>::value;

template<typename T>
constexpr bool is_strob_explicit_serialized_v = is_strob_explicit_serialized<T>::value;

template <class T>
constexpr bool is_strob_serializable_strong_v = is_strob_serializable_v<T> && !is_i_srlzd_base_v<T> && !is_mono_serializable_v<T>;

template <class T>
constexpr bool is_mono_serializable_strong_v = is_mono_serializable_v<T> && !is_strob_serializable_v<T> && !is_i_srlzd_base_v<T>;

template<typename T>
struct is_byte : public std::integral_constant< bool, sizeof(std::decay_t<T>) == 1 > {};

template <class T>
constexpr bool is_byte_v = is_byte<T>::value;

template<typename TIterator>
constexpr bool is_iterator_v = is_iterator<TIterator>::value;

template<typename TIterInt>
constexpr bool is_iterator_of_integral_v = is_iterator_of_integral<TIterInt>::value;

template<class _Ty = void>
using iterator_value_t = typename iterator_value<_Ty>::x_type;

template<typename TIterator>
constexpr bool is_input_iterator_v = is_input_iterator<TIterator>::value;

template<typename T>
constexpr bool is_container_v = is_container_t<T>::value;

template<typename T>
constexpr bool has_members_cbegin_cend_v = has_members_cbegin_cend<T>::value;

template<typename T>
constexpr bool has_members_begin_end_v = has_members_begin_end<T>::value;

template<typename T>
constexpr bool is_range_v = is_range_t<T>::value;

template<typename TPair>
constexpr bool is_pair_const_key_value_v = is_pair_const_key_value<TPair>::value;

template<typename TDictionary>
constexpr bool is_dictionary_v = is_container_v<TDictionary> && is_dictionary<TDictionary>::value;

template<class T>
bool constexpr is_string_type_v = is_string_type<T>::value;

template<typename T>
constexpr bool is_container_contiguous_v = is_container_v<T> && is_container_contiguous_cond_t<T>::value;

template<class _Ty = void>
using contaiter_value_type_t = typename contaiter_value_type<_Ty>::x_type;

template<class _Ty = void>
using range_value_type_t = typename range_value_type<_Ty>::x_type;

template<typename T>
constexpr bool is_container_of_mono_v = is_container_of_mono<T>::value;

template<class _Ty = void>
using dictionary_mapped_type_t = typename dictionary_mapped_type<_Ty>::x_type;

template<class _Ty = void>
using dictionary_key_type_t = typename dictionary_key_type<_Ty>::x_type;

template<typename T>
constexpr bool is_range_of_mono_v = is_range_of_mono<T>::value;

template <class T>
constexpr bool is_strob_flat_serializable_v = is_strob_serializable_v<T> && !is_container_v<T>;

template <class T>
constexpr bool is_can_serializabled_only_strob_v = is_strob_serializable_v<T> && !is_mono_serializable_v<T> && !is_container_v<T>;

template<typename T>
constexpr bool is_container_of_strob_v = is_container_of_strob<T>::value;

template<typename T>
constexpr bool is_dictionary_of_strob_v = is_dictionary_of_strob<T>::value;

template<typename T>
constexpr bool is_container_of_containers_v = is_container_of_containers<T>::value;

template<typename T>
constexpr bool is_container_of_containers_mono_v = is_container_of_containers_mono<T>::value;

template<typename T>
constexpr bool is_container_of_containers_strob_v = is_container_of_containers_strob<T>::value;

template<typename T>
constexpr bool is_container_of_bytes_v = is_container_bytes_t<T>::value;

template<typename T>
constexpr bool is_container_of_integral_v = is_container_integral_t<T>::value;

template<typename T>
constexpr bool is_range_of_bytes_v = is_range_bytes_t<T>::value;

template<typename T>
constexpr bool is_container_random_access_v = is_container_random_access_t<T>::value;

template<typename T>
constexpr bool is_iterator_mono_v = is_iterator_mono<T>::value;

template<typename TIterator>
constexpr bool is_input_iterator_flat_strob_v = is_input_iterator_v<TIterator> && is_iterator_mono_v<iterator_value_t<TIterator>>;

template<typename T>
constexpr bool is_iterator_strob_v = is_iterator_strob<T>::value;

template<typename TIterator>
constexpr bool is_input_iterator_strob_v = is_input_iterator_v<TIterator> && is_iterator_strob_v</*iterator_value_t<*/TIterator/*>*/>;

template<typename T>
constexpr bool is_iterator_fixed_sized_v = is_iterator_fixed_sized<T>::value;

template<typename T>
constexpr bool is_iterator_byte_v = is_iterator_byte_t<T>::value;

template<typename T>
constexpr bool is_contiguous_iterator_v = is_contiguous_iterator_t<std::decay_t<T>>::value;

template<typename T>
constexpr bool is_resize_support_v = is_resize_support_t<T>::value;

template<typename T>
constexpr bool is_constructible_with_size_v = is_constructible_with_size<std::decay_t<T>>::value;

template<typename T>
constexpr bool is_reserve_support_v = is_reserve_support_t<T>::value;

template<typename T>
constexpr bool is_enum_serializabled_v = std::is_enum_v<T> && is_enum_serializabled_t<T>::value;

template<typename T, typename Options = none_options>
constexpr auto type_serialized_options_v = get_type_serialized_options_as_<T, Options>::value;

template<typename T, const serialize_properties_length CmpVal,
	typename Options = none_options>
	constexpr bool is_type_serialized_options_as()noexcept {
	return type_serialized_options_v<T, Options> == CmpVal;
	}

template<typename T>
constexpr bool is_serializable_properties_prefix_trait_v = is_serializable_properties_prefix_triat<T>::value;


template<typename T, typename Options = none_options>
using serializable_prefix_type_trait_t = serializable_prefix_type_trait<T, Options>;


template<typename T>
using option_trait_length_type = typename option_trait_length_type_<T>::type;


template<typename T>
constexpr size_t option_max_serialized_v = option_max_serialized_value<T>::value;

template<typename T>
constexpr bool is_serialized_with_prefix_v = x_from_stream_base<T>::is_serialized_with_prefix();

template<typename T>
constexpr bool is_container_serialized_v = is_container_serialized<T>::value;

template<typename T>
constexpr bool is_iterator_serialized_value_v = is_iterator_serialized_value<T>::value;

template<typename T, typename O = none_options>
using fixed_array_trait_t = fixed_array_trait<T, O>;

template<typename _Tx, typename T = std::decay_t<_Tx>>
struct is_serializable_t : public std::integral_constant<bool,
	(
		detail::is_mono_serializable_v<T>
		|| detail::is_strob_flat_serializable_v<T>
		|| detail::is_strob_explicit_serialized_v<T>
		|| detail::is_container_of_mono_v<T>
		|| detail::is_container_of_strob_v<T>
		|| detail::is_container_of_containers_mono_v<T>
		|| detail::is_container_of_containers_strob_v<T>
		|| detail::is_i_srlzd_v<T>
		|| detail::is_container_of_isrlzd_v<T>
		|| detail::is_serialized_trait_v<T>
		|| detail::is_fixed_size_serialized_v<T>
		|| detail::is_fixed_size_array_v<T>
		)> {};


template<typename T, typename Opt = none_options>
constexpr bool has_serializable_properties_as_fixed_v = 
	is_type_serialized_options_as<T, serialize_properties_length::as_fixedsize, Opt>();


struct stream_base_traits {
	virtual ~stream_base_traits() {};

	template<typename Pred>
	static constexpr bool is_accumulate_invocable_v = std::is_invocable_v<Pred, uint8_t>;

	static constexpr size_t all_available = (size_t)(-1);

	template<typename T>
	struct fault_cathegory_detect : std::false_type {};

	template<typename ITerator,
		typename std::enable_if_t<is_iterator_v<ITerator>, int> = 0>
	static constexpr iterator_tag iterator_cathegory()noexcept {
		using trait_iterator = typename std::iterator_traits<ITerator>;
		using cat_type = typename trait_iterator::iterator_category;

		if constexpr(std::is_same_v<cat_type, std::input_iterator_tag>)
			return iterator_tag::input_iterator_tag;
		else if constexpr(std::is_same_v<cat_type, std::output_iterator_tag>) {
			return iterator_tag::output_iterator_tag;
			}
		else if constexpr(std::is_same_v<cat_type, std::forward_iterator_tag>) {
			return iterator_tag::forward_iterator_tag;
			}
		else if constexpr(std::is_same_v<cat_type, std::bidirectional_iterator_tag>) {
			return iterator_tag::bidirectional_iterator_tag;
			}
		else if constexpr(std::is_same_v<cat_type, std::random_access_iterator_tag>) {
			return iterator_tag::random_access_iterator_tag;
			}
		/*else if constexpr(std::is_same_v<cat_type, std::contiguous_iterator_tag>) {
			return iterator_tag::contiguous_iterator_tag;
			}*/
		else {
			static_assert(fault_cathegory_detect<void>::value, __FILE_LINE__);
			}
		}

protected:
	struct actor_null_dummy { void operator()(uint8_t) {} };
	};
}

template<typename _Tx>
constexpr bool is_serializable_v = detail::is_serializable_t<_Tx>::value;


template<typename TObject>
constexpr bool has_static_typeid_trait_v = has_static_typeid_trait_t<std::decay_t<TObject>>::value;
}
}
