#pragma once


#include <memory>


namespace crm{


class exceptions_driver_t;

struct i_types_driver_t {
	virtual ~i_types_driver_t() = 0;

	virtual std::shared_ptr<exceptions_driver_t> exceptions_driver() = 0;
	virtual std::shared_ptr<const exceptions_driver_t> exceptions_driver()const = 0;
	};
}
