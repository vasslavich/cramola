#pragma once


#include "../internal_invariants.h"


namespace crm {

typedef std::decay<decltype(std::declval<std::chrono::system_clock::time_point>().time_since_epoch())>::type _xstreamed_time_t;
typedef std::decay<decltype(std::declval<_xstreamed_time_t>().count())>::type streamed_time_t;
}
