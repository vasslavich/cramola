#pragma once


#include <type_traits>
#include <iterator>
#include <chrono>
#include "../internal_invariants.h"
#include "./base_typetrait_decls.h"
#include "./base_typetrait_using.h"
#include "./options.h"


namespace crm {


template<typename THashableSTD,
	typename std::enable_if_t<detail::has_std_hash_support_v<THashableSTD>, int>* = 0>
std_hash_type get_hash_std(THashableSTD&& o)noexcept {
	return std::hash<std::decay_t<THashableSTD>>{}(std::forward<THashableSTD>(o));
	}


template<typename T,
	typename std::enable_if_t<detail::has_hashable_support_v<T>, int> = 0>
	decltype(auto) object_hash(const T& v)noexcept {
	return v.object_hash();
	}

template <typename T>
struct is_has_null_property<T, std::void_t<decltype(std::declval<T>().is_null())>>
	: std::true_type {};

template<typename T>
struct null_trait<T, std::enable_if_t<(is_has_null_property_v<T> && !is_has_null_static_value_v<T>)>> : std::true_type {
	template<typename X>
	static constexpr bool null(X&& obj)noexcept {
		return obj.is_null();
		}
	};



template <typename T>
struct is_has_null_static_value<T, std::void_t<decltype(std::decay_t<T>::null)>>
	: std::true_type {

	static constexpr const T& null_value() { return std::decay_t<T>::null; }
	};



template<typename T>
struct null_trait<T, std::enable_if_t<std::is_integral_v<std::decay_t<T>>>> : std::true_type {

	template<typename X>
	static constexpr bool null(X&& obj)noexcept {
		return obj == 0;
		}

	static constexpr T get_null_value()noexcept {
		return 0;
		}
	};


template<typename T>
struct null_trait<T, std::enable_if_t<(
	is_has_null_static_value_v<T> && !is_has_null_property_v<T>
	)>> : std::true_type{

	template<typename X>
	static constexpr bool null(X&& obj)noexcept {
		return obj == is_has_null_static_value<T>::null_value();
		}

	static constexpr decltype(auto) get_null_value()noexcept {
		return is_has_null_static_value<T>::null_value();
		}
	};


template<typename T>
struct null_trait<T, std::enable_if_t<(
	is_has_null_static_value_v<T>&& is_has_null_property_v<T>
	)>> : std::true_type{

	template<typename X>
	static constexpr bool null(X&& obj)noexcept {
		return obj.is_null();
		}

	static constexpr T get_null_value()noexcept {
		static_assert(std::is_lvalue_reference_v<decltype(is_has_null_static_value<T>::null_value())>, __FILE_LINE__);
		return is_has_null_static_value<T>::null_value();
		}
	};


template<typename T>
struct null_trait<T, std::enable_if_t<(
	srlz::detail::is_chrono_duration_v<T>
	)>> : std::true_type{

	template<typename X>
	static constexpr bool null(X&& obj)noexcept {
		return obj.count() == 0;
		}

	static constexpr T get_null_value()noexcept {
		return T{ 0 };
		}
	};


template< class T >
struct has_null_value_type_specialization<T,
	std::void_t<decltype(null_value_t<std::decay_t<T>>::null)>
> : std::true_type {};


template<typename T>
struct null_trait<T,
	std::enable_if_t <(has_null_value_type_specialization_v<T> && !is_has_null_static_value_v<T>)>> : std::true_type{

	using value_type = std::decay_t<T>;
	using null_trait_type = null_value_t<value_type>;

	template<typename X>
	static constexpr bool null(X&& obj)noexcept {
		return obj == null_trait_type::null;
		}

	static constexpr T get_null_value()noexcept {
		return null_trait_type::null;
		}
	};



template<typename T>
constexpr bool is_null(T&& o)noexcept {
	return null_trait<T>::null(std::forward<T>(o));
	}

template<typename T>
constexpr T get_null()noexcept {
	return null_trait<T>::get_null_value();
	}

namespace detail {


template<typename T>
auto make_comparator_cast(T&& t)noexcept {
	return comparator_cast<std::decay_t< T>> (std::forward<T>(t));
	}


template<typename T>
bool operator == (const comparator_cast<T>& l, const comparator_cast<T>& r)noexcept {
	return l.equals(r);
	}
}


template<typename _Tx,
	typename T = std::decay_t<_Tx>>
	struct is_invoke_with_async_operation_result_helper_t {
	typedef typename std::decay_t<decltype(std::declval<T>()(async_operation_result_t::st_abandoned))> invoke_result_t;
	};

template<typename T>
struct is_invoke_with_async_operation_result_t<T,
	typename std::enable_if_t<std::is_same_v<typename is_invoke_with_async_operation_result_helper_t<T>::invoke_result_t, void>>> : public std::true_type {};


template< class T >
struct is_bool_cast<T,
	std::enable_if_t < (
		std::is_convertible_v <std::decay_t<T>, bool >) >> : std::true_type {};

template< class T >
struct is_bool_cast<T, std::void_t<
	decltype( nullptr == std::declval<std::decay_t<T>>())
	>> : std::true_type {};


template<class T>
struct is_functor<T,
	std::void_t<decltype(&std::decay_t<T>::operator())>> : std::true_type{};

template< class T >
struct is_check_nullptr<T, std::void_t<decltype(nullptr == std::declval<std::decay_t<T>>())>> : std::true_type {};


namespace srlz{

template<typename TObject>
struct xtype_properties_t<TObject,
	typename std::enable_if<detail::is_mono_serializable_v<TObject>>::type> {

	static const std::string name;
	};

template<typename TObject>
const std::string xtype_properties_t<TObject, typename std::enable_if<detail::is_mono_serializable_v<TObject>>::type>::name = typeid(TObject).name();


namespace detail {


template<typename T>
struct is_i_srlzd_base<T,
	std::void_t<
	decltype(std::declval<T>().srlz(rstream_constructor_t::make(std::declval<std::vector<uint8_t>>().cbegin(), std::declval<std::vector<uint8_t>>().cend()))),
	decltype(std::declval<T>().dsrlz(wstream_constructor_t::make()))
	>
>
	: public std::true_type {};


template<typename T>
struct is_i_srlzd_prefixed<T,
	std::void_t<
	typename std::enable_if_t<is_i_srlzd_base_v<T>>,
	typename std::enable_if_t<std::is_convertible_v<decltype(std::declval<std::decay_t<T>>().get_serialized_size()), std::size_t>>
	>
>
	: public std::true_type {};

template<typename T>
struct is_i_srlzd_eof<T,
	std::void_t<
	typename std::enable_if_t<is_i_srlzd_base_v<T>>,
	typename std::enable_if_t<std::is_convertible_v<decltype(std::declval<T>().get_eof_marker()), std::vector<uint8_t>>>,
	decltype(std::declval<T>().eof_marker_validate(std::declval<T>().get_eof_marker()))
	>
>
	: public std::true_type {};

template<typename T, typename O>
struct get_type_serialized_options_as_<T, O,
	std::enable_if_t<detail::has_serializable_properties_v<T>>>{

	using typex = typename std::decay_t<T>;
	using trait_type = serializable_properties_trait_t<typex, O>;
	static constexpr serialize_properties_length value = trait_type::get().size_as;
	};

template<typename T >
struct has_serializable_size < T,
	typename std::enable_if_t < (
	((is_container_v<T> || is_dictionary_v<T>) && is_srlzd_prefixed_v<contaiter_value_type_t<T>>)

		) >> : public std::true_type{};

template<typename T>
struct has_serializable_properties<T, std::void_t<
	typename std::enable_if_t<std::is_same_v<std::decay_t<decltype(serializable_properties_trait_t<T>::get())>, serializable_properties>>
	>>: public std::true_type {};


template<typename T>
struct is_headed_serialized_trait<T,
	std::void_t<
	decltype(std::decay_t<T>::is_serialized_with_prefix())
	>
>
	: public std::true_type {};


template<typename T>
struct is_serializable_properties_prefix_triat < T, std::void_t<
	typename std::decay_t<T>::prefix_trait,
	typename std::decay_t<T>::serialized_type,
	decltype(std::decay_t<T>::type_serialized_maxsize),
	decltype(std::decay_t<T>::maxsize)
	>> : std::true_type{};



template<typename T>
constexpr std::tuple<fixed_array_not, char, std::integral_constant<size_t, 0>> detect_is_fixed_carray(T)noexcept;

template<typename T, size_t N>
constexpr std::tuple<fixed_array_c, T, std::integral_constant<size_t, N>> detect_is_fixed_carray(T(&x)[N])noexcept;

template<typename T, size_t N>
constexpr std::tuple<fixed_array_c, T, std::integral_constant<size_t, N>> detect_is_fixed_carray(const T(&x)[N])noexcept;

template<typename T, size_t N>
constexpr std::tuple<fixed_array_std, T, std::integral_constant<size_t, N>> detect_is_fixed_carray(std::array<T, N>&)noexcept;

template<typename T, size_t N>
constexpr std::tuple<fixed_array_std, T, std::integral_constant<size_t, N>> detect_is_fixed_carray(const std::array<T, N>&)noexcept;

template<typename T>
struct is_fixed_size_array<T, std::void_t<
	std::enable_if_t<(
		!std::is_same_v<std::decay_t<decltype(std::get<0>(detect_is_fixed_carray(std::declval<T>())))>, fixed_array_not>
		)>
	>> : std::true_type{};

template<typename T>
struct fixed_size_array_value_type<T,
	std::enable_if_t<is_fixed_size_array_v<T>>> {

	using type = typename std::decay_t<decltype(std::get<1>(detect_is_fixed_carray(std::declval<T>())))>;
	};

template<typename T>
struct fixed_size_array_type<T,
	std::enable_if_t<is_fixed_size_array_v<T>>> {

	using type = typename std::decay_t<decltype(std::get<0>(detect_is_fixed_carray(std::declval<T>())))>;

	static_assert(type::value <= (char)fixed_array_type::fixed_array_c && type::value >= (char)fixed_array_type::fixed_array_not, __FILE_LINE__);
	static constexpr auto value = (fixed_array_type)type::value;
	};

template<typename T, typename Options>
struct is_fixed_size_serialized_array < T, Options, std::void_t<
	std::enable_if_t<(
		is_fixed_size_array_v<T>
		&&
		is_fixed_size_serialized_v<fixed_size_array_value_type_t<T>, Options>
		)>
	>> : std::true_type{};

template<typename T>
struct fixed_size_array_count<T,
	std::enable_if_t<is_fixed_size_array_v<T>>> {

	using size_constant_type = typename std::decay_t<decltype(std::get<2>(detect_is_fixed_carray(std::declval<T>())))>;
	static constexpr size_t value = size_constant_type::value;
	};



template <std::size_t I, std::size_t N, typename Options>
struct tuple_prefixed_serialized_size_impl {
	template <class T>
	static constexpr size_t size(T&& value) noexcept {

		return object_trait_serialized_size(std::get<I>(std::forward<T>(value)),
			serialized_options_cond<decltype(std::get<I>(std::forward<T>(value))), Options>{}) +
			tuple_prefixed_serialized_size_impl<I + 1, N, Options>::size(std::forward<T>(value));
		}
	};

template <std::size_t I, typename Options>
struct tuple_prefixed_serialized_size_impl<I, I, Options> {
	template <class T>
	static constexpr size_t size(T&&) noexcept { return 0; }
	};

template<typename T>
struct option_max_serialized_value<T,
	std::enable_if_t<has_max_serialized_value_v<T>>> : std::integral_constant<size_t,
	std::decay_t<T>::max_serialized_size()> {};

template<typename Option>
struct option_trait_length_type_<Option,
	std::enable_if_t<has_max_serialized_value_v<Option>>> {

	using type = typename serializable_properties_prefix_nbytes_t<option_max_serialized_v<Option>>::length_type;
	};


template<typename T>
struct is_invoke_with_byte_argument<T,
	std::void_t<std::enable_if_t<std::is_invocable_v<T, std::uint8_t>>>> : std::true_type{};


template<typename T>
struct is_container_of_isrlzd<T,
	typename std::enable_if<
	(
		is_container_v<T> && 
		is_i_srlzd_v<typename std::decay_t<T>::value_type>
		)
>::type>
	: std::true_type {};


template<typename T>
struct is_container_of_isrlzd_eof<T,
	typename std::enable_if<
	(is_i_srlzd_eof_v<typename std::decay_t<T>::value_type>
		&& is_container_v<T>)
>::type>
	: std::true_type {};


template<typename T>
struct is_container_of_isrlzd_prefixed<T,
	typename std::enable_if<
	(is_i_srlzd_prefixed_v<typename std::decay_t<T>::value_type>
		&& is_container_v<T>)
>::type>
	: std::true_type {};


template<typename T>
struct x_from_stream_base <T,
	std::enable_if_t<is_headed_serialized_trait_v<T>>> {

	static constexpr bool is_serialized_with_prefix()noexcept {
		using type = typename std::decay_t<T>;
		return type::is_serialized_with_prefix();
		}
	};

/*template<typename TObject, typename TDecoder>
struct is_decoder_stream2object<TObject, TDecoder,
	std::void_t<
	typename std::enable_if_t<std::is_invocable_r_v<TObject, TDecoder, decltype(
		rstream_constructor_t::make(std::declval<std::vector<uint8_t>>().cbegin(), std::declval<std::vector<uint8_t>>().cend())
		)>>
	>
	> : std::true_type{};*/


template<typename T>
struct is_serialization_stream < T,
	std::void_t<
	typename T::item_value_t,
	typename std::enable_if_t<is_byte_v<typename T::item_value_t>>
	>
> : std::true_type {};



template<typename T>
struct is_serialization_rstream < T,
	std::void_t<
	typename std::enable_if_t<is_serialization_stream_v<T>>,
	decltype(std::declval<T>().pop(std::declval< std::uint8_t&>())),
	typename std::enable_if_t<is_container_of_bytes_v<decltype(std::declval<T>().pop2end(std::declval<std::size_t>()))>>,
	decltype(std::declval<T>().pop(std::declval<std::uint8_t*>(), std::declval<std::uint8_t*>())),
	typename std::enable_if_t<std::is_convertible_v<std::decay_t< decltype(std::declval<T>().pop2end(std::declval<std::vector<std::uint8_t>&>()))>, std::size_t>>
	>
> : std::true_type {};


template<typename T>
struct is_container_serialized<T,
	typename std::enable_if_t<is_container_v<T>&& is_serializable_v<contaiter_value_type_t<T>>>>
	: std::true_type {};

template<typename T>
struct is_iterator_serialized_value<T,
	typename std::enable_if_t<(
		is_iterator_v<T>&&
		is_serializable_v<iterator_value_t<T>>
		)>>
	: public std::true_type {};




template<typename T, typename Opt>
struct is_fixed_size_serialized<T, Opt,
	typename std::enable_if_t<is_fixed_size_serialized_array_v<T, Opt>>>
	: std::integral_constant<int, 1> {

	using trait_type = typename fixed_array_trait_t<T, Opt>;
	using length_type = typename trait_type::length_type;
	using value_type = typename trait_type::value_type;

	static constexpr size_t array_size = trait_type::range_size;
	constexpr static size_t size = trait_type::serialized_size;
	};

template<typename T, typename Opt>
struct is_fixed_size_serialized<T, Opt,
	typename std::enable_if_t<(
		is_strob_fixed_serialized_size_v<T, Opt> && 
		!has_serializable_properties_as_fixed_v<T, Opt>
		)>>
	: std::integral_constant<int, 2> {

	constexpr static size_t size = strob_fields_serialized_size_v<T, Opt>;
	using length_type = typename max_serialized_size_to_arranged_length_type_t<size>;
	};

template<typename T, typename Opt>
struct is_fixed_size_serialized<T, Opt,
	typename std::enable_if_t<(
		has_serializable_properties_as_fixed_v<T, Opt> &&
		type_prefix_serialize_cond_v<T, Opt>
		)>>
	: std::integral_constant<int,3> {

	using value_type = typename std::decay_t<T>;
	using prefix_trait = typename serializable_prefix_type_trait_t<value_type, Opt>;
	using trait_type = typename serializable_properties_trait_t<value_type, Opt>;
	constexpr static size_t prefix_serialized_size = prefix_trait::serialized_size;
	constexpr static size_t value_serialized_size = trait_type::get().serialized_size;
	constexpr static size_t size = prefix_serialized_size + value_serialized_size;
	using length_type = typename max_serialized_size_to_arranged_length_type_t<size>;
	};

template<typename T, typename Opt>
struct is_fixed_size_serialized<T, Opt,
	typename std::enable_if_t<(
		has_serializable_properties_as_fixed_v<T, Opt> &&
		!type_prefix_serialize_cond_v<T, Opt>
		)>>
	: std::integral_constant<int, 4> {

	using value_type = typename std::decay_t<T>;
	using trait_type = typename serializable_properties_trait_t<value_type, Opt>;
	constexpr static size_t value_serialized_size = trait_type::get().serialized_size;
	constexpr static size_t size = value_serialized_size;
	using length_type = typename max_serialized_size_to_arranged_length_type_t<size>;
	};
}
}
}
