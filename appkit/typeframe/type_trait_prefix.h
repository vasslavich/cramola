#pragma once

#include "./base_typetrait_decls.h"
#include "./base_typetrait_using.h"
#include "../notifications/base_predecl.h"

namespace crm::srlz {
namespace detail {


/*! Max length can be serialized for this type prefix length type*/
template<typename TSerializedTypePrefixLengthType,
	typename std::enable_if_t < (
		std::is_integral_v<TSerializedTypePrefixLengthType>&&
		std::is_unsigned_v<TSerializedTypePrefixLengthType>
		), int> = 0>
	constexpr size_t serializable_properties_prefix_bytes_maxsize()noexcept {
	static_assert(sizeof(std::numeric_limits<TSerializedTypePrefixLengthType>::max()) <= sizeof(size_t), __FILE_LINE__);
	return (TSerializedTypePrefixLengthType)(std::numeric_limits<TSerializedTypePrefixLengthType>::max())
		& (((TSerializedTypePrefixLengthType)(-1)) >> serialized_prefix_payload_bits);
	}

template<typename TLengthType>
constexpr size_t serializable_properties_prefix_bytes_maxsize_v = serializable_properties_prefix_bytes_maxsize<TLengthType>();


constexpr size_t detect_serializable_properties_prefix_bytes_arranged(const size_t SerializedMaxSize)noexcept {
	if(SerializedMaxSize <= serializable_properties_prefix_bytes_maxsize_v<uint8_t>) {
		return 1;
		}
	else {
		if(SerializedMaxSize <= serializable_properties_prefix_bytes_maxsize_v<uint16_t>) {
			return 2;
			}
		else {
			if(SerializedMaxSize <= serializable_properties_prefix_bytes_maxsize_v<uint32_t>) {
				return 4;
				}
			else {
				if(SerializedMaxSize <= serializable_properties_prefix_bytes_maxsize_v<uint64_t>) {
					return 8;
					}
				else {
					return (size_t)(-1);
					}
				}
			}
		}
	}


template<typename TIntegralBase>
struct serializable_properties_prefix_trait_nbytes final {
	static_assert(std::is_integral_v<TIntegralBase>, __FILE_LINE__);

	using length_type = typename TIntegralBase;
	using serialized_type = typename TIntegralBase;

	length_type length{};
	srlz_prefix_type_t type{ srlz_prefix_type_t::prefixed };

	static constexpr bool is_serialized_with_prefix()noexcept { return false; }

	template<typename Ts>
	void srlz(Ts&& dest)const {
		if(length <= serializable_properties_prefix_bytes_maxsize_v<length_type>) {
#ifdef USE_DCF_CHECK
			auto off = dest.offset();
#endif

			serialized_type field = length << serialized_prefix_payload_bits;
			if(srlz_prefix_type_t::eof == type) {
				field |= 1;
				}

			serialize(std::forward<Ts>(dest), field);

#ifdef USE_DCF_CHECK
			CBL_VERIFY((dest.offset() - off) == serialized_size<>());
#endif
			}
		else {
			THROW_EXC_FWD(nullptr);
			}
		}

	template<typename Ts>
	void dsrlz(Ts&& src) {
		serialized_type field{ 0 };
		deserialize(std::forward<Ts>(src), field);

		const auto utype = field & ((serialized_type)0x1);
		if(0 == utype) {
			type = srlz_prefix_type_t::prefixed;
			}
		else {
			type = srlz_prefix_type_t::eof;
			}

		length = (field & ~((serialized_type)0x1)) >> serialized_prefix_payload_bits;
		}

	template<typename Options = none_options>
	static constexpr size_t serialized_size()noexcept {
		return write_size_v<serialized_type, Options>;
		}

	constexpr size_t get_serialized_size()const noexcept {
		return serialized_size<>();
		}
	};
}


template<typename IIntegral, typename Options>
struct serializable_properties_trait<
	detail::serializable_properties_prefix_trait_nbytes<IIntegral>,
	Options> {

	using type_ = detail::serializable_properties_prefix_trait_nbytes<IIntegral>;
	constexpr static size_t mxs = type_::serialized_size<Options>();
	using serialized_options_type =  detail::option_max_serialized_size<mxs>;

	static constexpr auto get()noexcept {
		return serializable_properties{ 
			serialized_options_type::max_serialized_size(), 
			serialize_properties_length::as_fixedsize 
			};
		}
	};


namespace detail {


template<typename T>
struct serializable_properties_nbytes2type_fault : std::false_type {};

template<typename T>
struct serializable_properties_nbytes2type {
	static_assert(serializable_properties_nbytes2type_fault<T>::value, __FILE_LINE__);
	};

template<>
struct serializable_properties_nbytes2type<std::integral_constant<size_t, 1>> {
	using type = uint8_t;
	};

template<>
struct serializable_properties_nbytes2type<std::integral_constant<size_t, 2>> {
	using type = uint16_t;
	};

template<>
struct serializable_properties_nbytes2type<std::integral_constant<size_t, 4>> {
	using type = uint32_t;
	};

template<>
struct serializable_properties_nbytes2type<std::integral_constant<size_t, 8>> {
	using type = uint64_t;
	};

template<typename T>
using serializable_properties_nbytes2type_t = typename serializable_properties_nbytes2type<T>::type;


template<size_t MaxSerializedSize>
struct max_serialized_size_to_arranged_length_type {
	static constexpr size_t size_type_in_bytes = detect_serializable_properties_prefix_bytes_arranged(MaxSerializedSize);
	using length_type = serializable_properties_nbytes2type_t<std::integral_constant<size_t, size_type_in_bytes>>;
	};

template<const size_t MaxSerializedSize>
using max_serialized_size_to_arranged_length_type_t = typename max_serialized_size_to_arranged_length_type< MaxSerializedSize>::length_type;


template<size_t MaxSerializedSize>
struct serializable_properties_prefix_nbytes_ {
	using type = typename serializable_properties_prefix_trait_nbytes<
		max_serialized_size_to_arranged_length_type_t<MaxSerializedSize>>;
	};

template<size_t MaxSerializedSize>
using serializable_properties_prefix_nbytes_t = typename serializable_properties_prefix_nbytes_<MaxSerializedSize>::type;

template<size_t SerializedMaxSize>
struct serializable_properties_prefix_type {
	static constexpr auto detect_serializable_properties_prefix_type()noexcept {
		return serializable_properties_prefix_nbytes_t<SerializedMaxSize>{};
		}

	using type = typename std::decay_t<decltype(detect_serializable_properties_prefix_type())>;
	};

template<size_t SerializedSize>
using serializable_properties_prefix_type_t = typename serializable_properties_prefix_type<SerializedSize>::type;


template<typename T, typename Options>
struct serializable_prefix_type_trait<T, Options, std::void_t<
	std::enable_if_t<has_serializable_properties_v<T>>
	>> {
	static constexpr size_t type_serialized_maxsize = serializable_properties_trait<std::decay_t<T>, Options>::get().serialized_size;
	using prefix_trait = typename serializable_properties_prefix_type_t<type_serialized_maxsize>;
	using serialized_type = typename prefix_trait::serialized_type;
	using length_type = typename prefix_trait::length_type;
	static constexpr size_t maxsize = serializable_properties_prefix_bytes_maxsize_v<length_type>;
	static constexpr size_t serialized_size = write_size_v<serialized_type, Options>;
	};

template<typename T, typename Options>
struct serializable_prefix_type_trait<T, Options, std::void_t<
	std::enable_if_t<!has_serializable_properties_v<T>>
	>> {
	using prefix_trait = srlz_prefix_t;
	using serialized_type = prefix_trait;
	using length_type = srlz_prefix_t::length_type;

	static constexpr size_t type_serialized_maxsize = serializable_properties_prefix_bytes_maxsize_v<length_type>;
	static constexpr size_t maxsize = type_serialized_maxsize;
	};
}
}

