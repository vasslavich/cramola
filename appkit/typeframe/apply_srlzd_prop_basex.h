#pragma once

#include "./base_typetrait_decls.h"

namespace crm::srlz {

template<typename T, typename Opt>
struct serializable_properties_trait<T, Opt,
	std::enable_if_t<detail::is_mono_serializable_v<T>>> {
	using serialized_options_type = detail::option_max_serialized_size<sizeof(T)>;
	static constexpr auto get()noexcept {
		return serializable_properties{
			sizeof(T),
			serialize_properties_length::as_fixedsize
			};
		}
	};


namespace detail {

template<typename T>
struct x_from_stream_base <T,
	std::enable_if_t<is_mono_serializable_v<T>>> {

	static constexpr bool is_serialized_with_prefix()noexcept {
		return false;
		}
	};
}
}
