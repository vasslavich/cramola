#include "../../internal.h"
#include "../internal_terms.h"
#include "../isrlz.h"
#include "../utype_trait_std.h"


using namespace crm::srlz;


struct A {
	int i;
	char c;
	};

struct B : A{};

struct Ad {
	std::string s;
	int i;
	};

B bx1{ 1, 'c' };
static_assert(std::is_trivial_v<B>, __FILE_LINE__);
static_assert(detail::is_strob_fixed_serialized_size_v<A>, __FILE_LINE__);
static_assert(!detail::is_strob_fixed_serialized_size_v<Ad>, __FILE_LINE__);
static_assert(detail::is_fixed_size_serialized_v<uint64_t>, __FILE_LINE__);
static_assert(detail::is_fixed_size_serialized_v<A>, __FILE_LINE__);
static_assert(detail::is_fixed_size_serialized_v<srlz_prefix_t>, __FILE_LINE__);
static_assert(detail::is_fixed_size_serialized_v<detail::serializable_properties_prefix_trait_nbytes<uint8_t>>, __FILE_LINE__);
static_assert(detail::is_fixed_size_serialized_v<detail::serializable_properties_prefix_trait_nbytes<uint16_t>>, __FILE_LINE__);

static_assert(detail::is_fixed_size_serialized_t<A, crm::none_options>::value == 2, __FILE_LINE__);
static_assert(write_size_v<A> == (
	write_size_v<decltype(A::i)> + write_size_v<decltype(A::c)>), __FILE_LINE__);

static_assert(!detail::is_strob_serializable_v<uint64_t>, __FILE_LINE__);
static_assert(is_serializable_v<srlz_prefix_t>, __FILE_LINE__);
static_assert(detail::is_strob_serializable_strong_v<srlz_prefix_t>, __FILE_LINE__);
static_assert(detail::is_strob_serializable_v<srlz_prefix_t>, __FILE_LINE__);
static_assert(detail::is_strob_flat_serializable_v<srlz_prefix_t>, __FILE_LINE__);
constexpr size_t mfx = detail::detect_fields_count_dispatch<srlz_prefix_t>(detail::size_t_<sizeof(srlz_prefix_t) * 8>{}, 0L);
static_assert(detail::__fields_count_hepler_t<srlz_prefix_t>::count == 2, __FILE_LINE__);
static_assert(detail::is_strob_serializable_v<detail::serializable_properties_prefix_trait_nbytes<uint8_t>>, __FILE_LINE__);
static_assert(!detail::is_strob_serializable_strong_v<detail::serializable_properties_prefix_trait_nbytes<uint8_t>>, __FILE_LINE__);


/*srlz_prefix_t crm::srlz::create_srlz_prefixed( size_t length ) {

	if (length <= std::numeric_limits< serialized_length_type>::max()) {
		srlz_prefix_t pref;
		pref.streamed_length = length;
		pref.type = srlz_prefix_type_t::prefixed;
		return pref;
		}
	else {
		THROW_EXC_FWD(nullptr);
		}
	}

srlz_prefix_t crm::srlz::create_srlz_eof() {
	srlz_prefix_t pref;
	pref.streamed_length = srlz_prefix_length_undefined;
	pref.type = srlz_prefix_type_t::eof;
	return pref;
	}
	*/
/*! ���������������� ������������������ ��� ����������� ����� �������������� ����� ������ */
const std::vector<uint8_t> eofMarkerSequence{0xAA, 0xCC, 0xAA, 0xCC, 0xAA, 0xCC, 0xAA, 0xCC};

const std::vector<uint8_t>& serialized_base_definitions::get_eof_marker()const noexcept{
	return eofMarkerSequence;
	}

bool serialized_base_definitions::eof_marker_validate( const std::vector<uint8_t> & eofm )const noexcept {
	return get_eof_marker() == eofm;
	}

