#include "../../internal.h"
#include "../internal_terms.h"
#include "../typeid.h"
#include "../streams.h"
#include "../../utilities/hash/cmn.h"


using namespace crm;
using namespace crm::srlz;



i_typeid_t::~i_typeid_t() {}



std::array<uint8_t, address_hash_size> crm::srlz::str2hash( const std::string & s ){
	const auto h128 = crm::hash::hash_128( s );
	address_hash_t h;
	apply_at_hash(h, h128);

	return h.to_bytes<uint8_t, address_hash_size>();
}


void crm::srlz::apply_at_hash( address_hash_t & to, const i_typeid_t & value ) {
	apply_at_hash( to, value.type_id() );
	apply_at_hash( to, value.type_name() );
	}





const typemap_key_t typemap_key_t::null;

typemap_key_t::typemap_key_t()noexcept {}

typemap_key_t::typemap_key_t(const i_typeid_t& tid)noexcept
	:typemap_key_t(tid.type_id(), tid.type_name()) {}

typemap_key_t::typemap_key_t( const type_identifier_t & id, std::string_view name )noexcept 
	: _name( name )
	, _id( id ) {
	
	CBL_VERIFY(!_name.empty());
	CBL_VERIFY(!is_null(_id));

	update_hash();
	}

void typemap_key_t::update_hash()noexcept {
	_hash = address_hash_t{};
	apply_at_hash(_hash, _name);
	apply_at_hash(_hash, _id);
	}

 std::string typemap_key_t::type_name()const  noexcept{
	return _name;
	}

 type_identifier_t typemap_key_t::type_id()const  noexcept{
	return _id;
	}

size_t typemap_key_t::get_serialized_size()const noexcept{
	return object_trait_serialized_size(_id)
		+ object_trait_serialized_size(_name)
		+ object_trait_serialized_size(_hash);
	}

const address_hash_t& typemap_key_t::object_hash()const noexcept {
	return _hash;
	}


bool crm::srlz::operator == (const i_typeid_t & lval, const i_typeid_t &rval)noexcept{
	return lval.type_id() == rval.type_id() && lval.type_name() == rval.type_name();
	}

bool crm::srlz::operator != (const i_typeid_t & lval, const i_typeid_t &rval)noexcept{
	return lval.type_id() != rval.type_id() || lval.type_name() != rval.type_name();
	}

bool crm::srlz::operator < (const i_typeid_t & lval, const i_typeid_t &rval)noexcept{
	if( lval.type_id() < rval.type_id() )return true;
	else {
		if( lval.type_id() > rval.type_id() )
			return false;
		else
			return lval.type_name() < rval.type_name();
		}
	}

bool crm::srlz::operator > (const i_typeid_t & lval, const i_typeid_t &rval) noexcept{
	if( lval.type_id() > rval.type_id() )return true;
	else {
		if( lval.type_id() < rval.type_id() )
			return false;
		else
			return lval.type_name() > rval.type_name();
		}
	}

bool crm::srlz::operator >= (const i_typeid_t & lval, const i_typeid_t &rval)noexcept{
	if( lval.type_id() > rval.type_id() )return true;
	else {
		if( lval.type_id() < rval.type_id() )
			return false;
		else
			return lval.type_name() >= rval.type_name();
		}
	}

bool crm::srlz::operator <= (const i_typeid_t & lval, const i_typeid_t &rval)noexcept{
	if( lval.type_id() < rval.type_id() )return true;
	else {
		if( lval.type_id() > rval.type_id() )
			return false;
		else
			return lval.type_name() <= rval.type_name();
		}
	}


size_t std::hash<crm::srlz::typemap_key_t>::operator()(const crm::srlz::typemap_key_t& v) const noexcept {
	return crm::as_std_hash(v);
	}




const iol_typeid_spcf_t iol_typeid_spcf_t::null;

iol_typeid_spcf_t::iol_typeid_spcf_t() noexcept {}

iol_typeid_spcf_t::iol_typeid_spcf_t(const global_object_identity_t& id)noexcept
	: _typeId(id) {}

iol_typeid_spcf_t::iol_typeid_spcf_t(const global_object_identity_t& id, const std::string& name)noexcept
	: _typeId(id)
	, _typeName(name) {}

type_identifier_t iol_typeid_spcf_t::type_id()const  noexcept {
	return _typeId;
	}

void iol_typeid_spcf_t::set_id(const type_identifier_t& id) noexcept {
	_typeId = id;
	}

std::string iol_typeid_spcf_t::type_name()const  noexcept {
	return _typeName;
	}

void iol_typeid_spcf_t::set_name(const std::string& name)  noexcept {
	_typeName = name;
	}

bool iol_typeid_spcf_t::is_null()const  noexcept {
	return (*this) == null;
	}

std::size_t iol_typeid_spcf_t::get_serialized_size()const noexcept {
	return srlz::object_trait_serialized_size(_typeId)
		+ srlz::object_trait_serialized_size(_typeName);
	}

bool crm::operator==(const iol_typeid_spcf_t& l, const iol_typeid_spcf_t& r) noexcept {
	return l.type_id() == r.type_id() && l.type_name() == r.type_name();
	}

bool crm::operator!=(const iol_typeid_spcf_t& l, const iol_typeid_spcf_t& r)  noexcept {
	return !(l == r);
	}

