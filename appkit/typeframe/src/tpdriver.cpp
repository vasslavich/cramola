#include "../tpdriver.h"
#include "../../internal.h"
#include "../../notifications/exceptions/excdrv.h"


using namespace crm;


types_driver_t::types_driver_t()
	: _excDrv(std::make_shared<exceptions_driver_t>()) {}

std::shared_ptr<exceptions_driver_t> types_driver_t::get_excdrv() {
	return std::atomic_load_explicit( &_excDrv, std::memory_order::memory_order_acquire );
	}

std::shared_ptr<const exceptions_driver_t> types_driver_t::get_excdrv()const {
	return std::atomic_load_explicit( &_excDrv, std::memory_order::memory_order_acquire );
	}

std::shared_ptr<exceptions_driver_t> types_driver_t::exceptions_driver() {
	return get_excdrv();
	}

std::shared_ptr<const exceptions_driver_t> types_driver_t::exceptions_driver()const {
	return get_excdrv();
	}
