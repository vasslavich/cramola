#pragma once


#include <string>
#include <sstream>
#include <typeinfo>
#include "./base_terms.h"
#include "./base_typetrait_decls.h"
#include "./base_typetrait_using.h"
#include "../utilities/hash.h"
#include "./itypeid.h"
#include "./isrlz.h"


namespace crm::srlz{


class typemap_key_t final :
	public i_typeid_t {

private:
	std::string _name;
	type_identifier_t _id;
	address_hash_t _hash;

	void update_hash()noexcept;

public:
	static const typemap_key_t null;

	typemap_key_t()noexcept;
	typemap_key_t( const i_typeid_t& tid )noexcept;
	typemap_key_t( const type_identifier_t & id, std::string_view name )noexcept;

	template<typename _Tx,
		typename T = std::decay_t<_Tx>,
		typename std::enable_if<std::is_base_of_v<i_typeid_t, T>, int>::type = 0>
	static const typemap_key_t& make()noexcept(std::is_nothrow_default_constructible_v<T>) {
		static const typemap_key_t _DefaultValue(static_cast<const i_typeid_t&>(T{}));
		return _DefaultValue;
		}

	template<typename _Tx,
		typename T = std::decay_t<_Tx>,
		typename std::enable_if<!std::is_base_of_v<i_typeid_t, T>, int>::type = 0>
	static const typemap_key_t& make()noexcept(
		std::is_nothrow_invocable_v<decltype(make_type<T>::id())> &&
		std::is_nothrow_invocable_v<decltype(make_type<T>::name())>
		) {
		static const typemap_key_t _DefaultValue{ make_type<T>::id(), make_type<T>::name() };
		return _DefaultValue;
		}

	std::string type_name()const noexcept final;
	type_identifier_t type_id()const noexcept final;

	template<typename Ts>
	void srlz(Ts && dest)const{
		serialize(dest, _id);
		serialize(dest, _name);
		serialize(dest, _hash);
		}

	template<typename Ts>
	void dsrlz(Ts && src ){
		deserialize(src, _id);
		deserialize(src, _name);
		deserialize(src, _hash);
		}

	size_t get_serialized_size()const noexcept;

	const address_hash_t& object_hash()const noexcept;
	};

//using typemap_key_t = typemap_key_t;

/*! ������� ���������� ���������� i_typeid_t, ��� ������������� ���� ������ */
template<typename Ttype>
class typeid_t : public i_typeid_t {
public:
	std::string type_name()const noexcept {
		return make_type<Ttype>::name();
		}

	type_identifier_t type_id()const noexcept {
		return make_type<Ttype>::id();
		}
	};


bool operator == (const i_typeid_t & lval, const i_typeid_t &rval)noexcept;
bool operator != (const i_typeid_t & lval, const i_typeid_t &rval)noexcept;
bool operator < (const i_typeid_t & lval, const i_typeid_t &rval)noexcept;
bool operator > (const i_typeid_t & lval, const i_typeid_t &rval)noexcept;
bool operator >= (const i_typeid_t & lval, const i_typeid_t &rval)noexcept;
bool operator <= (const i_typeid_t & lval, const i_typeid_t &rval)noexcept;
}


template<>
struct ::std::hash<crm::srlz::typemap_key_t> {
	size_t operator()(const crm::srlz::typemap_key_t& v) const noexcept;
	};



