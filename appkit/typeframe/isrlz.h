#pragma once


#include <type_traits>
#include <array>
#include <vector>
#include <memory>
#include "./options.h"



namespace crm::srlz{

namespace detail{
struct rstream_base_handler;
struct wstream_base_handler;
}




struct serialized_base_definitions {
	static constexpr size_t not_available_value = (size_t)(-1);

	virtual ~serialized_base_definitions() {}


	virtual const std::vector<uint8_t>& get_eof_marker()const noexcept;
	virtual bool eof_marker_validate(const std::vector<uint8_t> & eofm)const noexcept;
	};


struct serialized_base_r : public virtual serialized_base_definitions {
	virtual void deserialize(detail::rstream_base_handler & src) = 0;

	template<typename S,
		typename Options = none_options,
		typename std::enable_if_t<!std::is_base_of_v<detail::rstream_base_handler, std::decay_t<S>>, int> = 0>
	void deserialize(S&& s, Options = {}) {
		detail::rstream_base_handler rh(std::forward<S>(s));
		deserialize(rh);
		}
	};

struct serialized_base_w : public virtual serialized_base_definitions {
	virtual void serialize(detail::wstream_base_handler & dest)const = 0;

	template<typename S,
		typename Options = none_options,
		typename std::enable_if_t<!std::is_base_of_v<detail::wstream_base_handler, std::decay_t<S>>, int> = 0>
	void serialize(S&& s, Options = {}) {
		detail::wstream_base_handler rh(std::forward<S>(s));
		serialize(rh);
		}
	};

struct serialized_base_rw : public serialized_base_r, public serialized_base_w{};


namespace detail{


template<typename T>
struct is_serialized_trait_r<T,
	std::void_t<std::enable_if_t<std::is_base_of_v<serialized_base_r, std::decay_t<T>>>>> : std::true_type{};


template<typename T>
struct is_serialized_trait_w<T,
	std::void_t<std::enable_if_t<std::is_base_of_v<serialized_base_w, std::decay_t<T>>>>> : std::true_type{};


template<typename T>
struct is_serialized_trait<T,
	std::void_t<std::enable_if_t<(
		std::is_base_of_v<serialized_base_rw, std::decay_t<T>>) ||
	(std::is_base_of_v<serialized_base_w, std::decay_t<T>> &&
		std::is_base_of_v<serialized_base_r, std::decay_t<T>>)>>> : std::true_type{};
}
}


