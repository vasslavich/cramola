#pragma once


#include "./base_typetrait_decls.h"
#include "./base_typetrait_using.h"


namespace crm::srlz {
namespace detail {

template<typename TObject, typename TPrefix, typename Options>
struct serialize_prefix_size_<TObject, TPrefix, Options,
	std::enable_if_t<is_i_srlzd_prefixed_v<TObject>&& type_prefix_serialize_cond_v<TObject, Options>>> {

	static_assert(is_trait_as_options_v<Options>, __FILE_LINE__);
	using prefix_type = typename TPrefix;

	static constexpr size_t prefix_serialized_size()noexcept {
		return object_trait_serialized_size(prefix_type{}, serialized_options_cond<prefix_type, Options>{});
		}

	static constexpr size_t get_serialized_size(const std::decay_t<TObject>& obj)noexcept {
		return prefix_serialized_size() + obj.get_serialized_size();
		}
	};

template<typename TObject, typename TPrefix, typename Options>
struct serialize_prefix_size_<TObject, TPrefix, Options,
	std::enable_if_t<is_i_srlzd_prefixed_v<TObject> && !type_prefix_serialize_cond_v<TObject, Options>>> {

	static_assert(is_trait_as_options_v<Options>, __FILE_LINE__);
	using prefix_type = typename TPrefix;

	static constexpr size_t get_serialized_size(const std::decay_t<TObject>& obj)noexcept {
		return obj.get_serialized_size();
		}
	};

template<typename TObject, typename TPrefix, typename Options>
struct serialize_prefix_size_<TObject, TPrefix, Options,
	std::enable_if_t<is_i_srlzd_eof_v<TObject>&& type_prefix_serialize_cond_v<TObject, Options>>> {

	static_assert(is_trait_as_options_v<Options>, __FILE_LINE__);
	using prefix_type = typename TPrefix;

	static size_t prefix_serialized_size()noexcept {
		return object_trait_serialized_size(prefix_type{}, serialized_options_cond<prefix_type, Options>{});
		}
	};

template<typename TObject, typename TPrefix, typename Options>
struct serialize_prefix_size_<TObject, TPrefix, Options,
	std::enable_if_t<is_i_srlzd_eof_v<TObject> && !type_prefix_serialize_cond_v<TObject, Options>>> {

	static_assert(is_trait_as_options_v<Options>, __FILE_LINE__);
	using prefix_type = typename TPrefix;
	};


template<typename T, typename Options>
struct serialize_prefix<T, Options, std::enable_if_t<is_i_srlzd_prefixed_v<T>>>
	: serialize_prefix_size_<T, typename serializable_prefix_type_trait_t<T, Options>::prefix_trait, Options > {
	static_assert(is_trait_as_options_v<Options>, __FILE_LINE__);

	using type = typename std::decay_t<T>;
	using prefix_trait = typename serializable_prefix_type_trait_t<T, Options>;
	using prefix_type = typename prefix_trait::prefix_trait;
	using length_type = typename prefix_trait::length_type;

	static constexpr auto make_prefix(const type& obj)noexcept {
		const auto ssize = get_serialized_size(obj);
		if(ssize <= prefix_trait::maxsize) {

			prefix_type pref;
			pref.length = (length_type)ssize;
			pref.type = srlz_prefix_type_t::prefixed;
			return pref;
			}
		else {
			FATAL_ERROR_FWD(nullptr);
			}
		}
	};

template<typename T, typename Options>
struct serialize_prefix<T, Options, std::enable_if_t<is_i_srlzd_eof_v<T>>>
	: serialize_prefix_size_<T, typename serializable_prefix_type_trait_t<T, Options>::prefix_trait, Options> {
	static_assert(is_trait_as_options_v<Options>, __FILE_LINE__);

	using type = typename std::decay_t<T>;
	using prefix_trait = typename serializable_prefix_type_trait_t<T, Options>;
	using prefix_type = typename prefix_trait::prefix_trait;
	using length_type = typename prefix_trait::length_type;

	static auto make_prefix(const type&)noexcept {
		prefix_type pref;
		pref.length = 0;
		pref.type = srlz_prefix_type_t::eof;
		return pref;
		}
	};


template<typename TValue, typename Options>
struct srlz_trait_t<TValue, Options, typename std::enable_if_t<is_i_srlzd_prefixed_v<TValue>>> : serialize_prefix <TValue, Options> {
	static_assert(is_trait_as_options_v<Options>, __FILE_LINE__);
	static_assert(is_i_srlzd_prefixed_v<TValue>, "bad consistency");

	template<typename Tws,
		typename TObject,
		typename std::enable_if_t<(
			is_i_srlzd_prefixed_v<TObject>
			), int> = 0>
		static auto write_prefix(Tws&& dest, TObject&& object_) {

#ifdef USE_DCF_CHECK
		size_t destOff = dest.offset();
#endif

		const auto prefix = make_prefix(std::forward<TObject>(object_));
		SERIALIZE_VERIFY(prefix.type == srlz_prefix_type_t::prefixed);

		serialize(std::forward<Tws>(dest), prefix, serialized_options_cond<decltype(prefix), Options>{});

#ifdef USE_DCF_CHECK
		if constexpr(type_prefix_serialize_cond_v<TObject, Options>) {
			if((dest.offset() - destOff) != prefix_serialized_size())
				SERIALIZE_THROW_EXC_FWD(L"bad format");
			}
#endif

		return prefix;
		}

	template<typename Trs,
		typename TObject,
		typename std::enable_if_t<(
			is_i_srlzd_prefixed_v<TObject>
			), int> = 0>
	static auto read_prefix(Trs&& src, TObject&&) {

#ifdef USE_DCF_CHECK
		size_t srcOff = src.offset();
#endif

		serialize_prefix_type<TObject, Options> prefix;
		deserialize(std::forward<Trs>(src), prefix, serialized_options_cond<decltype(prefix), Options>{});

#ifdef USE_DCF_CHECK
		if constexpr(type_prefix_serialize_cond_v<TObject, Options>) {
			if((src.offset() - srcOff) != prefix_serialized_size())
				SERIALIZE_THROW_EXC_FWD(L"bad format");
			}

		if(prefix.type != srlz_prefix_type_t::prefixed)
			SERIALIZE_THROW_EXC_FWD(L"bad prefix");
#endif

		return prefix;
		}
	};

template<typename TValue, typename Options>
struct srlz_trait_t<TValue, Options, typename std::enable_if_t<is_i_srlzd_eof_v<TValue>>> :serialize_prefix <TValue, Options> {
	static_assert(is_trait_as_options_v<Options>, __FILE_LINE__);
	static_assert(is_i_srlzd_eof_v<TValue>, "bad consistency");

	template<typename Tws,
		typename TObject,
		typename std::enable_if_t<(
			is_i_srlzd_eof_v<TObject>/* && is_serialization_wstream_v<Tws>*/
			), int> = 0>
		static auto write_prefix(Tws&& dest, TObject&& obj) {

#ifdef USE_DCF_CHECK
		size_t destOff = dest.offset();
#endif

		auto prefix = make_prefix(std::forward<TObject>(obj));
		SERIALIZE_VERIFY(prefix.type == srlz_prefix_type_t::eof);

		serialize(std::forward<Tws>(dest), prefix, serialized_options_cond<decltype(prefix), Options>{});

#ifdef USE_DCF_CHECK
		if constexpr(type_prefix_serialize_cond_v<TObject, Options>) {
			if((dest.offset() - destOff) != prefix_serialized_size())
				SERIALIZE_THROW_EXC_FWD(L"bad format");
			}
#endif

		return prefix;
		}

	template<typename Trs,
		typename TObject,
		typename std::enable_if_t<(
			is_i_srlzd_eof_v<TObject>
			), int> = 0>
		static auto read_prefix(Trs&& src, TObject&&) {

#ifdef USE_DCF_CHECK
		size_t srcOff = src.offset();
#endif

		prefix_type prefix;
		deserialize(std::forward<Trs>(src), prefix, serialized_options_cond<decltype(prefix), Options>{});

#ifdef USE_DCF_CHECK
		if constexpr(type_prefix_serialize_cond_v<TObject, Options>) {
			if((src.offset() - srcOff) != prefix_serialized_size())
				SERIALIZE_THROW_EXC_FWD(L"bad format");
			}

		if(prefix.type != srlz_prefix_type_t::eof)
			SERIALIZE_THROW_EXC_FWD(L"bad prefix");
#endif

		return prefix;
		}
	};

}
}
