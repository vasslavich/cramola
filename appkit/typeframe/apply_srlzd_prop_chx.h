#pragma once


#include "../typeframe/base_typetrait_decls.h"
#include "../channel_terms/session_desc.h"
#include "../channel_terms/base_terms.h"
#include "../channel_terms/address_desc.h"


namespace crm::srlz {


template<typename Options>
struct serializable_properties_trait< session_description_t, Options> {
	using serialized_options_type = detail::option_max_serialized_size<CRM_KB>;
	static constexpr auto get()noexcept {
		return serializable_properties{ serialized_options_type::max_serialized_size() };
		}
	};

template<typename Options>
struct serializable_properties_trait<identity_descriptor_t, Options> {
	using serialized_options_type = detail::option_max_serialized_size<identity_descriptor_t::max_full_size>;
	static constexpr auto get()noexcept {
		return serializable_properties{ serialized_options_type::max_serialized_size() };
		}
	};


template<typename Options>
struct serializable_properties_trait< address_descriptor_t, Options> {
	using serialized_options_type = detail::option_max_serialized_size<CRM_KB>;

	static constexpr auto get()noexcept {
		return serializable_properties{ serialized_options_type::max_serialized_size() };
		}
	};
}

