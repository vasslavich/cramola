#pragma once


#include <string>
#include <vector>
#include "../utilities/hash/predecl.h"


namespace crm{

#ifdef CBL_MESSAGE_ADDRESS_TYPES_SHORT
/*! Type identifier */
using type_identifier_t = sx_glob_64;
#else
/*! Type identifier */
using type_identifier_t = sx_uuid_t;
#endif


namespace srlz {


/*! ��������� �������������� ���� */
struct i_typeid_t {
public:
	virtual ~i_typeid_t() = 0;

	/*! ������������� ���� */
	virtual type_identifier_t type_id()const noexcept = 0;
	/*! ������������� ����, ����� �������������� � ������, ���� ��������� �������� �� /ref id */
	virtual std::string type_name()const noexcept = 0;
	};


std::array<uint8_t, address_hash_size> str2hash(const std::string & s);
void apply_at_hash(address_hash_t & to, const i_typeid_t & value);

template<typename _XType,
	typename XType = std::decay_t<_XType>,
	typename = std::enable_if_t<!std::is_base_of_v<i_typeid_t, XType>>>
struct make_type {
	static constexpr std::string name()noexcept {
		return typeid(std::decay_t<XType>).name();
		}

	static constexpr type_identifier_t id()noexcept {
		return type_identifier_t{ str2hash(name()) };
		}
	};
}


class iol_typeid_spcf_t final : public srlz::i_typeid_t {
private:
	type_identifier_t  _typeId;
	std::string _typeName;

public:
	static const iol_typeid_spcf_t null;

	iol_typeid_spcf_t()noexcept;
	iol_typeid_spcf_t(const type_identifier_t& id) noexcept;
	iol_typeid_spcf_t(const type_identifier_t& id, const std::string& name) noexcept;

	void set_id(const type_identifier_t& id) noexcept;
	void set_name(const std::string& name) noexcept;

	/*const type_identifier_t& id()const  noexcept;*/
	/*const std::string& name()const  noexcept;*/

	type_identifier_t type_id()const  noexcept;
	std::string type_name()const  noexcept;

	bool is_null()const  noexcept;

	template<typename Ts>
	void srlz(Ts&& dest)const {
		srlz::serialize(dest, _typeId);
		crm::srlz::serialize(dest, _typeName);
		}

	template<typename Ts>
	void dsrlz(Ts&& src) {
		crm::srlz::deserialize(src, _typeId);
		crm::srlz::deserialize(src, _typeName);
		}

	std::size_t get_serialized_size()const noexcept;

	friend bool operator==(const iol_typeid_spcf_t& l, const iol_typeid_spcf_t& r)  noexcept;
	friend bool operator!=(const iol_typeid_spcf_t& l, const iol_typeid_spcf_t& r)  noexcept;

	template<typename T>
	static iol_typeid_spcf_t make() {
		return iol_typeid_spcf_t(typeid(T).hash_code(), typeid(T).name());
		}
	};


template <typename T>
class has_equals_fields_t {
private:
	typedef char Yes;
	typedef Yes No[2];

	template <typename U, U> struct really_has;

	template<typename C> static Yes& Test(really_has <bool(C::*)(const C&)const, &C::equals_fields>*);
	template<typename> static No& Test(...);

public:
	static bool const value = sizeof(Test<T>(0)) == sizeof(Yes);
	};


template <typename T>
class has_less_fields_t {
private:
	typedef char Yes;
	typedef Yes No[2];

	template <typename U, U> struct really_has;

	template<typename C> static Yes& Test(really_has <bool(C::*)(const C&)const, &C::less_fields>*);
	template<typename> static No& Test(...);

public:
	static bool const value = sizeof(Test<T>(0)) == sizeof(Yes);
	};

template <typename T>
class has_great_fields_t {
private:
	typedef char Yes;
	typedef Yes No[2];

	template <typename U, U> struct really_has;

	template<typename C> static Yes& Test(really_has <bool(C::*)(const C&)const, &C::great_fields>*);
	template<typename> static No& Test(...);

public:
	static bool const value = sizeof(Test<T>(0)) == sizeof(Yes);
	};

template <typename T>
class has_great_equals_fields_t {
private:
	typedef char Yes;
	typedef Yes No[2];

	template <typename U, U> struct really_has;

	template<typename C> static Yes& Test(really_has <bool(C::*)(const C&)const, &C::great_equals_fields>*);
	template<typename> static No& Test(...);

public:
	static bool const value = sizeof(Test<T>(0)) == sizeof(Yes);
	};

template <typename T>
class has_get_hash_t {
private:
	typedef char Yes;
	typedef Yes No[2];

	template <typename U, U> struct really_has;

	template<typename C> static Yes& Test(really_has <const address_hash_t&(C::*)()const, &C::object_hash>*);
	template<typename> static No& Test(...);

public:
	static bool const value = sizeof(Test<T>(0)) == sizeof(Yes);
	};



template<typename T>
using is_hash_get_interface_t = typename std::conditional<
(has_get_hash_t<T>::value),
std::true_type,
std::false_type>;


template<typename T>
using is_hash_fields_interface_t = typename std::conditional<
(has_equals_fields_t<T>::value
	&& has_less_fields_t<T>::value
	&& has_great_fields_t<T>::value
	&& has_great_equals_fields_t<T>::value),

	std::true_type,
	std::false_type>;



template<typename T>
struct is_1 {
	using t1 = typename is_hash_fields_interface_t<T>::type;
	static const bool value = t1::value;
	};


template<typename T>
struct is_2 {
	using t1 = typename is_hash_get_interface_t<T>::type;
	static const bool value = t1::value;
	};




template<typename T>
bool __hf_equals(const typename T &l, const typename T &r) noexcept {
	return l.equals_fields(r);
	}

template<typename T>
bool __hf_great(const typename T &l, const typename T &r) noexcept {
	return l.great_fields(r);
	}


template<typename T>
bool __hf_less(const typename T &l, const typename T &r) noexcept {
	return l.less_fields(r);
	}


template<typename T>
bool __hf_great_equals(const typename T &l, const typename T &r) noexcept {
	return l.great_equals_fields(r);
	}



template<typename T,
	typename std::enable_if<is_1<T>::value && is_2<T>::value, int>::type = 0>
	bool hf_equals(const typename T &l, const typename T &r)noexcept {
	return __hf_equals(l, r);
	}

template<typename T,
	typename std::enable_if<is_1<T>::value && is_2<T>::value, int>::type = 0>
	bool hf_great(const typename T &l, const typename T &r) noexcept {
	return __hf_great(l, r);
	}


template<typename T,
	typename std::enable_if<is_1<T>::value && is_2<T>::value, int>::type = 0>
	bool hf_less(const typename T &l, const typename T &r) noexcept {
	return __hf_less(l, r);
	}


template<typename T,
	typename std::enable_if<is_1<T>::value && is_2<T>::value, int>::type = 0>
	bool hf_great_equals(const typename T &l, const typename T &r) noexcept {
	return __hf_great_equals(l, r);
	}







template<typename T>
struct hf_operators_t {
	virtual ~hf_operators_t() {};

	using hash_t = address_hash_t;

	virtual const address_hash_t& object_hash()const noexcept = 0;

	/*! ����� r [�� ��������� �����]*/
	virtual bool equals_fields(const T& r)const noexcept = 0;
	/*! ������ ��� r [�� ��������� �����]*/
	virtual bool less_fields(const T& r)const noexcept = 0;
	/*! ������ ��� r [�� ��������� �����]*/
	virtual bool great_fields(const T& r)const noexcept = 0;
	/*! ������ ��� ����� r [�� ��������� �����]*/
	virtual bool great_equals_fields(const T& r)const noexcept = 0;
	};





template<typename T,
	typename std::enable_if<std::is_base_of<hf_operators_t<T>, T>::value, int>::type = 0>
	bool hf_equals(const typename T &l, const typename T &r)noexcept {
	return __hf_equals(l, r);
	}

template<typename T,
	typename std::enable_if<std::is_base_of<hf_operators_t<T>, T>::value, int>::type = 0>
	bool hf_great(const typename T &l, const typename T &r) noexcept {
	return __hf_great(l, r);
	}


template<typename T,
	typename std::enable_if<std::is_base_of<hf_operators_t<T>, T>::value, int>::type = 0>
	bool hf_less(const typename T &l, const typename T &r) noexcept {
	return __hf_less(l, r);
	}


template<typename T,
	typename std::enable_if<std::is_base_of<hf_operators_t<T>, T>::value, int>::type = 0>
	bool hf_great_equals(const typename T &l, const typename T &r) noexcept {
	return __hf_great_equals(l, r);
	}




template<typename T,
	typename std::enable_if_t<(std::is_base_of_v<hf_operators_t<T>, T> && !detail::has_hashable_support_v<T>), int> = 0>
bool operator==(const T &l, const T &r) noexcept {
	return __hf_equals(l, r);
	}

template<typename T,
	typename std::enable_if_t<(std::is_base_of_v<hf_operators_t<T>, T> && !detail::has_hashable_support_v<T>), int> = 0>
	bool operator!=(const T &l, const T &r) noexcept {
	return !(l == r);
	}

template<typename T,
	typename std::enable_if_t<(std::is_base_of_v<hf_operators_t<T>, T> && !detail::has_hashable_support_v<T>), int> = 0>
	bool operator<(const T &l, const T &r) noexcept {
	return __hf_less(l, r);
	}

template<typename T,
	typename std::enable_if_t<(std::is_base_of_v<hf_operators_t<T>, T> && !detail::has_hashable_support_v<T>), int> = 0>
	bool operator>(const T &l, const T &r)noexcept {
	return __hf_great(l, r);
	}

template<typename T,
	typename std::enable_if_t<(std::is_base_of_v<hf_operators_t<T>, T> && !detail::has_hashable_support_v<T>), int> = 0>
	bool operator>=(const T &l, const T &r)noexcept {
	return __hf_great_equals(l, r);
	}


template<typename T, typename = std::void_t<>>
struct is_i_typeid_based : std::false_type {};


template<typename T>
struct is_i_typeid_based<T, 
	typename std::enable_if_t<std::is_base_of_v<srlz::i_typeid_t, std::decay_t<T>>>> : std::true_type {};

template<typename T>
constexpr bool is_i_typeid_based_v = is_i_typeid_based<T>::value;
}



