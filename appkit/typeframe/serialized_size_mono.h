#pragma once


#include "./base_typetrait_decls.h"
#include "./base_typetrait_using.h"
#include "./options.h"


namespace crm::srlz {

template<typename _TxValueMono,
	typename Options = none_options,
	typename TValueMono = typename std::decay_t<_TxValueMono>,
	typename std::enable_if_t<detail::is_mono_serializable_v<TValueMono>, int> = 0>
constexpr size_t object_trait_serialized_size(_TxValueMono&&, Options = {})noexcept {
	static_assert(is_trait_as_options_v<Options>, __FILE_LINE__);
	return write_size_v<std::decay_t<TValueMono>, Options>;
	}
}