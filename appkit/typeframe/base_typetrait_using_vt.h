#pragma once

#include "./base_typetrait_decls.h"
#include "./base_typetrait_using.h"

namespace crm {
namespace srlz {
namespace detail {


template<typename ... AL>
constexpr bool is_all_srlzd_prefixed_v = (is_srlzd_prefixed_v<AL> && ...);

template<typename ... AL>
constexpr bool is_all_srlzd_eof_v = (is_i_srlzd_eof_v<AL> && ...);

template<typename ... AL>
constexpr bool is_all_srlzd_mono_v = (is_mono_serializable_v<AL> && ...);

template<typename ... AL>
constexpr bool is_all_srlzd_v = (is_serializable_v<AL> && ...);




template<class T, typename ... AL>
constexpr bool exists_type_impl() {
	return ((std::is_same_v<std::decay_t<T>, std::decay_t<AL>>) | ...);
	}

template<typename T, typename ... AL>
constexpr bool exists_type_split() {
	if constexpr(sizeof...(AL) > 0) {
		return exists_type_impl<T, AL...>();
		}
	else {
		return false;
		}
	}

template<typename ... AL>
constexpr bool exists_type() {
	return exists_type_split<AL...>();
	}

template<typename ... AL>
constexpr bool exists_type_v = exists_type<AL...>();







template<class T, typename ... AL>
constexpr bool is_has_inherited_from_impl() {
	return ((std::is_base_of_v<std::decay_t<T>, std::decay_t<AL>>) || ...);
	}

template<typename T, typename ... AL>
constexpr bool is_has_inherited_from_impl_split() {
	if constexpr(sizeof...(AL) > 0) {
		return is_has_inherited_from_impl<T, AL...>();
		}
	else {
		return false;
		}
	}

template<typename ... AL>
constexpr bool is_has_inherited_from() {
	return is_has_inherited_from_impl_split<AL...>();
	}

template<typename ... AL>
constexpr bool is_has_inherited_from_v = is_has_inherited_from<AL...>();








template<typename ... AL>
constexpr size_t count_of()noexcept {
	using tuple_type = typename std::decay_t<std::tuple<AL...>>;
	return std::tuple_size_v<tuple_type>;
	}

template<typename ... AL>
constexpr size_t count_of_v = count_of<AL...>();











constexpr size_t serialized_size_all(serialized_size_dispatch)noexcept {
	return 0;
	}

template<typename T,
	typename ... AL>
	constexpr auto serialized_size_all(serialized_size_dispatch tag, T&& h, AL&& ... tail)noexcept ->
	typename std::enable_if_t<is_all_srlzd_prefixed_v<T, AL...>, size_t> {
	return object_trait_serialized_size(std::forward<T>(h)) + serialized_size_all(tag, std::forward<AL>(tail)...);
	}
}


template<typename ... AL>
constexpr auto object_trait_serialized_size_all(AL&& ... al)noexcept ->
typename std::enable_if_t<detail::is_all_srlzd_prefixed_v<AL...>, size_t> {
	if constexpr(sizeof...(AL) > 1) {
		return serialized_size_all(detail::serialized_size_dispatch{}, std::forward<AL>(al)...);
		}
	else {
		if constexpr(sizeof...(AL) == 1) {
			return object_trait_serialized_size(std::forward<AL>(al)...);
			}
		else {
			return 0;
			}
		}
	}

template<typename Options, typename ... AL>
constexpr auto object_trait_serialized_size(const std::tuple<AL...>& tpl, Options)noexcept -> typename std::enable_if_t<detail::is_all_srlzd_v<AL...>, size_t> {
	static_assert(is_trait_as_options_v<Options>, __FILE_LINE__);
	return detail::tuple_prefixed_serialized_size_impl<0, std::tuple_size_v<std::decay_t<decltype(tpl)>>, Options>::size(tpl);
	}


template<typename ... AL>
constexpr auto object_trait_serialized_size(const std::tuple<AL...>& tpl)noexcept -> typename std::enable_if_t<detail::is_all_srlzd_v<AL...>, size_t> {
	return detail::tuple_prefixed_serialized_size_impl<0, std::tuple_size_v<std::decay_t<decltype(tpl)>>, none_options>::size(tpl);
	}




template<typename T, typename ... AL>
constexpr bool is_has_same_type_as_from_impl()noexcept {
	return ((std::is_same_v<std::decay_t<T>, std::decay_t<AL>>) || ...);
	}

template<typename T, typename ... AL>
constexpr bool is_has_same_type_as_from_impl_split()noexcept {
	if constexpr(sizeof...(AL) > 0) {
		return is_has_same_type_as_from_impl<T, AL...>();
		}
	else {
		return false;
		}
	}

template<typename ... AL>
constexpr bool is_has_same_type_as_from() noexcept {
	return is_has_same_type_as_from_impl_split<AL...>();
	}

template<typename ... AL>
constexpr bool is_has_same_type_v = is_has_same_type_as_from<AL...>();
}
}

