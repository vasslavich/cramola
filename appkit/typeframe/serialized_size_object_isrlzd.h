#pragma once


#include "./base_typetrait_decls.h"
#include "./base_typetrait_using.h"
#include "./options.h"


namespace crm::srlz {
namespace detail {


template<typename TValueISerialized,
	typename Options = none_options,
	typename std::enable_if_t<(
		detail::is_i_srlzd_prefixed_v<TValueISerialized> &&
		!detail::is_fixed_size_serialized_v<TValueISerialized, Options>
		), int> = 0>
size_t serialized_size_i_srlzd(TValueISerialized&& object, Options = {})noexcept {
	return srlz_object_trait_t<typename std::decay_t<TValueISerialized>, Options>::get_serialized_size(std::forward<TValueISerialized>(object));
	}

template<typename TValueISerialized,
	typename Options = none_options,
	typename std::enable_if_t<(
		detail::is_i_srlzd_prefixed_v<TValueISerialized>&&
		detail::is_fixed_size_serialized_v<TValueISerialized, Options>
		), int> = 0>
constexpr size_t serialized_size_i_srlzd()noexcept{
	return write_size_v<TValueISerialized, Options>;
	}

template<typename T>
struct serialized_size_i_srlzd_fault : std::false_type {};

template<typename TValueISerialized,
	typename Options = none_options,
	typename std::enable_if_t<detail::is_i_srlzd_eof_v<TValueISerialized>, int> = 0>
size_t serialized_size_i_srlzd(TValueISerialized&& object, Options = {}) noexcept {
	static_assert(serialized_size_i_srlzd_fault< TValueISerialized>::value, __FILE_LINE__);
	}
}


template<typename _TxISerializedPrefixed,
	typename Options = none_options,
	typename TISerializedPrefixed = typename std::decay_t<_TxISerializedPrefixed>,
	typename std::enable_if_t<detail::is_i_srlzd_prefixed_v<TISerializedPrefixed>, int> = 0>
constexpr size_t object_trait_serialized_size(_TxISerializedPrefixed&& c , Options = {})noexcept {
	static_assert(is_trait_as_options_v<Options>, __FILE_LINE__);

	/*! compiler only */
	(void)c;

	if constexpr(detail::is_fixed_size_serialized_v<TISerializedPrefixed, Options>) {
		return detail::serialized_size_i_srlzd<TISerializedPrefixed, Options>();
		}
	else {
		return detail::serialized_size_i_srlzd(std::forward<_TxISerializedPrefixed>(c),
			Options{});
		}
	}


template<typename _TxISerializedEof,
	typename Options = none_options,
	typename TISerializedEof = typename std::decay_t<_TxISerializedEof>,
	typename std::enable_if_t<detail::is_i_srlzd_eof_v<TISerializedEof>, int> = 0>
size_t object_trait_serialized_size(_TxISerializedEof&& v, Options = {})noexcept {
	static_assert(is_trait_as_options_v<Options>, __FILE_LINE__);
	return detail::serialized_size_i_srlzd(std::forward<_TxISerializedEof>(v), Options{});
	}
}
