#include "../../appkit.h"

using namespace crm;

template<typename S, typename C>
auto flatting(S && s, C&& c)noexcept {
	srlz::serialize(std::forward<S>(s), std::forward<C>(c));
	return s.release();
	}

template<typename S, typename C>
auto deflat( S& s, C & c)noexcept {
	srlz::deserialize(s, c);
	return std::move(c);
	}

struct AsISrlzdPref final {
	template<typename S>
	void srlz(S&& )const {}

	template<typename S>
	void dsrlz(S&& ) {}

	size_t get_serialized_size()const noexcept;
	};

static_assert(srlz::detail::is_i_srlzd_v<AsISrlzdPref>, __FILE_LINE__);
static_assert(srlz::detail::is_container_of_isrlzd_v<std::vector<AsISrlzdPref>>, __FILE_LINE__);
static_assert(std::is_same_v<
	decltype(flatting(srlz::wstream_constructor_t::make(), std::declval<std::vector<AsISrlzdPref>>())),
	std::vector<uint8_t>>, 
	__FILE_LINE__);

template<typename TCont>
using rstream_container_type = typename std::decay_t<
	decltype(srlz::rstream_constructor_t::make(std::declval<std::vector<uint8_t>>()))>;

static_assert(std::is_same_v<
	decltype(deflat(std::declval<rstream_container_type<std::vector<uint8_t>>&>(), std::declval<std::vector<AsISrlzdPref>&>())),
	std::vector<AsISrlzdPref>>,
	__FILE_LINE__);

template<typename T>
T get_rvalue();

template<typename T>
T& get_lvalue();

std::vector<uint8_t> v8;

static_assert(srlz::detail::is_container_t<std::vector<std::uint8_t>>::value, "");
static_assert(srlz::detail::is_container_t<decltype(v8)>::value, "");
static_assert(srlz::detail::is_container_t<decltype(get_rvalue<std::vector<std::uint8_t>>())>::value, "");
static_assert(srlz::detail::is_container_t<decltype(get_lvalue<std::vector<std::uint8_t>>())>::value, "");

static_assert(srlz::detail::is_container_bytes_t<std::vector<std::uint8_t>>::value, "");
static_assert(srlz::detail::is_container_bytes_t<decltype(v8)>::value, "");
static_assert(srlz::detail::is_container_bytes_t<decltype(get_rvalue<std::vector<std::uint8_t>>())>::value, "");
static_assert(srlz::detail::is_container_bytes_t<decltype(get_lvalue<std::vector<std::uint8_t>>())> ::value, "");

static_assert(srlz::detail::is_iterator_byte_t<decltype(get_lvalue<std::vector<std::uint8_t>>().cbegin())>::value, "");
static_assert(srlz::detail::is_iterator_byte_t<decltype(get_rvalue<std::vector<std::uint8_t>>().cbegin())>::value, "");


