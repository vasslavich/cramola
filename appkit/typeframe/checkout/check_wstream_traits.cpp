#include "../../appkit.h"

static_assert(crm::srlz::detail::is_fixed_size_serialized_v<crm::sx_uuid_t>, __FILE_LINE__);
static_assert(crm::srlz::detail::is_fixed_size_serialized_v<crm::sx_glob_64>, __FILE_LINE__);
static_assert(crm::srlz::detail::is_fixed_size_serialized_v<crm::sx_locid_t>, __FILE_LINE__);
static_assert(crm::srlz::detail::is_fixed_size_serialized_v<crm::sx_loc_64>, __FILE_LINE__);

static_assert(crm::srlz::write_size_v<crm::sx_uuid_t> != 0, __FILE_LINE__);
static_assert(crm::srlz::write_size_v<crm::sx_glob_64> != 0, __FILE_LINE__);
static_assert(crm::srlz::write_size_v<crm::sx_locid_t> != 0, __FILE_LINE__);
static_assert(crm::srlz::write_size_v<crm::sx_loc_64> != 0, __FILE_LINE__);




