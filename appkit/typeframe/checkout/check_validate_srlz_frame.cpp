#include "../../internal.h"
#include "./check_validate_srlz_frame.h"


using namespace crm;
using namespace crm::srlz;
using namespace crm::srlz::detail;


static_assert(std::is_same_v<decltype(object_trait_serialized_size(std::declval<crm::detail::_packet_t>())), size_t>, __FILE_LINE__);
static_assert(crm::srlz::detail::fields_count_v<A1> == 3, __FILE_LINE__);

static_assert(is_i_srlzd_v<inotification_t>, __FILE_LINE__);
static_assert(std::is_default_constructible_v<inotification_t>, __FILE_LINE__);
static_assert(std::is_base_of_v<i_iol_handler_t, inotification_t>, __FILE_LINE__);

static_assert(
	is_ctr_as_i_srlzd_v<inotification_t>&&
	std::is_base_of_v<i_iol_handler_t, inotification_t>
	, __FILE_LINE__);
static_assert(std::is_base_of_v<i_type_ctr_i_srlzd_t<i_iol_handler_t>,
	fram_typectr4_i_srlzd_based_t<inotification_t, i_iol_handler_t>>, __FILE_LINE__);

static_assert(std::is_base_of_v<i_type_ctr_base_t,
	fram_typectr4_i_srlzd_based_t<inotification_t, i_iol_handler_t>>, __FILE_LINE__);

static_assert(is_ctr_as_i_srlzd_v<inotification_t>, __FILE_LINE__);


static_assert(std::is_same_v<decltype(type_ctr_cast_as_i_srlzd_based(
	std::declval<fram_typectr4_i_srlzd_based_t<inotification_t, i_iol_handler_t>*>())), i_type_ctr_i_srlzd_trait*>, __FILE_LINE__);


