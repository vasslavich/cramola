#include <array>
#include "../../appkit.h"


using namespace crm;

namespace {


using prefix_nbytes_u8 = srlz::detail::serializable_properties_prefix_trait_nbytes<uint8_t>;
using prefix_nbytes_u16 = srlz::detail::serializable_properties_prefix_trait_nbytes<uint16_t>;
using prefix_nbytes_u32 = srlz::detail::serializable_properties_prefix_trait_nbytes<uint32_t>;
using prefix_nbytes_u64 = srlz::detail::serializable_properties_prefix_trait_nbytes<uint64_t>;

static_assert(srlz::detail::has_serializable_properties_v<sx_uuid_t>, __FILE_LINE__);
static_assert(srlz::detail::has_serializable_properties_v<sx_locid_t>, __FILE_LINE__);
static_assert(srlz::detail::has_serializable_properties_v<sx_glob_64>, __FILE_LINE__);
static_assert(srlz::detail::has_serializable_properties_v<sx_loc_64>, __FILE_LINE__);
static_assert(srlz::detail::has_serializable_properties_v<address_descriptor_t>, __FILE_LINE__);
static_assert(srlz::detail::has_serializable_properties_v<session_description_t>, __FILE_LINE__);
static_assert(srlz::detail::has_serializable_properties_v<identity_descriptor_t>, __FILE_LINE__);

static_assert(srlz::detail::has_serializable_properties_v<prefix_nbytes_u8>, __FILE_LINE__);
static_assert(srlz::detail::has_serializable_properties_v<prefix_nbytes_u16>, __FILE_LINE__);
static_assert(srlz::detail::has_serializable_properties_v<prefix_nbytes_u32>, __FILE_LINE__);
static_assert(srlz::detail::has_serializable_properties_v<prefix_nbytes_u64>, __FILE_LINE__);

static_assert(srlz::write_size_v<prefix_nbytes_u8> == 1, __FILE_LINE__);
static_assert(srlz::write_size_v<prefix_nbytes_u16> == 2, __FILE_LINE__);
static_assert(srlz::write_size_v<prefix_nbytes_u32> == 4, __FILE_LINE__);
static_assert(srlz::write_size_v<prefix_nbytes_u64> == 8, __FILE_LINE__);

static_assert(srlz::detail::has_serializable_properties_as_fixed_v<prefix_nbytes_u8>, __FILE_LINE__);
static_assert(!srlz::detail::type_prefix_serialize_cond_v<prefix_nbytes_u8>, __FILE_LINE__);
static_assert(srlz::write_size_v<prefix_nbytes_u8> == (
	prefix_nbytes_u8::serialized_size<>()), __FILE_LINE__);

static_assert(srlz::detail::has_serializable_properties_as_fixed_v<prefix_nbytes_u16>, __FILE_LINE__);
static_assert(!srlz::detail::type_prefix_serialize_cond_v<prefix_nbytes_u16>, __FILE_LINE__);
static_assert(srlz::write_size_v<prefix_nbytes_u16> == (
	prefix_nbytes_u16::serialized_size<>()), __FILE_LINE__);

static_assert(srlz::detail::has_serializable_properties_as_fixed_v<prefix_nbytes_u32>, __FILE_LINE__);
static_assert(!srlz::detail::type_prefix_serialize_cond_v<prefix_nbytes_u32>, __FILE_LINE__);
static_assert(srlz::write_size_v<prefix_nbytes_u32> == (
	prefix_nbytes_u32::serialized_size<>()), __FILE_LINE__);

static_assert(srlz::detail::has_serializable_properties_as_fixed_v<prefix_nbytes_u64>, __FILE_LINE__);
static_assert(!srlz::detail::type_prefix_serialize_cond_v<prefix_nbytes_u64>, __FILE_LINE__);
static_assert(srlz::write_size_v<prefix_nbytes_u64> == (
	prefix_nbytes_u64::serialized_size<>()), __FILE_LINE__);


static_assert(srlz::write_size_v<sx_uuid_t> == (
	srlz::write_size_v<uint64_t> * 2 +
	srlz::detail::is_fixed_size_serialized_t < sx_uuid_t, none_options > ::prefix_serialized_size), __FILE_LINE__);

static_assert(srlz::write_size_v<sx_locid_t> == (
	sx_locid_t::serialized_size<>() +
	srlz::detail::is_fixed_size_serialized_t<sx_locid_t, none_options>::prefix_serialized_size), __FILE_LINE__);

static_assert(srlz::write_size_v<sx_glob_64> == (
	sx_glob_64::serialized_size<>() +
	srlz::detail::is_fixed_size_serialized_t<sx_glob_64, none_options>::prefix_serialized_size), __FILE_LINE__);

static_assert(srlz::write_size_v<sx_loc_64> == (
	sx_loc_64::serialized_size<>() +
	srlz::detail::is_fixed_size_serialized_t<sx_loc_64, none_options >::prefix_serialized_size), __FILE_LINE__);


static_assert(srlz::detail::is_type_serialized_options_as<sx_uuid_t, srlz::serialize_properties_length::as_fixedsize, none_options>(), __FILE_LINE__);
static_assert(srlz::detail::is_type_serialized_options_as<sx_locid_t, srlz::serialize_properties_length::as_fixedsize, none_options>(), __FILE_LINE__);
static_assert(srlz::detail::is_type_serialized_options_as<sx_glob_64, srlz::serialize_properties_length::as_fixedsize, none_options>(), __FILE_LINE__);
static_assert(srlz::detail::is_type_serialized_options_as<sx_locid_t, srlz::serialize_properties_length::as_fixedsize, none_options>(), __FILE_LINE__);

static_assert(srlz::detail::is_fixed_size_serialized_v<sx_loc_64>, __FILE_LINE__);
static_assert(srlz::detail::is_fixed_size_serialized_v<sx_glob_64>, __FILE_LINE__);
static_assert(srlz::detail::is_fixed_size_serialized_v<sx_uuid_t>, __FILE_LINE__);
static_assert(srlz::detail::is_fixed_size_serialized_v<sx_locid_t>, __FILE_LINE__);

static_assert(!srlz::detail::is_fixed_size_serialized_v<address_descriptor_t>, __FILE_LINE__);
static_assert(!srlz::detail::is_fixed_size_serialized_v<session_description_t>, __FILE_LINE__);
static_assert(!srlz::detail::is_fixed_size_serialized_v<identity_descriptor_t>, __FILE_LINE__);

static_assert(!srlz::detail::has_serializable_properties_as_fixed_v<session_description_t>, __FILE_LINE__);
static_assert(!srlz::detail::has_serializable_properties_as_fixed_v<address_descriptor_t>, __FILE_LINE__);
static_assert(!srlz::detail::has_serializable_properties_as_fixed_v<identity_descriptor_t>, __FILE_LINE__);

static_assert(srlz::detail::type_prefix_serialize_cond_v<sx_loc_64>, __FILE_LINE__);
static_assert(srlz::detail::type_prefix_serialize_cond_v<sx_glob_64>, __FILE_LINE__);
static_assert(srlz::detail::type_prefix_serialize_cond_v<sx_locid_t>, __FILE_LINE__);
static_assert(srlz::detail::type_prefix_serialize_cond_v<sx_uuid_t>, __FILE_LINE__);

static_assert(!srlz::detail::type_prefix_serialize_cond_v<sx_loc_64, without_prefix>, __FILE_LINE__);
static_assert(!srlz::detail::type_prefix_serialize_cond_v<sx_glob_64, without_prefix>, __FILE_LINE__);
static_assert(!srlz::detail::type_prefix_serialize_cond_v<sx_locid_t, without_prefix>, __FILE_LINE__);
static_assert(!srlz::detail::type_prefix_serialize_cond_v<sx_uuid_t, without_prefix>, __FILE_LINE__);


static_assert(srlz::detail::is_fixed_size_serialized <sx_glob_64, none_options>::value == 3, __FILE_LINE__);
static_assert(srlz::detail::is_fixed_size_serialized <sx_glob_64, without_prefix>::value == 4, __FILE_LINE__);

static_assert(srlz::detail::is_fixed_size_serialized<sx_glob_64, none_options>::size == (
	srlz::detail::is_fixed_size_serialized<sx_glob_64, none_options>::prefix_serialized_size +
	srlz::detail::is_fixed_size_serialized<sx_glob_64, without_prefix>::size
	), __FILE_LINE__);
static_assert(srlz::detail::is_fixed_size_serialized<sx_glob_64, without_prefix>::size == (
	srlz::write_size_v<uint64_t>
	), __FILE_LINE__);

static_assert(srlz::detail::is_fixed_size_serialized<sx_glob_64, crm::none_options>::value_serialized_size == (
	srlz::detail::is_fixed_size_serialized<sx_glob_64, without_prefix>::value_serialized_size
	), __FILE_LINE__);
static_assert(srlz::detail::is_fixed_size_serialized<sx_glob_64, without_prefix>::value_serialized_size == (
	sizeof(uint64_t)
	), __FILE_LINE__);


static_assert(srlz::write_size_v<sx_glob_64, crm::none_options> == (
	srlz::detail::is_fixed_size_serialized<sx_glob_64, none_options>::prefix_serialized_size +
	srlz::write_size_v<sx_glob_64, crm::without_prefix>
	), __FILE_LINE__);
static_assert(srlz::write_size_v<sx_glob_64, without_prefix> == (
	srlz::write_size_v<uint64_t>
	), __FILE_LINE__);
}

