#include "../../appkit.h"


using namespace crm;


static_assert(srlz::detail::is_fixed_size_serialized_v<std::array<long, 2>>, __FILE_LINE__);
static_assert(!srlz::detail::is_fixed_size_serialized_v<std::array<std::string, 2>>, __FILE_LINE__);

static_assert(std::is_same_v<
	srlz::detail::fixed_serialized_length_type_t<std::array<int, 10>>,
	uint8_t>, __FILE_LINE__);

static_assert(std::is_same_v<
	srlz::detail::fixed_serialized_length_type_t<std::array<int, 1000>>,
	uint16_t>, __FILE_LINE__);

static_assert(std::is_same_v<
	srlz::detail::fixed_serialized_length_type_t<std::array<int, 10000>>,
	uint32_t>, __FILE_LINE__);


struct pod_sizeof_Kb {
	std::array<char, CRM_KB> buf;
	};
static_assert(srlz::detail::is_strob_serializable_v<pod_sizeof_Kb>, __FILE_LINE__);
static_assert(srlz::detail::is_fixed_size_serialized_v<decltype(pod_sizeof_Kb::buf)>, __FILE_LINE__);
static_assert(srlz::detail::strob_fields_trait_as_fixed_serialized<pod_sizeof_Kb, crm::none_options>::detect(), __FILE_LINE__);
static_assert(srlz::detail::strob_fields_trait_as_fixed_serialized_v<pod_sizeof_Kb, crm::none_options>, __FILE_LINE__);
static_assert(srlz::detail::is_strob_fixed_serialized_size_v<pod_sizeof_Kb>, __FILE_LINE__);
static_assert(srlz::detail::is_fixed_size_serialized_v<pod_sizeof_Kb>, __FILE_LINE__);
static_assert(srlz::detail::is_fixed_size_array_v<std::array<pod_sizeof_Kb, 10>>, __FILE_LINE__);
static_assert(srlz::detail::is_fixed_size_serialized_v<srlz::detail::fixed_size_array_value_type_t<std::array<pod_sizeof_Kb, 10>>>, __FILE_LINE__);
static_assert(srlz::detail::is_fixed_size_serialized_array_v< std::array<pod_sizeof_Kb, 10>>, __FILE_LINE__);

static_assert(std::is_same_v<
	srlz::detail::fixed_serialized_length_type_t<std::array<pod_sizeof_Kb, 10>>,
	uint16_t>, __FILE_LINE__);

static_assert(std::is_same_v<srlz::detail::fixed_serialized_length_type_t<long>, uint8_t>, __FILE_LINE__);
static_assert(std::is_same_v<srlz::detail::fixed_serialized_length_type_t<pod_sizeof_Kb>, uint16_t>, __FILE_LINE__);
static_assert(srlz::detail::fixed_serialized_size_v<decltype(pod_sizeof_Kb::buf)> < std::numeric_limits<uint16_t>::max(), __FILE_LINE__);
static_assert(srlz::detail::fixed_serialized_size_v<pod_sizeof_Kb> < std::numeric_limits<uint16_t>::max(), __FILE_LINE__);
static_assert(srlz::detail::fixed_serialized_size_v<std::array<char, 200>> < std::numeric_limits<uint8_t>::max(), __FILE_LINE__);
static_assert(srlz::detail::fixed_serialized_size_v<std::array<pod_sizeof_Kb, 20>> < std::numeric_limits<uint16_t>::max(), __FILE_LINE__);


struct pod_sizeof_0 {
	long l;
	int i;
	double d;
	char c;
	};
static_assert(srlz::detail::fixed_serialized_size_v<pod_sizeof_0> < std::numeric_limits<uint8_t>::max(), __FILE_LINE__);
static_assert(std::is_same_v<
	srlz::detail::fixed_serialized_length_type_t<pod_sizeof_0>,
	uint8_t>, __FILE_LINE__);



union u2 {
	std::size_t s;
	double d;
	std::uint8_t u8[sizeof(std::size_t)];
	};

/*! struct with std::array member */
template<typename T, const int N = 8>
struct s_carray_0 {
	int size()const noexcept { return N; }
	std::array<T, N> ca;
	};

struct my_struct_fixed_size_00 {

	// 8
	double s;
	// 4
	int i;
	// 8
	size_t sss;
	// 1
	char c;
	// 1
	char b;
	};

struct my_struct_fixed_size_01 {

	// 8
	u2 u;
	// 4
	int i;
	};

struct my_struct_fixed_size_10 {
	my_struct_fixed_size_00 m00;
	my_struct_fixed_size_01 m01;

	u2 u;
	size_t s;
	};

struct my_struct_fixed_size_11 {
	my_struct_fixed_size_10 m10;
	std::array<my_struct_fixed_size_00, 2> v00;
	};

static_assert(srlz::detail::is_fixed_size_serialized_v< my_struct_fixed_size_11>, __FILE_LINE__);
static_assert(srlz::detail::is_fixed_size_serialized_v<std::array<my_struct_fixed_size_11, 1>>, __FILE_LINE__);
static_assert(std::is_same_v<srlz::detail::fixed_size_array_value_type_t<std::array<my_struct_fixed_size_11, 1>>, my_struct_fixed_size_11>, __FILE_LINE__);
static_assert(srlz::detail::is_strob_fixed_serialized_size_v<srlz::detail::fixed_size_array_value_type_t<std::array<my_struct_fixed_size_11, 1>>>, __FILE_LINE__);
static_assert(srlz::write_size_v<std::array<my_struct_fixed_size_11, 1>> < std::numeric_limits<short>::max(), __FILE_LINE__);

static_assert(srlz::detail::fixed_serialized_size_v< std::array<my_struct_fixed_size_11, 1>> < std::numeric_limits<uint8_t>::max(), __FILE_LINE__);
static_assert(sizeof(srlz::detail::fixed_serialized_length_type_t<std::array<my_struct_fixed_size_11, 1>>) <= 2, __FILE_LINE__);
static_assert(srlz::detail::fixe_size_array_count_v<std::array<my_struct_fixed_size_11, 1>> == 1, __FILE_LINE__);
static_assert(srlz::detail::is_strob_fixed_serialized_size_v<my_struct_fixed_size_11>, __FILE_LINE__);
static_assert(srlz::detail::is_strob_flat_serializable_v< my_struct_fixed_size_11>, __FILE_LINE__);
static_assert(srlz::detail::is_container_of_strob_v<std::array<my_struct_fixed_size_11, 1>>, __FILE_LINE__);
static_assert(srlz::detail::is_input_iterator_v<const my_struct_fixed_size_11*>, __FILE_LINE__);
static_assert(srlz::detail::is_iterator_strob_v<const my_struct_fixed_size_11*>, __FILE_LINE__);
static_assert(srlz::detail::is_input_iterator_strob_v<const my_struct_fixed_size_11*>, __FILE_LINE__);
