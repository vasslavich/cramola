#include "../../appkit.h"


using namespace crm::srlz;


namespace {


static_assert(detail::is_fixed_size_serialized_v<uint8_t>, __FILE_LINE__);
static_assert(detail::is_fixed_size_serialized_v<uint16_t>, __FILE_LINE__);
static_assert(detail::is_fixed_size_serialized_v<uint32_t>, __FILE_LINE__);
static_assert(detail::is_fixed_size_serialized_v<uint64_t>, __FILE_LINE__);
static_assert(detail::is_fixed_size_serialized_v<float>, __FILE_LINE__);
static_assert(detail::is_fixed_size_serialized_v<double>, __FILE_LINE__);

static_assert(!detail::type_prefix_serialize_cond_v<double>, __FILE_LINE__);
static_assert(!detail::type_prefix_serialize_cond_v<float>, __FILE_LINE__);
static_assert(!detail::type_prefix_serialize_cond_v<int>, __FILE_LINE__);

static_assert(detail::is_mono_serializable_v<double>, __FILE_LINE__);
static_assert(detail::is_mono_serializable_v<float>, __FILE_LINE__);
static_assert(detail::is_mono_serializable_v<int>, __FILE_LINE__);

static_assert(detail::is_fixed_size_serialized<double, crm::none_options>::size == (
	sizeof(double)
	), __FILE_LINE__);

static_assert(detail::is_type_serialized_options_as<double, serialize_properties_length::as_fixedsize>(), __FILE_LINE__);
static_assert(detail::is_type_serialized_options_as<float, serialize_properties_length::as_fixedsize>(), __FILE_LINE__);
static_assert(detail::is_type_serialized_options_as<int, serialize_properties_length::as_fixedsize>(), __FILE_LINE__);

static_assert(detail::has_serializable_properties_v<double>, __FILE_LINE__);
static_assert(detail::has_serializable_properties_v<float>, __FILE_LINE__);
static_assert(detail::has_serializable_properties_v<int>, __FILE_LINE__);

static_assert(!detail::type_prefix_serialize_cond_v<double>, __FILE_LINE__);
static_assert(!detail::type_prefix_serialize_cond_v<float>, __FILE_LINE__);
static_assert(!detail::type_prefix_serialize_cond_v<int>, __FILE_LINE__);

static_assert(!detail::type_prefix_serialize_cond_v<double, crm::without_prefix>, __FILE_LINE__);
static_assert(!detail::type_prefix_serialize_cond_v<float, crm::without_prefix>, __FILE_LINE__);
static_assert(!detail::type_prefix_serialize_cond_v<int, crm::without_prefix>, __FILE_LINE__);

static_assert(detail::is_fixed_size_serialized <uint64_t, crm::none_options>::value == 4, __FILE_LINE__);
static_assert(detail::is_fixed_size_serialized <uint64_t, crm::without_prefix>::value == 4, __FILE_LINE__);

static_assert(detail::is_fixed_size_serialized_t<uint64_t, crm::none_options>::size ==
	detail::is_fixed_size_serialized<uint64_t, crm::without_prefix>::size, __FILE_LINE__);
static_assert(detail::is_fixed_size_serialized<uint64_t, crm::without_prefix>::size == (
	sizeof(uint64_t)
	), __FILE_LINE__);

static_assert(detail::is_fixed_size_serialized<uint64_t, crm::none_options>::value_serialized_size == 
	detail::is_fixed_size_serialized<uint64_t, crm::without_prefix>::value_serialized_size, __FILE_LINE__);
static_assert(detail::is_fixed_size_serialized<uint64_t, crm::without_prefix>::size == (
	sizeof(uint64_t)
	), __FILE_LINE__);

static_assert(write_size_v<uint64_t> == write_size_v<uint64_t, crm::without_prefix>, __FILE_LINE__);
static_assert(write_size_v<uint64_t, crm::without_prefix> == (
	sizeof(uint64_t)
	), __FILE_LINE__);

static_assert(write_size_v<double> == (
	sizeof(double)
	), __FILE_LINE__);

static_assert(write_size_v<float> == (
	sizeof(float)
	), __FILE_LINE__);

static_assert(write_size_v<int> == (
	sizeof(int)
	), __FILE_LINE__);



static_assert(write_size_v<double, crm::without_prefix> == (
	sizeof(double)
	), __FILE_LINE__);

static_assert(write_size_v<float, crm::without_prefix> == (
	sizeof(float)
	), __FILE_LINE__);

static_assert(write_size_v<int, crm::without_prefix> == (
	sizeof(int)
	), __FILE_LINE__);



static_assert(detail::is_byte<char>::value, "");
static_assert(detail::is_byte<unsigned char>::value, "");
static_assert(detail::is_byte<std::uint8_t>::value, "");
static_assert(detail::is_byte<std::int8_t>::value, "");
static_assert(detail::is_byte<std::byte>::value, "");
static_assert(!detail::is_byte<wchar_t>::value, "");
static_assert(!detail::is_byte<size_t>::value, "");
}
