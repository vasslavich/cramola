#include "../../internal.h"
#include "../streamcat/strob/strob_object2value.h"
#include "../../utilities/complex_traits/tuple_trait.h"


using namespace crm;
using namespace crm::detail;

namespace {
struct move_only_1 final {
	std::string a;
	std::vector<char> c;

	move_only_1();
	move_only_1(move_only_1&&) = default;
	move_only_1& operator=(move_only_1&&) = default;

	move_only_1(const move_only_1&) = delete;
	move_only_1& operator=(const move_only_1&) = delete;

	template<typename S>
	void srlz(S&& s);

	template<typename S>
	void dsrlz(S&& s);

	size_t get_serialized_size()const noexcept;
	};

struct copy_only_1 final {
	std::string a;
	std::vector<char> c;

	copy_only_1();
	copy_only_1(copy_only_1&&) = delete;
	copy_only_1& operator=(copy_only_1&&) = delete;

	copy_only_1(const copy_only_1&) = default;
	copy_only_1& operator=(const copy_only_1&) = default;

	template<typename S>
	void srlz(S&& s);

	template<typename S>
	void dsrlz(S&& s);

	size_t get_serialized_size()const noexcept;
	};

using trait_envelop_3 = decltype(make_tuple_envelop(
	std::declval<double>(),
	std::declval<std::string>(),
	std::declval<move_only_1>()
));
}

static_assert(srlz::is_serializable_v<move_only_1>, __FILE_LINE__);
static_assert(trait_envelop_3::elements_count() == 3, __FILE_LINE__);

static_assert(std::is_same_v<double,
	std::decay_t<decltype(std::declval<trait_envelop_3>().value<0>())>>, __FILE_LINE__);

static_assert(std::is_same_v<std::string,
	std::decay_t<decltype(std::declval<trait_envelop_3>().value<1>())>>, __FILE_LINE__);

static_assert(std::is_same_v<move_only_1,
	std::decay_t<decltype(std::declval<trait_envelop_3>().value<2>())>>, __FILE_LINE__);

static_assert(std::is_same_v<std::tuple<double, std::string, move_only_1>,
	std::decay_t<decltype(std::declval<trait_envelop_3&&>().values_tuple())>>, __FILE_LINE__);

namespace {
using type_0 = std::decay_t<decltype(srlz::detail::tie_as_tuple_element<0>(
	std::declval<trait_envelop_3&&>().values_tuple()))>;
using type_1 = std::decay_t<decltype(srlz::detail::tie_as_tuple_element<1>(
	std::declval<trait_envelop_3&&>().values_tuple()))>;
using type_2 = std::decay_t<decltype(srlz::detail::tie_as_tuple_element<2>(
	std::declval<trait_envelop_3&&>().values_tuple()))>;
}


static_assert(std::is_same_v<double, type_0>, __FILE_LINE__);
static_assert(std::is_same_v<std::string, type_1>, __FILE_LINE__);
static_assert(std::is_same_v<move_only_1, type_2>, __FILE_LINE__);


namespace{
using message_envelop_type_1 = std::decay_t<decltype(make_imessage_envelop(
	std::declval<double>(),
	std::declval<std::string>(),
	std::declval<move_only_1>(),
	std::declval<const copy_only_1&>()))>;

using type_m_0 = std::decay_t<decltype(std::declval<message_envelop_type_1>().value<0>())>;
using type_m_1 = std::decay_t<decltype(std::declval<message_envelop_type_1>().value<1>())>;
using type_m_2 = std::decay_t<decltype(std::declval<message_envelop_type_1&&>().value<2>())>;
using type_m_3 = std::decay_t<decltype(std::declval<const message_envelop_type_1&>().value<3>())>;
}

static_assert(std::is_same_v<double, type_m_0>, __FILE_LINE__);
static_assert(std::is_same_v<std::string, type_m_1>, __FILE_LINE__);
static_assert(std::is_same_v<move_only_1, type_m_2>, __FILE_LINE__);
static_assert(std::is_same_v<copy_only_1, type_m_3>, __FILE_LINE__);


