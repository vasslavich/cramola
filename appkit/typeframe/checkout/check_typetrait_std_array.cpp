#include "../../appkit.h"

using namespace crm;


std::array<long,2> buf3;
static_assert(std::is_same_v<srlz::detail::fixed_size_array_type_t<decltype(buf3)>, srlz::detail::fixed_array_std>, __FILE_LINE__);
static_assert(srlz::detail::fixed_size_array_v<decltype(buf3)> == srlz::fixed_array_type::fixed_array_std, __FILE_LINE__);
static_assert(srlz::detail::is_fixed_size_array_v<decltype(buf3)>, __FILE_LINE__);
static_assert(std::is_same_v<srlz::detail::fixed_size_array_value_type_t<decltype(buf3)>, long>, __FILE_LINE__);
static_assert(srlz::detail::is_fixed_size_serialized_array_v<decltype(buf3)>, __FILE_LINE__);
static_assert(srlz::detail::fixe_size_array_count_v<decltype(buf3)> == 2, __FILE_LINE__);

std::array<std::string, 2> bufs;
static_assert(std::is_same_v<srlz::detail::fixed_size_array_type_t<decltype(bufs)>, srlz::detail::fixed_array_std>, __FILE_LINE__);
static_assert(srlz::detail::fixed_size_array_v<decltype(bufs)> == srlz::fixed_array_type::fixed_array_std, __FILE_LINE__);
static_assert(srlz::detail::is_fixed_size_array_v<decltype(bufs)>, __FILE_LINE__);
static_assert(std::is_same_v<srlz::detail::fixed_size_array_value_type_t<decltype(bufs)>, std::string>, __FILE_LINE__);
static_assert(!srlz::detail::is_fixed_size_serialized_array_v<decltype(bufs)>, __FILE_LINE__);
static_assert(srlz::detail::fixe_size_array_count_v<decltype(bufs)> == 2, __FILE_LINE__);

std::array<int, 20> ibuf10;
static_assert(std::is_same_v<srlz::detail::fixed_array_trait_t<decltype(ibuf10)>::length_type, uint8_t>, __FILE_LINE__);
static_assert(srlz::write_size_v<decltype(ibuf10)> == (
	sizeof(srlz::detail::fixed_array_trait_t<decltype(ibuf10)>::length_type) +
	sizeof(int) * 20
	), __FILE_LINE__);

