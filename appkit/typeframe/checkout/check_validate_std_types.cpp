#include <chrono>
#include "../../appkit.h"


using namespace crm;


static_assert(std::is_integral_v<srlz::std_types_trait_t::streamed_time_point_t>, __FILE_LINE__);
static_assert(srlz::detail::is_mono_serializable_v<srlz::std_types_trait_t::streamed_time_point_t>, __FILE_LINE__);
static_assert(srlz::detail::is_fixed_size_serialized_v<srlz::std_types_trait_t::streamed_time_point_t>, __FILE_LINE__);
static_assert(sizeof(srlz::write_size<srlz::std_types_trait_t::streamed_time_point_t, none_options>::size) >= 8, __FILE_LINE__);
static_assert(srlz::detail::is_chrono_duration_v<std::chrono::seconds>, __FILE_LINE__);
static_assert(srlz::detail::is_chrono_duration_v<decltype(std::declval<detail::i_rt0_command_t>().wait_timeout())>, __FILE_LINE__);
static_assert(std::is_same_v<bool, decltype(is_null(std::declval<detail::i_rt0_command_t>()))>, __FILE_LINE__);
static_assert(is_null_trait_v<decltype(std::declval<detail::i_rt0_command_t>().wait_timeout())>, __FILE_LINE__);


void dmpdb_x103003() {
	auto d = std::chrono::seconds(1);
	auto isNull = is_null(d);
	static_assert(std::is_same_v<bool, decltype(isNull)>, __FILE_LINE__);

	detail::i_rt0_cmd_addlink_t cmd;
	auto isNullw = is_null(cmd.wait_timeout());
	static_assert(std::is_same_v<bool, decltype(isNullw)>, __FILE_LINE__);
	}


