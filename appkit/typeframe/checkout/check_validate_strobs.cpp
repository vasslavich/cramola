#include "../../appkit.h"


using namespace crm;


static_assert(!srlz::detail::is_strob_serializable_v<std::array<int, 10>>, __FILE_LINE__);
static_assert(!srlz::detail::is_strob_fixed_serialized_size_v<std::array<int, 10>>, __FILE_LINE__);

struct cvu_0 {
	std::array<int, 10> ia;
	double d;
	};
struct cvu_1 {
	std::array<cvu_0, 10> cvu_0_a;
	};

struct S2_1 final {
	long m1;
	double m2;
	std::array<cvu_1, 2> cva;
	};

static_assert(crm::srlz::detail::is_aggregate_initializable_v<S2_1>, __FILE_LINE__);
static_assert(crm::srlz::detail::is_strob_flat_serializable_v<S2_1>, __FILE_LINE__);
static_assert(crm::srlz::detail::__fields_count_hepler_t<S2_1>::count == 3, __FILE_LINE__);
static_assert(crm::srlz::detail::__fields_count_t<S2_1>::value, __FILE_LINE__);
static_assert(crm::srlz::detail::__fields_count_t<S2_1>::count == 3, __FILE_LINE__);
static_assert(crm::srlz::detail::fields_count_v<S2_1> == 3, __FILE_LINE__);
static_assert(crm::srlz::detail::is_strob_serializable_v<S2_1>, __FILE_LINE__);

static_assert(srlz::detail::is_strob_fixed_serialized_size_v<cvu_0>, __FILE_LINE__);
static_assert(srlz::detail::is_fixed_size_serialized_v<cvu_0>, __FILE_LINE__);

static_assert(srlz::detail::is_strob_fixed_serialized_size_v<cvu_1>, __FILE_LINE__);
static_assert(srlz::detail::is_fixed_size_serialized_v<cvu_1>, __FILE_LINE__);
static_assert(srlz::detail::is_fixed_size_serialized_v<std::array<cvu_1, 10>>, __FILE_LINE__);

static_assert(std::is_same_v < srlz::detail::fixed_array_trait_t<decltype(cvu_0::ia)>::value_type, int>, __FILE_LINE__);
static_assert(srlz::detail::fixed_array_trait_t<decltype(cvu_0::ia)>::range_size == 10, __FILE_LINE__);

constexpr size_t max_container_size = 10;

static_assert(
	srlz::write_size_v<std::array<cvu_1, 10>> ==
	(
		sizeof(typename srlz::detail::fixed_array_trait_t<std::array<cvu_1, 10>>::length_type) +
		10 * srlz::write_size_v<cvu_1>
		),
	__FILE_LINE__);


union u2 {
	std::size_t s;
	double d;
	std::uint8_t u8[sizeof(std::size_t)];
	};


struct st0 {
	uint32_t i;
	double d;
	};


struct st1 {
	std::string s;
	std::vector<uint16_t> u2;
	};

struct st2 {
	st0 s0;
	st1 s1;
	};


struct st3 {
	std::vector<st2> st2array;
	st2 s2;
	st1 s1;
	st0 s0;
	};


struct st4 {
	st3 a;
	u2 u;
	};


struct st5 {
	std::list<st3> x3l;
	std::vector<st4> x4v;
	};


template<typename T>
using list_t = typename std::list<T>;

template<typename T>
using vector_t = typename std::vector<T>;

template<typename T>
using string_t = typename std::basic_string<T>;

struct my_struct_fixed_size_00 {

	// 8
	double s;
	// 4
	int i;
	// 8
	size_t sss;
	// 1
	char c;
	// 1
	char b;
	};


struct my_struct_fixed_size_01 {

	// 8
	u2 u;
	// 4
	int i;
	};

struct my_struct_fixed_size_10 {
	my_struct_fixed_size_00 m00;
	my_struct_fixed_size_01 m01;

	u2 u;
	size_t s;
	};

struct my_struct_fixed_size_11 {
	my_struct_fixed_size_10 m10;
	std::array<my_struct_fixed_size_00, 2> v00;

	std::chrono::system_clock::time_point t = std::chrono::system_clock::now();
	std::chrono::system_clock::time_point::duration d = t.time_since_epoch();
	};

struct my_struct_fixed_size_mixed1 {

	crm::sx_uuid_t uuid;
	int i;
	};







static_assert(std::is_fundamental_v<size_t>, __FILE_LINE__);

static_assert(!crm::srlz::detail::is_strob_serializable_v<crm::sx_uuid_t>, __FILE_LINE__);
static_assert(!crm::srlz::detail::is_container_v<crm::sx_uuid_t>, __FILE_LINE__);

static_assert(crm::srlz::detail::is_srlzd_prefixed_v<std::string>, __FILE_LINE__);
static_assert(crm::srlz::detail::is_container_v<std::string>, __FILE_LINE__);

static_assert(crm::srlz::detail::is_strob_serializable_v<st0>, __FILE_LINE__);
static_assert(crm::srlz::detail::is_strob_serializable_v<st1>, __FILE_LINE__);
static_assert(crm::srlz::detail::is_strob_serializable_v<st2>, __FILE_LINE__);
static_assert(crm::srlz::detail::is_strob_serializable_v<st3>, __FILE_LINE__);
static_assert(!crm::srlz::detail::is_strob_serializable_v<u2>, __FILE_LINE__);
static_assert(crm::srlz::detail::is_mono_serializable_v<u2>, __FILE_LINE__);
static_assert(crm::srlz::detail::is_strob_serializable_v<st4>, __FILE_LINE__);
static_assert(crm::srlz::detail::is_fixed_size_serialized_v<double>, __FILE__);
static_assert(crm::srlz::detail::is_fixed_size_serialized_v<int>, __FILE__);
static_assert(crm::srlz::detail::is_fixed_size_serialized_v<int&>, __FILE__);
static_assert(crm::srlz::detail::is_fixed_size_serialized_v<int*>, __FILE__);
static_assert(crm::srlz::detail::is_fixed_size_serialized_v<const size_t&>, __FILE__);
static_assert(crm::srlz::detail::is_fixed_size_serialized_v<const char>, __FILE__);

static_assert(!crm::srlz::detail::is_fixed_size_serialized_v<std::vector<int>>, __FILE__);
static_assert(!crm::srlz::detail::is_fixed_size_serialized_v<std::list<std::vector<int>>>, __FILE__);
static_assert(!crm::srlz::detail::is_fixed_size_serialized_v<std::list<char>>, __FILE__);
static_assert(!crm::srlz::detail::is_fixed_size_serialized_v<std::vector<std::vector<int>>>, __FILE__);
static_assert(!crm::srlz::detail::is_fixed_size_serialized_v<std::vector<char>&>, __FILE__);
static_assert(!crm::srlz::detail::is_fixed_size_serialized_v<const std::vector<char>&>, __FILE__);

static_assert(std::is_array_v<decltype(u2::u8)>, __FILE__);

static_assert(crm::srlz::detail::is_strob_fixed_serialized_size_v<my_struct_fixed_size_00>
	&& crm::srlz::detail::is_fixed_size_serialized_v<my_struct_fixed_size_00>, __FILE__);
auto sssl = crm::srlz::write_size_v<my_struct_fixed_size_00>;

static_assert(crm::srlz::detail::is_strob_fixed_serialized_size_v<my_struct_fixed_size_01>
	&& crm::srlz::detail::is_fixed_size_serialized_v<my_struct_fixed_size_01>, __FILE__);
static constexpr auto sssl1 = crm::srlz::write_size_v<my_struct_fixed_size_01>;

static_assert(crm::srlz::detail::is_strob_fixed_serialized_size_v<my_struct_fixed_size_10>
	&& crm::srlz::detail::is_fixed_size_serialized_v<my_struct_fixed_size_10>, __FILE__);
static constexpr auto  sssl2 = crm::srlz::write_size_v<my_struct_fixed_size_10>;

static_assert(crm::srlz::detail::is_strob_serializable_v<my_struct_fixed_size_mixed1>, __FILE__);
static_assert(crm::srlz::detail::is_strob_fixed_serialized_size_v<my_struct_fixed_size_mixed1>, __FILE__);

static_assert(crm::srlz::detail::is_strob_explicit_serialized_v<std::chrono::system_clock::time_point>, __FILE__);
static_assert(crm::srlz::detail::is_strob_explicit_serialized_v<std::chrono::high_resolution_clock::time_point>, __FILE__);
static_assert(crm::srlz::detail::is_strob_explicit_serialized_v<std::chrono::steady_clock::time_point>, __FILE__);
static_assert(crm::srlz::detail::is_strob_explicit_serialized_v<std::chrono::nanoseconds>, __FILE__);
static_assert(crm::srlz::detail::is_strob_explicit_serialized_v<std::chrono::microseconds>, __FILE__);
static_assert(crm::srlz::detail::is_strob_explicit_serialized_v<std::chrono::milliseconds>, __FILE__);
static_assert(crm::srlz::detail::is_strob_explicit_serialized_v<std::chrono::seconds>, __FILE__);
static_assert(crm::srlz::detail::is_strob_explicit_serialized_v<std::chrono::minutes>, __FILE__);
static_assert(crm::srlz::detail::is_strob_explicit_serialized_v<std::chrono::hours>, __FILE__);

static_assert(!crm::srlz::detail::is_strob_explicit_serialized_v<my_struct_fixed_size_00>, __FILE__);
static_assert(!crm::srlz::detail::is_strob_explicit_serialized_v<my_struct_fixed_size_01>, __FILE__);
static_assert(!crm::srlz::detail::is_strob_explicit_serialized_v<my_struct_fixed_size_10>, __FILE__);
static_assert(!crm::srlz::detail::is_strob_explicit_serialized_v<my_struct_fixed_size_mixed1>, __FILE__);

//static_assert(crm::srlz::detail::is_strob_prefixed_serialized_size_v<u2>, __FILE__);

