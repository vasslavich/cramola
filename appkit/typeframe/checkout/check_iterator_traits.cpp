#include "../../appkit.h"

using namespace crm;


namespace {

const uint8_t* pu8{};

static_assert(srlz::detail::is_iterator_byte_v<decltype(pu8)>, __FILE_LINE__);
static_assert(srlz::detail::is_contiguous_iterator_v<decltype(pu8)>, __FILE_LINE__);

static_assert(srlz::detail::is_iterator_byte_v<std::istream_iterator<uint8_t>>, __FILE_LINE__);
static_assert(!srlz::detail::is_contiguous_iterator_v<std::istream_iterator<uint8_t>>, __FILE_LINE__);

static_assert(srlz::detail::is_contiguous_iterator_v<decltype(std::cbegin(std::declval<std::vector<char>>()))>, __FILE_LINE__);
static_assert(srlz::detail::stream_base_traits::iterator_cathegory<decltype(std::cbegin(std::declval<std::vector<char>>()))>() ==
	srlz::detail::iterator_tag::random_access_iterator_tag, __FILE_LINE__);

static_assert(!srlz::detail::is_contiguous_iterator_v<decltype(std::cbegin(std::declval<std::list<char>>()))>, __FILE_LINE__);
static_assert(srlz::detail::stream_base_traits::iterator_cathegory<decltype(std::cbegin(std::declval<std::list<char>>()))>() ==
	srlz::detail::iterator_tag::bidirectional_iterator_tag, __FILE_LINE__);

static_assert(srlz::detail::is_contiguous_iterator_v<decltype(std::cbegin(std::declval<std::array<char, 1>>()))>, __FILE_LINE__);
static_assert(srlz::detail::stream_base_traits::iterator_cathegory<decltype(std::cbegin(std::declval<std::array<char, 1>>()))>() ==
	srlz::detail::iterator_tag::random_access_iterator_tag, __FILE_LINE__);

static_assert(srlz::detail::is_contiguous_iterator_v<decltype(std::cbegin(std::declval<std::string>()))>, __FILE_LINE__);
static_assert(srlz::detail::stream_base_traits::iterator_cathegory<decltype(std::cbegin(std::declval<std::string>()))>() ==
	srlz::detail::iterator_tag::random_access_iterator_tag, __FILE_LINE__);

static_assert(srlz::detail::is_contiguous_iterator_v<decltype(std::cbegin(std::declval<std::string_view>()))>, __FILE_LINE__);
static_assert(srlz::detail::stream_base_traits::iterator_cathegory<decltype(std::cbegin(std::declval<std::string_view>()))>() ==
	srlz::detail::iterator_tag::random_access_iterator_tag, __FILE_LINE__);




static_assert(srlz::detail::is_container_random_access_v<srlz::detail::binary_iterator_adapter_t<uint64_t>>, __FILE_LINE__);
static_assert(srlz::detail::is_container_contiguous_of_bytes_v<srlz::detail::binary_iterator_adapter_t<uint64_t>>, __FILE_LINE__);
static_assert(srlz::detail::is_contiguous_iterator_v<decltype(std::declval< srlz::detail::binary_iterator_adapter_t<uint64_t>>().cbegin())>, __FILE_LINE__);
static_assert(srlz::detail::stream_base_traits::iterator_cathegory<decltype(std::declval<srlz::detail::binary_iterator_adapter_t<uint64_t>>().cbegin())>() ==
	srlz::detail::iterator_tag::random_access_iterator_tag, __FILE_LINE__);

static_assert(srlz::detail::is_container_random_access_v<srlz::detail::const_binary_iterator_adapter_t<uint64_t>>, __FILE_LINE__);
static_assert(srlz::detail::is_container_contiguous_of_bytes_v<srlz::detail::const_binary_iterator_adapter_t<uint64_t>>, __FILE_LINE__);
static_assert(srlz::detail::is_contiguous_iterator_v<decltype(std::declval< srlz::detail::const_binary_iterator_adapter_t<uint64_t>>().cbegin())>, __FILE_LINE__);
static_assert(srlz::detail::stream_base_traits::iterator_cathegory<decltype(std::declval< srlz::detail::const_binary_iterator_adapter_t<uint64_t>>().cbegin())>() ==
	srlz::detail::iterator_tag::random_access_iterator_tag, __FILE_LINE__);

static_assert(srlz::detail::is_container_random_access_v<srlz::detail::const_binary_iterator_pointer_t<uint64_t>>, __FILE_LINE__);
static_assert(srlz::detail::is_container_contiguous_of_bytes_v < srlz::detail::const_binary_iterator_pointer_t<uint64_t>>, __FILE_LINE__);
static_assert(srlz::detail::is_contiguous_iterator_v<decltype(std::declval< srlz::detail::const_binary_iterator_pointer_t<uint64_t>>().cbegin())>, __FILE_LINE__);
static_assert(srlz::detail::stream_base_traits::iterator_cathegory<decltype(std::declval< srlz::detail::const_binary_iterator_pointer_t<uint64_t>>().cbegin())>() ==
	srlz::detail::iterator_tag::random_access_iterator_tag, __FILE_LINE__);
}
