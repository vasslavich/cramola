#include "../../appkit.h"

using namespace crm;
using namespace crm::srlz;


static char cbuf[1024];
static char* pbuf;
static_assert(crm::srlz::detail::is_range_v<decltype(cbuf)>, __FILE__);
static_assert(crm::srlz::detail::is_range_v<decltype(pbuf)>, __FILE__);

long buf3[2];
static_assert(std::is_same_v<srlz::detail::fixed_size_array_type_t<decltype(buf3)>, srlz::detail::fixed_array_c>, __FILE_LINE__);
static_assert(srlz::detail::fixed_size_array_v<decltype(buf3)> == srlz::fixed_array_type::fixed_array_c, __FILE_LINE__);
static_assert(srlz::detail::is_fixed_size_array_v<decltype(buf3)>, __FILE_LINE__);
static_assert(std::is_same_v<srlz::detail::fixed_size_array_value_type_t<decltype(buf3)>, long>, __FILE_LINE__);
static_assert(srlz::detail::is_fixed_size_serialized_array_v<decltype(buf3)>, __FILE_LINE__);
static_assert(srlz::detail::fixe_size_array_count_v<decltype(buf3)> == 2, __FILE_LINE__);

std::string bufs[2];
static_assert(std::is_same_v<srlz::detail::fixed_size_array_type_t<decltype(bufs)>, srlz::detail::fixed_array_c>, __FILE_LINE__);
static_assert(srlz::detail::fixed_size_array_v<decltype(bufs)> == srlz::fixed_array_type::fixed_array_c, __FILE_LINE__);
static_assert(srlz::detail::is_fixed_size_array_v<decltype(bufs)>, __FILE_LINE__);
static_assert(std::is_same_v<srlz::detail::fixed_size_array_value_type_t<decltype(bufs)>, std::string>, __FILE_LINE__);
static_assert(!srlz::detail::is_fixed_size_serialized_array_v<decltype(bufs)>, __FILE_LINE__);
static_assert(srlz::detail::fixe_size_array_count_v<decltype(bufs)> == 2, __FILE_LINE__);

template<typename TValue, typename T1, typename T2, typename T3>
auto carray_passing(TValue a1[7], TValue a2[], // pointers by language rules
	TValue(&a3)[42], // reference to array of known	bound
	TValue(&x0)[], // reference to array of unknown	bound
	T1 x1, // passing by value decays
	T2& x2, T3&& x3) // passing by reference
	{
	size_t l = 0;

	static_assert(!srlz::detail::is_fixed_size_array_v<decltype(a1)>, __FILE_LINE__);
	//l = carray_trait_size(a1);

	static_assert(!srlz::detail::is_fixed_size_array_v<decltype(a2)>, __FILE_LINE__);
	/*l = carray_trait_size(a2);*/

	static_assert(srlz::detail::is_fixed_size_array_v<decltype(a3)>, __FILE_LINE__);
	l = object_trait_serialized_size(a3);

	static_assert(!srlz::detail::is_fixed_size_array_v<decltype(x0)>, __FILE_LINE__);
	//l = carray_trait_size(x0);

	static_assert(!srlz::detail::is_fixed_size_array_v<decltype(x1)>, __FILE_LINE__);
	//l = carray_trait_size(x1);

	static_assert(!srlz::detail::is_fixed_size_array_v<decltype(x2)>, __FILE_LINE__);
	//l = carray_trait_size(x2);

	static_assert(!srlz::detail::is_fixed_size_array_v<decltype(x3)>, __FILE_LINE__);
	//l = carray_trait_size(std::forward<T3>(x3));

	return 0;
	}

template<typename TValue>
auto test_0() {
	TValue a[42];
	extern TValue x[];
	return std::conditional_t<std::is_integral_v<decltype(carray_passing(a, a, a, x, x, x, x))>, int, void>{0};
	}

static_assert(std::is_integral_v<decltype(test_0<int>())>, __FILE_LINE__);

