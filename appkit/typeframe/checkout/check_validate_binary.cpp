#include "../../appkit.h"


using namespace crm;
using namespace crm::srlz;
using namespace crm::srlz::detail;

namespace {


template<typename T>
T get_rvalue()noexcept;

template<typename T>
T& get_lvalue()noexcept;


static_assert(crm::srlz::detail::is_byte<char>::value, "");
static_assert(crm::srlz::detail::is_byte<unsigned char>::value, "");
static_assert(crm::srlz::detail::is_byte<std::uint8_t>::value, "");
static_assert(crm::srlz::detail::is_byte<std::int8_t>::value, "");
static_assert(crm::srlz::detail::is_byte<std::byte>::value, "");

static_assert(!crm::srlz::detail::is_byte<wchar_t>::value, "");
static_assert(!crm::srlz::detail::is_byte<size_t>::value, "");

std::vector<uint8_t> v8;

static_assert(crm::srlz::detail::is_container_t<std::vector<std::uint8_t>>::value, "");
static_assert(crm::srlz::detail::is_container_t<decltype(v8)>::value, "");
static_assert(crm::srlz::detail::is_container_t<decltype(get_rvalue<std::vector<std::uint8_t>>())>::value, "");
static_assert(crm::srlz::detail::is_container_t<decltype(get_lvalue<std::vector<std::uint8_t>>())>::value, "");

static_assert(crm::srlz::detail::is_container_bytes_t<std::vector<std::uint8_t>>::value, "");
static_assert(crm::srlz::detail::is_container_bytes_t<decltype(v8)>::value, "");
static_assert(crm::srlz::detail::is_container_bytes_t<decltype(get_rvalue<std::vector<std::uint8_t>>())>::value, "");
static_assert(crm::srlz::detail::is_container_bytes_t<decltype(get_lvalue<std::vector<std::uint8_t>>())> ::value, "");

static_assert(crm::srlz::detail::is_iterator_byte_t<decltype(get_lvalue<std::vector<std::uint8_t>>().cbegin())>::value, "");
static_assert(crm::srlz::detail::is_iterator_byte_t<decltype(get_rvalue<std::vector<std::uint8_t>>().cbegin())>::value, "");
}
