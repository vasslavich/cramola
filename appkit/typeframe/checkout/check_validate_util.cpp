#include "../../appkit.h"


using namespace crm;
using namespace crm::detail;


using srlz_prefix_uint8_t = typename srlz::detail::serializable_properties_prefix_trait_nbytes<uint8_t>;

static_assert(srlz::is_serializable_v<std::array<int, 10>>, __FILE_LINE__);
static_assert(srlz::detail::is_fixed_size_array_v<std::array<int, 10>>, __FILE_LINE__);
static_assert(srlz::detail::is_fixed_size_serialized_v<std::array<int, 10>>, __FILE_LINE__);
static_assert(srlz::detail::fixed_array_trait_t<std::array<char, 10>>::range_size == 10, __FILE_LINE__);

static_assert(std::is_same_v<srlz::detail::fixed_serialized_length_type_t<std::array<char, 10>>, uint8_t>, __FILE_LINE__);
static_assert(srlz::detail::fixe_size_array_count_v<std::array<char, 10>> == 10, __FILE_LINE__);
static_assert(std::is_same_v<srlz::detail::fixed_size_array_value_type_t<std::array<long, 1>>, long>, __FILE_LINE__);


static_assert(srlz::write_size_v<std::array<char, 10>> == (
	sizeof(char) +
	10 * sizeof(char)
	), __FILE_LINE__);


static_assert(std::is_same_v<srlz::detail::max_serialized_size_to_arranged_length_type_t<10>, uint8_t> , __FILE_LINE__);
static_assert(std::is_same_v<srlz::detail::fixed_array_trait_t<std::array<int, 10>>::length_type, uint8_t >, __FILE_LINE__);
static_assert(std::is_same_v<srlz::detail::fixed_array_trait_t<std::array<int, 10>>::value_type, int>, __FILE_LINE__);
static_assert( std::is_same_v<srlz::write_size<std::array<int, 10>, none_options>::length_type, uint8_t> );


static_assert(
	srlz::write_size_v<std::array<int, 10>> ==
	(
		sizeof(typename srlz::detail::fixed_array_trait_t<std::array<int, 10>>::length_type) +
		10 * sizeof(int)
	),
	__FILE_LINE__);




static_assert(srlz::detail::serializable_properties_prefix_bytes_maxsize<uint8_t>() == ((uint8_t)(-1) >> 1), __FILE_LINE__);
static_assert(std::is_same_v<srlz::detail::serializable_properties_prefix_nbytes_t<32>::length_type, uint8_t>, __FILE_LINE__);
static_assert(std::is_same_v<srlz::detail::serializable_properties_prefix_nbytes_t<8192>::length_type, uint16_t>, __FILE_LINE__);
static_assert(srlz::detail::serializable_properties_prefix_bytes_maxsize<uint32_t>() == ((uint32_t)(-1) >> 1), __FILE_LINE__);
static_assert(std::is_same_v<srlz::detail::serializable_properties_prefix_nbytes_t<std::numeric_limits<uint32_t>::max()>::length_type, uint64_t>, __FILE_LINE__);


static_assert(!srlz::detail::is_iterator_v<size_t>, __FILE_LINE__);
static_assert(srlz::detail::is_iterator_v<size_t*>, __FILE_LINE__);
static_assert(srlz::detail::is_iterator_v<decltype(std::declval<std::array<size_t, 2>>().cbegin())>, __FILE_LINE__);

static_assert(!srlz::detail::is_iterator_of_integral_v<size_t>, __FILE_LINE__);
static_assert(srlz::detail::is_iterator_of_integral_v<decltype(std::declval<std::array<size_t, 2>>().cbegin())>, __FILE_LINE__);
static_assert(!srlz::detail::is_iterator_of_integral_v<decltype(std::declval<std::vector<std::string>>().cbegin())>, __FILE_LINE__);
static_assert(srlz::detail::is_iterator_of_integral_v<decltype(std::declval<std::initializer_list<size_t>>().begin())>, __FILE_LINE__);
static_assert(srlz::detail::is_iterator_of_integral_v<const int*>, __FILE_LINE__);


static_assert(srlz::is_has_same_type_v<int, int, int>, __FILE_LINE__);
static_assert(!srlz::is_has_same_type_v<short, int, int>, __FILE_LINE__);

static_assert(srlz::detail::is_container_v<std::array<char, 2>>, __FILE_LINE__);
static_assert(srlz::detail::is_container_v<std::map<std::string, std::string>>, __FILE_LINE__);
static_assert(srlz::detail::is_container_v<std::vector<int>>, __FILE_LINE__);
static_assert(srlz::detail::is_container_v<std::list<int>>, __FILE_LINE__);

static_assert(is_smart_pointer_v<std::unique_ptr<std::string>>, __FILE_LINE__);
static_assert(crm::srlz::detail::is_all_srlzd_v<std::string, std::vector<std::string>>, __FILE_LINE__);

static_assert(srlz::detail::exists_type_v<int, double, long, int>, __FILE_LINE__);
static_assert(!srlz::detail::exists_type_v<int, double, long, short>, __FILE_LINE__);


static_assert(srlz::detail::has_serializable_properties_v<char>, __FILE_LINE__);
static_assert(sizeof(srlz::detail::serializable_prefix_type_trait_t<char>::serialized_type) == 1, __FILE_LINE__);
static_assert(sizeof(srlz::detail::serializable_prefix_type_trait_t<std::uintmax_t>::serialized_type) == 1, __FILE_LINE__);
static_assert(std::is_same_v<srlz::detail::serializable_prefix_type_trait_t<char>::serialized_type, uint8_t>, __FILE_LINE__);
static_assert(std::is_same_v<srlz::detail::serializable_prefix_type_trait_t<char>::serialized_type,
	srlz_prefix_uint8_t::serialized_type>, __FILE_LINE__);

static_assert(sizeof(srlz::detail::serializable_prefix_type_trait_t<std::uintmax_t>::serialized_type) == 1, __FILE_LINE__);
static_assert(std::is_same_v<srlz::detail::serializable_prefix_type_trait_t<std::uintmax_t>::serialized_type, uint8_t>, __FILE_LINE__);
static_assert(std::is_same_v<srlz::detail::serializable_prefix_type_trait_t<std::uintmax_t>::serialized_type,
	srlz_prefix_uint8_t::serialized_type>, __FILE_LINE__);


static_assert(srlz::serializable_properties_trait_t<sx_glob_64>::get().serialized_size == srlz::write_size_v<uint64_t>, __FILE_LINE__);
static_assert(srlz::serializable_properties_trait_t<sx_loc_64>::get().serialized_size == srlz::write_size_v<uint64_t>, __FILE_LINE__);
static_assert(srlz::write_size_v<uint64_t> ==sizeof(uint64_t), __FILE_LINE__);


static_assert(srlz::detail::has_serializable_properties_v<sx_glob_64>, __FILE_LINE__);
static_assert(std::is_same_v<srlz::detail::serializable_prefix_type_trait_t<sx_glob_64>::prefix_trait,
	srlz_prefix_uint8_t>, __FILE_LINE__);

static_assert(srlz::detail::has_serializable_properties_v<sx_loc_64>, __FILE_LINE__);
static_assert(std::is_same_v<srlz::detail::serializable_prefix_type_trait_t<sx_loc_64>::prefix_trait,
	srlz_prefix_uint8_t>, __FILE_LINE__);

static_assert(srlz::detail::strob_fields_trait_as_fixed_serialized<srlz_prefix_uint8_t, crm::none_options>::detect(), __FILE_LINE__);
static_assert(srlz::detail::is_strob_fixed_serialized_size_v< srlz_prefix_uint8_t>, __FILE_LINE__);
static_assert(srlz::detail::is_fixed_size_serialized_v<srlz_prefix_uint8_t>, __FILE_LINE__);

static_assert(!srlz_prefix_uint8_t::is_serialized_with_prefix(), __FILE_LINE__);
static_assert(srlz::detail::is_headed_serialized_trait_v<srlz_prefix_uint8_t>, __FILE_LINE__);
static_assert(!srlz::detail::is_headed_serialized_trait_v<srlz::srlz_prefix_t>, __FILE_LINE__);



struct uucv_1 {
	std::array<int, 33> i33;
	long ll;
	double cd;
	};
static_assert(srlz::detail::is_fixed_size_serialized_v<uucv_1>, __FILE_LINE__);
static_assert(srlz::detail::is_strob_serializable_v<uucv_1>, __FILE_LINE__);
static_assert(srlz::detail::is_strob_fixed_serialized_size_v<uucv_1>, __FILE_LINE__);
static_assert(srlz::detail::fields_count_v<uucv_1> == 3, __FILE_LINE__);
static_assert(std::is_same_v<srlz::detail::tuple_element_t<0, uucv_1>, decltype(uucv_1::i33)> , __FILE_LINE__);
static_assert(std::is_same_v<srlz::detail::tuple_element_t<1, uucv_1>, long>, __FILE_LINE__);
static_assert(std::is_same_v<srlz::detail::tuple_element_t<2, uucv_1>, double>, __FILE_LINE__);

char buf[8];
static_assert(!std::is_same_v<srlz::detail::tuple_element_t<0, uucv_1>, decltype(buf)>, __FILE_LINE__);
