#pragma once

#include <experimental/coroutine>
#include "./base_typetrait_decls.h"
#include "./base_typetrait_using.h"
#include "./base_predecl.h"


namespace crm {


template<typename T, typename TResult>
struct has_avaitable_requirements<T, TResult, std::void_t<
	std::enable_if_t<std::is_convertible_v<decltype(std::declval<T>().await_ready()), bool>>,
	decltype(std::declval<T>().await_suspend(std::declval<std::experimental::template coroutine_handle<>>())),
	std::enable_if_t<std::is_constructible_v<TResult, decltype(std::declval<T>().await_resume())>>>> : std::true_type{};


template<typename T>
struct has_cancel_t<T, std::void_t<
	decltype(std::declval<T>().cancel())
	>> : std::true_type {};

namespace srlz {
namespace detail {


template<typename TObject>
struct is_serialized_as_blob_t<TObject,
	typename std::enable_if_t<is_srlzd_prefixed_v<TObject>>> : std::true_type {};

template<typename TObject>
struct is_serialized_as_blob_t<TObject,
	typename std::enable_if_t<is_strob_serializable_strong_v<TObject>>> : std::true_type {};

template<typename T>
struct is_chrono_duration_t<T, std::void_t<
	typename T::rep,
	typename T::period,
	decltype(std::declval<T>().count()),
	decltype(std::declval<T>().zero()),
	decltype(std::declval<T>().min()),
	decltype(std::declval<T>().max()),
	decltype(std::chrono::duration_cast<std::chrono::milliseconds>(std::declval<T>()))
	>> : public std::true_type{};

template<typename T>
struct is_mono_serializable<T,
	typename std::enable_if_t<((
		std::is_fundamental_v<std::decay_t<T>>
		|| std::is_enum_v <std::decay_t<T>>
		|| std::is_union_v<std::decay_t<T>>
		|| std::is_pointer_v<std::decay_t<T>>
		)

		/*&& !(
			std::is_class_v<std::decay_t<T>>
			|| std::is_polymorphic_v<std::decay_t<T>>
			|| std::is_empty_v<std::decay_t<T>>)*/)
	>> : public std::true_type{};

template<typename T>
struct is_chrono_time_point_t < T, std::void_t<
	decltype(std::declval<T>().time_since_epoch()),
	typename std::enable_if_t< std::is_same_v<typename std::decay_t<T>::duration, decltype(std::declval<T>().time_since_epoch())>>
	>> : public std::true_type{};


template<typename T>
struct  is_strob_explicit_serialized < T,
	typename std::enable_if_t < (
		is_chrono_duration_v<T> ||
		is_chrono_time_point_v<T>
		)>> : public std::true_type{};


template<typename T >
struct is_strob_serializable<T,
	typename std::enable_if_t<(
		!(
			is_strob_explicit_serialized_v<T>
			|| std::is_fundamental_v<T>
			|| std::is_polymorphic_v<T>
			|| std::is_empty_v<T>
			|| std::is_union_v<T>
			|| std::is_pointer_v<T>
			|| crm::detail::is_smart_pointer_v<T>
			|| std::is_function_v<T>
			|| crm::detail::is_functorable_v<T>
			|| is_fixed_size_array_v<T>
			)
		&& std::is_aggregate_v<T>
		&& std::is_default_constructible_v<T>
		&& std::is_class_v<T>
		/*&& is_aggregate_initializable_v<T>*/
		)>> : public std::true_type{};

template<typename T>
struct has_members_cbegin_cend<T, std::void_t<

	decltype(std::declval<std::decay_t<T>>().cbegin()),
	decltype(std::declval<std::decay_t<T>>().cend())
	>> : std::true_type{};

template<typename T>
struct has_members_begin_end<T, std::void_t<

	decltype(std::declval<std::decay_t<T>>().begin()),
	decltype(std::declval<std::decay_t<T>>().end())
	>> : std::true_type{};

template<typename T>
struct is_container_t < T, std::void_t<

	typename std::decay_t<T>::value_type,
	typename std::decay_t<T>::const_iterator,
	typename std::decay_t<T>::iterator,
	typename std::decay_t<T>::size_type,

	decltype(std::declval<T>().size()),
	typename std::enable_if_t<(
		has_members_cbegin_cend_v<T> || 
		has_members_begin_end_v<T>
		)>
	>>
	: std::true_type{};

template<typename T>
struct is_range_t < T, std::void_t<decltype(std::declval<T>()[0])>>
	: std::true_type {};


template<typename TPair>
struct is_pair_const_key_value<TPair, std::void_t<
	decltype(std::declval<TPair>().first),
	decltype(std::declval<TPair>().second)
	>> : std::true_type{};


template<typename TDictionary>
struct is_dictionary < TDictionary, std::void_t<
	decltype(std::declval<TDictionary>().key_comp()),
	decltype(std::declval<TDictionary>().value_comp()),
	std::enable_if_t<is_pair_const_key_value_v<decltype(*std::declval<TDictionary>().begin())>>
	>> : std::true_type{};


template< class T >
struct is_string_type<T, std::void_t<
	std::enable_if_t<is_container_v<T>>, decltype(std::declval<T>().c_str())
	>> : std::true_type {};


template <typename T>
struct is_container_contiguous_cond_t<T, std::void_t<decltype(std::declval<T>().data())>>
	: std::true_type {};

template<typename T>
struct contaiter_value_type <T,
	typename std::enable_if_t<is_container_v<T>>> : public std::true_type {
	using x_type = typename std::decay_t<T>::value_type;
	};

template<typename T>
struct range_value_type <T,
	typename std::enable_if_t<is_range_v<T>>> : public std::true_type {
	using x_type = typename std::decay_t< decltype(std::begin(std::declval<T>()))>::value_type;
	};

template<typename T>
struct is_container_of_mono<T,
	typename std::enable_if_t<is_container_v<T>&& is_mono_serializable_v<contaiter_value_type_t<T>>>>
	: std::true_type {};

template<typename T>
struct dictionary_mapped_type <T,
	typename std::enable_if_t<is_dictionary_v<T>>> : public std::true_type {
	using x_type = typename std::decay_t<T>::mapped_type;
	};

template<typename T>
struct dictionary_key_type <T,
	typename std::enable_if_t<is_dictionary_v<T>>> : public std::true_type {
	using x_type = typename std::decay_t<T>::key_type;
	};

template<typename T>
struct is_range_of_mono<T,
	typename std::enable_if_t<is_mono_serializable_v<range_value_type_t<T>>&& is_range_v<T>>>
	: std::true_type {};

template<typename T>
struct is_container_of_strob<T,
	typename std::enable_if_t< is_container_v<T>&& is_strob_flat_serializable_v<contaiter_value_type_t<T>> && !is_container_v<contaiter_value_type_t<T>>>>
	: std::true_type {};

template<typename T>
struct is_dictionary_of_strob<T,
	typename std::enable_if_t< is_dictionary_v<T>&& is_strob_flat_serializable_v<dictionary_mapped_type_t<T>> && !is_container_v<dictionary_mapped_type_t<T>>>>
	: std::true_type {};

template<typename T>
struct is_container_of_containers<T,
	typename std::enable_if_t<is_container_v<T>&& is_container_v<contaiter_value_type_t<T>>>>
	: std::true_type{};

template<typename T>
struct is_container_of_containers_mono<T,
	typename std::enable_if_t<is_container_v<T>&& is_container_of_mono_v<contaiter_value_type_t<T>>>>
	: std::true_type{};

template<typename T>
struct is_container_of_containers_strob<T,
	typename std::enable_if_t<is_container_v<T>&& is_container_of_strob_v<contaiter_value_type_t<T>>>>
	: std::true_type {};

template<typename T>
struct is_container_bytes_t<T,
	typename std::enable_if_t<(is_container_of_mono_v<T>&& is_byte_v<contaiter_value_type_t<T>>)>>
	: public std::true_type {};

template<typename T>
struct is_container_integral_t<T,
	typename std::enable_if_t<(is_container_v<T>&& std::is_integral_v<contaiter_value_type_t<T>>)>>
	: public std::true_type {};

template<typename T>
struct is_range_bytes_t<T,
	typename std::enable_if_t<(is_range_of_mono_v<T>&& is_byte_v<range_value_type_t<T>>)>>
	: public std::true_type {};

template<typename T>
struct is_container_random_access_t<T,
	typename std::enable_if_t<(is_container_v<T>&& is_random_access_iterator_v<decltype(std::declval<T>().cbegin())>)>>
	: public std::true_type {};

template<typename T>
constexpr bool is_container_contiguous_of_bytes_v = is_container_of_bytes_v<T>
&& !is_dictionary_v<T>
&& is_container_contiguous_v<T>
&& is_container_random_access_v<T>;


template<typename T>
struct is_contiguous_iterator_t<T, std::void_t<
	typename T::iterator_category,
	std::enable_if_t<std::is_base_of_v<std::random_access_iterator_tag, typename T::iterator_category>>
	>> : std::true_type{};

template<typename T>
struct is_contiguous_iterator_t<T*> : std::true_type{};

template<typename T>
struct is_contiguous_iterator_t<const T*> : std::true_type{};

template<typename T>
struct is_iterator_mono<T,
	typename std::enable_if_t< is_iterator_v<T>&& is_mono_serializable_v<iterator_value_t<T>>>>
	: public std::true_type {};

template<typename T>
struct is_iterator_strob<T,
	typename std::enable_if_t<is_iterator_v<T> && (is_strob_flat_serializable_v<iterator_value_t<T>> && !is_container_v<iterator_value_t<T>>)>>
	: public std::true_type {};

template<typename T>
struct is_iterator_fixed_sized<T,
	typename std::enable_if_t<(
		is_iterator_v<T>&&
		is_fixed_size_serialized_v<iterator_value_t<T>>)>>
	: public std::true_type {};

template<typename T>
struct is_iterator_byte_t<T,
	typename std::enable_if_t<is_iterator_v<T>&& is_byte_v<iterator_value_t<T>>>>
	: public std::true_type {};

template<typename T>
struct is_resize_support_t<T,
	std::void_t<
	decltype(std::declval<T>().resize(0)),
	decltype(std::declval<T>().push_back(std::declval<contaiter_value_type_t<T>>()))>>
	: public std::true_type {};

template<typename T>
struct is_reserve_support_t<T,
	std::void_t<decltype(std::declval<T>().reserve(0))>>
	: public std::true_type {};


template<typename T>
struct is_enum_serializabled_t<T,
	std::void_t< typename T::upper_bound, typename T::lower_bound>>
	: public std::true_type {};

template<typename TIterator>
struct is_iterator_helper_t {
	using iterator_type = typename TIterator;
	using value_type = typename std::iterator_traits<TIterator>::value_type;
	using reference = typename std::iterator_traits<TIterator>::reference;
	using iterator_category = typename std::iterator_traits<TIterator>::iterator_category;
	using pointer = typename std::iterator_traits<TIterator>::pointer;
	using difference_type = typename std::iterator_traits<TIterator>::difference_type;

	using dereference_result_t = typename decltype(*(std::declval<TIterator>()));
	};

template<typename TIterator>
struct is_iterator<TIterator,
	std::void_t<
	typename std::iterator_traits<TIterator>::value_type,
	typename std::enable_if_t<
	(std::is_same_v<typename is_iterator_helper_t<TIterator>::dereference_result_t, typename is_iterator_helper_t<TIterator>::reference>) &&
	(std::is_same_v<typename std::decay_t<typename is_iterator_helper_t<TIterator>::dereference_result_t>, typename is_iterator_helper_t<TIterator>::value_type>)
	>
	>> : public std::true_type{};


template<typename TIterInt>
struct is_iterator_of_integral<TIterInt,
	std::void_t<
	typename std::enable_if_t<is_iterator_v<TIterInt>>,
	typename std::enable_if_t<std::is_integral_v<std::decay_t<decltype(*std::declval<TIterInt>())>>>
	>> : std::true_type{};

template<typename T>
struct iterator_value<T, typename std::enable_if_t<is_iterator_v<T>>> : public std::true_type {
	using x_type = typename is_iterator_helper_t<T>::value_type;
	};


template<typename TIterator>
struct is_input_iterator<TIterator,
	typename std::enable_if_t<(
		is_iterator_v<TIterator>&&
		std::is_same_v<typename std::iterator_traits<TIterator>::value_type, std::decay_t<decltype(*std::declval<TIterator>())>>
		)
	>> : public std::true_type {};


template<typename C>
struct is_constructible_with_size<C, std::void_t<
	typename std::enable_if_t<std::is_constructible_v<C, size_t>>
	>> : std::true_type {};
}

template<typename TObject>
struct has_static_typeid_trait_t<TObject, std::void_t<
	std::enable_if_t<std::is_same_v<typemap_key_t, std::decay_t<decltype(std::decay_t<TObject>::typeid_key())>>>
	>>: std::true_type {};
}
}
