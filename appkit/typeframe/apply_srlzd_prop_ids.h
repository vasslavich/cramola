#pragma once


#include "./base_typetrait_decls.h"
#include "./base_typetrait_using.h"
#include "../utilities/identifiers/predecl.h"


namespace crm::srlz {


template<typename Options>
struct serializable_properties_trait<sx_uuid_t, Options> {
	using serialized_options_type = detail::option_max_serialized_size<sx_uuid_t::serialized_size<Options>()>;
	static constexpr auto get()noexcept {
		return serializable_properties{
			serialized_options_type::max_serialized_size(),
			serialize_properties_length::as_fixedsize };
		}
	};


template<typename Options>
struct serializable_properties_trait<sx_glob_64, Options> {
	using serialized_options_type = detail::option_max_serialized_size <sx_glob_64::serialized_size<Options>()>;
	static constexpr auto get()noexcept {
		return serializable_properties{
			serialized_options_type::max_serialized_size(),
			serialize_properties_length::as_fixedsize };
		}
	};

template<typename Options>
struct serializable_properties_trait<sx_loc_64, Options> {
	using serialized_options_type = detail::option_max_serialized_size <sx_loc_64::serialized_size<Options>()>;
	static constexpr auto get()noexcept {
		return serializable_properties{
			serialized_options_type::max_serialized_size(),
			serialize_properties_length::as_fixedsize };
		}
	};

template<typename Options>
struct serializable_properties_trait<sx_locid_t, Options> {
	using serialized_options_type = detail::option_max_serialized_size <sx_locid_t::serialized_size<Options>()>;
	static constexpr auto get()noexcept {
		return serializable_properties{
			serialized_options_type::max_serialized_size(),
			serialize_properties_length::as_fixedsize };
		}
	};
}


