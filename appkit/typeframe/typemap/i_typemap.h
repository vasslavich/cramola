#pragma once


#include <memory>
#include <map>
#include <vector>
#include <list>
#include <functional>
#include "../typeid.h"
#include "../../channel_terms/predecl.h"


namespace crm::srlz {


/*! ��������� ������������ ���� */
struct i_type_ctr_base_t {
	virtual ~i_type_ctr_base_t() {}

	/*! ������������� ���� ������� */
	virtual typemap_key_t typemap_key()const noexcept = 0;

	virtual size_t ctrid()const noexcept = 0;

	template<typename XType>
	static constexpr typemap_key_t make_key() {
		if constexpr (std::is_base_of_v<i_typeid_t, XType>) {
			return typemap_key_t::make<std::decay_t<XType>>();
			}
		else {
			return typeid_t<XType>();
			}
		}
	};


struct i_type_ctr_i_srlzd_trait : i_type_ctr_base_t {
	size_t ctrid()const noexcept { return typeid(i_type_ctr_i_srlzd_trait).hash_code(); }
	virtual typemap_key_t typemap_key_base()const noexcept = 0;
	};

template<typename TBase>
struct decouple_1 {
	std::unique_ptr<TBase> pObj;

	template<typename XDerived,
		typename std::enable_if_t<std::is_base_of_v<TBase, XDerived>, int> = 0>
	decouple_1(std::unique_ptr<XDerived> && o_)
		:pObj(std::move(o_)) {}

	template<typename _XDerived,
		typename XDerived = std::decay_t<_XDerived>,
		typename std::enable_if_t<std::is_base_of_v<TBase, XDerived>, int> = 0>
	decouple_1( _XDerived && o_)
		: pObj(std::make_unique<XDerived>(std::forward<_XDerived>(o_))) {}

	virtual ~decouple_1() {}
	virtual void decode( detail::rstream_base_handler & h) = 0;

	template<typename S,
		typename = std::enable_if_t<!std::is_base_of_v< detail::rstream_base_handler, std::decay_t<S>>>>
	void decode(S && s) {
		detail::rstream_base_handler rh(std::forward<S>(s));
		decode(rh);
		}
	};

/*! ��������� ������������ ���� */
template<typename TBase>
struct i_type_ctr_i_srlzd_t : i_type_ctr_i_srlzd_trait {
	/*! ������ ������ */
	virtual std::unique_ptr<decouple_1<TBase>> create()const = 0;
	};


/*! ��������� ������������ ���� */
struct i_type_ctr_serialized_trait_t : i_type_ctr_base_t {
	/*! ������ ������ */
	virtual std::unique_ptr<serialized_base_r> create()const = 0;
	size_t ctrid()const noexcept { return typeid(i_type_ctr_serialized_trait_t).hash_code(); }
	};

template<typename TCtr,
	typename std::enable_if_t<std::is_base_of_v<i_type_ctr_i_srlzd_trait, std::decay_t<TCtr>>, int> = 0>
	std::conditional_t<std::is_const_v<TCtr>, const i_type_ctr_i_srlzd_trait*, i_type_ctr_i_srlzd_trait*>
type_ctr_cast_as_i_srlzd_based(TCtr * ctr)noexcept {
	if constexpr (std::is_const_v<TCtr>) {
		return dynamic_cast<const i_type_ctr_i_srlzd_trait*>(ctr);
		}
	else {
		return dynamic_cast<i_type_ctr_i_srlzd_trait*>(ctr);
		}
	}


template<typename TCtr>
constexpr bool is_type_ctr_i_srlzd_v = std::is_base_of_v<i_type_ctr_i_srlzd_trait, std::decay_t<TCtr>>;


template<typename TCtr>
constexpr bool is_type_ctr_serialized_trait_v = std::is_base_of_v<i_type_ctr_serialized_trait_t, std::decay_t<TCtr>>;



template<typename T>
constexpr bool is_ctr_as_serialized_trait_v = detail::is_serialized_trait_v<T> && std::is_default_constructible_v<T>;


template<typename T, typename = std::void_t<>>
struct is_ctr_as_i_srlzd : std::false_type {};

template<typename T>
constexpr bool is_ctr_as_i_srlzd_v = is_ctr_as_i_srlzd<T>::value;


template<typename T>
struct is_ctr_as_i_srlzd<T,
	std::void_t<

	typename std::enable_if_t<detail::is_i_srlzd_v<T> && std::is_default_constructible_v<T>>
	>> : std::true_type{};
}

