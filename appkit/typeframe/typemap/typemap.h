#pragma once


#include <memory>
#include <unordered_map>
#include <shared_mutex>
#include <vector>
#include <list>
#include <functional>
#include "../typeid.h"
#include "../isrlz.h"
#include "../../notifications/internal_terms.h"
#include "./i_typemap.h"


namespace crm::srlz {


class dcf_typemap_t {
private:
	struct _type_ctr_info_t {
		std::shared_ptr<i_type_ctr_base_t> ctr;
		};

	typedef std::unordered_map<typemap_key_t, _type_ctr_info_t> collection_t;
	typedef collection_t::iterator collection_it_t;
	typedef collection_t::const_iterator collection_cit_t;

	collection_t _ctrmap;
	mutable std::shared_mutex _tableLck;

	std::list<typemap_key_t> _exists_keys()const noexcept;

	_type_ctr_info_t _find(const i_typeid_t & key)const;

	template<typename Ttype>
	_type_ctr_info_t _find() {
		return _find(i_type_ctr_base_t::make_key<Ttype>());
		}

	template<typename TTypeCtr>
	[[nodiscard]]
	register_type_result _add(const i_typeid_t& key, std::unique_ptr<TTypeCtr>&& ictr, bool overwrite = true) {
		static_assert(std::is_base_of_v<i_type_ctr_base_t, TTypeCtr>, __FILE__);

		std::lock_guard lck(_tableLck);

		auto it = _ctrmap.find(key);
		if (_ctrmap.end() == it) {

			_type_ctr_info_t i;
			i.ctr = std::move(ictr);

			auto insrIt = _ctrmap.insert({ key, i });
			if (!insrIt.second)
				THROW_EXC_FWD(nullptr);
			}
		else {
			if (overwrite) {
				it->second.ctr = std::move(ictr);
				}
			else {
				return register_type_result::exists;
				}
			}

		return register_type_result::success;
		}

	bool exists(const i_typeid_t& key)const noexcept {
		std::lock_guard lck(_tableLck);
		return _ctrmap.find(key) != _ctrmap.end();
		}

public:
	/*! �������� ������� ������������� */
	template<typename TTypeCtr, typename ... Args >
	[[nodiscard]]
	register_type_result add_drv_ctr(const i_typeid_t & key, typename Args && ... args) {
		static_assert(std::is_base_of_v<i_type_ctr_base_t, TTypeCtr>, __FILE__);

		return _add<TTypeCtr>(key, std::make_unique<TTypeCtr>(std::forward<Args>(args) ...));
		}

	/*! �������� ������� ������������� */
	template<typename TTypeCtr, typename ... Args >
	[[nodiscard]]
	register_type_result add_drv_ctr(typename Args && ... args) {
		static_assert(std::is_base_of_v<i_type_ctr_base_t, TTypeCtr>, __FILE__);

		auto typeCtr = std::make_unique<TTypeCtr>(std::forward<Args>(args) ...);
		auto key = typeCtr->typemap_key();

		return _add<TTypeCtr>(key, std::move(typeCtr));
		}

	template<typename TTypeCtr>
	[[nodiscard]]
	register_type_result add_drv_ctr(const i_typeid_t & key, std::unique_ptr<TTypeCtr> ictr) {
		static_assert(std::is_base_of_v<i_type_ctr_base_t, TTypeCtr>, __FILE__);

		return _add<TTypeCtr>(key, std::move(ictr));
		}

	template<typename TTypeCtr>
	[[nodiscard]]
	register_type_result add_drv_ctr(std::unique_ptr<TTypeCtr> ictr) {
		static_assert(std::is_base_of_v<i_type_ctr_base_t, TTypeCtr>, __FILE__);

		auto key = ictr->typemap_key();
		return _add<TTypeCtr>(key, std::move(ictr));
		}

private:
	template<typename XType>
	[[nodiscard]]
	register_type_result add_drv_type_as_serialized_trait() {
		return add_drv_ctr(std::make_unique<fram_typectr_default_t<XType>>());
		}

	template<typename XType>
	[[nodiscard]]
	register_type_result add_drv_type_as_serialized_trait(const i_typeid_t & key) {
		return add_drv_ctr(key, std::make_unique<fram_typectr_default_t<XType>>());
		}

	template<typename XType>
	[[nodiscard]]
	register_type_result add_drv_type_as_i_srlzd() {
		return add_drv_ctr(std::make_unique<fram_typectr4_i_srlzd_t<XType>>());
		}

	template<typename XType>
	[[nodiscard]]
	register_type_result add_drv_type_as_i_srlzd(const i_typeid_t & key) {
		return add_drv_ctr(key, std::make_unique<fram_typectr4_i_srlzd_t<XType>>());
		}

	template<typename XType, typename XBase>
	[[nodiscard]]
	register_type_result add_drv_type_as_i_srlzd_based() {
		return add_drv_ctr(std::make_unique<fram_typectr4_i_srlzd_based_t<XType, XBase>>());
		}

	template<typename XKeyType, typename XMakeType, typename XBase>
	[[nodiscard]]
	register_type_result add_drv_type_as_i_srlzd_based() {
		return add_drv_ctr(i_type_ctr_base_t::make_key<XKeyType>(), std::make_unique<fram_typectr4_i_srlzd_based_t<XMakeType, XBase>>());
		}

	template<typename XType, typename XBase>
	[[nodiscard]]
	register_type_result add_drv_type_as_i_srlzd_based(const i_typeid_t & key ) {
		return add_drv_ctr(key, std::make_unique<fram_typectr4_i_srlzd_based_t<XType, XBase>>());
		}

	template<typename T>
	struct fail_dispatch : std::false_type {};

public:
	template<typename XType,
		typename std::enable_if_t<detail::is_serialized_trait_v<XType>, int> = 0>
	[[nodiscard]]
	register_type_result add_drv_type() {
		return add_drv_type_as_serialized_trait<XType>();
		}

	template<typename XType,
		typename std::enable_if_t<(
			is_ctr_as_i_srlzd_v<XType> && 
			!detail::is_serialized_trait_v<XType>), int> = 0>
	[[nodiscard]]
	register_type_result add_drv_type() {
		return add_drv_type_as_i_srlzd<XType>();
		}

	template<typename XType, typename XBase,
		typename std::enable_if_t<(
			is_ctr_as_i_srlzd_v<XType> && 
			std::is_base_of_v<XBase, XType> && 
			!detail::is_serialized_trait_v<XType>), int> = 0>
	[[nodiscard]]
	register_type_result add_drv_type() {
		static_assert(std::is_base_of_v<XBase, XType>, __FILE_LINE__);

		return add_drv_type_as_i_srlzd_based<XType, XBase>();
		}

	template<typename XKeyType, typename XMakeType, typename XBase,
		typename std::enable_if_t<(
			is_ctr_as_i_srlzd_v<XMakeType>&&
			std::is_base_of_v<XBase, XMakeType> &&
			!detail::is_serialized_trait_v<XMakeType>), int> = 0>
	[[nodiscard]]
	register_type_result add_drv_type() {
		static_assert(std::is_base_of_v<XBase, XKeyType>, __FILE_LINE__);

		return add_drv_type_as_i_srlzd_based<XKeyType, XMakeType, XBase>();
		}

	template<typename XType,
		typename std::enable_if_t<detail::is_serialized_trait_v<XType>, int> = 0>
	[[nodiscard]]
	register_type_result add_drv_type(const i_typeid_t & key) {
		return add_drv_type_as_serialized_trait<XType>(key);
		}

	template<typename XType,
		typename std::enable_if_t<(
			is_ctr_as_i_srlzd_v<XType> && 
			!detail::is_serialized_trait_v<XType>), int> = 0>
	[[nodiscard]]
	register_type_result add_drv_type(const i_typeid_t & key) {
		return add_drv_type_as_i_srlzd<XType>(key);
		}

	template<typename XType, typename XBase,
		typename std::enable_if_t<(
			is_ctr_as_i_srlzd_v<XType> && 
			std::is_base_of_v<XBase, XType> &&
			!detail::is_serialized_trait_v<XType>), int> = 0>
	[[nodiscard]]
	register_type_result add_drv_type(const i_typeid_t & key) {
		static_assert(std::is_base_of_v<XBase, XType>, __FILE_LINE__);

		return add_drv_type_as_i_srlzd_based<XType, XBase>(key);
		}

	template<typename Ttype>
	std::shared_ptr<i_type_ctr_base_t> find()const {
		static_assert(std::is_default_constructible_v<Ttype>, __FILE__);
		return _find<Ttype>().ctr;
		}

	std::shared_ptr<i_type_ctr_base_t> find(const i_typeid_t & key)const;
	};
}
