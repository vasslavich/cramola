#pragma once


#include "./i_typemap.h"


namespace crm::srlz {


template<typename _XType,
	typename XType = std::decay_t<_XType>,
	typename = std::enable_if_t< is_ctr_as_serialized_trait_v<XType>>>
struct fram_typectr_default_t final :
	i_type_ctr_serialized_trait_t,
	i_typeid_t {

	static_assert(detail::is_serialized_trait_v<XType>, __FILE__);

	typedef XType value_type;

	typemap_key_t _tkey{ make_key<XType>() };

	/*! ������ ������ */
	std::unique_ptr<serialized_base_r> create()const final {
		return std::move(std::unique_ptr<XType>(new XType()));
		}

	/*! ������������� ���� ������� */
	typemap_key_t typemap_key()const noexcept final {
		return _tkey;
		}

	/*! ������������� ���� */
	type_identifier_t type_id()const noexcept final {
		return _tkey.type_id();
		}

	/*! ������������� ����, ����� �������������� � ������, ���� ��������� �������� �� /ref id */
	std::string type_name()const noexcept final {
		return _tkey.type_name();
		}
	};


template<typename _XType, 
	typename _XBase>
struct fram_typectr4_i_srlzd_based_no_typeid_traits :
	i_type_ctr_i_srlzd_t<_XBase>,
	i_typeid_t {

	static const typemap_key_t typeKeyValue;
	static const typemap_key_t baseKeyValue;

	/*! ������������� ���� ������� */
	typemap_key_t typemap_key()const noexcept {
		return typeKeyValue;
		}

	typemap_key_t typemap_key_base()const noexcept {
		return baseKeyValue;
		}
	};


template<typename _XType, typename _XBase>
const typemap_key_t fram_typectr4_i_srlzd_based_no_typeid_traits<_XType, _XBase>::typeKeyValue =
	i_type_ctr_base_t::make_key<std::decay_t<_XType>>();

template<typename _XType, typename _XBase>
const typemap_key_t fram_typectr4_i_srlzd_based_no_typeid_traits<_XType, _XBase>::baseKeyValue =
	i_type_ctr_base_t::make_key<std::decay_t<_XBase>>();


template<typename _XType,
	typename _XBase>
struct fram_typectr4_i_srlzd_based_has_typeid_traits :
	i_type_ctr_i_srlzd_t<_XBase>,
	i_typeid_t {

	using XType = typename std::decay_t<_XType>;
	using XBase = typename std::decay_t < _XBase>;

	static const typemap_key_t baseKeyValue;

	/*! ������������� ���� ������� */
	typemap_key_t typemap_key()const noexcept {
		return XType::typeid_key();
		}

	typemap_key_t typemap_key_base()const noexcept {
		return baseKeyValue;
		}
	};

template<typename _XType, typename _XBase>
const typemap_key_t fram_typectr4_i_srlzd_based_has_typeid_traits<_XType, _XBase>::baseKeyValue =
	i_type_ctr_base_t::make_key<std::decay_t<_XBase>>();


template<typename _XType,
	typename _XBase,
	typename = std::enable_if_t<(
		is_ctr_as_i_srlzd_v<_XType>&&
		std::is_base_of_v<_XBase, _XType>
		)>>
struct fram_typectr4_i_srlzd_based_t final :
	std::conditional_t<has_static_typeid_trait_v<_XType>,
		fram_typectr4_i_srlzd_based_has_typeid_traits<_XType, _XBase>,
		fram_typectr4_i_srlzd_based_no_typeid_traits<_XType, _XBase>> {

	using XType = typename std::decay_t<_XType>;
	using XBase = typename std::decay_t<_XBase>;

	typedef XType value_type;
	typedef XBase base_type;

	/*typemap_key_t _tkey = make_key<XType>() ;
	typemap_key_t _tbaseKey= make_key<XBase>() ;*/

	struct decouplex final : decouple_1<XBase> {
		using base_type = decouple_1<XBase>;

		decouplex()
			: base_type(XType{}) {}

		void decode( detail::rstream_base_handler & h) final {
			static_assert(std::is_base_of_v<XBase, XType>, __FILE_LINE__);
			if (pObj) {
				deserialize(h, dynamic_cast<XType&>((*pObj)));
				}
			else {
				FATAL_ERROR_FWD(nullptr);
				}
			}
		};

	/*! ������ ������ */
	std::unique_ptr<decouple_1<XBase>> create()const final {
		return std::make_unique<decouplex>();
		}

	///*! ������������� ���� ������� */
	//typemap_key_t typemap_key()const noexcept final {
	//	return fram_typectr4_i_srlzd_based_trait::typemap_key();// _tkey;
	//	}

	//typemap_key_t typemap_key_base()const noexcept final {
	//	return fram_typectr4_i_srlzd_based_trait::typemap_key_base(); //_tbaseKey;
	//	}

	/*! ������������� ���� */
	type_identifier_t type_id()const noexcept final {
		return typemap_key().type_id();
		}

	/*! ������������� ����, ����� �������������� � ������, ���� ��������� �������� �� /ref id */
	std::string type_name()const noexcept final {
		return typemap_key().type_name();
		}
	};



template<typename _XType,
	typename XType = typename std::decay_t<_XType>,
	typename = typename std::enable_if_t<is_ctr_as_i_srlzd_v<XType>>>
struct fram_typectr4_i_srlzd_t final :
	i_type_ctr_i_srlzd_t<XType>,
	i_typeid_t {

	using self_type = typename fram_typectr4_i_srlzd_t<_XType>;
	typedef XType value_type;

	typemap_key_t _tkey = make_key<XType>();

	struct decouplex final : decouple_1<XType> {
		using base_type = decouple_1<XType>;

		decouplex()
			: base_type(XType{}) {}

		void decode(detail::rstream_base_handler & h) final {
			if (pObj) {
				deserialize(h, dynamic_cast<XType&>((*pObj)));
				}
			else {
				FATAL_ERROR_FWD(nullptr);
				}
			}
		};

	/*! ������ ������ */
	std::unique_ptr<decouple_1<XType>> create()const final {
		return std::make_unique<decouplex>();
		}

	/*! ������������� ���� ������� */
	typemap_key_t typemap_key()const noexcept final {
		return _tkey;
		}

	typemap_key_t typemap_key_base()const noexcept final {
		return _tkey;
		}

	/*! ������������� ���� */
	type_identifier_t type_id()const noexcept final {
		return _tkey.type_id();
		}

	/*! ������������� ����, ����� �������������� � ������, ���� ��������� �������� �� /ref id */
	std::string type_name()const noexcept final {
		return _tkey.type_name();
		}
	};
}

