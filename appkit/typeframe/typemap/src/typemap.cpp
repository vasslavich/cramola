#include <sstream>
#include <atomic>
#include <sstream>
#include "../../../internal.h"
#include "../typemap.h"
#include "../typekey_default.h"
#include "../../../exceptions.h"
#include "../../../messages/terms.h"


using namespace crm;
using namespace crm::srlz;
using namespace crm::srlz::detail;


//type_identifier_t typemap_key_t::type_id()const noexcept{
//	if (_impl) {
//		return _impl->type_id();
//		}
//	else {
//		FATAL_ERROR_FWD(nullptr);
//		}
//	}
//
//std::string typemap_key_t::type_name()const noexcept {
//	if (_impl) {
//		return _impl->type_name();
//		}
//	else {
//		FATAL_ERROR_FWD(nullptr);
//		}
//	}

//bool crm::srlz::operator == (
//	const typemap_key_t & lval, const typemap_key_t &rval)noexcept {
//	return lval.type_id() == rval.type_id() && lval.type_name() == rval.type_name();
//	}
//
//bool crm::srlz::operator != (
//	const typemap_key_t & lval, const typemap_key_t &rval)noexcept {
//	return !(lval == rval);
//	}
//
//bool crm::srlz::operator < (
//	const typemap_key_t & lval, const typemap_key_t &rval)noexcept {
//
//	if(lval.type_id() < rval.type_id())
//		return true;
//	else if(lval.type_id() > rval.type_id())
//		return false;
//	else
//		return lval.type_name() < rval.type_name();
//	}
//
//bool crm::srlz::operator >= (
//	const typemap_key_t & lval, const typemap_key_t &rval) noexcept {
//
//	if(lval.type_id() > rval.type_id())
//		return true;
//	else if(lval.type_id() < rval.type_id())
//		return false;
//	else
//		return lval.type_name() >= rval.type_name();
//	}

std::list<typemap_key_t> dcf_typemap_t::_exists_keys()const noexcept {

	std::list<typemap_key_t> lkeys;

	auto it = _ctrmap.cbegin();
	for(; it != _ctrmap.cend(); ++it) {
		lkeys.push_back((*it).first);
		}

	return std::move(lkeys);
	}

dcf_typemap_t::_type_ctr_info_t dcf_typemap_t::_find(const i_typeid_t & key)const {

	auto it = _ctrmap.find(typemap_key_t(key));
	if(_ctrmap.end() == it) {

		std::ostringstream log;
		log << ("search key=" + key.type_name() + ":" + key.type_id().to_str()) << std::endl;
		log << "list of collection:" << std::endl;

		auto lkeys(_exists_keys());
		for(const auto & k : lkeys) {
			log << "    " << k.type_name() << ":" << k.type_id().to_str() << std::endl;
			}

#ifdef CBL_TRACE_TYPEMAP_FAIL
		FATAL_ERROR_FWD(log.str());
#else
		SERIALIZE_THROW_EXC_FWD(log.str());
#endif
		}
	else {
		return it->second;
		}
	}

std::shared_ptr<i_type_ctr_base_t> dcf_typemap_t::find(const i_typeid_t & key)const {
	std::shared_lock lck(_tableLck);
	return _find(key).ctr;
	}


