#pragma once


#include <type_traits>
#include "../../type_trait_base.h"
#include "../../type_trait_prop.h"
#include "./strob_detect_fields_count.h"
#include "./strob_object2tuple.h"
#include "./strob_trait_terms.h"


namespace crm {
namespace srlz {
namespace detail {



template <std::size_t I, class T>
using tuple_element = tuple::tuple_element<I, decltype(tie_as_tuple(std::declval<T&>())) >;


template <std::size_t I, class T>
using tuple_element_t = typename tuple_element<I, T>::type;


template <std::size_t I, class T>
constexpr bool tuple_element_fixed_serialized_v = is_fixed_size_serialized_v/*is_tuple_element_fixed_serialized*/<tuple_element_t<I, T>>/*::value*/;

template <std::size_t I, class T>
constexpr bool tuple_element_prefixed_serialized_v = is_srlzd_prefixed_v<tuple_element_t<I, T>>;


template <class T, std::size_t N>
constexpr bool detect_fileds_fixed_serialized(size_t_<N>, size_t_<N>) noexcept {
	return true;
	}

template <class T, std::size_t L, std::size_t U>
constexpr bool detect_fileds_fixed_serialized(size_t_<L>, size_t_<U>) noexcept {
	return tuple_element_fixed_serialized_v<L, T> && detect_fileds_fixed_serialized<T>(size_t_<L + 1>{}, size_t_<U>{});
	}

template <class T, std::size_t N>
constexpr bool detect_fileds_fixed_serialized_dispatch() noexcept {
	return detect_fileds_fixed_serialized<T>(size_t_<0>{}, size_t_<N>{});
	}



template <class T, std::size_t N>
constexpr bool detect_fileds_prefixed_serialized(size_t_<N>, size_t_<N>) noexcept {
	return true;
	}

template <class T, std::size_t L, std::size_t U>
constexpr bool detect_fileds_prefixed_serialized(size_t_<L>, size_t_<U>) noexcept {
	return tuple_element_prefixed_serialized_v<L, T> && detect_fileds_prefixed_serialized<T>(size_t_<L + 1>{}, size_t_<U>{});
	}

template <class T, std::size_t N>
constexpr bool detect_fileds_prefixed_serialized_dispatch() noexcept {
	return detect_fileds_prefixed_serialized<T>(size_t_<0>{}, size_t_<N>{});
	}


template<typename T, const size_t fdcf = fields_count_v<T>>
constexpr bool __is_strob_fixed_serialized_size_v = detect_fileds_fixed_serialized_dispatch<T, fdcf>();



template <std::size_t I, class T, typename Options>
constexpr size_t tuple_element_size_of = write_size_v<tuple_element_t<I, T>, Options>;


template <class T, std::size_t N, typename Options>
constexpr size_t detect_fileds_size(size_t_<N>, size_t_<N>, Options) noexcept {
	return 0;
	}

template <class T, std::size_t L, std::size_t U, typename Options>
constexpr size_t detect_fileds_size(size_t_<L>, size_t_<U>, Options) noexcept {
	return tuple_element_size_of<L, T, Options> +detect_fileds_size<T>(size_t_<L + 1>{}, size_t_<U>{}, Options{});
	}

template <class T, std::size_t N, typename Options>
constexpr size_t detect_fields_size_dispatch() noexcept {
	return detect_fileds_size<T>(size_t_<0>{}, size_t_<N>{}, Options{});
	}

template<typename T, typename Options>
struct strob_fields_serialized_size<T, Options,
	std::enable_if_t<is_strob_fixed_serialized_size_v<T>>> {

	static constexpr size_t fldc = fields_count_v<T>;
	static constexpr size_t value = detect_fields_size_dispatch<T, fldc, Options>();
	};
}
}
}
