#pragma once


#include <type_traits>
#include "../../type_trait_base.h"
#include "./strob_base_tuple.h"


namespace crm::srlz{
namespace detail {


template<typename _TxStrobValue,
	typename TStrobValue = typename std::decay_t<_TxStrobValue>,
	typename std::enable_if_t<is_strob_serializable_v<TStrobValue>, int> = 0>
 auto strob_fields_trait_as_tuple(_TxStrobValue && value) {
	return tie_as_tuple(std::forward<_TxStrobValue>(value));
	}

template<typename Opt, typename Tuple, std::size_t I, std::size_t N>
struct strob_fields_trait_as_fixed_serialized_impl;

template <typename Opt, typename Tuple, std::size_t I>
struct strob_fields_trait_as_fixed_serialized_impl<Opt, Tuple, I, I> {
	static constexpr bool is_fixed_serialized() noexcept { return true; }
	};

template <typename Opt, typename Tuple, std::size_t I, std::size_t N>
struct strob_fields_trait_as_fixed_serialized_impl {

	static constexpr bool is_fixed_serialized()noexcept {
		using item_type = std::decay_t<decltype(tuple::get<I>(std::declval<Tuple>()))>;
		return is_fixed_size_serialized_v<item_type, Opt> && strob_fields_trait_as_fixed_serialized_impl<Opt, Tuple, I + 1, N>::is_fixed_serialized();
		}
	};






template<typename _TxStrobValue, typename Opt>
struct strob_fields_trait_as_fixed_serialized<_TxStrobValue, Opt,
	std::enable_if_t<is_strob_serializable_v<_TxStrobValue>>> : std::true_type{
	using strob_type = typename std::decay_t< _TxStrobValue>;
	using tuple_type = typename std::decay_t<decltype(strob_fields_trait_as_tuple(std::declval<strob_type>()))>;
	static constexpr auto fieldsCount = fields_count_v<strob_type>;
	static constexpr bool detect()noexcept { return strob_fields_trait_as_fixed_serialized_impl<Opt, tuple_type, 0, fieldsCount>::is_fixed_serialized(); };
	};


template<typename T, typename Opt>
struct is_strob_fixed_serialized_size<T, Opt,
	typename std::enable_if_t<(
		!is_container_v<T> && 
		strob_fields_trait_as_fixed_serialized_v<T, Opt>
		)>> : std::true_type{};







template<typename TAll, class Enable = void>
struct is_strob_prefixed_serialized_size : public std::false_type {};

template<typename TStrob>
struct is_strob_prefixed_serialized_size<TStrob,
	typename std::enable_if_t<is_strob_serializable_strong_v<TStrob>>> {

	static constexpr size_t fdc = fields_count_v<TStrob>;
	static constexpr bool value = detect_fileds_prefixed_serialized_dispatch<TStrob, fdc>();
	};

template<typename T>
constexpr bool is_strob_prefixed_serialized_size_v = is_strob_prefixed_serialized_size<typename std::decay_t<T>>::value;


template <std::size_t I, std::size_t N, typename Options>
struct strob_prefixed_serialized_size_impl {
	template <class T>
	static size_t size(T&& value) {

		const auto & item = tuple::get<I>(std::forward<T>(value));

		static_assert(!std::is_rvalue_reference_v<decltype(item)>, __FILE_LINE__);
		static_assert(std::is_lvalue_reference_v<decltype(item)>, __FILE_LINE__);

		return object_trait_serialized_size(item, serialized_options_cond<decltype(item), Options>{}) +
			strob_prefixed_serialized_size_impl<I + 1, N, Options>::size(std::forward<T>(value));
		}
	};

template <std::size_t I, typename Options>
struct strob_prefixed_serialized_size_impl<I, I, Options> {
	template <class T> static size_t size(T&&) noexcept { return 0; }
	};
}

template<typename _TxStrobValue,
	typename Options = none_options,
	typename TStrobValue = typename std::decay_t<_TxStrobValue>,
	typename std::enable_if_t<(
		detail::is_strob_flat_serializable_v<TStrobValue> &&
		detail::is_strob_prefixed_serialized_size_v<TStrobValue> &&
		!detail::is_i_srlzd_v<TStrobValue> &&
		!detail::is_serialized_trait_w_v<TStrobValue>
		), int> = 0>
size_t object_trait_serialized_size(_TxStrobValue&& value, Options = {}) {

	constexpr auto fieldsCount = detail::fields_count_v<TStrobValue>;
	auto strobTuple = detail::tie_as_tuple(std::forward<_TxStrobValue>(value));

	return detail::strob_prefixed_serialized_size_impl<0, fieldsCount, Options>::size(strobTuple);
	}
}


