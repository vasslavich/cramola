#pragma once


#include "./strob_base_tuple.h"


namespace crm::srlz::detail {


template<typename TFault>
struct __tie_as_tuple_element_fault : std::false_type {};


//template <int Idx, typename _Tag, class T >
//constexpr decltype(auto) __tie_as_tuple_element(_Tag, T&& /*val*/, size_t_<0>) noexcept {
//	return __tie_as_tuple_element_fault<T>::value;
//	}

template <int Idx, typename _Tag, class T/*,
	std::enable_if_t<std::is_class_v<std::decay_t<T>>>* = 0*/>
	constexpr decltype(auto) __tie_as_tuple_element(_Tag tag, T&& val, size_t_<1>)noexcept {
	static_assert(std::is_reference_v<T>, __FILE__);

	auto&& [a] = std::forward<T>(val);
	return tuple::get<Idx>(make_tuple_of_references(tag, a));
	}

//template <int Idx, typename _Tag, class T,
//	std::enable_if_t<!std::is_class_v<std::decay_t<T>>>* = 0>
//	constexpr decltype(auto) __tie_as_tuple_element(_Tag tag, T&& val, size_t_<1>)noexcept {
//	static_assert(std::is_reference_v<T>, __FILE__);
//
//	return tuple::get<Idx>(make_tuple_of_references(tag, val));
//	}

template<int Idx, typename _Tag, class T>
constexpr decltype(auto) __tie_as_tuple_element(_Tag tag, T&& val, size_t_<2>) noexcept {
	static_assert(std::is_reference_v<T>, __FILE__);

	auto&& [a, b] = std::forward<T>(val);
	return tuple::get<Idx>(make_tuple_of_references(tag, a, b));
	}

template<typename _Tag, class T>
constexpr decltype(auto) __tie_as_tuple_element(_Tag tag, T&& val, size_t_<3>) noexcept {
	static_assert(std::is_reference_v<T>, __FILE__);

	auto&& [a, b, c] = std::forward<T>(val);
	return make_tuple_of_references(tag, a, b, c);
	}

template<int Idx, typename _Tag, class T>
constexpr decltype(auto) __tie_as_tuple_element(_Tag tag, T&& val, size_t_<4>) noexcept {
	static_assert(std::is_reference_v<T>, __FILE__);

	auto&& [a, b, c, d] = std::forward<T>(val);
	return tuple::get<Idx>(make_tuple_of_references(tag, a, b, c, d));
	}
//
//template<int Idx, typename _Tag, class T>
//constexpr decltype(auto) __tie_as_tuple_element(_Tag tag, T&& val, size_t_<5>) noexcept {
//	static_assert(std::is_reference_v<T>, __FILE__);
//
//	auto&& [a, b, c, d, e] = std::forward<T>(val);
//	return make_tuple_of_references(tag, a, b, c, d, e);
//	}
//
//template<typename _Tag, class T, int Idx>
//constexpr decltype(auto) __tie_as_tuple_element(_Tag tag, T&& val, size_t_<6>) noexcept {
//	static_assert(std::is_reference_v<T>, __FILE__);
//
//	auto&& [a, b, c, d, e, f] = std::forward<T>(val);
//	return make_tuple_of_references(tag, a, b, c, d, e, f);
//	}
//
//template<typename _Tag, class T, int Idx>
//constexpr decltype(auto) __tie_as_tuple_element(_Tag tag, T&& val, size_t_<7>) noexcept {
//	static_assert(std::is_reference_v<T>, __FILE__);
//
//	auto&& [a, b, c, d, e, f, g] = std::forward<T>(val);
//	return make_tuple_of_references(tag, a, b, c, d, e, f, g);
//	}
//
//template<typename _Tag, class T, int Idx>
//constexpr decltype(auto) __tie_as_tuple_element(_Tag tag, T&& val, size_t_<8>) noexcept {
//	static_assert(std::is_reference_v<T>, __FILE__);
//
//	auto&& [a, b, c, d, e, f, g, h] = std::forward<T>(val);
//	return make_tuple_of_references(tag, a, b, c, d, e, f, g, h);
//	}
//
//template<typename _Tag, class T, int Idx>
//constexpr decltype(auto) __tie_as_tuple_element(_Tag tag, T&& val, size_t_<9>) noexcept {
//	static_assert(std::is_reference_v<T>, __FILE__);
//
//	auto&& [a, b, c, d, e, f, g, h, j] = std::forward<T>(val);
//	return make_tuple_of_references(tag, a, b, c, d, e, f, g, h, j);
//	}
//
//template<typename _Tag, class T, int Idx>
//constexpr decltype(auto) __tie_as_tuple_element(_Tag tag, T&& val, size_t_<10>) noexcept {
//	static_assert(std::is_reference_v<T>, __FILE__);
//
//	auto&& [a, b, c, d, e, f, g, h, j, k] = std::forward<T>(val);
//	return make_tuple_of_references(tag, a, b, c, d, e, f, g, h, j, k);
//	}
//
//template<typename _Tag, class T, int Idx>
//constexpr decltype(auto) __tie_as_tuple_element(_Tag tag, T&& val, size_t_<11>) noexcept {
//	static_assert(std::is_reference_v<T>, __FILE__);
//
//	auto&& [a, b, c, d, e, f, g, h, j, k, l] = std::forward<T>(val);
//	return make_tuple_of_references(tag, a, b, c, d, e, f, g, h, j, k, l);
//	}
//
//template<typename _Tag, class T, int Idx>
//constexpr decltype(auto) __tie_as_tuple_element(_Tag tag, T&& val, size_t_<12>) noexcept {
//	static_assert(std::is_reference_v<T>, __FILE__);
//
//	auto&& [a, b, c, d, e, f, g, h, j, k, l, m] = std::forward<T>(val);
//	return make_tuple_of_references(tag, a, b, c, d, e, f, g, h, j, k, l, m);
//	}
//
//template<typename _Tag, class T, int Idx>
//constexpr decltype(auto) __tie_as_tuple_element(_Tag tag, T&& val, size_t_<13>) noexcept {
//	static_assert(std::is_reference_v<T>, __FILE__);
//
//	auto&& [a, b, c, d, e, f, g, h, j, k, l, m, n] = std::forward<T>(val);
//	return make_tuple_of_references(tag, a, b, c, d, e, f, g, h, j, k, l, m, n);
//	}
//
//template<typename _Tag, class T, int Idx>
//constexpr decltype(auto) __tie_as_tuple_element(_Tag tag, T&& val, size_t_<14>) noexcept {
//	static_assert(std::is_reference_v<T>, __FILE__);
//
//	auto&& [a, b, c, d, e, f, g, h, j, k, l, m, n, p] = std::forward<T>(val);
//	return make_tuple_of_references(tag, a, b, c, d, e, f, g, h, j, k, l, m, n, p);
//	}
//
//template<typename _Tag, class T, int Idx>
//constexpr decltype(auto) __tie_as_tuple_element(_Tag tag, T&& val, size_t_<15>) noexcept {
//	static_assert(std::is_reference_v<T>, __FILE__);
//
//	auto&& [a, b, c, d, e, f, g, h, j, k, l, m, n, p, q] = std::forward<T>(val);
//	return make_tuple_of_references(tag, a, b, c, d, e, f, g, h, j, k, l, m, n, p, q);
//	}
//
//template<typename _Tag, class T, int Idx>
//constexpr decltype(auto) __tie_as_tuple_element(_Tag tag, T&& val, size_t_<16>) noexcept {
//	static_assert(std::is_reference_v<T>, __FILE__);
//
//	auto&& [a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r] = std::forward<T>(val);
//	return make_tuple_of_references(tag, a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r);
//	}
//
//template<typename _Tag, class T, int Idx>
//constexpr decltype(auto) __tie_as_tuple_element(_Tag tag, T&& val, size_t_<17>) noexcept {
//	static_assert(std::is_reference_v<T>, __FILE__);
//
//	auto&& [a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s] = std::forward<T>(val);
//	return make_tuple_of_references(tag, a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s);
//	}
//
//template<typename _Tag, class T, int Idx>
//constexpr decltype(auto) __tie_as_tuple_element(_Tag tag, T&& val, size_t_<18>) noexcept {
//	static_assert(std::is_reference_v<T>, __FILE__);
//
//	auto&& [a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s, t] = std::forward<T>(val);
//	return make_tuple_of_references(tag, a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s, t);
//	}
//
//template<typename _Tag, class T, int Idx>
//constexpr decltype(auto) __tie_as_tuple_element(_Tag tag, T&& val, size_t_<19>) noexcept {
//	static_assert(std::is_reference_v<T>, __FILE__);
//
//	auto&& [a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s, t, u] = std::forward<T>(val);
//	return make_tuple_of_references(tag, a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s, t, u);
//	}
//
//template<typename _Tag, class T, int Idx>
//constexpr decltype(auto) __tie_as_tuple_element(_Tag tag, T&& val, size_t_<20>) noexcept {
//	static_assert(std::is_reference_v<T>, __FILE__);
//
//	auto&& [a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s, t, u, v] = std::forward<T>(val);
//	return make_tuple_of_references(tag, a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s, t, u, v);
//	}
//
//template<typename _Tag, class T, int Idx>
//constexpr decltype(auto) __tie_as_tuple_element(_Tag tag, T&& val, size_t_<21>) noexcept {
//	static_assert(std::is_reference_v<T>, __FILE__);
//
//	auto&& [a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s, t, u, v, w] = std::forward<T>(val);
//	return make_tuple_of_references(tag, a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s, t, u, v, w);
//	}
//
//template<typename _Tag, class T, int Idx>
//constexpr decltype(auto) __tie_as_tuple_element(_Tag tag, T&& val, size_t_<22>) noexcept {
//	static_assert(std::is_reference_v<T>, __FILE__);
//
//	auto&& [a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s, t, u, v, w, x] = std::forward<T>(val);
//	return make_tuple_of_references(tag, a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s, t, u, v, w, x);
//	}
//
//template<typename _Tag, class T, int Idx>
//constexpr decltype(auto) __tie_as_tuple_element(_Tag tag, T&& val, size_t_<23>) noexcept {
//	static_assert(std::is_reference_v<T>, __FILE__);
//
//	auto&& [a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s, t, u, v, w, x, y] = std::forward<T>(val);
//	return make_tuple_of_references(tag, a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s, t, u, v, w, x, y);
//	}
//
//template<typename _Tag, class T, int Idx>
//constexpr decltype(auto) __tie_as_tuple_element(_Tag tag, T&& val, size_t_<24>) noexcept {
//	static_assert(std::is_reference_v<T>, __FILE__);
//
//	auto&& [a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s, t, u, v, w, x, y, z] = std::forward<T>(val);
//	return make_tuple_of_references(tag, a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s, t, u, v, w, x, y, z);
//	}
//
//template<typename _Tag, class T, int Idx>
//constexpr decltype(auto) __tie_as_tuple_element(_Tag tag, T&& val, size_t_<25>) noexcept {
//	static_assert(std::is_reference_v<T>, __FILE__);
//
//	auto&& [a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s, t, u, v, w, x, y, z,
//		A] = std::forward<T>(val);
//
//	return make_tuple_of_references(tag,
//		a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s, t, u, v, w, x, y, z,
//		A);
//	}
//
//template<typename _Tag, class T, int Idx>
//constexpr decltype(auto) __tie_as_tuple_element(_Tag tag, T&& val, size_t_<26>) noexcept {
//	static_assert(std::is_reference_v<T>, __FILE__);
//
//	auto&& [a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s, t, u, v, w, x, y, z,
//		A, B] = std::forward<T>(val);
//
//	return make_tuple_of_references(tag,
//		a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s, t, u, v, w, x, y, z,
//		A, B);
//	}
//
//template<typename _Tag, class T, int Idx>
//constexpr decltype(auto) __tie_as_tuple_element(_Tag tag, T&& val, size_t_<27>) noexcept {
//	static_assert(std::is_reference_v<T>, __FILE__);
//
//	auto&& [a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s, t, u, v, w, x, y, z,
//		A, B, C] = std::forward<T>(val);
//
//	return make_tuple_of_references(tag,
//		a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s, t, u, v, w, x, y, z,
//		A, B, C);
//	}
//
//template<typename _Tag, class T, int Idx>
//constexpr decltype(auto) __tie_as_tuple_element(_Tag tag, T&& val, size_t_<28>) noexcept {
//	static_assert(std::is_reference_v<T>, __FILE__);
//
//	auto&& [a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s, t, u, v, w, x, y, z,
//		A, B, C, D] = std::forward<T>(val);
//
//	return make_tuple_of_references(tag,
//		a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s, t, u, v, w, x, y, z,
//		A, B, C, D);
//	}
//
//template<typename _Tag, class T, int Idx>
//constexpr decltype(auto) __tie_as_tuple_element(_Tag tag, T&& val, size_t_<29>) noexcept {
//	static_assert(std::is_reference_v<T>, __FILE__);
//
//	auto&& [a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s, t, u, v, w, x, y, z,
//		A, B, C, D, E] = std::forward<T>(val);
//
//	return make_tuple_of_references(tag,
//		a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s, t, u, v, w, x, y, z,
//		A, B, C, D, E);
//	}
//
//template<typename _Tag, class T, int Idx>
//constexpr decltype(auto) __tie_as_tuple_element(_Tag tag, T&& val, size_t_<30>) noexcept {
//	static_assert(std::is_reference_v<T>, __FILE__);
//
//	auto&& [a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s, t, u, v, w, x, y, z,
//		A, B, C, D, E, F] = std::forward<T>(val);
//
//	return make_tuple_of_references(tag,
//		a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s, t, u, v, w, x, y, z,
//		A, B, C, D, E, F);
//	}
//
//template<typename _Tag, class T, int Idx>
//constexpr decltype(auto) __tie_as_tuple_element(_Tag tag, T&& val, size_t_<31>) noexcept {
//	static_assert(std::is_reference_v<T>, __FILE__);
//
//	auto&& [a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s, t, u, v, w, x, y, z,
//		A, B, C, D, E, F, G] = std::forward<T>(val);
//
//	return make_tuple_of_references(tag,
//		a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s, t, u, v, w, x, y, z,
//		A, B, C, D, E, F, G);
//	}
//
//template<typename _Tag, class T, int Idx>
//constexpr decltype(auto) __tie_as_tuple_element(_Tag tag, T&& val, size_t_<32>) noexcept {
//	static_assert(std::is_reference_v<T>, __FILE__);
//
//	auto&& [a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s, t, u, v, w, x, y, z,
//		A, B, C, D, E, F, G, H] = std::forward<T>(val);
//
//	return make_tuple_of_references(tag,
//		a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s, t, u, v, w, x, y, z,
//		A, B, C, D, E, F, G, H);
//	}
//
//template<typename _Tag, class T, int Idx>
//constexpr decltype(auto) __tie_as_tuple_element(_Tag tag, T&& val, size_t_<33>) noexcept {
//	static_assert(std::is_reference_v<T>, __FILE__);
//
//	auto&& [a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s, t, u, v, w, x, y, z,
//		A, B, C, D, E, F, G, H, J] = std::forward<T>(val);
//
//	return make_tuple_of_references(tag,
//		a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s, t, u, v, w, x, y, z,
//		A, B, C, D, E, F, G, H, J);
//	}
//
//template<typename _Tag, class T, int Idx>
//constexpr decltype(auto) __tie_as_tuple_element(_Tag tag, T&& val, size_t_<34>) noexcept {
//	static_assert(std::is_reference_v<T>, __FILE__);
//
//	auto&& [a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s, t, u, v, w, x, y, z,
//		A, B, C, D, E, F, G, H, J, K] = std::forward<T>(val);
//
//	return make_tuple_of_references(tag,
//		a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s, t, u, v, w, x, y, z,
//		A, B, C, D, E, F, G, H, J, K);
//	}
//
//template<typename _Tag, class T, int Idx>
//constexpr decltype(auto) __tie_as_tuple_element(_Tag tag, T&& val, size_t_<35>) noexcept {
//	static_assert(std::is_reference_v<T>, __FILE__);
//
//	auto&& [a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s, t, u, v, w, x, y, z,
//		A, B, C, D, E, F, G, H, J, K, L] = std::forward<T>(val);
//
//	return make_tuple_of_references(tag,
//		a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s, t, u, v, w, x, y, z,
//		A, B, C, D, E, F, G, H, J, K, L);
//	}
//
//template<typename _Tag, class T, int Idx>
//constexpr decltype(auto) __tie_as_tuple_element(_Tag tag, T&& val, size_t_<36>) noexcept {
//	static_assert(std::is_reference_v<T>, __FILE__);
//
//	auto&& [a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s, t, u, v, w, x, y, z,
//		A, B, C, D, E, F, G, H, J, K, L, M] = std::forward<T>(val);
//
//	return make_tuple_of_references(tag,
//		a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s, t, u, v, w, x, y, z,
//		A, B, C, D, E, F, G, H, J, K, L, M);
//	}
//
//template<typename _Tag, class T, int Idx>
//constexpr decltype(auto) __tie_as_tuple_element(_Tag tag, T&& val, size_t_<37>) noexcept {
//	static_assert(std::is_reference_v<T>, __FILE__);
//
//	auto&& [a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s, t, u, v, w, x, y, z,
//		A, B, C, D, E, F, G, H, J, K, L, M, N] = std::forward<T>(val);
//
//	return make_tuple_of_references(tag,
//		a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s, t, u, v, w, x, y, z,
//		A, B, C, D, E, F, G, H, J, K, L, M, N);
//	}
//
//template<typename _Tag, class T, int Idx>
//constexpr decltype(auto) __tie_as_tuple_element(_Tag tag, T&& val, size_t_<38>) noexcept {
//	static_assert(std::is_reference_v<T>, __FILE__);
//
//	auto&& [a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s, t, u, v, w, x, y, z,
//		A, B, C, D, E, F, G, H, J, K, L, M, N, P] = std::forward<T>(val);
//
//	return make_tuple_of_references(tag,
//		a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s, t, u, v, w, x, y, z,
//		A, B, C, D, E, F, G, H, J, K, L, M, N, P);
//	}
//
//template<typename _Tag, class T, int Idx>
//constexpr decltype(auto) __tie_as_tuple_element(_Tag tag, T&& val, size_t_<39>) noexcept {
//	static_assert(std::is_reference_v<T>, __FILE__);
//
//	auto&& [a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s, t, u, v, w, x, y, z,
//		A, B, C, D, E, F, G, H, J, K, L, M, N, P, Q] = std::forward<T>(val);
//
//	return make_tuple_of_references(tag,
//		a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s, t, u, v, w, x, y, z,
//		A, B, C, D, E, F, G, H, J, K, L, M, N, P, Q);
//	}
//
//template<typename _Tag, class T, int Idx>
//constexpr decltype(auto) __tie_as_tuple_element(_Tag tag, T&& val, size_t_<40>) noexcept {
//	static_assert(std::is_reference_v<T>, __FILE__);
//
//	auto&& [a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s, t, u, v, w, x, y, z,
//		A, B, C, D, E, F, G, H, J, K, L, M, N, P, Q, R] = std::forward<T>(val);
//
//	return make_tuple_of_references(tag,
//		a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s, t, u, v, w, x, y, z,
//		A, B, C, D, E, F, G, H, J, K, L, M, N, P, Q, R);
//	}
//
//template<typename _Tag, class T, int Idx>
//constexpr decltype(auto) __tie_as_tuple_element(_Tag tag, T&& val, size_t_<41>) noexcept {
//	static_assert(std::is_reference_v<T>, __FILE__);
//
//	auto&& [a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s, t, u, v, w, x, y, z,
//		A, B, C, D, E, F, G, H, J, K, L, M, N, P, Q, R, S] = std::forward<T>(val);
//
//	return make_tuple_of_references(tag,
//		a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s, t, u, v, w, x, y, z,
//		A, B, C, D, E, F, G, H, J, K, L, M, N, P, Q, R, S);
//	}
//
//template<typename _Tag, class T, int Idx>
//constexpr decltype(auto) __tie_as_tuple_element(_Tag tag, T&& val, size_t_<42>) noexcept {
//	static_assert(std::is_reference_v<T>, __FILE__);
//
//	auto&& [a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s, t, u, v, w, x, y, z,
//		A, B, C, D, E, F, G, H, J, K, L, M, N, P, Q, R, S, U] = std::forward<T>(val);
//
//	return make_tuple_of_references(tag,
//		a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s, t, u, v, w, x, y, z,
//		A, B, C, D, E, F, G, H, J, K, L, M, N, P, Q, R, S, U);
//	}
//
//template<typename _Tag, class T, int Idx>
//constexpr decltype(auto) __tie_as_tuple_element(_Tag tag, T&& val, size_t_<43>) noexcept {
//	static_assert(std::is_reference_v<T>, __FILE__);
//
//	auto&& [a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s, t, u, v, w, x, y, z,
//		A, B, C, D, E, F, G, H, J, K, L, M, N, P, Q, R, S, U, V] = std::forward<T>(val);
//
//	return make_tuple_of_references(tag,
//		a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s, t, u, v, w, x, y, z,
//		A, B, C, D, E, F, G, H, J, K, L, M, N, P, Q, R, S, U, V);
//	}
//
//template<typename _Tag, class T, int Idx>
//constexpr decltype(auto) __tie_as_tuple_element(_Tag tag, T&& val, size_t_<44>) noexcept {
//	static_assert(std::is_reference_v<T>, __FILE__);
//
//	auto&& [a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s, t, u, v, w, x, y, z,
//		A, B, C, D, E, F, G, H, J, K, L, M, N, P, Q, R, S, U, V, W] = std::forward<T>(val);
//
//	return make_tuple_of_references(tag,
//		a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s, t, u, v, w, x, y, z,
//		A, B, C, D, E, F, G, H, J, K, L, M, N, P, Q, R, S, U, V, W);
//	}
//
//template<typename _Tag, class T, int Idx>
//constexpr decltype(auto) __tie_as_tuple_element(_Tag tag, T&& val, size_t_<45>) noexcept {
//	static_assert(std::is_reference_v<T>, __FILE__);
//
//	auto&& [a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s, t, u, v, w, x, y, z,
//		A, B, C, D, E, F, G, H, J, K, L, M, N, P, Q, R, S, U, V, W, X] = std::forward<T>(val);
//
//	return make_tuple_of_references(tag,
//		a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s, t, u, v, w, x, y, z,
//		A, B, C, D, E, F, G, H, J, K, L, M, N, P, Q, R, S, U, V, W, X);
//	}
//
//template<typename _Tag, class T, int Idx>
//constexpr decltype(auto) __tie_as_tuple_element(_Tag tag, T&& val, size_t_<46>) noexcept {
//	static_assert(std::is_reference_v<T>, __FILE__);
//
//	auto&& [a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s, t, u, v, w, x, y, z,
//		A, B, C, D, E, F, G, H, J, K, L, M, N, P, Q, R, S, U, V, W, X, Y] = std::forward<T>(val);
//
//	return make_tuple_of_references(tag,
//		a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s, t, u, v, w, x, y, z,
//		A, B, C, D, E, F, G, H, J, K, L, M, N, P, Q, R, S, U, V, W, X, Y);
//	}
//
//template<typename _Tag, class T, int Idx>
//constexpr decltype(auto) __tie_as_tuple_element(_Tag tag, T&& val, size_t_<47>) noexcept {
//	static_assert(std::is_reference_v<T>, __FILE__);
//
//	auto&& [a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s, t, u, v, w, x, y, z,
//		A, B, C, D, E, F, G, H, J, K, L, M, N, P, Q, R, S, U, V, W, X, Y, Z] = std::forward<T>(val);
//
//	return make_tuple_of_references(tag,
//		a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s, t, u, v, w, x, y, z,
//		A, B, C, D, E, F, G, H, J, K, L, M, N, P, Q, R, S, U, V, W, X, Y, Z);
//	}
//
//template<typename _Tag, class T, int Idx>
//constexpr decltype(auto) __tie_as_tuple_element(_Tag tag, T&& val, size_t_<48>) noexcept {
//	static_assert(std::is_reference_v<T>, __FILE__);
//
//	auto&& [a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s, t, u, v, w, x, y, z,
//		A, B, C, D, E, F, G, H, J, K, L, M, N, P, Q, R, S, U, V, W, X, Y, Z,
//		aa] = std::forward<T>(val);
//
//	return make_tuple_of_references(tag,
//		a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s, t, u, v, w, x, y, z,
//		A, B, C, D, E, F, G, H, J, K, L, M, N, P, Q, R, S, U, V, W, X, Y, Z,
//		aa);
//	}
//
//template<typename _Tag, class T, int Idx>
//constexpr decltype(auto) __tie_as_tuple_element(_Tag tag, T&& val, size_t_<49>) noexcept {
//	static_assert(std::is_reference_v<T>, __FILE__);
//
//	auto&& [a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s, t, u, v, w, x, y, z,
//		A, B, C, D, E, F, G, H, J, K, L, M, N, P, Q, R, S, U, V, W, X, Y, Z,
//		aa, ab] = std::forward<T>(val);
//
//	return make_tuple_of_references(tag,
//		a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s, t, u, v, w, x, y, z,
//		A, B, C, D, E, F, G, H, J, K, L, M, N, P, Q, R, S, U, V, W, X, Y, Z,
//		aa, ab);
//	}
//
//template<typename _Tag, class T, int Idx>
//constexpr decltype(auto) __tie_as_tuple_element(_Tag tag, T&& val, size_t_<50>) noexcept {
//	static_assert(std::is_reference_v<T>, __FILE__);
//
//	auto&& [a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s, t, u, v, w, x, y, z,
//		A, B, C, D, E, F, G, H, J, K, L, M, N, P, Q, R, S, U, V, W, X, Y, Z,
//		aa, ab, ac] = std::forward<T>(val);
//
//	return make_tuple_of_references(tag,
//		a, b, c, d, e, f, g, h, j, k, l, m, n, p, q, r, s, t, u, v, w, x, y, z,
//		A, B, C, D, E, F, G, H, J, K, L, M, N, P, Q, R, S, U, V, W, X, Y, Z,
//		aa, ab, ac);
//	}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


template <int Idx, typename T>
constexpr decltype(auto) tie_as_tuple_element(T && val) noexcept {
	using fc_tag = size_t_<fields_count_v<T>>;
	using cv_tag = typename detect_argument_type<T>::category_type;
	using base_type = typename std::decay_t<T>;

	return tuple::get<Idx>(__tie_as_tuple_element(cv_tag{}, val, size_t_<3>{}/* fc_tag{}*/));
	}
}


