#pragma once


#include <type_traits>
#include "../../base_typetrait_decls.h"
#include "../../base_typetrait_using.h"
#include "./strob_detect_fields_count.h"
#include "./strob_object2tuple.h"


namespace crm {
namespace srlz {
namespace detail {

template <std::size_t I, std::size_t N, typename Options>
struct deserialize_impl {
	template <class Stream, class T>
	static void read( Stream && out, T & value) {

		static_assert(is_trait_as_options_v<Options>, __FILE_LINE__);
		static_assert(!std::is_const_v<std::remove_reference_t<decltype(value)>>, __FILE_LINE__);
		static_assert(!std::is_const_v<decltype(value)>, __FILE_LINE__);


		auto && t = tuple::get<I>(value);
		static_assert(!std::is_const_v<std::remove_reference_t<decltype(t)>>, __FILE_LINE__);
		static_assert(!std::is_const_v<decltype(t)>, __FILE_LINE__);

		deserialize(std::forward<Stream>(out), t, serialized_options_cond<decltype(t), Options>{});
		deserialize_impl<I + 1, N, Options>::read(std::forward<Stream>(out), value);
		}
	};

template <std::size_t I, typename Options>
struct deserialize_impl<I, I, Options> {
	template <class Stream, class T> static void read(Stream&&, T&) noexcept {}
	};
}

template<class TStream,
	typename _TxStrobValue,
	typename Options = none_options,
	typename TStrobValue = typename std::decay_t<_TxStrobValue>,
	typename std::enable_if_t<(
		detail::is_strob_flat_serializable_v<TStrobValue> && 
		! detail::is_serialized_trait_r_v<TStrobValue> &&
		! detail::is_i_srlzd_v<TStrobValue>), int> = 0>
void deserialize(TStream&& out, _TxStrobValue& value, Options = {}) {

	static_assert(is_trait_as_options_v<Options>, __FILE_LINE__);
	static_assert(!std::is_const_v<std::remove_reference_t<decltype(value)>>, __FILE_LINE__);
	static_assert(!std::is_const_v<decltype(value)>, __FILE_LINE__);

	constexpr auto fieldsCount = detail::fields_count_v<TStrobValue>;
	auto && strobTuple = detail::tie_as_tuple(value);

	static_assert(!std::is_const_v<std::remove_reference_t<decltype(strobTuple)>>, __FILE_LINE__);
	static_assert(!std::is_const_v<decltype(strobTuple)>, __FILE_LINE__);

	detail::deserialize_impl<0, fieldsCount, Options>::read(std::forward<TStream>(out), strobTuple);
	}
}
}

