#pragma once


#include <type_traits>
#include "../../type_trait_base.h"


namespace crm {
namespace srlz {
namespace detail {

template <const std::size_t Index>
using size_t_ = std::integral_constant<std::size_t, Index >;

template<typename T>
struct fault_detect_argument_type : std::false_type {};

enum class __tuple_of_references_t {
	is_const_lvalue,
	is_no_const_lvalue
	};


template <const __tuple_of_references_t tag>
using tuple_of_references_t = std::integral_constant<__tuple_of_references_t, tag>;

template<typename T>
constexpr decltype(auto) detect_argument_type_r(T&&)noexcept {
	if constexpr (!std::is_const_v<std::remove_reference_t<T>>) {
		return tuple_of_references_t< __tuple_of_references_t::is_no_const_lvalue>{};
		}
	else if constexpr (std::is_const_v<std::remove_reference_t<T>>) {
		return  tuple_of_references_t< __tuple_of_references_t::is_const_lvalue>{};
		}
	else {
		static_assert(fault_detect_argument_type<T>::value, __FILE_LINE__);
		}
	}

template<typename T>
struct detect_argument_type {
	using category_type = typename std::decay_t<decltype(detect_argument_type_r(std::declval<T>()))>;
	};


namespace tuple {


template <std::size_t Index>
using index_t_ = std::integral_constant<std::size_t, Index >;


template <std::size_t N, class T>
struct base_from_member {
	static_assert(std::is_reference_v<T>, __FILE__);

	using type = typename T;
	type value;
	};

template <class I, class ...Tail>
struct tuple_base;



template <std::size_t... I, class ...Tail>
struct tuple_base< std::index_sequence<I...>, Tail... >
	: base_from_member<I, Tail>...
	{
	static constexpr std::size_t size_v = sizeof...(I);

	// We do not use `noexcept` in the following functions, because if user forget to put one then clang will issue an error:
	// "error: exception specification of explicitly defaulted default constructor does not match the calculated one".
	constexpr tuple_base() = default;
	constexpr tuple_base(tuple_base&&) = default;
	constexpr tuple_base(const tuple_base&) = default;

	constexpr tuple_base(Tail&& ... v) noexcept
		: base_from_member<I, Tail>{ std::forward<Tail>(v) }...
		{}
	};

template <>
struct tuple_base<std::index_sequence<> > {
	static constexpr std::size_t size_v = 0;
	};

template <std::size_t N, class T>
constexpr T& get_impl(base_from_member<N, T>& t) noexcept {
	return t.value;
	}

template <std::size_t N, class T>
constexpr const T& get_impl(const base_from_member<N, T>& t) noexcept {
	return t.value;
	}

template <std::size_t N, class T>
constexpr volatile T& get_impl(volatile base_from_member<N, T>& t) noexcept {
	return t.value;
	}

template <std::size_t N, class T>
constexpr const volatile T& get_impl(const volatile base_from_member<N, T>& t) noexcept {
	return t.value;
	}

template <std::size_t N, class T>
constexpr T&& get_impl(base_from_member<N, T>&& t) noexcept {
	return std::forward<T>(t.value);
	}


template <class ...Values>
struct strob_tuple : tuple_base<
	std::make_index_sequence<sizeof...(Values)>,
	Values...> {

	using tuple_base<
		std::make_index_sequence<sizeof...(Values)>,
		Values...
	>::tuple_base;
	};


template <std::size_t N, class ...T>
constexpr decltype(auto) get(strob_tuple<T...>& t) noexcept {
	static_assert(N < strob_tuple<T...>::size_v, "Tuple index out of bounds");
	return get_impl<N>(t);
	}

template <std::size_t N, class ...T>
constexpr decltype(auto) get(const strob_tuple<T...>& t) noexcept {
	static_assert(N < strob_tuple<T...>::size_v, "Tuple index out of bounds");
	return get_impl<N>(t);
	}

template <std::size_t N, class ...T>
constexpr decltype(auto) get(const volatile strob_tuple<T...>& t) noexcept {
	static_assert(N < strob_tuple<T...>::size_v, "Tuple index out of bounds");
	return get_impl<N>(t);
	}

template <std::size_t N, class ...T>
constexpr decltype(auto) get(volatile strob_tuple<T...>& t) noexcept {
	static_assert(N < strob_tuple<T...>::size_v, "Tuple index out of bounds");
	return get_impl<N>(t);
	}

template <std::size_t N, class ...T>
constexpr decltype(auto) get(strob_tuple<T...>&& t) noexcept {
	static_assert(N < strob_tuple<T...>::size_v, "Tuple index out of bounds");
	return get_impl<N>(std::move(t));
	}

template <std::size_t I, class T>
using tuple_element = std::remove_reference< decltype(get<I>(std::declval<T>()))>;
}


template <class... Args>
constexpr decltype(auto) make_tuple_of_references(tuple_of_references_t<__tuple_of_references_t::is_const_lvalue>,
	Args&&... args) noexcept {

	return tuple::strob_tuple<std::add_lvalue_reference_t<std::add_const_t<std::decay_t<Args>>> ...>{std::forward<Args>(args)...};
	}

template <class... Args>
constexpr decltype(auto) make_tuple_of_references(tuple_of_references_t<__tuple_of_references_t::is_no_const_lvalue>,
	Args&&... args) noexcept {

	return tuple::strob_tuple<std::add_lvalue_reference_t<std::decay_t<Args>> ...>{std::forward<Args>(args)...};
	}
}
}
}

