#pragma once


#include <type_traits>
#include "./strob_trait_terms.h"


namespace crm {
namespace srlz {
namespace detail {



///////////////////// General utility stuff


///////////////////// Structure that can be converted to reference to anything
struct ubiq_lref_constructor {
	std::size_t ignore;
	template <class Type> /*constexpr*/ operator Type&() const noexcept; // Undefined, allows initialization of reference fields (T& and const T&)
	};

///////////////////// Structure that can be converted to rvalue reference to anything
struct ubiq_rref_constructor {
	std::size_t ignore;
	template <class Type> /*constexpr*/ operator Type && () const noexcept; // Undefined, allows initialization of rvalue reference fields and move-only types
	};

///////////////////// Structure that can be converted to reference to anything except reference to T
template <class T, bool IsCopyConstructible>
struct ubiq_constructor_except {
	template <class Type> constexpr operator std::enable_if_t<!std::is_same<T, Type>::value, Type&>() const noexcept; // Undefined
	};

template <class T>
struct ubiq_constructor_except<T, false> {
	template <class Type> constexpr operator std::enable_if_t<!std::is_same<T, Type>::value, Type&&>() const noexcept; // Undefined
	};

///////////////////// Hand-made is_aggregate_initializable_n<T> trait

// `std::is_constructible<T, ubiq_constructor_except<T>>` consumes a lot of time, so we made a separate lazy trait for it.
template <std::size_t N, class T> struct is_single_field_and_aggregate_initializable : std::false_type {};
template <class T> struct is_single_field_and_aggregate_initializable<1, T> : std::integral_constant<
	bool, !std::is_constructible<T, ubiq_constructor_except<T, std::is_copy_constructible<T>::value>>::value
> {};

// Hand-made is_aggregate<T> trait:
// Aggregates could be constructed from `decltype(ubiq_constructor{I})...` but report that there's no constructor from `decltype(ubiq_constructor{I})...`
// Special case for N == 1: `std::is_constructible<T, ubiq_constructor>` returns true if N == 1 and T is copy/move constructible.
template <class T, std::size_t N>
struct is_aggregate_initializable_n {
	template <std::size_t ...I>
	static constexpr bool is_not_constructible_n(std::index_sequence<I...>) noexcept {
		return (!std::is_constructible < T, decltype(ubiq_lref_constructor{ I })... > ::value && !std::is_constructible < T, decltype(ubiq_rref_constructor{ I })... > ::value)
			|| is_single_field_and_aggregate_initializable<N, T>::value
			;
		}

	static constexpr bool value =
		std::is_empty<T>::value
		|| std::is_fundamental<T>::value
		|| std::is_array_v<T>
		|| is_not_constructible_n(std::make_index_sequence<N>{})
		;
	};

///////////////////// Helper for SFINAE on fields count

template <class T, std::size_t... I, class /*Enable*/ = typename std::enable_if<std::is_copy_constructible<T>::value>::type>
constexpr auto enable_if_constructible_helper(std::index_sequence<I...>) noexcept
-> typename std::add_pointer<decltype(T{ ubiq_lref_constructor{ I }... }) > ::type;

template <class T, std::size_t... I, class /*Enable*/ = typename std::enable_if<!std::is_copy_constructible<T>::value>::type>
constexpr auto enable_if_constructible_helper(std::index_sequence<I...>) noexcept
-> typename std::add_pointer<decltype(T{ ubiq_rref_constructor{ I }... }) > ::type;


template<typename T, size_t N, typename = std::void_t<> >
struct if_constructible_helper_t : std::false_type{};

template<typename T, size_t N>
struct if_constructible_helper_t<T, N,
	std::void_t< decltype(enable_if_constructible_helper<T>(std::make_index_sequence<N>()))>> : std::true_type{};



///////////////////// Non greedy fields count search. Templates instantiation depth is log(sizeof(T)), templates instantiation count is log(sizeof(T)).
template <class T, std::size_t N>
constexpr std::size_t detect_fields_count(size_t_<N>, size_t_<N>, long) noexcept {
	return N;
	}

template <class T, std::size_t Begin, std::size_t Middle>
constexpr std::size_t detect_fields_count(size_t_<Begin>, size_t_<Middle>, int) noexcept;

template <class T, std::size_t Begin, std::size_t Middle, typename std::enable_if_t<if_constructible_helper_t<T, Middle>::value, int> = 0>
constexpr size_t detect_fields_count(size_t_<Begin>, size_t_<Middle>, long) noexcept {
	using next_t = size_t_<Middle + (Middle - Begin + 1) / 2>;
	return detect_fields_count<T>(size_t_<Middle>{}, next_t{}, 1L);
	}

template <class T, std::size_t Begin, std::size_t Middle>
constexpr std::size_t detect_fields_count(size_t_<Begin>, size_t_<Middle>, int) noexcept {
	using next_t = size_t_<(Begin + Middle) / 2>;
	return detect_fields_count<T>(size_t_<Begin>{}, next_t{}, 1L);
	}

///////////////////// Greedy search. Templates instantiation depth is log(sizeof(T)), templates instantiation count is log(sizeof(T))*T in worst case.
template <class T, std::size_t N, typename std::enable_if_t<if_constructible_helper_t<T, N>::value, int> = 0>
constexpr std::size_t detect_fields_count_greedy_remember(size_t_<N>, long) noexcept {
	return N;
	}

template <class T, std::size_t N>
constexpr std::size_t detect_fields_count_greedy_remember(size_t_<N>, int) noexcept {
	return 0;
	}

template <class T, std::size_t N>
constexpr std::size_t detect_fields_count_greedy(size_t_<N>, size_t_<N>) noexcept {
	return detect_fields_count_greedy_remember<T>(size_t_<N>{}, 1L);
	}

template <class T, std::size_t Begin, std::size_t Last>
constexpr std::size_t detect_fields_count_greedy(size_t_<Begin>, size_t_<Last>) noexcept {
	constexpr std::size_t middle = Begin + (Last - Begin) / 2;
	constexpr std::size_t fields_count_big = detect_fields_count_greedy<T>(size_t_<middle + 1>{}, size_t_<Last>{});
	constexpr std::size_t fields_count_small = detect_fields_count_greedy<T>(size_t_<Begin>{}, size_t_<fields_count_big ? Begin : middle>{});
	return fields_count_big ? fields_count_big : fields_count_small;
	}

///////////////////// Choosing between greedy and non greedy search.
//template <class T, std::size_t N>
//constexpr auto detect_fields_count_dispatch(size_t_<N>, long) noexcept
//-> typename std::enable_if<std::is_array<T>::value, std::size_t>::type {
//	return sizeof(T) / sizeof(typename std::remove_all_extents<T>::type);
//	}

template <class T, std::size_t N>
constexpr std::size_t detect_fields_count_dispatch(size_t_<N>, long) noexcept{
	return detect_fields_count<T>(size_t_<0>{}, size_t_<N / 2 + 1>{}, 1L);
	}

template <class T, std::size_t N>
constexpr std::size_t detect_fields_count_dispatch(size_t_<N>, int) noexcept {
	// T is not default aggregate initialzable. It means that at least one of the members is not default constructible,
	// so we have to check all the aggregate initializations for T up to N parameters and return the bigest succeeded
	// (we can not use binary search for detecting fields count).
	return detect_fields_count_greedy<T>(size_t_<0>{}, size_t_<N>{});
	}


template<typename _Tx,
	typename T = std::decay_t<_Tx>,
	typename std::enable_if_t<is_strob_flat_serializable_v<T>, int> = 0 >
struct __fields_count_hepler_t {
	using type = typename std::decay_t<T>;

	static constexpr std::size_t count = detect_fields_count_dispatch<type>(size_t_<(sizeof(type) * 8)>{}, 1L);
	};


template<typename T, class Enable = void>
struct __fields_count_t : public std::false_type{
	static constexpr std::size_t count = 0;
	};




template<typename T>
struct is_aggregate_initializable_t<T,
	typename std::enable_if_t<is_aggregate_initializable_n<typename __fields_count_hepler_t<T>::type, __fields_count_hepler_t<T>::count>::value>> : std::true_type{};




template<typename T>
struct __fields_count_t<T, 
	std::enable_if_t<(
		is_strob_flat_serializable_v<T>
		&& is_aggregate_initializable_n<typename __fields_count_hepler_t<T>::type, __fields_count_hepler_t<T>::count>::value)>> : public std::true_type{

	static constexpr auto count = __fields_count_hepler_t<T>::count;
	};


template<typename T>
struct fields_count_t {
	//static_assert(__fields_count_t<T>::value, "fail checking type supported fields countering" );

	static constexpr auto value = __fields_count_t<T>::count;
	};

template<typename T>
constexpr std::size_t fields_count_v = fields_count_t<T>::value;
}
}
}
