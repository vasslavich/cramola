#pragma once


#include <type_traits>
#include "../../base_typetrait_decls.h"
#include "../../base_typetrait_using.h"
#include "./strob_detect_fields_count.h"
#include "./strob_object2tuple.h"


namespace crm {
namespace srlz {
namespace detail {

template <std::size_t I, std::size_t N, typename Options>
struct serialize_impl {
	template <class Stream, class T>
	static void write(Stream && out, T && value) {
		
		auto && item = tuple::get<I>(std::forward<T>(value));
		static_assert(!std::is_rvalue_reference_v<decltype(item)>, __FILE__);

		serialize(std::forward<Stream>(out), item, serialized_options_cond<decltype(item), Options>{});
		serialize_impl<I + 1, N, Options>::write(std::forward<Stream>(out), std::forward<T>(value));
		}
	};

template <std::size_t I, typename Options>
struct serialize_impl<I, I, Options> {
	template <class Stream, class T> static void write(Stream &&, T&&) noexcept {}
	};
}

template<class TStream,
	typename _TxStrobValue,
	typename Options = none_options,
	typename TStrobValue = typename std::decay_t<_TxStrobValue>,
	typename std::enable_if_t<(
		detail::is_strob_flat_serializable_v<TStrobValue> &&
		!detail::is_i_srlzd_v<TStrobValue> && 
		!detail::is_serialized_trait_w_v<TStrobValue>), int> = 0>
void serialize(TStream&& out, _TxStrobValue&& value, Options = {}) {
	static_assert(detail::is_strob_flat_serializable_v<TStrobValue>, __FILE__);

	constexpr auto fieldsCount = detail::fields_count_v<TStrobValue>;
	auto strobTuple = detail::tie_as_tuple(std::forward<_TxStrobValue>(value));

#ifdef USE_DCF_CHECK
	auto off = out.offset();
#endif

	detail::serialize_impl<0, fieldsCount, Options>::write(std::forward<TStream>(out), strobTuple);

#ifdef USE_DCF_CHECK
	CBL_VERIFY((out.offset() - off) == object_trait_serialized_size(value, serialized_options_cond<decltype(value), Options>{}));
#endif
	}
}
}

