#pragma once


#include <type_traits>
#include <functional>
#include "../../../internal_invariants.h"


namespace crm{
namespace srlz{
namespace detail {


#ifdef CBL_USE_BIG_LITTLE_ENDIAN
template< typename TSrc, typename Tvalue >
void sread_cvt_little_endian(typename TSrc&& src, typename Tvalue* pval) {
	static_assert(std::is_pod<Tvalue>::value, __FILE__);

	uint8_t* bV = (uint8_t*)pval;
	for (max_serialized_length_type i = 0; i < sizeof(Tvalue); ++i)
		bV[i] = src();
	}

template< typename TSrc, typename Tvalue >
void sread_cvt_big_endian(typename TSrc&& src, typename Tvalue* pval) {
	static_assert(std::is_pod<Tvalue>::value, __FILE__);

	uint8_t* bV = (uint8_t*)pval;
	for (max_serialized_length_type i = 0; i < sizeof(Tvalue); ++i)
		bV[sizeof(Tvalue) - 1 - i] = src();
	}


template< typename TSrc, typename Tvalue >
void sread_cvt(typename TSrc&& src, typename Tvalue* pval) {

	if (crm::sysenv::sys_is_little_endian()) {
		sread_cvt_little_endian(std::forward<TSrc>(src), pval);
		}
	else {
		sread_cvt_big_endian(std::forward<TSrc>(src), pval);
		}
	}


template< typename TSrc, typename Tvalue >
void sread_cvt_pmem_little_endian(typename TSrc&& src, typename Tvalue* pval) {
	static_assert(std::is_pod<Tvalue>::value, __FILE__);

	uint8_t* bV = (uint8_t*)pval->pmobject();
	for (max_serialized_length_type i = 0; i < pval->pmsize(); ++i)
		bV[i] = src();
	}

template< typename TSrc, typename Tvalue >
void sread_cvt_pmem_big_endian(typename TSrc&& src, typename Tvalue* pval) {
	static_assert(std::is_pod<Tvalue>::value, __FILE__);

	uint8_t* bV = (uint8_t*)pval->pmobject();
	for (max_serialized_length_type i = 0; i < pval->pmsize(); ++i)
		bV[sizeof(Tvalue) - 1 - i] = src();
	}


template< typename TSrc, typename Tvalue >
void sread_cvt_pmem(typename TSrc&& src, typename Tvalue* pval) {

	if (crm::sysenv::sys_is_little_endian()) {
		sread_cvt_pmem_little_endian(std::forward<TSrc>(src), pval);
		}
	else {
		sread_cvt_pmem_big_endian(std::forward<TSrc>(src), pval);
		}
	}
#endif//CBL_USE_BIG_LITTLE_ENDIAN


template< typename Ts,
	typename Tvalue,
	typename Options = none_options,
	typename std::enable_if_t< (
		is_mono_serializable_v<Tvalue>
		&& !is_byte_v<Tvalue>
		), int> = 0 >
	void sread(Ts&& src, Tvalue&& dest, Options = {}) {

	static_assert(is_trait_as_options_v<Options>, __FILE_LINE__);
	static_assert(sizeof(dest) > 1, __FILE__);
	static_assert(!std::is_const_v<decltype(dest)>, __FILE__);

#ifdef CBL_USE_BIG_LITTLE_ENDIAN

	sread_cvt([&src]() {
		uint8_t byte;
		if (!src.pop(byte))
			SERIALIZE_THROW_EXC_FWD(nullptr);
		return byte;
		}, &dest);

#else//CBL_USE_BIG_LITTLE_ENDIAN

	binary_iterator_adapter_t<Tvalue> adpt{};
	static_assert(is_contiguous_iterator_v<decltype(adpt.cbegin())>, __FILE__);
	static_assert(is_container_contiguous_of_bytes_v<decltype(adpt)>, __FILE__);

	if (adpt.size() == std::forward<Ts>(src).read(adpt)) {
		std::forward<Tvalue>(dest) = adpt.base_value();
		}
	else {
		SERIALIZE_THROW_EXC_FWD("read fail");
		}

#endif//CBL_USE_BIG_LITTLE_ENDIAN
	}

template< typename Ts,
	typename Tvalue,
	typename Options = none_options,
	typename std::enable_if_t<(
		is_mono_serializable_v<Tvalue>
		&& is_byte_v<Tvalue>
		), int> = 0 >
	void sread(Ts&& src, Tvalue&& value, Options = {}) {

	static_assert(is_trait_as_options_v<Options>, __FILE_LINE__);
	static_assert(sizeof(value) == 1, __FILE__);
	static_assert(!std::is_const_v<decltype(value)>, __FILE__);

	if (!std::forward<Ts>(src).read(std::forward<Tvalue>(value))) {
		SERIALIZE_THROW_EXC_FWD(nullptr);
		}
	}

template< typename Ts, typename Options = none_options>
void sread(Ts&& src, bool& value, Options = {}) {
	static_assert(is_trait_as_options_v<Options>, __FILE_LINE__);

	std::uint8_t ub;
	if (src.read(ub)) {
		if (ub <= 1)
			value = ub == 1 ? true : false;
		else {
			SERIALIZE_THROW_EXC_FWD(nullptr);
			}
		}
	else {
		SERIALIZE_THROW_EXC_FWD(nullptr);
		}
	}

template<typename Ts,
	typename TOutIt,
	typename Options = none_options,
	typename std::enable_if_t<(
		is_iterator_mono_v<TOutIt>
		&& is_input_iterator_v<TOutIt>
		&& !is_iterator_byte_v<TOutIt>), int> = 0>
	size_t sread_range(Ts&& src,
		TOutIt first,
		TOutIt last,
		TOutIt& next,
		Options = {}) {

	static_assert(is_trait_as_options_v<Options>, __FILE_LINE__);
	static_assert(sizeof(decltype((*first))) > 1, __FILE__);
	static_assert(is_mono_serializable_v<std::decay_t<decltype(*(std::declval<TOutIt>()))>>, __FILE__);
	static_assert(!(std::is_const_v<decltype(first)> || std::is_const_v<decltype(next)>), __FILE__);

	size_t c{ 0 };
	auto it = first;
	for (; it != last && !std::forward<Ts>(src).eof(); ++it, ++c) {
		sread(std::forward<Ts>(src), (*it), serialized_options_cond<decltype(*it), Options>{});
		}

	next = it;
	return c;
	}

template<typename Ts,
	typename TOutIt,
	typename Options = none_options,
	typename std::enable_if_t<(
		is_iterator_strob_v<TOutIt>
		&& is_input_iterator_v<TOutIt>), int> = 0>
	size_t sread_range(Ts&& src,
		TOutIt first,
		TOutIt last,
		TOutIt& next,
		Options = {}) {

	static_assert(is_trait_as_options_v<Options>, __FILE_LINE__);
	static_assert(is_strob_flat_serializable_v<std::decay_t<decltype(*(std::declval<TOutIt>()))>>, __FILE__);
	static_assert(!(std::is_const_v<decltype(first)> || std::is_const_v<decltype(next)>), __FILE__);

	auto it = first;
	size_t c{ 0 };
	for (; it != last && !std::forward<Ts>(src).eof(); ++it, ++c) {
		deserialize(std::forward<Ts>(src), (*it), serialized_options_cond<decltype(*it), Options>{});
		}

	next = it;
	return c;
	}

template<typename Ts,
	typename TOutIt,
	typename Options = none_options,
	typename std::enable_if_t<(
		!is_iterator_strob_v<TOutIt> &&
		!is_iterator_mono_v<TOutIt> &&
		is_input_iterator_v<TOutIt>), int> = 0>
	size_t sread_range(Ts&& src,
		TOutIt first,
		TOutIt last,
		TOutIt& next,
		Options = {}) {

	static_assert(is_trait_as_options_v<Options>, __FILE_LINE__);
	static_assert(!(std::is_const_v<decltype(first)> || std::is_const_v<decltype(next)>), __FILE__);

	auto it = first;
	size_t c{ 0 };
	for(; it != last && !std::forward<Ts>(src).eof(); ++it, ++c) {
		deserialize(std::forward<Ts>(src), (*it), serialized_options_cond<decltype(*it), Options>{});
		}

	next = it;
	return c;
	}

template<typename Ts,
	typename TOutIt,
	typename Options = none_options,
	typename std::enable_if_t<(
		is_iterator_byte_v<TOutIt>
		&& is_input_iterator_v<TOutIt>), int> = 0 >
	size_t sread_range(Ts&& src,
		TOutIt first,
		TOutIt last,
		TOutIt& next,
		Options = {}) {

	static_assert(is_trait_as_options_v<Options>, __FILE_LINE__);
	static_assert(sizeof(decltype(*first)) == 1, __FILE__);
	static_assert(is_byte_v<std::decay_t<decltype(*(std::declval<TOutIt>()))>>, __FILE__);
	static_assert(!(std::is_const_v<decltype(first)> || std::is_const_v<decltype(next)>), __FILE__);

	auto [itLast, c] = src.read(first, last);

	next = itLast;
	return c;
	}
}

template< typename Ts, 
	typename _TxMonoValue,
	typename Options = none_options,
	typename TMonoValue = typename std::decay_t<_TxMonoValue>,
	typename = std::enable_if_t<(
		detail::is_mono_serializable_v<TMonoValue> && 
		!std::is_enum_v<TMonoValue>
		)>>
void deserialize(Ts&& src, _TxMonoValue& value, Options = {}) {

	static_assert(is_trait_as_options_v<Options>, __FILE_LINE__);
	static_assert(detail::is_mono_serializable_v<TMonoValue>, __FILE__);
	static_assert(!std::is_const_v<decltype(value)> && !std::is_const_v<std::remove_reference_t<decltype(value)>>, __FILE__);

	detail::sread(std::forward<Ts>(src), value, serialized_options_cond<decltype(value), Options>{});
	}

template< typename Ts, 
	typename _TEnum,
	typename Options = none_options,
	typename T = typename std::decay_t<_TEnum>,
	typename std::enable_if_t<std::is_enum_v<T>, int> = 0>
void deserialize(Ts&& src, _TEnum& value, Options = {}) {

	static_assert(is_trait_as_options_v<Options>, __FILE_LINE__);
	static_assert(!std::is_const_v<decltype(value)>, __FILE__);

	typedef std::underlying_type_t<T> ubase_t;

	ubase_t ubase = (ubase_t)T::lower_bound;
	detail::sread(std::forward<Ts>(src), ubase, serialized_options_cond<decltype(ubase), Options>{});
	if( ubase <= (ubase_t)T::lower_bound || ubase >= (ubase_t)T::upper_bound )
		SERIALIZE_THROW_EXC_FWD( "type=" + xtype_properties_t<T>::name );

	value = (T)ubase;
	}

template< typename Ts, 
	typename TContainerContigouosBytes,
	typename Options = none_options,
	typename _TxTContainerContigouosBytes = typename std::decay_t<TContainerContigouosBytes>,
	typename std::enable_if_t<(
		detail::is_container_contiguous_of_bytes_v<TContainerContigouosBytes> &&
		!detail::is_fixed_size_array_v<TContainerContigouosBytes>
		), int> = 0>
size_t deserialize(Ts&& stream, TContainerContigouosBytes& c, Options = {}) {

	static_assert(is_trait_as_options_v<Options>, __FILE_LINE__);
	static_assert(detail::is_contiguous_iterator_v<decltype(std::declval<_TxTContainerContigouosBytes>().begin())>, __FILE__);
	static_assert(!std::is_const_v<decltype(c)>, __FILE__);

	using length_type = typename detail::option_trait_length_type<Options>;
	using range_elem_t = typename detail::contaiter_value_type_t<TContainerContigouosBytes>;
	static_assert(detail::is_byte_v<range_elem_t>, __FILE__);

	length_type sizeval{ 0 };

	// �����
	detail::sread(std::forward<Ts>(stream), sizeval, Options{});

	// ��������
	if constexpr(detail::is_resize_support_v<TContainerContigouosBytes>) {
		if(sizeval) {
			c.resize(sizeval);
			CBL_VERIFY(c.size() >= sizeval);
			}
		}
	else {
		if(c.size() < sizeval) {
			SERIALIZE_THROW_EXC_FWD(nullptr);
			}
		}

	if(sizeval != std::forward<Ts>(stream).read(c, 0, sizeval)) {
		SERIALIZE_THROW_EXC_FWD(nullptr);
		}

	return sizeval;
	}

template< typename Ts, 
	typename Tsequence,
	typename Options = none_options,
	typename std::enable_if_t<(
		detail::is_container_of_mono_v<Tsequence> &&
		!detail::is_container_contiguous_of_bytes_v<Tsequence> &&
		!detail::is_fixed_size_array_v<Tsequence>), int> = 0>
size_t deserialize(Ts && stream, Tsequence & seq, Options = {}) {
	
	static_assert(is_trait_as_options_v<Options>, __FILE_LINE__);
	static_assert(detail::is_container_v<Tsequence>, "bad consistency");
	static_assert(detail::is_container_of_mono_v<Tsequence>, __FILE__);
	static_assert(!std::is_const_v<decltype(seq)>, __FILE__);

	using length_type = typename detail::option_trait_length_type<Options>;
	typedef std::decay_t<decltype(*std::declval<Tsequence>().begin())> sequence_elem_t;

	length_type sizeval = 0;

	// �����
	detail::sread(std::forward<Ts>(stream), sizeval, Options{});

	// ��������
	if constexpr(detail::is_resize_support_v<Tsequence>) {
		if(sizeval) {
			seq.resize(sizeval);
			}
		}
	else {
		if(seq.size() < sizeval) {
			SERIALIZE_THROW_EXC_FWD(nullptr);
			}
		}

	auto next = std::forward<Tsequence>(seq).begin();
	detail::sread_range(std::forward<Ts>(stream), seq.begin(), seq.end(), next, serialized_options_cond<decltype(*next), Options>{});
	if(std::forward<Tsequence>(seq).cend() != next) {
		SERIALIZE_THROW_EXC_FWD(nullptr);
		}

	return sizeval;
	}

template< typename Ts, 
	typename TC2X,
	typename Options = none_options,
	typename std::enable_if_t<(
		detail::is_container_of_containers_v<TC2X> &&
		!detail::is_fixed_size_array_v<TC2X>
		), int> = 0 >
size_t deserialize(Ts&& stream, TC2X & seq, Options = {}) {

	static_assert(is_trait_as_options_v<Options>, __FILE_LINE__);
	static_assert(detail::is_container_v<TC2X>, "bad consistency");
	static_assert(detail::is_container_v<detail::contaiter_value_type_t<TC2X>>, __FILE__);
	static_assert(!std::is_const_v<decltype(seq)>, __FILE__);

	using c2x_type = typename std::decay_t<TC2X>;
	using length_type = typename detail::option_trait_length_type<Options>;
	using sequence_2_t = typename std::decay_t<decltype(*std::declval<TC2X>().begin())> ;

	length_type tablesize = 0;

	// ����� �������������������
	deserialize(std::forward<Ts>(stream), tablesize, Options{});
	c2x_type seq_;

	for( size_t i = 0; i < tablesize; ++i ) {
		sequence_2_t line;
		deserialize(std::forward<Ts>(stream), line, serialized_options_cond<decltype(line), Options>{});

		seq_.push_back(std::move(line));
		}

	std::swap(seq, seq_);

	return tablesize;
	}

template< typename Ts,
	typename OutIt,
	typename Options = none_options,
	typename std::enable_if<(
		detail::is_iterator_mono_v<OutIt> 
		&& !detail::is_iterator_byte_v<OutIt>
		), int>::type = 0 >
typename OutIt deserialize(Ts&& stream, OutIt first, OutIt last, Options = {}) {

	static_assert(is_trait_as_options_v<Options>, __FILE_LINE__);
	static_assert(detail::is_mono_serializable_v<std::decay_t<decltype(*(std::declval<OutIt>()))>>
		&& !detail::is_strob_flat_serializable_v<std::decay_t<decltype(*(std::declval<OutIt>()))>>, "bad consistency");
	static_assert(!std::is_const_v<decltype(*first)>, __FILE__);

	using length_type = detail::option_trait_length_type<Options>;
	typedef std::decay_t<decltype(*std::declval<OutIt>())> sequence_elem_t;

	length_type sizeval {0};

	// �����
	detail::sread(std::forward<Ts>(stream), sizeval, Options{});

	// ��������
	auto next = first;
	if(sizeval != detail::sread_range(std::forward<Ts>(stream), first, last, next, serialized_options_cond<decltype(*next), Options>{})) {
		SERIALIZE_THROW_EXC_FWD(nullptr);
		}

	return next;
	}


template< typename Ts,
	typename OutIt,
	typename Options = none_options,
	typename std::enable_if_t<detail::is_iterator_strob_v<OutIt>, int> = 0 >
typename OutIt deserialize(Ts&& stream, OutIt first, OutIt last, Options = {}) {

	static_assert(is_trait_as_options_v<Options>, __FILE_LINE__);
	static_assert(detail::is_strob_flat_serializable_v<std::decay_t<decltype(*(std::declval<OutIt>()))>>
		&& !detail::is_mono_serializable_v<std::decay_t<decltype(*(std::declval<OutIt>()))>>, "bad consistency");
	static_assert(!std::is_const_v<decltype(*first)>, __FILE__);

	using length_type = detail::option_trait_length_type<Options>;
	typedef std::decay_t<decltype(*std::declval<OutIt>())> sequence_elem_t;

	// �����
	length_type sizeval = 0;
	detail::sread(std::forward<Ts>(stream), sizeval, Options{});

	// ��������
	auto next = first;
	if (sizeval != detail::sread_range(std::forward<Ts>(stream), first, last, next, 
		serialized_options_cond<decltype(*next), Options>{})) {

		SERIALIZE_THROW_EXC_FWD(nullptr);
		}

	return next;
	}


template< typename Ts,
	typename _TxSequenceStrob,
	typename Options = none_options,
	typename TsequenceStrob = std::decay_t<_TxSequenceStrob>,
	typename std::enable_if_t<(
		detail::is_container_of_strob_v<TsequenceStrob> &&
		!detail::is_container_of_isrlzd_v<_TxSequenceStrob> &&
		!detail::is_fixed_size_array_v< _TxSequenceStrob> &&
		!detail::is_dictionary_v<TsequenceStrob> &&
		!detail::is_container_v<detail::contaiter_value_type_t<TsequenceStrob>>), int> = 0>
	size_t deserialize(Ts&& stream, _TxSequenceStrob& seq, Options = {}) {

	static_assert(is_trait_as_options_v<Options>, __FILE_LINE__);
	static_assert(detail::is_container_v<TsequenceStrob>, "bad consistency");
	static_assert(detail::is_container_of_strob_v<TsequenceStrob>, __FILE__);
	static_assert(!std::is_const_v < decltype(seq) >, __FILE__);

	using length_type = typename detail::option_trait_length_type<Options>;
	typedef std::decay_t<decltype(*std::declval<TsequenceStrob>().begin())> sequence_elem_t;

	// �����
	length_type sizeval = 0;
	detail::sread(std::forward<Ts>(stream), sizeval, Options{});

	// ��������
	if constexpr(detail::is_resize_support_v<_TxSequenceStrob>) {
		if(sizeval) {
			seq.resize(sizeval);
			CBL_VERIFY(seq.size() >= sizeval);
			}
		}
	else {
		if(seq.size() < sizeval) {
			SERIALIZE_THROW_EXC_FWD(nullptr);
			}
		}

	auto next = seq.begin();

	detail::sread_range(std::forward<Ts>(stream), seq.begin(), seq.end(), next, 
		serialized_options_cond<decltype(*next), Options>{});
	if(seq.cend() != next) {
		SERIALIZE_THROW_EXC_FWD(nullptr);
		}

	return sizeval;
	}


template< typename Ts,
	typename _TxDictionary,
	typename Options = none_options,
	typename TDictionary = std::decay_t<_TxDictionary>,
	typename std::enable_if_t<(
		detail::is_dictionary_v<TDictionary>), int> = 0>
size_t deserialize(Ts&& stream, _TxDictionary& seq, Options = {}) {

	static_assert(is_trait_as_options_v<Options>, __FILE_LINE__);
	static_assert(!std::is_const_v < decltype(seq) >, __FILE__);

	using length_type = detail::option_trait_length_type<Options>;
	using mapped_key_t = detail::dictionary_key_type_t<TDictionary>;
	using mapped_value_t = detail::dictionary_mapped_type_t<TDictionary>;

	// �����
	length_type sizeval { 0 };
	detail::sread(std::forward<Ts>(stream), sizeval, Options{});

	// ��������
	for (size_t i = 0; i < sizeval; ++i){
		mapped_key_t key;
		deserialize(std::forward<Ts>(stream), key, serialized_options_cond<decltype(key), Options>{});

		mapped_value_t val;
		deserialize(std::forward<Ts>(stream), val, serialized_options_cond<decltype(val), Options>{});

		if (!seq.insert({std::move(key), std::move(val)}).second){
			SERIALIZE_THROW_EXC_FWD(nullptr);
			}
		}

	return sizeval;
	}

template<typename T, size_t N>
struct deserialize_fixarray_result {
	static constexpr size_t size = N;
	using value_type = typename std::decay_t<T>;
	size_t count{ 0 };
	};

template< typename Tstream,
	typename TFixedArray,
	typename Options = none_options,
	typename std::enable_if_t<(
		detail::is_fixed_size_array_v<TFixedArray>
		), int> = 0>
auto deserialize(Tstream&& stream, TFixedArray& va, Options = {}) {

	static_assert(is_trait_as_options_v<Options>, __FILE_LINE__);

	using trait_type = typename detail::fixed_array_trait_t<TFixedArray, Options>;
	using length_type = typename trait_type::length_type;
	using range_elem_t = typename trait_type::value_type;
	const auto destcount = trait_type::range_size;

#ifdef USE_DCF_CHECK
	auto off = stream.offset();
	off;/* disable warning compiler only */
#endif

	// ����� ������
	length_type sizeval{ 0 };
	detail::sread(std::forward<Tstream>(stream), sizeval, Options{});

	if(destcount < sizeval) {
		SERIALIZE_THROW_EXC_FWD(nullptr);
		}

	// ��������
	auto next = std::addressof(va[0]);

	detail::sread_range(std::forward<Tstream>(stream),
		std::addressof(va[0]),
		std::addressof(va[0]) + sizeval,
		next, 
		serialized_options_cond<decltype(*next), Options>{});

#ifdef USE_DCF_CHECK
	if constexpr(detail::is_fixed_size_serialized_v<range_elem_t, Options>){
		CBL_VERIFY(((stream.offset() - off) == write_size_v<TFixedArray, Options>));
		}
#endif

	return deserialize_fixarray_result<range_elem_t, destcount>{sizeval};
	}


namespace detail {

struct deserialize_dispatch {};

template<typename S>
void deserialize_pack( S &, deserialize_dispatch) { }

template<typename S, typename T, typename ... AL>
void deserialize_pack(S & s, deserialize_dispatch tag, T&& h, AL&& ... tail){
	deserialize(s, std::forward<T>(h));
	deserialize_pack(s, tag, std::forward<AL>(tail)...);
	}



template <std::size_t I, std::size_t N, typename Options>
struct tuple_prefixed_deserialize_impl {
	template <typename S, typename T>
	static void deserialize_do(S &s, T&& value) {
		deserialize(s, std::get<I>(std::forward<T>(value)), 
			serialized_options_cond<decltype(get<I>(std::forward<T>(value))), Options>{});
		tuple_prefixed_deserialize_impl<I + 1, N, Options>::deserialize_do(s, std::forward<T>(value));
		}
	};

template <std::size_t I, typename Options>
struct tuple_prefixed_deserialize_impl<I, I, Options> {
	template <typename S, typename T> static void deserialize_do(S &, T&&) noexcept {}
	};
}


template<typename S,
	typename Options,
	typename ... AL>
auto deserialize(S&& s, std::tuple<AL...>& tpl, Options)noexcept -> typename std::enable_if_t<detail::is_all_srlzd_v<AL...>, void> {
	static_assert(is_trait_as_options_v<Options>, __FILE_LINE__);
	detail::tuple_prefixed_deserialize_impl<0, std::tuple_size_v<std::decay_t<decltype(tpl)>>, Options>::deserialize_do(std::forward<S>(s), tpl);
	}

template<typename S,
	typename ... AL>
	auto deserialize(S&& s, std::tuple<AL...>& tpl)noexcept -> typename std::enable_if_t<detail::is_all_srlzd_v<AL...>, void> {
	detail::tuple_prefixed_deserialize_impl<0, std::tuple_size_v<std::decay_t<decltype(tpl)>>, none_options>::deserialize_do(std::forward<S>(s), tpl);
	}



template<typename S,
	typename ... AL>
auto deserialize_all(S && s, AL&& ... al) ->
typename std::enable_if_t<detail::is_all_srlzd_v<AL...>, void> {
	if constexpr (sizeof...(AL) > 1) {
		return detail::deserialize_pack(std::forward<S>(s), detail::deserialize_dispatch{}, std::forward<AL>(al)...);
		}
	else {
		if constexpr (sizeof...(AL) == 1) {
			return deserialize(std::forward<S>(s), std::forward<AL>(al)...);
			}
		else {
			return 0;
			}
		}
	}
}
}

