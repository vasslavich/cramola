#pragma once


#include <type_traits>
#include <functional>
#include "../../../internal_invariants.h"
#include "../../../notifications/base_predecl.h"

namespace crm{
namespace srlz {
namespace detail {


#ifdef CBL_USE_BIG_LITTLE_ENDIAN
template< typename TCvt, typename Tvalue >
void swrite_cvt_little_endian(typename TCvt&& cvt, const Tvalue& v) {
	static_assert(std::is_pod<Tvalue>::value, __FILE__);

	const uint8_t* pV = (const uint8_t*)&v;
	for(size_t i = 0; i < sizeof(Tvalue); ++i)
		cvt(pV[i]);
	}

template< typename TCvt, typename Tvalue >
void swrite_cvt_big_endian(typename TCvt&& cvt, const Tvalue& v) {
	static_assert(std::is_pod<Tvalue>::value, __FILE__);

	const uint8_t* pV = (const uint8_t*)&v;
	for(size_t i = 0; i < sizeof(Tvalue); ++i)
		cvt(pV[sizeof(Tvalue) - 1 - i]);
	}


template< typename TCvt, typename Tvalue >
void swrite_cvt(typename TCvt&& cvt, const Tvalue& v) {

	if(crm::sysenv::sys_is_little_endian()) {
		swrite_cvt_little_endian(std::forward<TCvt>(cvt), v);
		}
	else {
		swrite_cvt_big_endian(std::forward<TCvt>(cvt), v);
		}
	}


template< typename TCvt, typename Tvalue >
void swrite_cvt_pmem_little_endian(typename TCvt&& cvt, const Tvalue& v) {
	static_assert(std::is_pod<Tvalue>::value, __FILE__);

	const uint8_t* pV = (const uint8_t*)v.pmobject();
	for(size_t i = 0; i < v.pmsize(); ++i)
		cvt(pV[i]);
	}

template< typename TCvt, typename Tvalue >
void swrite_cvt_pmem_big_endian(typename TCvt&& cvt, const Tvalue& v) {
	static_assert(std::is_pod<Tvalue>::value, __FILE__);

	const uint8_t* pV = (const uint8_t*)v.pmobject();
	for(size_t i = 0; i < v.pmsize(); ++i)
		cvt(pV[sizeof(Tvalue) - 1 - i]);
	}


template< typename TCvt, typename Tvalue >
void swrite_cvt_pmem(typename TCvt&& cvt, const Tvalue& v) {

	if(crm::sysenv::sys_is_little_endian()) {
		swrite_cvt_pmem_little_endian(std::forward<TCvt>(cvt), v);
		}
	else {
		swrite_cvt_pmem_big_endian(std::forward<TCvt>(cvt), v);
		}
	}
#endif//CBL_USE_BIG_LITTLE_ENDIAN


template< typename Tstream,
	typename Tvalue,
	typename Options = none_options,
	typename std::enable_if_t< (
		is_mono_serializable_v<Tvalue>
		&& !is_byte_v<Tvalue>), int> = 0 >
void swrite(Tstream&& dest, Tvalue&& v, Options = {}) {
	static_assert(sizeof(v) > 1, __FILE__);

#ifdef CBL_USE_BIG_LITTLE_ENDIAN
	swrite_cvt([&dest](uint8_t byte) { dest.push(byte); }, v);

#else//CBL_USE_BIG_LITTLE_ENDIAN

	const_binary_iterator_adapter_t<Tvalue> adpt(v);
	static_assert(is_contiguous_iterator_v<decltype(adpt.cbegin())>, __FILE__);
	static_assert(is_container_contiguous_of_bytes_v<decltype(adpt)>, __FILE__);

	dest.write(adpt);
#endif//CBL_USE_BIG_LITTLE_ENDIAN
	}

template< typename Tstream,
	typename _TxValue,
	typename Options = none_options,
	typename Tvalue = typename _TxValue,
	typename std::enable_if_t<(
		is_mono_serializable_v<Tvalue>
		&& is_byte_v<Tvalue>
		&& !std::is_same_v<bool, Tvalue>), int> = 0>
void swrite(Tstream&& dest, _TxValue&& v, Options = {}) {
	if(!dest.write(std::forward<_TxValue>(v))) {
		SERIALIZE_THROW_EXC_FWD(nullptr);
		}
	}

template< typename Tstream,
	typename Options = none_options>
	void swrite(Tstream&& dest, bool v, Options = {}) {

	std::uint8_t ub = v ? 1 : 0;
	if(!std::forward<Tstream>(dest).write(ub)) {
		SERIALIZE_THROW_EXC_FWD(nullptr);
		}
	}


template< typename Tstream,
	typename TInIt,
	typename Options = none_options,
	typename std::enable_if_t<(
		is_input_iterator_strob_v<TInIt>
		&& is_input_iterator_v<TInIt>), int> = 0>
void swrite_range(Tstream&& dest, TInIt first, TInIt last, Options = {}) {

	static_assert(is_trait_as_options_v<Options>, __FILE_LINE__);
	static_assert(!is_iterator_byte_v<TInIt>, __FILE__);
	static_assert(is_strob_flat_serializable_v<iterator_value_t<TInIt>>, __FILE__);

	if(first != last) {
		TInIt it = first;
		for(; it != last; ++it) {
#ifdef USE_DCF_CHECK
			auto off = dest.offset();
#endif

			serialize(std::forward<Tstream>(dest), (*it), serialized_options_cond<decltype(*it), Options>{});

#ifdef USE_DCF_CHECK
			CBL_VERIFY((dest.offset() - off) == object_trait_serialized_size((*it), serialized_options_cond<decltype(*it), Options>{}));
#endif
			}
		}
	}


template< typename Tstream,
	typename TInIt,
	typename Options = none_options,
	typename std::enable_if_t<(
		!is_input_iterator_strob_v<TInIt> &&
		!is_iterator_mono_v<TInIt> &&
		is_input_iterator_v<TInIt>
		), int> = 0>
void swrite_range(Tstream&& dest, TInIt first, TInIt last, Options = {}) {

	static_assert(is_trait_as_options_v<Options>, __FILE_LINE__);

	if(first != last) {
		TInIt it = first;
		for(; it != last; ++it) {
#ifdef USE_DCF_CHECK
			auto off = dest.offset();
#endif

			serialize(std::forward<Tstream>(dest), (*it), serialized_options_cond<decltype(*it), Options>{});

#ifdef USE_DCF_CHECK
			CBL_VERIFY((dest.offset() - off) == object_trait_serialized_size((*it), serialized_options_cond<decltype(*it), Options>{}));
#endif
			}
		}
	}


template< typename Tstream,
	typename TInIt,
	typename Options = none_options,
	typename std::enable_if_t<(
		is_iterator_mono_v<TInIt>
		&& is_input_iterator_v<TInIt>
		&& !is_iterator_byte_v<TInIt>), int> = 0>
void swrite_range(Tstream&& dest, TInIt first, TInIt last, Options = {}) {

	static_assert(is_trait_as_options_v<Options>, __FILE_LINE__);
	static_assert(sizeof(iterator_value_t<TInIt>) > 1, __FILE__);
	static_assert(is_mono_serializable_v<iterator_value_t<TInIt>>, __FILE__);

	if(first != last) {
		auto it = first;
		for(; it != last; ++it)
			swrite(std::forward<Tstream>(dest), (*it), serialized_options_cond<decltype(*it), Options>{});
		}
	}

template< typename Tstream,
	typename TInIt,
	typename Options = none_options,
	typename std::enable_if_t<(
		is_iterator_byte_v<TInIt>
		&& is_input_iterator_v<TInIt>), int> = 0 >
void swrite_range(Tstream&& dest, TInIt first, TInIt last, Options = {}) {

	static_assert(is_trait_as_options_v<Options>, __FILE_LINE__);
	static_assert(is_byte_v<iterator_value_t<TInIt>>, __FILE__);

	if(first != last) {
		auto [wit, wl] = dest.write(first, last);
		if(!(wit == last)) {
			SERIALIZE_THROW_EXC_FWD(nullptr);
			}
		}
	}


template< typename Tstream,
	typename TContainer,
	typename Options = none_options,
	typename std::enable_if_t<is_container_of_bytes_v<TContainer>, int> = 0 >
void swrite_range(Tstream&& dest, TContainer&& c, Options = {}) {

	static_assert(is_trait_as_options_v<Options>, __FILE_LINE__);
	static_assert(is_byte_v<contaiter_value_type_t<TContainer>>, __FILE__);

	dest.write(std::forward<TContainer>(c), 0, std::size(c));
	}


template< typename Tstream,
	typename TContainer,
	typename Options = none_options,
	typename std::enable_if_t<is_container_of_strob_v<TContainer>, int> = 0 >
void swrite_range(Tstream&& dest, TContainer&& c, Options = {}) {

	static_assert(is_trait_as_options_v<Options>, __FILE_LINE__);
	static_assert(is_strob_flat_serializable_v<contaiter_value_type_t<TContainer>>, __FILE__);

	auto it = std::cbegin(c);
	const auto endIt = std::cend(c);
	for(; it != endIt; ++it)
		serialize(std::forward<Tstream>(dest), (*it), serialized_options_cond<decltype(*it), Options>{});
	}

template< typename Tstream,
	typename TContainer,
	typename Options = none_options,
	typename std::enable_if_t<(
		is_container_of_mono_v<TContainer> &&
		!is_container_of_bytes_v<TContainer>
		), int> = 0 >
void swrite_range(Tstream&& dest, TContainer&& c, Options = {}) {

	static_assert(is_trait_as_options_v<Options>, __FILE_LINE__);
	static_assert(is_mono_serializable_v<contaiter_value_type_t<TContainer>>, __FILE__);

	auto it = std::cbegin(c);
	const auto endIt = std::cend(c);

	for(; it != endIt; ++it)
		swrite(std::forward<Tstream>(dest), (*it), serialized_options_cond<decltype(*it), Options>{});
	}
}


template< typename Tstream,
	typename _TxMonoValue,
	typename Options = none_options,
	typename TMonoValue = typename std::decay_t<_TxMonoValue>,
	typename std::enable_if_t<(
		detail::is_mono_serializable_v<TMonoValue> &&
		!std::is_enum_v<TMonoValue>), int> = 0>
void serialize(Tstream&& dest, _TxMonoValue&& value, Options = {}) {

	static_assert(is_trait_as_options_v<Options>, __FILE_LINE__);
	static_assert(detail::is_mono_serializable_v<TMonoValue>, __FILE__);

#ifdef USE_DCF_CHECK
	auto off = dest.offset();
#endif

	detail::swrite(std::forward<Tstream>(dest), std::forward<_TxMonoValue>(value), serialized_options_cond<decltype(value), Options>{});

#ifdef USE_DCF_CHECK
	CBL_VERIFY((dest.offset() - off) == object_trait_serialized_size(value, serialized_options_cond<decltype(value), Options>{}));
#endif
	}

template< typename Tstream,
	typename _TxEnum,
	typename Options = none_options,
	typename T = typename std::decay_t<_TxEnum>,
	typename std::enable_if_t<std::is_enum_v<T>, int> = 0>
	void serialize(Tstream&& dest, _TxEnum&& value, Options = {}) {

	static_assert(is_trait_as_options_v<Options>, __FILE_LINE__);
	static_assert(std::is_enum_v<T>, __FILE__);

	typedef std::underlying_type<T>::type ubase_t;

	const auto ubase = (ubase_t)value;
	if(ubase <= (ubase_t)T::lower_bound || ubase >= (ubase_t)T::upper_bound)
		SERIALIZE_THROW_EXC_FWD("type=" + xtype_properties_t<T>::name);


#ifdef USE_DCF_CHECK
	auto off = dest.offset();
#endif

	detail::swrite(std::forward<Tstream>(dest), ubase, serialized_options_cond<decltype(ubase), Options>{});

#ifdef USE_DCF_CHECK
	CBL_VERIFY((dest.offset() - off) == object_trait_serialized_size(value, serialized_options_cond<decltype(value), Options>{}));
#endif
	}


template< typename Tstream,
	typename TContainer,
	typename Options = none_options,
	typename std::enable_if_t<detail::is_container_of_bytes_v<TContainer>, int> = 0>
void serialize_raw(Tstream&& dest, TContainer&& c, Options = {}) {
	static_assert(is_trait_as_options_v<Options>, __FILE_LINE__);

	detail::swrite_range(std::forward<Tstream>(dest),
		std::forward<TContainer>(c), 
		serialized_options_cond<decltype(c), Options>{});
	}


template< typename Tstream,
	typename TItIn,
	typename Options = none_options,
	typename std::enable_if_t<(
		detail::is_iterator_mono_v<TItIn>
		&& !detail::is_iterator_byte_v<TItIn>
		), int> = 0>
void serialize_raw(Tstream&& dest, TItIn first, TItIn last, Options = {}) {
	static_assert(is_trait_as_options_v<Options>, __FILE_LINE__);

	detail::swrite_range(std::forward<Tstream>(dest),
		first, 
		last, 
		serialized_options_cond<decltype(*last), Options>{});
	}

template< typename Tstream,
	typename TItIn,
	typename Options = none_options,
	typename std::enable_if_t<(
		detail::is_input_iterator_strob_v<TItIn>), int> = 0>
void serialize_raw(Tstream&& dest, TItIn first, TItIn last, Options = {}, size_t l_ = (size_t)(-1)) {
	static_assert(is_trait_as_options_v<Options>, __FILE_LINE__);

	detail::swrite_range(std::forward<Tstream>(dest),
		first, 
		last,
		l_,
		serialized_options_cond<decltype(*last), Options>{});
	}

template< typename Tstream,
	typename InIt,
	typename Options = none_options,
	typename std::enable_if_t<(
		detail::is_iterator_mono_v<InIt>
		&& !detail::is_iterator_byte_v<InIt>
		), int> = 0 >
void serialize(Tstream&& stream, InIt first, InIt last, Options = {}) {
	static_assert(is_trait_as_options_v<Options>, __FILE_LINE__);

	using length_type = typename detail::option_trait_length_type<TOptions>;
	using range_elem_t = detail::iterator_value_t<InIt>;

	// �����
	const auto ldiff = std::distance(first, last);
	if(ldiff < 0) {
		SERIALIZE_THROW_EXC_FWD(nullptr);
		}

	if(ldiff <= (std::numeric_limits< length_type>::max)()) {
		detail::swrite(std::forward<Tstream>(stream), static_cast<length_type>(ldiff), Options{});
		}
	else {
		SERIALIZE_THROW_EXC_FWD(nullptr);
		}

	// ��������
	detail::swrite_range(std::forward<Tstream>(stream), 
		first, 
		last, 
		ldiff, 
		serialized_options_cond<decltype(*last), Options>{});
	}

template< typename Tstream,
	typename InIt,
	typename Options = none_options,
	typename std::enable_if_t<(
		detail::is_iterator_strob_v<InIt>), int> = 0 >
void serialize(Tstream&& stream, InIt first, InIt last, Options = {}) {
	static_assert(is_trait_as_options_v<Options>, __FILE_LINE__);

	using length_type = typename detail::option_trait_length_type<TOptions>;
	using range_elem_t = detail::iterator_value_t<InIt>;

	// �����
	const auto ldiff = std::distance(first, last);
	if(ldiff < 0) {
		SERIALIZE_THROW_EXC_FWD(nullptr);
		}

	if(ldiff <= (std::numeric_limits< length_type>::max)()) {
		detail::swrite(std::forward<Tstream>(stream), static_cast<length_type>(ldiff), Options{});
		}
	else {
		SERIALIZE_THROW_EXC_FWD(nullptr);
		}

	// ��������
	detail::swrite_range(std::forward<Tstream>(stream), 
		first,
		last, 
		ldiff,
		serialized_options_cond<decltype(*last), Options>{});
	}


template< typename Tstream,
	typename _TxContainerContigouosBytes,
	typename Options = none_options,
	typename TContainerContigouosBytes = typename std::decay_t<_TxContainerContigouosBytes>,
	typename std::enable_if_t<(
		detail::is_container_contiguous_of_bytes_v<TContainerContigouosBytes> &&
		!detail::is_fixed_size_array_v<TContainerContigouosBytes>
		), int> = 0 >
void serialize(Tstream&& stream, _TxContainerContigouosBytes& cb, Options = {}) {

	static_assert(is_trait_as_options_v<Options>, __FILE_LINE__);

	using length_type = typename detail::option_trait_length_type<Options>;
	using range_elem_t = detail::contaiter_value_type_t<TContainerContigouosBytes>;

	static_assert(detail::is_byte_v<range_elem_t>, __FILE__);

#ifdef USE_DCF_CHECK
	auto off = stream.offset();
#endif

	// �����
	const auto l = std::size(cb);
	if(l <= (std::numeric_limits<length_type>::max)()) {
		detail::swrite(std::forward<Tstream>(stream), static_cast<length_type>(l), Options{});
		}
	else {
		SERIALIZE_THROW_EXC_FWD(nullptr);
		}

	// ��������
	stream.write(std::forward<_TxContainerContigouosBytes>(cb), 0, l);

#ifdef USE_DCF_CHECK
	CBL_VERIFY((stream.offset() - off) == object_trait_serialized_size(cb, serialized_options_cond<decltype(cb), Options>{}));
#endif
	}


template< typename Tstream,
	typename _TxSequence,
	typename Options = none_options,
	typename Tsequence = typename std::decay_t<_TxSequence>,
	typename std::enable_if_t<(
		detail::is_container_of_mono_v<Tsequence> &&
		!detail::is_dictionary_v<Tsequence> &&
		!detail::is_container_contiguous_of_bytes_v<Tsequence> &&
		!detail::is_fixed_size_array_v<Tsequence>
		), int> = 0 >
void serialize(Tstream&& stream, _TxSequence&& c, Options = {}) {

	static_assert(is_trait_as_options_v<Options>, __FILE_LINE__);
	static_assert(detail::is_iterator_mono_v<decltype(c.cbegin())>, __FILE__);
	static_assert(detail::is_mono_serializable_v<detail::contaiter_value_type_t<Tsequence>>, __FILE__);

	using length_type = typename detail::option_trait_length_type<Options>;
	using range_elem_t = detail::contaiter_value_type_t<_TxSequence>;

#ifdef USE_DCF_CHECK
	auto off = stream.offset();
#endif

	// �����
	const auto count_ = std::size(c);
	if(count_ <= (std::numeric_limits<length_type>::max)()) {
		detail::swrite(std::forward<Tstream>(stream), static_cast<length_type>(count_), Options{});
		}
	else {
		SERIALIZE_THROW_EXC_FWD(nullptr);
		}

	// ��������
	detail::swrite_range(std::forward<Tstream>(stream), 
		std::forward<_TxSequence>(c), 
		serialized_options_cond<decltype(c),Options>{});

#ifdef USE_DCF_CHECK
	CBL_VERIFY((stream.offset() - off) == object_trait_serialized_size(c, serialized_options_cond<decltype(c), Options>{}));
#endif
	}

template< typename Tstream,
	typename _TxSequence,
	typename Options = none_options,
	typename Tsequence = typename std::decay_t<_TxSequence>,
	typename TVT = typename detail::contaiter_value_type_t<Tsequence>,
	typename std::enable_if_t<(
		detail::is_container_of_strob_v<Tsequence> && 
		!detail::is_container_of_isrlzd_v<Tsequence> &&
		!detail::is_container_of_isrlzd_v<Tsequence> &&
		!detail::is_fixed_size_array_v< _TxSequence> &&
		!detail::is_dictionary_v<Tsequence>
		), int> = 0>
void serialize(Tstream&& stream, _TxSequence&& c, Options = {}) {

	static_assert(is_trait_as_options_v<Options>, __FILE_LINE__);
	static_assert(!detail::is_container_v<detail::contaiter_value_type_t<Tsequence>>, __FILE__);
	static_assert(detail::is_strob_flat_serializable_v<detail::iterator_value_t<decltype(c.cbegin())>>, __FILE__);
	static_assert(detail::is_iterator_strob_v<decltype(c.cbegin())>, __FILE__);

#ifdef USE_DCF_CHECK
	auto off = stream.offset();
#endif

	using length_type = typename detail::option_trait_length_type<Options>;
	using range_elem_t = detail::contaiter_value_type_t<Tsequence>;

	// �����
	const auto count_ = std::size(c);
	if(count_ <= (std::numeric_limits<length_type>::max)()) {
		detail::swrite(std::forward<Tstream>(stream), static_cast<length_type>(count_), Options{});
		}
	else {
		SERIALIZE_THROW_EXC_FWD(nullptr);
		}

	// ��������
	detail::swrite_range(std::forward<Tstream>(stream), 
		std::forward<_TxSequence>(c), 
		serialized_options_cond<decltype(c), Options>{});

#ifdef USE_DCF_CHECK
	CBL_VERIFY((stream.offset() - off) == object_trait_serialized_size(c, serialized_options_cond<decltype(c), Options>{}));
#endif
	}


template< typename Tstream,
	typename _TxDictionary,
	typename Options = none_options,
	typename TDictionary = typename std::decay_t<_TxDictionary>,
	typename std::enable_if_t<(detail::is_dictionary_v<TDictionary>), int> = 0>
void serialize(Tstream&& stream, _TxDictionary&& c, Options = {}) {

	static_assert(is_trait_as_options_v<Options>, __FILE_LINE__);

#ifdef USE_DCF_CHECK
	auto off = stream.offset();
#endif

	using length_type = typename detail::option_trait_length_type<Options>;
	using mapped_key_t = detail::dictionary_key_type_t<TDictionary>;
	using mapped_value_t = detail::dictionary_mapped_type_t<TDictionary>;

	// �����
	const auto count_ = std::size(c);
	if(count_ <= (std::numeric_limits<length_type>::max)()) {
		detail::swrite(std::forward<Tstream>(stream), static_cast<length_type>(count_), Options{});
		}
	else {
		SERIALIZE_OVERFLOW_THROW_EXC_FWD(nullptr);
		}

	// ��������
	for(const auto& e : c) {
		serialize(std::forward<Tstream>(stream), e.first, serialized_options_cond<decltype(e.first), Options>{});
		serialize(std::forward<Tstream>(stream), e.second, serialized_options_cond<decltype(e.second), Options>{});
		}

#ifdef USE_DCF_CHECK
	CBL_VERIFY((stream.offset() - off) == object_trait_serialized_size(c, serialized_options_cond<decltype(c), Options>{}));
#endif
	}



template<typename TStream,
	typename _TxSequenceOfSequence,
	typename Options = none_options,
	typename TsequenceOfSequence = typename std::decay_t<_TxSequenceOfSequence>,
	typename std::enable_if_t<(
		detail::is_container_of_containers_v<TsequenceOfSequence> &&
		!detail::is_fixed_size_array_v<TsequenceOfSequence>
		), int> = 0 >
void serialize(TStream&& stream, _TxSequenceOfSequence&& seq, Options = {}) {

	static_assert(is_trait_as_options_v<Options>, __FILE_LINE__);
	static_assert(detail::is_container_v<detail::contaiter_value_type_t<TsequenceOfSequence>>, __FILE__);

	using length_type = typename detail::option_trait_length_type<Options>;

#ifdef USE_DCF_CHECK
	auto off = stream.offset();
#endif

	// ����� 1-�� ���������
	const auto count_ = std::size(seq);
	if(count_ <= (std::numeric_limits<length_type>::max)()) {
		serialize(std::forward<TStream>(stream), static_cast<length_type>(count_), Options{});
		}
	else {
		SERIALIZE_OVERFLOW_THROW_EXC_FWD(nullptr);
		}

	// ��������
	auto it = std::cbegin(seq);
	const auto endIt = std::cend(seq);
	for(; it != endIt; ++it) {
		serialize(std::forward<TStream>(stream), (*it), serialized_options_cond<decltype(*it), Options>{});
		}

#ifdef USE_DCF_CHECK
	CBL_VERIFY((stream.offset() - off) == object_trait_serialized_size(seq, serialized_options_cond<decltype(seq), Options>{}));
#endif
	}


template<typename T, size_t N>
struct serialize_fixarray_result {
	static constexpr size_t size = N;
	using value_type = typename std::decay_t<T>;
	};

template< typename Tstream,
	typename TFixedArray,
	typename Options = none_options,
	typename std::enable_if_t<(
		detail::is_fixed_size_array_v<TFixedArray>
		), int> = 0>
	auto serialize(Tstream&& stream, TFixedArray&& va, Options = {}) {

	static_assert(is_trait_as_options_v<Options>, __FILE_LINE__);

	using trait_type = typename detail::fixed_array_trait_t<TFixedArray, Options>;
	using length_type = typename trait_type::length_type;
	using range_elem_t = typename trait_type::value_type;
	const auto maxcount = trait_type::range_size;
	
#ifdef USE_DCF_CHECK
	auto off = stream.offset();
	off; /* disable warning compiler only */
#endif

	// �����
	if(maxcount <= (std::numeric_limits< length_type>::max)()) {
		detail::swrite(std::forward<Tstream>(stream), static_cast<length_type>(maxcount), Options{});
		}
	else {
		SERIALIZE_THROW_EXC_FWD(nullptr);
		}

	// ��������
	if(maxcount) {
		detail::swrite_range(std::forward<Tstream>(stream),
			std::addressof(va[0]),
			std::addressof(va[0]) + maxcount,
			serialized_options_cond<range_elem_t, Options>{});
		}

#ifdef USE_DCF_CHECK
	if constexpr(detail::is_fixed_size_serialized_v<range_elem_t, Options>) {
		CBL_VERIFY(((stream.offset() - off) == write_size_v<TFixedArray, Options>));
		}
#endif

	return serialize_fixarray_result<range_elem_t, maxcount>{};
	}


namespace detail {

struct serialize_dispatch {};

template<typename S>
void serialize_pack(S&&, serialize_dispatch) {}

template<typename S, typename T, typename ... AL>
void serialize_pack(S& s, serialize_dispatch tag, T&& h, AL&& ... tail) {
	serialize(s, std::forward<T>(h));
	serialize_pack(s, tag, std::forward<AL>(tail)...);
	}



template <std::size_t I, std::size_t N, typename Options>
struct tuple_prefixed_serialize_impl {
	template <typename S, typename T>
	static void serialize_do(S& s, T&& value) {

		serialize(std::forward<S>(s), std::get<I>(std::forward<T>(value)),
			serialized_options_cond<decltype(std::get<I>(std::forward<T>(value))), Options>{});
		tuple_prefixed_serialize_impl < I + 1, N, Options > ::serialize_do(s, std::forward<T>(value));
		}
	};

template <std::size_t I, typename Options>
struct tuple_prefixed_serialize_impl<I, I, Options> {
	template <typename S, typename T>
	static void serialize_do(S&, T&&) noexcept {}
	};
}

template<typename S, typename Options, typename ... AL>
auto serialize(S&& s, const std::tuple<AL...>& tpl, Options)noexcept ->typename std::enable_if_t<detail::is_all_srlzd_v<AL...>, void> {
	static_assert(is_trait_as_options_v<Options>, __FILE_LINE__);
	detail::tuple_prefixed_serialize_impl<0, std::tuple_size_v<std::decay_t<decltype(tpl)>>, Options>::serialize_do(std::forward<S>(s), tpl);
	}

template<typename S, typename ... AL>
auto serialize(S&& s, const std::tuple<AL...>& tpl)noexcept -> typename std::enable_if_t<detail::is_all_srlzd_v<AL...>, void> {
	detail::tuple_prefixed_serialize_impl<0, std::tuple_size_v<std::decay_t<decltype(tpl)>>, none_options>::serialize_do(std::forward<S>(s), tpl);
	}



template<typename S, typename ... AL>
auto serialize_all(S&& s, AL&& ... al)noexcept ->
typename std::enable_if_t<detail::is_all_srlzd_v<AL...>, void> {
	if constexpr(sizeof...(AL) > 1) {
		return detail::serialize_pack(s, detail::serialize_dispatch{}, std::forward<AL>(al)...);
		}
	else {
		if constexpr(sizeof...(AL) == 1) {
			return serialize(std::forward<S>(s), std::forward<AL>(al)...);
			}
		else {
			return 0;
			}
		}
	}


namespace detail{

enum class wstream_implement_tag_t {
	undefined,
	sequential_container_adpt,
	sequential_container_adpt_actor,
	iterator_vector_it_adpt,
	iterator_ptr_adptr
	};
}
}
}

