#pragma once


#include "./wstream_base.h"


namespace crm::srlz::detail{


struct wstream_base_handler{
private:
	__wstream_base_actor_i & _ws;

public:
	wstream_base_handler(const wstream_base_handler &) = delete;
	wstream_base_handler& operator=(const wstream_base_handler &) = delete;

	size_t offset()const noexcept {
		return _ws.offset();
		}

	template<typename T,
		typename = std::enable_if_t<std::is_base_of_v<__wstream_base_actor_i, std::decay_t<T>>>>
		explicit wstream_base_handler(T & ws_)
		: _ws(ws_){}

	wstream_base_push_type type()const noexcept{
		return _ws.type();
		}

	template<typename TValue,
		typename std::enable_if_t<is_byte_v<TValue>, int> = 0>
	bool write(TValue value){
		if (type() == wstream_base_push_type::actor_0){
			return dynamic_cast<__wstream_base_actor_0&>(_ws).write(value);
			}
		else{
			return dynamic_cast<__wstream_base_actor_1&>(_ws).write(value);
			}
		}

	template<typename C,
		typename std::enable_if_t<is_container_of_bytes_v<C>, int> = 0>
	void write(C && c){
		if (type() == wstream_base_push_type::actor_0){
			dynamic_cast<__wstream_base_actor_0&>(_ws).write(std::forward<C>(c));
			}
		else{
			dynamic_cast<__wstream_base_actor_1&>(_ws).write(std::forward<C>(c));
			}
		}

	template<typename C,
		typename std::enable_if_t<is_container_of_bytes_v<C>, int> = 0>
	void write(C&& c, size_t off_, size_t l_) {
		if(type() == wstream_base_push_type::actor_0) {
			dynamic_cast<__wstream_base_actor_0&>(_ws).write(std::forward<C>(c), off_, l_);
			}
		else {
			dynamic_cast<__wstream_base_actor_1&>(_ws).write(std::forward<C>(c), off_, l_);
			}
		}

	template<typename TInputIt,
		typename std::enable_if_t<is_iterator_byte_v<TInputIt>, int> = 0>
	std::pair<TInputIt, size_t> write(TInputIt first, TInputIt last){
		if (type() == wstream_base_push_type::actor_0){
			return dynamic_cast<__wstream_base_actor_0&>(_ws).write(first, last);
			}
		else{
			return dynamic_cast<__wstream_base_actor_1&>(_ws).write(first, last);
			}
		}
	};
}
