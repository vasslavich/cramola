#pragma once


#include "../../isrlz.h"
#include "../../base_typetrait_decls.h"
#include "../../base_typetrait_using.h"
#include "../comutil/uwstreams.h"


namespace crm{
namespace srlz{
namespace detail{


template<typename TValue,
	typename Options,
	typename = void >
struct write_to_stream_t {};


/*! �������� ������������ �������� �������������� ��������� i_srlz_length_prefixed_t  */
template<typename Tobject, typename Options>
struct write_to_stream_t<Tobject, Options, std::enable_if_t< is_i_srlzd_prefixed_v<Tobject>>> {
	using object_type = typename std::decay_t<Tobject>;

public:
	template<typename Ts, typename Xo,
		typename std::enable_if_t<std::is_same_v<std::decay_t<Tobject>, std::decay_t<Xo>>, int> = 0 >
	void write( Ts && dest, Xo && object ) {

		using value_type = typename std::decay_t<Xo>;
		using serialize_prefix_trait_t = typename serialize_prefix_trait_t<Xo, Options>;

		/* -----------------------------------------------------------
		�������
		--------------------------------------------------------------*/
		typename serialize_prefix_type<Xo, Options> prefix;
		/* compiler only */
		prefix;

#ifdef USE_DCF_CHECK
		size_t destOff = dest.offset();
#endif

		if constexpr(type_prefix_serialize_cond_v<Xo, Options>) {

			/* ---------------------------------------------------------------------
			������ � ����� ��������
			------------------------------------------------------------------------*/
			prefix = serialize_prefix_trait_t::write_prefix(std::forward<Ts>(dest), std::forward<Xo>(object));

#ifdef USE_DCF_CHECK
			if((dest.offset() - destOff) != serialize_prefix_trait_t::prefix_serialized_size())
				SERIALIZE_THROW_EXC_FWD(L"bad format");
#endif
			}

		/*----------------------------------------------------------------------
		������ �����������
		------------------------------------------------------------------------*/
		std::forward<Xo>(object).srlz( dest );

#ifdef USE_DCF_CHECK
		auto disp = dest.offset() - destOff;
		if( disp != serialize_prefix_trait_t::get_serialized_size(std::forward<Xo>(object)) )
			SERIALIZE_THROW_EXC_FWD( L"bad format" );

		if constexpr (type_prefix_serialize_cond_v<Xo, Options>) {
			if (disp != prefix.length)
				SERIALIZE_THROW_EXC_FWD(L"bad format");
			}
#endif
		}
	};


/*! �������� ������������ �������� �������������� ��������� i_srlz_length_eof_t */
template<typename Tobject, typename Options>
struct write_to_stream_t<Tobject, Options, std::enable_if_t<is_i_srlzd_eof_v<Tobject>>> {
public:
	template<typename Ts, typename Xo,
		typename std::enable_if_t<std::is_same_v<std::decay_t<Tobject>, std::decay_t<Xo>>, int> = 0 >
	void write(Ts && dest, Xo && object) {

		using value_type = typename std::decay_t<Xo>;
		using serialize_prefix_trait_t = serialize_prefix_trait_t<Xo, Options>;

		/* ---------------------------------------------------------------------
		������ � ����� ��������
		------------------------------------------------------------------------*/
		typename serialize_prefix_type<Xo, Options> prefix;

#ifdef USE_DCF_CHECK
		size_t destOff = dest.offset();
#endif

		if constexpr(type_prefix_serialize_cond_v<Xo, Options>) {

			/* ---------------------------------------------------------------------
			������ � ����� ��������
			------------------------------------------------------------------------*/
			prefix = serialize_prefix_trait_t::write_prefix(std::forward<Ts>(dest), std::forward<Xo>(object));

#ifdef USE_DCF_CHECK
			if((dest.offset() - destOff) != serialize_prefix_trait_t::prefix_serialized_size())
				SERIALIZE_THROW_EXC_FWD(L"bad format");
#endif
			}

		/* ---------------------------------------------------------
		������ �����������
		------------------------------------------------------------*/
		std::forward<Xo>(object).srlz( dest );

		/* ----------------------------------------------------------
		������ � ����� ������� �����
		----------------------------------------------------------------------------------------*/
		serialize(dest, std::forward<Xo>(object).get_eof_marker(),
			serialized_options_cond<decltype(std::forward<Xo>(object).get_eof_marker()), Options>{});
		}
	};
}


template<typename Tsdest, 
	typename Tisrlz,
	typename Options = none_options,
	typename std::enable_if_t<detail::is_i_srlzd_v<Tisrlz> && !detail::is_serialized_trait_w_v<Tisrlz>, int> = 0>
void serialize(Tsdest&& dest, Tisrlz&& isrlz, Options = {}) {

#ifdef USE_DCF_CHECK
	auto off = dest.offset();
	/*compiler only*/
	(void)(off);
#endif

	detail::write_to_stream_t<Tisrlz, Options> writer;
	if constexpr(detail::is_i_srlzd_prefixed_v<Tisrlz>) {

		writer.write(std::forward<Tsdest>(dest), std::forward<Tisrlz>(isrlz));

#ifdef USE_DCF_CHECK
		CBL_VERIFY((dest.offset() - off) == object_trait_serialized_size(isrlz, serialized_options_cond<decltype(isrlz), Options>{}));
#endif
		}
	else {
		writer.write(std::forward<Tsdest>(dest), std::forward<Tisrlz>(isrlz));
		}
	}


template<typename TStream, 
	typename TContainerValueISerialized,
	typename Options = none_options,
	std::enable_if_t<(
			detail::is_container_of_isrlzd_v<TContainerValueISerialized> &&
			!detail::is_fixed_size_array_v<TContainerValueISerialized>
		),int> = 0>
void serialize(TStream&& stream, TContainerValueISerialized&& seq, Options = {}) {
	using length_type = typename detail::option_trait_length_type<Options>;
	using range_elem_t = detail::contaiter_value_type_t<TContainerValueISerialized>;

#ifdef USE_DCF_CHECK
	auto off = stream.offset();
#endif

	// �����
	const auto count_ = std::size(seq);
	if (count_ < (std::numeric_limits<length_type>::max)()) {
		detail::swrite(std::forward<TStream>(stream), static_cast<length_type>(count_), Options{});
		}
	else {
		SERIALIZE_THROW_EXC_FWD(nullptr);
		}

	// ��������
	auto it = std::cbegin(seq);
	const auto endIt = std::cend(seq);
	for(; it != endIt; ++it) {
		serialize(std::forward<TStream>(stream), (*it), serialized_options_cond<decltype(*it), Options>{});
		}

#ifdef USE_DCF_CHECK
	CBL_VERIFY((stream.offset() - off) == object_trait_serialized_size(seq, serialized_options_cond<decltype(seq), Options>{}));
#endif
	}


/* ���� ������ ������������ �������� ����� � �������� */
template<typename TStream,
	typename Tisrlz,
	typename Options = none_options,
	typename std::enable_if_t<detail::is_serialized_trait_w_v<Tisrlz> && std::is_base_of_v<detail::wstream_base_handler, std::decay_t<TStream>>, int> = 0>
void serialize(TStream&& src, Tisrlz&& isrlz, Options = {}) {
	dynamic_cast<const serialized_base_w&>(isrlz).serialize(std::forward< TStream>(src));
	}


/* ���� ������ ������������ �������� ����� � �������� */
template<typename TStream, 
	typename Tisrlz,
	typename Options = none_options,
	typename std::enable_if_t<detail::is_serialized_trait_w_v<Tisrlz> && std::is_base_of_v<detail::__wstream_base_actor_i, std::decay_t<TStream>>, int> = 0>
void serialize(TStream&& src, Tisrlz&& isrlz, Options = {}) {

	detail::wstream_base_handler h(std::forward<TStream>(src));
	serialize(h, std::forward< Tisrlz>(isrlz), serialized_options_cond<Tisrlz, Options>{});
	}
}
}

