#pragma once


#include <type_traits>
#include <functional>
#include "../comutil/uwstreams.h"
#include "../read/rstreams.h"
#include "./wstream_base.h"
#include "./wstream_container.h"
#include "../interators/wstream_iterator.h"
#include "./wstream_base_handler.h"


namespace crm::srlz{


template<typename TFunctor,
	typename _TFunctor = typename std::decay_t<TFunctor> >
constexpr bool is_stream_elem_functor_v = !(
	std::is_base_of_v<detail::__wstream_base_actor_i, _TFunctor>
	) 
	&& 
	std::is_invocable_v<_TFunctor, uint8_t>;


template<typename TStreamElemActor,
	typename _TStreamElemActor = typename std::decay_t<TStreamElemActor> >
constexpr bool is_stream_elem_actor_v = std::is_base_of_v<detail::__wstream_base_actor_i, _TStreamElemActor>;


class wstream_constructor_t {
public:
	template<typename TOutIt,
		typename std::enable_if_t<detail::is_iterator_byte_v<TOutIt>, int> = 0>
	static auto make(TOutIt first, TOutIt last) {
		return detail::wstream_iterator_adpt_t<TOutIt, 0>(first, last);
		}

	template<typename TSequence,
		typename std::enable_if_t<detail::is_container_of_bytes_v<TSequence>, int> = 0>
	static auto make() {
		return detail::wstream_sequence_adpt_t<TSequence, 0>();
		}

	static auto make() {
		return make<std::vector<uint8_t>>();
		}

	template<typename XFunctor>
	static auto make(XFunctor && f) {
		return make<std::vector<uint8_t>, std::decay_t<XFunctor>>(std::forward<XFunctor>(f));
		}

	template<typename XContainer, 
		typename XActor,
		typename _XContainer = typename std::decay_t<XContainer>,
		typename _XActor = typename std::decay_t<XActor>,
		typename std::enable_if_t<detail::is_container_of_bytes_v<XContainer> && is_stream_elem_actor_v<XActor>, int> = 0>
	static auto make(XActor && actor) {
		return detail::wstream_sequence_adpt_t<_XContainer, 1>(std::forward<XActor>(actor));
		}

	template<typename XContainer,
		typename XFunctor,
		typename _XContainer = typename std::decay_t<XContainer>,
		typename _XFunctor = typename std::decay_t<XFunctor>,
		typename std::enable_if_t<detail::is_container_of_bytes_v<XContainer> && is_stream_elem_functor_v<XFunctor>, int> = 0>
	static auto make(XFunctor && f) {
		return detail::wstream_sequence_adpt_t<_XContainer, 1>(detail::__wstream_actor_wrapper<_XFunctor>(std::forward<XFunctor>(f)));
		}
	};
}

