#pragma once


#include <type_traits>
#include <functional>
#include "../comutil/uwstreams.h"
#include "../read/rstreams.h"
#include "./wstream_base.h"


namespace crm::srlz::detail{


template<typename Tsequence,
	const bool WithActor,
	typename TBaseType = std::conditional_t<WithActor, __wstream_base_actor_1, __wstream_base_actor_0>>
	class wstream_sequence_adpt_t final :
	public std::conditional_t<WithActor, __wstream_base_actor_1, __wstream_base_actor_0>{
	public:
		using base_type = std::conditional_t<WithActor, __wstream_base_actor_1, __wstream_base_actor_0>;
		typedef typename Tsequence sequence_type_t;
		typedef typename sequence_type_t::value_type sequence_value_t;

	private:
		sequence_type_t _buf;

	public:
		wstream_sequence_adpt_t(){}

		template<typename XActor,
			typename _XActor = typename std::decay_t<XActor>,
			typename std::enable_if_t<(std::is_invocable_v<_XActor, uint8_t>), int> = 0>
		explicit wstream_sequence_adpt_t(XActor && actor)
			: base_type(std::forward<XActor>(actor)){}

		explicit wstream_sequence_adpt_t(sequence_type_t && obj)
			: _buf(std::move(obj)){}

		template<typename XContainer,
			typename XActor,
			typename _XActor = typename std::decay_t<XActor>,
			typename std::enable_if_t<is_container_of_bytes_v<XContainer>, int> = 0,
			typename std::enable_if_t<std::is_invocable_v<_XActor, uint8_t>, int> = 0>
		wstream_sequence_adpt_t(XContainer && obj, XActor && actor)
			: base_type(std::forward<XActor>(actor))
			, _buf(std::forward<XContainer>(obj)){}

		template<typename XContainer,
			typename std::enable_if_t<is_container_of_bytes_v<XContainer>, int> = 0>
		void assign(XContainer && obj){
			_buf = std::forward<XContainer>(obj);
			}

		template<typename XActor,
			typename _XActor = typename std::decay_t<XActor>,
			typename std::enable_if_t<std::is_invocable_v<_XActor, uint8_t>, int> = 0>
		void assign(XActor && actor){
			base_type::assign(std::forward<XActor>(actor));
			}

		template<typename XContainer,
			typename XActor,
			typename _XActor = typename std::decay_t<XActor>,
			typename std::enable_if_t<is_container_of_bytes_v<XContainer>, int> = 0,
			typename std::enable_if_t<std::is_invocable_v<_XActor, uint8_t>, int> = 0>
		void assign(XContainer && obj, XActor && actor){
			_buf = std::forward<XContainer>(obj);
			base_type::assign(std::forward<XActor>(actor));
			}

		template<typename TContainer,
			typename std::enable_if_t<is_container_of_bytes_v<TContainer>, int> = 0>
			typename TContainer copy2()const{
			return TContainer{_buf};
			}

		const sequence_type_t& sequence()const{
			return _buf;
			}

		sequence_type_t release(){
			sequence_type_t tmp;

			tmp.swap(_buf);
			return tmp;
			}

		template<typename XContainer,
			typename _XContainer = typename std::decay_t<XContainer>,
			typename std::enable_if_t<is_container_of_bytes_v<_XContainer>, int> = 0>
			auto get_reader()const{
			return rstream_constructor_t::make<_XContainer>{_XContainer{_buf}};
			}

		size_t size()const{
			return _buf.size();
			}

		size_t offset()const noexcept{
			return _buf.size();
			}

		size_t length()const{
			return _buf.size();
			}

	private:
		bool push_(std::uint8_t b) final{
			_buf.push_back(b);
			return true;
			}

		size_t push_(const uint8_t * first, const uint8_t *last)final{
			static_assert(is_contiguous_iterator_v<decltype(first)>, __FILE__);

			if (first <= last){
				const auto lsrc = std::distance(first, last);
				if (lsrc >= 0){
					const auto ulsrc = (size_t)lsrc;

					if constexpr (is_container_contiguous_of_bytes_v<decltype(_buf)>){
						static_assert(is_contiguous_iterator_v<decltype(_buf.begin())>, __FILE__);

						if (ulsrc){
							const auto inpos = _buf.size();
							_buf.resize(inpos + ulsrc);

							auto oit = std::copy(first, last, _buf.begin() + inpos);
							CBL_VERIFY(std::distance(_buf.begin() + inpos, oit) == lsrc);
							}
						}
					else{
						std::copy(first, last, std::back_inserter(_buf));
						}

					return ulsrc;
					}
				else{
					SERIALIZE_THROW_EXC_FWD(nullptr);
					}
				}
			else{
				SERIALIZE_THROW_EXC_FWD(nullptr);
				}
			}
	};
}
