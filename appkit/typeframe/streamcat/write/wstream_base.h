#pragma once


#include <type_traits>
#include <functional>
#include "../comutil/uwstreams.h"
#include "../read/rstreams.h"
#include "../../../functorapp/function_meta.h"


namespace crm::srlz::detail {


template<typename TFunctor,
	typename _TFunctor = typename std::decay_t<TFunctor>,
	typename std::enable_if_t< std::is_invocable_v<_TFunctor, uint8_t>, int > = 0 >
struct __wstream_actor_wrapper {
	using self_type = typename __wstream_actor_wrapper<TFunctor>;

	TFunctor _f;

	void operator()(uint8_t b) {
		_f(b);
		}

	template<typename XFunctor,
		typename _XFunctor = typename std::decay_t<XFunctor>,
		typename std::enable_if_t<!std::is_base_of_v<self_type, _XFunctor> && std::is_invocable_v<_XFunctor, uint8_t>, int > = 0 >
	explicit __wstream_actor_wrapper(XFunctor && f)
		: _f(std::forward<XFunctor>(f)) {}
	};


enum class wstream_base_push_type {
	actor_0,
	actor_1
	};

struct __wstream_base_actor_i{
	virtual ~__wstream_base_actor_i(){};

	virtual size_t offset()const noexcept = 0;
	virtual wstream_base_push_type type()const noexcept = 0;
	virtual bool push_(std::uint8_t) = 0;
	virtual size_t push_(const std::uint8_t * first, const std::uint8_t *last) = 0;
	};


struct __wstream_base_actor_xbase : 
	virtual __wstream_base_actor_i, 
	virtual stream_base_traits {

public:
	using self_type = __wstream_base_actor_xbase;

	__wstream_base_actor_xbase() noexcept{}

	template<typename XActor,
		typename _XActor = typename std::decay_t<XActor>,
		typename std::enable_if_t<!std::is_base_of_v<self_type, _XActor>, int> = 0>
	explicit __wstream_base_actor_xbase(XActor && af)noexcept{
		static_assert(false, __FILE__);
		}

	template<typename TValue,
		typename Pred,
		typename std::enable_if_t<is_byte_v<TValue>, int> = 0>
	bool write(TValue value, Pred && p) {
		static_assert(sizeof(value) == 1, __FILE__);
		/*! compiler only */
		(void)p;

		auto r = push_(value);
		if(r) {
			if constexpr(is_accumulate_invocable_v<Pred>) {
				p(value);
				}
			}

		return r;
		}

	template<typename It,
		typename Pred,
		typename std::enable_if_t<(
			is_iterator_byte_v<It> &&
			is_contiguous_iterator_v<It>
			), int> = 0>
	std::pair<It, size_t> push_c(It first, It last, Pred&& p) {
		//static_cast(std::is_lvalue_reference_v<decltype(*(first))>, __FILE_LINE__);

		/*! compiler only */
		(void)(p);

		const auto count_ = std::distance(first, last);
		if(count_ > 0) {
			const auto l_ = static_cast<size_t>(count_);
			if(l_ != push_(reinterpret_cast<const uint8_t*>(&(*(first))),
				reinterpret_cast<const uint8_t*>(&(*(first))) + l_)) {
				SERIALIZE_THROW_EXC_FWD(nullptr);
				}

			if constexpr(is_accumulate_invocable_v<Pred>) {
				for(size_t i = 0; i < l_; ++i, ++first) {
					p(*first);
					}
				}

			return std::make_pair(last, (size_t)count_);
			}
		else {
			return std::make_pair(first, 0);
			}
		}

	template<typename It,
		typename Pred,
		typename std::enable_if_t<(
			is_iterator_byte_v<It> &&
			!is_contiguous_iterator_v<It>
			), int> = 0>
	std::pair<It, size_t> push_c(It first, It last, Pred&& p) {
		size_t ic{ 0 };
		while(first != last ) {
			auto  b = (*first);
			push_(b);

			++ic;
			++first;

			if constexpr(is_accumulate_invocable_v<Pred>) {
				p(b);
				}
			}

		return std::make_pair(first,size_t);
		}


	template<typename It,
		typename Pred,
		typename std::enable_if_t<(
			is_iterator_byte_v<It> &&
			!is_contiguous_iterator_v<It>
			), int> = 0>
	std::pair<It, size_t> push_c(It first, size_t count_, Pred&& p) {

		size_t ic{ 0 };
		while(ic < count_) {
			auto  b = (*first);
			push_(b);

			++ic;
			++first;

			if constexpr(is_accumulate_invocable_v<Pred>) {
				p(b);
				}
			}

		return std::make_pair(first, ic);
		}

	template<typename C,
		typename Pred,
		typename std::enable_if_t<is_container_contiguous_of_bytes_v<C>, int> = 0>
	void push_c(const C& c, size_t off_, size_t count_, Pred&& p) {
		static_assert(std::is_pointer_v<decltype(std::declval<C>().data())> &&
			is_byte_v<decltype(*std::declval<C>().data())>, __FILE__);

		auto first = std::cbegin(c);
		std::advance(first, off_);

		decltype(first) last;
		if(count_ < all_available) {
			last = std::cbegin(c);
			std::advance(last, off_ + count_);
			}
		else {
			last = std::cend(c);
			}

		auto ret = push_c(first, last, std::forward<Pred>(p));
		if(ret.first != last) {
			SERIALIZE_THROW_EXC_FWD(nullptr);
			}
		else {
			CBL_VERIFY(ret.second == std::size(c));
			}
		}

	template<typename C,
		typename Pred,
		typename std::enable_if_t<is_container_contiguous_of_bytes_v<C>, int> = 0>
	void write(const C& c, Pred&& p, size_t off_ , size_t count_ ) {
		push_c(c,  off_, count_, std::forward<Pred>(p) );
		}

	template<typename C,
		typename Pred,
		typename std::enable_if_t<(
			!is_container_contiguous_of_bytes_v<C> && 
			is_container_v<C>
			), int> = 0>
	void write(const C & c, Pred && p, size_t off_ , size_t count_ ) {
		auto it = std::cbegin(c);
		std::advance(it, off_);

		push_c(it, count_, std::forward<Pred>(p));
		}

	template<typename TInputIt,
		typename Pred,
		typename std::enable_if_t<(
			is_iterator_byte_v<TInputIt> &&
			!is_contiguous_iterator_v<TInputIt>
			), int> = 0>
	std::pair<TInputIt, size_t> write(TInputIt first, TInputIt last, Pred && p) {
		return push_c(first, last, std::forward<Pred>(p));
		}

	template<typename TInputIt,
		typename Pred,
		typename std::enable_if_t<(
			is_iterator_byte_v<TInputIt> &&
			is_contiguous_iterator_v<TInputIt>
			), int> = 0>
	std::pair<TInputIt, size_t> write(TInputIt first, TInputIt last, Pred && p) {
		return push_c(first, last, std::forward<Pred>(p));
		}
	};


struct __wstream_base_actor_0 : __wstream_base_actor_xbase {
public:
	__wstream_base_actor_0()noexcept {}

	wstream_base_push_type type() const noexcept {
		return wstream_base_push_type::actor_0;
		}

	template<typename TValue,
		typename std::enable_if_t<is_byte_v<TValue>, int> = 0>
	bool write(TValue value) {
		static_assert(sizeof(value) == 1, __FILE__);
		return __wstream_base_actor_xbase::write(value, actor_null_dummy{});
		}

	template<typename C,
		typename std::enable_if_t<is_container_of_bytes_v<C>, int> = 0>
	void write(C&& c) {
		__wstream_base_actor_xbase::write(c, actor_null_dummy{}, 0, stream_base_traits::all_available);
		}

	template<typename C,
		typename std::enable_if_t<is_container_of_bytes_v<C>, int> = 0>
	void write(C&& c, size_t off_, size_t l_) {
		__wstream_base_actor_xbase::write(c, actor_null_dummy{}, off_, l_);
		}

	template<typename It,
		typename std::enable_if_t<is_iterator_byte_v<It>, int> = 0>
	std::pair<It, size_t> write(It first, It last) {
		return __wstream_base_actor_xbase::write(first, last, actor_null_dummy{});
		}
	};

struct __wstream_base_actor_1 : __wstream_base_actor_xbase {
private:
	utility::invoke_functor<void, uint8_t> _actor;

public:
	__wstream_base_actor_1()noexcept {}

	template<typename XActor,
		typename _XActor = typename std::decay_t<XActor>,
		typename std::enable_if_t<std::is_invocable_v<_XActor, uint8_t>, int> = 0>
	explicit __wstream_base_actor_1(XActor && af)noexcept
		: _actor(std::forward<XActor>(af)) {}

	wstream_base_push_type type() const noexcept{
		return wstream_base_push_type::actor_1;
		}

	template<typename XActor,
		typename _XActor = typename std::decay_t<XActor>,
		typename std::enable_if_t<std::is_invocable_v<_XActor, uint8_t>, int> = 0>
	void assign(XActor && actor) noexcept{
		_XActor tmp(std::forward< XActor>(actor));
		std::swap(_actor, tmp);
		}

	template<typename TValue,
		typename std::enable_if_t<is_byte_v<TValue>, int> = 0>
	bool write(TValue value) {
		static_assert(sizeof(value) == 1, __FILE__);
		return __wstream_base_actor_xbase::write(value, _actor);
		}

	template<typename C,
		typename std::enable_if_t<is_container_of_bytes_v<C>, int> = 0>
	void write(C&& c) {
		return __wstream_base_actor_xbase::write(c, _actor, 0, stream_base_traits::all_available);
		}

	template<typename C,
		typename std::enable_if_t<is_container_of_bytes_v<C>, int> = 0>
	void write(C && c, size_t off_, size_t l_) {
		return __wstream_base_actor_xbase::write(c, _actor, off_, l_);
		}

	template<typename It,
		typename std::enable_if_t<is_iterator_byte_v<It>, int> = 0>
	std::pair<It, size_t> write(It first, It last) {
		return __wstream_base_actor_xbase::write(first, last, _actor);
		}
	};
}


