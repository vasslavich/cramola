#pragma once


#include <chrono>
#include "../comutil/uwstreams.h"
#include "../../utype_trait_std.h"


namespace crm{
namespace srlz{


template<typename TOStream, typename Options = none_options>
void serialize(typename TOStream&& dest, const std::chrono::system_clock::time_point& tp, Options = {}) {

#ifdef USE_DCF_CHECK
	auto off = dest.offset();
#endif

	std_types_trait_t::streamed_time_point_t stp = tp.time_since_epoch().count();
	serialize(std::forward<TOStream>(dest), stp, serialized_options_cond<decltype(stp), Options>{});

#ifdef USE_DCF_CHECK
	CBL_VERIFY((dest.offset() - off) == object_trait_serialized_size(stp, serialized_options_cond<decltype(stp), Options>{}));
#endif
	}

template<typename TOStream, typename Rep, typename Period, typename Options = none_options>
void serialize( typename TOStream && dest, const std::chrono::duration<Rep, Period> & tp, Options = {}) {

#ifdef USE_DCF_CHECK
	auto off = dest.offset();
#endif

	std_types_trait_t::streamed_time_point_t stp = tp.count();
	serialize(std::forward<TOStream>(dest), stp, serialized_options_cond<decltype(stp), Options>{});

#ifdef USE_DCF_CHECK
	CBL_VERIFY((dest.offset() - off) == object_trait_serialized_size(stp, serialized_options_cond<decltype(stp), Options>{}));
#endif
	}

template<typename TOStream, typename TFirst, typename TSecond, typename Options = none_options>
void serialize(typename TOStream && dest, const typename std::pair<TFirst, TSecond> & tp, Options = {}) {

#ifdef USE_DCF_CHECK
	auto off = dest.offset();
#endif

	serialize(std::forward<TOStream>(dest), tp.first, serialized_options_cond<decltype(tp.first), Options>{});
	serialize(std::forward<TOStream>(dest), tp.second, serialized_options_cond<decltype(tp.second), Options>{});

#ifdef USE_DCF_CHECK
	CBL_VERIFY((dest.offset() - off) == (
		object_trait_serialized_size(tp.first, serialized_options_cond<decltype(tp.first), Options>{}) +
		object_trait_serialized_size(tp.second, serialized_options_cond<decltype(tp.second), Options>{})
		));
#endif
	}
}
}
