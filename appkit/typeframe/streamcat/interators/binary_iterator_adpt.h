//#pragma once
//
//
//#include <memory>
//#include <type_traits>
//#include "../../../notifications/internal_terms.h"
//#include "../../type_trait_base.h"
//
//
//namespace crm {
//namespace srlz {
//namespace detail {
//
//
//struct __i_byte_iterator_trait_base_t {
//	virtual ~__i_byte_iterator_trait_base_t() {}
//
//	using size_type = std::size_t;
//	using value_type = std::uint8_t;
//	using iterator_category = std::random_access_iterator_tag;
//	};
//
//struct __i_byte_const_iterator_trait_t : __i_byte_iterator_trait_base_t {
//	using reference = const value_type&;
//	using pointer = const value_type*;
//	using difference_type = typename std::decay<decltype(std::distance(std::declval<pointer>(), std::declval<pointer>()))>::type;
//	};
//
//struct __i_byte_iterator_trait_t : __i_byte_iterator_trait_base_t {
//	using reference = value_type&;
//	using pointer = value_type*;
//	using difference_type = typename std::decay<decltype(std::distance(std::declval<pointer>(), std::declval<pointer>()))>::type;
//	};
//
//struct __i_byte_const_iterator_t : __i_byte_const_iterator_trait_t {
//
//	virtual void inc(size_t off) = 0;
//	virtual reference operator_dereference()const = 0;
//	virtual pointer operator_to_pointer()const = 0;
//	virtual std::unique_ptr<__i_byte_const_iterator_t> copy()const = 0;
//	virtual bool eq(const std::unique_ptr<__i_byte_const_iterator_t> & o)const = 0;
//	virtual size_t base_offset()const noexcept = 0;
//	};
//
//struct __i_byte_iterator_t : __i_byte_iterator_trait_t {
//
//	virtual void inc(size_t off) = 0;
//	virtual reference operator_dereference()const = 0;
//	virtual pointer operator_to_pointer()const = 0;
//	virtual std::unique_ptr<__i_byte_iterator_t> copy()const = 0;
//	virtual bool eq(const std::unique_ptr<__i_byte_iterator_t> & o)const = 0;
//	};
//
//
//template<typename TIterator>
//class __byte_const_iterator_adpt final : public __i_byte_const_iterator_t {
//	static_assert(is_iterator_byte_t<TIterator>::value, "invalid iterator type");
//
//public:
//	using iterator_type = std::decay_t<TIterator>;
//	using self_type = __byte_const_iterator_adpt<iterator_type>;
//
//	__byte_const_iterator_adpt() = delete;
//
//	__byte_const_iterator_adpt(iterator_type begin_, iterator_type end_)
//		: _curr(begin_)
//		, _end(end_) {}
//
//	void inc(size_t off) final {
//		CBL_VERIFY( off == 1 );
//		++_curr;
//		++_offset;
//		}
//
//	reference operator_dereference()const final {
//		return *_curr;
//		}
//
//	pointer operator_to_pointer()const final {
//		return &(*_curr);
//		}
//
//	std::unique_ptr<__i_byte_const_iterator_t> copy()const final{
//		return std::make_unique<self_type>(_curr, _end);
//		}
//
//	bool eq(const std::unique_ptr<__i_byte_const_iterator_t> & o)const final {
//		return _curr == dynamic_cast<const self_type*>(o.get())->_curr;
//		}
//
//	size_t base_offset()const noexcept final{
//		return _offset;
//		}
//
//private:
//	iterator_type _curr;
//	iterator_type _end;
//	size_t _offset{ 0 };
//	};
//
//
//struct __byte_const_iterator_t : public __i_byte_const_iterator_trait_t {
//private:
//	std::unique_ptr<__i_byte_const_iterator_t> _pIt;
//
//public:
//	using self_type = __byte_const_iterator_t;
//
//	__byte_const_iterator_t() {}
//
//	__byte_const_iterator_t(std::unique_ptr<__i_byte_const_iterator_t> && it)
//		: _pIt(std::move(it)) {}
//
//	__byte_const_iterator_t(const __byte_const_iterator_t &o)
//		: _pIt(o._pIt->copy()) {}
//
//	__byte_const_iterator_t& operator=(const __byte_const_iterator_t & o) {
//		if(this != std::addressof(o)) {
//			_pIt = o._pIt->copy();
//			}
//
//		return (*this);
//		}
//
//	__byte_const_iterator_t(__byte_const_iterator_t && o)
//		: _pIt(std::move(o._pIt)) {}
//
//	__byte_const_iterator_t& operator=(__byte_const_iterator_t && o) {
//		if(this != std::addressof(o)) {
//			_pIt = std::move(o._pIt);
//			}
//
//		return (*this);
//		}
//
//
//	self_type& operator++() {
//		_pIt->inc(1);
//		return (*this);
//		}
//
//	self_type& operator+=(size_t off) {
//		_pIt->inc(off);
//		return (*this);
//		}
//
//	reference operator*()const {
//		return _pIt->operator_dereference();
//		}
//
//	pointer operator->()const {
//		return _pIt->operator_to_pointer();
//		}
//
//	std::ptrdiff_t base_offset()const{
//		return _pIt->base_offset();
//		}
//
//	friend bool operator==(const self_type& l, const self_type& r) { return l._pIt->eq( r._pIt ); }
//	friend bool operator!=(const self_type& l, const self_type& r) { return !(l == r); }
//	};
//
//
//
//template<typename TIt,
//	typename std::enable_if_t<!is_contiguous_iterator_v<TIt>, int> = 0>
//std::ptrdiff_t iterator_distance( const typename TIt & b, const typename TIt & e ){
//	FATAL_ERROR_FWD(nullptr);
//	}
//
//template<typename TIt,
//	typename std::enable_if_t<is_contiguous_iterator_v<TIt>, int> = 0>
//std::ptrdiff_t iterator_distance( const typename TIt & b, const typename TIt & e ){
//	return std::distance( b, e );
//	}
//}
//}
//}
