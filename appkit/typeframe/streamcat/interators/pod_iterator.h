#pragma once


#include <type_traits>
#include <iterator>
#include <cstdint>
#include "../../../notifications/internal_terms.h"
#include ".../../../../type_trait_base.h"


namespace crm{
namespace srlz{
namespace detail{


template<typename _Tx,
	typename T = typename std::decay_t<_Tx>>
class __pointer_iterator_t {
public:
	typedef std::size_t size_type;
	typedef typename T value_type;
	typedef __pointer_iterator_t<value_type> self_type;
	typedef value_type& reference;
	typedef value_type* pointer;
	typedef std::random_access_iterator_tag iterator_category;
	typedef typename std::decay<decltype(std::distance( std::declval<pointer>(), std::declval<pointer>() ))>::type difference_type;

	__pointer_iterator_t( pointer ptr, pointer up )
		: ptr_( ptr )
		, _up( up ) {}

	self_type operator++() {
		++ptr_;
		return (*this);
		}

	self_type operator++(int /*postfix*/) {
		auto temp( *this );
		++ptr_;
		return temp;
		}

	self_type operator+=(size_t off) {
		ptr_ += off;
		return (*this);
		}

	self_type operator+(size_t off)const {
		auto temp( *this );
		temp += off;
		return std::move( temp );
		}

	value_type& operator*() {
		if( ptr_ < _up )
			return *ptr_;
		else {
			THROW_EXC_FWD(nullptr);
			}
		}

	const value_type& operator*()const {
		if( ptr_ < _up )
			return *ptr_;
		else {
			THROW_EXC_FWD(nullptr);
			}
		}

	pointer operator->() {
		return ptr_;
		}

	friend bool operator==(const self_type& l, const self_type& r) { return l.ptr_ == r.ptr_; }
	friend bool operator!=(const self_type& l, const self_type& r) { return l.ptr_ != r.ptr_; }
	friend bool operator>(const self_type& l, const self_type& r) { return l.ptr_ > r.ptr_; }
	friend bool operator<(const self_type& l, const self_type& r) { return l.ptr_ < r.ptr_; }
	friend bool operator>=(const self_type& l, const self_type& r) { return l.ptr_ >= r.ptr_; }
	friend bool operator<=(const self_type& l, const self_type& r) { return l.ptr_ <= r.ptr_; }

private:
	pointer ptr_;
	pointer _up;
	};


template<typename _Tx,
	typename T = typename std::decay_t<_Tx>>
class __const_pointer_iterator_t {
public:
	typedef std::size_t size_type;
	typedef typename T value_type;
	typedef __const_pointer_iterator_t<value_type> self_type;
	typedef value_type& reference;
	typedef const value_type* pointer;
	typedef typename std::decay<decltype(std::distance( std::declval<pointer>(), std::declval<pointer>() ))>::type difference_type;
	typedef std::random_access_iterator_tag iterator_category;

	__const_pointer_iterator_t( pointer ptr, pointer up )
		: ptr_( ptr )
		, _up( up ) {}

	self_type operator++() {
		++ptr_;
		return (*this);
		}

	self_type operator++(int /*postfix*/) {
		auto temp( *this );
		++ptr_;
		return temp;
		}

	self_type operator+=(size_t off) {
		ptr_ += off;
		return (*this);
		}

	self_type operator+(size_t off)const {
		auto temp( *this );
		temp += off;
		return std::move( temp );
		}

	const value_type& operator*()const {
		if( ptr_ < _up )
			return *ptr_;
		else {
			THROW_EXC_FWD(nullptr);
			}
		}

	const pointer operator->() {
		return ptr_;
		}

	friend bool operator==(const self_type& l, const self_type& r) { return l.ptr_ == r.ptr_; }
	friend bool operator!=(const self_type& l, const self_type& r) { return l.ptr_ != r.ptr_; }
	friend bool operator>(const self_type& l, const self_type& r) { return l.ptr_ > r.ptr_; }
	friend bool operator<(const self_type& l, const self_type& r) { return l.ptr_ < r.ptr_; }
	friend bool operator>=(const self_type& l, const self_type& r) { return l.ptr_ >= r.ptr_; }
	friend bool operator<=(const self_type& l, const self_type& r) { return l.ptr_ <= r.ptr_; }

private:
	pointer ptr_;
	pointer _up;
	};
}



class make_binary_iterator_std_checked_t{
public:
	typedef stdext::checked_array_iterator<uint8_t*> iterator_t;
	static_assert(detail::is_contiguous_iterator_v<iterator_t>, __FILE__);

	static iterator_t create( uint8_t *first, size_t size, size_t offset );
	};

class make_const_binary_iterator_std_checked_t {
public:
	typedef stdext::checked_array_iterator<const uint8_t*> iterator_t;
	static_assert(detail::is_contiguous_iterator_v<iterator_t>, __FILE__);

	static iterator_t create( const uint8_t *first, size_t size, size_t offset );
	};


class make_binary_iterator_raw_t {
public:
	typedef detail::__pointer_iterator_t<uint8_t> iterator_t;
	static_assert(detail::is_contiguous_iterator_v<iterator_t>, __FILE__);

	static iterator_t create( uint8_t *first,  size_t size, size_t offset  );
	};

class make_const_binary_iterator_raw_t {
public:
	typedef detail::__const_pointer_iterator_t<uint8_t> iterator_t;
	static_assert(detail::is_contiguous_iterator_v<iterator_t>, __FILE__);

	static iterator_t create( const uint8_t *first,  size_t size, size_t offset  );
	};



#ifdef CBL_USE_STD_CHECKED_ITERATORS
using make_binary_iterator_t = make_binary_iterator_std_checked_t;
using make_const_binary_iterator_t = make_const_binary_iterator_std_checked_t;
#else
using make_binary_iterator_t = make_binary_iterator_raw_t;
using make_const_binary_iterator_t = make_const_binary_iterator_raw_t;
#endif

namespace detail{

template<typename _Tx,
	typename TIteratorMaker = make_const_binary_iterator_t,
	typename T = typename std::decay_t<_Tx>,
	typename std::enable_if_t<is_mono_serializable_v<T>, int> = 0>
struct const_binary_iterator_adapter_t {
	using base_type = typename T;
	typedef typename TIteratorMaker::iterator_t iterator;
	typedef typename TIteratorMaker::iterator_t const_iterator;
	using iterator_category = typename iterator::iterator_category;

	typedef std::size_t size_type;
	using value_type = typename std::decay_t<decltype(*(std::declval<iterator>()))>;
	typedef typename const_binary_iterator_adapter_t<T> self_t;

	static_assert(is_byte_v<value_type>, __FILE__);

	const_iterator begin() const {
		return TIteratorMaker::create( _begin, _size, 0 );
		}

	const_iterator end() const {
		return TIteratorMaker::create( _begin, _size, _size );
		}

	const_iterator cbegin() const {
		return TIteratorMaker::create( _begin, _size, 0 );
		}

	const_iterator cend() const {
		return TIteratorMaker::create( _begin, _size, _size );
		}

	const_binary_iterator_adapter_t( const self_t & ) = delete;
	const_binary_iterator_adapter_t( self_t && ) = delete;

	self_t& operator=(const self_t &) = delete;
	self_t& operator=(self_t &&) = delete;

	template<typename XBaseValue,
		typename _XBaseValue = typename std::decay_t<XBaseValue>,
		typename std::enable_if_t<is_mono_serializable_v<_XBaseValue> && sizeof(base_type) == sizeof(_XBaseValue), int> = 0>
	const_binary_iterator_adapter_t(XBaseValue && v)
		: _baseValue(std::forward<XBaseValue>(v))
		, _begin((const uint8_t*)&_baseValue)
		, _size(sizeof(_baseValue)) {}

	size_type size()const noexcept {
		return _size;
		}

	const value_type* data()const noexcept {
		return _begin;
		}

	const value_type& operator[](size_type idx)const noexcept{
		return _begin[idx];
		}

private:
	base_type _baseValue;
	const value_type *_begin = nullptr;
	size_type _size = 0;
	};



template<typename _Tx,
	typename TIteratorMaker = make_binary_iterator_t,
	typename TConstIteratorMaker = make_const_binary_iterator_t,
	typename T = typename std::decay_t<_Tx>,
	typename std::enable_if_t<is_mono_serializable_v<T>, int> = 0>
struct binary_iterator_adapter_t {
	using base_type = typename T;
	typedef typename TIteratorMaker::iterator_t iterator;
	typedef typename TConstIteratorMaker::iterator_t const_iterator;
	using iterator_category = typename iterator::iterator_category;

	typedef std::size_t size_type;
	using value_type = typename std::decay_t<decltype(*(std::declval<iterator>()))>;
	typedef typename binary_iterator_adapter_t<T> self_t;

	static_assert(is_byte_v<value_type>, __FILE__);




	iterator begin() {
		return TIteratorMaker::create( _begin, _size, 0 );
		}

	iterator end() {
		return TIteratorMaker::create( _begin, _size, _size );
		}

	const_iterator begin() const {
		return TConstIteratorMaker::create( _begin, _size, 0 );
		}

	const_iterator end() const {
		return TConstIteratorMaker::create(_begin, _size, _size );
		}

	const_iterator cbegin() const {
		return TConstIteratorMaker::create(_begin, _size, 0 );
		}

	const_iterator cend() const {
		return TConstIteratorMaker::create(_begin, _size, _size );
		}

	binary_iterator_adapter_t( const self_t & ) = delete;
	binary_iterator_adapter_t( self_t && ) = delete;

	self_t& operator=(const self_t &) = delete;
	self_t& operator=(self_t &&) = delete;


	binary_iterator_adapter_t(){
		_begin = (uint8_t*)&_baseValue;
		_size = sizeof(_baseValue);
		}

	size_type size()const noexcept {
		return _size;
		}

	const value_type* data()const noexcept {
		return _begin;
		}

	value_type* data() noexcept{
		return _begin;
		}

	const value_type& operator[](size_type idx)const noexcept{
		return _begin[idx];
		}

	value_type& operator[](size_type idx) noexcept{
		return _begin[idx];
		}

	const base_type& base_value()const noexcept {
		return _baseValue;
		}

private:
	base_type _baseValue{};
	uint8_t *_begin{ nullptr };
	size_type _size{ 0 };
	};


template<typename _Tx,
	typename T = typename std::decay_t<_Tx>,
	typename std::enable_if_t<is_mono_serializable_v<T>, int> = 0>
struct const_binary_iterator_pointer_t {
	using base_type = typename T;
	using iterator = const uint8_t*;
	using const_iterator = const uint8_t*;
	using size_type = std::size_t ;
	using value_type = typename std::decay_t<decltype(*(std::declval<iterator>()))>;
	typedef typename const_binary_iterator_pointer_t<_Tx> self_t;
	typedef std::random_access_iterator_tag iterator_category;

	static_assert(is_byte_v<value_type>, __FILE__);

	const_iterator begin() const {
		return _begin;
		}

	const_iterator end() const {
		return _begin + _size;
		}

	const_iterator cbegin() const {
		return _begin;
		}

	const_iterator cend() const {
		return _begin + _size;
		}

	const_binary_iterator_pointer_t( const self_t & ) = delete;
	const_binary_iterator_pointer_t(self_t && o)noexcept = delete;

	self_t& operator=(const self_t &) = delete;
	self_t& operator=(self_t && o)noexcept = delete;

	template<typename XBaseValue,
		typename _XBaseValue = typename std::decay_t<XBaseValue>,
		typename std::enable_if_t<is_mono_serializable_v<_XBaseValue> && sizeof( base_type ) == sizeof( _XBaseValue ), int> = 0>
	void assign( XBaseValue && v )noexcept{
		_XBaseValue tmp(std::forward<XBaseValue>(v));
		std::swap(_baseValue, tmp);

		_begin = (const uint8_t*)std::addressof( _baseValue );
		_size = sizeof( _baseValue );
		}

	template<typename XBaseValue,
		typename _XBaseValue = typename std::decay_t<XBaseValue>,
		typename std::enable_if_t<is_mono_serializable_v<_XBaseValue> && sizeof(base_type) == sizeof(_XBaseValue), int> = 0>
		const_binary_iterator_pointer_t(XBaseValue && v)
		: _baseValue(std::forward<XBaseValue>(v))
		, _begin((const uint8_t*)std::addressof(_baseValue))
		, _size(sizeof(_baseValue)) {}

	size_type size()const noexcept {
		return _size;
		}

	const value_type* data()const noexcept {
		return _begin;
		}

	const value_type& operator[](size_type idx)const noexcept{
		return _begin[idx];
		}

private:
	base_type _baseValue;
	const value_type *_begin = nullptr;
	size_type _size = 0;
	};
}


template<typename TInIt, typename T,
	typename std::enable_if_t<detail::is_mono_serializable_v<T>, int> = 0>
void copy( typename TInIt first, typename TInIt last, typename T & dest ) {

	detail::binary_iterator_adapter_t<T> d( dest );
	std::copy( first, last, d.begin() );
	}

template<typename TOutIt, typename T,
	typename std::enable_if_t<detail::is_mono_serializable_v<T>, int> = 0>
void copy( const typename T & src, typename TOutIt dest ) {

	detail::const_binary_iterator_adapter_t<T> s( src );
	std::copy( s.cbegin(), s.cend(), dest );
	}
}
}
