#include "../../../../internal.h"
#include "../pod_iterator.h"


using namespace crm;
using namespace crm::srlz;
using namespace crm::srlz::detail;



make_binary_iterator_std_checked_t::iterator_t make_binary_iterator_std_checked_t::create( uint8_t *first,
	size_t size, size_t offset ) {

	if( offset <= size )
		return stdext::make_checked_array_iterator( first, size, offset );
	else
		THROW_EXC_FWD(nullptr);
	}

make_const_binary_iterator_std_checked_t::iterator_t make_const_binary_iterator_std_checked_t::create(
	const uint8_t *first, size_t size, size_t offset ) {

	if( offset <= size )
		return stdext::make_checked_array_iterator( first, size, offset );
	else
		THROW_EXC_FWD(nullptr);
	}


make_binary_iterator_raw_t::iterator_t make_binary_iterator_raw_t::create( uint8_t *first,
	size_t size, size_t offset ) {

	if( offset <= size ) {
		return iterator_t( first + offset, first + size );
		}
	else
		THROW_EXC_FWD(nullptr);
	}

make_const_binary_iterator_raw_t::iterator_t make_const_binary_iterator_raw_t::create( 
	const uint8_t *first, size_t size, size_t offset  ) {

	if( offset <= size ) {
		return iterator_t( first + offset, first + size );
		}
	else
		THROW_EXC_FWD(nullptr);
	}

