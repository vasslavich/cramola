#pragma once


#include <type_traits>
#include <functional>
#include "../comutil/urstreams.h"
#include "../interators/pod_iterator.h"
#include "../interators/rstream_iterator.h"



namespace crm::srlz::detail {


template<typename _TIterator,
	const bool WithActor,
	typename = std::enable_if_t<!is_contiguous_iterator_v<_TIterator>>,
	typename TBaseType = std::conditional_t<WithActor, __rstream_base_actor_1, __rstream_base_actor_0>>
	class rstream_iterator_adaptor_t final :
	public std::conditional_t<WithActor, __rstream_base_actor_1, __rstream_base_actor_0>,
	virtual stream_base_traits {

	public:
		typedef typename std::decay_t<_TIterator> iterator_type_t;
		typedef typename std::iterator_traits<iterator_type_t>::value_type iterator_value_t;
		typedef typename rstream_iterator_adaptor_t<iterator_type_t, WithActor> self_t;
		using base_type = TBaseType;

	private:
		iterator_type_t _current;
		iterator_type_t _end;
		std::size_t _offset{ 0 };

	public:
		rstream_iterator_adaptor_t()noexcept {}
		rstream_iterator_adaptor_t(const rstream_iterator_adaptor_t&) = delete;
		rstream_iterator_adaptor_t& operator=(const rstream_iterator_adaptor_t&) = delete;

		template<typename TInputIt,
			typename std::enable_if_t<is_iterator_byte_v<TInputIt>, int> = 0>
			rstream_iterator_adaptor_t(TInputIt&& begin, TInputIt&& end)
			: _current(std::forward<TInputIt>(begin))
			, _end(std::forward<TInputIt>(end)) {}

		template<typename TInputIt,
			typename XActor,
			typename _XActor = typename std::decay_t<XActor>,
			typename std::enable_if_t<is_iterator_byte_v<TInputIt>, int> = 0,
			typename std::enable_if_t<is_invoke_with_byte_argument_v<_XActor>, int> = 0>
			rstream_iterator_adaptor_t(TInputIt&& begin, TInputIt&& end, XActor&& actor)
			: base_type(std::forward<XActor>(actor))
			, _current(std::forward<TInputIt>(begin))
			, _end(std::forward< TInputIt>(end)) {}

		rstream_iterator_adaptor_t(rstream_iterator_adaptor_t&& o)noexcept {
			assign(std::forward<rstream_iterator_adaptor_t >(o));
			}

		rstream_iterator_adaptor_t& operator=(rstream_iterator_adaptor_t&& o)noexcept {
			if(this != std::addressof(o)) {
				assign(std::forward<rstream_iterator_adaptor_t >(o));
				}

			return (*this);
			}

		template<typename TInputIt,
			typename std::enable_if_t<is_iterator_byte_v<TInputIt>, int> = 0>
			void assign(TInputIt&& begin, TInputIt&& end)noexcept {
			_current = std::forward<TInputIt>(begin);
			_end = std::forward< TInputIt>(end);
			_offset = 0;
			}

		template<typename TInputIt,
			typename XActor,
			typename _XActor = typename std::decay_t<XActor>,
			typename std::enable_if_t<is_iterator_byte_v<TInputIt>, int> = 0,
			typename std::enable_if_t<is_invoke_with_byte_argument_v<_XActor>, int> = 0>
			void assign(TInputIt&& begin, TInputIt&& end, XActor&& actor)noexcept {
			base_type::assign(std::forward<XActor>(actor));

			_current = std::forward<TInputIt>(begin);
			_end = std::forward<TInputIt>(end);
			_offset = 0;
			}

		void assign(rstream_iterator_adaptor_t&& o)noexcept {
			base_type::assign(std::move(o));

			_current = std::move(o._current);
			_end = std::move(o._end);
			_offset = o._offset;
			}

		bool eof()const noexcept {
			return _current == _end;
			}

	private:
		bool pop_(uint8_t& dest) {
			if(!eof()) {
				dest = (*_current);

				++_current;
				++_offset;

				return true;
				}
			else {
				return false;
				}
			}

	public:
		size_t tail_size()const noexcept {
			return all_available;
			}

		size_t offset()const noexcept {
			return _offset;
			}

	private:
		size_t pop_(std::uint8_t* first, std::uint8_t* last) {
			if(first <= last) {
				const auto targetL_ = std::distance(first, last);
				if(targetL_ >= 0) {
					const auto targetL = (size_t)targetL_;
					const auto off0 = _offset;

					while(first != last && !eof()) {
						(*first) = (*_current);

						++first;
						++_current;
						++_offset;
						}

					if((_offset - off0) != targetL_) {
						auto e = eof();
						int j = 0;
						}

					return _offset - off0;
					}
				else {
					return 0;
					}
				}
			else {
				return 0;
				}
			}

	public:
		size_t length()const noexcept {
			return sizel();
			}

		/*������� ������� ����� ������*/
		size_t shifft(size_t count) {
			size_t off = 0;
			while(off < count && !eof()) {
				++off;
				++_current;
				++_offset;
				}

			return off;
			}

		iterator_type_t current()const {
			return _current;
			}

		iterator_type_t begin()const {
			FATAL_ERROR_FWD(nullptr);
			}

		iterator_type_t end()const {
			return _end;
			}

		iterator_tag cathegory()const noexcept {
			return iterator_cathegory<decltype(current())>();
			}
	};

template<typename _TIterator,
	const bool WithActor,
	typename = std::enable_if_t<is_contiguous_iterator_v<_TIterator>>,
	typename TBaseType = std::conditional_t<WithActor, __rstream_base_actor_1, __rstream_base_actor_0>>
class rstream_iterator_adaptor_contiguous_t final :
	public std::conditional_t<WithActor, __rstream_base_actor_1, __rstream_base_actor_0>,
	virtual stream_base_traits {

	public:
		typedef typename std::decay_t<_TIterator> iterator_type_t;
		typedef typename std::iterator_traits<iterator_type_t>::value_type iterator_value_t;
		typedef typename rstream_iterator_adaptor_contiguous_t<iterator_type_t, WithActor> self_t;
		using base_type = TBaseType;

	private:
		iterator_type_t _begin;
		iterator_type_t _end;

		iterator_type_t _current;
		std::size_t _offset{ 0 };

		mutable size_t _sizel{ serialized_base_definitions::not_available_value };

	public:
		rstream_iterator_adaptor_contiguous_t()noexcept{}

		rstream_iterator_adaptor_contiguous_t(const rstream_iterator_adaptor_contiguous_t&) = delete;
		rstream_iterator_adaptor_contiguous_t& operator=(const rstream_iterator_adaptor_contiguous_t&) = delete;

		template<typename It,
			typename std::enable_if_t<(
				is_iterator_byte_v<It> &&
				!std::is_pointer_v<It>
				), int> = 0>
		rstream_iterator_adaptor_contiguous_t(It&& begin_, It&& end_)
			: _begin(std::forward<It>(begin_))
			, _end(std::forward<It>(end_)) {

			_current = _begin;
			}

		template<typename T>
		rstream_iterator_adaptor_contiguous_t(const T* begin_, const T* end_)
			: _begin(begin_)
			, _end(end_) 
			, _current(begin_){}

		template<typename It,
			typename XActor,
			typename _XActor = typename std::decay_t<XActor>,
			typename std::enable_if_t<is_iterator_byte_v<It>, int> = 0,
			typename std::enable_if_t<is_invoke_with_byte_argument_v<_XActor>, int> = 0>
		rstream_iterator_adaptor_contiguous_t(It&& begin_, It&& end_, XActor&& actor)
			: base_type(std::forward<XActor>(actor))
			, _begin(std::forward<It>(begin_))
			, _end(std::forward<It>(end_)) {

			_current = _begin;
			}

		template<typename T,
			typename XActor,
			typename _XActor = typename std::decay_t<XActor>,
			typename std::enable_if_t<is_invoke_with_byte_argument_v<_XActor>, int> = 0>
		rstream_iterator_adaptor_contiguous_t(const T* begin_, const T* end_, XActor&& actor)
			: base_type(std::forward<XActor>(actor))
			, _begin(begin_)
			, _end(end_)
			, _current(begin_){}

		rstream_iterator_adaptor_contiguous_t(rstream_iterator_adaptor_contiguous_t&& o)noexcept {
			assign(std::forward<rstream_iterator_adaptor_contiguous_t >(o));
			}

		rstream_iterator_adaptor_contiguous_t& operator=(rstream_iterator_adaptor_contiguous_t&& o)noexcept {
			if(this != std::addressof(o)) {
				assign(std::forward<rstream_iterator_adaptor_contiguous_t >(o));
				}

			return (*this);
			}

		template<typename It,
			typename std::enable_if_t<(
				is_iterator_byte_v<It> &&
				!std::is_pointer_v<It>
				), int> = 0>
		void assign(It&& begin_, It&& end_)noexcept {
			_begin = std::forward<It>(begin_);
			_end = std::forward<It>(end_);
			_current = _begin;
			_offset = 0;
			}

		template<typename T>
		void assign(const T* begin_, const T* end_)noexcept {
			_begin = begin_;
			_end = end_;
			_current = _begin;
			_offset = 0;
			}

		template<typename It,
			typename XActor,
			typename _XActor = typename std::decay_t<XActor>,
			typename std::enable_if_t<(
				is_iterator_byte_v<It> &&
				!std::is_pointer_v<It> && 
				is_invoke_with_byte_argument_v<_XActor>
				), int> = 0>
		void assign(It&& begin_, It&& end_, XActor&& actor)noexcept {
			base_type::assign(std::forward<XActor>(actor));

			_begin = std::forward<It>(begin_);
			_end = std::forward<It>(end_);
			_current = _begin;
			_offset = 0;
			}

		template<typename T,
			typename XActor,
			typename _XActor = typename std::decay_t<XActor>,
			typename std::enable_if_t<is_invoke_with_byte_argument_v<_XActor>, int> = 0>
		void assign(const T* begin_, const T* end_, XActor&& actor)noexcept {
			base_type::assign(std::forward<XActor>(actor));

			_begin = begin_;
			_end = end_;
			_current = begin_;
			_offset = 0;
			}

		void assign(rstream_iterator_adaptor_contiguous_t&& o)noexcept {
			base_type::assign(std::move(o));

			_begin = std::move(o._begin);
			_current = std::move(o._current);
			_end = std::move(o._end);
			_offset = o._offset;
			_sizel = o._sizel;
			}

		bool eof()const noexcept {
			return _current == _end;
			}

	private:
		bool pop_(uint8_t& dest) {
			if(!eof()) {
				dest = (*_current);

				++_current;
				++_offset;

				return true;
				}
			else {
				return false;
				}
			}

		size_t sizel()const {
			if(serialized_base_definitions::not_available_value == _sizel) {
				_sizel = std::distance(_begin, _end);
				}

			return _sizel;
			}

	public:
		size_t tail_size()const noexcept {
			return sizel() - _offset;
			}

		size_t offset()const noexcept {
			return _offset;
			}

	private:
		size_t pop_(std::uint8_t* first, std::uint8_t* last) {
			if(first <= last) {
				const auto targetL_ = std::distance(first, last);
				if(targetL_ >= 0) {
					const auto targetL = (size_t)targetL_;
					const auto srcL = tail_size();
					const auto copyL = targetL <= srcL ? targetL : srcL;

					auto srcBegin = _current;
					auto srcEnd = srcBegin;
					std::advance(srcEnd, copyL);

					std::copy(srcBegin, srcEnd, first);

					_current = srcEnd;
					_offset += copyL;

					return copyL;
					}
				else {
					return 0;
					}
				}
			else {
				return 0;
				}
			}

	public:
		size_t length()const noexcept {
			return sizel();
			}

		/*������� ������� ����� ������*/
		size_t shifft(size_t count) {
			size_t off = std::min<size_t>(std::distance(_current, _end), count);

			std::advance(_current, off);
			_offset += off;

			return off;
			}

		iterator_type_t current()const {
			return _current;
			}

		iterator_type_t begin()const {
			return _begin;
			}

		iterator_type_t end()const {
			return _end;
			}

		iterator_tag cathegory()const noexcept {
			return iterator_cathegory<decltype(current())>();
			}
	};
}


