#pragma once


#include <type_traits>
#include <functional>
#include "../comutil/uwstreams.h"
#include "../read/rstreams.h"


namespace crm::srlz::detail {


template<typename TOutIt,
	const bool WithActor,
	typename TBaseType = std::conditional_t<WithActor, __wstream_base_actor_1, __wstream_base_actor_0>>
	class wstream_iterator_adpt_t final :
	public std::conditional_t<WithActor, __wstream_base_actor_1, __wstream_base_actor_0>,
	virtual stream_base_traits {

	public:
		using base_type = std::conditional_t<WithActor, __wstream_base_actor_1, __wstream_base_actor_0>;
		using self_type = typename wstream_iterator_adpt_t<TOutIt, WithActor>;

		typedef typename TOutIt iterator_t;
		typedef typename std::iterator_traits<TOutIt>::value_type iterator_value_t;

	private:
		iterator_t _begin;
		iterator_t _end;
		iterator_t _wOffset;
		size_t _offset{ 0 };

	public:
		template<typename T,
			typename = std::enable_if_t<std::is_base_of_v<__wstream_base_actor_i, std::decay_t<T>>>>
			wstream_iterator_adpt_t(const T&) = delete;

		template<typename T,
			typename = std::enable_if_t<std::is_base_of_v<__wstream_base_actor_i, std::decay_t<T>>>>
			self_type & operator=(const T&) = delete;

		template<typename XActor,
			typename _XActor = typename std::decay_t<XActor>,
			typename std::enable_if_t<(std::is_invocable_v<_XActor, uint8_t>), int> = 0>
			wstream_iterator_adpt_t(iterator_t begin, iterator_t end, XActor&& actor)
			: base_type(std::forward<XActor>(actor))
			, _begin(begin)
			, _end(end)
			, _wOffset(begin) {}

		wstream_iterator_adpt_t(iterator_t begin, iterator_t end)
			: _begin(begin)
			, _end(end)
			, _wOffset(begin) {}

		void assign(wstream_iterator_adpt_t&& o) {
			_begin = std::move(o._begin);
			_end = std::move(o._end);
			_wOffset = std::move(o._wOffset);
			_offset = o._offset;

			if constexpr(WithActor) {
				base_type::assign(o.actor_release());
				}
			}

		void assign(iterator_t begin, iterator_t end) {
			_begin = begin;
			_end = end;
			_wOffset = begin;
			}

		template<typename XActor,
			typename _XActor = typename std::decay_t<XActor>,
			typename std::enable_if_t<(std::is_invocable_v<_XActor, uint8_t>), int> = 0>
			void assign(iterator_t begin, iterator_t end, XActor&& actor) {
			_begin = begin;
			_end = end;
			_wOffset = begin;

			base_type::assign(std::forward<XActor>(actor));
			}

		template<typename TContainer,
			typename std::enable_if_t<is_container_of_bytes_v<TContainer>, int> = 0>
			typename TContainer copy2()const {
			return TContainer{ _begin, _wOffset };
			}

		template<typename XContainer,
			typename _XContainer = typename std::decay_t<XContainer>,
			typename std::enable_if_t<is_container_of_bytes_v<_XContainer>, int> = 0>
			auto get_reader()const {
			return rstream_constructor_t::make<_XContainer>(copy2<_XContainer>());
			}

		size_t offset()const noexcept {
			return _offset;
			}

		size_t length()const noexcept {
			return i_rstream_t::not_available_value;
			}

		iterator_t current() {
			return _wOffset;
			}

		iterator_t begin() {
			return _begin;
			}

		iterator_t end() {
			return _end;
			}

	private:
		bool push_(std::uint8_t b) final {
			if(_wOffset != _end) {

				(*_wOffset) = b;
				++_wOffset;
				++_offset;

				return true;
				}
			else {
				return false;
				}
			}

		size_t push_(const uint8_t* first, const uint8_t* last)final {
			auto it{ first };
			size_t cnt{ 0 };
			while(it != last && _wOffset != _end) {

				(*_wOffset) = (*it);

				++it;
				++_wOffset;
				++_offset;
				++cnt;
				}

			return cnt;
			}

		iterator_tag cathegory()const noexcept {
			return iterator_cathegory<decltype(current())>();
			}
	};
}
