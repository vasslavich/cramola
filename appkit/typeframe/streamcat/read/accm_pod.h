#pragma once


#include <array>
#include <atomic>
#include "../../base_typetrait_decls.h"
#include "../../base_typetrait_using.h"


namespace crm{
namespace srlz{
namespace detail{


/*! �������� ����������� ��������� ������������� �������� */
template<typename _Tobject, 
	const size_t NSerialized, 
	typename Options,
	/*typename TDeserializer,*/
	typename Tobject = std::decay_t<_Tobject>,
	typename std::enable_if_t<is_serializable_v<Tobject>, int> = 0>
struct _x_accumulate_t/* : public i_srlz_stream_accumulator_t*/ {
public:
	typedef typename Tobject object_t;
	//typedef typename std::decay<decltype((std::declval<TDeserializer>())(std::declval<i_rstream_t>()))>::type decoder_result_t;
	typedef typename _x_accumulate_t<_Tobject, NSerialized, Options/*, TDeserializer*/> self_t;

private:
	bool _readyf = false;
	std::array<uint8_t, NSerialized> _podData{0};
	/*typename TDeserializer _decoder;*/
	object_t _object;
	size_t _cnt{0};

public:
	typedef typename std::array<uint8_t, NSerialized>::const_iterator citerator_t;

	/*template<typename XDeserializer>*/
	_x_accumulate_t( /*typename XDeserializer && xdecoder*/ )noexcept
		/*: _decoder( std::forward<XDeserializer>( xdecoder ) )*/ {}

	_x_accumulate_t( self_t && x )noexcept
		: _readyf( x._readyf )
		, _podData( std::move( x._podData ) )
		/*, _decoder( std::move( x._decoder ) )*/
		, _object( std::move( x._object ) )
		, _cnt( x._cnt )
		, _isAssignedValue(x._isAssignedValue)
		{
		
		x.reset();
		}

	self_t& operator=(self_t && x)noexcept{
		if(this != std::addressof(x)) {
			_readyf = x._readyf;
			_podData = std::move(x._podData);
			/*_decoder = std::move(x._decoder);*/
			_object = std::move(x._object);
			_cnt = x._cnt;
			_isAssignedValue = x._isAssignedValue;

			x.reset();
			}

		return (*this);
		}

	/*_x_accumulate_t() {
		reset();
		}*/

	void reset() {
		_podData.fill(0);
		_cnt = 0;
		_readyf = false;
		_isAssignedValue = false;
		_object = object_t{};
		}

	template<typename TInIt>
	size_t tadd( typename TInIt first, typename TInIt last ) {
		if( !ready() ) {

			if( _cnt >= _podData.size() )
				SERIALIZE_THROW_EXC_FWD( L"internal error" );

			/* ������ ������� */
			const size_t tailSize = _podData.size() - _cnt;
			/* ������ ������ */
			const auto streamLength = std::distance( first, last );
			if( streamLength < 0 )
				SERIALIZE_THROW_EXC_FWD( L"bad destination range" );

			/* ����� ���������� ����� */
			SERIALIZE_VERIFY( streamLength > 0 );
			const size_t cpySize = (std::min)( tailSize, (size_t)streamLength );

			if( cpySize > 0 ) {
				std::copy( first, first + cpySize, _podData.begin() + _cnt );
				_cnt += cpySize;

				if( _cnt == _podData.size() ) {
					_readyf = true;
					}
				}

			return cpySize;
			}
		else {
			SERIALIZE_THROW_EXC_FWD(nullptr);
			}
		}

	void add( uint8_t byte ) {
		tadd( (&byte), (&byte) + 1 );
		}

	size_t add( const uint8_t* first, const uint8_t* last ) {
		return tadd( first, last );
		}

	size_t add( const std::vector<uint8_t> & line )  {
		return tadd( line.cbegin(), line.cend() );
		}

	size_t add( std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last ) {
		return tadd( first, last );
		}

private:
	mutable bool _isAssignedValue{ false };

	void check_initialize()const {
		if( ready() ) {

			if( !_isAssignedValue) {
				auto rstream = rstream_constructor_t::make( _podData.data(), _podData.data() + _podData.size() );
				deserialize(rstream, const_cast<object_t&>(_object));
				CBL_VERIFY(_podData.size() == rstream.offset());
				_isAssignedValue = true;
				}
			}
		else
			SERIALIZE_THROW_EXC_FWD(nullptr);
		}

	const object_t& getp()const&  {
		if( ready() ) {
			check_initialize();

			SERIALIZE_VERIFY(_isAssignedValue );
			return _object;
			}
		else
			SERIALIZE_THROW_EXC_FWD( nullptr );
		}

	object_t getp()&& {
		if( ready() ) {
			check_initialize();

			SERIALIZE_VERIFY(_isAssignedValue );
			return std::move(_object);
			}
		else
			SERIALIZE_THROW_EXC_FWD( nullptr );
		}

public:
	const object_t& get()const & noexcept {
		return getp();
		}

	object_t get()&& noexcept {
		return getp();
		}

	bool ready()const noexcept{
		return _readyf;
		}

	citerator_t cbegin()const {
		return _podData.cbegin();
		}

	citerator_t cend()const {
		return _podData.cend();
		}

	citerator_t clast()const {
		return _podData.cbegin() + _cnt;
		}

	size_t need_count()const {
		return std::distance( clast(), cend() );
		}
	};


template<typename Tobject, typename TserializeType, typename Options, typename = std::void_t<>>
struct _pod_accumulate_base_t {};

/*! �������� ����������� ��������� ������������� POD-����� */
template<typename Tobject, typename TserializeType, typename Options>
struct _pod_accumulate_base_t<Tobject, TserializeType, Options,
	typename std::enable_if_t<(is_serializable_v<Tobject> && is_fixed_size_serialized_v<TserializeType, Options>)>> /*: public i_srlz_stream_accumulator_t*/{
	static_assert(is_fixed_size_serialized_v<TserializeType, Options>, __FILE_LINE__);

	using object_t = typename Tobject;

private:
	/*using decoder_t = utility::invoke_functor<Tobject, ;*/
	typedef typename _x_accumulate_t<Tobject, write_size_v<TserializeType, Options>, Options/*, decoder_t*/> xaccumulator_t;
	typedef typename _pod_accumulate_base_t<Tobject, TserializeType, Options> self_t;

	xaccumulator_t _accm;

public:
	typedef typename xaccumulator_t::citerator_t citerator_t;

	/*template<typename Trs>*/
	_pod_accumulate_base_t() /*
		: _accm( []( Trs && rstream ) {

		object_t obj;
		deserialize( rstream, obj );
		return obj;
		}) */{}

	_pod_accumulate_base_t( self_t && x )noexcept
		: _accm( std::move( x._accm ) ) {
		x.reset();
		}

	self_t& operator=(self_t && x)noexcept{
		if(this != std::addressof(x)) {
			_accm = std::move(x._accm);
			x.reset();
			}

		return (*this);
		}

	void reset()noexcept {
		_accm.reset();
		}

	void add( uint8_t byte ) {
		_accm.add( byte );
		}

	size_t add( const uint8_t* first, const uint8_t* last ) {
		return _accm.add( first, last );
		}

	size_t add( const std::vector<uint8_t> & line )  {
		return _accm.add( line );
		}

	size_t add( std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last ) {
		return _accm.add( first, last );
		}

	const object_t& get()const & noexcept {
		return _accm.get();
		}

	object_t get()&& noexcept {
		return std::move(_accm).get();
		}

	bool ready()const {
		return _accm.ready();
		}

	citerator_t cbegin()const {
		return _accm.cbegin();
		}

	citerator_t clast()const {
		return _accm.clast();
		}
	};


template<typename T, typename Options, typename= std::void_t<>>
struct pod_accumulate_trait{};

template<typename T, typename Options>
struct pod_accumulate_trait<T, Options, std::enable_if_t<is_mono_serializable_v<T>>> {
	using type = typename _pod_accumulate_base_t<T, T, Options>;
	};

template<typename T, typename Options>
struct pod_accumulate_trait<T, Options, std::enable_if_t< is_serializable_properties_prefix_trait_v<T>>> {
	using type = typename _pod_accumulate_base_t<typename std::decay_t<T>::prefix_trait, typename  std::decay_t<T>::serialized_type, Options>;
	};

template<typename T, typename Options = none_options>
using pod_accumulate_trait_t = typename pod_accumulate_trait<T, Options>::type;
}
}
}

