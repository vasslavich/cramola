#pragma once


#include <type_traits>
#include <functional>
#include "../../type_trait_base.h"
#include "../comutil/urstreams.h"
#include "../interators/pod_iterator.h"
#include "../interators/binary_iterator_adpt.h"
#include "../read/rstream_container.h"
#include "../interators/rstream_iterator.h"
#include "../read/rstream_base_handler.h"


namespace crm::srlz {


class rstream_constructor_t {
public:
	template<typename TContainer,
		typename _TContainer = typename std::decay_t<TContainer>,
		typename std::enable_if_t<detail::is_container_of_bytes_v<TContainer>, int> = 0>
	static auto make(TContainer&& xs) {
		return detail::rstream_sequence_adpt_t<_TContainer, false>(std::forward<TContainer>(xs));
		}

	template<typename It,
		typename _TIterator = typename std::decay_t<It>,
		typename std::enable_if_t<(
			detail::is_iterator_byte_v<It> &&
			!std::is_pointer_v<std::decay_t<It>>
			), int> = 0>
	static auto make(It&& f, It&& s) {
		if constexpr(detail::is_contiguous_iterator_v<_TIterator>) {
			return detail::rstream_iterator_adaptor_contiguous_t<_TIterator, false>(
				std::forward<It>(f), std::forward<It>(s));
			}
		else {
			return detail::rstream_iterator_adaptor_t<_TIterator, false>(
				std::forward<It>(f), std::forward<It>(s));
			}
		}


	template<typename T>
	static auto make(const T *f, const T *s) {
		return typename detail::rstream_iterator_adaptor_contiguous_t<const T*, false>(f, s);
		}
	};
}

