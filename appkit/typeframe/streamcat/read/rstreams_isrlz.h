#pragma once


#include "../../options.h"
#include "../../isrlz.h"
#include "../comutil/urstreams.h"
#include "./rstream_base_handler.h"
#include "./accm_rstream.h"
#include "./xsyncseq.h"


namespace crm{
namespace srlz{
namespace detail{


/*! �������� �������������� �������� �������������� ��������� i_srlz_length_eof_t  */
template<typename Tobject, typename Options>
struct read_from_stream_t_<Tobject, Options,
	typename std::enable_if_t<is_i_srlzd_eof_v<Tobject>>> {
	static_assert(is_i_srlzd_eof_v<Tobject>, __FILE_LINE__);

public:
	template<typename Trs, typename Xo, 
		typename std::enable_if_t<std::is_same_v<std::decay_t<Tobject>, std::decay_t<Xo>>, int> =0 >
	void read( Trs && src, Xo && object ) {
		static_assert(is_i_srlzd_eof_v<Xo>, __FILE_LINE__);

		using value_type = typename std::decay_t<Xo>;
		using serialize_prefix_trait_t = typename serialize_prefix_trait_t<Xo, Options>;

		/* -----------------------------------------------------------
		�������
		--------------------------------------------------------------*/
#ifdef USE_DCF_CHECK
		size_t srcOff = src.offset();
#endif

		if constexpr(type_prefix_serialize_cond_v<Xo, Options>) {

			const auto prfix = serialize_prefix_trait_t::read_prefix(std::forward<Trs>(src), std::forward<Xo>(object));
			if(prfix.type != srlz_prefix_type_t::eof)
				SERIALIZE_THROW_EXC_FWD(L"bad prefix type");

#ifdef USE_DCF_CHECK
			if((src.offset() - srcOff) != serialize_prefix_trait_t::prefix_serialized_size())
				SERIALIZE_THROW_EXC_FWD(L"bad prefix length");
#endif
			}

		/* ---------------------------------------------------------------------
		����������
		-------------------------------------------------------------------------*/

		std::forward<Xo>(object).dsrlz(std::forward<Trs>(src));

		/* ----------------------------------------------------------
		������� �����
		----------------------------------------------------------------------------------------*/
		std::vector<uint8_t> endofMarker;
		deserialize(std::forward<Trs>(src), endofMarker, serialized_options_cond<decltype(endofMarker), Options>{});

		if( !std::forward<Xo>(object).eof_marker_validate( endofMarker ) )
			SERIALIZE_THROW_EXC_FWD(nullptr);
		}
	};


/*! �������� �������������� �������� �������������� ��������� i_srlz_stream_reader_prefixed_t */
template<typename Tobject, typename Options>
struct read_from_stream_t_<Tobject, Options,
	typename std::enable_if_t<is_i_srlzd_prefixed_v<Tobject>>> {
public:
	template<typename Trs, typename Xo,
		typename std::enable_if_t<std::is_same_v<std::decay_t<Tobject>, std::decay_t<Xo>>, int> = 0 >
	void read(Trs && src, Xo && object) {

		using value_type = std::decay_t<Xo>;
		using prefix_trait_type = serialize_prefix_trait_t<Xo, Options>;

		/* -----------------------------------------------------------
		�������
		--------------------------------------------------------------*/
		typename serialize_prefix_type<Xo, Options> prefix;
		/* compiler only */
		prefix;

#ifdef USE_DCF_CHECK
		size_t srcOff = src.offset();
#endif

		if constexpr(type_prefix_serialize_cond_v<Xo, Options>) {

			prefix = prefix_trait_type::read_prefix(std::forward<Trs>(src), std::forward<Xo>(object));
			if(prefix.type != srlz_prefix_type_t::prefixed)
				SERIALIZE_THROW_EXC_FWD(L"bad prefix type");

#ifdef USE_DCF_CHECK
			if((src.offset() - srcOff) != prefix_trait_type::prefix_serialized_size())
				SERIALIZE_THROW_EXC_FWD(L"bad prefix length");
#endif
			}

		/* ---------------------------------------------------------------------
		����������
		-------------------------------------------------------------------------*/

		std::forward<Xo>(object).dsrlz(std::forward<Trs>(src));

#ifdef USE_DCF_CHECK
		const auto disp = src.offset() - srcOff;
		if( disp != prefix_trait_type::get_serialized_size(std::forward<Xo>(object)) )
			SERIALIZE_THROW_EXC_FWD( L"bad format" );

		if constexpr (type_prefix_serialize_cond_v<Xo, Options>) {
			if (disp != prefix.length)
				SERIALIZE_THROW_EXC_FWD(L"bad format");
			}
#endif
		}
	};
}


/*! �������� ����������� �������� �������������� ��������� i_srlz_length_prefixed_t */
template<typename Tobject, typename Options>
struct read_from_stream_accumulate_t<Tobject, Options,
	std::void_t<typename std::enable_if<detail::is_i_srlzd_prefixed_v<Tobject> && detail::type_prefix_serialize_cond_v<Tobject, Options>>>> : std::true_type {
public:
	typedef std::vector<uint8_t> buffer_t;
	typedef typename Tobject object_t;
	typedef typename buffer_t::const_iterator buffer_const_iterator_t;
	typedef typename detail::srlz_trait_t<object_t, Options> type_trait_t;
	typedef typename read_from_stream_accumulate_t<object_t, Options> self_t;
	using prefix_trait = typename detail::serializable_prefix_type_trait_t<Tobject, Options>;

private:
	buffer_t _data;
	size_t _dataOffset = 0;
	detail::pod_accumulate_trait_t<prefix_trait> _prefixLine;
	mutable object_t _object;
	bool _hasPrefix = false;
	bool _readyf = false;

public:
	typedef typename buffer_const_iterator_t citerator_t;

	read_from_stream_accumulate_t() {}

	read_from_stream_accumulate_t( self_t && x )
		: _data( std::move(x._data) )
		, _dataOffset( std::move( x._dataOffset ) )
		, _prefixLine( std::move( x._prefixLine ) )
		, _object( std::move( x._object ) )
		, _hasPrefix( x._hasPrefix )
		, _readyf( x._readyf ) 
		, _isAssignedValue(x._isAssignedValue){
		
		x.reset();
		}

	self_t& operator=(self_t && x) {
		if(this != std::addressof(x)) {
			_data = std::move(x._data);
			_dataOffset = std::move(x._dataOffset);
			_prefixLine = std::move(x._prefixLine);
			_object = std::move(x._object);
			_hasPrefix = x._hasPrefix;
			_readyf = x._readyf;
			_isAssignedValue = x._isAssignedValue;

			x.reset();
			}

		return (*this);
		}

	void reset() {
		_data.clear();
		_dataOffset = 0;
		_prefixLine.reset();
		_hasPrefix = false;
		_readyf = false;
		_isAssignedValue = false;
		_object = object_t{};
		}

	template<typename TInIt,
		typename TInItValue = std::decay<decltype(*(std::declval<TInIt>()))>::type,
		typename std::enable_if<detail::is_byte<TInItValue>::value, int>::type = 0>
	size_t add_prefix( TInIt first, TInIt last ) {

		/* ����� �� �������� */
		if( !_prefixLine.ready() ) {

			const size_t readCount = _prefixLine.add( first, last );

			/* ����� �������� */
			if( _prefixLine.ready() ) {

				/* ����� ������ �������, ��� ����� �������� */
				const auto size = _prefixLine.get().length - type_trait_t::prefix_serialized_size();

				_data.resize( size );
				_dataOffset = 0;
				_hasPrefix = true;
				}

			return readCount;
			}
		else
			return 0;
		}

	template<typename TInIt,
		typename TInItValue = std::decay<decltype(*(std::declval<TInIt>()))>::type,
		typename std::enable_if<detail::is_byte<TInItValue>::value, int>::type = 0>
	typename size_t add_body( TInIt first, TInIt last ) {

		if( !ready() ) {

			if( !_hasPrefix )
				SERIALIZE_THROW_EXC_FWD( L"prefix not assigned" );
			if( _dataOffset >= _data.size() )
				SERIALIZE_THROW_EXC_FWD( L"bad format" );

			/* ������ ������� */
			const size_t tailSize = _data.size() - _dataOffset;
			/* ������ ������ */
			const auto streamLength = std::distance( first, last );
			if( streamLength < 0 )
				SERIALIZE_THROW_EXC_FWD( L"bad destination range" );

			/* ����� ���������� ����� */
			SERIALIZE_VERIFY( streamLength > 0 );
			const size_t cpySize = (std::min)( tailSize, (size_t)streamLength );
			if( cpySize > 0 ) {
				std::copy( first, first + cpySize, _data.begin() + _dataOffset );
				_dataOffset += cpySize;

				if( _dataOffset == _data.size() ) {
					_readyf = true;
					}
				}

			return cpySize;
			}
		else
			return 0;
		}

	template<typename TInIt, 
		typename TInItValue = std::decay_t<decltype(*(std::declval<TInIt>()))>,
		typename std::enable_if_t<detail::is_byte<TInItValue>::value, int> = 0>
	size_t tadd( TInIt first, TInIt last ) {

		if( !ready() ) {

			auto itBegin = first;
			while( first != last && !ready() ) {

				if( _hasPrefix ) {
					first += add_body( first, last );
					}
				else {
					first += add_prefix( first, last );
					}
				}

			auto rdist( std::distance( itBegin, first ) );
			SERIALIZE_VERIFY( rdist >= 0 );

			return (size_t)rdist;
			}
		else
			return 0;
		}

	mutable bool _isAssignedValue{ false };

	void check_ininitialize()const {

		if( ready() ) {
			if( !_isAssignedValue) {
				auto rstream( rstream_constructor_t::make( cbegin(), clast() ) );

				/* ������� ��� ������! */
				const_cast<object_t&>(_object).dsrlz( rstream );

				_isAssignedValue = true;
				}
			}
		else
			SERIALIZE_THROW_EXC_FWD(nullptr);
		}

	const object_t& getp() const & {
		if( ready() ) {
			check_ininitialize();

			SERIALIZE_VERIFY(_isAssignedValue );
			return _object;
			}
		else
			SERIALIZE_THROW_EXC_FWD( nullptr );
		}

	object_t getp()&& {
		if( ready() ) {
			check_ininitialize();

			SERIALIZE_VERIFY(_isAssignedValue);
			return std::move(_object);
			}
		else
			SERIALIZE_THROW_EXC_FWD( nullptr );
		}

public:
	void add( uint8_t byte ) {
		tadd( (&byte), (&byte) + 1 );
		}

	size_t add( const uint8_t* first, const uint8_t* last ) {
		return tadd( first, last );
		}

	size_t add( const std::vector<uint8_t> & line )  {
		return tadd( line.cbegin(), line.cend() );
		}

	size_t add( std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last ) {
		return tadd( first, last );
		}

	const object_t& get() const & {
		return getp();
		}

	object_t get()&& {
		return std::move(*this).getp();
		}

	bool ready()const noexcept{
		return _readyf;
		}

	citerator_t cbegin()const {
		return _data.cbegin();
		}

	citerator_t clast()const {
		return _data.cend();
		}
	};


/*! �������� ����������� �������� �������������� ��������� i_srlz_length_eof_t */
template<typename Tobject, typename Options>
struct read_from_stream_accumulate_t<Tobject, Options, 
	std::void_t<typename std::enable_if_t<detail::is_i_srlzd_eof_v<Tobject> && detail::type_prefix_serialize_cond_v<Tobject, Options>>>> : std::true_type{

public:
	using buffer_t = std::vector<uint8_t> ;
	using object_t = typename std::decay_t<Tobject>;
	using buffer_const_iterator_t = typename buffer_t::const_iterator;
	using type_trait_t = typename detail::srlz_trait_t<object_t, Options>;
	using self_t = typename read_from_stream_accumulate_t<object_t, Options>;
	using prefix_trait = typename detail::serializable_prefix_type_trait_t<Tobject, Options>;

private:
	typedef typename detail::marker_sequence_accumulator_t<std::uint64_t> eof_marker_line_t;

	buffer_t _blob;
	eof_marker_line_t _eofPattern;
	eof_marker_line_t _eofLine;
	detail::pod_accumulate_trait_t<prefix_trait> _prefixLine;
	mutable object_t _object;
	bool _hasPrefix = false;
	bool _readyf = false;

public:
	typedef typename buffer_const_iterator_t citerator_t;

	read_from_stream_accumulate_t() {}

	read_from_stream_accumulate_t( const buffer_t & eofMarker )
		: _eofPattern( eofMarker.cbegin(), eofMarker.cend() ) {}

	read_from_stream_accumulate_t( self_t && x )
		: _blob( std::move( x._blob ) )
		, _eofPattern( std::move( x._eofPattern ) )
		, _eofLine( std::move( x._eofLine ) )
		, _prefixLine( std::move( x._prefixLine ) )
		, _object( std::move( x._object ) )
		, _hasPrefix( x._hasPrefix )
		, _readyf( x._readyf ) 
		, _isAssignedValue(x._isAssignedValue)
		{

		x.reset();
		}

	self_t& operator=(self_t && x) {
		if(this != std::addressof(x)) {
			_blob = std::move(x._blob);
			_eofPattern = std::move(x._eofPattern);
			_eofLine = std::move(x._eofLine);
			_prefixLine = std::move(x._prefixLine);
			_object = std::move(x._object);
			_hasPrefix = x._hasPrefix;
			_readyf = x._readyf;
			_isAssignedValue = x._isAssignedValue;

			x.reset();
			}

		return (*this);
		}

	void reset() {
		_blob.clear();
		_prefixLine.reset();
		_eofLine.reset();
		_hasSize = false;
		_readyf = false;
		_isAssignedValue = false;
		_object = object_t{};
		}

	template<typename TInIt,
		typename TInItValue = std::decay_t<decltype(*(std::declval<TInIt>()))>,
		typename std::enable_if_t<detail::is_byte<TInItValue>::value, int> = 0>
	size_t add_prefix( TInIt first, TInIt last ) {

		/* ����� �� �������� */
		if( !_prefixLine.ready() ) {

			const size_t readCount = _prefixLine.add( first, last );

			/* ������� ������� */
			if( _prefixLine.ready() ) {
				_blob.clear();
				_eofLine.reset();
				_hasPrefix = true;
				}

			return readCount;
			}
		else
			return 0;
		}

	template<typename TInIt,
		typename TInItValue = std::decay_t<decltype(*(std::declval<TInIt>()))>,
		typename std::enable_if_t<detail::is_byte<TInItValue>::value, int> = 0>
	typename size_t add_body( TInIt first, TInIt last ) {

		if( !ready() ) {

			if( !_hasPrefix )
				SERIALIZE_THROW_EXC_FWD( L"prefix not assigned" );

			/* ������ ������ */
			const auto streamLength = std::distance( first, last );
			if( streamLength < 0 )
				SERIALIZE_THROW_EXC_FWD( L"bad destination range" );

			/* ��������������� ����� */
			_blob.reserve( streamLength );

			/* ��-������� � ������� ������� */
			auto itBegin = first;
			while( !ready() && first != last ) {

				auto byte = (*first);

				/* ���� ������ */
				_blob.push_back( byte );
				/* ������������� */
				_eofLine.shifft( byte );

				++first;
				}

			auto rdist( std::distance( itBegin, first ) );
			SERIALIZE_VERIFY( rdist >= 0 );

			return (size_t)rdist;
			}
		else
			return 0;
		}

	template<typename TInIt,
		typename TInItValue = std::decay_t<decltype(*(std::declval<TInIt>()))>,
		typename std::enable_if_t<detail::is_byte<TInItValue>::value, int> = 0>
	size_t tadd( TInIt first, TInIt last ) {

		if( !ready() ) {

			auto itBegin = first;
			while( first != last && !ready() ) {

				if( _hasPrefix ) {
					first += add_body( first, last );
					}
				else {
					first += add_prefix( first, last );
					}
				}

			auto rdist( std::distance( itBegin, first ) );
			SERIALIZE_VERIFY( rdist >= 0 );

			return (size_t)rdist;
			}
		else
			return 0;
		}

	mutable bool _isAssignedValue{ false };

	void check_initialize() const {
		if( ready() ) {
			if( !_isAssignedValue) {
				auto rstream( crm::srlz::rstream_constructor_t::make( cbegin(), clast() ) );

				/* ------------------------------------------
				������ ���� ������� -  ������� ��� ������!
				---------------------------------------------------------------------------------*/
				const_cast<object_t&>(_object).dsrlz( rstream );

				/* ----------------------------------------------------------
				������� �����
				----------------------------------------------------------------------------------------*/
				std::vector<uint8_t> endofMarker;
				sread_elseq_pod( rstream, endofMarker );

				if( !obj.eof_marker_validate( endofMarker ) )
					SERIALIZE_THROW_EXC_FWD(nullptr);

				_isAssignedValue = true;
				}
			}
		else
			SERIALIZE_THROW_EXC_FWD(nullptr);
		}

	const object_t& getp()const& {
		if( ready() ) {
			check_initialize();

			SERIALIZE_VERIFY(_isAssignedValue );
			return _object;
			}
		else
			SERIALIZE_THROW_EXC_FWD( nullptr );
		}

	object_t getp()&& {
		if( ready() ) {
			check_initialize();

			SERIALIZE_VERIFY(_isAssignedValue );
			return std::move(_object);
			}
		else
			SERIALIZE_THROW_EXC_FWD(nullptr);
		}

public:
	void add( uint8_t byte )final {
		tadd( (&byte), (&byte) + 1 );
		}

	size_t add( const uint8_t* first, const uint8_t* last, size_t * readCount )final {
		return tadd( first, last, readCount );
		}

	size_t add( const std::vector<uint8_t> & line, size_t * readCount ) final {
		return tadd( line.cbegin(), line.cend(), readCount );
		}

	size_t add( std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last )final {
		return tadd( first, last );
		}

	const object_t& get()const&{
		return getp();
		}

	object_t get()&& {
		return std::move(*this).getp();
		}

	bool ready()const {
		return _readyf;
		}

	citerator_t cbegin()const {
		return _data.cbegin();
		}

	citerator_t clast()const {
		return _data.cend();
		}
	};


template<typename T, typename Options = none_options>
using read_from_stream_accumulate_options_t = read_from_stream_accumulate_t<T, Options>;


template<typename Tssource, 
	typename Tisrlz,
	typename Options = none_options,
	typename std::enable_if_t<detail::is_i_srlzd_v<std::decay_t<Tisrlz>> && !detail::is_serialized_trait_r_v<std::decay_t<Tisrlz>>, int> = 0>
void deserialize(Tssource&& src, Tisrlz& isrlz, Options = {}) {
	static_assert(detail::is_i_srlzd_base_v<Tisrlz>, __FILE_LINE__);
	static_assert(detail::is_i_srlzd_v<Tisrlz>, __FILE_LINE__);
	static_assert(!detail::is_serialized_trait_r_v<Tisrlz>, __FILE_LINE__);

	detail::read_from_stream_t_<Tisrlz, Options> reader;
	return reader.read(std::forward<Tssource>(src), std::forward<Tisrlz>(isrlz));
	}


template<typename TStream, 
	typename TContainerValueISerialized,
	typename Options = none_options,
	std::enable_if_t<(
		detail::is_container_of_isrlzd_v<TContainerValueISerialized> &&
		!detail::is_fixed_size_array_v<TContainerValueISerialized>&&
		detail::is_i_srlzd_v<detail::contaiter_value_type_t<TContainerValueISerialized>>
		), int> = 0>
size_t deserialize( TStream & stream, TContainerValueISerialized & seq, Options = {}) {

	static_assert(!std::is_const_v<decltype(seq)>, __FILE__);
	using length_type = typename detail::option_trait_length_type<Options>;
	using range_elem_t = detail::contaiter_value_type_t<TContainerValueISerialized>;

	// �����
	length_type countof = 0;
	detail::sread(std::forward<TStream>(stream), countof, Options{});

	if constexpr(detail::is_resize_support_v<TContainerValueISerialized>) {
		if(countof) {
			seq.resize(countof);
			CBL_VERIFY(seq.size() >= countof);
			}
		}
	else {
		if(seq.size() < countof) {
			SERIALIZE_THROW_EXC_FWD(nullptr);
			}
		}

	// ��������
	auto itOut = seq.begin();
	for( size_t i = 0; i < countof; ++i, ++itOut ) {
		typename TContainerValueISerialized::value_type obj;

		deserialize(stream, obj, serialized_options_cond<decltype(obj), Options>{});
		(*itOut) = std::move(obj);
		}

	return countof;
	}


/* ���� ������ ������������ �������� ����� � �������� */
template<typename Tssource, 
	typename Tisrlz,
	typename Options = none_options,
	typename std::enable_if_t<detail::is_serialized_trait_r_v<Tisrlz> && std::is_base_of_v<detail::rstream_base_handler, std::decay_t<Tssource>>, int> = 0>
void deserialize(Tssource&& src, Tisrlz& isrlz, Options = {}) {
	return dynamic_cast<serialized_base_r&>(isrlz).deserialize(src);
	}


/* ���� ������ ������������ �������� ����� � �������� */
template<typename Tssource, 
	typename Tisrlz,
	typename Options = none_options,
	typename std::enable_if_t<detail::is_serialized_trait_r_v<Tisrlz> && std::is_base_of_v<detail::__rstream_base_interface, std::decay_t<Tssource>>, int> = 0>
	void deserialize(Tssource&& src, Tisrlz& isrlz, Options = {}) {

	detail::rstream_base_handler h(std::forward<Tssource>(src));
	return deserialize(h, isrlz, serialized_options_cond<Tisrlz, Options>{});
	}
}
}
