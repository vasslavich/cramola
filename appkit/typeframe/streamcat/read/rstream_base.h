#pragma once


#include <type_traits>
#include <functional>
#include "../comutil/urstreams.h"
#include "../comutil/uwstreams.h"
#include "../../../functorapp/functorapp.h"


namespace crm ::srlz::detail {


struct __rstream_base_interface{
	virtual ~__rstream_base_interface(){};

	virtual bool eof()const noexcept = 0;
	virtual size_t offset()const noexcept = 0;
	virtual size_t tail_size()const noexcept = 0;
	virtual bool pop_(std::uint8_t &) = 0;
	virtual size_t pop_(std::uint8_t * first, std::uint8_t *last) = 0;
	virtual rstream_base_type type()const noexcept = 0;
	};

struct __rstream_base_actor_xbase :
	virtual __rstream_base_interface,
	virtual stream_base_traits {

public:
	using item_value_t = std::uint8_t;

	__rstream_base_actor_xbase() noexcept {}

	void assign(__rstream_base_actor_xbase&&)noexcept {}

	template<typename XActor,
		typename _XActor = typename std::decay_t<XActor>,
		typename std::enable_if_t<is_invoke_with_byte_argument_v<_XActor>, int> = 0>
		explicit __rstream_base_actor_xbase(XActor&& af)noexcept {
		static_assert(false, __FILE__);
		}

	template<typename TValue,
		typename Pred,
		typename std::enable_if_t<is_byte_v<TValue>, int> = 0>
	bool read(TValue& value, Pred&& p) {
		static_assert(std::is_lvalue_reference_v<decltype(value)>, __FILE__);
		static_assert(!std::is_const_v<decltype(value)>, __FILE__);
		static_assert(sizeof(TValue) == 1, __FILE__);

		/*! complier only */
		(void)p;

		auto r = pop_(reinterpret_cast<uint8_t&>(value));
		if(r) {
			if constexpr(is_accumulate_invocable_v<Pred>) {
				p(value);
				}
			}

		return r;
		}

	template<typename It,
		typename Pred,
		typename std::enable_if_t<(
			is_iterator_byte_v<It>&&
			is_contiguous_iterator_v<It>
			), int> = 0>
	std::pair<It, size_t> pop2c(It first, It last, Pred&& p) {
		//static_cast(std::is_lvalue_reference_v<decltype(*first)>, __FILE_LINE__);

		/*! compiler only */
		(void)(p);

		const auto count_ = std::distance(first, last);
		if(count_ > 0) {
			const auto l_ = static_cast<size_t>(count_);
			if(l_ != pop_(reinterpret_cast<uint8_t*>(&(*(first))),
				reinterpret_cast<uint8_t*>(&(*(first))) + l_)) {
				SERIALIZE_THROW_EXC_FWD(nullptr);
				}

			if constexpr(is_accumulate_invocable_v<Pred>) {
				for(size_t i = 0; i < l_; ++i, ++first) {
					p(*first);
					}
				}

			return std::make_pair(last, l_);
			}
		else {
			return std::make_pair(first, 0);
			}
		}

	template<typename Out,
		typename Pred,
		typename std::enable_if_t<(
			is_iterator_byte_v<Out> &&
			!is_contiguous_iterator_v<Out>
			), int> = 0>
	std::pair<Out, size_t> pop2c(Out firstOut, size_t l_, Pred&& p) {
		uint8_t b;
		size_t ic{ 0 };
		while(ic < l_ && pop_(b)) {
			(*firstOut) = b;

			++ic;
			++firstOut;

			if constexpr(is_accumulate_invocable_v<Pred>) {
				p(b);
				}
			}

		return std::make_pair(firstOut, ic);
		}

	template<typename C,
		typename Pred,
		typename std::enable_if_t<is_container_contiguous_of_bytes_v<C>, int> = 0>
	size_t pop2c(C& c, size_t off_, size_t count_, Pred&& p) {
		static_assert(std::is_pointer_v<decltype(std::declval<C>().data())> &&
			is_byte_v<decltype(*std::declval<C>().data())>, __FILE__);

		auto first = std::begin(c);
		std::advance(first, off_);

		decltype(first) last;
		if(count_ < all_available) {
			last = std::begin(c);
			std::advance(last, off_ + count_);
			}
		else {
			last = std::end(c);
			}

		return pop2c(first, last, std::forward<Pred>(p)).second;
		}

	template<typename C,
		typename Pred>
		typename C ret2c_as_constructible_with_size(size_t l_, Pred&& p) {
		auto ts = tail_size();
		ts = (std::min)(ts, l_);

		C c(ts);
		pop2c(c, 0, ts, std::forward<Pred>(p));

		return c;
		}

	template<typename C,
		typename Pred>
		typename C ret2c_as_resizeble(size_t l_, Pred&& p) {
		const auto ts = tail_size();
		ts = (std::min)(ts, l_);

		C c;
		c.resize(ts);
		pop2c(c, 0, ts, std::forward<Pred>(p));

		return c;
		}

	template<typename C,
		typename Pred>
		typename C ret2c_as_reservible(size_t l_, Pred&& p) {
		const auto ts = tail_size();
		ts = (std::min)(ts, l_);

		C c;
		c.reserve(ts);
		pop2c(std::back_inserter(c), ts, std::forward<Pred>(pred));

		return c;
		}

	template<typename C,
		typename Pred,
		typename std::enable_if_t<is_fixed_size_array_v<C>, int> = 0>
		typename C ret2c_as_target_sized(Pred&& p) {
		const auto ts = tail_size();
		ts = (std::min)(ts, fixe_size_array_count_v<C>);

		C c;
		pop2c(std::back_inserter(c), ts, std::forward<Pred>(p));

		return c;
		}

	template<typename C,
		typename Pred>
		typename C ret2c(size_t l_, Pred&& p) {
		C c;
		pop2c(std::back_inserter(c), l_, std::forward<Pred>(p));

		return c;
		}

	template<typename C,
		typename Pred>
	std::enable_if_t<is_container_v<C>, C> read(Pred&& p, size_t l_ = all_available) {

		const auto streamType = iterator_cathegory<decltype(std::declval<C>().begin())>();
		if constexpr(streamType == iterator_tag::random_access_iterator_tag ||
			streamType == iterator_tag::contiguous_iterator_tag) {

			if constexpr(is_constructible_with_size_v<C>) {
				return ret2c_as_constructible_with_size<C>(l_, std::forward<Pred>(p));
				}
			else if constexpr(is_resize_support_v<C>) {
				return ret2c_as_resizeble<C>(l_, std::forward<Pred>(p));
				}
			else if constexpr(is_reserve_support_v<C>) {
				return ret2c_as_resizeble<C>(l_, std::forward<Pred>(p));
				}
			else if constexpr(is_fixed_size_array_v<C>) {
				return ret2c_as_target_sized<C>(std::forward<Pred>(p));
				}
			else {
				return ret2c<C>(l_, std::forward<Pred>(p));
				}
			}
		else {
			return ret2c<C>(l_, std::forward<Pred>(p));
			}
		}

	template<typename C,
		typename Pred>
	size_t pop2c_as_resizeble(C& c, size_t off_, size_t l_, Pred&& p) {
		auto ts = tail_size();
		ts = (std::min)((size_t)ts, l_);
		if(ts) {
			c.resize(off_ + ts);
			pop2c(c, off_, ts, std::forward<Pred>(p));
			}

		return ts;
		}

	template<typename C,
		typename Pred>
	size_t pop2c_as_reservible(C& c, size_t off_, size_t l_, Pred&& p) {
		auto ts = tail_size();
		ts = (std::min)((size_t)ts, l_);
		if(ts) {
			c.reserve(off_ + ts);
			pop2c(std::back_inserter(c), ts, std::forward<Pred>(p));
			}

		return ts;
		}

	template<typename C,
		typename Pred>
	size_t pop2c_as_fixed_sized(C& c, size_t off_, size_t l_, Pred&& p) {
		auto ts = tail_size();
		ts = (std::min)((size_t)ts, l_);
		if(ts) {
			if((off_ + ts) > c.size()) {
				SERIALIZE_THROW_EXC_FWD(nullptr);
				}

			pop2c(c, off_, ts, std::forward<Pred>(p));
			}

		return ts;
		}

	template<typename C, typename Pred,
		typename std::enable_if_t<is_container_v<C>, int> = 0>
	size_t read(C& dest, Pred&& p, size_t off_, size_t l_) {

		const auto streamType = iterator_cathegory<decltype(std::cbegin(std::declval<C>()))>();
		if(streamType == iterator_tag::random_access_iterator_tag ||
			streamType == iterator_tag::contiguous_iterator_tag) {
			if constexpr(is_resize_support_v<C>) {
				return pop2c_as_resizeble(dest, off_, l_, std::forward<Pred>(p));
				}
			else if constexpr(is_reserve_support_v<C>) {
				return pop2c_as_resizeble(dest, off_, l_, std::forward<Pred>(p));
				}
			else if constexpr(is_fixed_size_array_v<C>) {
				return pop2c_as_fixed_sized(dest, off_, l_, std::forward<Pred>(p));
				}
			else {
				return pop2c(dest, off_, l_, std::forward<Pred>(p));
				}
			}
		else {
			return pop2c(dest, off_, l_, std::forward<Pred>(p));
			}
		}

	template<typename TOutputIt,
		typename Pred,
		typename std::enable_if_t<(
			is_iterator_byte_v<TOutputIt>
			), int> = 0>
	std::pair<TOutputIt, size_t> read(TOutputIt first, TOutputIt last, Pred&& p) {
		return pop2c(first, last, std::forward<Pred>(p));
		}

	template<typename XActor,
		typename _XActor = typename std::decay_t<XActor>,
		typename std::enable_if_t<is_invoke_with_byte_argument_v<_XActor>, int> = 0>
	void assign(XActor&& actor) {
		_XActor tmp(std::forward<XActor>(actor));
		std::swap(_actor, tmp);
		}
	};


	struct __rstream_base_actor_0 : __rstream_base_actor_xbase {
	public:
		__rstream_base_actor_0()noexcept {}

		rstream_base_type type()const noexcept override {
			return rstream_base_type::actor_0;
			}

		void assign(__rstream_base_actor_0&&)noexcept {}

		template<typename TValue,
			typename std::enable_if_t<is_byte_v<TValue>, int> = 0>
			bool read(TValue& value) {
			return __rstream_base_actor_xbase::read(value, actor_null_dummy{});
			}


		template<typename C>
		std::enable_if_t<is_container_v<C>, std::decay_t<C>> read() {
			return __rstream_base_actor_xbase::read<std::decay_t<C>>(actor_null_dummy{}, stream_base_traits::all_available);
			}

		template<typename C>
		std::enable_if_t<is_container_v<C>, std::decay_t<C>> read(size_t l_) {
			return __rstream_base_actor_xbase::read<std::decay_t<C>>(actor_null_dummy{}, l_);
			}

		template<typename C,
			typename std::enable_if_t<is_container_v<C>, int> = 0>
		size_t read(C& dest) {
			return __rstream_base_actor_xbase::read(dest, actor_null_dummy{}, 0, stream_base_traits::all_available);
			}

		template<typename C,
			typename std::enable_if_t<is_container_v<C>, int> = 0>
		size_t read(C& dest, size_t off_, size_t l_) {
			return __rstream_base_actor_xbase::read(dest, actor_null_dummy{}, off_, l_);
			}

		template<typename TOutputIt,
			typename std::enable_if_t<is_iterator_byte_v<TOutputIt>, int> = 0>
		std::pair<typename TOutputIt, size_t> read(TOutputIt first, TOutputIt last) {
			return __rstream_base_actor_xbase::read(first, last, actor_null_dummy{});
			}
		};

struct __rstream_base_actor_1 : __rstream_base_actor_xbase {
private:
	utility::invoke_functor<void, std::uint8_t> _actor;

public:
	__rstream_base_actor_1()noexcept {}

	template<typename XActor,
		typename std::enable_if_t< is_invoke_with_byte_argument_v<XActor>,int> = 0>
	explicit __rstream_base_actor_1(XActor && af)noexcept
		: _actor(std::forward<XActor>(af)) {}

	rstream_base_type type()const noexcept override{
		return rstream_base_type::actor_1;
		}

	template<typename XActor,
		typename _XActor = typename std::decay_t<XActor>,
		typename std::enable_if_t<is_invoke_with_byte_argument_v<_XActor>, int> = 0>
	void assign(XActor && actor)noexcept {
		_XActor tmp(std::forward<XActor>(actor));
		std::swap(_actor, tmp);
		}

	void assign(__rstream_base_actor_1 && o)noexcept {
		_actor = std::move(o._actor);
		}

	auto actor_release()noexcept {
		return std::move(_actor);
		}

	template<typename TValue,
		typename std::enable_if_t<is_byte_v<TValue>, int> = 0>
	bool read(TValue & value) {
		return __rstream_base_actor_xbase::read(value, _actor);
		}

	template<typename C>
	std::enable_if_t<is_container_v<C>, std::decay_t<C>> read() {
		return __rstream_base_actor_xbase::read<std::decay_t<C>>(_actor, stream_base_traits::all_available);
		}

	template<typename C>
	std::enable_if_t<is_container_v<C>, std::decay_t<C>> read(size_t l_ ) {
		return __rstream_base_actor_xbase::read<std::decay_t<C>>(_actor, l_);
		}

	template<typename C,
		typename std::enable_if_t<is_container_v<C>, int> = 0>
	size_t read(C& dest) {
		return __rstream_base_actor_xbase::read(dest, _actor, 0, stream_base_traits::all_available);
		}

	template<typename C,
		typename std::enable_if_t<is_container_v<C>, int> = 0>
	size_t read(C & dest, size_t off_ , size_t l_ ) {
		return __rstream_base_actor_xbase::read(dest, _actor, off_, l_);
		}

	template<typename TOutputIt,
		typename std::enable_if_t<is_iterator_byte_v<TOutputIt>, int> = 0>
	std::pair<typename TOutputIt, size_t> read(TOutputIt first, TOutputIt last) {
		return __rstream_base_actor_xbase::read(first, last, _actor);
		}
	};
}