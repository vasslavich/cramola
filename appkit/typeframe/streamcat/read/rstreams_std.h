#pragma once


#include <chrono>
#include "../comutil/urstreams.h"
#include "../../utype_trait_std.h"


namespace crm{
namespace srlz{

template<typename Ts, typename Rep, typename Period>
void deserialize( Ts && src, std::chrono::duration<Rep, Period> & tp ) {

	std_types_trait_t::streamed_time_point_t stp;
	deserialize(std::forward<Ts>(src), stp );

	tp = std::chrono::duration<Rep, Period>(
		std::chrono::duration<Rep, Period>::duration( stp ) );
	}

template<typename Ts>
void deserialize( Ts && src, std::chrono::system_clock::time_point & tp ) {

	std_types_trait_t::streamed_time_point_t stp;
	deserialize(std::forward<Ts>(src), stp );

	tp = std::chrono::system_clock::time_point( 
		std::chrono::system_clock::duration( stp ));
	}

template<typename Ts, typename TFirst, typename TSecond>
void deserialize( Ts &&src, typename std::pair<TFirst, TSecond> & p) {

	deserialize(std::forward<Ts>(src), p.first);
	deserialize(std::forward<Ts>(src), p.second);
	}
}
}

