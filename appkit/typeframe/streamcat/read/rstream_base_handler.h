#pragma once


#include "./rstream_base.h"


namespace crm::srlz::detail{



struct rstream_base_handler{
private:
	__rstream_base_interface & _ri;

public:
	template<typename T,
		typename = std::enable_if_t<std::is_base_of_v<__rstream_base_interface, std::decay_t<T>>>>
	explicit rstream_base_handler(T & ri_)
		: _ri(ri_){}

	rstream_base_handler(const rstream_base_handler &) = delete;
	rstream_base_handler& operator=(const rstream_base_handler &) = delete;

	rstream_base_type type()const noexcept{
		return _ri.type();
		}

	bool eof()const noexcept {
		return _ri.eof();
		}

	size_t offset()const noexcept {
		return _ri.offset();
		}

	template<typename TValue,
		typename std::enable_if_t<is_byte_v<TValue>, int> = 0>
	bool read(TValue & value){
		static_assert(std::is_lvalue_reference_v<decltype(value)>, __FILE__);
		static_assert(!std::is_const_v<decltype(value)>, __FILE__);

		if (type() == rstream_base_type::actor_0){
			return dynamic_cast<__rstream_base_actor_0&>(_ri).read(value);
			}
		else{
			return dynamic_cast<__rstream_base_actor_1&>(_ri).read(value);
			}
		}

	template<typename C,
		typename std::enable_if_t<is_container_of_bytes_v<C>, int> = 0>
	C read(size_t l_){
		if (type() == rstream_base_type::actor_0){
			return dynamic_cast<__rstream_base_actor_0&>(_ri).read<C>(l_);
			}
		else{
			return dynamic_cast<__rstream_base_actor_1&>(_ri).read<C>(l_);
			}
		}

	template<typename C,
		typename std::enable_if_t<is_container_of_bytes_v<C>, int> = 0>
	C read() {
		if(type() == rstream_base_type::actor_0) {
			return dynamic_cast<__rstream_base_actor_0&>(_ri).read<C>();
			}
		else {
			return dynamic_cast<__rstream_base_actor_1&>(_ri).read<C>();
			}
		}

	template<typename C,
		typename std::enable_if_t<is_container_contiguous_of_bytes_v<C>, int> = 0>
	size_t read(C & dest, size_t off_, size_t l_){
		if (type() == rstream_base_type::actor_0){
			return dynamic_cast<__rstream_base_actor_0&>(_ri).read<C>(dest, off_, l_);
			}
		else{
			return dynamic_cast<__rstream_base_actor_1&>(_ri).read<C>(dest, off_, l_);
			}
		}

	template<typename C,
		typename std::enable_if_t<is_container_contiguous_of_bytes_v<C>, int> = 0>
	size_t read(C& dest) {
		if(type() == rstream_base_type::actor_0) {
			return dynamic_cast<__rstream_base_actor_0&>(_ri).read<C>(dest);
			}
		else {
			return dynamic_cast<__rstream_base_actor_1&>(_ri).read<C>(dest);
			}
		}

	template<typename TOutputIt,
		typename std::enable_if_t<is_iterator_byte_v<TOutputIt>, int> = 0>
	std::pair<TOutputIt, size_t> read(TOutputIt first, TOutputIt last){
		if (type() == rstream_base_type::actor_0){
			return dynamic_cast<__rstream_base_actor_0&>(_ri).read(first, last);
			}
		else{
			return dynamic_cast<__rstream_base_actor_1&>(_ri).read(first, last);
			}
		}
	};
}

