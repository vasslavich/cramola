#pragma once


#include "./accm_pod.h"


namespace crm{
namespace srlz{
namespace detail{

template<typename TSequence,
	typename TSize,
	typename = std::void_t<>>
struct sequence_accumulate_t {};


/*! �������� ����������� ��������� ������������� ������������ ����� */
template<typename TSequence, typename TSize >
struct sequence_accumulate_t<TSequence,
	TSize,
	typename std::enable_if_t<is_container_of_bytes_v<TSequence>>>{

	static_assert(sizeof( TSize ) >= sizeof( TSequence::size_type ), __FILE__);

public:
	typedef typename TSequence sequence_t;
	typedef typename sequence_t::value_type element_t;
	typedef typename TSize length_t;
	typedef typename sequence_accumulate_t<sequence_t, length_t> self_t;

private:
	enum state_t { 
		is_emtpy,
		get_size,
		get_elem,
		is_ready 
		};

	crm::srlz::detail::pod_accumulate_trait_t<length_t> _size;
	crm::srlz::detail::pod_accumulate_trait_t<uint8_t> _currElem;
	size_t _readySize;
	sequence_t _seq;
	state_t _state = state_t::is_emtpy;

public:
	sequence_accumulate_t() {
		reset();
		}

	sequence_accumulate_t( self_t && x )noexcept
		: _size( std::move( x._size ) )
		, _currElem( std::move( x._currElem ) )
		, _readySize( std::move( x._readySize ) )
		, _seq( std::move( x._seq ) )
		, _state( std::move( x._state ) ) {}

	self_t & operator=(self_t && x) noexcept{
		if(this != std::addressof(x)) {
			_size = std::move(x._size);
			_currElem = std::move(x._currElem);
			_readySize = std::move(x._readySize);
			_seq = std::move(x._seq);
			_state = std::move(x._state);

			x.reset();
			}

		return (*this);
		}

	void reset() {
		_size.reset();
		_currElem.reset();
		_readySize = 0;
		_seq.clear();
		_state = state_t::is_emtpy;
		}

	bool append( uint8_t byte ) {
		switch( _state ) {

			/* ��������� ������� ������� ���������� �� ���������� ����� ������������������ */
			case state_t::is_emtpy:

				/* c��������� ����� ������������������ */
			case state_t::get_size:

				/* ���� ����� ������������������ �� ���������� */
				if( !_size.ready() ) {
					_size.add( byte );
					_state = state_t::get_size;
					}

				/* ���� ����� ������������������ ���������� */
				if( _size.ready() ) {
					if( _size.get() == 0 ) {
						_state = state_t::is_ready;
						}
					else {
						/* ������� � ��-����������� ���������� ������������������ */
						_state = state_t::get_elem;
						_seq.reserve( _size.get() );
						}
					}

				break;

			case state_t::get_elem:
				_currElem.add( byte );

				/* ���� ����������� ������� ������������������ */
				if( _currElem.ready() ) {
					_seq.push_back( _currElem.get() );
					_currElem.reset();

					if( _seq.size() == _size.get() )
						_state = state_t::is_ready;
					}

				break;

			case state_t::is_ready:
				SERIALIZE_THROW_EXC_FWD(nullptr);

			default:
				SERIALIZE_THROW_EXC_FWD(nullptr);
			}

		return ready();
		}

	const sequence_t& get()const {
		return _seq;
		}

	bool ready()const {
		return _state == state_t::is_ready;
		}
	};
}
}
}

