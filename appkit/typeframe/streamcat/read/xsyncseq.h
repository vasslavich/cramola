#pragma once


#include <type_traits>


namespace crm{
namespace srlz{
namespace detail{


/*! ���������� ��������� ������ ���������������� ������������������ � �������� ������ */
template<typename _TxbaseType = uint64_t,
	typename TbaseType = std::decay_t<_TxbaseType>,
	typename std::enable_if_t<std::is_integral_v<TbaseType>, int> = 0>
struct marker_sequence_accumulator_t {
private:
	/*! ���� ���������������� ������������������ */
	typedef TbaseType sycn_line_t;

	/*! ������� ���������������� ������������������ */
	sycn_line_t _syncSequenceMask{0};

	static void reset( sycn_line_t *line ) {
		(*line) = 0;
		}

	static void shifft( sycn_line_t *line, uint8_t byte ) {
		(*line) = ((sycn_line_t)byte << (sizeof( sycn_line_t ) - 1) * 8) | ((*line) >> 8);
		}

	static uint8_t gget( const sycn_line_t *src, const size_t indx ) {
		return (uint8_t)((((sycn_line_t)0xFF << (indx * 8)) & (*src)) >> (indx * 8));
		}

	static void set( sycn_line_t *src, const size_t indx, const uint8_t byte ) {
		(*src) &= (~((sycn_line_t)0xFF)) << (indx * 8);
		(*src) |= ((sycn_line_t)byte << (indx * 8));
		}

public:
	static constexpr size_t size()noexcept {
		return sizeof(sycn_line_t);
		}

	typedef typename marker_sequence_accumulator_t<sycn_line_t> self_t;

	marker_sequence_accumulator_t() {}

	marker_sequence_accumulator_t( const self_t & x )
		: _syncSequenceMask( x._syncSequenceMask ) {}

	marker_sequence_accumulator_t( self_t && x )
		: _syncSequenceMask( std::move( x._syncSequenceMask ) ) {}

	self_t& operator=(self_t && x) {
		if(this != std::addressof(x)) {
			_syncSequenceMask = std::move(x.__syncSequenceMask);
			}

		return (*this);
		}

	self_t& operator=(const self_t & x) {
		if(this != std::addressof(x)) {
			_syncSequenceMask = x.__syncSequenceMask;
			}

		return (*this);
		}

	template<typename InIt>
	marker_sequence_accumulator_t( typename InIt first, typename InIt last ) {
		if( std::distance( first, last ) != size() )
			SERIALIZE_THROW_EXC_FWD(nullptr);

		for( auto it = first; it != last; ++it )
			shifft( &_syncSequenceMask, *it );

		}

	template<typename InIt>
	void assign( typename InIt first, typename InIt last ) {
		if( std::distance( first, last ) != size() )
			SERIALIZE_THROW_EXC_FWD(nullptr);

		_syncSequenceMask = 0;

		for( auto it = first; it != last; ++it )
			shifft( &_syncSequenceMask, *it );
		}

	/*! ������ ������� ���������������� ������������������ */
	const sycn_line_t& line()const {
		return _syncSequenceMask;
		}

	/*! ����� ���� ������������������ �� ���� ���� */
	void shifft( uint8_t byte ) {
		shifft( &_syncSequenceMask, byte );
		}
	};


template<typename Tbase>
bool operator==(const marker_sequence_accumulator_t<Tbase> & l, const marker_sequence_accumulator_t<Tbase> & r) {
	return l.line() == r.line();
	}

template<typename Tbase>
bool operator!=(const marker_sequence_accumulator_t<Tbase> & l, const marker_sequence_accumulator_t<Tbase> & r) {
	return !(l == r);
	}


/*! ���������� ��������� ������ ���������������� ������������������ � �������� ������ */
template<typename TbaseType = uint64_t>
struct _sync_sequence_t {
private:
	/*! ���� ���������������� ������������������ */
	typedef TbaseType sycn_line_t;
	typedef typename _sync_sequence_t<sycn_line_t> self_t;

	/*! ������� ���������������� ������������������ */
	sycn_line_t _syncSequenceMask{0};
	/*! ����������� ���������������� ������������������ � ������ */
	sycn_line_t _syncSequence{0};

	static void reset( sycn_line_t *line ) {
		(*line) = 0;
		}

	static void shifft( sycn_line_t *line, uint8_t byte ) {
		(*line) = ((sycn_line_t)byte << (sizeof( sycn_line_t ) - 1) * 8) | ((*line) >> 8);
		}

	static uint8_t gget( const sycn_line_t *src, const size_t indx ) {
		return (uint8_t)((((sycn_line_t)0xFF << (indx * 8)) & (*src)) >> (indx * 8));
		}

	static void set( sycn_line_t *src, const size_t indx, const uint8_t byte ) {
		(*src) &= (~((sycn_line_t)0xFF)) << (indx * 8);
		(*src) |= ((sycn_line_t)byte << (indx * 8));
		}

public:
	_sync_sequence_t() {}

	template<typename InIt>
	_sync_sequence_t( typename InIt first, typename InIt last ) {
		assign( first, last );
		}

	template<typename InIt>
	void assign( typename InIt first, typename InIt last ) {

		const auto diffit = std::distance( first, last );
		if( diffit >= 0 && (decltype(size()))diffit == size() ) {

			_syncSequenceMask = 0;
			_syncSequence = 0;

			for( auto it = first; it != last; ++it )
				shifft( &_syncSequenceMask, *it );
			}
		else{
			SERIALIZE_THROW_EXC_FWD(nullptr);
			}
		}

	/*! ������ ������� ���������������� ������������������ */
	sycn_line_t syncmask()const {
		return _syncSequenceMask;
		}

	/*! ����� �������� ���� ������������������ */
	void checkline_reset() {
		reset( &_syncSequence );
		}

	/*! ����� ���� ������������������ �� ���� ���� */
	void checkline_shifft( uint8_t byte ) {
		shifft( &_syncSequence, byte );
		}

	template<typename TInIt>
	size_t checkline_shifft_until( typename TInIt first, typename TInIt last,
		const std::function<bool( const self_t & current )> & cond ) {

		auto it = first;
		while( it != last && !cond( (*this) ) ) {
			checkline_shifft( (*it) );
			++it;
			}

		SERIALIZE_VERIFY( first <= it );
		return std::distance( first, it );
		}

	template<typename TInIt>
	size_t checkline_shifft_until_syncmask( typename TInIt first, typename TInIt last, 
		const std::function<bool( const self_t & current )> & validator ) {

		return checkline_shifft_until( first, last, [this, &validator]( const self_t & thisObject ) {
			if( !validator( (*this) ) )
				SERIALIZE_THROW_EXC_FWD(nullptr);

			return thisObject.checkline() == thisObject.syncmask();
			} );
		}

	/*! ������� ���� ������������������ */
	sycn_line_t checkline()const {
		return _syncSequence;
		}

	/*! ������ � ������, ���������������� ������������������ */
	size_t size()const {
		return sizeof( sycn_line_t );
		}

	/*! �������� � ������ */
	template<typename OutIt>
	static typename OutIt pack( sycn_line_t line,
		typename OutIt first, typename OutIt last ) {

		if( std::distance( first, last ) < size() )
			SERIALIZE_THROW_EXC_FWD(nullptr);

		OutIt it = first;
		for( size_t i = 0; i < size(); ++i, ++it )
			(*it) = get( &line, i );

		return it;
		}

	/*! ���������� */
	template<typename InIt>
	static typename InIt unpack( sycn_line_t *line,
		typename InIt first, typename InIt last ) {

		if( std::distance( first, last ) < size() )
			SERIALIZE_THROW_EXC_FWD(nullptr);

		InIt it = first;
		for( size_t i = 0; i < size(); ++i, ++it )
			set( line, i, (*it) );

		return it;
		}
	};
}
}
}

