#pragma once


#include <type_traits>
#include <functional>
#include "../../../notifications/internal_terms.h"
#include "../comutil/urstreams.h"
#include "../interators/pod_iterator.h"
#include "../../type_trait_base.h"
#include "./rstream_base.h"


namespace crm::srlz::detail{


template<typename Tsequence,
	const bool WithActor,
	typename TBaseType = std::conditional_t<WithActor, __rstream_base_actor_1, __rstream_base_actor_0>>
class rstream_sequence_adpt_t final :
	public std::conditional_t<WithActor, __rstream_base_actor_1, __rstream_base_actor_0>{

	public:
		typedef typename Tsequence sequence_t;
		typedef typename sequence_t::value_type sequence_value_t;
		typedef typename sequence_t::const_iterator iterator_t;
		using base_type = TBaseType;

	private:
		sequence_t _sequence;
		iterator_t _current;
		size_t _offset{0};

	public:
		/*std::unique_ptr<operator_read_t> reader()const final{
			FATAL_ERROR_FWD(nullptr);
			}*/

		rstream_sequence_adpt_t(){
			_current = _sequence.cbegin();
			}

		template<typename XContainer,
			typename std::enable_if_t<is_container_of_bytes_v<XContainer>, int> = 0>
			explicit rstream_sequence_adpt_t(XContainer && src, size_t offset = 0)
			: _sequence(std::forward<XContainer>(src)){

			_current = _sequence.cbegin();
			std::advance(_current, offset);

			_offset = offset;
			}

		template<typename XContainer,
			typename XActor,
			typename _XActor = typename std::decay_t<XActor>,
			typename std::enable_if_t<is_container_of_bytes_v<XContainer>, int> = 0,
			typename std::enable_if_t<is_invoke_with_byte_argument_v<_XActor>, int> = 0>
			rstream_sequence_adpt_t(XContainer && src, XActor && actor, size_t offset = 0)
			: base_type(std::forward<XActor>(actor))
			, _sequence(std::forward<XContainer>(src)){

			_current = _sequence.cbegin();
			std::advance(_current, offset);

			_offset = offset;
			}

		rstream_sequence_adpt_t(rstream_sequence_adpt_t  &&o){
			assign(std::forward<rstream_sequence_adpt_t >(o));
			}

		rstream_sequence_adpt_t& operator=(rstream_sequence_adpt_t &&o){
			if (this != std::addressof(o)){
				assign(std::forward<rstream_sequence_adpt_t >(o));
				}

			return (*this);
			}

		template<typename XContainer,
			typename std::enable_if_t<is_container_of_bytes_v<XContainer>, int> = 0>
			void assign(XContainer && src, size_t offset = 0){
			_current = _sequence.cbegin();
			std::advance(_current, offset);

			_offset = offset;
			}

		template<typename XContainer,
			typename XActor,
			typename _XActor = typename std::decay_t<XActor>,
			typename std::enable_if_t<is_container_of_bytes_v<XContainer>, int> = 0,
			typename std::enable_if_t<is_invoke_with_byte_argument_v<_XActor>, int> = 0>
			void assign(XContainer && src, XActor && actor, size_t offset = 0){

			_current = _sequence.cbegin();
			std::advance(_current, offset);

			_offset = offset;

			if constexpr (WithActor){
				base_type::assign(std::forward<XActor>(actor));
				}
			}

		void assign(rstream_sequence_adpt_t &&o){
			_current = o._current;
			_sequence = std::move(o._sequence);
			_offset = o._offset;

			if constexpr (WithActor){
				base_type::assign(o.actor_release());
				}
			}

		bool eof()const noexcept{
			return _current == _sequence.cend();
			}

	private:
		bool pop_(uint8_t &dest){
			if (!eof()){
				dest = (*_current);

				++_current;
				++_offset;

				return true;
				}
			else{
				return false;
				}
			}

	public:
		size_t tail_size()const noexcept {
			return _sequence.size() - _offset;
			}

		size_t offset()const noexcept {
			return _offset;
			}

	private:
		size_t pop_(std::uint8_t * first, std::uint8_t *last){
			if (first <= last){
				const auto targetL = std::distance(first, last);
				if (targetL < 0){
					SERIALIZE_THROW_EXC_FWD(nullptr);
					}

				const auto srcL = tail_size();

				if (srcL != serialized_base_definitions::not_available_value){
					const auto copyL = (size_t)targetL <= srcL ? (size_t)targetL : srcL;

					auto srcBegin = _current;

					auto srcEnd = srcBegin;
					std::advance(srcEnd, copyL);

					std::copy(srcBegin, srcEnd, first);

					_current = srcEnd;
					_offset += copyL;

					return copyL;
					}
				else{
					const auto off0 = _offset;

					while (first != last && !eof()){
						(*first) = (*_current);

						++first;
						++_current;
						++_offset;
						}

					return _offset - off0;
					}
				}
			else{
				SERIALIZE_THROW_EXC_FWD(nullptr);
				}
			}

	public:
		size_t length()const{
			return _sequence.size();
			}

		size_t/*������� ������� ����� ������*/ shifft(size_t count){
			size_t off = 0;

			if constexpr (is_random_access_iterator_v<decltype(_current)>){
				off = std::min<size_t>(std::distance(_current, _sequence.cend()), count);

				std::advance(_current, off);
				_offset += off;
				}
			else{
				while (off < count && !eof()){
					++off;
					++_current;
					++_offset;
					}
				}

			return off;
			}

		iterator_t current()const{
			return _current;
			}

		iterator_t begin()const{
			return _sequence.cbegin();
			}

		iterator_t end()const{
			return _sequence.cend();
			}
	};
}
