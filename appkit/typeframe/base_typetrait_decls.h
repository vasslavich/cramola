#pragma once


#include <type_traits>
#include <iterator>
#include "../internal_invariants.h"


namespace crm {

struct base_options {};
struct none_options : base_options {};
struct without_prefix : base_options {};
struct null_options {};


template<typename T>
struct fault_instantiaion_detect : std::false_type {};


template<typename TObject, typename TResult, typename = void>
struct has_avaitable_requirements : std::false_type{};



template<typename T, typename = void>
struct has_cancel_t : std::false_type {};

template<typename T, typename = std::void_t<>>
struct null_trait : std::false_type {};

template <typename T, typename = void>
struct is_has_null_static_value : std::false_type {};

template <typename T, typename = std::void_t<>>
struct is_has_null_property : std::false_type {};

template< class, class = std::void_t<> >
struct has_null_value_type_specialization : std::false_type {};

using std_hash_type = std::decay_t<decltype(std::declval<std::hash<std::string>>()(std::declval<std::string>()))>;

template<typename T>
struct null_value_t;

template<typename THashableSTD, typename = std::void_t<>>
struct has_std_hash_support : std::false_type {};

template<typename T, typename Enable = void>
struct is_invoke_with_async_operation_result_t : public std::false_type {};

template< class, typename = std::void_t<> >
struct is_bool_cast : std::false_type {};

template< class, typename = std::void_t<> >
struct is_functor : std::false_type {};

template< class, typename = std::void_t<> >
struct is_check_nullptr : std::false_type {};


namespace detail {


template<typename T, typename THash, typename = std::void_t<>>
struct has_hashable_support : std::false_type {};

template<typename T, typename = std::void_t<>>
struct is_functorable_t : std::false_type {};

template<typename T, typename = std::void_t<>>
struct is_smart_pointer_t : std::false_type {};

template<typename T, typename = std::void_t<>>
struct comparator_cast {};
}


namespace srlz {


struct srlz_prefix_t {
	using length_type = max_serialized_length_type;

	length_type length;
	srlz_prefix_type_t type;
	};

template<typename TObject,
	typename = std::void_t<>>
struct is_serialized_as_blob_t : std::false_type {};

template<typename TObject, typename = std::void_t<>>
struct has_static_typeid_trait_t : std::false_type{};



namespace detail {


template<typename TObject,
	typename Options,
	typename = std::void_t<>>
	struct sequence_serialized_size_t : std::false_type {};
}


template<typename TObject,
	typename = std::void_t<>>
struct xtype_properties_t;

struct format_traits {
	using length_type = std::uint64_t;
	using length_header = length_type;
	};

struct serializable_properties {
	size_t serialized_size{ 0 };
	serialize_properties_length size_as{ serialize_properties_length::as_maxsize };
	};


template<typename T, typename DefaultOptions, typename = std::void_t<>>
struct serialized_options_trait : std::false_type {
	using type = typename DefaultOptions;
	};

template<typename T, typename Options, typename = std::void_t<>>
struct serializable_properties_trait;


template<typename TValue,
	typename Options,
	typename = void >
	struct read_from_stream_accumulate_t : std::false_type {};


template<typename T, typename Options, class Enable = void>
struct write_size;

template<typename T, class Enable = void>
struct is_chrono_duration_t : std::false_type {};


namespace detail {

template<typename TArray, typename O, typename = void>
struct fixed_array_trait;

template<typename T, typename = void>
struct x_from_stream_base {
	static constexpr bool is_serialized_with_prefix()noexcept {
		return true;
		}
	};

template<typename TValue,
	typename Options,
	typename = std::void_t<> >
	struct srlz_trait_t;

template<typename T, typename = std::void_t<>>
struct has_flag_without_prefix : std::false_type {};

template<typename T, typename = std::void_t<>>
struct has_max_serialized_value : std::false_type {};

template<typename T, typename = std::void_t<>>
struct is_container_serialized : std::false_type {};

template<typename T, typename = void>
struct is_iterator_serialized_value : std::false_type {};

class type_properties_base_t {
protected:
	typedef decltype(std::declval<std::type_info>().hash_code()) type_hash_code_t;
	};

/*template<typename TObject, typename TDecoder, typename = std::void_t<>>
struct is_decoder_stream2object : std::false_type {};*/

template<typename T>
struct detect_fail_serializable_properties : std::false_type {};

template<typename T, typename = std::void_t<>>
struct is_aggregate_initializable_t : std::false_type {};

template<typename T, typename Options, typename = std::void_t<>>
struct serializable_prefix_type_trait;

template<typename T, typename = void>
struct fixed_size_array_value_type;

template<typename T, typename = void>
struct fixed_size_array_type;

template<typename TStrob, typename Options, typename = void>
struct strob_fields_serialized_size;

template<typename T, typename = void>
struct fixed_size_array_count;

template<typename T, typename Opt, typename = void>
struct is_fixed_size_serialized_array : std::false_type {};

template<typename T, typename O, typename = void>
struct get_type_serialized_options_as_{
	static constexpr serialize_properties_length value = serialize_properties_length::not_supported;
	};

template<typename _TxStrobValue, typename Opt, typename = std::void_t<>>
struct strob_fields_trait_as_fixed_serialized : std::false_type {
	static constexpr bool detect()noexcept {
		return false;
		}
	};


template<typename T, typename = std::void_t<>>
struct is_fixed_size_array : std::false_type {};

template<typename T, typename Opt, typename = std::void_t<>>
struct is_fixed_size_serialized : std::integral_constant<int, 0> {};

template<typename T, typename Enable = void>
struct is_i_srlzd_base : std::false_type {};

template<typename T, typename Enable = void>
struct is_i_srlzd_prefixed :  std::false_type {};

template<typename T, typename Enable = void>
struct is_i_srlzd_eof : public std::false_type {};

template<typename T, typename = std::void_t<>>
struct is_headed_serialized_trait : std::false_type {};

template<typename T, class Enable = void>
struct is_mono_serializable : std::false_type {};

template<typename T, class Enable = void>
struct is_strob_serializable : std::false_type {};

template<typename T, class Enable = void>
struct has_serializable_size : std::false_type {};

template<typename T, class Enable = void>
struct has_serializable_properties : std::false_type {};

template<typename T, typename = std::void_t<>>
struct is_serialized_trait_r : std::false_type {};

template<typename T, typename = std::void_t<>>
struct is_serialized_trait_w : std::false_type {};

template<typename T, typename = std::void_t<>>
struct is_serialized_trait : std::false_type {};

template<typename T, typename Opt, typename = void>
struct is_strob_fixed_serialized_size : std::false_type {};

template<typename T, typename Opt, typename = void>
struct fixed_serialized_size;

template<typename T, typename Opt, typename = void>
struct fixed_serialized_length_type;


template<typename T, typename Enable = void>
struct is_container_of_isrlzd : std::false_type {};

template<typename T, typename Enable = void>
struct is_container_of_isrlzd_eof : std::false_type {};

template<typename T, typename Enable = void>
struct is_container_of_isrlzd_prefixed : std::false_type {};

template<typename T, typename = std::void_t<>>
struct is_invoke_with_byte_argument : std::false_type {};

template<typename T, typename = std::void_t<>>
struct is_serialization_stream : std::false_type {};

template<typename T, typename = std::void_t<>>
struct is_serialization_wstream : std::false_type {};

template<typename T, typename = std::void_t<>>
struct is_serialization_rstream : std::false_type {};

template<typename T, class Enable = void>
struct is_strob_explicit_serialized :std::false_type {};

template<typename TIterator, typename = std::void_t<>>
struct is_iterator : std::false_type {};

template<typename T, class Enable = void>
struct is_chrono_time_point_t : std::false_type {};

template<typename T, class Enable = void>
struct iterator_value : std::false_type {};

template<typename TIterator, class Enable = void>
struct is_input_iterator : std::false_type {};

template<typename TIterator, typename = void>
struct is_contiguous_iterator_t : std::false_type{};

template<typename T, typename Enable = void>
struct is_container_t : std::false_type {};

template<typename T, typename Enable = void>
struct is_container_contiguous_cond_t : std::false_type {};

template<typename T, typename Enable = void>
struct is_container_of_mono : std::false_type {};

template<typename T, typename Enable = void>
struct is_container_of_containers_mono : std::false_type {};

template<typename T, typename Enable = void>
struct is_container_of_strob : std::false_type {};

template<typename T, typename Enable = void>
struct is_container_of_containers_strob : std::false_type {};

template<typename T, typename Enable = void>
struct is_dictionary_of_strob : std::false_type {};

template<typename T, typename Enable = void>
struct has_members_begin_end : std::false_type {};

template<typename T, typename Enable = void>
struct has_members_cbegin_cend : std::false_type {};

template<typename T, typename Enable = void>
struct is_range_t : std::false_type {};

template<typename TPair, typename = void>
struct is_pair_const_key_value : std::false_type {};

template<typename TDictionary, typename = void>
struct is_dictionary : std::false_type {};

template< class, class = std::void_t<> >
struct is_string_type : std::false_type {};

template<typename T, class Enable = void>
struct contaiter_value_type : std::false_type {};

template<typename T, class Enable = void>
struct range_value_type : std::false_type {};

template<typename T, class Enable = void>
struct dictionary_mapped_type : std::false_type {};

template<typename T, class Enable = void>
struct dictionary_key_type : std::false_type {};

template<typename T, typename Enable = void>
struct is_range_of_mono : std::false_type {};

template<typename TIterInt, typename = std::void_t<>>
struct is_iterator_of_integral : std::false_type {};

template<typename T, typename = void>
struct is_constructible_with_size : std::false_type{};

template<typename T, class Enable = void>
struct is_container_of_containers : std::false_type {};

template<typename T, typename Enable = void>
struct is_container_bytes_t : std::false_type {};

template<typename T, typename Enable = void>
struct is_container_integral_t : std::false_type {};

template<typename T, typename Enable = void>
struct is_range_bytes_t : std::false_type {};

template<typename T, typename Enable = void>
struct is_container_random_access_t : std::false_type {};

template<typename T, typename Enable = void>
struct is_iterator_mono : std::false_type {};

template<typename T, typename = void>
struct is_iterator_strob : std::false_type {};

template<typename T, typename = void>
struct is_iterator_fixed_sized : std::false_type {};

template<typename T, typename = void>
struct is_iterator_byte_t : std::false_type {};

template<typename T, typename = void>
struct is_resize_support_t : std::false_type {};

template<typename T, typename = void>
struct is_reserve_support_t : std::false_type {};

template<typename T, typename = void>
struct is_enum_serializabled_t : std::false_type {};

template<typename T, typename = std::void_t<>>
struct is_serializable_properties_prefix_triat : std::false_type {};

template<typename T, typename = std::void_t<>>
struct option_trait_length_type_ {
	using type = max_serialized_length_type;
	};

template<typename T, typename = std::void_t<>>
struct option_max_serialized_value : std::integral_constant<size_t,
	(std::numeric_limits<max_serialized_length_type>::max)()> {};

struct serialized_size_dispatch {};

template<typename TValue,
	typename Options,
	typename = void>
	struct read_from_stream_t_;

template<typename O, typename Options>
using serialize_prefix_trait_t = srlz_trait_t<std::decay_t<O>, Options>;

template<typename O, typename Options>
using serialize_prefix_type = typename serialize_prefix_trait_t<O, Options>::prefix_type;

template<typename T, typename Options, typename = std::void_t<>>
struct serialize_prefix;

template<typename TObject, typename TPrefix, typename Options, typename = std::void_t<>>
struct serialize_prefix_size_;

template<typename T, typename Options = none_options>
using serialize_prefix_t = serialize_prefix<T, Options>;
}


template<typename TValue, typename Options = none_options>
using srlz_object_trait_t = detail::srlz_trait_t<TValue, Options>;


class rstream_constructor_t;
class wstream_constructor_t;
}
}
