#pragma once


#include "./base_typetrait_decls.h"
#include "./base_typetrait_using.h"
#include "./type_trait_prefix.h"
#include "./options.h"


namespace crm::srlz{
namespace detail{


template<typename TElement, typename Options>
struct sequence_serialized_size_t<TElement, Options,
	std::enable_if_t<is_mono_serializable_v<TElement>>>
	: std::true_type {

	using value_type = typename std::decay_t<TElement>;
	static constexpr size_t value_serialized_size = sizeof(value_type);
	};

template<typename TElement, typename Options>
struct sequence_serialized_size_t<TElement, Options,
	std::enable_if_t<(
		is_fixed_size_serialized_v<TElement, Options> &&
		!is_mono_serializable_v<TElement>
		)>>
	: std::true_type {

	using value_type = typename std::decay_t<TElement>;
	static constexpr size_t value_serialized_size = write_size_v<value_type, Options>;
	};

template<typename T, typename O>
struct fixed_array_trait <T, O, std::void_t<
	std::enable_if_t<(
		is_fixed_size_array_v<T>
		&&
		is_fixed_size_serialized_array_v<T, O>
		)>
	>>{

	using value_type = typename fixed_size_array_value_type_t<T>;
	static_assert(is_fixed_size_serialized_v<value_type, O>, __FILE_LINE__);

	static constexpr size_t range_size = fixe_size_array_count_v<T>;
	using trait_type = typename sequence_serialized_size_t<value_type, O>;
	static constexpr size_t range_serialized_size = trait_type::value_serialized_size * range_size;

	/* if take a max length type */
	using length_type = typename max_serialized_size_to_arranged_length_type_t<(range_serialized_size + 8)>;
	static constexpr size_t serialized_size = sizeof(length_type) + range_serialized_size;
	};


template<typename T, typename O>
struct fixed_array_trait <T, O, std::void_t<
	std::enable_if_t<(
		is_fixed_size_array_v<T>
		&&
		!is_fixed_size_serialized_array_v<T, O>
		)>
	>>{

	using value_type = typename fixed_size_array_value_type_t<T>;
	using length_type = typename detail::option_trait_length_type<O>;

	static constexpr size_t range_size = fixe_size_array_count_v<T>;
	};

template<typename T, typename Opt>
struct fixed_serialized_length_type<T, Opt,
	std::enable_if_t<is_fixed_size_serialized_v<T, Opt>>> {

	using type = typename is_fixed_size_serialized_t<T, Opt>::length_type;
	};

template<typename T, typename Opt>
struct fixed_serialized_size<T, Opt,
	std::enable_if_t<is_fixed_size_serialized_v<T, Opt>>> :
	std::integral_constant<size_t, is_fixed_size_serialized_t<T, Opt>::size> {};
}


template<typename T, typename Options>
struct write_size<T, Options,
	typename std::enable_if_t<detail::is_fixed_size_serialized_v<T, Options>>> {

	using trait_type = typename detail::is_fixed_size_serialized_t<T, Options>;
	static constexpr size_t size = trait_type::size;
	using length_type = typename trait_type::length_type;
	};
}

