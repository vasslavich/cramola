#pragma once


#include <chrono>
#include "./isrlz.h"
#include "./type_trait_base.h"


namespace crm{
namespace srlz{


struct std_types_trait_t {
	using streamed_time_point_t = std::decay<decltype(std::declval<std::chrono::system_clock::time_point>().time_since_epoch().count())>::type;
	using streamed_time_duration_t = streamed_time_point_t;
	};


template < typename TClock, 
	typename Options = none_options,
	typename TDuration = typename TClock::duration >
constexpr size_t object_trait_serialized_size(const std::chrono::time_point<TClock, TDuration>&, Options = {})noexcept {
	return write_size_v<std_types_trait_t::streamed_time_point_t, Options>;
	}


template < typename TRep,
	typename TPeriod,
	typename Options = none_options>
constexpr size_t object_trait_serialized_size(const std::chrono::duration<TRep, TPeriod>&, Options = {})noexcept {
	return write_size_v<std_types_trait_t::streamed_time_duration_t, Options>;
	}


template<typename TFirst, typename TSecond, typename Options = none_options>
size_t object_trait_serialized_size(const std::pair<TFirst, TSecond>& v, Options = {}) {
	return object_trait_serialized_size(v.first, serialized_options_cond<decltype(v.first), Options>{})
		+ object_trait_serialized_size(v.second, serialized_options_cond<decltype(v.second), Options>{});
	}
}
}
