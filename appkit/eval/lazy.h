#pragma once


#include <functional>
#include <condition_variable>
#include <mutex>
#include <map>
#include <set>
#include <thread>
#include <queue>
#include <memory>
#include <list>
#include <future>
#include "../internal_invariants.h"
#include "../exceptions.h"
#include "./async_pool_tracer.h"
#include "../notifications/i_notification_services.h"
#include "../tables/mt_dynqueue.h"
#include "../functorapp/functorapp.h"
#include "./base_terms.h"
#include "./variadics.h"


namespace crm::detail {


/*! ���� ���������� �������� */
class lazy_action_stack_t :
	public i_notification_provider_t,
	public std::enable_shared_from_this<lazy_action_stack_t> {

public:
	typedef std::function<void(std::unique_ptr<crm::dcf_exception_t> obj)> notification_handler_t;
	static const std::chrono::milliseconds threadpool_queue_timeout;

private:
	/*! ��������� ��������� ��������� */
	enum class state_t : int {
		execution,
		canceled
		};

public:
	/*! ���������������� ������� �������� */
	typedef mt_dynamic_queue_t<
		std::mutex,
		std::unique_lock<std::mutex>,
		std::condition_variable,
		std::queue<std::unique_ptr<item_base_t>>> expressions_queue_t;

private:
	class work_item_t
		: public std::enable_shared_from_this<work_item_t> {

	private:
		notification_handler_t _ntfHndl;
		std::chrono::milliseconds _timeoutQexp;
		std::atomic<bool> _cancelf{ false };

#ifdef CBL_TRACE_LAZY_EXECUTION_STACK
		std::weak_ptr<activity_list_t> _activity;
#endif

		std::thread _th;
		std::thread::id _id;

		std::string _objectId;
		std::atomic<bool> _closedf{ false };

		work_item_t(
#ifdef CBL_TRACE_LAZY_EXECUTION_STACK
			std::weak_ptr<activity_list_t> activity,
#endif
			notification_handler_t ntfHndl,
			const std::chrono::milliseconds& checkTimeout );

		void ntf_hndl(std::unique_ptr<dcf_exception_t> && exc)noexcept;
		void ctr(std::shared_ptr<expressions_queue_t> exestack, const std::string & ownerName);

	public:
		~work_item_t();

		static std::shared_ptr<work_item_t> launch(
			std::shared_ptr<expressions_queue_t> items,
#ifdef CBL_TRACE_LAZY_EXECUTION_STACK
			std::weak_ptr<activity_list_t> activity,
#endif
			notification_handler_t ntfHndl,
			const std::chrono::milliseconds & checkTimeout,
			const std::string & ownerName);

		void cancel()noexcept;
		bool canceled()const noexcept;
		void close()noexcept;
		bool closed()const noexcept;
		const std::thread::id& id()const noexcept;
		};


	/*! ���� ������� ������� */
	std::vector<std::shared_ptr<work_item_t>> _threads;
	mutable std::mutex _thlstLck;
	void create_threads(const size_t count);

	/*! ���������� ����������� */
	notification_handler_t _ntfHndl;
	/*! ������������� ������� */
	std::string _objectId;
	/*! ������� �������� */
	std::shared_ptr<expressions_queue_t> _qexpr;
	/*! ������� ������ ������� �������� */
	std::chrono::milliseconds _timeoutQexp;
	/*! �������� */
	lazy_action_stack_properties_t _properties;

#ifdef CBL_TRACE_LAZY_EXECUTION_STACK
	std::shared_ptr<activity_list_t> _activity;
#endif

	std::atomic<bool> _closedf = false;
	std::atomic<bool> _cancelf = false;
	std::atomic<bool> _startf = false;
	std::atomic<size_t> _threadsCount = 0;

	/*! �������� ����������� � ���� */
	void ntf_hndl(std::unique_ptr<dcf_exception_t> && ntf)noexcept;
	/*! �������� ���������� � ���� */
	void ntf_hndl(dcf_exception_t && exc)noexcept;
	/*! �������� ���������� � ���� */
	void ntf_hndl(const dcf_exception_t & exc)noexcept;

	std::shared_ptr<expressions_queue_t> expqueue()const noexcept;

	struct emplace_item_ctr_t {
		std::weak_ptr<lazy_action_stack_t> baseStck;

#ifdef CBL_TRACE_LAZY_EXECUTION_STACK
		std::string_view name;
#endif

		emplace_item_ctr_t(std::weak_ptr<lazy_action_stack_t> baseStck_, 
#ifdef CBL_TRACE_LAZY_EXECUTION_STACK
			std::string_view  name_
#else
			std::string_view
#endif
		)noexcept
			: baseStck(baseStck_)

#ifdef CBL_TRACE_LAZY_EXECUTION_STACK
			, name(name_)
#endif
			{}

		template<typename TAction, typename ... TArgs>
		std::unique_ptr<item_base_t> operator()(
			typename TAction && action,
			typename TArgs && ... args) {

			return std::make_unique<vitem_t<TAction, std::decay_t<TArgs>...>>(
				std::move(baseStck),

#ifdef CBL_TRACE_LAZY_EXECUTION_STACK
				name,
#else
				"",
#endif

				std::forward<TAction>(action), std::forward<TArgs>(args)...);
			}
		};

	/*! ��������� �������� � ���� */
	template<typename Rep, typename Duration,
		typename _TAction,
		typename ... TArgs>
		bool push_to_queue_emplace(
			std::string_view  name,
			const std::chrono::duration<Rep, Duration> & /*timeout*/,
			typename _TAction && action,
			typename TArgs && ... args) {

		typedef emplace_item_ctr_t ctr_t;
		if(!canceled()) {

			if(auto qexp = expqueue()) {
				qexp->push_emplace_ctr( /*timeout,*/
					ctr_t(weak_from_this(), name),
					std::forward<_TAction>(action),
					std::forward<TArgs>(args)...);

				return true;
				}
			}

		return false;
		}

public:
	struct async_handler_t {
		bool ready = false;
		std::shared_future<std::unique_ptr<async_expression_result_t>> future;
		};

private:
	template<typename __XAction,
		typename ... __XArgs>
		std::unique_ptr<promise_wrapper_t<std::decay_t<__XAction>, std::decay_t<__XArgs>...>> create_promised_hndl(
			std::string_view name,
			typename __XAction && action,
			typename __XArgs && ... args
		) {

		std::weak_ptr<lazy_action_stack_t> wthis(shared_from_this());
		return std::make_unique<promise_wrapper_t<std::decay_t<__XAction>, std::decay_t<__XArgs>...>>(std::move(wthis),
			name,
			std::forward<__XAction>(action),
			std::forward<__XArgs>(args)...);
		}

	template<typename _YCallback, 
		typename __XAction,
		typename ... __XArgs,
		typename std::enable_if_t<std::is_invocable_v<_YCallback, async_expression_result_t &&>, int> = 0>
		std::unique_ptr<callback_wrapper_t<std::decay_t<__XAction>, std::decay_t<__XArgs>...>> create_callback_hndl(
			std::string_view name,
			_YCallback && clb,
			typename __XAction && action,
			typename __XArgs && ... args
		) {

		std::weak_ptr<lazy_action_stack_t> wthis(shared_from_this());
		return std::make_unique<callback_wrapper_t<std::decay_t<__XAction>, std::decay_t<__XArgs>...>>(
			std::forward<_YCallback>(clb),
			std::move(wthis),
			name,
			std::forward<__XAction>(action),
			std::forward<__XArgs>(args)...);
		}

	/*! ��������� �������� � ���� */
	template<typename Rep, typename Duration,
		typename _TAction,
		typename ... TArgs>
		async_handler_t push_to_queue_emplace_handler(
			std::string_view name,
			const std::chrono::duration<Rep, Duration> & /*timeout*/,
			typename _TAction && action,
			typename TArgs && ... args) {

		typedef emplace_item_ctr_t ctr_t;
		if(!canceled()) {

			auto qexp(expqueue());
			if(qexp) {
				async_handler_t result;

				std::weak_ptr<lazy_action_stack_t> wthis(shared_from_this());
				auto item(create_promised_hndl(
					name,
					std::forward<_TAction>(action),
					std::forward<TArgs>(args)...));

				result.future = item->get_future();
				result.ready = true;

				qexp->push(std::move(item));

				return result;
				}
			}

		return async_handler_t();
		}


	/*! ��������� �������� � ���� */
	template<typename Rep, typename Duration,
		typename _YCallback,
		typename _TAction,
		typename ... TArgs>
		bool push_to_queue_emplace_callback(
			std::string_view name,
			const std::chrono::duration<Rep, Duration> & /*timeout*/,
			_YCallback && clb,
			typename _TAction && action,
			typename TArgs && ... args) {

		typedef emplace_item_ctr_t ctr_t;
		if(!canceled()) {

			if(auto qexp = expqueue()) {
				auto item(create_callback_hndl(
					name,
					std::forward<_YCallback>(clb),
					std::forward<_TAction>(action),
					std::forward<TArgs>(args)...));

				qexp->push(std::move(item));
				return true;
				}
			}

		return false;
		}

	lazy_action_stack_t(const std::string & objectName,
		notification_handler_t ntfHndl,
		const std::chrono::milliseconds & timeoutQWait);

	void cancel_workitems()noexcept;
	void close_actions_stack(bool softly )noexcept;
	void close_workitems()noexcept;
	void close_thread(std::shared_ptr<work_item_t> &&)noexcept;

public:
	static std::shared_ptr<lazy_action_stack_t> make(const std::string & objectName,
		notification_handler_t ntfHndl,
		size_t poolSize,
		const std::chrono::milliseconds & timeoutQWait = threadpool_queue_timeout,
		bool startNow = true);

	~lazy_action_stack_t();

	void start(size_t poolSize);

	size_t conncurrency_size()const noexcept;
	size_t stacked_count()const noexcept;

#ifdef CBL_TRACE_LAZY_EXECUTION_STACK
	std::list<activity_item_t> get_activity(std::size_t)const;
	void set_activity(activity_item_t && itemName);
	void unset_activity(const activity_item_t & itemName)noexcept;
#endif

	/*! ��������� �������� � ���� */
	template<typename Rep, typename Period,
		typename _TAction,
		typename ... TArgs>
		bool tlaunch(
			std::string_view name,
			const std::chrono::duration<Rep, Period> & timeout,
			typename _TAction && action_,
			typename TArgs && ... args) {

		return !canceled()
			&& push_to_queue_emplace(
				name,
				timeout,
				std::forward<_TAction>(action_),
				std::forward<TArgs>(args)...);
		}

	void push_item(std::unique_ptr<item_base_t> && item) {
		if(!canceled()) {
			auto qexp(expqueue());
			if(qexp) {
				qexp->push(std::move(item));
				}
			}
		}

	/*! ��������� �������� � ���� */
	template<typename _TAction,
		typename ... TArgs>
		void launch(
			std::string_view name,
			typename _TAction && action_,
			typename TArgs && ... args) {

		while(!canceled() &&
			!tlaunch(name,
				_properties.timeout_push2stack(),
				std::forward<_TAction>(action_),
				std::forward<TArgs>(args)...))
			crm::file_logger_t::logging(L"async pool: wait operation", 0x000010000, __FILEW__, __LINE__);
		}

	/*! ��������� �������� � ���� */
	template<typename _TAction,
		typename ... TArgs>
		async_handler_t launch_hanlder(
			std::string_view name,
			typename _TAction && action_,
			typename TArgs && ... args) {

		async_handler_t result;
		while(!canceled() && !result.ready) {
			result = tlaunch_handler(name,
				_properties.timeout_push2stack(),
				std::forward<_TAction>(action_),
				std::forward<TArgs>(args)...);
			if(!result.ready) {
				crm::file_logger_t::logging(L"async pool: wait operation", 0x000010000, __FILEW__, __LINE__);
				}
			}

		return result;
		}

	/*! ��������� �������� � ���� */
	template<typename Rep, typename Period,
		typename _TAction,
		typename ... TArgs>
		async_handler_t tlaunch_handler(
			std::string_view name,
			const std::chrono::duration<Rep, Period> & timeout,
			typename _TAction && action_,
			typename TArgs && ... args) {

		if(!canceled()) {
			return push_to_queue_emplace_handler(
				name,
				timeout,
				std::forward<_TAction>(action_),
				std::forward<TArgs>(args)...);
			}
		else
			return async_handler_t();
		}

	/*! ��������� �������� � ���� */
	template<typename _YCallback,
		typename _TAction,
		typename ... TArgs,
		typename std::enable_if_t<std::is_invocable_v<_YCallback, async_expression_result_t &&>, int> = 0>
		void launch_callback(
			std::string_view name,
			_YCallback && clb,
			typename _TAction && action_,
			typename TArgs && ... args) {

		bool ready = false;
		while(!canceled() && !ready) {
			ready = tlaunch_callback(name,
				_properties.timeout_push2stack(),
				std::forward<_YCallback>(clb),
				std::forward<_TAction>(action_),
				std::forward<TArgs>(args)...);
			if(!ready) {
				crm::file_logger_t::logging(L"async pool: wait operation", 0x000010000, __FILEW__, __LINE__);
				}
			}
		}

	/*! ��������� �������� � ���� */
	template<typename Rep, typename Period,
		typename _YCallback,
		typename _TAction,
		typename ... TArgs,
		typename std::enable_if_t<std::is_invocable_v<_YCallback, async_expression_result_t &&>, int> = 0>
		bool tlaunch_callback(
			std::string_view name,
			const std::chrono::duration<Rep, Period> & timeout,
			_YCallback && clb,
			typename _TAction && action_,
			typename TArgs && ... args) {

		if(!canceled()) {
			return push_to_queue_emplace_callback(
				name,
				timeout,
				std::forward<_YCallback>(clb),
				std::forward<_TAction>(action_),
				std::forward<TArgs>(args)...);
			}
		else
			return false;
		}

	/*! ������� */
	void close(bool softly = false)noexcept;

	/*! ������ ������� ���������� */
	bool closed()const noexcept;

	/*! ������ ��������� */
	void cancel()noexcept;

	bool canceled()const noexcept;

	const std::string& id()const noexcept;
	};
}


