#pragma once


#include <memory>


namespace crm::detail {


struct i_async_expression_handler_t {
	virtual ~i_async_expression_handler_t() = 0;
	virtual void cancel()noexcept = 0;
	};

struct async_expression_handler_t : public i_async_expression_handler_t {
private:
	std::weak_ptr<i_async_expression_handler_t> _wthis;

public:
	async_expression_handler_t();
	async_expression_handler_t(std::weak_ptr<i_async_expression_handler_t> wthis_);

	void cancel()noexcept override;
	};


}
