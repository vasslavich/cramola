#include <chrono>
#include <sstream>
#include "../../internal.h"
#include "../lazy.h"
#include "../../logging/logger.h"
#include "../../utilities/utilities.h"


using namespace crm;
using namespace crm::detail;


void lazy_action_stack_t::work_item_t::ntf_hndl( std::unique_ptr<dcf_exception_t> && exc )noexcept {
	if( _ntfHndl )
		_ntfHndl( std::move( exc ) );
	}

void lazy_action_stack_t::work_item_t::ctr( std::shared_ptr<expressions_queue_t> exestck,
	const std::string & ownerName ) {

	_th = std::thread([this, exestck] {
#ifdef CBL_TRACE_THREAD_CREATE_POINT
		CHECK_NO_EXCEPTION(crm::file_logger_t::logging(get_this_thread_info(),
			CBL_TRACE_THREAD_CREATE_POINT_TAG, __CBL_FILEW__, __LINE__));
#endif

#ifdef CBL_TRACE_LAZY_EXECUTION_STACK
		std::string activityItem;
#endif

		typedef lazy_action_stack_t::expressions_queue_t::value_type action_value_t;

		while(!canceled()) {
			try {

				while(!canceled()) {

					/* ������� ������� �� ������� */
					action_value_t item;
					if(exestck->pop(item, _timeoutQexp)) {

#ifdef CBL_TRACE_LAZY_EXECUTION_STACK
						if(auto rt = _activity.lock()) {

							activity_item_t entry;
							entry.name = item->name() + ":" + sx_uuid_t::rand().to_string();
							entry.launch_timepoint = std::chrono::system_clock::now();

							post_scope_action_t delRec([entry, rt] {
								if(rt)
									rt->rem_activity(entry);
								});

							if(rt) {
								rt->set_activity(std::move(entry));
								}

							activityItem = item->name();
							}
#endif

						item->execute();
						}
					}
				}
			catch(const dcf_exception_t &exc) {
				_ntfHndl(exc.dcpy());
				}
			catch(std::exception & exc) {

#ifdef CBL_TRACE_LAZY_EXECUTION_STACK
				_ntfHndl(CREATE_PTR_EXC_FWD(activityItem + exc.what()));
#else
				_ntfHndl(CREATE_PTR_EXC_FWD(exc.what()));
#endif
				}
			catch(...) {

#ifdef CBL_TRACE_LAZY_EXECUTION_STACK
				_ntfHndl(CREATE_PTR_EXC_FWD(activityItem));
#else
				_ntfHndl(CREATE_PTR_EXC_FWD(nullptr));
#endif
				}
			}

#ifdef CBL_TRACE_LAZY_EXECUTION_STACK
		std::ostringstream slog;
		slog << "[lazy_action_stack_t::work_item_t]:" << std::this_thread::get_id() << ":exited";
		crm::file_logger_t::logging(slog.str(), 0, __CBL_FILEW__, __LINE__);
#endif
		});

	_id = _th.get_id();

	std::ostringstream sname;
	sname << ownerName << ":" << _id;

	_objectId = sname.str();
	}

lazy_action_stack_t::work_item_t::work_item_t( 
#ifdef CBL_TRACE_LAZY_EXECUTION_STACK
	std::weak_ptr<activity_list_t> activity,
#endif
	notification_handler_t ntfHndl,
	const std::chrono::milliseconds & checkTimeout )
	: _ntfHndl( ntfHndl )
	, _timeoutQexp( checkTimeout )
#ifdef CBL_TRACE_LAZY_EXECUTION_STACK
	, _activity( activity )
#endif
	{}

lazy_action_stack_t::work_item_t::~work_item_t() {
	close();
	}

std::shared_ptr<lazy_action_stack_t::work_item_t> lazy_action_stack_t::work_item_t::launch(
	std::shared_ptr<expressions_queue_t> items,
#ifdef CBL_TRACE_LAZY_EXECUTION_STACK
	std::weak_ptr<activity_list_t> activity,
#endif
	notification_handler_t ntfHndl,
	const std::chrono::milliseconds & checkTimeout,
	const std::string & ownerName ) {

	auto obj = std::shared_ptr<work_item_t>( new work_item_t( 
#ifdef CBL_TRACE_LAZY_EXECUTION_STACK
		activity,
#endif
		ntfHndl,
		checkTimeout ) );
	obj->ctr( items, ownerName );

	return std::move( obj );
	}

void lazy_action_stack_t::work_item_t::close() noexcept {
	_closedf.store(true, std::memory_order::memory_order_release);

	/* �������� ���������� ������ */
	if(_th.get_id() != std::this_thread::get_id()) {
		if(_th.joinable())
			_th.join();
		}
	}

void lazy_action_stack_t::work_item_t::cancel()noexcept {
	_cancelf.store(true, std::memory_order::memory_order_release);
	}

bool lazy_action_stack_t::work_item_t::canceled()const noexcept {
	return _cancelf.load(std::memory_order::memory_order_acquire);
	}

const std::thread::id& lazy_action_stack_t::work_item_t::id()const noexcept {
	return _id;
	}

bool lazy_action_stack_t::work_item_t::closed()const noexcept {
	return _closedf.load( std::memory_order::memory_order_acquire );
	}


