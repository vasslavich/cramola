#include "../../internal.h"
#include "../async_pool_tracer.h"
#include "../../exceptions.h"
#include "../lazy.h"
#include "../../logging/logger.h"


using namespace crm;
using namespace crm::detail;


#ifdef CBL_TRACE_LAZY_EXECUTION_STACK


size_t async_pool_tracer_t::set(std::weak_ptr<lazy_action_stack_t> && wptr) {
	std::lock_guard lck(_objectsLck);

	_objects.push_back(std::move(wptr));
	
	auto it = _objects.begin();
	while(it != _objects.end()) {
		if(it->expired()) {
			it = _objects.erase(it);
			}
		else {
			++it;
			}
		}

	return _objects.size();
	}

size_t async_pool_tracer_t::unset(const std::weak_ptr<lazy_action_stack_t> & wptr)noexcept {
	std::lock_guard lck(_objectsLck);

	const auto wkey = wptr.lock().get();

	auto it = _objects.begin();
	while( it != _objects.end() ) {
		if(auto p = it->lock()) {
			if(p.get() == wkey) {
				it = _objects.erase(it);
				}
			else {
				++it;
				}
			}
		else {
			it = _objects.erase(it);
			}
		}

	return _objects.size();
	}

bool async_pool_tracer_t::closed()const noexcept {
	return _closef.load(std::memory_order::memory_order_acquire);
	}

async_pool_tracer_t::async_pool_tracer_t()noexcept{}

async_pool_tracer_t:: ~async_pool_tracer_t() {
	CHECK_NO_EXCEPTION(close());
	}

std::list<std::shared_ptr<lazy_action_stack_t>> async_pool_tracer_t::get_pools()noexcept {
	std::lock_guard lck(_objectsLck);

	std::list<std::shared_ptr<lazy_action_stack_t>> rlst;

	auto it = _objects.begin();
	while(it != _objects.end()) {
		if(auto p = it->lock()) {
			rlst.push_back(std::move(p));
			++it;
			}
		else {
			it = _objects.erase(it);
			}
		}

	return rlst;
	}

void async_pool_tracer_t::open() {
	_th = std::thread([this] {

#ifdef CBL_TRACE_THREAD_CREATE_POINT
		CHECK_NO_EXCEPTION(crm::logger_t::logging(get_this_thread_info(),
			CBL_TRACE_THREAD_CREATE_POINT_TAG, __CBL_FILEW__, __LINE__));
#endif
		while(!closed()) {
			try {
				auto stpEnd = std::chrono::system_clock::now() + std::chrono::seconds(60);

				auto pl = get_pools();
				for(auto & ipool : pl) {
					if(!closed()) {

						size_t firstCount = 20;

						std::ostringstream os;

						auto stackSize = ipool->stacked_count();
						os << "async pool[" << ipool->id() << "]stacked items=" << stackSize << std::endl;

						auto alst = ipool->get_activity(firstCount);
						os << "async pool trace[" << alst.size() << "]:" << std::endl;

						size_t cln = 0;
						for(auto & ai : alst) {

							auto timeline = std::chrono::system_clock::now() - ai.launch_timepoint;
							auto dur = std::chrono::duration_cast<std::chrono::seconds>(timeline);
							if(dur.count() > 10) {
								os << "    name=" << ai.name << ":dur=" << dur.count() << ":TOO LONG OPERATION, s" << std::endl;

								if(std::chrono::duration_cast<std::chrono::hours>(dur).count() > 1) {
									FATAL_ERROR_FWD(nullptr);
									}

								++cln;
								}
							}

						if(cln || stackSize > 200) {
							crm::logger_t::logging(os.str(), NOTIFICATION_TRACE_ASYNC_POOL_TAG, __CBL_FILEW__, __LINE__);
							}
						}
					else {
						break;
						}
					}

				if (auto cl = crm::utility::__xcounters_logger_t::instance()) {
					cl->traceout();
					}

				while(!closed() && stpEnd > std::chrono::system_clock::now()) {
					std::this_thread::sleep_for(std::chrono::seconds(2));
					}
				}
			catch(const dcf_exception_t & exc0) {
				crm::logger_t::logging(exc0);
				}
			catch(const std::exception & exc1) {
				crm::logger_t::logging(exc1);
				}
			catch(...) {
				crm::logger_t::logging("undefined", NOTIFICATION_TRACE_ASYNC_POOL_TAG, __CBL_FILEW__, __LINE__);
				}
			}
		});
	}

void async_pool_tracer_t::close()noexcept {
	_closef.store(true, std::memory_order::memory_order_release);

	if(_th.joinable()) {
		_th.join();
		}
	}

std::unique_ptr< async_pool_tracer_t> _GlobalTracerAsyncPool;
std::mutex _GlobalTracerLock;

void crm::detail::bind_global_context(std::weak_ptr<lazy_action_stack_t> && wptr)noexcept {
	std::lock_guard lck(_GlobalTracerLock);

	if(!_GlobalTracerAsyncPool) {
		_GlobalTracerAsyncPool = std::make_unique<async_pool_tracer_t>();
		_GlobalTracerAsyncPool->open();
		}

	_GlobalTracerAsyncPool->set(std::move(wptr));
	}

void crm::detail::unbind_global_context(const std::weak_ptr<lazy_action_stack_t> & wptr)noexcept {
	std::lock_guard lck(_GlobalTracerLock);

	if(_GlobalTracerAsyncPool) {
		if(!_GlobalTracerAsyncPool->unset(wptr)) {
			_GlobalTracerAsyncPool->close();
			_GlobalTracerAsyncPool.reset();
			}
		}
	}

bool crm::detail::operator == (const activity_item_t & lval, const activity_item_t &rval)noexcept {
	return lval.name == rval.name;
	}

bool crm::detail::operator < (const activity_item_t & lval, const activity_item_t &rval)noexcept {
	return lval.name < rval.name;
	}

bool crm::detail::operator >= (const activity_item_t & lval, const activity_item_t &rval) noexcept {
	return lval.name >= rval.name;
	}


void activity_list_t::set_activity(activity_item_t && value) {

	std::lock_guard<std::mutex> lck(_activityLck);
	_activity.insert(std::move(value));
	}
void activity_list_t::rem_activity(const activity_item_t &value)noexcept {
	std::lock_guard<std::mutex> lck(_activityLck);
	auto it = _activity.find(value);
	if(it != _activity.end())
		_activity.erase(it);
	}
std::list<activity_item_t> activity_list_t::get_activity(size_t count_)const {
	std::lock_guard<std::mutex> lck(_activityLck);

	if(count_ < _activity.size()) {
		const auto count = (std::min)(_activity.size(), count_);
		std::list<activity_item_t> lst;

		auto it = _activity.cbegin();
		for(size_t i = 0; i < count; ++i)
			lst.push_back((*it));

		return lst;
		}
	else {
		return std::list<activity_item_t>(_activity.cbegin(), _activity.cend());
		}
	}
#endif

