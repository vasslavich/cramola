#include "../../internal.h"
#include "../../exceptions.h"
#include "../async_decls.h"


using namespace crm;
using namespace crm::detail;


i_async_expression_handler_t::~i_async_expression_handler_t() {}

async_expression_handler_t::async_expression_handler_t() {}

async_expression_handler_t::async_expression_handler_t( std::weak_ptr<i_async_expression_handler_t> wthis_ )
	: _wthis( std::move( wthis_ ) ) {}

void async_expression_handler_t::cancel()noexcept {
	if(auto sthis = _wthis.lock()) {
		CHECK_NO_EXCEPTION(sthis->cancel());
		}
	}



