#include "../../internal.h"
#include "../base_terms.h"


using namespace crm;
using namespace crm::detail;


const std::chrono::milliseconds& lazy_action_stack_properties_t::timeout_push2stack()const noexcept {
	return _timeoutPush2Stack; 
	}

async_expression_result_t::async_expression_result_t()noexcept {}

async_expression_result_t::async_expression_result_t(async_expression_result_t && o)noexcept
	: flag(o.flag)
	, exc(std::move(o.exc)) {}

async_expression_result_t& async_expression_result_t::operator=(async_expression_result_t && o) noexcept {
	if (this != std::addressof(o)) {
		flag = o.flag;
		exc = std::move(o.exc);
		}

	return (*this);
	}
