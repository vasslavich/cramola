#include <chrono>
#include <sstream>
#include "../../internal.h"
#include "../../logging/logger.h"
#include "../lazy.h"
#include "../../utilities/convert/cvt.h"


using namespace crm;
using namespace crm::detail;


#ifdef CBL_TRACE_LAZY_EXECUTION_STACK
const std::string& item_base_t::tag()const noexcept {
	return _tag;
	}

std::string item_base_t::name()const noexcept {
	auto t = std::chrono::system_clock::to_time_t( _timePoint );
	std::string sbuild;
	sbuild += _tag + "|" + std::ctime( &t );
	sbuild += ":" + crm::detail::cvt_to_str( std::chrono::duration_cast<std::chrono::nanoseconds>(_timePoint.time_since_epoch()).count() );
	return std::move( sbuild );
	}
#endif


item_base_t::~item_base_t() {
	cancel();
	}

void item_base_t::ntf_hndl(std::unique_ptr<dcf_exception_t> && ntf)noexcept {
	auto rt(_baseStack.lock());
	if(rt) {
		auto intf(std::dynamic_pointer_cast<i_notification_provider_t>(rt));
		intf->ntf_hndl(std::move(ntf));
		}
	}

void item_base_t::ntf_hndl(dcf_exception_t && exc) noexcept {
	auto rt(_baseStack.lock());
	if(rt) {
		auto intf(std::dynamic_pointer_cast<i_notification_provider_t>(rt));
		intf->ntf_hndl(std::move(exc));
		}
	}

void item_base_t::ntf_hndl(const dcf_exception_t & exc)noexcept {
	auto rt(_baseStack.lock());
	if(rt) {
		auto intf(std::dynamic_pointer_cast<i_notification_provider_t>(rt));
		intf->ntf_hndl( exc );
		}
	}

const std::chrono::system_clock::time_point& item_base_t::time_point()const noexcept {
	return _timePoint;
	}

const std::thread::id& item_base_t::thread_id()const noexcept {
	return _threadId;
	}

item_base_t::item_base_t(std::weak_ptr<lazy_action_stack_t> baseStck, 
#ifdef CBL_TRACE_LAZY_EXECUTION_STACK
	std::string_view tag_
#else
	std::string_view
#endif
)noexcept
	: _timePoint(std::chrono::system_clock::now())
	, _threadId(std::this_thread::get_id())
	, _baseStack(std::move(baseStck))
#ifdef CBL_TRACE_LAZY_EXECUTION_STACK
	, _tag(tag_)
	, _createTP(_timePoint)
#endif
	{}


#ifdef CBL_TRACE_LAZY_EXECUTION_STACK
const std::chrono::system_clock::time_point & item_base_t::create_timepoint()const noexcept {
	return _createTP;
	}

const std::chrono::system_clock::time_point & item_base_t::launch_timepoint()const noexcept {
	return _launchTP;
	}

void item_base_t::mark_launch_timepoint()noexcept {
	_launchTP = std::chrono::system_clock::now();
	}
#endif

 void item_base_t::cancel()noexcept {
	 _cancelf.store( true, std::memory_order::memory_order_release );
	 }

 bool item_base_t::canceled()const noexcept {
	 if( _cancelf.load( std::memory_order::memory_order_acquire ) )
		 return true;

	 auto bstk( _baseStack.lock() );
	 if( bstk )
		 return bstk->canceled();
	 else
		 return false;
	 }



/*##############################################################################################

lazy_action_stack_t

################################################################################################*/

const std::chrono::milliseconds lazy_action_stack_t::threadpool_queue_timeout = std::chrono::milliseconds(1000);

item_base_t::item_base_t() {}


/*=======================================================================
lazy_action_stack_t
=========================================================================*/


std::shared_ptr<lazy_action_stack_t::expressions_queue_t> lazy_action_stack_t::expqueue()const noexcept{
	return std::atomic_load_explicit( &_qexpr, std::memory_order::memory_order_acquire );
	}

std::shared_ptr<lazy_action_stack_t> lazy_action_stack_t::make( const std::string & objectName,
	notification_handler_t ntfHndl,
	size_t poolSize,
	const std::chrono::milliseconds & timeoutQWait,
	bool startNow ) {

	auto obj = std::shared_ptr<lazy_action_stack_t>( new lazy_action_stack_t(
		objectName, ntfHndl, timeoutQWait ) );

	if(startNow)
		obj->start(poolSize);

	return std::move( obj );
	}

void lazy_action_stack_t::create_threads( const size_t count ) {

	std::weak_ptr<lazy_action_stack_t> wthis( shared_from_this() );
	std::unique_lock<std::mutex> lck( _thlstLck );
	if( !canceled() ) {

		CBL_VERIFY( _threads.empty() );
		_threads.resize( count );

		for( size_t i = 0; i < count; ++i ) {

			auto ntfHndl = _ntfHndl;

			auto obj = work_item_t::launch( expqueue(),
#ifdef CBL_TRACE_LAZY_EXECUTION_STACK
				_activity,
#endif
				_ntfHndl,
				_timeoutQexp,
				_objectId );

			_threads[i] = std::move( obj );
			}

		_threadsCount.store( count, std::memory_order::memory_order_release );
		}
	}

lazy_action_stack_t::lazy_action_stack_t( const std::string & objectName, 
	notification_handler_t ntfHndl,
	const std::chrono::milliseconds & timeoutQWait )
	: _ntfHndl( std::move( ntfHndl ) )
	, _objectId(objectName)
	, _qexpr( std::make_shared<expressions_queue_t>( objectName ) ) 
	, _timeoutQexp( timeoutQWait ) 
#ifdef CBL_TRACE_LAZY_EXECUTION_STACK
	, _activity( std::make_shared<activity_list_t>() )
#endif
	{}

void lazy_action_stack_t::start(size_t poolSize) {
	bool stf = false;
	if(_startf.compare_exchange_strong(stf, true)) {

#ifdef CBL_TRACE_LAZY_EXECUTION_STACK
		bind_global_context(weak_from_this());
#endif

		create_threads(poolSize);
		}
	else {
		THROW_EXC_FWD(nullptr);
		}
	}

lazy_action_stack_t::~lazy_action_stack_t() {
	close();
	CBL_VERIFY( conncurrency_size() == 0 );
	}

size_t lazy_action_stack_t::conncurrency_size()const noexcept {
	return _threadsCount.load( std::memory_order::memory_order_acquire );
	}

size_t lazy_action_stack_t::stacked_count()const noexcept {
	if(auto qexp = expqueue())
		return qexp->stored_count();
	else
		return 0;
	}

void lazy_action_stack_t::close_actions_stack(bool softly )noexcept {

	/* ��������� ������� ���������� */
	auto qexpr( std::atomic_exchange( &_qexpr, std::shared_ptr<expressions_queue_t>() ) );
	if( qexpr ) {
		if(softly) {
			auto remlst(qexpr->pop(std::chrono::milliseconds(0)));
			if(!remlst.empty()) {

				auto it = remlst.begin();
				for(; it != remlst.end(); ++it) {
					try {
						auto tmp(std::move((*it)));
						if(tmp)
							tmp->cancel();
						}
					catch(const dcf_exception_t & exc) {
						ntf_hndl(exc);
						}
					catch(...) {
						ntf_hndl(CREATE_PTR_EXC_FWD(nullptr));
						}
					}
				}
			}

		CHECK_NO_EXCEPTION( qexpr->clear() );
		qexpr.reset();
		}
	}

void lazy_action_stack_t::close_thread( std::shared_ptr<work_item_t> && thObj ) noexcept {
	if( thObj ) {
		thObj->close();
		thObj.reset();
		}
	}

void lazy_action_stack_t::cancel_workitems()noexcept {
	std::unique_lock<std::mutex> lck(_thlstLck);
	auto itTh = _threads.begin();
	for(; itTh != _threads.end(); ++itTh) {
		(*itTh)->cancel();
		}
	}

void lazy_action_stack_t::close_workitems()noexcept {

	std::unique_lock<std::mutex> lck( _thlstLck );
	auto itTh = _threads.begin();
	for( ; itTh != _threads.end(); ++itTh ) {
		close_thread( std::move( (*itTh) ) );
		}

	_threads.clear();
	_threadsCount.store( 0, std::memory_order::memory_order_release );
	}

void lazy_action_stack_t::close(bool softly )noexcept {
	/* ������ ��������� */
	cancel();

	bool closedf = false;
	if( _closedf.compare_exchange_strong( closedf, true ) ) {

		cancel_workitems();

#ifdef CBL_TRACE_LAZY_EXECUTION_STACK
		unbind_global_context(weak_from_this());
#endif

		/* ��������� ������� ���������� */
		close_actions_stack(softly);

		/* �������� ������� */
		close_workitems();
		}
	}

bool lazy_action_stack_t::closed()const noexcept {
	return _closedf.load( std::memory_order::memory_order_acquire );
	}

void lazy_action_stack_t::ntf_hndl( std::unique_ptr<dcf_exception_t> && ntf ) noexcept {
	try {
		if( _ntfHndl )
			_ntfHndl( std::move( ntf ) );
		}
	catch( const crm::dcf_exception_t &exc ) {
		logger_t::logging( exc.msg(), 0, __CBL_FILEW__, __LINE__ );
		}
	catch( ... ) {
		logger_t::logging( "undefined exception", 0, __CBL_FILEW__, __LINE__ );
		}
	}

void lazy_action_stack_t::ntf_hndl( dcf_exception_t && exc )noexcept {
	ntf_hndl(exc.dcpy());
	}

void lazy_action_stack_t::ntf_hndl(const dcf_exception_t & exc)noexcept {
	ntf_hndl(exc.dcpy());
	}


#ifdef CBL_TRACE_LAZY_EXECUTION_STACK
std::list<activity_item_t> lazy_action_stack_t::get_activity(size_t count)const {
	return _activity->get_activity(count);
	}

void lazy_action_stack_t::set_activity( activity_item_t && item ) {
	return _activity->set_activity(std::move(item));
	}

void lazy_action_stack_t::unset_activity( const activity_item_t &  itemName )noexcept {
	_activity->rem_activity(itemName);
	}
#endif

const std::string& lazy_action_stack_t::id()const noexcept {
	return _objectId;
	}

void lazy_action_stack_t::cancel() noexcept {
	_cancelf.store( true, std::memory_order::memory_order_release );
	}

bool lazy_action_stack_t::canceled()const noexcept {
	return _cancelf.load(std::memory_order::memory_order_acquire);
	}


