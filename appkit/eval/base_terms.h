#pragma once


#include <functional>
#include <thread>
#include <memory>
#include <future>
#include <chrono>
#include "../internal_invariants.h"
#include "../exceptions.h"
#include "../notifications/i_notification_services.h"
#include "../functorapp/functorapp.h"


namespace crm::detail {


class lazy_action_stack_t;


struct lazy_action_stack_properties_t {
private:
	std::chrono::milliseconds _timeoutPush2Stack = std::chrono::milliseconds(1000);

public:
	const std::chrono::milliseconds& timeout_push2stack()const noexcept;
	};


class async_expression_result_t {
public:
	async_operation_result_t flag;
	std::unique_ptr<dcf_exception_t> exc;

	async_expression_result_t()noexcept;
	async_expression_result_t(async_expression_result_t && o)noexcept;
	async_expression_result_t& operator=(async_expression_result_t && o)noexcept;

	async_expression_result_t(const async_expression_result_t&) = delete;
	async_expression_result_t& operator=(const async_expression_result_t &) = delete;
	};


struct item_base_t {
	item_base_t();
	virtual ~item_base_t();

private:
	std::chrono::system_clock::time_point _timePoint;
	std::thread::id _threadId;
	std::weak_ptr<lazy_action_stack_t> _baseStack;
	std::atomic<bool> _cancelf = false;

#ifdef CBL_TRACE_LAZY_EXECUTION_STACK
	std::string _tag;
	std::chrono::system_clock::time_point _createTP;
	std::chrono::system_clock::time_point _launchTP;
#endif

public:
#ifdef CBL_TRACE_LAZY_EXECUTION_STACK
	const std::string& tag()const noexcept;
	std::string name()const noexcept;

	const std::chrono::system_clock::time_point &create_timepoint()const noexcept;
	const std::chrono::system_clock::time_point &launch_timepoint()const noexcept;

	void mark_launch_timepoint()noexcept;
#endif

	const std::chrono::system_clock::time_point& time_point()const noexcept;
	const std::thread::id& thread_id()const noexcept;

	item_base_t(std::weak_ptr<lazy_action_stack_t> baseStck, std::string_view tag_)noexcept;

	/*! ������ ��� ������� ���������, ������� ����� ��������� ��������� */
	virtual void execute() = 0;
	/*! ������ �������� */
	virtual void cancel()noexcept;
	/*! ������� ������ �������� */
	virtual bool canceled()const noexcept;

	/*! �������� ����������� � ���� */
	void ntf_hndl(std::unique_ptr<dcf_exception_t> && ntf)noexcept;
	/*! �������� ���������� � ���� */
	void ntf_hndl(dcf_exception_t && exc)noexcept;
	/*! �������� ���������� � ���� */
	void ntf_hndl(const dcf_exception_t & exc)noexcept;

	virtual operator bool()const noexcept = 0;

	item_base_t(const item_base_t&) = delete;
	item_base_t& operator=(const item_base_t&) = delete;
	};


typedef item_base_t callable_functor_t;

using i_callable_functor_parpack_t = item_base_t;

}

