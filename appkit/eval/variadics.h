#pragma once


#include "./base_terms.h"
#include "../exceptions.h"


namespace crm::detail {



template<int...>
struct index_tuple {};

template<int I, typename IndexTuple, typename... Types>
struct make_indexes_impl;

template<int I, int... Indexes, typename T, typename ... Types>
struct make_indexes_impl<I, index_tuple<Indexes...>, T, Types...> {
	typedef typename make_indexes_impl<I + 1, index_tuple<Indexes..., I>, Types...>::type type;
	};

template<int I, int... Indexes>
struct make_indexes_impl<I, index_tuple<Indexes...> > {
	typedef index_tuple<Indexes...> type;
	};

template<typename ... Types>
struct make_indexes : make_indexes_impl<0, index_tuple<>, std::decay_t<Types>...> {};



/*! ����� ��� �������� � ������� ���������� */
template<typename _TAction, typename ... TArgs>
struct vitem_t : public item_base_t {
private:
	typedef item_base_t base_t;
	using action_t = typename std::decay_t<_TAction>;

public:
	using tuple_args_t = typename std::tuple<std::decay_t<TArgs>...>;

private:
	action_t action;
	tuple_args_t args;

	template<int... Indexes >
	void apply_helper(index_tuple< Indexes... >) {
		action(std::forward<TArgs>(std::get<Indexes>(args))...);
		}

	void apply() {
		apply_helper(typename make_indexes<std::decay_t<TArgs>...>::type());
		}

protected:
	void execute() {
		if(!canceled())
			apply();
		}

public:
	template<typename _XAction,
		typename ... XArgs,
		typename std::enable_if_t<std::is_invocable_v<std::decay_t<_XAction>, std::decay_t<XArgs>...>, int> = 0>
	vitem_t(std::weak_ptr<lazy_action_stack_t> && baseStck,
			std::string_view name,
			typename _XAction && action_,
			typename XArgs && ... args_)
		: base_t(std::move(baseStck), name)

		, action(std::forward<_XAction>(action_))
		, args(std::make_tuple(std::forward<XArgs>(args_)...)) {

		CBL_VERIFY(is_functional_empty(action));
		}

	template<typename _XAction, typename ... XArgs>
	vitem_t(const vitem_t<std::decay_t<_XAction>, std::decay_t<XArgs>...>&) = delete;
	template<typename _XAction, typename ... XArgs>
	vitem_t& operator=(const vitem_t<std::decay_t<_XAction>, std::decay_t<XArgs>...>&) = delete;

	template<typename _XAction, typename ... XArgs>
	vitem_t(vitem_t<std::decay_t<_XAction>, std::decay_t<XArgs>...> && o)
		: base_t(std::forward<vitem_t<std::decay_t<_XAction>, std::decay_t<XArgs>...>>(o))
		, action(std::forward<_XAction>(o.action))
		, args(std::forward<typename vitem_t<std::decay_t<_XAction>, std::decay_t<XArgs>...>::tuple_args_t>(o.args)) {}

	operator bool()const noexcept final {
		return is_functional_empty(action);
		}
	};





template<typename __XAction, typename ... __XArgs>
class callback_wrapper_t final : public vitem_t<std::decay_t<__XAction>, std::decay_t<__XArgs>...> {
public:
	using base_type_t = typename vitem_t<std::decay_t<__XAction>, std::decay_t<__XArgs>...>;
	using callback_t = crm::utility::invoke_functor<void, async_expression_result_t &&>;

private:
	callback_t _clb;
	std::atomic<bool> _setf = false;
	std::atomic<bool> _invokef = false;

	void callback_invoke(async_expression_result_t && result) noexcept {
		bool invoked = false;
		if(_invokef.compare_exchange_strong(invoked, true)) {
			CBL_VERIFY(_clb);

			try {
				_clb(std::move(result));
				}
			catch(const dcf_exception_t & exc) {
				ntf_hndl(exc);
				}
			catch(const std::exception & exc1) {
				ntf_hndl(CREATE_EXC_S(exc1.what()));
				}
			catch(...) {
				ntf_hndl(CREATE_EXC(0));
				}
			}
		}

	static async_expression_result_t create_result(const std::exception & exc)noexcept {
		return create_result(CREATE_EXC_S(exc.what()));
		}

	static async_expression_result_t create_result(const dcf_exception_t & exc)noexcept {
		async_expression_result_t result;
		result.flag = async_operation_result_t::st_exception;
		result.exc = exc.dcpy();
		return result;
		};

	static async_expression_result_t create_result(async_operation_result_t flag)noexcept {
		async_expression_result_t result;
		result.flag = flag;
		return result;
		};

	void execute(bool onClear)noexcept {
		bool exef = false;
		if(_setf.compare_exchange_strong(exef, true)) {

			/* ����� �� ��������������� ������ */
			if(!onClear) {
				try {
					if(!canceled()) {
						base_type_t::execute();
						callback_invoke(create_result(async_operation_result_t::st_ready));
						}
					else {
						callback_invoke(create_result(async_operation_result_t::st_canceled));
						}
					}
				catch(const dcf_exception_t & exc) {
					callback_invoke(create_result(exc));
					}
				catch(const std::exception & exc1) {
					callback_invoke(create_result(exc1));
					}
				catch(...) {
					callback_invoke(create_result(CREATE_EXC(0)));
					}
				}
			else {
				callback_invoke(create_result(async_operation_result_t::st_abandoned));
				}
			}
		};

	void execute() final {
		execute(false);
		};

public:
	~callback_wrapper_t() {
		CHECK_NO_EXCEPTION(execute(true));
		}

	template<typename _YCallback,
		typename _YAction,
		typename ... YArgs,
		typename std::enable_if_t<std::is_invocable_v<_YCallback, async_expression_result_t &&>, int> = 0>
		callback_wrapper_t(_YCallback && clb,
			std::weak_ptr<lazy_action_stack_t> && baseStck,
			std::string_view  name,
			typename _YAction && action_,
			typename YArgs && ... args_)
		: base_type_t(std::move(baseStck)
			, name
			, std::forward<_YAction>(action_)
			, std::forward<YArgs>(args_)...)
		, _clb(std::forward<_YCallback>(clb)) {}

	template<typename _YCallback,
		typename _YAction,
		typename std::enable_if_t<std::is_invocable_v<_YCallback, async_expression_result_t &&>, int> = 0>
		callback_wrapper_t(_YCallback && clb,
			std::weak_ptr<lazy_action_stack_t> && baseStck,
			std::string_view  name,
			typename _YAction && action_)
		: base_type_t(std::move(baseStck)
			, name
			, std::forward<_YAction>(action_))
		, _clb(std::forward<_YCallback>(clb)) {}

	template<typename _XAction, typename ... XArgs>
	callback_wrapper_t(const callback_wrapper_t<std::decay_t<_XAction>, std::decay_t<XArgs>...>&) = delete;
	template<typename _XAction, typename ... XArgs>
	callback_wrapper_t& operator=(const callback_wrapper_t<std::decay_t<_XAction>, std::decay_t<XArgs>...>&) = delete;

	template<typename _XAction, typename ... XArgs>
	callback_wrapper_t(callback_wrapper_t<std::decay_t<_XAction>, std::decay_t<XArgs>...> && o)
		: base_type_t(std::forward<callback_wrapper_t<std::decay_t<_XAction>, std::decay_t<XArgs>...>>(o)
			, std::forward<_XAction>(o.action)
			, std::forward<typename callback_wrapper_t<std::decay_t<_XAction>, std::decay_t<XArgs>...>::base_type_args_t>(o.args)) {

		bool invokedf = false;
		if(o._invokef.compare_exchange_strong(invokedf, true)) {
			_clb = std::move(o._clb);
			o._clb = nullptr;
			}
		else
			THROW_EXC_FWD(nullptr);

		o._setf.store(true, std::memory_order::memory_order_release);
		}
	};


template<typename __XAction, typename ... __XArgs>
class promise_wrapper_t final : public vitem_t<std::decay_t<__XAction>, std::decay_t<__XArgs>...> {
public:
	using base_type_t = vitem_t<std::decay_t<__XAction>, std::decay_t<__XArgs>...>;

private:
	std::promise<std::unique_ptr<async_expression_result_t>> _result;
	std::shared_future<std::unique_ptr<async_expression_result_t>> _sharedFuture;
	std::atomic<bool> _setf = false;
	std::atomic<bool> _setInvoked = false;

	void set_value(std::unique_ptr<async_expression_result_t> && value) {
		bool seti = false;
		if(_setInvoked.compare_exchange_strong(seti, true)) {

			try {
				_result.set_value(std::move(value));
				}
			catch(const dcf_exception_t & exc) {
				ntf_hndl(exc);
				}
			catch(const std::exception & exc1) {
				ntf_hndl(CREATE_EXC_S(exc1.what()));
				}
			catch(...) {
				ntf_hndl(CREATE_EXC(0));
				}
			}
		}

	static std::unique_ptr<async_expression_result_t> create_result(const dcf_exception_t & exc)noexcept {
		auto result(std::make_unique<async_expression_result_t>());
		result->flag = async_operation_result_t::st_exception;
		result->exc = exc.dcpy();
		return result;
		}

	static std::unique_ptr<async_expression_result_t> create_result(const std::exception & exc)noexcept {
		return create_result(CREATE_EXC_S(exc.what()));
		}

	static std::unique_ptr<async_expression_result_t> create_result(async_operation_result_t flag)noexcept {
		auto result(std::make_unique<async_expression_result_t>());
		result->flag = flag;
		return result;
		}

	void execute(bool onClear)noexcept {
		bool exef = false;
		if(_setf.compare_exchange_strong(exef, true)) {

			/* ����� �� ��������������� ������ */
			if(!onClear) {
				try {
					if(!canceled()) {
						base_type_t::execute();
						set_value(create_result(async_operation_result_t::st_ready));
						}
					else {
						set_value(create_result(async_operation_result_t::st_canceled));
						}
					}
				catch(const dcf_exception_t & exc) {
					set_value(create_result(exc));
					}
				catch(const std::exception & exc1) {
					set_value(create_result(exc1));
					}
				catch(...) {
					set_value(create_result(CREATE_EXC(0)));
					}
				}
			else {
				set_value(create_result(async_operation_result_t::st_abandoned));
				}
			}
		}

	void execute() final {
		execute(false);
		}

public:
	~promise_wrapper_t() {
		CHECK_NO_EXCEPTION(execute(true));
		}

	template<typename _YAction, typename ... YArgs>
	promise_wrapper_t(std::weak_ptr<lazy_action_stack_t> && baseStck,
		std::string_view name,
		typename _YAction && action_,
		typename YArgs && ... args_)
		: base_type_t(std::move(baseStck)
			, name
			, std::forward<_YAction>(action_)
			, std::forward<YArgs>(args_)...) {

		_sharedFuture = _result.get_future().share();
		}

	template<typename _YAction>
	promise_wrapper_t(std::weak_ptr<lazy_action_stack_t> && baseStck,
		std::string_view name,
		typename _YAction && action_)
		: base_type_t(std::move(baseStck)
			, name
			, std::forward<_YAction>(action_)) {

		_sharedFuture = _result.get_future().share();
		}

	template<typename _XAction, typename ... XArgs>
	promise_wrapper_t(const promise_wrapper_t<std::decay_t<_XAction>, std::decay_t<XArgs>...>&) = delete;
	template<typename _XAction, typename ... XArgs>
	promise_wrapper_t& operator=(const promise_wrapper_t<std::decay_t<_XAction>, std::decay_t<XArgs>...>&) = delete;

	template<typename _XAction, typename ... XArgs>
	promise_wrapper_t(promise_wrapper_t<std::decay_t<_XAction>, std::decay_t<XArgs>...> && o)
		: base_type_t(std::forward<promise_wrapper_t<std::decay_t<_XAction>, std::decay_t<XArgs>...>>(o)
			, std::forward<_XAction>(o.action)
			, std::forward<typename promise_wrapper_t<std::decay_t<_XAction>, std::decay_t<XArgs>...>::base_type_args_t>(o.args)) {

		bool invokedf = false;
		if(o._invokef.compare_exchange_strong(invokedf, true)) {
			std::swap(_result, o._result);
			}
		else
			THROW_EXC_FWD(nullptr);

		o._setf.store(true, std::memory_order::memory_order_release);
		}

	std::shared_future<std::unique_ptr<async_expression_result_t>> get_future()const {
		return _sharedFuture;
		}
	};


template<typename _TAction, typename ... _TArgs>
using callable_functor_parpack_t = vitem_t<_TAction, _TArgs...>;

template<typename _TAction, typename ... TArgs>
std::unique_ptr<callable_functor_parpack_t<_TAction, TArgs...>>
make_callable_functor_pack(std::shared_ptr<lazy_action_stack_t> && baseStack,
	typename _TAction && action,
	typename TArgs && ... args) {

	return std::make_unique<callable_functor_parpack_t<_TAction, TArgs... >>(std::move(baseStack),
		__FILE_LINE__,
		std::forward<_TAction>(action),
		std::forward<TArgs>(args)...);
	}
}
