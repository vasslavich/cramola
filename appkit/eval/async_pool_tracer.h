#pragma once


#include <chrono>
#include <set>
#include <list>
#include <thread>
#include <mutex>
#include <string>
#include <atomic>
#include "../internal_invariants.h"


namespace crm::detail {


class lazy_action_stack_t;

#ifdef CBL_TRACE_LAZY_EXECUTION_STACK
struct activity_item_t {
	std::string name;
	std::chrono::system_clock::time_point launch_timepoint;
	};

bool operator == (const activity_item_t & lval, const activity_item_t &rval)noexcept;
bool operator < (const activity_item_t & lval, const activity_item_t &rval)noexcept;
bool operator >= (const activity_item_t & lval, const activity_item_t &rval)noexcept;


class activity_list_t {
private:
	std::multiset<activity_item_t> _activity;
	mutable std::mutex _activityLck;

public:
	void set_activity(activity_item_t && value);
	void rem_activity(const activity_item_t &value)noexcept;
	std::list<activity_item_t> get_activity(size_t count)const;
	};

class async_pool_tracer_t {
	std::thread _th;
	mutable std::mutex _objectsLck;
	std::list<std::weak_ptr<lazy_action_stack_t>> _objects;
	std::atomic<bool> _closef = false;

	std::list<std::shared_ptr<lazy_action_stack_t>> get_pools()noexcept;

public:
	async_pool_tracer_t()noexcept;
	~async_pool_tracer_t();

	void open();
	void close()noexcept;
	bool closed()const noexcept;
	size_t set(std::weak_ptr<lazy_action_stack_t> &&);
	size_t unset(const std::weak_ptr<lazy_action_stack_t> &)noexcept;
	};


void bind_global_context(std::weak_ptr<lazy_action_stack_t> &&)noexcept;
void unbind_global_context(const std::weak_ptr<lazy_action_stack_t> & )noexcept;

#endif
}

