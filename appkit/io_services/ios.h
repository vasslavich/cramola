#pragma once


#include <memory>
#include <thread>
#include <vector>
#include <atomic>
#include <future>
#include "../internal_invariants.h"
#include "../exceptions.h"
#include "../counters/internal_terms.h"


namespace crm{
namespace detail{
class shared_io_services_impl_t;
}


class shared_io_services_t{

public:
	typedef std::function<void( std::unique_ptr<dcf_exception_t> && nt )> notify_handler_t;

private:
	std::shared_ptr<detail::shared_io_services_impl_t> _impl;
	std::atomic<bool> _startf = false;

#ifdef CBL_MPF_OBJECTS_COUNTER_CHECKER_BOOST_IOS
	crm::utility::__xobject_counter_logger_t<shared_io_services_t> _xDbgCounter;
#endif

public:
	shared_io_services_t( notify_handler_t nf );
	~shared_io_services_t();

	void start( size_t concurrency_ );
	bool started()const noexcept;
	std::shared_ptr<detail::shared_io_services_impl_t> detail()const noexcept;
	void close()noexcept;
	void stop()noexcept;
	};
}


