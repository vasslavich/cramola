#include <iostream>
#include "../../internal.h"
#include "../ios.h"
#include "./ios_boost_impl.h"


using namespace crm;
using namespace crm::detail;


shared_io_services_t::shared_io_services_t( notify_handler_t nf )
	: _impl( std::make_unique<shared_io_services_impl_t>( std::move(nf) ) ) {}

shared_io_services_t::~shared_io_services_t() {}

void shared_io_services_t::start(size_t concurrency_) {
	bool stf = false;
	if(_startf.compare_exchange_strong(stf, true)) {
		_impl->start(concurrency_);
		}
	else {
		FATAL_ERROR_FWD(nullptr);
		}
	}

bool shared_io_services_t::started()const noexcept {
	return _impl->started();
	}

void shared_io_services_t::close() noexcept {
	_impl->close();
	}

void shared_io_services_t::stop()noexcept {
	_impl->stop();
	}

std::shared_ptr<crm::detail::shared_io_services_impl_t> shared_io_services_t::detail()const noexcept {
	return _impl;
	}

