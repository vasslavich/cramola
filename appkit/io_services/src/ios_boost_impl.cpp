#include "../../internal.h"
#include "./ios_boost_impl.h"
#include "../../exceptions.h"
#include "../../logging/logger.h"
#include <iostream>


using namespace crm;
using namespace crm::detail;


shared_io_services_impl_t::work_thread_t::work_thread_t() {}

shared_io_services_impl_t::work_thread_t::work_thread_t( shared_io_services_impl_t::work_thread_t && o )
	: _ownptr(std::move(o._ownptr))
	, _iow( std::move( o._iow ) )
	, _th( std::move( o._th ) )
	, _closedf( o._closedf.load() ) {}

shared_io_services_impl_t::work_thread_t& shared_io_services_impl_t::work_thread_t::operator=(
	shared_io_services_impl_t::work_thread_t && o) {

	if( _iow || _th )
		FATAL_ERROR_FWD(nullptr);

	_ownptr = std::move(o._ownptr);
	_iow = std::move( o._iow );
	_th = std::move( o._th );
	_closedf = o._closedf.load();

	return (*this);
	}

shared_io_services_impl_t::work_thread_t::work_thread_t(std::shared_ptr<boost::asio::io_service> pios,
	notify_handler_t && nfhndl,
	std::weak_ptr<shared_io_services_impl_t> ownp )
	: _ownptr(ownp)
	, _iow(std::make_unique<boost::asio::io_service::work>(*CHECK_PTR(pios))){

	_th = std::make_unique<std::thread>([this, pios, nfhndl] {

#ifdef CBL_TRACE_THREAD_CREATE_POINT
		CHECK_NO_EXCEPTION(crm::file_logger_t::logging(get_this_thread_info(),
			CBL_TRACE_THREAD_CREATE_POINT_TAG, __CBL_FILEW__, __LINE__));
#endif

		while(!closed() && !pios->stopped()) {
			try {
				pios->run();
				}
			catch(const dcf_exception_t & e0) {
				logger_t::logging(e0);
				}
			catch(const std::exception & e1) {
				logger_t::logging(e1);
				}
			catch(...) {
				logger_t::logging(__FILE_LINE__, 11001100);
				}
			}
		});
	}

bool shared_io_services_impl_t::work_thread_t::closed()const noexcept {
	if(_closedf.load(std::memory_order::memory_order_acquire)) {
		return true;
		}
	else {
		if(auto ownp = _ownptr.lock()) {
			return ownp->closed();
			}
		else {
			return true;
			}
		}
	}

void shared_io_services_impl_t::work_thread_t::_dctr()noexcept {
	bool closedf = false;
	if( _closedf.compare_exchange_strong( closedf, true ) ) {

		if( _th && _th->joinable() ) {
			_th->join();
			}
		}
	}

shared_io_services_impl_t::work_thread_t::~work_thread_t() {
	CHECK_NO_EXCEPTION( _dctr() );
	}

void shared_io_services_impl_t::start_io( size_t concurrency_ ) {

	bool startedf = false;
	if( _startedf.compare_exchange_strong( startedf, true ) ) {

		if(concurrency_) {
			lck_scp_t lck(_lck);
			for(size_t i = 0; i < concurrency_; ++i) {

				auto nh = _nfhndl;
				_ioThreads.emplace_back(_pIOS, std::move(nh), weak_from_this());
				}
			}
		else {
			FATAL_ERROR_FWD(nullptr);
			}
		}
	else {
		FATAL_ERROR_FWD(nullptr);
		}
	}

bool shared_io_services_impl_t::started()const noexcept {
	return _startedf.load(std::memory_order::memory_order_acquire);
	}

bool shared_io_services_impl_t::closed()const noexcept {
	return _closedf.load(std::memory_order::memory_order_acquire);
	}

const boost::asio::io_service& shared_io_services_impl_t::services()const {
	return (*_pIOS);
	}

boost::asio::io_service& shared_io_services_impl_t::services() {
	return (*_pIOS);
	}

shared_io_services_impl_t::shared_io_services_impl_t( notify_handler_t && nfhndl )
	: _pIOS(std::make_shared<boost::asio::io_service>())
	, _nfhndl( std::move( nfhndl ) ) {}

shared_io_services_impl_t::~shared_io_services_impl_t() {
	close();
	}

void shared_io_services_impl_t::start(size_t concurrency_) {
	std::call_once(_oncef, &shared_io_services_impl_t::start_io, this, concurrency_);
	}

void shared_io_services_impl_t::stop() noexcept {
	bool stoppedf = false;
	if(_stoppedf.compare_exchange_strong(stoppedf, true)) {
		services().stop();
		}
	}

void shared_io_services_impl_t::close() noexcept {

	bool closedf = false;
	if(_closedf.compare_exchange_strong(closedf, true)) {

		stop();

		lck_scp_t lck( _lck );
		_ioThreads.clear();
		}
	}

