#pragma once


#if defined(__ICC) || defined(__ICL)
#define BOOST_ASIO_HAS_MOVE 1
#endif

#include <boost/asio.hpp>
#include <iostream>
#include <memory>
#include <mutex>
#include <atomic>
#include "../../base_predecl.h"
#include "../../counters/internal_terms.h"


namespace crm::detail {


class shared_io_services_impl_t :
	public std::enable_shared_from_this<shared_io_services_impl_t> {

	typedef shared_io_services_impl_t self_t;

public:
	typedef std::function<void( std::unique_ptr<dcf_exception_t> && ntf )> notify_handler_t;

private:
	typedef std::mutex lck_t;
	typedef std::lock_guard<lck_t> lck_scp_t;

	class work_thread_t {
	private:
		std::weak_ptr< shared_io_services_impl_t> _ownptr;
		std::unique_ptr<boost::asio::io_service::work> _iow;
		std::unique_ptr< std::thread> _th;
		std::atomic<bool> _closedf = false;

		void _dctr()noexcept;

#ifdef CBL_MPF_OBJECTS_COUNTER_CHECKER_BOOST_FWDORK
		crm::utility::__xobject_counter_logger_t<work_thread_t> _xDbgCounter;
#endif

	public:
		work_thread_t();

		work_thread_t( std::shared_ptr<boost::asio::io_service> ios,
			notify_handler_t && nfhndl,
			std::weak_ptr<shared_io_services_impl_t> ownptr );

		work_thread_t( const work_thread_t & ) = delete;
		work_thread_t& operator=(const work_thread_t &) = delete;

		work_thread_t( work_thread_t && );
		work_thread_t& operator=(work_thread_t &&);

		~work_thread_t();

		bool closed()const noexcept;
		};

	std::atomic<bool> _startedf = false;
	std::atomic<bool> _stoppedf = false;
	std::atomic<bool> _closedf = false;

#ifdef CBL_MPF_OBJECTS_COUNTER_CHECKER_BOOST_IOS
	crm::utility::__xobject_counter_logger_t<self_t> _xDbgCounter;
#endif

	std::shared_ptr<boost::asio::io_service> _pIOS;
	notify_handler_t _nfhndl;
	std::once_flag _oncef;
	std::vector<work_thread_t> _ioThreads;
	lck_t _lck;

	void start_io(size_t concurrency_);
	
public:
	shared_io_services_impl_t( notify_handler_t && nfhndl );
	~shared_io_services_impl_t();

	boost::asio::io_service& services();
	const boost::asio::io_service& services()const;

	void start(size_t concurrency_);
	bool started()const noexcept;

	void close()noexcept;
	bool closed()const noexcept;

	void stop()noexcept;
	bool stopped()const noexcept;
	};
}

