#pragma once


#include <string>
#include <memory>


namespace crm{
namespace xml{

class xml_node_t;
class xml_element_t;
class xml_text_t;
class xml_document_t;
class xml_comment_t;
class xml_declaration_t;
class xml_unknown_t;
class xml_attribute_t;


enum class xml_error_t {
	XML_NO_ERROR = 0,
	XML_SUCCESS = 0,

	XML_NO_ATTRIBUTE,
	XML_FWDRONG_ATTRIBUTE_TYPE,

	XML_ERROR_FILE_NOT_FOUND,
	XML_ERROR_FILE_COULD_NOT_BE_OPENED,
	XML_ERROR_FILE_READ_ERROR,
	XML_ERROR_ELEMENT_MISMATCH,
	XML_ERROR_PARSING_ELEMENT,
	XML_ERROR_PARSING_ATTRIBUTE,
	XML_ERROR_IDENTIFYING_TAG,
	XML_ERROR_PARSING_TEXT,
	XML_ERROR_PARSING_CDATA,
	XML_ERROR_PARSING_COMMENT,
	XML_ERROR_PARSING_DECLARATION,
	XML_ERROR_PARSING_UNKNOWN,
	XML_ERROR_EMPTY_DOCUMENT,
	XML_ERROR_MISMATCHED_ELEMENT,
	XML_ERROR_PARSING,
	XML_ERROR_FWDRONG_CFGCTR,

	XML_CAN_NOT_CONVERT_TEXT,
	XML_NO_TEXT_NODE
	};


class xml_node_t {
	friend class xml_document_t;

	void *_pimpl = nullptr;
	bool _needFree = false;

protected:
	xml_node_t( void *impl, bool needFree = false );

	const void *base_ptr()const;
	void *base_ptr();

public:
	xml_node_t();
	virtual ~xml_node_t();

	std::string value() const;
	void set_value( const std::string &val );

	std::unique_ptr<xml_node_t>	parent() const;
	std::unique_ptr<xml_node_t> parent();

	bool no_children() const;

	std::unique_ptr<xml_node_t>	previous_sibling() const;
	std::unique_ptr<xml_node_t>	previous_sibling();

	std::unique_ptr<xml_node_t>	next_sibling() const;
	std::unique_ptr<xml_node_t>	next_sibling();

	std::unique_ptr<xml_node_t>  first_child() const;
	std::unique_ptr<xml_node_t>		first_child();

	std::unique_ptr<xml_node_t>	last_child() const;
	std::unique_ptr<xml_node_t>		last_child();

	std::unique_ptr<xml_element_t>	previous_sibling_element( const std::string &value = "" ) const;
	std::unique_ptr<xml_element_t>	previous_sibling_element( const std::string &value = "" );

	std::unique_ptr<xml_element_t>	next_sibling_element( const std::string &value = "" ) const;
	std::unique_ptr<xml_element_t>	next_sibling_element( const std::string &value = "" );

	std::unique_ptr<xml_element_t>  first_child_element( const std::string &name = "" ) const;
	std::unique_ptr<xml_element_t>		first_child_element( const std::string &name = "" );

	std::unique_ptr<xml_element_t>	last_child_element( const std::string &name = "" ) const;
	std::unique_ptr<xml_element_t>		last_child_element( const std::string &name = "" );

	std::unique_ptr<xml_node_t> insert_end_child( std::unique_ptr<xml_node_t> && addThis );
	std::unique_ptr<xml_node_t> link_end_child( std::unique_ptr<xml_node_t> && addThis );

	std::unique_ptr<xml_node_t> insert_first_child( std::unique_ptr<xml_node_t> && addThis );
	std::unique_ptr<xml_node_t> insert_after_child( std::unique_ptr<xml_node_t> && afterThis, 
		std::unique_ptr<xml_node_t> && addThis );

	void delete_child( std::unique_ptr<xml_node_t> && node );

	virtual std::unique_ptr<xml_element_t>		to_element() const;
	virtual std::unique_ptr<xml_text_t>			to_text() const;
	virtual std::unique_ptr<xml_comment_t>		to_comment() const;
	virtual std::unique_ptr<xml_document_t>		to_document() const;
	virtual std::unique_ptr<xml_declaration_t>	to_declaration() const;
	virtual std::unique_ptr<xml_unknown_t>		to_unknown() const;

	virtual std::unique_ptr<xml_element_t>		to_element();
	virtual std::unique_ptr<xml_text_t>			to_text();
	virtual std::unique_ptr<xml_comment_t>		to_comment();
	virtual std::unique_ptr<xml_document_t>		to_document();
	virtual std::unique_ptr<xml_declaration_t>	to_declaration();
	virtual std::unique_ptr<xml_unknown_t>		to_unknown();

	std::string error_message()const;
	bool has_error()const;
	};


class xml_attribute_t : public xml_node_t {
	friend class xml_node_t;
	friend class xml_element_t;

private:
	xml_attribute_t( void *impl );

public:
	xml_attribute_t();
	virtual ~xml_attribute_t();

	std::string name() const;
	std::string value() const;

	std::unique_ptr<xml_attribute_t> next() const;

	xml_error_t get( std::string &value ) const;
	xml_error_t get( int *val ) const;
	xml_error_t get( unsigned int *val ) const;
	xml_error_t get( bool *val ) const;
	xml_error_t get( double *val ) const;
	xml_error_t get( float *val ) const;

	void set( const std::string &value );
	void set( const char *value );
	void set( int value );
	void set( unsigned value );
	void set( bool value );
	void set( double value );
	void set( float value );
	};


class xml_element_t : public xml_node_t {
	friend class xml_node_t;
	friend class xml_document_t;

private:
	xml_element_t( void *impl );

public:
	xml_element_t();
	virtual ~xml_element_t();

	std::string name()const;
	void set_name(std::string name);
	std::unique_ptr<xml_element_t> to_element()const override;
	std::unique_ptr<xml_element_t> to_element() override;

	std::string attribute( const std::string &name, const std::string &value = "" ) const;
	xml_error_t get_attribute( const std::string &name, int *val ) const;
	xml_error_t get_attribute( const std::string &name, unsigned int *val ) const;
	xml_error_t get_attribute( const std::string &name, bool *val ) const;
	xml_error_t get_attribute( const std::string &name, double *val ) const;
	xml_error_t get_attribute( const std::string &name, float *val ) const;

	void set_attribute( const std::string &name, const char *value );
	void set_attribute( const std::string &name, const std::string &value );
	void set_attribute( const std::string &name, int value );
	void set_attribute( const std::string &name, unsigned int value );
	void set_attribute( const std::string &name, bool value );
	void set_attribute( const std::string &name, double value );
	void set_attribute( const std::string &name, float value );

	void delete_attribute( const std::string &name );

	std::unique_ptr<xml_attribute_t> first_attribute() const;
	std::unique_ptr< xml_attribute_t> find_attribute( const std::string &name ) const;

	std::string get_text() const;
	xml_error_t get_text( int* ival ) const;
	xml_error_t get_text( unsigned int* uval ) const;
	xml_error_t get_text( bool* bval ) const;
	xml_error_t get_text( double* dval ) const;
	xml_error_t get_text( float* fval ) const;

	void set_text( const std::string& inText );
	void set_text( const char *inText );
	void set_text( int value );
	void set_text( unsigned value );
	void set_text( bool value );
	void set_text( double value );
	void set_text( float value );
	};


class xml_document_t : public xml_node_t {
	friend class xml_node_t;
	typedef xml_node_t base_type_t;

private:
	xml_document_t( void *impl );

public:
	enum class whitespace {
		PRESERVE_FWDHITESPACE,
		COLLAPSE_FWDHITESPACE
		};

	xml_document_t( bool processEntities = true, whitespace whitespace_ = whitespace::PRESERVE_FWDHITESPACE );
	virtual ~xml_document_t();

	std::unique_ptr<xml_document_t> to_document()const override;
	std::unique_ptr<xml_document_t> to_document()override;

	int error_id()const;

	std::unique_ptr<xml_element_t> root_element()const;
	std::unique_ptr<xml_element_t> root_element();

	void file_load( const std::string &fileName );
	void file_write( const std::string &filename, bool compact = false );

	void parse( const std::string &data );
	void print()const;
	std::string prints()const;

	std::unique_ptr<xml_element_t> new_element( const std::string &name );
	std::unique_ptr<xml_comment_t> new_comment( const std::string &comment );
	std::unique_ptr<xml_text_t> new_text( const std::string &text );
	std::unique_ptr<xml_declaration_t> new_declaration( const std::string &text );
	std::unique_ptr<xml_unknown_t> new_unknown( const std::string &text );

	void delete_node( xml_node_t* node );

	void clear();
	};


class xml_text_t : public xml_node_t {
	friend class xml_node_t;
	friend class xml_document_t;

private:
	xml_text_t( void *impl );

public:
	xml_text_t();
	virtual ~xml_text_t();

	std::unique_ptr<xml_text_t> to_text()const override;
	std::unique_ptr<xml_text_t> to_text() override;
	};


class  xml_comment_t : public xml_node_t {
	friend class xml_node_t;
	friend class xml_document_t;

private:
	xml_comment_t( void *impl );

public:
	xml_comment_t();
	virtual ~xml_comment_t();

	std::unique_ptr<xml_comment_t> to_comment()const override;
	std::unique_ptr<xml_comment_t> to_comment()override;
	};


class xml_declaration_t : public xml_node_t {
	friend class xml_node_t;
	friend class xml_document_t;

private:
	xml_declaration_t( void *impl );

public:
	xml_declaration_t();
	virtual ~xml_declaration_t();

	std::unique_ptr<xml_declaration_t> to_declaration()const override;
	std::unique_ptr<xml_declaration_t> to_declaration() override;
	};


class xml_unknown_t : public xml_node_t {
	friend class xml_node_t;
	friend class xml_document_t;

private:
	xml_unknown_t( void *impl );

public:
	xml_unknown_t();
	virtual ~xml_unknown_t();

	std::unique_ptr<xml_unknown_t> to_unknown()const override;
	std::unique_ptr<xml_unknown_t> to_unknown() override;
	};
}
}

