#include <sstream>
#include <filesystem>
#include "../../internal.h"
#include "../../xml/xml.h"
#include "./tinyxml/tinyxml2.h"
#include "../../notifications/internal_terms.h"
#include "./xml_util.h"


using namespace tinyxml2;
using namespace crm;
using namespace crm::xml;


#define XML_DOC_CAST(ptr) ((XMLDocument*)(ptr))
#define XML_CDOC_CAST(ptr) ((const XMLDocument*)(ptr))


xml_document_t::xml_document_t( void *impl ) : xml_node_t( impl ) {}

xml_document_t::xml_document_t( bool processEntities, whitespace whitespace_ )
	: base_type_t( (void*)new XMLDocument( processEntities, (Whitespace)(int)whitespace_ ), true ){}

xml_document_t::~xml_document_t() {}

std::unique_ptr<xml_document_t> xml_document_t::to_document()const {
	auto p = XML_DOC_CAST( base_ptr() )->ToDocument();
	if( nullptr == p )return nullptr;

	return std::unique_ptr<xml_document_t>( new xml_document_t( p ) );
	}

std::unique_ptr<xml_document_t> xml_document_t::to_document() {
	auto p = XML_DOC_CAST( base_ptr() )->ToDocument();
	if( nullptr == p )return nullptr;

	return std::unique_ptr<xml_document_t>( new xml_document_t( p ) );
	}

int xml_document_t::error_id()const {
	return XML_CDOC_CAST( base_ptr() )->ErrorID();
	}

std::unique_ptr<xml_element_t> xml_document_t::root_element()const {
	auto p = XML_CDOC_CAST( base_ptr() )->RootElement();
	if( nullptr == p )return nullptr;

	return std::unique_ptr<xml_element_t>( new xml_element_t( (void*)p ) );
	}

std::unique_ptr<xml_element_t> xml_document_t::root_element() {
	auto p = XML_DOC_CAST( base_ptr() )->RootElement();
	if( nullptr == p )return nullptr;

	return std::unique_ptr<xml_element_t>( new xml_element_t( (void*)p ) );
	}

void xml_document_t::file_load(const std::string &fileName) {
	if (std::filesystem::exists(fileName)) {
		XMLError load_error = XML_DOC_CAST(base_ptr())->LoadFile(fileName.c_str());
		if (load_error == XML_ERROR_EMPTY_DOCUMENT) {
			FILE* file = fopen(fileName.c_str(), "w");
			if (file == nullptr) {/*��� ������ ������ ������*/ }
			else {
				fputs("<?xml version=\"1.0\" encoding=\"utf - 8\"?> \n <dcf> \n </dcf>", file);
				fclose(file);
				XML_DOC_CAST(base_ptr())->LoadFile(fileName.c_str());
				}
			}
		TEST_ERROR; //������� ���������� �� �������������� ��� ������ ����.
		}
	else {
		THROW_EXC_FWD("file open fail=" + fileName);
		}
	}

void xml_document_t::file_write( const std::string &filename, bool compact ) {
	XML_DOC_CAST( base_ptr() )->SaveFile( filename.c_str(), compact );
	TEST_ERROR;
	}

void xml_document_t::parse( const std::string &data ) {
	XML_DOC_CAST( base_ptr() )->Parse( data.c_str(), data.size() );
	TEST_ERROR;
	}

void xml_document_t::print()const {
	XML_CDOC_CAST( base_ptr() )->Print();
	}

std::string xml_document_t::prints()const {
	XMLPrinter printer;
	XML_CDOC_CAST( base_ptr() )->Print(&printer);
	return printer.CStr();
	}

std::unique_ptr<xml_element_t> xml_document_t::new_element( const std::string &name ) {
	auto p = XML_DOC_CAST( base_ptr() )->NewElement( name.empty() ? 0 : name.c_str() );
	TEST_ERROR;

	if( nullptr == p )return nullptr;
	return std::unique_ptr<xml_element_t>( new xml_element_t( p ) );
	}

std::unique_ptr<xml_comment_t> xml_document_t::new_comment( const std::string &comment ) {
	auto p = XML_DOC_CAST( base_ptr() )->NewComment( comment.empty() ? 0 : comment.c_str() );
	TEST_ERROR;

	if( nullptr == p )return nullptr;
	return std::unique_ptr<xml_comment_t>( new xml_comment_t( p ) );
	}

std::unique_ptr<xml_text_t> xml_document_t::new_text( const std::string &text ) {
	auto p = XML_DOC_CAST( base_ptr() )->NewText( text.empty() ? 0 : text.c_str() );
	TEST_ERROR;

	if( nullptr == p )
		return nullptr;
	else
		return std::unique_ptr<xml_text_t>( new xml_text_t( p ) );
	}

std::unique_ptr<xml_declaration_t> xml_document_t::new_declaration( const std::string &text ) {
	auto p = XML_DOC_CAST( base_ptr() )->NewDeclaration( text.empty() ? 0 : text.c_str() );
	TEST_ERROR;

	if( nullptr == p )return nullptr;
	return std::unique_ptr<xml_declaration_t>( new xml_declaration_t( p ) );
	}

std::unique_ptr<xml_unknown_t> xml_document_t::new_unknown( const std::string &text ) {
	auto p = XML_DOC_CAST( base_ptr() )->NewUnknown( text.empty() ? 0 : text.c_str() );
	TEST_ERROR;

	if( nullptr == p )return nullptr;
	return std::unique_ptr<xml_unknown_t>( new xml_unknown_t( p ) );
	}

void xml_document_t::delete_node( xml_node_t* node ) {
	XML_DOC_CAST( base_ptr() )->DeleteNode( (XMLNode*)node->base_ptr() );
	TEST_ERROR;
	}

void xml_document_t::clear() {
	XML_DOC_CAST( base_ptr() )->Clear();
	TEST_ERROR;
	}


#undef XML_DOC_CAST
#undef XML_CDOC_CAST
