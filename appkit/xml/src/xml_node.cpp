#include <sstream>
#include "../../internal.h"
#include "../../xml/xml.h"
#include "./tinyxml/tinyxml2.h"
#include "../../notifications/internal_terms.h"
#include "./xml_util.h"


using namespace tinyxml2;
using namespace crm;
using namespace crm::xml;


#define XML_NODE_CAST(ptr) ((XMLNode*)(ptr))
#define XML_CNODE_CAST(ptr) ((const XMLNode*)(ptr))


xml_node_t::xml_node_t() {}

xml_node_t::xml_node_t( void *p, bool needFree )
	: _pimpl( p ) 
	, _needFree( needFree ){}

xml_node_t::~xml_node_t() {
	if( _needFree && _pimpl )
		delete (XMLDocument*)_pimpl;
	}

std::string xml_node_t::value() const{
	return XML_NODE_CAST( base_ptr() )->Value();
	}

const void* xml_node_t::base_ptr() const {
	return _pimpl;
}

void* xml_node_t::base_ptr() {
	return _pimpl;
	}

void xml_node_t::set_value( const std::string &val ){
	XML_NODE_CAST( base_ptr() )->SetValue( val.empty() ? 0 : val.c_str() );
	TEST_ERROR;
	}

std::unique_ptr<xml_node_t>	xml_node_t::parent() const {
	auto p = XML_NODE_CAST( base_ptr() )->Parent();
	if( nullptr == p )return nullptr;

	return std::unique_ptr<xml_node_t>( new xml_node_t( p ) );
	}

std::unique_ptr<xml_node_t> xml_node_t::parent() {
	auto p = XML_NODE_CAST( base_ptr() )->Parent();
	if( nullptr == p )return nullptr;

	return std::unique_ptr<xml_node_t>( new xml_node_t( p ) );
	}

bool xml_node_t::no_children() const {
	return XML_CNODE_CAST( base_ptr() )->NoChildren();
	}

std::unique_ptr<xml_node_t>	xml_node_t::previous_sibling() const {
	auto p = XML_NODE_CAST( base_ptr() )->PreviousSibling();
	if( nullptr == p )return nullptr;

	return std::unique_ptr<xml_node_t>( new xml_node_t( p ) );
	}

std::unique_ptr<xml_node_t>	xml_node_t::previous_sibling() {
	auto p = XML_NODE_CAST( base_ptr() )->PreviousSibling();
	if( nullptr == p )return nullptr;

	return std::unique_ptr<xml_node_t>( new xml_node_t( p ) );
	}

std::unique_ptr<xml_node_t>	xml_node_t::next_sibling() const {
	auto p = XML_NODE_CAST( base_ptr() )->NextSibling();
	if( nullptr == p )return nullptr;

	return std::unique_ptr<xml_node_t>( new xml_node_t( p ) );
	}

std::unique_ptr<xml_node_t>	xml_node_t::next_sibling() {
	auto p = XML_NODE_CAST( base_ptr() )->NextSibling();
	if( nullptr == p )return nullptr;

	return std::unique_ptr<xml_node_t>( new xml_node_t( p ) );
	}

std::unique_ptr<xml_node_t>  xml_node_t::first_child() const {
	auto p = XML_NODE_CAST( base_ptr() )->FirstChild();
	if( nullptr == p )return nullptr;

	return std::unique_ptr<xml_node_t>( new xml_node_t( p ) );
	}

std::unique_ptr<xml_node_t>		xml_node_t::first_child() {
	auto p = XML_NODE_CAST( base_ptr() )->FirstChild();
	if( nullptr == p )return nullptr;

	return std::unique_ptr<xml_node_t>( new xml_node_t( p ) );
	}

std::unique_ptr<xml_node_t>	xml_node_t::last_child() const {
	auto p = XML_NODE_CAST( base_ptr() )->LastChild();
	if( nullptr == p )return nullptr;

	return std::unique_ptr<xml_node_t>( new xml_node_t( p ) );
	}

std::unique_ptr<xml_node_t>		xml_node_t::last_child() {
	auto p = XML_NODE_CAST( base_ptr() )->LastChild();
	if( nullptr == p )return nullptr;

	return std::unique_ptr<xml_node_t>( new xml_node_t( p ) );
	}

std::unique_ptr<xml_element_t>	xml_node_t::previous_sibling_element( const std::string &value ) const {
	auto p = XML_NODE_CAST( base_ptr() )->PreviousSiblingElement( value.empty() ? 0 : value.c_str() );
	if( nullptr == p )return nullptr;

	return std::unique_ptr<xml_element_t>( new xml_element_t( p ) );
	}

std::unique_ptr<xml_element_t>	xml_node_t::previous_sibling_element( const std::string &value ) {
	auto p = XML_NODE_CAST( base_ptr() )->PreviousSiblingElement( value.empty() ? 0 : value.c_str() );
	if( nullptr == p )return nullptr;

	return std::unique_ptr<xml_element_t>( new xml_element_t( p ) );
	}

std::unique_ptr<xml_element_t>	xml_node_t::next_sibling_element(const std::string &value ) const {
	auto p = XML_NODE_CAST( base_ptr() )->NextSiblingElement( value.empty() ? 0 : value.c_str() );
	if( nullptr == p )return nullptr;

	return std::unique_ptr<xml_element_t>( new xml_element_t( p ) );
	}

std::unique_ptr<xml_element_t>	xml_node_t::next_sibling_element(const std::string &value) {
	auto p = XML_NODE_CAST( base_ptr() )->NextSiblingElement( value.empty() ? 0 : value.c_str() );
	if( nullptr == p )return nullptr;

	return std::unique_ptr<xml_element_t>( new xml_element_t( p ) );
	}

std::unique_ptr<xml_element_t>  xml_node_t::first_child_element( const std::string &name ) const {
	auto p = XML_NODE_CAST( base_ptr() )->FirstChildElement( name.empty() ? 0 : name.c_str() );
	if( nullptr == p )return nullptr;

	return std::unique_ptr<xml_element_t>( new xml_element_t( p ) );
	}

std::unique_ptr<xml_element_t>		xml_node_t::first_child_element( const std::string &name ) {
	auto p = XML_NODE_CAST( base_ptr() )->FirstChildElement( name.empty() ? 0 : name.c_str() );
	if( nullptr == p )return nullptr;

	return std::unique_ptr<xml_element_t>( new xml_element_t( p ) );
	}

std::unique_ptr<xml_element_t>	xml_node_t::last_child_element( const std::string &name ) const {
	auto p = XML_NODE_CAST( base_ptr() )->LastChildElement( name.empty() ? 0 : name.c_str() );
	if( nullptr == p )return nullptr;

	return std::unique_ptr<xml_element_t>( new xml_element_t( p ) );
	}

std::unique_ptr<xml_element_t>		xml_node_t::last_child_element( const std::string &name ) {
	auto p = XML_NODE_CAST( base_ptr() )->LastChildElement( name.empty() ? 0 : name.c_str() );
	if( nullptr == p )return nullptr;

	return std::unique_ptr<xml_element_t>( new xml_element_t( p ) );
	}

std::unique_ptr<xml_node_t> xml_node_t::insert_end_child( std::unique_ptr<xml_node_t> && addThis ) {
	auto p = XML_NODE_CAST( base_ptr() )->InsertEndChild( (XMLNode*)addThis->base_ptr() );
	if( nullptr == p )return nullptr;

	return std::unique_ptr<xml_node_t>( new xml_node_t( p ) );
	}

std::unique_ptr<xml_node_t> xml_node_t::link_end_child( std::unique_ptr<xml_node_t> && addThis ) {
	auto p = XML_NODE_CAST( base_ptr() )->LinkEndChild( (XMLNode*)addThis->base_ptr() );
	if( nullptr == p )return nullptr;

	return std::unique_ptr<xml_node_t>( new xml_node_t( p ) );
	}

std::unique_ptr<xml_node_t> xml_node_t::insert_first_child( std::unique_ptr<xml_node_t> && addThis ) {
	auto p = XML_NODE_CAST( base_ptr() )->InsertFirstChild( (XMLNode*)addThis->base_ptr() );
	if( nullptr == p )return nullptr;

	return std::unique_ptr<xml_node_t>( new xml_node_t( p ) );
	}

std::unique_ptr<xml_node_t> xml_node_t::insert_after_child( std::unique_ptr<xml_node_t> && afterThis,
	std::unique_ptr<xml_node_t> && addThis ) {

	auto p = XML_NODE_CAST( base_ptr() )->InsertAfterChild( (XMLNode*)afterThis->base_ptr(), (XMLNode*)addThis->base_ptr() );
	if( nullptr == p )return nullptr;

	return std::unique_ptr<xml_node_t>( new xml_node_t( p ) );
	}

void xml_node_t::delete_child( std::unique_ptr<xml_node_t> && node ) {
	XML_NODE_CAST( base_ptr() )->DeleteChild( (XMLNode*)node->base_ptr() );
	TEST_ERROR;
	}

std::unique_ptr<xml_element_t>		xml_node_t::to_element() const {
	auto p = XML_NODE_CAST( base_ptr() )->ToElement();
	if( nullptr == p )return nullptr;

	return std::unique_ptr<xml_element_t>( new xml_element_t( p ) );
	}

std::unique_ptr< xml_text_t>			xml_node_t::to_text() const {
	auto p = XML_NODE_CAST( base_ptr() )->ToText();
	if( nullptr == p )return nullptr;

	return std::unique_ptr< xml_text_t>( new xml_text_t( p ) );
	}

std::unique_ptr< xml_comment_t>		xml_node_t::to_comment() const {
	auto p = XML_NODE_CAST( base_ptr() )->ToComment();
	if( nullptr == p )return nullptr;

	return std::unique_ptr< xml_comment_t>( new xml_comment_t( p ) );
	}

std::unique_ptr< xml_document_t>		xml_node_t::to_document() const {
	auto p = XML_NODE_CAST( base_ptr() )->ToDocument();
	if( nullptr == p )return nullptr;

	return std::unique_ptr< xml_document_t>( new xml_document_t( p ) );
	}

std::unique_ptr< xml_declaration_t>	xml_node_t::to_declaration() const {
	auto p = XML_NODE_CAST( base_ptr() )->ToDeclaration();
	if( nullptr == p )return nullptr;

	return std::unique_ptr< xml_declaration_t>( new xml_declaration_t( p ) );
	}

std::unique_ptr< xml_unknown_t>		xml_node_t::to_unknown() const {
	auto p = XML_NODE_CAST( base_ptr() )->ToUnknown();
	if( nullptr == p )return nullptr;

	return std::unique_ptr< xml_unknown_t>( new xml_unknown_t( p ) );
	}

std::unique_ptr<xml_element_t>		xml_node_t::to_element() {
	auto p = XML_NODE_CAST( base_ptr() )->ToElement();
	if( nullptr == p )return nullptr;

	return std::unique_ptr<xml_element_t>( new xml_element_t( p ) );
	}

std::unique_ptr< xml_text_t>			xml_node_t::to_text() {
	auto p = XML_NODE_CAST( base_ptr() )->ToText();
	if( nullptr == p )return nullptr;

	return std::unique_ptr< xml_text_t>( new xml_text_t( p ) );
	}

std::unique_ptr< xml_comment_t>		xml_node_t::to_comment() {
	auto p = XML_NODE_CAST( base_ptr() )->ToComment();
	if( nullptr == p )return nullptr;

	return std::unique_ptr< xml_comment_t>( new xml_comment_t( p ) );
	}

std::unique_ptr< xml_document_t>		xml_node_t::to_document() {
	auto p = XML_NODE_CAST( base_ptr() )->ToDocument();
	if( nullptr == p )return nullptr;

	return std::unique_ptr< xml_document_t>( new xml_document_t( p ) );
	}

std::unique_ptr< xml_declaration_t>	xml_node_t::to_declaration() {
	auto p = XML_NODE_CAST( base_ptr() )->ToDeclaration();
	if( nullptr == p )return nullptr;

	return std::unique_ptr< xml_declaration_t>( new xml_declaration_t( p ) );
	}

std::unique_ptr< xml_unknown_t>		xml_node_t::to_unknown() {
	auto p = XML_NODE_CAST( base_ptr() )->ToUnknown();
	if( nullptr == p )return nullptr;

	return std::unique_ptr< xml_unknown_t>( new xml_unknown_t( p ) );
	}


std::string xml_node_t::error_message()const {
	if( nullptr != base_ptr() ) {
		XMLDocument* doc = ((XMLNode*)base_ptr())->GetDocument();
		if( doc->Error() ) {
			std::ostringstream sbuf;

			sbuf << "first message = " << doc->GetErrorStr1() << std::endl;
			sbuf << "second message = " << doc->GetErrorStr2() << std::endl;

			return std::move( sbuf.str() );
			}
		}
	
	return "success";
	}

bool xml_node_t::has_error()const {
	if( nullptr != base_ptr() ) {
		XMLDocument* doc = ((XMLNode*)base_ptr())->GetDocument();
		if( doc->Error() )return true;
		}

	return false;
	}





#undef XML_NODE_CAST
#undef XML_CNODE_CAST
