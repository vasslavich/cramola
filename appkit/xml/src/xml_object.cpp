#include <sstream>
#include "../../internal.h"
#include "../../xml/xml.h"
#include "./tinyxml/tinyxml2.h"
#include "../../notifications/internal_terms.h"
#include "./xml_util.h"


using namespace tinyxml2;
using namespace crm;
using namespace crm::xml;



xml_text_t::xml_text_t( void *impl ) : xml_node_t( impl ) {}

xml_text_t::xml_text_t() {}

xml_text_t::~xml_text_t() {}

std::unique_ptr< xml_text_t> xml_text_t::to_text()const {
	auto p = ((XMLText*)base_ptr())->ToText();
	if( nullptr == p )return nullptr;

	return std::unique_ptr< xml_text_t>( new xml_text_t( p ) );
	}

std::unique_ptr< xml_text_t> xml_text_t::to_text() {
	auto p = ((XMLText*)base_ptr())->ToText();
	if( nullptr == p )return nullptr;

	return std::unique_ptr< xml_text_t>( new xml_text_t( p ) );
	}


xml_comment_t::xml_comment_t( void *impl ) : xml_node_t( impl ) {}

xml_comment_t::xml_comment_t() {}
xml_comment_t::~xml_comment_t() {}

std::unique_ptr< xml_comment_t> xml_comment_t::to_comment()const {
	auto p = ((XMLComment*)base_ptr())->ToComment();
	if( nullptr == p )return nullptr;

	return  std::unique_ptr< xml_comment_t>( new xml_comment_t( p ) );
	}

std::unique_ptr< xml_comment_t> xml_comment_t::to_comment() {
	auto p = ((XMLComment*)base_ptr())->ToComment();
	if( nullptr == p )return nullptr;

	return  std::unique_ptr< xml_comment_t>( new xml_comment_t( p ) );
	}


xml_declaration_t::xml_declaration_t( void *impl ) : xml_node_t( impl ) {}

xml_declaration_t::xml_declaration_t() {}
xml_declaration_t::~xml_declaration_t() {}

std::unique_ptr<  xml_declaration_t> xml_declaration_t::to_declaration()const {
	auto p = ((XMLDeclaration*)base_ptr())->ToDeclaration();
	if( nullptr == p )return nullptr;

	return std::unique_ptr<  xml_declaration_t>( new xml_declaration_t( p ) );
	}

std::unique_ptr< xml_declaration_t> xml_declaration_t::to_declaration() {
	auto p = ((XMLDeclaration*)base_ptr())->ToDeclaration();
	if( nullptr == p )return nullptr;

	return std::unique_ptr<  xml_declaration_t>( new xml_declaration_t( p ) );
	}



xml_unknown_t::xml_unknown_t( void *impl ) : xml_node_t( impl ) {}

xml_unknown_t::xml_unknown_t() {}
xml_unknown_t::~xml_unknown_t() {}

std::unique_ptr<  xml_unknown_t> xml_unknown_t::to_unknown()const {
	auto p = ((XMLUnknown*)base_ptr())->ToUnknown();
	if( nullptr == p )return nullptr;

	return std::unique_ptr<  xml_unknown_t>( new xml_unknown_t( p ) );
	}

std::unique_ptr< xml_unknown_t> xml_unknown_t::to_unknown() {
	auto p = ((XMLUnknown*)base_ptr())->ToUnknown();
	if( nullptr == p )return nullptr;

	return std::unique_ptr<  xml_unknown_t>( new xml_unknown_t( p ) );
	}

