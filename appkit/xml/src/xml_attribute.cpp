#include "../../internal.h"
#include "../../xml/xml.h"
#include "./tinyxml/tinyxml2.h"
#include "../../notifications/internal_terms.h"
#include "./xml_util.h"


using namespace tinyxml2;
using namespace crm;
using namespace crm::xml;


#define XML_ATTR_CAST(ptr) ((XMLAttribute*)(ptr))
#define XML_CATTR_CAST(ptr) ((const XMLAttribute*)(ptr))


xml_attribute_t::xml_attribute_t( void *impl ) : xml_node_t( impl ) {}

xml_attribute_t::xml_attribute_t() {}

xml_attribute_t::~xml_attribute_t() {}

std::string xml_attribute_t::name() const {
	return XML_ATTR_CAST( base_ptr() )->Name();
	}

std::string xml_attribute_t::value() const {
	return XML_ATTR_CAST( base_ptr() )->Value();
	}

std::unique_ptr<xml_attribute_t> xml_attribute_t::next() const {
	auto p = XML_ATTR_CAST( base_ptr() )->Next();
	if( nullptr == p )return nullptr;

	return std::unique_ptr<xml_attribute_t>( new xml_attribute_t( (void*)p ) );
	}

xml_error_t xml_attribute_t::get( std::string &value ) const {
	value = XML_ATTR_CAST( base_ptr() )->Value();
	return xml_error_t::XML_SUCCESS;
	}

xml_error_t xml_attribute_t::get( int *val ) const {
	return (xml_error_t)XML_ATTR_CAST( base_ptr() )->QueryIntValue( val );
	}

xml_error_t xml_attribute_t::get( unsigned int *val ) const {
	return (xml_error_t)XML_ATTR_CAST( base_ptr() )->QueryUnsignedValue( val );
	}

xml_error_t xml_attribute_t::get( bool *val ) const {
	return (xml_error_t)XML_ATTR_CAST( base_ptr() )->QueryBoolValue( val );
	}

xml_error_t xml_attribute_t::get( double *val ) const {
	return (xml_error_t)XML_ATTR_CAST( base_ptr() )->QueryDoubleValue( val );
	}

xml_error_t xml_attribute_t::get( float *val ) const {
	return (xml_error_t)XML_ATTR_CAST( base_ptr() )->QueryFloatValue( val );
	}

void xml_attribute_t::set( const std::string &value ) {
	XML_ATTR_CAST( base_ptr() )->SetAttribute( value.empty() ? 0 : value.c_str() );
	TEST_ERROR;
	}

void xml_attribute_t::set( const char *value ) {
	XML_ATTR_CAST( base_ptr() )->SetAttribute( value );
	TEST_ERROR;
}

void xml_attribute_t::set( int value ) {
	XML_ATTR_CAST( base_ptr() )->SetAttribute( value );
	TEST_ERROR;
	}

void xml_attribute_t::set( unsigned value ) {
	XML_ATTR_CAST( base_ptr() )->SetAttribute( value );
	TEST_ERROR;
	}

void xml_attribute_t::set( bool value ) {
	XML_ATTR_CAST( base_ptr() )->SetAttribute( value );
	TEST_ERROR;
	}

void xml_attribute_t::set( double value ) {
	XML_ATTR_CAST( base_ptr() )->SetAttribute( value );
	TEST_ERROR;
	}

void xml_attribute_t::set( float value ) {
	XML_ATTR_CAST( base_ptr() )->SetAttribute( value );
	TEST_ERROR;
	}



#undef XML_ATTR_CAST
#undef XML_CATTR_CAST
