#pragma once

#include "../../notifications/internal_terms.h"
#include "../../utilities/convert/cvt.h"

#define TEST_ERROR	{\
	if( has_error() )\
	throw crm::dcf_exception_t( error_message(), __CBL_FILEW__, __LINE__ );\
	}


