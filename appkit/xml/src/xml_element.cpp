#include "../../internal.h"
#include "../../xml/xml.h"
#include "./tinyxml/tinyxml2.h"
#include "../../notifications/internal_terms.h"
#include "./xml_util.h"


using namespace tinyxml2;
using namespace crm;
using namespace crm::xml;


#define XML_ELEM_CAST(ptr) ((XMLElement*)(ptr))
#define XML_CELEM_CAST(ptr) ((const XMLElement*)(ptr))


xml_element_t::xml_element_t( void *impl ) : xml_node_t( impl ) {}

xml_element_t::xml_element_t() {}

xml_element_t::~xml_element_t() {}

std::string xml_element_t::name()const {
	return XML_ELEM_CAST( base_ptr() )->Name();
	}

void xml_element_t::set_name(std::string name) {
	return XML_ELEM_CAST( base_ptr() )->SetName(name.c_str());
}

std::unique_ptr<xml_element_t> xml_element_t::to_element()const {
	auto p = XML_ELEM_CAST( base_ptr() )->ToElement();
	if( nullptr == p )return nullptr;

	return std::unique_ptr<xml_element_t>( new xml_element_t( (void*)p ) );
	}

std::unique_ptr<xml_element_t> xml_element_t::to_element() {
	auto p = XML_ELEM_CAST( base_ptr() )->ToElement();
	if( nullptr == p )return nullptr;

	return std::unique_ptr<xml_element_t>( new xml_element_t( (void*)p ) );
	}

std::string xml_element_t::attribute( const std::string &name, const std::string &value ) const {
	auto p = XML_CELEM_CAST( base_ptr() )->Attribute( name.empty() ? 0 : name.c_str(), value.empty() ? 0 : value.c_str() );
	if( nullptr == p )return "";

	return std::string( p );
	}

xml_error_t xml_element_t::get_attribute( const std::string &name, int *val ) const {
	return (xml_error_t)XML_CELEM_CAST( base_ptr() )->QueryAttribute( name.empty() ? 0 : name.c_str(), val );
}

xml_error_t xml_element_t::get_attribute( const std::string &name, unsigned int *val ) const {
	return (xml_error_t)XML_CELEM_CAST( base_ptr() )->QueryAttribute( name.empty() ? 0 : name.c_str(), val );
	}

xml_error_t xml_element_t::get_attribute( const std::string &name, bool *val ) const {
	return (xml_error_t)XML_CELEM_CAST( base_ptr() )->QueryAttribute( name.empty() ? 0 : name.c_str(), val );
	}

xml_error_t xml_element_t::get_attribute( const std::string &name, double *val ) const {
	return (xml_error_t)XML_CELEM_CAST( base_ptr() )->QueryAttribute( name.empty() ? 0 : name.c_str(), val );
	}

xml_error_t xml_element_t::get_attribute( const std::string &name, float *val ) const {
	return (xml_error_t)XML_CELEM_CAST( base_ptr() )->QueryAttribute( name.empty() ? 0 : name.c_str(), val );
	}

void xml_element_t::set_attribute( const std::string &name, const char *value ) {
	XML_ELEM_CAST( base_ptr() )->SetAttribute( name.empty() ? 0 : name.c_str(), value );
	TEST_ERROR;
}

void xml_element_t::set_attribute( const std::string &name, const std::string &value ) {
	XML_ELEM_CAST( base_ptr() )->SetAttribute( name.empty() ? 0 : name.c_str(), value.empty() ? 0 : value.c_str() );
	TEST_ERROR;
	}

void xml_element_t::set_attribute( const std::string &name, int value ) {
	XML_ELEM_CAST( base_ptr() )->SetAttribute( name.empty() ? 0 : name.c_str(), value );
	TEST_ERROR;
	}

void xml_element_t::set_attribute( const std::string &name, unsigned int value ) {
	XML_ELEM_CAST( base_ptr() )->SetAttribute( name.empty() ? 0 : name.c_str(), value );
	TEST_ERROR;
	}

void xml_element_t::set_attribute( const std::string &name, bool value ) {
	XML_ELEM_CAST( base_ptr() )->SetAttribute( name.empty() ? 0 : name.c_str(), value );
	TEST_ERROR;
	}

void xml_element_t::set_attribute( const std::string &name, double value ) {
	XML_ELEM_CAST( base_ptr() )->SetAttribute( name.empty() ? 0 : name.c_str(), value );
	TEST_ERROR;
	}

void xml_element_t::set_attribute( const std::string &name, float value ) {
	XML_ELEM_CAST( base_ptr() )->SetAttribute( name.empty() ? 0 : name.c_str(), value );
	TEST_ERROR;
	}

void xml_element_t::delete_attribute( const std::string &name ) {
	XML_ELEM_CAST( base_ptr() )->DeleteAttribute( name.empty() ? 0 : name.c_str() );
	TEST_ERROR;
	}

std::unique_ptr<xml_attribute_t> xml_element_t::first_attribute() const {
	auto p = XML_ELEM_CAST( base_ptr() )->FirstAttribute();
	if( nullptr == p )return nullptr;

	return std::unique_ptr<xml_attribute_t>( new xml_attribute_t( (void*)p ) );
	}

std::unique_ptr<xml_attribute_t> xml_element_t::find_attribute( const std::string &name ) const {
	auto p = XML_CELEM_CAST( base_ptr() )->FindAttribute( name.empty() ? 0 : name.c_str() );
	if( nullptr == p )return nullptr;

	return std::unique_ptr<xml_attribute_t>( new xml_attribute_t( (void*)p ) );
	}


std::string xml_element_t::get_text() const {
	auto p = XML_CELEM_CAST( base_ptr() )->GetText();
	if( nullptr == p )return "";

	return p;
	}

xml_error_t xml_element_t::get_text( int* ival ) const {
	return (xml_error_t)XML_CELEM_CAST( base_ptr() )->QueryIntText( ival );
	}

xml_error_t xml_element_t::get_text( unsigned int* uval ) const {
	return (xml_error_t)XML_CELEM_CAST( base_ptr() )->QueryUnsignedText( uval );
	}

xml_error_t xml_element_t::get_text( bool* bval ) const {
	return (xml_error_t)XML_CELEM_CAST( base_ptr() )->QueryBoolText( bval );
	}

xml_error_t xml_element_t::get_text( double* dval ) const {
	return (xml_error_t)XML_CELEM_CAST( base_ptr() )->QueryDoubleText( dval );
	}

xml_error_t xml_element_t::get_text( float* fval ) const {
	return (xml_error_t)XML_CELEM_CAST( base_ptr() )->QueryFloatText( fval );
	}

void xml_element_t::set_text( const char *inText ) {
	XML_ELEM_CAST( base_ptr() )->SetText( inText );
	TEST_ERROR;
}

void xml_element_t::set_text( const std::string& inText ) {
	XML_ELEM_CAST( base_ptr() )->SetText( /*inText.empty() ? 0 :*/ inText.c_str() );
	TEST_ERROR;
	}

void xml_element_t::set_text( int value ) {
	XML_ELEM_CAST( base_ptr() )->SetText( value );
	TEST_ERROR;
	}

void xml_element_t::set_text( unsigned value ) {
	XML_ELEM_CAST( base_ptr() )->SetText( value );
	TEST_ERROR;
	}

void xml_element_t::set_text( bool value ) {
	XML_ELEM_CAST( base_ptr() )->SetText( value );
	TEST_ERROR;
	}

void xml_element_t::set_text( double value ) {
	XML_ELEM_CAST( base_ptr() )->SetText( value );
	TEST_ERROR;
	}

void xml_element_t::set_text( float value ) {
	XML_ELEM_CAST( base_ptr() )->SetText( value );
	TEST_ERROR;
	}


#undef XML_ELEM_CAST
#undef XML_CELEM_CAST

