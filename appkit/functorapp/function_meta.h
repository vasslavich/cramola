#pragma once

#include <functional>

namespace crm {


struct function_descriptor {
	std::string name;
	};

bool operator<(const function_descriptor& l, const function_descriptor& r)noexcept;
bool operator==(const function_descriptor& l, const function_descriptor& r)noexcept;


namespace detail {

template <typename>
struct function_meta;

template <typename TRes, typename ... TArgs>
struct function_meta<std::function<TRes(TArgs ...)>> {
	using result_type = std::decay_t<TRes>;
	using args_tuple = std::tuple<std::decay_t<TArgs> ... >;
	};

template<typename TFunction>
struct function_meta2 {
	using function_signature = std::decay_t<decltype(std::function{ std::declval<TFunction>() })>;
	using result_type = typename function_meta<function_signature>::result_type;
	using params_tuple = typename function_meta<function_signature>::args_tuple;
	};

template<typename H>
struct function_params_trait {
	using result_handler_params_as_tuple = typename function_meta2<H>::params_tuple;
	static constexpr size_t result_handler_params_countof = std::tuple_size_v<result_handler_params_as_tuple>;

	template<size_t I>
	using argument_type = typename std::decay_t<decltype(std::get<I>(std::declval<result_handler_params_as_tuple>()))>;
	};


template <const std::size_t Index>
using size_tuple_ = std::integral_constant<std::size_t, Index >;

template <class T, std::size_t N>
constexpr auto detect_tuple_nth(const T &, size_tuple_<N>, size_tuple_<N>) noexcept {
	return std::tuple<>{};
	}

template <class T, std::size_t L, std::size_t U>
constexpr auto detect_tuple_nth(const T & t, size_tuple_<L>, size_tuple_<U>) noexcept {
	return std::tuple_cat(std::make_tuple(std::get<L>(t)),
		detect_tuple_nth(t, size_tuple_<L + 1>{}, size_tuple_<U>{}));
	}

template <class T, std::size_t N>
constexpr auto detect_tuple_nth_dispatch(const T &t) noexcept {
	return detect_tuple_nth(t, size_tuple_<0>{}, size_tuple_<N>{});
	}


template<typename Tuple, const size_t NArgsTuple>
struct tuple_from_first_nth {
	using type = std::decay_t<decltype(detect_tuple_nth_dispatch<Tuple, NArgsTuple>(std::declval<Tuple>()))>;
	};
}
}


template<>
struct ::std::hash<crm::function_descriptor> {
	size_t operator()(crm::function_descriptor const& v) const noexcept {
		return std::hash<decltype(v.name)>{}(v.name);
		}
	};


