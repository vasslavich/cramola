#pragma once


#include <type_traits>
#include "../typeframe/type_trait_base.h"


namespace crm::utility {

template<typename TR, typename ... Args>
struct invoke_functor;
}


namespace crm::detail {


template<typename T>
constexpr bool is_functional_check_empty_v = is_bool_cast_v<T>;

template<typename T>
bool is_functional_empty(T & t)noexcept {
	if constexpr(is_functional_check_empty_v<decltype(t)>) {
		if constexpr(std::is_convertible_v < decltype(std::declval<std::decay_t<T>>()), bool >) {
			return t;
			}
		else {
			return nullptr != t;
			}
		}
	else {
		return true;
		}
	}
}


namespace crm {

template<typename TCancelTkn, typename = std::void_t<>>
struct is_cancel_token : std::false_type {};

template<typename TCancelTkn>
bool constexpr is_cancel_token_v = is_cancel_token<TCancelTkn>::value;


template<typename TCancelTkn>
struct is_cancel_token<TCancelTkn,
	std::void_t<
	std::enable_if_t<std::is_invocable_r_v<bool, decltype(std::declval<TCancelTkn>())>>
	>> : std::true_type{};

}

