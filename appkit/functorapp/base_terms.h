#pragma once


#include <string>
#include "../notifications/base_terms.h"
#include "./functor_traits.h"


namespace crm{


struct make_fault_result_t{
	make_fault_result_t()noexcept;

	std::unique_ptr<dcf_exception_t> operator()( std::string && msg )noexcept;
	std::unique_ptr<dcf_exception_t> operator()()noexcept;
	};
}

