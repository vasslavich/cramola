#pragma once


#include <atomic>
#include <cstdint>
#include <tuple>
#include <type_traits>
#include <functional>
#include "../internal_invariants.h"
#include "../notifications/internal_terms.h"
#include "./base_terms.h"
#include "../counters/internal_terms.h"
#include "../logging/trace_terms.h"


namespace crm {
namespace utility {


template<std::size_t, typename T>
struct arg {
	using type = typename std::decay_t<T>;

	template<typename X>
	arg(X && val)
		: value(std::forward<X>(val)) {}

private:
	type value;
	std::atomic<bool> _isMoved = false;

public:
	type take_value() {
		bool isMoved = false;
		if(_isMoved.compare_exchange_strong(isMoved, true)) {
			return std::move(value);
			}
		else {
			FATAL_ERROR_FWD(nullptr);
			}
		}
	};

template<typename, typename...>
struct args_list_impl;

template<std::size_t... I, typename... Args>
struct args_list_impl<std::index_sequence<I...>, Args...> : arg<I, Args>...
	{
	template<typename... OtherArgs>
	args_list_impl(OtherArgs &&... args) : arg<I, Args>(std::forward<OtherArgs>(args))... {}
	};

template<typename... Args>
struct args_list : args_list_impl<std::index_sequence_for<Args...>, Args... > {
	using base_t = args_list_impl<std::index_sequence_for<Args...>, Args... >;

	template<typename... OtherArgs>
	args_list(OtherArgs &&... args) : base_t(std::forward<OtherArgs>(args)...) {}
	};

template<std::size_t I, typename Head, typename... Tail>
struct type_at_index {
	using type = typename type_at_index<I - 1, Tail...>::type;
	};

template<typename Head, typename... Tail>
struct type_at_index<0, Head, Tail...> {
	using type = Head;
	};

template<std::size_t I, typename... Args>
using type_at_index_t = typename type_at_index<I, Args...>::type;

template<std::size_t I, typename... Args>
auto get_arg(args_list<Args...> && args)
->typename decltype(std::declval<arg< I, type_at_index_t<I, Args...>>>().take_value()){

	arg< I, type_at_index_t<I, Args...> > & argument = args;
	return argument.take_value();
	};

template<typename T, typename S>
decltype(auto) take_argument(T && arg, S const & /*args*/) {
	return std::forward<T>(arg);
	}

template<typename T, typename... Args,
	std::size_t I = std::is_placeholder<std::decay_t<T>>::value,
	typename = typename std::enable_if< I != 0 >::type >
	decltype(auto) take_argument(T && /*ph*/, args_list<Args...> && args) {
	return get_arg< I - 1, Args... >(std::forward<args_list<Args...> >(args));
	}


template<typename _TFunc, 
	typename... _TBinderArgs>
struct binder {
	using binder_arglist_t = typename args_list<_TBinderArgs...>;
	using functor_t = typename std::decay_t<_TFunc>;
	using self_t = typename binder<_TFunc, _TBinderArgs...>;


	binder()noexcept {}

	template<typename _XF, typename... _XAL>
	binder(trace_tag&& tt_,
		_XF&& func,
		_XAL&&... binderArgs)
		: d(std::make_unique<_data_t>(std::move(tt_), std::forward<_XF>(func), std::forward<_XAL>(binderArgs)...)) {}

	template<typename... Args>
	decltype(auto) operator()(Args&&... args)const {
		return call_function(std::index_sequence_for<_TBinderArgs...>{}, std::forward<Args>(args)...);
		}

	self_t& operator=(const self_t&) = delete;
	binder(const self_t&) = delete;

	self_t& operator=(self_t&& o)noexcept {
		if (this != std::addressof(o)) {
			d = std::move(o.d);
			}

		return (*this);
		}

	binder(self_t&& o) noexcept
		: d(std::move(o.d)) {}

	operator bool()const noexcept {
		return !d->_invokedf;
		}

	bool is_functor()const noexcept {
		return true;
		}

	private:
		template< std::size_t... I, typename... Args >
		decltype(auto) call_function(std::index_sequence<I...> const&, Args&&... args) const{
			bool inkf = false;
			if (d->_invokedf.compare_exchange_strong(inkf, true)) {
				struct empty_list {
					empty_list(Args&&... /*args*/) {}
					};

				using args_t = std::conditional_t< sizeof...(Args) == 0, empty_list, args_list<Args...> >;
				args_t argsList(std::forward<Args>(args)...);

				return d->m_invoker.invoke(std::move(d->m_func),
					take_argument(get_arg<I, _TBinderArgs...>(std::move(d->m_args)), std::move(argsList))...);
				}
			else {
#ifdef CBL_TRACELEVEL_TRACE_TAG_ON
				FATAL_ERROR_FWD(d->_tag.name + ":" + typeid(self_t).name());
#else
				FATAL_ERROR_FWD(typeid(self_t).name());
#endif
				}
			}

		struct free_function_invoker {
			template<typename XFunc, typename...Args>
			decltype(auto) invoke(XFunc&& func, Args&&... args)
				noexcept(noexcept(func(std::forward<Args>(args)...))) {

				return func(std::forward<Args>(args)...);
				}
			};

		struct member_function_invoker {
			template<typename ObjType, typename...Args>
			decltype(auto) invoke(_TFunc&& func, ObjType&& obj, Args&&... args)
				noexcept(noexcept((obj.*func)(std::forward<Args>(args)...))) {

				return (obj.*func)(std::forward<Args>(args)...);
				}
			};

		using invoker_t =
			std::conditional_t< std::is_member_function_pointer<functor_t>::value, member_function_invoker, free_function_invoker >;

		struct _data_t {
			invoker_t m_invoker;
			functor_t m_func;
			binder_arglist_t m_args;
			std::atomic<bool> _invokedf = false;

#if defined( CBL_OBJECTS_COUNTER_CHECKER )
			crm::utility::__xobject_counter_logger_t<self_t> _xDbgCounter;
#endif

#if defined(CBL_TRACELEVEL_TRACE_TAG_ON)
			trace_tag _tag;
#endif

			template<typename _XF, typename ... _XAList>
			_data_t(
#if defined(CBL_TRACELEVEL_TRACE_TAG_ON)
				trace_tag&& tag_,
#else
				trace_tag&&,
#endif

				_XF&& func, _XAList&&... binderArgs)
				: m_func(std::forward<_XF>(func))
				, m_args(std::forward<_XAList>(binderArgs)...)

#if defined( CBL_OBJECTS_COUNTER_CHECKER ) && defined(CBL_TRACELEVEL_TRACE_TAG_ON)
				, _xDbgCounter(tag_.name)
#endif
#if defined(CBL_TRACELEVEL_TRACE_TAG_ON)
				, _tag(std::move(tag_))
#endif
				{
				static_assert(sizeof...(_XAList) == sizeof...(_TBinderArgs), __FILE_LINE__);
				}
			};

		std::unique_ptr<_data_t> d;
	};



template<typename _TFunc, 
	typename... _TBinderArgs>
struct binder_no_throw_invokable {
	using binder_no_throw_invokable_arglist_t = typename args_list<_TBinderArgs...>;
	using functor_t = typename std::decay_t<_TFunc>;
	using self_t = typename binder_no_throw_invokable<_TFunc, _TBinderArgs...>;

	binder_no_throw_invokable()noexcept {}

	template<typename _XF, typename... _XAL>
	binder_no_throw_invokable(
		trace_tag&& tag_,		
		_XF && func, 
		_XAL &&... binder_no_throw_invokableArgs)
		: d(std::make_unique<_data_t>(std::move(tag_), std::forward<_XF>(func), std::forward<_XAL>(binder_no_throw_invokableArgs)...)){}

	template<typename... Args>
	decltype(auto) operator()(Args &&... args)const {
		return call_function(std::index_sequence_for<_TBinderArgs...>{}, std::forward<Args>(args)...);
		}

	self_t& operator=(const self_t &) = delete;
	binder_no_throw_invokable(const self_t &) = delete;

	self_t& operator=(self_t && o)noexcept {
		if(this != std::addressof(o)) {
			d = std::move(o.d);
			}

		return (*this);
		}

	binder_no_throw_invokable(self_t && o) noexcept
		: d(std::move(o.d)){}

	operator bool()const noexcept {
		return !d->_invokedf;
		}

	bool is_functor()const noexcept {
		return true;
		}

private:
	template< std::size_t... I, typename... Args >
	decltype(auto) call_function(std::index_sequence<I...> const &, Args &&... args)const {

		bool inkf = false;
		if(d->_invokedf.compare_exchange_strong(inkf, true)) {
			struct empty_list {
				empty_list(Args &&... /*args*/) {}
				};

			using args_t = std::conditional_t< sizeof...(Args) == 0, empty_list, args_list<Args...> >;
			args_t argsList(std::forward<Args>(args)...);

			return d->m_invoker.invoke(std::move(d->m_func),
				take_argument(get_arg<I, _TBinderArgs...>(std::move(d->m_args)), std::move(argsList))...);
			}
		else {
#ifdef CBL_TRACELEVEL_TRACE_TAG_ON
			FATAL_ERROR_FWD(d->_tag.name + ":" + typeid(self_t).name());
#else
			FATAL_ERROR_FWD(typeid(self_t).name());
#endif
			}
		}

	struct free_function_invoker {
		template<typename XFunc, typename...Args>
		decltype(auto) invoke(XFunc && func, Args &&... args)
			noexcept(noexcept(func(std::forward<Args>(args)...))){
			return func(std::forward<Args>(args)...);
			}
		};

	struct member_function_invoker {
		template<typename ObjType, typename...Args>
		decltype(auto) invoke(_TFunc && func, ObjType && obj, Args &&... args)
			noexcept(noexcept((obj.*func)(std::forward<Args>(args)...)))
			{
			return (obj.*func)(std::forward<Args>(args)...);
			}
		};

	using invoker_t =
		std::conditional_t< std::is_member_function_pointer<functor_t>::value, member_function_invoker, free_function_invoker >;

	struct _data_t {
		invoker_t m_invoker;
		functor_t m_func;
		binder_no_throw_invokable_arglist_t m_args;
		std::atomic<bool> _invokedf = false;

#ifdef CBL_OBJECTS_COUNTER_CHECKER
		crm::utility::__xobject_counter_logger_t<self_t> _xDbgCounter;
#endif
#ifdef CBL_TRACELEVEL_TRACE_TAG_ON
		trace_tag _tag;
#endif

		template<typename _XF, typename ... _XAList>
		_data_t(
#if defined(CBL_OBJECTS_COUNTER_CHECKER)
			trace_tag&& tag_,
#else
			trace_tag&&,
#endif

			_XF && func,
			_XAList &&... binder_no_throw_invokableArgs)
			: m_func(std::forward<_XF>(func))
			, m_args(std::forward<_XAList>(binder_no_throw_invokableArgs)...)
			
#if defined( CBL_OBJECTS_COUNTER_CHECKER )
			, _xDbgCounter(tag_.name)
#endif
#if defined(CBL_TRACELEVEL_TRACE_TAG_ON) && defined(CBL_TRACELEVEL_TRACE_TAG_ON)
			, _tag(std::move(tag_))
#endif
			{
			static_assert(sizeof...(_XAList) == sizeof...(_TBinderArgs), __FILE_LINE__);
			}
		};

	std::unique_ptr<_data_t> d;
	};


	template<typename Func,
		typename... BinderArgs,
		typename std::enable_if_t < !std::is_constructible_v<trace_tag, Func>, int > = 0 >
	decltype(auto) bind_once_call(Func && func, BinderArgs && ... args) {
		return bind_once_call({ typeid(func).name() }, std::forward<Func>(func), std::forward<BinderArgs>(args)...);
	}

template<typename Func, 
	typename... BinderArgs>
decltype(auto) bind_once_call(trace_tag&& tag_, Func && func, BinderArgs &&... args) {
	return binder<Func, BinderArgs...>(std::move(tag_), std::forward<Func>(func), std::forward<BinderArgs>(args)...);
	}

template<typename Func, 
	typename... BinderArgs,
	typename std::enable_if_t<!std::is_constructible_v<trace_tag, Func>, int> = 0>
decltype(auto) bind_once_call_no_throw(Func && func, BinderArgs &&... args) {
	return bind_once_call_no_throw({ typeid(func).name() }, std::forward<Func>(func), std::forward<BinderArgs>(args)...);
	}

template<typename Func, 
	typename... BinderArgs>
decltype(auto) bind_once_call_no_throw(trace_tag&& tag_, Func && func, BinderArgs &&... args) {
	return binder_no_throw_invokable<Func, BinderArgs...>(std::move(tag_), std::forward<Func>(func), std::forward<BinderArgs>(args)...);
	}
}
}
