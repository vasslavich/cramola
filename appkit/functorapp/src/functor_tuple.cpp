#include "../../internal.h"
#include "../functor_tuple.h"


using namespace crm;
using namespace crm::detail;


functor_tuple::functor_tuple()noexcept {}

functor_tuple::functor_tuple(std::vector<uint8_t>&& hld)
	: _holder(std::move(hld)) {}

const std::vector<uint8_t>& functor_tuple::data()const noexcept {
	return _holder;
	}
