#include "../../internal.h"
#include "../function_meta.h"


using namespace crm;
using namespace crm::detail;

static_assert(srlz::detail::is_strob_serializable_v< function_descriptor>, __FILE_LINE__);
static_assert(srlz::detail::is_srlzd_prefixed_v<std::wstring>, __FILE_LINE__);
static_assert(srlz::detail::is_srlzd_prefixed_v<function_descriptor>, __FILE_LINE__);
static_assert(srlz::detail::is_strob_prefixed_serialized_size_v<function_descriptor>, __FILE_LINE__);
static_assert(srlz::detail::is_strob_flat_serializable_v<function_descriptor>, __FILE_LINE__);
static_assert(std::is_same_v<size_t, decltype(srlz::object_trait_serialized_size(std::declval<function_descriptor>()))>, __FILE_LINE__);

bool crm::operator<(const function_descriptor& l, const function_descriptor& r)noexcept {
	return l.name < r.name;
	}

bool crm::operator==(const function_descriptor& l, const function_descriptor& r)noexcept {
	return l.name == r.name;
	}


