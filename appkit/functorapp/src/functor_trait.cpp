#include "../../internal.h"
#include "../functor_trait.h"


using namespace crm;
using namespace crm::detail;


functor_trait::functor_trait()noexcept {}

functor_trait::functor_trait(function_descriptor&& fd, std::vector<uint8_t>&& hld)
	: base_type(std::move(hld))
	, _fd(std::move(fd)){}

const function_descriptor& functor_trait::descriptor()const noexcept {
	return _fd;
	}

size_t functor_trait::get_serialized_size()const noexcept {
	return srlz::object_trait_serialized_size(_fd) +
		base_type::get_serialized_size();
	}

