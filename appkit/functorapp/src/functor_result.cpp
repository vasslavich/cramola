#include "../../internal.h"
#include "../functor_result.h"

using namespace crm;
using namespace crm::detail;

functor_result::functor_result()noexcept {}

functor_result::functor_result(std::vector<uint8_t>&& hld)
	: base_type(std::move(hld)){}

bool functor_result::is_void()const noexcept {
	return data().empty();
	}


