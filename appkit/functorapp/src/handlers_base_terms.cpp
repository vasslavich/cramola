#include "../../internal.h"
#include "../base_terms.h"
#include "../../exceptions.h"


using namespace crm;
using namespace crm::detail;


make_fault_result_t::make_fault_result_t()noexcept{}

std::unique_ptr<dcf_exception_t> make_fault_result_t::operator()( std::string && msg )noexcept{
	return CREATE_ABANDONED_PTR_EXC_FWD( std::move( msg ) );
	}

std::unique_ptr<dcf_exception_t> make_fault_result_t::operator()()noexcept{
	return CREATE_ABANDONED_PTR_EXC_FWD(nullptr);
	}
