#pragma once


#include "function_meta.h"
#include "functor_tuple.h"


namespace crm::detail {


class functor_trait : public functor_tuple {
	function_descriptor _fd;

public:
	using base_type = functor_tuple;

	functor_trait()noexcept;
	functor_trait(function_descriptor&& fd, std::vector<uint8_t>&& hld);

	const function_descriptor& descriptor()const noexcept;

	template<typename THandler>
	decltype(auto) apply(THandler&& h)const {
		using meta_type = function_meta2<THandler>;
		using tuple_type = meta_type::params_tuple;

		tuple_type tpl{};

		unpack(tpl, std::make_index_sequence<std::tuple_size_v<tuple_type>>{});

		return std::apply(std::forward<THandler>(h), std::move(tpl));
		}

	template<typename S>
	void srlz(S&& dest)const {
		srlz::serialize(dest, _fd);
		base_type::srlz(std::forward<S>(dest));
		}

	template<typename S>
	void dsrlz(S&& src) {
		srlz::deserialize(src, _fd);
		base_type::dsrlz(std::forward<S>(src));
		}

	size_t get_serialized_size()const noexcept;
	};
}
