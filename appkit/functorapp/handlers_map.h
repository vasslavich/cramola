#pragma once


#include <unordered_map>
#include <shared_mutex>
#include <memory>
#include <type_traits>
#include "function_meta.h"
#include "functor_trait.h"
#include "functor_result.h"


namespace crm::detail {


class handlers_map {
	using handler_type = std::function<functor_result(functor_trait && ft)>;

	struct handler_entry {
		handler_type hdl;

		handler_entry(handler_type&& h)
			: hdl(std::move(h)){}
		};

	std::unordered_map<function_descriptor, std::shared_ptr<handler_entry>> _map;
	std::shared_mutex _lck;

public:
	decltype(auto) invoke(functor_trait&& ft) {
		std::shared_lock lck(_lck);

		auto hIt = _map.find(ft.descriptor());
		if (hIt != _map.end()) {
			auto phndl = hIt->second;
			lck.unlock();

			return phndl->hdl(std::move(ft));
			}
		else {
			NOT_FOUND_THROW_EXC_FWD(ft.descriptor().name);
			}
		}

	template<typename THandler>
	void add_handler(function_descriptor&& fd, THandler&& h_) {
		std::lock_guard lck(_lck);

		if (!_map.contains(fd)) {
			auto handler = [h = std::move(h_)](functor_trait&& ft) {
				return functor_result::make( ft.apply(h) );
				};
			static_assert(std::is_constructible_v<handler_type, decltype(handler)>, __FILE_LINE__);

			if (!_map.insert({ std::move(fd), std::make_shared<handler_entry>(std::move(handler)) }).second) {
				THROW_EXC_FWD(fd.name);
				}
			}
		else {
			DUPLICATE_OBJECT_THROW_EXC_FWD(fd.name);
			}
		}
	};
}

