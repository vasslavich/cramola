#pragma once

#include "function_meta.h"

namespace crm::detail {

class functor_tuple {
	std::vector<uint8_t> _holder;

protected:
	template<typename Tup, std::size_t ... I>
	void unpack(Tup& tpl, std::index_sequence<I...>)const {

		auto rs = srlz::rstream_constructor_t::make(_holder.cbegin(), _holder.cend());
		auto dsrlz_item = [&rs](auto&& i) {
			srlz::deserialize(rs, i);
			};

		(dsrlz_item(get<I>(tpl)), ...);
		}

	template<typename Tup, std::size_t ... I>
	void pack(const Tup& tpl, std::index_sequence<I...>) {
		auto ws = srlz::wstream_constructor_t::make();
		auto srlz_item = [&ws](auto&& i) {
			srlz::serialize(ws, i);
			};

		(srlz_item(get<I>(tpl)), ...);

		_holder = ws.release();
		}

	const std::vector<uint8_t>& data()const noexcept;

public:
	functor_tuple()noexcept;
	functor_tuple(std::vector<uint8_t>&& hld);

	functor_tuple(functor_tuple&&)noexcept = default;
	functor_tuple& operator=(functor_tuple&&)noexcept = default;

	functor_tuple(const functor_tuple&) = delete;
	functor_tuple& operator=(const functor_tuple&) = delete;

	template<typename S>
	void srlz(S&& dest)const {
		crm::srlz::serialize(dest, _holder);
		}

	template<typename S>
	void dsrlz(S&& src) {
		crm::srlz::deserialize(src, _holder);
		}

	size_t get_serialized_size()const noexcept {
		return crm::srlz::object_trait_serialized_size(_holder);
		}

protected:
	template<typename ... AL_>
	functor_tuple(std::tuple<AL_...>&& t) {
		static_assert(!(... || (std::is_base_of_v<functor_tuple, std::decay_t<AL_>>)), __FILE_LINE__);
		pack(t, std::index_sequence_for<AL_...>{});
		}

	template<typename ... AL_>
	functor_tuple(AL_&& ... al) {
		static_assert(!(... || (std::is_base_of_v<functor_tuple, std::decay_t<AL_>>)), __FILE_LINE__);
		static_assert((sizeof...(al) > 1 || sizeof...(al) == 0) || !srlz::detail::exists_type_v<functor_tuple, AL_...>, __FILE_LINE__);
		static_assert((sizeof...(al) > 1 || sizeof...(al) == 0) || !srlz::detail::exists_type_v<std::vector<uint8_t>, AL_...>, __FILE_LINE__);
		static_assert(srlz::detail::is_all_srlzd_v<AL_...>, __FILE_LINE__);

		const auto& tpl = srlz::detail::make_tuple_of_references(srlz::detail::tuple_of_references_t<srlz::detail::__tuple_of_references_t::is_const_lvalue>(),
			std::forward<AL_>(al)...);

		pack(tpl, std::index_sequence_for<AL_...>{});
		}

public:
	template<typename ... AL>
	decltype(auto) load_to_tuple()const {
		using tuple_type = std::tuple<std::decay_t<AL>...>;
		tuple_type tpl{};

		unpack(tpl, std::index_sequence_for<AL...>{});

		return tpl;
		}

	template<typename ... AL>
	void load_to_tuple(std::tuple<AL...>& t) {
		unpack(t, std::index_sequence_for<AL...>{});
		}

	template<typename ... AL_>
	typename std::enable_if_t < (
		!(... || (std::is_base_of_v<functor_tuple, std::decay_t<AL_>>))
		), functor_tuple>
	static make(AL_&& ... al){
		return functor_tuple{ std::forward<AL_>(al)... };
		}

	template<typename ... AL_>
	typename std::enable_if_t < (
		!(... || (std::is_base_of_v<functor_tuple, std::decay_t<AL_>>))
		), functor_tuple>
	static make(std::tuple<AL_...>&& t) {
		return functor_tuple{ std::move(t) };
		}
	};
}

