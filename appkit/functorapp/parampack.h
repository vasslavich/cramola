#pragma once


#include <type_traits>


namespace crm::detail {


template <class... Args>
class parameter_tuple;

template <>
class parameter_tuple<> {};

template <class T, class... Args>
class parameter_tuple<T, Args...> : public parameter_tuple<Args...> {
	using base_type = parameter_tuple<Args...>;
	using self_type = parameter_tuple<T, Args...>;

	T value_{};

public:
	parameter_tuple()noexcept {}

	parameter_tuple(self_type&&)noexcept = default;
	self_type& operator=(self_type&&)noexcept = default;

	parameter_tuple(const self_type&)noexcept = default;
	self_type& operator=(const self_type&)noexcept = default;

	parameter_tuple(T&& value, Args&&... args)
		: base_type(std::forward<Args>(args)...)
		, value_(std::forward<T>(value)){}

	T& value()& noexcept {
		return value_;
		}

	const T& value() const & noexcept {
		return value_;
		}

	T value() && noexcept {
		return std::move(value_);
		}
	};


template<typename ... AL>
decltype(auto) make_parameter_tuple(AL&& ... al) {
	return parameter_tuple<AL...>(std::forward<AL>(al)...);
	}

template <size_t k, class T, class... Args>
struct select {
	using type = typename select<k - 1, Args...>::type;
	};

template <class T, class... Args>
struct select<0, T, Args...> {
	using type = T;
	};

template <size_t k, class... Args>
using select_t = typename select<k, Args...>::type;

template <size_t k, class T, class... Args>
std::enable_if_t<(k != 0), select_t<k, T, Args...>&> get_value(parameter_tuple<T, Args...>& t) {
	return get_value<k - 1, Args...>(t);
	}

template <size_t k, class T, class... Args>
std::enable_if_t<(k == 0), select_t<k, T, Args...>&> get_value(parameter_tuple<T, Args...>& t) {
	return t.value();
	}



template <typename>
struct parameter_tuple_size; // undefined base template

template <typename... Types>
struct parameter_tuple_size<parameter_tuple<Types...>> : std::integral_constant<size_t, sizeof...(Types)> {};


template<typename ... Types>
constexpr size_t  parameter_tuple_size_v = parameter_tuple_size<Types...>::value;


}

