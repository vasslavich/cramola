#include "../../internal.h"


using namespace crm;
using namespace crm::detail;


namespace {


static_assert(is_functor_v<std::function<void()>>, __FILE_LINE__);
static_assert(is_check_nullptr_v<std::function<void()>>, __FILE_LINE__);
static_assert(is_bool_cast_v<std::function<void()>>, __FILE_LINE__);


template<typename T>
constexpr decltype(auto) detect_lambada(T&&)noexcept {
	if constexpr (is_lambda_v<T>)
		return std::true_type{};
	else
		return std::false_type{};
	}

template<typename F>
constexpr bool is_llld(F&& f)noexcept {
	return std::decay_t<decltype(detect_lambada(std::forward<F>(f)))>::value;
	}

static_assert(is_llld([] {}), __FILE_LINE__);


std::function<void()> boolcastFunc = [] {};
static_assert(is_bool_cast_v <decltype(boolcastFunc)>, __FILE__);
static_assert(is_bool_cast_v <std::add_lvalue_reference_t<decltype(boolcastFunc)>>, __FILE__);
static_assert(is_bool_cast_v <std::add_rvalue_reference_t<decltype(boolcastFunc)>>, __FILE__);


static_assert(is_bool_cast_v < int>, __FILE__);
static_assert(is_bool_cast_v < int&>, __FILE__);
static_assert(is_bool_cast_v < int&&>, __FILE__);


struct StructNotBool {} structNotBool;
static_assert(!is_bool_cast_v <decltype(structNotBool)>, __FILE__);
static_assert(!is_bool_cast_v < std::add_lvalue_reference_t<decltype(structNotBool)>>, __FILE__);
static_assert(!is_bool_cast_v < std::add_lvalue_reference_t<decltype(structNotBool)>>, __FILE__);


struct fakedata1 {};
struct fakedata2{};

struct fakefunctor {
	void operator()(int, double d, fakedata1, fakedata2, std::unique_ptr<dcf_exception_t>&&, std::chrono::microseconds);
	};


using t0 = std::tuple<int, double, fakedata1, fakedata2>;
using t1 = std::tuple<int, double, fakedata1, fakedata2, std::unique_ptr<dcf_exception_t>, std::chrono::microseconds>;
using t1_2 = typename tuple_from_first_nth<t1, 4>::type;
using tfunct_args = typename function_params_trait<fakefunctor>::result_handler_params_as_tuple;

static_assert(std::is_same_v<t0, t1_2>, __FILE_LINE__);
static_assert(std::is_same_v<t1, tfunct_args>, __FILE_LINE__);
static_assert(std::is_same_v<t0, typename tuple_from_first_nth<tfunct_args, function_params_trait<fakefunctor>::result_handler_params_countof - 2>::type>, __FILE_LINE__);
}
