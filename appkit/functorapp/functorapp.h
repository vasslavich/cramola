#pragma once

#include "./base_terms.h"
#include "./bind.h"
#include "./invoker_ptr.h"

namespace crm {


class cancel_token_functor {
private:
	using functor_wrapper_t = utility::invoke_functor<bool>;

	functor_wrapper_t _f;

public:
	template<typename TCancelTkn,
		typename std::enable_if_t<!std::is_same_v<cancel_token_functor, std::decay_t<TCancelTkn>> && std::is_invocable_r_v<bool, TCancelTkn>, int> = 0>
	cancel_token_functor(TCancelTkn && tk)
		: _f(std::forward<TCancelTkn>(tk)) {}

	cancel_token_functor(const cancel_token_functor&) = delete;
	cancel_token_functor& operator=(const cancel_token_functor &) = delete;

	cancel_token_functor(cancel_token_functor &&) = default;
	cancel_token_functor& operator=(cancel_token_functor &&) = default;

	bool operator()()const noexcept {
		return _f();
		}
	};
}

