#pragma once

#include "function_meta.h"
#include "functor_tuple.h"


namespace crm::detail {


class functor_result final : public functor_tuple {
public:
	using base_type = functor_tuple;

	functor_result()noexcept;
	functor_result(std::vector<uint8_t>&& hld);

	functor_result(functor_result&&)noexcept = default;
	functor_result& operator=(functor_result&&)noexcept = default;

	functor_result(const functor_result&) = delete;
	functor_result& operator=(const functor_result&) = delete;

private:
	template<typename ... AL_>
	functor_result(AL_&& ... al)
		: functor_tuple(std::forward<AL_>(al)...){
		static_assert(!(... || (std::is_base_of_v<functor_tuple, std::decay_t<AL_>>)), __FILE_LINE__);
		}

	template<typename ... AL_>
	functor_result(std::tuple<AL_...> && t)
		: functor_tuple(std::move(t)) {
		static_assert(!(... || (std::is_base_of_v<functor_tuple, std::decay_t<AL_>>)), __FILE_LINE__);
		}

public:
	bool is_void()const noexcept;

	template<typename T,
		typename std::enable_if_t<std::is_same_v<void, T>, int> = 0>
	decltype(auto) as()const {
		if (is_void()) {
			return;
			}
		else {
			THROW_EXC_FWD(nullptr);
			}
		}

	//template<typename T,
	//	typename std::enable_if_t<!std::is_same_v<void, T>, int> = 0>
	//decltype(auto) as()const {
	//	if (!is_void()) {
	//		auto [r] = load_to_tuple<T>();
	//		return r;
	//		}
	//	else {
	//		THROW_EXC_FWD(nullptr);
	//		}
	//	}

	template<typename ... T>
	decltype(auto) as()const/* -> typename std::enable_if_t<((sizeof ... (T)) > 1), decltype(load_to_tuple<T...>())> */{
		return load_to_tuple<T...>();
		}

	template<typename S>
	void srlz(S&& dest)const {
		base_type::srlz(std::forward<S>(dest));
		}

	template<typename S>
	void dsrlz(S&& src) {
		base_type::dsrlz(std::forward<S>(src));
		}

	size_t get_serialized_size()const noexcept {
		return base_type::get_serialized_size();
		}

	template<typename ... AL_>
	typename std::enable_if_t<(
		!(... || (std::is_base_of_v<functor_tuple, std::decay_t<AL_>>))
		), functor_result>
	static	make(AL_&& ... al) {
		return functor_result( std::forward<AL_>(al)... );
		}


	template<typename ... AL_>
	typename std::enable_if_t<(
		!(... || (std::is_base_of_v<functor_tuple, std::decay_t<AL_>>))
		), functor_result>
	static	make(std::tuple<AL_...>&& t) {
		return functor_tuple(std::move(t));
		}
	};
}
