#pragma once

#include "../internal_invariants.h"
#include "./base_terms.h"


namespace crm{
namespace utility{


template<typename TR, typename ...Args>
struct invoker_ptr_base{
	using return_type = typename TR;
	
	virtual ~invoker_ptr_base(){}

	virtual return_type invoke( Args ... ) = 0;
	virtual return_type operator()( Args ... ) = 0;

	virtual bool empty()const noexcept = 0;
	virtual operator bool()const noexcept = 0;
	};

template<typename TFunc,
	typename ... Args>
struct invoker_ptr final : invoker_ptr_base < std::invoke_result_t<TFunc, Args...>, Args...>{
	using base_type = typename invoker_ptr_base < std::invoke_result_t<TFunc, Args...>, Args...>;
	using func_type = typename std::decay_t<TFunc>;
	using self_type = typename invoker_ptr<TFunc, Args...>;
	using return_type = typename base_type::return_type;

	func_type _f;

	return_type invoke( Args ... args ) final{
		return _f( std::forward<Args>( args )... );
		}

	return_type operator()( Args ... args ) final{
		return invoke( std::forward<Args>( args )... );
		}

	invoker_ptr( const self_type & ) = delete;
	self_type & operator=( const self_type & ) = delete;

	invoker_ptr( self_type && o)noexcept
		: _f(std::move(o._f)){}

	self_type& operator=( self_type && o )noexcept{
		if( this != std::addressof( o ) ){
			_f = std::move( o._f );
			}

		return (*this);
		}

	template<typename XFunc,
		typename std::enable_if_t<!std::is_base_of_v<self_type, std::decay_t<XFunc>>, int> = 0>
	invoker_ptr( XFunc && xf )
		: _f(std::forward<XFunc>(xf)){}

	bool empty()const noexcept final {
		if constexpr(is_bool_cast_v<decltype(_f)>) {
			return !_f;
			}
		else {
			return false;
			}
		}

	operator bool()const noexcept final {
		return !empty();
		}
	};


template<typename TR, typename ... Args>
struct invoke_functor_shared;


template<typename TR, typename ... Args>
struct invoke_functor : invoker_ptr_base<TR, Args...> {
	friend struct invoke_functor_shared<TR, Args...>;

	using base_type = invoker_ptr_base<TR, Args...>;
	using self_type = invoke_functor<TR, Args...>;
	using return_type = typename base_type::return_type;

	std::unique_ptr<base_type> _pf;

	invoke_functor()noexcept{}

	template<typename THandler,
		typename TInvokePtr = typename invoker_ptr<THandler, Args...>,
		typename std::enable_if_t<(
			!std::is_same_v<self_type, typename std::decay_t<THandler>> &&
			std::is_invocable_r_v<return_type, THandler, Args... > &&
			std::is_same_v<return_type, std::invoke_result_t<THandler, Args...>> &&
			std::is_base_of_v<base_type, TInvokePtr>
			),
		int> = 0>
	invoke_functor( THandler && h )
		: _pf(std::make_unique<TInvokePtr>(std::forward<THandler>(h))){}

	return_type invoke__( Args ... args )const{
		if( !empty() ){
			return _pf->invoke( std::forward<Args>( args )... );
			}
		else{
			THROW_EXC_FWD(nullptr);
			}
		}

	return_type invoke(Args ... args) {
		return invoke__(std::forward<Args>(args)...);
		}

	return_type invoke(Args ... args)const {
		return invoke__(std::forward<Args>(args)...);
		}

	return_type operator()(Args ... args) {
		return invoke__(std::forward<Args>(args)...);
		}

	return_type operator()( Args ... args )const{
		return invoke__( std::forward<Args>( args )... );
		}

	void reset()noexcept{
		_pf = nullptr;
		}

	bool empty()const noexcept {
		return !_pf || _pf->empty();
		}

	operator bool()const noexcept{
		return !empty();
		}
	};



template<typename TR, typename ... Args>
struct invoke_functor_shared : invoker_ptr_base<TR, Args...> {
	using base_type = invoker_ptr_base<TR, Args...>;
	using self_type = invoke_functor_shared<TR, Args...>;
	using return_type = typename base_type::return_type;

	std::shared_ptr<invoker_ptr_base<TR, Args...>> _pf;

	invoke_functor_shared()noexcept{}

	invoke_functor_shared(invoke_functor<TR, Args...> && uniquef)noexcept
		: _pf(std::move(uniquef._pf)) {
		static_assert(std::is_same_v<base_type, typename invoke_functor<TR, Args...>::base_type>, __FILE__);
		static_assert(std::is_base_of_v < base_type, std::decay_t<decltype(uniquef)>>, __FILE__);
		}

	template<typename THandler,
		typename TInvokePtr = typename invoker_ptr<THandler, Args...>,
		typename std::enable_if_t<(
			!std::is_same_v<self_type, typename std::decay_t<THandler>> &&
			std::is_invocable_r_v<return_type, THandler, Args... > &&
			std::is_same_v<return_type, std::invoke_result_t<THandler, Args...>> &&
			std::is_base_of_v<base_type, TInvokePtr>
			),
		int> = 0>
	invoke_functor_shared( THandler && h )
		: _pf( std::make_unique<TInvokePtr>( std::forward<THandler>( h ) ) ){}

	return_type invoke__( Args ... args )const{
		if( _pf ){
			return _pf->invoke(std::forward<Args>( args )...);
			}
		else{
			THROW_EXC_FWD(nullptr);
			}
		}

	return_type invoke(Args ... args)const {
		return invoke__(std::forward<Args>(args)...);
		}

	return_type operator()( Args ... args )const{
		return invoke__( std::forward<Args>( args )... );
		}

	return_type invoke(Args ... args) {
		return invoke__(std::forward<Args>(args)...);
		}

	return_type operator()(Args ... args) {
		return invoke__(std::forward<Args>(args)...);
		}

	void reset()noexcept{
		_pf = nullptr;
		}

	bool empty()const noexcept {
		return !_pf || _pf->empty();
		}

	operator bool()const noexcept {
		return !empty();
		}
	};
}
}
