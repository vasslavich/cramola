#pragma once

#include <string>

namespace crm {

struct trace_tag {
	std::string name;

	trace_tag()noexcept;
	trace_tag(std::string_view name);
	trace_tag(const char*);
	trace_tag(std::string);
	};
}
