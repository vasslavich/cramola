#include "../../internal.h"
#include "../keycounter.h"


using namespace crm;
using namespace crm::detail;


/*std_hash_type keycounter_trace::hash_keycounter::operator()(const key_t& obj) const noexcept{
	return std::hash<key_t>{}(obj);
	}*/

std::tuple<size_t, size_t> keycounter_trace::coverage_stat()const noexcept {
	size_t valued{ 0 };
	for (auto ik : _keyStat.il) {
		if (ik > 0) {
			++valued;
			}
		}

	return std::make_tuple(valued, _keyStat.il.size());
	}

keycounter_trace::keycounter_trace()noexcept{}

void keycounter_trace::set( key_t key, int i )noexcept{
	CBL_VERIFY( i < keycounter_lines::size );

	std::lock_guard lck( _lck );
	if( _blackList.find( key ) == _blackList.end() ){
		++_keyset[key].il.at( i );
		++_keyStat.il.at(i);
		}
	}

void keycounter_trace::unset( key_t key )noexcept{
	std::lock_guard lck( _lck );
	_keyset.erase( key );
	_blackList.insert( key );
	}

std::vector< std::tuple<keycounter_trace::key_t, keycounter_lines>> keycounter_trace::view()const noexcept{
	std::lock_guard lck( _lck );
	std::vector< std::tuple<keycounter_trace::key_t, keycounter_lines>> r;
	for( const auto l : _keyset ){
		r.emplace_back( l.first, l.second );
		}

	return r;
	}

size_t keycounter_trace::size()const noexcept{
	std::lock_guard lck( _lck );
	return _keyset.size();
	}

#ifdef CBL_MPF_MESSAGE_TRACEWAY_KEYS_BY_ID
keycounter_trace _KeyTracer;
keycounter_trace& detail::get_keytracer()noexcept{
	return _KeyTracer;
	}
#endif

