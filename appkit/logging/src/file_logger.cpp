#include <mutex>
#include <atomic>
#include <fstream>
#include "../../internal.h"
#include "../../utilities/convert/cvt.h"
#include "../../global.h"
#include "../logger.h"


using namespace crm;


static const std::wstring LogFName = L".\\syslv.log";
static const char* format_time_to_str = "%d:%m:%Y:%H:%M:%S";
static const char* format_str_to_time = "%d:%m:%Y:%H:%M:%S";


std::mutex file_logger_lock;
std::atomic<bool> closef = false;


static std::once_flag _firstOpenLog;
static void check_first_open(std::wstring_view/*file*/)noexcept {
	//std::experimental::filesystem::remove( std::experimental::filesystem::path( file ) );

	auto stopf = [] {
		std::unique_lock<std::mutex> lck(file_logger_lock);
		closef.store(true, std::memory_order::memory_order_release);
		};

	subscribe_terminate_list(std::move(stopf));
	}

static std::atomic<bool> first_logging_f = false;

static void print2console(const std::wstring & txt)noexcept {
	std::wcout << txt << std::endl;
	}

static void print2console(const std::string & txt)noexcept {
	std::cout << txt << std::endl;
	}

std::wstring get_this_thread_id()noexcept {
	std::wostringstream log;
	log << std::this_thread::get_id();
	return log.str();
	}


template<typename Tstring>
static void write2file_sync(Tstring &&strout, std::wstring filename) {

	typedef typename std::decay_t<Tstring>::value_type string_elem_t;
	typedef typename std::basic_ofstream<string_elem_t> ofstream_t;

	bool firstCall = false;
	if(first_logging_f.compare_exchange_strong(firstCall, true))
		check_first_open(filename);

	std::lock_guard<std::mutex> lock(file_logger_lock);

	ofstream_t ofile(filename, std::ios_base::out | std::ios_base::app);
	if(!ofile.is_open()) {
		print2console(std::wstring(L"logging: failed open file=") + filename);
		}
	else {
		ofile << strout << std::endl;
		ofile.close();

		print2console(strout);
		}
	}

template<typename Tstring>
static void write2file_nosync(Tstring &&strout, std::wstring_view filename) {

	typedef typename Tstring::value_type string_elem_t;
	typedef typename std::basic_ofstream<string_elem_t> ofstream_t;

	ofstream_t ofile(filename, std::ios_base::out | std::ios_base::app);
	if(!ofile.is_open()) {
		print2console(std::wstring(L"logging: failed open file=") + filename);
		}
	else {
		ofile << strout << std::endl;
		ofile.close();

		print2console(strout);
		}
	}


template<typename Tstring>
static void logging(Tstring && msg, 
	int type, 
	std::wstring_view file, 
	int line,
	const std::chrono::system_clock::time_point & timePoint)noexcept {

	typedef typename std::wstring string_t;
	try {
		if (!closef) {

			string_t strout;
			strout += std::to_wstring(type) + L"|";
			strout += string_t(msg.begin(), msg.end()) + L"|";

			if (!file.empty()) {
				strout += file.data();
				strout += L"|";
				strout += std::to_wstring(line) + L"|";
				}

			strout += get_this_thread_id() + L"|";

			auto t = std::chrono::system_clock::to_time_t(timePoint);
			strout += crm::cvt<string_t::value_type, char>(std::ctime(&t));

			/*std::call_once( _firstOpenLog, check_first_open, LogFName );*/

			write2file_sync(strout, LogFName);
			}
		}
	catch(...) {
		__debugbreak();
		std::exit(EXIT_FAILURE);
		}
	}

template<typename Tstring>
static void logging(Tstring && msg, int type, std::wstring_view file, int line)noexcept {
	::logging(std::forward<Tstring>(msg), type, file, line, std::chrono::system_clock::now());
	}


void file_logger_t::global_stout(
	std::wstring_view text, const int /*type*/, std::wstring_view file, const int line)noexcept {

	try {
		write2file_sync(std::wstring(text) + L":" + std::wstring(file) + L":" + std::to_wstring(line),
			LogFName);
		}
	catch (const dcf_exception_t & e0) {
		file_logger_t::logging(e0);
		}
	catch (const std::exception & e1) {
		file_logger_t::logging(e1);
		}
	catch (...) {
		file_logger_t::logging("undefined error at write fatal error to shared file", 1020224);
		}
	}

void file_logger_t::global_stout(
	std::string_view text, const int /*type*/, std::wstring_view file, const int line)noexcept {

	try {
		write2file_sync(std::string(text) + ":" + crm::cvt(std::wstring(file)) + ":" + std::to_string(line),
			LogFName);
		}
	catch (const dcf_exception_t & e0) {
		file_logger_t::logging(e0);
		}
	catch (const std::exception & e1) {
		file_logger_t::logging(e1);
		}
	catch (...) {
		file_logger_t::logging("undefined error at write fatal error to shared file", 1020224);
		}
	}

void file_logger_t::logging(const std::error_code & ec, const std::wstring & file, int line)noexcept {
	logging(ec.message(), ec.value(), file, line);
	}

void file_logger_t::logging(const dcf_exception_t & ntf_)noexcept {
	::logging(ntf_.msg(), ntf_.code(), ntf_.file(), ntf_.line());
	}

void file_logger_t::logging(const std::exception & e)noexcept {
	if(auto ebase = CREATE_PTR_EXC_FWD(e)) {
		logging((*ebase));
		}
	}

void file_logger_t::logging(const std::string & msg, const int type)noexcept {
	::logging(msg, type, std::wstring(), 0);
	}

void file_logger_t::logging(const std::wstring &msg, const int type) noexcept {
	::logging(msg, type, std::wstring(), 0);
	}

void file_logger_t::logging(const std::wstring &msg, const int type, const std::wstring &file, const int line) noexcept {
	::logging(msg, type, file, line);
	}

void file_logger_t::logging(const std::string & msg, const int type, const std::wstring &file, const int line) noexcept {
	::logging(msg, type, file, line);
	}

void file_logger_t::logging(const char* msg, const int type, const std::wstring &file, const int line) noexcept {
	::logging(nullptr != msg ? std::string(msg) : std::string("undefined"), type, file, line);
	}

void file_logger_t::logging(const wchar_t* msg, const int type, const std::wstring &file, const int line) noexcept {
	::logging(nullptr != msg ? std::wstring(msg) : std::wstring(L"undefined"), type, file, line);
	}

