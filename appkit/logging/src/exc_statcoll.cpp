#include "../../internal.h"
#include "../exc_statcoll.h"


using namespace crm;
using namespace crm::detail;


exceptions_trace_collector _Instance;


exceptions_trace_collector::item_value::item_value(std::wstring mess, 
	std::wstring file_,
	int code_,
	int line_)
	: message(std::move(mess))
	, file(std::move(file_))
	, code(code_)
	, line(line_) {}

void exceptions_trace_collector::add(const dcf_exception_t & e)noexcept {
	std::lock_guard lck(_lck);
	
	auto it = _map.find(e.key());
	if(it != _map.end()) {
		++it->second.count;
		}
	else {
		it = _map.emplace(std::piecewise_construct,
			std::forward_as_tuple(e.key()),
			std::forward_as_tuple(e.msg(), e.file(), e.code(), e.line())).first;
		}
	}

std::list<exceptions_trace_collector::item_value> exceptions_trace_collector::list()const noexcept {
	std::lock_guard lck(_lck);

	std::list<item_value> l;
	for(auto & it : _map) {
		l.push_back(it.second);
		}

	return l;
	}

exceptions_trace_collector& exceptions_trace_collector::instance()noexcept {
	return _Instance;
	}


