#include "../../internal.h"
#include "../collect_by_id.h"

using namespace crm;
using namespace crm::detail;


#ifdef CBL_TRACE_CTX_STATE

context_trace_collection_item::context_trace_collection_item(std::string_view name_)
	: context_trace_collection_item(sx_locid_t::as_hash(name_), name_) {
	}

context_trace_collection_item::context_trace_collection_item(const sx_locid_t & id, std::string_view name_)
	: _id(id)
	, _name(name_)
	, _tp(std::chrono::system_clock::now()) {
	}

const sx_locid_t& context_trace_collection_item::id()const noexcept {
	return _id;
	}

const std::string& context_trace_collection_item::name()const noexcept {
	return _name;
	}

std::intmax_t context_trace_collection_item::count()const noexcept {
	return _count;
	}

void context_trace_collection_item::inc_ref()noexcept {
	++_count;
	}

void context_trace_collection_item::dec_ref()noexcept {
	--_count;
	}

const std::chrono::system_clock::time_point& context_trace_collection_item::time_point()const noexcept {
	return _tp;
	}


std::vector<std::tuple<
	sx_locid_t,
	std::string,
	std::intmax_t,
	std::chrono::system_clock::time_point

	>> context_trace_collection::view()const noexcept {

	CHECK_NO_EXCEPTION_BEGIN

		std::vector<std::tuple<
		sx_locid_t,
		std::string,
		std::intmax_t,
		std::chrono::system_clock::time_point>> r;

	std::lock_guard<std::mutex> lck(_lck);
	for (const auto & item : _c) {
		r.emplace_back(item.second.id(), item.second.name(), item.second.count(), item.second.time_point());
		}

	return r;

	CHECK_NO_EXCEPTION_END
	}

void context_trace_collection::inc(const sx_locid_t & id_, std::string_view name_)noexcept {
	CBL_VERIFY(!name_.empty());

	CHECK_NO_EXCEPTION_BEGIN

	std::lock_guard<std::mutex> lck(_lck);
	auto it = _c.find(id_);
	if (it == _c.end()) {
		auto ins = _c.insert({id_, context_trace_collection_item{id_, name_}});
		if (ins.second) {
			it = ins.first;
			}
		else {
			FATAL_ERROR_FWD(nullptr);
			}
		}

	it->second.inc_ref();

	CHECK_NO_EXCEPTION_END
	}

void context_trace_collection::dec(const sx_locid_t& name_)noexcept {
	CHECK_NO_EXCEPTION_BEGIN

	std::lock_guard<std::mutex> lck(_lck);
	auto it = _c.find(name_);
	if (it != _c.end()) {
		it->second.dec_ref();
		if (it->second.count() == 0) {
			CHECK_NO_EXCEPTION(_c.erase(it));
			}
		}

	CHECK_NO_EXCEPTION_END
	}

std::intmax_t context_trace_collection::count(const sx_locid_t& name_)const noexcept {
	std::intmax_t r{0};

	CHECK_NO_EXCEPTION_BEGIN

		std::lock_guard<std::mutex> lck(_lck);
	auto it = _c.find(name_);
	if (it != _c.end()) {
		r = it->second.count();
		}

	return r;

	CHECK_NO_EXCEPTION_END
	}

context_trace_collection traceCollection;
context_trace_collection& crm::detail::get_trace_collection()noexcept {
	return traceCollection;
	}
#endif


#ifdef CBL_USE_OBJECTS_MONITOR


object_root_t::object_root_t(const sx_locid_t & id, std::weak_ptr<i_terminatable_t> wptr)
	: _id(id)
	, _iroot(std::move(wptr))
	, _tp(std::chrono::system_clock::now()) {
	}

const sx_locid_t& object_root_t::id()const noexcept {
	return _id;
	}

bool object_root_t::is_terminatable()const noexcept {
	if (auto p = _iroot.lock()) {
		return std::dynamic_pointer_cast<const i_terminatable_t>(p) != nullptr;
		}
	else {
		return false;
		}
	}

bool object_root_t::has_timeout()const noexcept {
	if (auto p = std::dynamic_pointer_cast<const i_terminatable_timeout_t>(_iroot.lock())) {
		return p->timeout_interval().count() != 0;
		}
	else {
		return false;
		}
	}

const std::chrono::system_clock::time_point& object_root_t::time_point()const noexcept {
	return _tp;
	}

void object_root_t::check_if_terminate(std::function<void(const sx_locid_t& id, std::shared_ptr<i_terminatable_t> pTerminate)> handler)noexcept {
	if (auto p = std::dynamic_pointer_cast<i_terminatable_timeout_t>(_iroot.lock())) {
		if (p->timeout_interval().count() == 0) {

			const auto stp = p->time_point_start();
			const auto ntp = std::chrono::system_clock::now();
			const auto dursec = std::chrono::duration_cast<std::chrono::seconds>(ntp - stp);
			if (dursec.count() > 60) {
				GLOBAL_STOUT_FWDRITE_STR(("terminatable operation long: " + p->name() + ":sec " + std::to_string(dursec.count())));
				}

			if (ntp > stp && std::chrono::duration_cast<decltype(p->default_timeline())>((ntp - stp)) > p->default_timeline()) {
				handler( id(), std::move(p));				
				}
			}
		}
	}



#ifdef CBL_OBJECTS_COUNTER_CHECKER
void object_rootable_collection::__size_trace()const noexcept {
	CHECK_NO_EXCEPTION(__sizeTraceer1.CounterCheck(_c.size()));
	}
#endif

void object_rootable_collection::set(const sx_locid_t & id_, std::weak_ptr<i_terminatable_t> wptr)noexcept {
	std::lock_guard<std::mutex> lck(_lck);

	auto it = _c.find(id_);
	if (it == _c.end()) {
		auto ins = _c.insert({id_, object_root_t(id_, std::move(wptr))});
		if (ins.second) {
			it = ins.first;
			}
		else {
			FATAL_ERROR_FWD(nullptr);
			}

#ifdef CBL_OBJECTS_COUNTER_CHECKER
		CHECK_NO_EXCEPTION(__size_trace());
#endif
		}
	else {
		FATAL_ERROR_FWD(nullptr);
		}
	}

void object_rootable_collection::unset(const sx_locid_t & id_)noexcept {
	std::lock_guard<std::mutex> lck(_lck);
	_c.erase(id_);

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	CHECK_NO_EXCEPTION(__size_trace());
#endif
	}

std::vector< object_root_t> object_rootable_collection::get_collection()const noexcept {
	std::lock_guard<std::mutex> lck(_lck);
	
	std::vector< object_root_t> r;
	std::transform(_c.cbegin(), _c.cend(), std::back_inserter(r), [](const auto& p) { return p.second; });

	return r;
	}

std::vector<sx_locid_t> object_rootable_collection::check_if_terminate() noexcept {
	std::vector<sx_locid_t> idl;
	auto wpl = get_collection();
	for (auto& o : wpl) {
		o.check_if_terminate([&, this](const sx_locid_t& id, std::shared_ptr<i_terminatable_t> p_)noexcept {
			CBL_NO_EXCEPTION_BEGIN
			if (p_) {
				idl.push_back(id);

				if (auto p = std::dynamic_pointer_cast<i_terminatable_timeout_t>(p_)) {
					p->do_terminate();
					}
				else {
					FATAL_ERROR_FWD(nullptr);
					}
				}
			CBL_NO_EXCEPTION_END(_ctx.lock())
			});
		}

	return idl;
	}

void object_rootable_collection::check()noexcept {
	CHECK_NO_EXCEPTION_BEGIN

	auto idl = check_if_terminate();

	std::lock_guard<std::mutex> lck(_lck);
	for (auto const & il : idl) {
		CHECK_NO_EXCEPTION(_c.erase(il));
		}

	CHECK_NO_EXCEPTION_END
	}

void object_rootable_collection :: set_ctx(std::weak_ptr<distributed_ctx_t> c)noexcept {
	_ctx = c;
	}
#endif
