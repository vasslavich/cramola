#pragma once


#include "../protocol/internal_terms.h"


namespace crm {
namespace detail {


#ifdef CBL_TRACE_CTX_STATE
class context_trace_collection_item {
	sx_locid_t _id;
	std::string _name;
	std::intmax_t _count{0};
	std::chrono::system_clock::time_point _tp;

public:
	context_trace_collection_item(std::string_view name_);
	context_trace_collection_item(const sx_locid_t & id, std::string_view name_);
	const std::string& name()const noexcept;
	const sx_locid_t& id()const noexcept;
	std::intmax_t count()const noexcept;
	void inc_ref()noexcept;
	void dec_ref()noexcept;
	const std::chrono::system_clock::time_point& time_point()const noexcept;
	};

class context_trace_collection {
	std::map<sx_locid_t, context_trace_collection_item> _c;
	mutable std::mutex _lck;

public:
	std::vector<std::tuple<
		sx_locid_t,
		std::string,
		std::intmax_t,
		std::chrono::system_clock::time_point
		>> view()const noexcept;

	void inc(const sx_locid_t & id_, std::string_view name_)noexcept;
	void dec(const sx_locid_t & id_)noexcept;
	std::intmax_t count(const sx_locid_t & id_)const noexcept;
	};


context_trace_collection& get_trace_collection()noexcept;

#endif


#ifdef CBL_USE_OBJECTS_MONITOR
class object_root_t {
	sx_locid_t _id;
	std::weak_ptr<i_terminatable_t> _iroot;
	std::chrono::system_clock::time_point _tp;

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	utility::__xobject_counter_logger_t<object_root_t, 1> _xDbgCounter;
#endif

public:
	object_root_t(const sx_locid_t & id, std::weak_ptr<i_terminatable_t> wptr);

	const sx_locid_t& id()const noexcept;
	bool is_terminatable()const noexcept;
	bool has_timeout()const noexcept;
	const std::chrono::system_clock::time_point& time_point()const noexcept;
	void check_if_terminate(std::function<void(const sx_locid_t& id, std::shared_ptr<i_terminatable_t> pTerminate)> handler )noexcept;
	};

class object_rootable_collection {
	std::map<sx_locid_t, object_root_t> _c;
	std::weak_ptr<distributed_ctx_t> _ctx;
	mutable std::mutex _lck;

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	mutable utility::__xvalue_counter_logger_t<1> __sizeTraceer1 = __FILE_LINE__;
	void __size_trace()const noexcept;
#endif

	std::vector< object_root_t> get_collection()const noexcept;
	std::vector<sx_locid_t> check_if_terminate() noexcept;

public:
	void set_ctx(std::weak_ptr<distributed_ctx_t> c)noexcept;
	void set(const sx_locid_t & id_, std::weak_ptr<i_terminatable_t> wptr)noexcept;
	void unset(const sx_locid_t & id_)noexcept;
	void check()noexcept;
	};

#endif
}
}



