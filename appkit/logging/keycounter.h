#pragma once


#include <array>
#include <unordered_set>
#include <tuple>
#include "../utilities/identifiers/predecl.h"
#include "../utilities/hash/predecl.h"


namespace crm::detail {


#ifdef CBL_MESSAGE_ADDRESS_TYPES_SHORT

/*! Message spin-tag type for send-receive operations */
using key_counter_type = sx_loc_64;

#else

/*! Message spin-tag type for send-receive operations */
using key_counter_type = sx_uuid_t;

#endif



struct keycounter_lines {
	static const size_t size = 1024;
	std::array<uint16_t, size> il;
	};

class keycounter_trace {
public:
	using key_t = key_counter_type;

	/*struct hash_keycounter {
		std_hash_type operator()(const key_t& obj) const noexcept;
		};*/

private:
	keycounter_lines _keyStat;
	std::unordered_map<key_t, keycounter_lines/*, hash_keycounter*/> _keyset;
	std::unordered_set<key_t/*, hash_keycounter*/> _blackList;
	mutable std::mutex _lck;

public:
	keycounter_trace()noexcept;

	void set(key_t key, int i)noexcept;
	void unset(key_t key)noexcept;
	std::vector<std::tuple<key_t, keycounter_lines>> view()const noexcept;
	size_t size()const noexcept;
	std::tuple<size_t, size_t> coverage_stat()const noexcept;

	keycounter_trace(const keycounter_trace&) = delete;
	keycounter_trace& operator=(const keycounter_trace &) = delete;
	};


#ifdef CBL_MPF_MESSAGE_TRACEWAY_KEYS_BY_ID
keycounter_trace& get_keytracer()noexcept;
#endif
}







