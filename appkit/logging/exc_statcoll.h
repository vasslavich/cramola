#pragma once


#include <map>
#include <list>
#include <mutex>
#include "../notifications/base_terms.h"


namespace crm::detail {


class exceptions_trace_collector {
private:
	using key_t = dcf_exception_t::key_t;

public:
	struct item_value {
		std::wstring message;
		std::wstring file;
		int code{ 0 };
		int line{ 0 };
		int count{ 0 };

		item_value(std::wstring mess, 
			std::wstring file,
			int code,
			int line);
		};

private:
	std::map<key_t, item_value> _map;
	mutable std::mutex _lck;

public:
	void add(const dcf_exception_t& e)noexcept;
	std::list<item_value> list()const noexcept;

	static exceptions_trace_collector& instance()noexcept;
	};
}

