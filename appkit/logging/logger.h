#pragma once


#include <exception>
#include "../internal_invariants.h"
#include "../notifications/base_terms.h"


namespace crm {


class file_logger_t {
public:
	static void logging(const std::wstring &msg, const int type = 0)noexcept;
	static void logging(const std::string & msg, const int type = 0)noexcept;
	static void logging(const dcf_exception_t& ntf)noexcept;
	static void logging(const std::exception & e)noexcept;
	static void logging(const std::error_code & ec, const std::wstring & file, int line)noexcept;

	static void logging(
		const std::wstring &msg, const int type, const std::wstring &file, const int line)noexcept;
	static void logging(
		const std::string & msg, const int type, const std::wstring &file, const int line)noexcept;
	static void logging(
		const char* msg, const int type, const std::wstring &file, const int line)noexcept;
	static void logging(
		const wchar_t* msg, const int type, const std::wstring &file, const int line)noexcept;

	static void global_stout(
		std::wstring_view msg, const int type, std::wstring_view file, const int line)noexcept;
	static void global_stout(
		std::string_view msg, const int type, std::wstring_view file, const int line)noexcept;
	};


using logger_t = file_logger_t;
}


