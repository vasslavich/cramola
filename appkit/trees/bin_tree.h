#pragma once 


#include "../../../core/err/err.h"
#include "./bin_tree_op.h"


namespace crm{
namespace aux{
/**
Tnode ���������:

���� ����������:
Tkey Tnode::key;
Tnode *left;
Tnode *right;

������ ����������:
void copy_data( const Tnode *src );

TnodeAllocator ���������:

������ ����������:
Tnode *allocate();
void destroy( Tnode *n );
*/
template< typename Tkey, typename Tnode, typename TnodeAllocator >
class bin_tree_t : public BinTreeOp<Tkey, Tnode> {
private:
	TnodeAllocator *_nodeAllocator;

	Tnode* insert_node( const Tkey &key ) {

		Tnode *y = _nil;
		Tnode *x = _root;

		/*  ����� �������� ���� ����� ��� ������� ��������
			���� ����������� �� ����� �����
			---------------------------------------------------------*/
		while( _nil != x ) {

			y = x;

			if( key < x->key )x = x->left;
			else if( key > x->key ) x = x->right;

			// ���� ��� ����������, ����� ���� �� �����������, ������������ �� ���������
			// ������� ��������� �� ������������ ����
			else return x;
			}

		/* ������� ����
		--------------------------------------------------------*/
		Tnode *z = _nodeAllocator->allocate();

		z->key = key;
		z->left = _nil;
		z->right = _nil;


		/* ���������� ���� � ������
		--------------------------------------------------------*/
		// �������� ��� Z
		z->parent = y;

		// �������� � ����� ��� ������ ��������� � ����������� �� �����
		if( _nil == y )
			_root = z;
		else {

			if( z->key < y->key )y->left = z;
			else y->right = z;
			}

		++_count;

		return z;
		}


	Tnode* delete_node( Tnode *z ) {

		Tnode *y = _nil;
		Tnode *x = _nil;

		CBL_VERIFY( z != _nil );

		if( z->left == _nil || z->right == _nil )y = z;
		else y = next( z );

		if( y->left != _nil )x = y->left;
		else x = y->right;

		if( _nil != x )
			x->parent = y->parent;

		if( y->parent == _nil )_root = x;
		else {
			if( y == y->parent->left )y->parent->left = x;
			else y->parent->right = x;
			}

		if( y != z ) {
			z->key = y->key;
			z->copy_data( y );
			}

		return y;
		}


	void clear_nodes( Tnode *x ) {
		if( 0 == x || x == _nil )return;

		if( x->left != _nil )clear_nodes( x->left );
		x->left = 0;

		if( x->right != _nil )clear_nodes( x->right );
		x->right = 0;

		_nodeAllocator->destroy( x );
		--_count;
		}


	/* ���� �� ����� ��������� ����������� */
	bin_tree_t( const bin_tree_t &o );
	bin_tree_t& operator=(const bin_tree_t &o);


	void remove( Tnode *n ) {
		CBL_VERIFY( _nil != n && 0 != n );

		Tnode *removedNode = delete_node( n );
		_nodeAllocator->destroy( removedNode );

		--_count;
		}

public:
	bin_tree_t( TnodeAllocator *nodeAllocator )
		: BinTreeOp( 0, 0 )
		, _nodeAllocator( nodeAllocator ) {}

	~bin_tree_t() { clear(); }

	Tnode* insert( const Tkey &key ) {
		return insert_node( key );
		}

	void remove( const Tkey &key ) {
		Tnode *n = find( key );
		if( _nil != n )remove( n );
		}

	const Tnode* end()const {
		return _nil;
		}

	const Tnode* nil()const {
		return _nil;
		}

	void clear() {
		if( _nil != _root && 0 != _root )
			clear_nodes( _root );

		CBL_VERIFY( 0 == _count );

		// ������ ������� ������ ��������� �� ������������!
		_root = _nil;
		}
	};
}
}



