#include <vector>
#include <set>
#include <iostream>
#include "../../../core/err/err.h"
#include "./bin_tree_op.h"


namespace crm{
namespace aux{
namespace test{

template< typename Tkey, typename TTree, typename TNode >
void test_map_walk( const std::set<Tkey> &baseSet, const TTree &test ) {

	if( baseSet.size() != test.count() )THROW_EXC_FWD(nullptr);

	if( baseSet.size() == 0 )return;

	/* ������ �� ������� � ����������
	------------------------------------------------------------*/
	std::set<Tkey>::const_iterator baseIt = baseSet.begin();
	const std::set<Tkey>::const_iterator baseEnd = baseSet.end();

	const TNode *testIt = test.first();
	const TNode *testEnd = test.end();

	while( baseIt != baseEnd ) {
		const Tkey keyBase = (*baseIt);

		if( testIt == testEnd )THROW_EXC_FWD(nullptr);
		const Tkey keyTest = testIt->get_key();

		if( keyTest != keyBase )THROW_EXC_FWD(nullptr);

		++baseIt;
		testIt = test.next( testIt );
		}

	if( testIt != test.end() )THROW_EXC_FWD(nullptr);

	/* ������ �� ���������� � �������
	-----------------------------------------------------------*/
	baseIt = baseSet.end();
	--baseIt;

	const std::set<Tkey>::const_iterator baseFirst = baseSet.begin();

	testIt = test.last();

	for( size_t i = 0; i < baseSet.size(); ++i ) {
		const Tkey keyBase = (*baseIt);
		const Tkey keyTest = testIt->get_key();

		if( keyTest != keyBase )THROW_EXC_FWD(nullptr);

		--baseIt;
		testIt = test.prev( testIt );
		}
	}


template< typename Tkey, typename TTree, typename TNode, typename TNodeAllocator >
void test_map_full( const std::vector<Tkey> &checkList, TNodeAllocator *allocator ) {

	std::set<Tkey> baseSet;
	TTree test( allocator );

	std::cout << "start check inserting..." << std::endl;

	/* ������ � �����������
	------------------------------------------------------------------*/
	for( size_t i = 0; i < checkList.size(); ++i ) {

		// ���������� � �������
		baseSet.insert( checkList[i] );
		test.insert( checkList[i] );

		// ��������
		test_map_walk<Tkey, TTree, TNode>( baseSet, test );

		wcout << L"\rprogress : " << 100.0 * ((double)(i + 1) / (double)checkList.size());
		}

	std::cout << std::endl << "start check removing..." << std::endl;

	/* ������ � ���������
	-----------------------------------------------------------------*/
	std::vector<Tkey> lostList( checkList.begin(), checkList.end() );
	srand( (uint32_t)lostList.size() );

	while( lostList.size() > 0 ) {

		// ��������� ������� � �������
		std::vector<Tkey>::iterator removeIt = lostList.begin() + (rand() % lostList.size());
		const Tkey removeKey = (*removeIt);

		// ������� �� ������
		lostList.erase( removeIt );

		// ������� �� ������
		baseSet.erase( removeKey );
		test.remove( removeKey );

		// ��������
		test_map_walk<Tkey, TTree, TNode>( baseSet, test );

		wcout << L"\rprogress : " << 100.0 * ((double)(checkList.size() - lostList.size()) / (double)checkList.size());
		}

	if( test.count() != 0 )THROW_EXC_FWD(nullptr);

	std::wcout << endl;
	}


template< typename TBTree, typename TKey, typename TBTableEntry >
void test_bintree_keytable( const std::vector<TKey> &checkList ) {

	BTKeyNodeAllocator<TKey> nAllocator;

	std::wcout << L"filling tables: " << std::endl;

	/* ���������� �������� ���������� � �������� �������
	----------------------------------------------*/
	std::set<TKey> baseSet;
	TBTree testBT( &nAllocator );

	for( size_t i = 0; i < checkList.size(); ++i ) {
		baseSet.insert( checkList[i] );
		testBT.insert( checkList[i] );

		wcout << L"\rprogress : " << 100.0 * ((double)(i) / (double)checkList.size());
		}

	// �������� � �������
	std::vector< TBTableEntry > testKTable;
	testBT.convert( testKTable );

	std::wcout << std::endl << "check [find]: " << std::endl;

	/* ��������� ����� ��� ��������
	----------------------------------------------*/
	for( size_t i = 0; i < checkList.size(); ++i ) {
		const TKey baseKey = *(baseSet.find( checkList[i] ));

		// � �������� �������
		const size_t testNodeID = find( &testKTable[0], testKTable.size(), baseKey );
		if( testNodeID >= testKTable.size() )THROW_EXC_FWD(nullptr);

		// ������ ��������� �� ��������
		if( baseKey != testKTable[testNodeID].key )THROW_EXC_FWD(nullptr);

		std::wcout << L"\rprogress : " << 100.0 * ((double)(i) / (double)checkList.size());
		}

	std::wcout << std::endl << L"check [removing]: " << std::endl;

	/* ������ ������ �� ������
	----------------------------------------------*/
	size_t testTableDelCnt = 0;
	std::vector<TKey> lostList( checkList.begin(), checkList.end() );
	while( lostList.size() > 0 ) {

		// ��������� ������� � �������
		std::vector<TKey>::iterator removeIt = lostList.begin() + (rand() % lostList.size());
		const TKey removeKey = (*removeIt);

		// ������� �� ������
		lostList.erase( removeIt );

		// ������� �� ������� ������
		baseSet.erase( removeKey );

		// ������� �� �������� �������
		int isEntry = 0;
		remove( &testKTable[0], testKTable.size(), removeKey, &isEntry );

		// ���������, ��� ������ ��� �������� ���
		if( find( &testKTable[0], testKTable.size(), removeKey ) < testKTable.size() )THROW_EXC_FWD(nullptr);

		// ����� ������� ��������� �� �������� ������� ���������
		testTableDelCnt += isEntry;

		wcout << L"\rprogress : " << 100.0 * ((double)(checkList.size() - lostList.size()) / (double)checkList.size());
		}

	// ��������� ������������ ��������/����� ����������� ���������� ����� � �������� �������
	if( testTableDelCnt != checkList.size() )THROW_EXC_FWD(nullptr);

	// ���������, ��� ��� �������� �������� ��� ���������
	for( size_t i = 0; i < testKTable.size(); ++i )
		if( !(testKTable[i].flags & btable_key_t<TKey>::NODE_REMOVED) )THROW_EXC_FWD(nullptr);

	std::cout << std::endl;
	}
}
}
}
