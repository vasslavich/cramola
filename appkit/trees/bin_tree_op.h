#pragma once

#include "../../../core/err/err.h"




/**
Tnode ���������:

	���� ����������:
		Tkey Tnode::key;
		Tnode *left;
		Tnode *right;

	������ ����������:
		void copy_data( const Tnode *src );
*/
template< typename Tkey, typename Tnode >
class BinTreeOp{
protected:
    Tnode *_nil;
	Tnode *_root;
    size_t _count;


#define RB_TREE_BRANCH_MIN(x){\
    while( x->left != _nil )x = x->left;\
    return x;\
    }

    Tnode* branch_min( Tnode *n ){
        RB_TREE_BRANCH_MIN(n); }

    const Tnode* branch_min( const Tnode *n )const{
        RB_TREE_BRANCH_MIN(n); }

#undef RB_TREE_BRANCH_MIN


#define RB_TREE_BRANCH_MAX(x){\
    while( x->right != _nil )x = x->right;\
    return x;\
    }

    Tnode* branch_max( Tnode *n ){
        RB_TREE_BRANCH_MAX(n); }

    const Tnode* branch_max( const Tnode *n )const{
        RB_TREE_BRANCH_MAX(n); }

#undef RB_TREE_BRANCH_MAX





#define  RB_TREE_FIND_NEXT(x){\
    if( x->right != _nil )return branch_min( x->right );\
\
    y = x->parent;\
    while( y != _nil && x == y->right ){\
        x = y;\
        y = y->parent;\
        }\
\
    return y;\
    }

    Tnode* find_next( Tnode *x ){
        Tnode *y = 0;
        RB_TREE_FIND_NEXT(x); 
        }

    const Tnode* find_next( const Tnode *x )const{
        const Tnode *y = 0;
        RB_TREE_FIND_NEXT(x); 
        }

#undef RB_TREE_FIND_NEXT


#define RB_TREE_FIND_PREV(x){\
    if( x->left != _nil )return branch_max( x->left );\
\
    y = x->parent;\
    while( y != _nil && x == y->left ){\
        x = y;\
        y = y->parent;\
        }\
\
    return y;\
    }

    Tnode* find_prev( Tnode *x ){
        Tnode *y = 0;
        RB_TREE_FIND_PREV(x); 
        }

    const Tnode* find_prev( const Tnode *x )const{
        const Tnode *y = 0;
        RB_TREE_FIND_PREV(x); 
        }

#undef RB_TREE_FIND_PREV



#define RB_TREE_FIND( x, key ){\
    if( _nil == x || key == x->key )return x;\
\
    if( key < x->key )return find( x->left, key );\
    else return find( x->right, key );\
    }

    Tnode * find( Tnode *x, const Tkey &key ){
        RB_TREE_FIND(x, key); }

        
    const Tnode * find( Tnode *x, const Tkey &key )const{
        RB_TREE_FIND(x, key); }

#undef RB_TREE_FIND


    /* ���� �� ����� ��������� ����������� */
    BinTreeOp( const BinTreeOp &o );
    BinTreeOp& operator=( const BinTreeOp &o );

protected:
    const Tnode *root()const{
        return _root; }

public:
    BinTreeOp( Tnode *root, Tnode *nil ) :_nil(nil), _root(root), _count(0){}

    virtual ~BinTreeOp(){}

    Tnode * find( const Tkey &key ){
        return find( _root, key ); }

    const Tnode * find( const Tkey &key )const{
        return find( _root, key ); }

    const Tnode* first()const{
        return find_min(); }

    Tnode* first(){
        return find_min(); }

    const Tnode* last()const{
        return find_max(); }

    Tnode* last(){
        return find_max(); }

    Tnode* find_min(){
        return branch_min( _root ); }

    const Tnode* find_min()const{
        return branch_min( _root ); }

    Tnode* find_max(){
        return branch_max( _root ); }

    const Tnode* find_max()const{
        return branch_max( _root ); }

    const Tnode* next( const Tnode *n )const{
        CBL_VERIFY( _nil != n );
        return find_next( n ); 
        }

    Tnode* next( Tnode *n ){
        CBL_VERIFY( _nil != n );
        return find_next( n ); 
        }

    const Tnode* prev( const Tnode *n )const{
        CBL_VERIFY( _nil != n );
        return find_prev( n ); 
        }

    Tnode* prev( Tnode *n ){
        CBL_VERIFY( _nil != n );
        return find_prev( n ); 
        }

    size_t count()const{
        return _count; }
	};



