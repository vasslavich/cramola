#pragma once


#include <sstream>
#include <fstream>
#include "../../declare.h"
#include "../../../core/err/err.h"
#include "./bin_tree_op.h"


namespace crm{
namespace aux{
/**
Tnode ���������:

���� ����������:
Tkey Tnode::key;
char Tnode::balanceFactor;
Tnode *parent;
Tnode *left;
Tnode *right;

������ ����������:
void copy_data( const Tnode *src );

TnodeAllocator ���������:

������ ����������:
Tnode *allocate();
void destroy( Tnode *n );
*/
template< typename Tkey, typename Tnode, typename TnodeAllocator >
class avl_tree_t : public BinTreeOp<Tkey, Tnode> {
private:
	static const int8_t LEFT_HEAVY = -1;
	static const int8_t BALANCED = 0;
	static const int8_t RIGHT_HEAVY = 1;

	TnodeAllocator *_nodeAllocator;

	const char* print_tree()const {

		std::ostringstream os;

		os << " node[hex]   :  key  :  balance  :  parent[hex]  :  left[hex]  :  right[hex]" << std::endl;

		const Tnode *it = first();
		while( it != end() ) {

			os << std::hex << ((uintptr_t)it);
			os << " : " << it->key;
			os << " : " << (int)it->balanceFactor;
			os << " : " << std::hex << ((uintptr_t)it->parent);
			os << " : " << std::hex << ((uintptr_t)it->left);
			os << " : " << std::hex << ((uintptr_t)it->right);
			os << std::endl;

			it = next( it );
			}

		std::ofstream f( ".\\avl-tree" );
		f << std::move( os.str() );
		f.close();

		return "write log to file avl-tree";
		}

	Tnode* left_rotate( Tnode *x ) {

		// ��������� Y
		Tnode *y = x->right;

		// ��������� � ������ ��������� X ����� Y
		x->right = y->left;

		// �������� �������� ������ ��������� Y �� X
		if( _nil != y->left )
			y->left->parent = x;

		// �������� X ���������� ��������� ��� Y
		y->parent = x->parent;

		// �������� ��������� �� �������� X
		// ���� x - ������ ������
		if( _nil == x->parent )
			_root = y;
		else {

			// ���� x - ����� ������� ������ ��������,
			// ��������� ��� ����� �������� Y
			if( x == x->parent->left )
				x->parent->left = y;
			else
				x->parent->right = y;
			}

		// ����������� X � ����� ��������� Y
		y->left = x;
		// �������� ��������� �� �������� � X
		x->parent = y;

		return y;
		}


	Tnode* right_rotate( Tnode *y ) {

		// ��������� X
		Tnode *x = y->left;

		// ��������� ������ ��������� X � ����� ��������� Y
		y->left = x->right;

		// �������� �������� ������� ��������� X �� Y
		if( _nil != x->right )
			x->right->parent = y;

		// �������� Y ���������� ��������� ��� X
		x->parent = y->parent;

		// �������� ��������� �� �������� Y
		if( _nil == y->parent )
			_root = x;
		else {

			// ���� Y - ����� ��������� ������ ��������
			if( y == y->parent->left )
				y->parent->left = x;
			else
				y->parent->right = x;
			}

		// ����������� Y � ������ ��������� X
		x->right = y;
		// �������� ���� �� �������� � Y
		y->parent = x;

		return x;
		}


	void left_rotate_balancing( Tnode *t ) {
		CBL_VERIFY( _nil != t );

		Tnode *x = t->right;
		CBL_VERIFY( _nil != x );

		CBL_VERIFY( t->balanceFactor >= LEFT_HEAVY && t->balanceFactor <= RIGHT_HEAVY );
		CBL_VERIFY( x->balanceFactor >= LEFT_HEAVY && x->balanceFactor <= RIGHT_HEAVY );

		if( RIGHT_HEAVY == x->balanceFactor ) {
			t->balanceFactor = BALANCED;
			x->balanceFactor = BALANCED;
			}
		else {
			t->balanceFactor = RIGHT_HEAVY;
			x->balanceFactor = LEFT_HEAVY;
			}
		}


	void right_rotate_balancing( Tnode *t ) {
		CBL_VERIFY( _nil != t );

		Tnode *x = t->left;
		CBL_VERIFY( _nil != x );

		CBL_VERIFY( t->balanceFactor >= LEFT_HEAVY && t->balanceFactor <= RIGHT_HEAVY );
		CBL_VERIFY( x->balanceFactor >= LEFT_HEAVY && x->balanceFactor <= RIGHT_HEAVY );

		if( LEFT_HEAVY == x->balanceFactor ) {
			t->balanceFactor = BALANCED;
			x->balanceFactor = BALANCED;
			}
		else {
			t->balanceFactor = LEFT_HEAVY;
			x->balanceFactor = RIGHT_HEAVY;
			}
		}


	void leftright_rotate_balancing( Tnode *t ) {
		CBL_VERIFY( _nil != t );

		Tnode *x = t->left;
		CBL_VERIFY( _nil != x );

		Tnode *y = x->right;
		CBL_VERIFY( _nil != y );

		CBL_VERIFY( t->balanceFactor >= LEFT_HEAVY && t->balanceFactor <= RIGHT_HEAVY );
		CBL_VERIFY( x->balanceFactor >= LEFT_HEAVY && x->balanceFactor <= RIGHT_HEAVY );

		if( LEFT_HEAVY == y->balanceFactor ) {
			t->balanceFactor = RIGHT_HEAVY;
			x->balanceFactor = BALANCED;
			}
		else if( BALANCED == y->balanceFactor ) {
			t->balanceFactor = BALANCED;
			x->balanceFactor = BALANCED;
			}
		else if( RIGHT_HEAVY == y->balanceFactor ) {
			t->balanceFactor = BALANCED;
			x->balanceFactor = LEFT_HEAVY;
			}
		else THROW_EXC_FWD(nullptr);

		y->balanceFactor = BALANCED;
		}


	void rightleft_rotate_balancing( Tnode *t ) {
		CBL_VERIFY( _nil != t );

		Tnode *x = t->right;
		CBL_VERIFY( _nil != x );

		Tnode *y = x->left;
		CBL_VERIFY( _nil != y );

		CBL_VERIFY( t->balanceFactor >= LEFT_HEAVY && t->balanceFactor <= RIGHT_HEAVY );
		CBL_VERIFY( x->balanceFactor >= LEFT_HEAVY && x->balanceFactor <= RIGHT_HEAVY );

		if( RIGHT_HEAVY == y->balanceFactor ) {
			t->balanceFactor = LEFT_HEAVY;
			x->balanceFactor = BALANCED;
			}
		else if( BALANCED == y->balanceFactor ) {
			t->balanceFactor = BALANCED;
			x->balanceFactor = BALANCED;
			}
		else if( LEFT_HEAVY == y->balanceFactor ) {
			t->balanceFactor = BALANCED;
			x->balanceFactor = RIGHT_HEAVY;
			}
		else 
			THROW_EXC_FWD(nullptr);

		y->balanceFactor = BALANCED;
		}


	void check_state( const Tnode *n, int *branchCounter )const {

		// ����
		if( _nil == n ) {
			(*branchCounter) = 1;
			return;
			}

		int lengthNLeft = 0;
		int lengthNRight = 0;

		check_state( n->left, &lengthNLeft );
		check_state( n->right, &lengthNRight );

		// ����� � ������ ����� �� ������ ���������� �� ����� ������ ��� �� 1
		if( abs( abs( lengthNRight ) - abs( lengthNLeft ) ) > 1 )THROW_EXC_FWD(nullptr);

		// ����� ����� ������ �������� ������ � ����� �����
		(*branchCounter) = MAX( lengthNLeft, lengthNRight ) + 1/* ��� ���� */;

		if( n->balanceFactor < LEFT_HEAVY || n->balanceFactor > RIGHT_HEAVY )THROW_EXC_FWD(nullptr);
		}

	void check_state()const {
		int h = 0;
		check_state( _root, &h );
		}

	Tnode* insert_node( const Tkey &key ) {

		/* ����� �����
		---------------------------------------------------------*/
		Tnode *xParent = _nil;
		Tnode *x = _root;

		/*  ����� �������� ���� ����� ��� ������� ��������
			���� ����������� �� ����� �����
			---------------------------------------------------------*/
		while( _nil != x ) {

			xParent = x;

			if( key < x->key )x = x->left;
			else if( key > x->key ) x = x->right;

			// ���� ��� ����������, ����� ���� �� �����������, ������������ �� ���������
			// ������� ��������� �� ������������ ����
			else {
				CBL_VERIFY( key == x->key );
				return x;
				}
			}

		/* ���� �� ������, �������� �����
		------------------------------------------------------------*/
		Tnode *z = _nil;
		if( _nil == x ) {
			/* ������� ����
			--------------------------------------------------------*/
			z = _nodeAllocator->allocate();
			++_count;

			z->balanceFactor = BALANCED;
			z->key = key;
			z->left = _nil;
			z->right = _nil;


			/* ���������� ���� � ������
			--------------------------------------------------------*/
			// �������� ��� Z
			z->parent = xParent;

			// �������� � ����� ��� ������ ��������� � ����������� �� �����
			if( _nil == xParent ) {
				CBL_VERIFY( _nil == _root );
				_root = z;
				}
			else {

				// ����������� ����� �����
				if( z->key < xParent->key ) {
					xParent->left = z;
					CBL_VERIFY( xParent->balanceFactor >= LEFT_HEAVY);
					}
				// ����������� ������ �����
				else {
					xParent->right = z;
					CBL_VERIFY( xParent->balanceFactor <= RIGHT_HEAVY);
					}
				}
			}

		/* ������������ ����� ������� ����
		---------------------------------------------------------------*/

		Tnode *t = xParent;
		Tnode *nbal = z;
		bool rebalance = true;

		while( _nil != t && rebalance ) {
			CBL_VERIFY( nbal == t->left || nbal == t->right );

			CBL_VERIFY( t->balanceFactor >= LEFT_HEAVY && t->balanceFactor <= RIGHT_HEAVY );

			/* �������� ����� ����� ��� � ����� ����� */
			if( nbal == t->left ) {

				// ��� ������� �� ������ �����
				// ������, ���� ��������������� ����� ������,
				// ����� ����� ���� �� ����������, ������� ������� ���� ������������� �� �����
				if( RIGHT_HEAVY == t->balanceFactor ) {
					t->balanceFactor = BALANCED;
					rebalance = false;
					}
				// ������ �������(-1) �� ����� ����� � ���� P, � �������(+/- 2) �� ������ P 
				else if( BALANCED == t->balanceFactor ) {
					t->balanceFactor = LEFT_HEAVY;
					}
				else {
					if( LEFT_HEAVY == t->left->balanceFactor ) {

						// ������� �������� ���� �����, ������ ��� ����� ��������� 
						// ��������� �������������� ����� ����� ������
						right_rotate_balancing( t );

						// �������
						t = right_rotate( t );
						}
					else {
						CBL_VERIFY( BALANCED == t->left->balanceFactor || RIGHT_HEAVY == t->left->balanceFactor );

						// ������� �������� ���� �����, ������ ��� ����� ��������� 
						// ��������� �������������� ����� ����� ������
						leftright_rotate_balancing( t );

						// ������� �������
						left_rotate( t->left );
						t = right_rotate( t );
						}

					rebalance = false;
					}
				}

			// ������ �����
			else {
				// ��� ������� �� ����� �����
				// ������, ���� ��������������� ������ ������,
				// ����� ����� ���� �� ����������, ������� ������� ���� ������������� �� �����
				if( LEFT_HEAVY == t->balanceFactor ) {
					t->balanceFactor = BALANCED;
					rebalance = false;
					}
				// ������ �������(-1) �� ������ ����� � ���� P, � �������(+/- 2) �� ������ P 
				else if( BALANCED == t->balanceFactor ) {
					t->balanceFactor = RIGHT_HEAVY;
					}
				else {
					if( RIGHT_HEAVY == t->right->balanceFactor ) {

						// ������� �������� ���� �����, ������ ��� ����� ��������� 
						// ��������� �������������� ����� ����� ������
						left_rotate_balancing( t );

						// �������
						t = left_rotate( t );
						}
					else {
						CBL_VERIFY( BALANCED == t->right->balanceFactor || LEFT_HEAVY == t->right->balanceFactor );

						// ������� �������� ���� �����, ������ ��� ����� ��������� 
						// ��������� �������������� ����� ����� ������
						rightleft_rotate_balancing( t );

						// ������� �������
						right_rotate( t->right );
						t = left_rotate( t );
						}

					rebalance = false;
					}
				}

			nbal = t;
			t = t->parent;
			}

		return z;
		}

	void delete_node( Tnode *z ) {

		static const int8_t DELETED_LEFT = -1;
		static const int8_t DELETED_RIGHT = 1;

		Tnode *y = _nil;
		Tnode *x = _nil;

		int8_t deletedBranch = 0;

		// ����, � �������� ���������� ���������� ������������
		Tnode *t = _nil;
		Tnode *deletedNode = _nil;

		CBL_VERIFY( z != _nil );

		/* ����������� ���������� ���� �� ����� Z(����������)
		----------------------------------------------------------*/
		// ����� ������������ �� ����� Z ����
		if( z->left == _nil || z->right == _nil )y = z;
		else y = next( z );

		/* ����������� ���� � ������, � �������� ����� ���������� ����������
		   ������������� ������� �� �����
		   -----------------------------------------------------------------*/
		// ���� � �������� ����� ���������� ���������� ������������
		t = y->parent;

		// � ����� ����� ��������� ��������.
		deletedBranch = 0;
		if( _nil != t ) {
			CBL_VERIFY( y == t->left || y == t->right );
			if( y == t->left )deletedBranch = DELETED_LEFT;
			else deletedBranch = DELETED_RIGHT;
			}

		/* ����, ������� ������ �� ����� Y
		------------------------------------------------------------------*/
		if( y->left != _nil )x = y->left;
		else x = y->right;

		if( _nil != x )
			x->parent = y->parent;

		if( y->parent == _nil )_root = x;
		else {
			if( y == y->parent->left )y->parent->left = x;
			else y->parent->right = x;
			}

		if( y != z ) {
			z->key = y->key;
			z->copy_data( y );
			}

		/*-------------------------------------
			Y - ���� ������� �������� ��������
			---------------------------------------*/
		deletedNode = y;


		/* ������ ����� �� ���� t ��� ���������� ������������
		-------------------------------------------------------------------------*/
		bool rebalanced = true;
		while( _nil != t && rebalanced ) {

			rebalanced = false;

			CBL_VERIFY( DELETED_LEFT == deletedBranch || DELETED_RIGHT == deletedBranch );

			// ��������� �������� �� ����� �����
			if( DELETED_LEFT == deletedBranch ) {

				// ����������� ������, ������ - �����������.
				// �� ����� ����� ����������, ���������� ����������������� ������� ����
				if( LEFT_HEAVY == t->balanceFactor ) {
					t->balanceFactor = BALANCED;
					rebalanced = true;
					}
				// ��� �����������, ������ ������� �������, �� ����� ����� �����
				// �� ����������, ������� ���� �� �������������.
				else if( BALANCED == t->balanceFactor ) {
					t->balanceFactor = RIGHT_HEAVY;
					}
				else {
					if( BALANCED == t->right->balanceFactor ) {

						// ������� �������� ���� �����, ������ ��� ����� ��������� 
						// ��������� �������������� ����� ����� ������
						left_rotate_balancing( t );

						// �������
						t = left_rotate( t );
						}
					else {
						rebalanced = true;

						if( RIGHT_HEAVY == t->right->balanceFactor ) {

							// ������� �������� ���� �����, ������ ��� ����� ��������� 
							// ��������� �������������� ����� ����� ������
							left_rotate_balancing( t );

							// �������
							t = left_rotate( t );
							}
						else {
							CBL_VERIFY( BALANCED == t->right->balanceFactor || LEFT_HEAVY == t->right->balanceFactor );

							// ������� �������� ���� �����, ������ ��� ����� ��������� 
							// ��������� �������������� ����� ����� ������
							rightleft_rotate_balancing( t );

							// �������
							right_rotate( t->right );
							t = left_rotate( t );
							}
						}
					}
				}
			else if( DELETED_RIGHT == deletedBranch ) {

				// ����������� �������, ������ - �����������.
				// �� ����� ����� ����������, ���������� ����������������� ������� ����
				if( RIGHT_HEAVY == t->balanceFactor ) {
					t->balanceFactor = BALANCED;
					rebalanced = true;
					}
				// ��� �����������, ������ ������� ������, �� ����� ����� �����
				// �� ����������, ������� ���� �� �������������.
				else if( BALANCED == t->balanceFactor ) {
					t->balanceFactor = LEFT_HEAVY;
					}
				else {
					if( BALANCED == t->left->balanceFactor ) {

						// ������� �������� ���� �����, ������ ��� ����� ��������� 
						// ��������� �������������� ����� ����� ������
						right_rotate_balancing( t );

						// �������
						t = right_rotate( t );
						}
					else {
						rebalanced = true;

						if( LEFT_HEAVY == t->left->balanceFactor ) {

							// ������� �������� ���� �����, ������ ��� ����� ��������� 
							// ��������� �������������� ����� ����� ������
							right_rotate_balancing( t );

							// �������
							t = right_rotate( t );
							}
						else {
							CBL_VERIFY( BALANCED == t->left->balanceFactor || RIGHT_HEAVY == t->left->balanceFactor );

							// ������� �������� ���� �����, ������ ��� ����� ��������� 
							// ��������� �������������� ����� ����� ������
							leftright_rotate_balancing( t );

							// �������
							left_rotate( t->left );
							t = right_rotate( t );
							}
						}
					}
				}
			else
				THROW_EXC_FWD(nullptr);

			// ������� ����������� ��������� ��������������
			deletedBranch = 0;

			if( rebalanced ) {

				/* ���������� ����������� ����� ��� ���������� ����
				-----------------------------------------------------------------*/
				if( _nil != t->parent ) {
					CBL_VERIFY( _nil != t->parent );
					CBL_VERIFY( t == t->parent->left || t->parent->right );

					if( t == t->parent->left )deletedBranch = DELETED_LEFT;
					else deletedBranch = DELETED_RIGHT;
					}

				/* ����� ����� �� ���� ���
				-----------------------------------------------------------------*/
				t = t->parent;
				}
			}

		_nodeAllocator->destroy( deletedNode );
		--_count;
		}


	void clear_nodes( Tnode *x ) {
		if( 0 == x || x == _nil )return;

		if( x->left != _nil )clear_nodes( x->left );
		x->left = 0;

		if( x->right != _nil )clear_nodes( x->right );
		x->right = 0;

		_nodeAllocator->destroy( x );
		--_count;
		}


	/* ���� �� ����� ��������� ����������� */
	avl_tree_t( const avl_tree_t &o );
	avl_tree_t& operator=(const avl_tree_t &o);

public:
	avl_tree_t( TnodeAllocator *nodeAllocator )
		: BinTreeOp( 0, 0 )
		, _nodeAllocator( nodeAllocator ) {}

	~avl_tree_t() { clear(); }

	Tnode* insert( const Tkey &key ) {
		Tnode *n = insert_node( key );

#ifdef USE_DCF_CHECK
		check_state();
#endif//USE_DCF_CHECK

		return n;
		}

	void remove( const Tkey &key ) {
		Tnode *n = find( key );
		if( _nil != n ) {
			delete_node( n );

#ifdef USE_DCF_CHECK
			check_state();
#endif//USE_DCF_CHECK
			}
		}

	const Tnode* end()const {
		return _nil;
		}

	const Tnode* nil()const {
		return _nil;
		}

	void clear() {
		if( _nil != _root && 0 != _root )
			clear_nodes( _root );

		CBL_VERIFY( 0 == _count );

		// ������ ������� ������ ��������� �� ������������!
		_root = _nil;
		}
	};
}
}


