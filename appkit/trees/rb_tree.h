#pragma once 


#include "../../../core/err/err.h"
#include "./bin_tree_op.h"

namespace crm{
namespace aux{
/**
Tnode ���������:

���� ����������:
Tkey Tnode::key;
char Tnode::color;
Tnode *parent;
Tnode *left;
Tnode *right;

������ ����������:
void copy_data( const Tnode *src );

TnodeAllocator ���������:

������ ����������:
Tnode *allocate();
void destroy( Tnode *n );
*/
template< typename Tkey, typename Tnode, typename TnodeAllocator >
class rb_tree_t : public BinTreeOp<Tkey, Tnode> {
private:
	static const char COLOR_RED = 1;
	static const char COLOR_BLACK = 2;

	TnodeAllocator *_nodeAllocator;

	void left_rotate( Tnode *x ) {

		// ��������� Y
		Tnode *y = x->right;

		// ��������� � ������ ��������� X ����� Y
		x->right = y->left;

		// �������� �������� ������ ��������� Y �� X
		if( _nil != y->left )
			y->left->parent = x;

		// �������� X ���������� ��������� ��� Y
		y->parent = x->parent;

		// �������� ��������� �� �������� X
		// ���� x - ������ ������
		if( _nil == x->parent )
			_root = y;
		else {

			// ���� x - ����� ������� ������ ��������,
			// ��������� ��� ����� �������� Y
			if( x == x->parent->left )
				x->parent->left = y;
			else
				x->parent->right = y;
			}

		// ����������� X � ����� ��������� Y
		y->left = x;
		// �������� ��������� �� �������� � X
		x->parent = y;
		}

	void right_rotate( Tnode *y ) {

		// ��������� X
		Tnode *x = y->left;

		// ��������� ������ ��������� X � ����� ��������� Y
		y->left = x->right;

		// �������� �������� ������� ��������� X �� Y
		if( _nil != x->right )
			x->right->parent = y;

		// �������� Y ���������� ��������� ��� X
		x->parent = y->parent;

		// �������� ��������� �� �������� Y
		if( _nil == y->parent )
			_root = x;
		else {

			// ���� Y - ����� ��������� ������ ��������
			if( y == y->parent->left )
				y->parent->left = x;
			else
				y->parent->right = x;
			}

		// ����������� Y � ������ ��������� X
		x->right = y;
		// �������� ���� �� �������� � Y
		y->parent = x;
		}

	Tnode* insert_node( const Tkey &key ) {

		Tnode *y = _nil;
		Tnode *x = _root;

		/*  ����� �������� ���� ����� ��� ������� ��������
			���� ����������� �� ����� �����
			---------------------------------------------------------*/
		while( _nil != x ) {

			y = x;

			if( key < x->key )x = x->left;
			else if( key > x->key ) x = x->right;

			// ���� ��� ����������, ����� ���� �� �����������, ������������ �� ���������
			// ������� ��������� �� ������������ ����
			else return x;
			}

		/* ������� ����
		--------------------------------------------------------*/
		Tnode *z = _nodeAllocator->allocate();
		z->key = key;


		/* ���������� ���� � ������
		--------------------------------------------------------*/
		// �������� ��� Z
		z->parent = y;

		// �������� � ����� ��� ������ ��������� � ����������� �� �����
		if( _nil == y )
			_root = z;
		else {

			if( z->key < y->key )y->left = z;
			else y->right = z;
			}

		z->left = _nil;
		z->right = _nil;
		z->color = COLOR_RED;

		insert_fixup( z );

		++_count;

		return z;
		}

	void insert_fixup( Tnode *z ) {

		// ����� �������, ���� Z ������������ � ������� ����.
		// �������, ����� ���������� �������� 4
		while( COLOR_RED == z->parent->color ) {

			// ���� ���� Z - ����� ����� ������� Z
			if( z->parent == z->parent->parent->left ) {

				// Y - ���� Z
				Tnode *y = z->parent->parent->right;
				if( COLOR_RED == y->color ) {

					z->parent->color = COLOR_BLACK;
					y->color = COLOR_BLACK;
					z->parent->parent->color = COLOR_RED;

					z = z->parent->parent;
					}
				else {

					// Z - ������ ������� ������ ����
					if( z == z->parent->right ) {

						z = z->parent;
						left_rotate( z );
						}

					z->parent->color = COLOR_BLACK;
					z->parent->parent->color = COLOR_RED;
					right_rotate( z->parent->parent );
					}
				}

			// ���� Z - ������ ����� ������� Z
			else {

				// Y - ���� Z
				Tnode *y = z->parent->parent->left;
				if( COLOR_RED == y->color ) {

					z->parent->color = COLOR_BLACK;
					y->color = COLOR_BLACK;
					z->parent->parent->color = COLOR_RED;

					z = z->parent->parent;
					}
				else {

					if( z == z->parent->left ) {

						z = z->parent;
						right_rotate( z );
						}

					z->parent->color = COLOR_BLACK;
					z->parent->parent->color = COLOR_RED;
					left_rotate( z->parent->parent );
					}
				}
			}

		CBL_VERIFY( _root != _nil );
		_root->color = COLOR_BLACK;
		}

	Tnode* delete_node( Tnode *z ) {

		Tnode *y = _nil;
		Tnode *x = _nil;

		CBL_VERIFY( z != _nil );

		if( z->left == _nil || z->right == _nil )y = z;
		else y = next( z );

		if( y->left != _nil )x = y->left;
		else x = y->right;

		x->parent = y->parent;

		if( y->parent == _nil )_root = x;
		else {
			if( y == y->parent->left )y->parent->left = x;
			else y->parent->right = x;
			}

		if( y != z ) {
			z->key = y->key;
			z->copy_data( y );
			}

		if( COLOR_BLACK == y->color )
			delete_fixup( x );

		return y;
		}

	void delete_fixup( Tnode *x ) {

		while( x != _root && x->color == COLOR_BLACK ) {

			// X - ����� ����� ����
			if( x == x->parent->left ) {

				// W - ���� X
				Tnode *w = x->parent->right;
				if( COLOR_RED == w->color ) {

					w->color = COLOR_BLACK;
					x->parent->color = COLOR_RED;
					left_rotate( x->parent );
					w = x->parent->right;
					}

				if( w->left->color == COLOR_BLACK && w->right->color == COLOR_BLACK ) {

					w->color = COLOR_RED;
					x = x->parent;
					}
				else {
					if( w->right->color == COLOR_BLACK ) {

						w->left->color = COLOR_BLACK;
						w->color = COLOR_RED;
						right_rotate( w );
						w = x->parent->right;
						}

					w->color = x->parent->color;
					x->parent->color = COLOR_BLACK;
					w->right->color = COLOR_BLACK;
					left_rotate( x->parent );
					x = _root;
					}
				}

			// X - ������ ����� ����
			else {

				// W - ���� X
				Tnode *w = x->parent->left;
				if( COLOR_RED == w->color ) {

					w->color = COLOR_BLACK;
					x->parent->color = COLOR_RED;
					right_rotate( x->parent );
					w = x->parent->left;
					}

				if( w->left->color == COLOR_BLACK && w->right->color == COLOR_BLACK ) {

					w->color = COLOR_RED;
					x = x->parent;
					}
				else {
					if( w->left->color == COLOR_BLACK ) {

						w->right->color = COLOR_BLACK;
						w->color = COLOR_RED;
						left_rotate( w );
						w = x->parent->left;
						}

					w->color = x->parent->color;
					x->parent->color = COLOR_BLACK;
					w->left->color = COLOR_BLACK;
					right_rotate( x->parent );
					x = _root;
					}
				}
			}

		x->color = COLOR_BLACK;
		}


	void clear_nodes( Tnode *x ) {
		if( 0 == x || x == _nil )return;

		if( x->left != _nil )clear_nodes( x->left );
		x->left = 0;

		if( x->right != _nil )clear_nodes( x->right );
		x->right = 0;

		_nodeAllocator->destroy( x );
		--_count;
		}

	void check_node_correctness( const Tnode *n, int *blackNodesCnt )const {
		CBL_VERIFY( 0 != n);

		// ����
		if( _nil == n ) {
			(*blackNodesCnt) = 1;
			return;
			}

		int blackNLeft = 0;
		int blackNRight = 0;

		check_node_correctness( n->left, &blackNLeft );
		check_node_correctness( n->right, &blackNRight );

		// � ����� � ������ ����� ������ ���� ����������
		// ����� ������ �����
		if( blackNRight != blackNLeft )THROW_EXC_FWD(nullptr);

		// ����� ����� ������ �������� ������ � ����� �����
		(*blackNodesCnt) = blackNRight;

		if( COLOR_RED == n->color ) {
			if( COLOR_BLACK != n->left->color )THROW_EXC_FWD(nullptr);
			if( COLOR_BLACK != n->right->color )THROW_EXC_FWD(nullptr);
			}
		}


	/* ���� �� ����� ��������� ����������� */
	rb_tree_t( const rb_tree_t &o );
	rb_tree_t& operator=(const rb_tree_t &o);


	void remove( Tnode *n ) {
		CBL_VERIFY( _nil != n && 0 != n );

		Tnode *removedNode = delete_node( n );
		_nodeAllocator->destroy( removedNode );

		--_count;

#ifdef USE_DCF_CHECK
		check_state();
#endif//USE_DCF_CHECK
		}

public:
	rb_tree_t( TnodeAllocator *nodeAllocator )
		: BinTreeOp( 0, 0 )
		, _nodeAllocator( nodeAllocator ) {

		// �������� ������������
		_nil = _nodeAllocator->allocate();

		_nil->color = COLOR_BLACK;
		_nil->left = 0;
		_nil->right = 0;
		_nil->parent = 0;

		// ������ ������� ������ ��������� �� ������������!
		_root = _nil;
		}

	~rb_tree_t() {
		clear();

		// �������� ������������
		if( 0 != _nil ) {
			_nodeAllocator->destroy( _nil );
			_nil = 0;
			}
		}

	Tnode* insert( const Tkey &key ) {
		Tnode *keyNode = insert_node( key );

#ifdef USE_DCF_CHECK
		check_state();
#endif//USE_DCF_CHECK

		return keyNode;
		}

	void remove( const Tkey &key ) {
		Tnode *n = find( key );
		if( _nil != n )remove( n );
		}

	const Tnode* end()const {
		return _nil;
		}

	const Tnode* nil()const {
		return _nil;
		}

	void check_state()const {
#ifdef USE_DCF_CHECK

		int blackNCnt = 0;
		check_node_correctness( _root, &blackNCnt );

#endif//USE_DCF_CHECK
		}

	void clear() {
		if( _nil != _root && 0 != _root )
			clear_nodes( _root );

		CBL_VERIFY( 0 == _count );

		// ������ ������� ������ ��������� �� ������������!
		_root = _nil;
		}
	};
}
}





