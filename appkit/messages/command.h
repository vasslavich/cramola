#pragma once


#include "../channel_terms/message_base_image.h"
#include "./base_terms.h"


namespace crm{


/*! ��������� ������� */
class icommand_t final :
	public i_iol_ihandler_t {
public:
	typedef i_iol_ihandler_t base_t;

public:
	static const std::string& cmd_identity_success()noexcept;
	static const std::string& cmd_initialize_data()noexcept;
	static const std::string& cmd_initialize_success()noexcept;

private:
	global_object_identity_t _targetId;
	global_command_identity_t _cmdId;
	std::string _cmd;
	std::string _tag;

public:
	static iol_type_spcf_t mpf_type_spcf()noexcept;

	icommand_t( )noexcept;

	icommand_t(const icommand_t&) = delete;
	icommand_t& operator=(const icommand_t&) = delete;

	icommand_t( icommand_t&&)noexcept = default;
	icommand_t& operator=( icommand_t&&)noexcept = default;

	const std::string& cmd()const noexcept;
	const std::string& tag()const noexcept;
	const global_command_identity_t& cmd_id()const noexcept;
	const global_object_identity_t& target_id()const noexcept;

	size_t get_serialized_size()const noexcept;

	template<typename Ts>
	void srlz(Ts && s)const{
		THROW_MPF_EXC_FWD(nullptr);
		}

	template<typename Ts>
	void dsrlz(Ts && s){

		// ������������� ������
		srlz::deserialize(std::forward<Ts>(s), _targetId);
		// ������������� �������
		srlz::deserialize(std::forward<Ts>(s), _cmdId);
		/* ��������� ��������� ������� */
		srlz::deserialize(std::forward<Ts>(s), _cmd);
		srlz::deserialize(std::forward<Ts>(s), _tag);
		}
	};

/*! ��������� ������� */
class ocommand_t final : public i_iol_ohandler_t {
public:
	typedef i_iol_ohandler_t base_t;
private:
	global_object_identity_t _targetId;
	global_command_identity_t _cmdId;
	std::string _cmd;
	std::string _tag;

public:
	ocommand_t( )noexcept;

	void set_cmd_id( const global_command_identity_t& id );
	void set_target_id( const global_object_identity_t & id );
	void set_cmd( const std::string &cmd );
	void set_cmd( std::string && cmd );
	void set_tag( const std::string & tag );

	size_t get_serialized_size()const noexcept;

	template<typename S>
	void srlz(S &&dest)const {
		// ������
		srlz::serialize(dest, _targetId);
		// ������������� �������
		srlz::serialize(dest, _cmdId);
		// ��������� ��������� �������
		srlz::serialize(dest, _cmd);
		srlz::serialize(dest, _tag);
	}

	template<typename S>
	void dsrlz(S &&src ) {
		THROW_MPF_EXC_FWD(nullptr);
	}
	};
}
