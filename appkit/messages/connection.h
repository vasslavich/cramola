#pragma once


#include "../channel_terms/message_base_image.h"
#include "./base_terms.h"


namespace crm{


struct iconnection_t  final : public i_iol_ihandler_t{
public:
	typedef i_iol_ihandler_t base_t;

private:
	/*identity_descriptor_t _senderId;
	session_description_t _session;*/

public:
	static iol_type_spcf_t mpf_type_spcf()noexcept;

	iconnection_t( )noexcept;

	iconnection_t(const iconnection_t&) = delete;
	iconnection_t& operator=(const iconnection_t&) = delete;

	iconnection_t(iconnection_t&&)noexcept = default;
	iconnection_t& operator=(iconnection_t&&)noexcept = default;

	/*const session_description_t & session()const;
	const identity_descriptor_t& sender_id()const;*/

	size_t get_serialized_size()const noexcept;

	template<typename Ts>
	void srlz(Ts && s)const{
		THROW_MPF_EXC_FWD(nullptr);
		}

	template<typename Ts>
	void dsrlz(Ts &&s){
		/*srlz::deserialize(std::forward<Ts>(src), _senderId);
		srlz::deserialize(std::forward<Ts>(src), _session);*/
		}
	};


struct oconnection_t  final : public i_iol_ohandler_t{
public:
	typedef i_iol_ohandler_t base_t;

private:
	/*identity_descriptor_t  _senderId;
	session_description_t _session;*/

public:
	oconnection_t(const session_description_t & session );

	/*const session_description_t & session()const;
	const identity_descriptor_t& sender_id()const;*/

	size_t get_serialized_size()const noexcept;

	template<typename Ts>
	void srlz(Ts && )const{
		//srlz::serialize(s, _senderId);
		//srlz::serialize(s, _session);
		}

	template<typename Ts>
	void dsrlz(Ts &&){ THROW_MPF_EXC_FWD(nullptr); }
	};
}

