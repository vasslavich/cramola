#pragma once


#include <optional>
#include "./strob_envelop.h"
#include "../channel_terms/internal_terms.h"
#include "../typeframe/internal_terms.h"
#include "../internal_invariants.h"
#include "../channel_terms/message_base_image.h"


namespace crm {


template<typename _TIMessage,
	typename TIMessage = typename std::decay_t<_TIMessage>,
	typename std::enable_if_t<std::is_base_of_v<i_iol_ihandler_t, TIMessage>, int> = 0>
std::optional<TIMessage> message_forward(std::unique_ptr<i_iol_ihandler_t> && p) {
	if(auto pt = message_cast<TIMessage>(std::move(p))) {
		return std::make_optional(std::move(*pt));
		}
	else {
		return std::nullopt;
		}
	}


namespace detail{

template<typename _TStrobMessage,
	typename TStrobMessage = typename std::decay_t<_TStrobMessage>,
	typename std::enable_if_t<(
		srlz::detail::is_strob_serializable_v<TStrobMessage> && 
		std::is_nothrow_move_constructible_v<TStrobMessage> && 
		std::is_nothrow_move_assignable_v<TStrobMessage>), int> = 0 >
	struct message_forward_result{
	using reverse_exception_guard = i_iol_ihandler_t::reverse_exception_guard;

	TStrobMessage _value;
	std::unique_ptr<i_iol_ihandler_t> _src;

	message_forward_result( std::unique_ptr<istrob_message_envelop_t<TStrobMessage>> && msrc_ )
		noexcept(noexcept(std::move(*msrc_).value<0>()))
		: _value( std::move( *msrc_ ).value<0>() )
		, _src( std::move( msrc_ ) ){}

	void commit()noexcept{
		if( _src ){
			_src->reset_invoke_exception_guard().reset();
			}
		}

	TStrobMessage value() && noexcept{
		return std::move( _value );
		}

	const TStrobMessage& value() const & noexcept{
		return _value;
		}

	const std::unique_ptr<i_iol_ihandler_t>& source()const & noexcept{
		return _src;
		}

	std::unique_ptr<i_iol_ihandler_t> source() && noexcept{
		return std::move( _src );
		}
	};
}

template<typename _TStrobMessage,
	typename TStrobMessage = typename std::decay_t<_TStrobMessage>,
	typename std::enable_if_t<srlz::detail::is_strob_serializable_v<TStrobMessage>, int> = 0>
std::optional<detail::message_forward_result<TStrobMessage>> message_forward(std::unique_ptr<i_iol_ihandler_t> && p)noexcept{
	if(auto pt = message_cast<detail::istrob_message_envelop_t<TStrobMessage>>(std::move(p))) {
		return std::make_optional( detail::message_forward_result<TStrobMessage>(std::move(pt)));
		}
	else {
		return std::nullopt;
		}
	}
}


