#pragma once


#include "../channel_terms/message_base_image.h"
#include "./base_terms.h"


namespace crm {


struct iping_id_t final :
	public i_iol_ihandler_t {
public:
	typedef i_iol_ihandler_t base_t;

public:
	static iol_type_spcf_t mpf_type_spcf()noexcept;

	iping_id_t()noexcept;

	iping_id_t(const iping_id_t&) = delete;
	iping_id_t& operator=(const iping_id_t&) = delete;

	iping_id_t(iping_id_t&&)noexcept = default;
	iping_id_t& operator=(iping_id_t&&)noexcept = default;

	size_t get_serialized_size()const noexcept { return 0; }

	template<typename Ts>
	void srlz(Ts &&)const {}

	template<typename Ts>
	void dsrlz(Ts &&) {}
	};


struct oping_id_t final : public i_iol_ohandler_t {
public:
	typedef i_iol_ohandler_t base_t;

	oping_id_t()noexcept;

	size_t get_serialized_size()const noexcept { return 0; }

	template<typename Ts>
	void srlz(Ts &&)const {}

	template<typename Ts>
	void dsrlz(Ts &&) {}
	};
}
