#pragma once


#include "../channel_terms/message_base_image.h"
#include "./base_terms.h"


namespace crm{


struct notification_keyval_t{
	std::string s;
	int i;
	};

class onotification_t final :
	public i_iol_ohandler_t,
	public srlz::serialized_base_definitions {

private:
	std::list<notification_keyval_t> _keys;
	std::unique_ptr<dcf_exception_t> _ntf;

public:
	typedef i_iol_ohandler_t base_type_t;
	static iol_type_spcf_t mpf_type_spcf()noexcept;

	onotification_t()noexcept;

	onotification_t(const onotification_t&) = delete;
	onotification_t& operator=(const onotification_t&) = delete;

	onotification_t(onotification_t&&)noexcept;
	onotification_t& operator=(onotification_t&&)noexcept;

	template<typename Ts>
	void srlz(Ts && dest)const {
		CBL_VERIFY(!is_sendrecv_request());
		CBL_VERIFY(is_null(source_subscriber_id()));

		srlz::serialize(dest, _keys);

		if (_ntf) {
			dcf_exception_t::serialize(*CHECK_PTR(typedriver()), dest, (*_ntf));
		}
	}

	template<typename Ts>
	void dsrlz(Ts && ) {
		THROW_MPF_EXC_FWD(nullptr);
	}

	void add( int i, std::string s )noexcept;

	void set_ntf( std::unique_ptr<dcf_exception_t> && ntf )noexcept;
	void set_ntf( dcf_exception_t && exc )noexcept;
	};

class inotification_t final :
	public i_iol_ihandler_t,
	public srlz::serialized_base_definitions{

private:
	std::list< notification_keyval_t> _keys;
	std::unique_ptr<dcf_exception_t> _ntf;

public:
	typedef i_iol_ihandler_t base_t;
	static iol_type_spcf_t mpf_type_spcf()noexcept;

	inotification_t()noexcept;
	inotification_t( std::unique_ptr<dcf_exception_t> && )noexcept;

	void set_ntf( std::unique_ptr<dcf_exception_t> && ntf )noexcept;
	void set_ntf( dcf_exception_t && exc )noexcept;

	template<typename Ts>
	void srlz(Ts &&)const { THROW_MPF_EXC_FWD(nullptr); }

	template<typename Ts>
	void dsrlz(Ts && src ) {
		srlz::deserialize(std::forward<Ts>(src), _keys);

		_ntf = dcf_exception_t::deserialize(*CHECK_PTR(typedriver()), std::forward<Ts>(src));
	}

	const std::list< notification_keyval_t>& keys() const noexcept;
	std::optional<std::string> find_by_key( int )const noexcept;
	const std::unique_ptr<dcf_exception_t>& ntf()const noexcept;

	std::unique_ptr<dcf_exception_t> release_ntf()noexcept;


	static std::unique_ptr< inotification_t> make_internal_report(const address_descriptor_t & a,
		std::unique_ptr<crm::dcf_exception_t> &&)noexcept;

	};
}

