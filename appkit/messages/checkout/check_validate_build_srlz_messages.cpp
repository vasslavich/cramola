#include "../../internal.h"


using namespace crm;
using namespace crm::srlz;


namespace {
struct check_validate_build_srlz_messagess {
	auto operator ()() {
		auto ws = wstream_constructor_t::make();

		serialize(ws, identity_value_t{});
		serialize(ws, identity_payload_entry_t{});
		serialize(ws, identity_payload_t{});

		auto rs = rstream_constructor_t::make(ws.release());

		identity_value_t iv;
		identity_payload_entry_t ie;
		identity_payload_t ip;
		deserialize(rs, iv);
		deserialize(rs, ie);
		deserialize(rs, ip);

		auto tmp2 = iv;
		auto tmp3 = ie;
		auto tmp4 = ip;

		auto ws2 = wstream_constructor_t::make();

		serialize(ws, iv);
		serialize(ws, ie);
		serialize(ws, ip);

		return ws2.release();
		}
	};

static_assert(srlz::detail::is_container_of_bytes_v<decltype(std::declval<check_validate_build_srlz_messagess>()())>, __FILE_LINE__);
}
