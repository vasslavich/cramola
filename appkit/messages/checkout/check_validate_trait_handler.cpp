#include "../../internal.h"
#include "../trait_handler.h"

using namespace crm;
using namespace crm::detail;

namespace {
struct as_strob_example { int i; long l; double d; };

struct move_only_1 final {
	std::string a;
	std::vector<char> c;

	move_only_1();
	move_only_1(move_only_1&&) = default;
	move_only_1& operator=(move_only_1&&) = default;

	move_only_1(const move_only_1&) = delete;
	move_only_1& operator=(const move_only_1&) = delete;

	template<typename S>
	void srlz(S&& s);

	template<typename S>
	void dsrlz(S&& s);

	size_t get_serialized_size()const noexcept;
	};

struct copy_only_1 final {
	std::string a;
	std::vector<char> c;

	copy_only_1();
	copy_only_1(copy_only_1&&) = delete;
	copy_only_1& operator=(copy_only_1&&) = delete;

	copy_only_1(const copy_only_1&) = default;
	copy_only_1& operator=(const copy_only_1&) = default;

	template<typename S>
	void srlz(S&& s);

	template<typename S>
	void dsrlz(S&& s);

	size_t get_serialized_size()const noexcept;
	};
}

static_assert(std::is_same_v<
	bool,
	decltype(std::declval< message_handler>().has_result_as< icommand_t>())
>, __FILE_LINE__);

static_assert(std::is_same_v<
	bool,
	decltype(std::declval< message_handler>().has_result_as<as_strob_example, move_only_1, copy_only_1, int, std::string>())
>, __FILE_LINE__);

static_assert(std::is_same_v<
	icommand_t,
	std::decay_t<decltype(std::declval< message_handler>().as<icommand_t>())>
>, __FILE_LINE__);

static_assert(std::is_same_v<
	icommand_t,
	std::decay_t<decltype(std::declval<const message_handler &>().as<icommand_t>())>
>, __FILE_LINE__);

static_assert(std::is_same_v<
	icommand_t,
	std::decay_t<decltype(std::declval< message_handler&&>().as<icommand_t>())>
>, __FILE_LINE__);




static_assert(std::is_same_v <
	std::tuple<std::string, int>&&,
	decltype(std::declval<tuple_envelop_t < std::string, int>&&>().values_tuple())>, __FILE_LINE__);

static_assert(std::is_same_v <
	const std::tuple<std::string, int>&,
	decltype(std::declval<const tuple_envelop_t < std::string, int>&>().values_tuple())>, __FILE_LINE__);



static_assert(std::is_same_v <
	std::tuple<std::string, int>&&,
	decltype(std::declval<istrob_message_envelop_t < std::string, int>&&>().values_tuple())>, __FILE_LINE__);

static_assert(std::is_same_v <
	const std::tuple<std::string, int>&,
	decltype(std::declval<const istrob_message_envelop_t < std::string, int>&>().values_tuple())>, __FILE_LINE__);







static_assert(std::is_same_v<
	std::tuple<as_strob_example, move_only_1, copy_only_1, int, std::string>,
	std::decay_t<decltype(std::declval< message_handler>().as<as_strob_example, move_only_1, copy_only_1, int, std::string>())>
>, __FILE_LINE__);

static_assert(std::is_same_v<
	const std::tuple<as_strob_example, move_only_1, copy_only_1, int, std::string>&,
	decltype(std::declval<const message_handler &>().as<as_strob_example, move_only_1, copy_only_1, int, std::string>())
>, __FILE_LINE__);

static_assert(std::is_same_v<
	std::tuple<as_strob_example, move_only_1, copy_only_1, int, std::string>&&,
	decltype(std::declval< message_handler&&>().as<as_strob_example, move_only_1, copy_only_1, int, std::string>())
>, __FILE_LINE__);



static_assert(srlz::has_static_typeid_trait_v<istrob_message_envelop_t<int, std::string, move_only_1, copy_only_1>>, __FILE_LINE__);
static_assert(srlz::has_static_typeid_trait_v<ostrob_message_envelop_t<int, std::string, move_only_1, copy_only_1>>, __FILE_LINE__);
