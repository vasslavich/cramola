#pragma once


#include "../channel_terms/message_base_image.h"
#include "./base_terms.h"
#include "../tableval/tableval.h"


namespace crm {


class osendrecv_datagram_t final :
	public i_iol_ohandler_t,
	public srlz::serialized_base_definitions {

private:
	std::string _key;
	datagram_t _value;

public:
	typedef i_iol_ohandler_t base_type_t;

	static iol_type_spcf_t mpf_type_spcf()noexcept;

	osendrecv_datagram_t()noexcept;

	osendrecv_datagram_t(const osendrecv_datagram_t&) = delete;
	osendrecv_datagram_t& operator=(const osendrecv_datagram_t&) = delete;

	osendrecv_datagram_t(osendrecv_datagram_t&&)noexcept;
	osendrecv_datagram_t& operator=(osendrecv_datagram_t&&)noexcept;

	osendrecv_datagram_t(datagram_t&& value);

	template<typename S>
	void srlz(S&& dest)const {
		srlz::serialize(dest, _key);
		srlz::serialize(dest, _value);
		}

	template<typename S>
	void dsrlz(S&& src) {
		srlz::deserialize(src, _key);
		srlz::deserialize(src, _value);
		}

	datagram_t& value()noexcept;
	const datagram_t& value()const noexcept;
	void set_key(const std::string& key)noexcept;
	const std::string& key()const noexcept;
	};


class isendrecv_datagram_t final :
	public i_iol_ihandler_t,
	public srlz::serialized_base_definitions {

private:
	std::string _key;
	datagram_t _value;

public:
	typedef i_iol_ihandler_t base_t;
	static iol_type_spcf_t mpf_type_spcf()noexcept;

	isendrecv_datagram_t()noexcept;

	isendrecv_datagram_t(isendrecv_datagram_t&& o)noexcept;
	isendrecv_datagram_t& operator=(isendrecv_datagram_t&& o)noexcept;

	isendrecv_datagram_t(const isendrecv_datagram_t&) = delete;
	isendrecv_datagram_t& operator=(const isendrecv_datagram_t&) = delete;

	template<typename S>
	void srlz(S&& dest)const {
		srlz::serialize(dest, _key);
		srlz::serialize(dest, _value);
		}

	template<typename S>
	void dsrlz(S&& src) {
		srlz::deserialize(src, _key);
		srlz::deserialize(src, _value);
		}

	datagram_t capture()noexcept;
	const datagram_t& value()const noexcept;
	datagram_t value_release()noexcept;
	const std::string& key()const noexcept;
	};
}
