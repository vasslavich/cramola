#pragma once


#include "../channel_terms/message_base_image.h"
#include "./base_terms.h"


namespace crm{


class obinary_vector_t final : public i_iol_ohandler_t {
public:
	typedef i_iol_ohandler_t base_t;

private:
	binary_vector_t _bin;

public:
	obinary_vector_t( binary_vector_t && bin );

	size_t get_serialized_size()const noexcept;

	template<typename S>
	void srlz(S &dest)const {
		FATAL_ERROR_FWD(nullptr);
	}

	template<typename S>
	void dsrlz( S &&src  ) {
		FATAL_ERROR_FWD(nullptr);
	}

	const binary_vector_t& blob()const & noexcept;
	binary_vector_t blob()&& noexcept;
	};
}

