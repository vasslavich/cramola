#pragma once


#include "../channel_terms/message_base_image.h"
#include "./base_terms.h"


namespace crm{

struct idisconnection_t  final : public i_iol_ihandler_t{
public:
	typedef i_iol_ihandler_t base_t;

	static iol_type_spcf_t mpf_type_spcf()noexcept;

	idisconnection_t()noexcept;

	idisconnection_t(const idisconnection_t&) = delete;
	idisconnection_t& operator=(const idisconnection_t&) = delete;

	idisconnection_t(idisconnection_t&&)noexcept = default;
	idisconnection_t& operator=(idisconnection_t&&)noexcept = default;

	size_t get_serialized_size()const noexcept{ return 0; }

	template<typename Ts>
	void srlz(Ts && )const{}

	template<typename Ts>
	void dsrlz(Ts &&){}
	};

struct odisconnection_t  final : public i_iol_ohandler_t{
public:
	typedef i_iol_ohandler_t base_t;

	odisconnection_t()noexcept;

	size_t get_serialized_size()const noexcept{ return 0; }

	template<typename Ts>
	void srlz(Ts && )const{}

	template<typename Ts>
	void dsrlz(Ts &&){}
	};
}
