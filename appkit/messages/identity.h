#pragma once


#include "../channel_terms/message_base_image.h"
#include "./base_terms.h"


namespace crm{


struct iidentity_t final :
	public i_iol_ihandler_t{

private:
	identity_value_t _identity;
	session_description_t _session;

public:
	typedef i_iol_ihandler_t base_t;

	static iol_type_spcf_t mpf_type_spcf()noexcept;

	iidentity_t()noexcept;

	iidentity_t(const iidentity_t&) = delete;
	iidentity_t& operator=(const iidentity_t&) = delete;

	iidentity_t(iidentity_t&&)noexcept = default;
	iidentity_t& operator=(iidentity_t&&)noexcept = default;

	const identity_value_t& identity()const;
	const session_description_t& session()const;

	size_t get_serialized_size()const noexcept;

	template<typename Ts>
	void srlz(Ts && s)const { THROW_MPF_EXC_FWD(nullptr); }

	template<typename Ts>
	void dsrlz(Ts &&s) {
		srlz::deserialize(s, _identity);
		srlz::deserialize(s, _session);
	}
	};


struct oidentity_t final :
	public i_iol_ohandler_t{

private:
	identity_value_t _id;
	session_description_t _session;

public:
	typedef i_iol_ohandler_t base_t;

	oidentity_t()noexcept;
	oidentity_t( identity_value_t && identity,
		const session_description_t & session );

	void set_identity( identity_value_t && identity );
	const identity_value_t& identity()const;
	void set_session( session_description_t && session );
	const session_description_t& session()const;

	size_t get_serialized_size()const noexcept;

	template<typename Ts>
	void srlz(Ts && dest)const {
		srlz::serialize(dest, _id);
		srlz::serialize(dest, _session);
	}

	/*! ��������� \ref crm::srlz::i_srlz_length_eof_t.
	������������ � �������� ����� */
	template<typename Ts>
	void dsrlz(Ts &&) {
		THROW_MPF_EXC_FWD(nullptr);
	}
	};
}
