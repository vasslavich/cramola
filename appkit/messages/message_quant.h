#pragma once


#include "../channel_terms/message_base_image.h"
#include "./base_terms.h"


namespace crm{


class ipacket_t final :
	public i_iol_ihandler_t{
public:
	typedef i_iol_ihandler_t base_t;

private:
	detail::_packet_t _pck;

public:
	static iol_type_spcf_t mpf_type_spcf()noexcept;

	ipacket_t( detail::_packet_t && pck  )noexcept;
	
	ipacket_t(const ipacket_t&) = delete;
	ipacket_t& operator=(const ipacket_t&) = delete;

	ipacket_t(ipacket_t&&)noexcept = default;
	ipacket_t& operator=(ipacket_t&&)noexcept = default;

	const detail::_packet_t& packet()const & noexcept;
	detail::_packet_t packet()&& noexcept;

	size_t get_serialized_size()const noexcept;

	template<typename Ts>
	void srlz(Ts &&)const {
		THROW_MPF_EXC_FWD(nullptr);
	}

	template<typename Ts>
	void dsrlz(Ts && src) {
		THROW_MPF_EXC_FWD(nullptr);
	}
	};
}

