#pragma once

#include "../channel_terms/predecl.h"
#include "../typeframe.h"

namespace crm {

class message_handler {
	std::unique_ptr<i_iol_ihandler_t> _m;
	std::unique_ptr<dcf_exception_t> _e;
	std::chrono::microseconds _timeline;
	async_operation_result_t _st{ async_operation_result_t::st_init };

	[[noreturn]]
	void rethrow()const;

public:
	message_handler()noexcept;

	message_handler(const message_handler&) = delete;
	message_handler& operator=(const message_handler&) = delete;

	message_handler(message_handler&&) = default;
	message_handler& operator=(message_handler&&) = default;

	template<typename TimeRep,
		typename TimeDur>
		message_handler(async_operation_result_t st,
			std::unique_ptr<i_iol_ihandler_t>&& m_,
			std::unique_ptr<dcf_exception_t>&& e_,
			std::chrono::duration<TimeRep, TimeDur> timeline_)noexcept
		: _m(std::move(m_))
		, _e(std::move(e_))
		, _timeline(std::chrono::duration_cast<std::chrono::microseconds>(timeline_))
		, _st(st){}

	bool has_canceled()const noexcept;
	bool has_result()const noexcept;
	bool has_exception()const noexcept;
	const std::unique_ptr<i_iol_ihandler_t>& source_message()const& noexcept;
	std::unique_ptr<i_iol_ihandler_t> source_message() && noexcept;
	const 	std::chrono::microseconds& timeline()const noexcept;
	const std::unique_ptr<dcf_exception_t>& exception()const& noexcept;
	std::unique_ptr<dcf_exception_t> exception() && noexcept;
	async_operation_result_t state()const noexcept;
	void commit_receive()noexcept;

	template<typename _T,
		typename T = std::decay_t<_T>,
		typename std::enable_if_t<std::is_base_of_v<i_iol_ihandler_t, T>, int> = 0>
		bool has_result_as()const noexcept {
		if (nullptr != dynamic_cast<const T*>(_m.get())) {
			CBL_VERIFY(!has_exception());
			return true;
			}
		else {
			return true;
			}
		}

	template<typename ... Ts,
		typename std::enable_if_t<(
			srlz::detail::is_all_srlzd_v<Ts...> &&
			!is_any_message_handler_based_v<Ts...> &&
			(sizeof...(Ts)) >= 1
			), int> = 0>
	bool has_result_as()const noexcept {
		if (_m) {
			/*auto yy = _m->type_id();
			auto xx = detail::ostrob_message_envelop_t<Ts...>::typeid_key();*/

			auto isF = detail::ostrob_message_envelop_t<Ts...>::typeid_key() == _m->type_id();

			CBL_VERIFY(nullptr != dynamic_cast<const detail::istrob_message_envelop_t<Ts...>*>(_m.get()));
			CBL_VERIFY(!has_exception());

			return isF;
			}
		else {
			return false;
			}
		}

	template<typename _T,
		typename T = std::decay_t<_T>,
		typename std::enable_if_t<std::is_base_of_v<i_iol_ihandler_t, T>, int> = 0>
		const T& as()const& {
		if (!has_exception()) {
			if (_m) {
				if (auto pT = dynamic_cast<const T*>(_m.get())) {
					return static_cast<const T&>(*pT);
					}
				else {
					THROW_EXC_FWD(nullptr);
					}
				}
			else {
				THROW_EXC_FWD(nullptr);
				}
			}
		else {
			rethrow();
			}
		}

	template<typename _T,
		typename T = std::decay_t<_T>,
		typename std::enable_if_t<(
			std::is_base_of_v<i_iol_ihandler_t, T>&&
			std::is_move_constructible_v<T>&& std::is_move_assignable_v<T>
			), int> = 0>
		T as()&& {
		if (!has_exception()) {
			if (_m) {
				if (auto pT = dynamic_cast<T*>(_m.get())) {
					return std::move(*pT);
					}
				else {
					THROW_EXC_FWD(nullptr);
					}
				}
			else {
				THROW_EXC_FWD(nullptr);
				}
			}
		else {
			rethrow();
			}
		}

	template<typename ... Ts,
		typename std::enable_if_t<(
			srlz::detail::is_all_srlzd_v<Ts...> &&
			!is_any_message_handler_based_v<Ts...> &&
			(sizeof...(Ts)) >= 1
			), int> = 0>
		decltype(auto) as()const& {
		if (!has_exception()) {
			if (_m) {
				if (detail::ostrob_message_envelop_t<Ts...>::typeid_key() == _m->type_id()) {
					CBL_VERIFY(dynamic_cast<const detail::istrob_message_envelop_t<Ts...>*>(_m.get()));
					auto pM = static_cast<const detail::istrob_message_envelop_t<Ts...>*>(_m.get());
					return (*pM).values_tuple();
					}
				else {
					THROW_EXC_FWD(nullptr);
					}
				}
			else {
				THROW_EXC_FWD(nullptr);
				}
			}
		else {
			rethrow();
			}
		}

	template<typename ... Ts,
		typename std::enable_if_t<(
			srlz::detail::is_all_srlzd_v<Ts...> &&
			!is_any_message_handler_based_v<Ts...> &&
			(sizeof...(Ts)) >= 1
			), int> = 0>
		decltype(auto) as()&& {
		if (!has_exception()) {
			if (_m) {
				//auto yy = _m->type_id();
				//auto xx = detail::istrob_message_envelop_t<Ts...>::typeid_key();

				if (detail::istrob_message_envelop_t<Ts...>::typeid_key() == _m->type_id()) {
					CBL_VERIFY(dynamic_cast<const detail::istrob_message_envelop_t<Ts...>*>(_m.get()));
					auto pM = static_cast<detail::istrob_message_envelop_t<Ts...>*>(_m.get());
					return std::move(*pM).values_tuple();
					}
				else {
					THROW_EXC_FWD(nullptr);
					}
				}
			else {
				THROW_EXC_FWD(nullptr);
				}
			}
		else {
			rethrow();
			}
		}
	};
}
