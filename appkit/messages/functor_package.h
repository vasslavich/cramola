#pragma once


#include "../channel_terms/message_base_image.h"
#include "../typeframe/internal_terms.h"
#include "../channel_terms/internal_terms.h"
#include "../functorapp/parampack.h"
#include "../functorapp/function_meta.h"
#include "../functorapp/functor_trait.h"


namespace crm::detail {

class ofunctor_package_t;


class ifunctor_package_t final : public i_iol_ihandler_t {
	friend ifunctor_package_t transform(ofunctor_package_t&&);

	using base_type = i_iol_ihandler_t;
	using self_type = ifunctor_package_t;

	function_descriptor _fd;
	binary_vector_t _value;

public:
	template<typename S>
	void srlz(S&& dest)const {
		srlz::serialize(dest, _fd);
		srlz::serialize(dest, _value);
		}

	template<typename S>
	void dsrlz(S&& src) {
		srlz::deserialize(src, _fd);
		srlz::deserialize(src, _value);
		}

	size_t get_serialized_size()const noexcept;

public:
	const function_descriptor& descriptor()const noexcept {
		return _fd;
		}

	functor_trait to_handler() && noexcept {
		return functor_trait(std::move(_fd), std::move(_value));
		}

	static iol_type_spcf_t mpf_type_spcf()noexcept {
		return iol_type_spcf_t::build(
			iol_type_spcf_t::make_attrb_pair(iol_type_attributes_t::type, iol_types_t::st_functor_package),
			iol_type_spcf_t::make_attrb_pair(iol_type_attributes_t::length, iol_length_spcf_t::prefixed));
		}

	ifunctor_package_t()
		: base_type(mpf_type_spcf()) {}

	ifunctor_package_t(const self_type&) = delete;
	self_type& operator=(const self_type&) = delete;

	ifunctor_package_t(self_type&& o)noexcept
		: base_type(std::move(o))
		, _fd(std::move(o._fd))
		, _value(std::move(o._value)) {}

	self_type& operator=(self_type&& o)noexcept {
		if (this != std::addressof(o)) {
			base_type::operator=(std::move(o));

			_fd = std::move(o._fd);
			_value = std::move(o._value);
			}

		return (*this);
		}
	};

class ofunctor_package_t final : public i_iol_ohandler_t {
	friend ifunctor_package_t transform(ofunctor_package_t&&);

public:
	using base_type = i_iol_ohandler_t;
	using self_type = typename ofunctor_package_t;

private:
	function_descriptor _fd;
	binary_vector_t _bin;

	static iol_type_spcf_t mpf_type_spcf() {
		return iol_type_spcf_t::build(
			iol_type_spcf_t::make_attrb_pair(iol_type_attributes_t::type, iol_types_t::st_functor_package),
			iol_type_spcf_t::make_attrb_pair(iol_type_attributes_t::length, iol_length_spcf_t::prefixed));
		}

	template<typename Tup, std::size_t ... I>
	void pack(const Tup& tpl, std::index_sequence<I...>) {
		auto ws = srlz::wstream_constructor_t::make();
		auto srlz_item = [&ws](auto&& i) {
			srlz::serialize(ws, i);
			};

		(srlz_item(get<I>(tpl)), ...);

		_bin = ws.release();
		}

public:
	ofunctor_package_t()noexcept
		: base_type(mpf_type_spcf(), global_object_identity_t::null) {}

	ofunctor_package_t(const self_type&) = delete;
	self_type& operator=(const self_type&) = delete;

	ofunctor_package_t(self_type&& o)noexcept
		: base_type(std::move(o))
		, _fd(std::move(o._fd))
		, _bin(std::move(o._bin)) {}

	self_type& operator=(self_type&& o)noexcept {
		if (this != std::addressof(o)) {
			base_type::operator=(std::move(o));

			_fd = std::move(o._fd);
			_bin = std::move(o._bin);
			}

		return (*this);
		}

	template<typename ... AL_>
	ofunctor_package_t(function_descriptor&& fd, AL_&& ... al)
		: base_type(mpf_type_spcf(), global_object_identity_t::null)
		, _fd(std::move(fd)) {

		static_assert(srlz::detail::is_all_srlzd_v<AL_...>, __FILE_LINE__);

		const auto& tpl = srlz::detail::make_tuple_of_references(srlz::detail::tuple_of_references_t<srlz::detail::__tuple_of_references_t::is_const_lvalue>(),
			std::forward<AL_>(al)...);

		pack(tpl, std::index_sequence_for<AL_...>{});
		}


	template<typename S>
	void srlz(S&& dest)const {
		srlz::serialize(dest, _fd);
		srlz::serialize(dest, _bin);
		}

	template<typename S>
	void dsrlz(S&& src) {
		srlz::deserialize(src, _fd);
		srlz::deserialize(src, _bin);
		}

	size_t get_serialized_size()const  noexcept;
	};

template<typename ... AL>
decltype(auto) make_ofunctor_package(function_descriptor&& fd, AL&& ... al) {
	return ofunctor_package_t(std::move(fd), std::forward<AL>(al)...);
	}


ifunctor_package_t transform(ofunctor_package_t&& op);
}

