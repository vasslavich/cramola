#pragma once


#include "../channel_terms/message_base_image.h"
#include "./base_terms.h"


namespace crm{


struct irequest_t final : public i_iol_ihandler_t {
public:
	typedef i_iol_ihandler_t base_t;

	enum class what_t{
		lower_bound,
		identity,
		empty,
		ping,
		upper_bound
		};

private:
	std::string _scode;
	what_t _ucode = what_t::empty;

public:
	static iol_type_spcf_t mpf_type_spcf();

	irequest_t()noexcept;

	irequest_t(const irequest_t&) = delete;
	irequest_t& operator=(const irequest_t&) = delete;

	irequest_t(irequest_t&&)noexcept = default;
	irequest_t& operator=(irequest_t&&)noexcept = default;

	const std::string& scode()const;
	what_t ucode()const;

	size_t get_serialized_size()const noexcept;

	template<typename S>
	void srlz(S &&)const {
		THROW_MPF_EXC_FWD(nullptr);
	}
	template<typename S>
	void dsrlz(S && src) {
		srlz::deserialize(src, _ucode);
		srlz::deserialize(src, _scode);
	}
	};


struct orequest_t final : public i_iol_ohandler_t {
public:
	typedef i_iol_ohandler_t base_t;

private:
	std::string _scode;
	irequest_t::what_t _ucode = irequest_t::what_t::empty;

public:
	orequest_t()noexcept;

	void set_code( const std::string &code );
	void set_code( std::string && code );
	void set_code( const irequest_t::what_t code );
	const std::string& scode()const;
	irequest_t::what_t ucode()const;

	size_t get_serialized_size()const noexcept;

	template<typename S>
	void srlz(S &&dest)const {
		srlz::serialize(dest, _ucode);
		srlz::serialize(dest, _scode);
	}

	template<typename S>
	void dsrlz(S &&src) {
		THROW_MPF_EXC_FWD(nullptr);
	}
	};
}
