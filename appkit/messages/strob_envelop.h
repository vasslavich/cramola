#pragma once


#include "../utilities/complex_traits/tuple_trait.h"
#include "../channel_terms/message_base_image.h"
#include "../typeframe/internal_terms.h"


namespace crm::detail {


template<typename ... AL>
class istrob_message_envelop_t final :
	public /*std::conditional_t<(
		srlz::detail::is_all_srlzd_v<AL...> &&
		(std::is_nothrow_move_constructible_v<AL> && ...) &&
		(std::is_nothrow_move_assignable_v<AL> && ...)
		),*/ i_iol_ihandler_t, detect_fail_self/*, void>*/ {
	static_assert(srlz::detail::is_all_srlzd_v<AL...>, __FILE_LINE__);

	using tuple_type = tuple_envelop_t<std::decay_t<AL>...>;

public:
	static constexpr auto typeid_key()noexcept {
		return srlz::typemap_key_t(iol_typeid_spcf_t::make<tuple_type>());
	}

	iol_type_spcf_t mpf_type_spcf() {
		if constexpr (srlz::detail::is_all_srlzd_prefixed_v<AL...>) {
			return iol_type_spcf_t::build(
				iol_type_spcf_t::make_attrb_pair(iol_type_attributes_t::type, iol_types_t::st_strob_envelop),
				iol_type_spcf_t::make_attrb_pair(iol_type_attributes_t::length, iol_length_spcf_t::prefixed));
			}
		else {
			return iol_type_spcf_t::build(
				iol_type_spcf_t::make_attrb_pair(iol_type_attributes_t::type, iol_types_t::st_strob_envelop),
				iol_type_spcf_t::make_attrb_pair(iol_type_attributes_t::length, iol_length_spcf_t::eof_marked_));
			}
		}

private:
	tuple_type _ts{};

public:
	using base_type = i_iol_ihandler_t;
	using self_type = typename istrob_message_envelop_t<AL...>;

	template<typename S>
	void srlz(S&& dest)const {
		srlz::serialize(std::forward<S>(dest), _ts);
		}

	template<typename S>
	void dsrlz(S&& src) {
		srlz::deserialize(std::forward<S>(src), _ts);
		}

	std::enable_if_t< srlz::detail::is_all_srlzd_prefixed_v<AL...>, size_t> get_serialized_size()const noexcept {
		return srlz::object_trait_serialized_size(_ts);
		}

public:
	template<typename ... XAL,
		typename std::enable_if_t<(
			!srlz::detail::is_has_inherited_from_v<detect_fail_self, XAL...> &&
			!srlz::detail::exists_type_v<self_type, XAL...> &&
			srlz::detail::is_all_srlzd_v<XAL...>
			), int> = 0>
		istrob_message_envelop_t(XAL&& ... al)
		: istrob_message_envelop_t(mpf_type_spcf(), std::forward<XAL>(al)...) {}

	template<typename ... XAL,
		typename std::enable_if_t<(
			!srlz::detail::is_has_inherited_from_v<detect_fail_self, XAL...> &&
			!srlz::detail::exists_type_v<self_type, XAL...> &&
			srlz::detail::is_all_srlzd_v<XAL...>
			), int> = 0>
		istrob_message_envelop_t(iol_type_spcf_t && spcf, XAL&& ... al)
		: base_type(std::move(spcf)/*, typeid_key()**/)
		, _ts(std::forward<XAL>(al)...) {}

	template<const size_t I>
	constexpr decltype(auto) value() && noexcept {
		return std::move(_ts).value<I>();
		}

	template<const size_t I>
	constexpr decltype(auto) value() & noexcept {
		return _ts.value<I>();
		}

	template<const size_t I>
	constexpr decltype(auto) value()const& noexcept {
		return _ts.value<I>();
		}

	constexpr decltype(auto) values_tuple() && noexcept {
		return std::move(_ts).values_tuple();
		}

	constexpr decltype(auto) values_tuple()const& noexcept {
		return _ts.values_tuple();
		}
	};


template<typename ... AL>
decltype(auto) make_imessage_envelop(AL&& ... al) {
	return istrob_message_envelop_t<AL...>(std::forward<AL>(al)...);
	}

template<typename ... AL>
istrob_message_envelop_t(AL&& ...)->istrob_message_envelop_t<AL...>;



template<typename ... AL>
class ostrob_message_envelop_t final :
	public i_iol_ohandler_t, detect_fail_self {

	static_assert(srlz::detail::is_all_srlzd_v<AL...>, __FILE_LINE__);

	using tuple_type = tuple_envelop_t<std::decay_t<AL>...>;

public:
	static constexpr auto typeid_key()noexcept {
		return srlz::typemap_key_t(iol_typeid_spcf_t::make<tuple_type>());
		}

	iol_type_spcf_t mpf_type_spcf() {
		if constexpr (srlz::detail::is_all_srlzd_prefixed_v<AL...>) {
			return iol_type_spcf_t::build(
				iol_type_spcf_t::make_attrb_pair(iol_type_attributes_t::type, iol_types_t::st_strob_envelop),
				iol_type_spcf_t::make_attrb_pair(iol_type_attributes_t::length, iol_length_spcf_t::prefixed));
			}
		else {
			return iol_type_spcf_t::build(
				iol_type_spcf_t::make_attrb_pair(iol_type_attributes_t::type, iol_types_t::st_strob_envelop),
				iol_type_spcf_t::make_attrb_pair(iol_type_attributes_t::length, iol_length_spcf_t::eof_marked_));
			}
		}

private:
	tuple_type _ts{};

public:
	using base_type = i_iol_ohandler_t;
	using self_type = ostrob_message_envelop_t<AL...>;

	template<typename ... XAL,
		typename std::enable_if_t<(
			!srlz::detail::is_has_inherited_from_v<detect_fail_self, XAL...> &&
			!srlz::detail::exists_type_v<self_type, XAL...> &&
			srlz::detail::is_all_srlzd_v<XAL...>
			), int> = 0>
		ostrob_message_envelop_t(XAL&& ... al)
		: ostrob_message_envelop_t(mpf_type_spcf(), /*typeid_key(),*/ std::forward<XAL>(al)...) {}

	template<typename ... XAL,
		typename std::enable_if_t<(
			!srlz::detail::is_has_inherited_from_v<detect_fail_self, XAL...> &&
			!srlz::detail::exists_type_v<self_type, XAL...> &&
			srlz::detail::is_all_srlzd_v<XAL...>
			), int> = 0>
		ostrob_message_envelop_t(iol_type_spcf_t && spcf, /*iol_typeid_spcf_t && typeid_,*/ XAL&& ... al)
		: base_type(std::move(spcf)/*, std::move(typeid_)*/)
		, _ts(std::forward<XAL>(al)...) {}

	template<const size_t I>
	constexpr decltype(auto) value() && noexcept {
		return std::move(_ts).value<I>();
		}

	template<const size_t I>
	constexpr decltype(auto) value() & noexcept {
		return _ts.value<I>();
		}

	template<const size_t I>
	constexpr decltype(auto) value()const& noexcept {
		return _ts.value<I>();
		}

	constexpr decltype(auto) values_tuple() && noexcept {
		return std::move(_ts).values_tuple();
		}

	constexpr decltype(auto) values_tuple()const& noexcept {
		return _ts.values_tuple();
		}

	template<typename S>
	void srlz(S&& dest)const {
		srlz::serialize(std::forward<S>(dest), _ts);
		}

	template<typename S>
	void dsrlz(S&& src) {
		srlz::deserialize(std::forward<S>(src), _ts);
		}

	std::enable_if_t< srlz::detail::is_all_srlzd_prefixed_v<AL...>, size_t> get_serialized_size()const noexcept {
		return srlz::object_trait_serialized_size(_ts);
		}
	};

template<typename ... AL>
decltype(auto) make_omessage_envelop(AL&& ... al) {
	return ostrob_message_envelop_t<AL...>(std::forward<AL>(al)...);
	}

template<typename ... AL>
ostrob_message_envelop_t(AL&& ...)->ostrob_message_envelop_t<AL...>;
}



