#include "../../internal.h"
#include "../ping.h"
#include "../../channel_terms/message_base_image.h"

using namespace crm;
using namespace crm::detail;


static_assert(std::is_default_constructible_v<iping_id_t>, __FILE_LINE__);
static_assert(std::is_base_of_v<i_iol_handler_t, i_iol_ihandler_t>, __FILE_LINE__);
static_assert( std::is_base_of_v<i_iol_handler_t, iping_id_t>, __FILE_LINE__);
static_assert(srlz::detail::is_i_srlzd_v< iping_id_t>, __FILE_LINE__);
static_assert(srlz::is_ctr_as_i_srlzd_v<iping_id_t>, __FILE_LINE__);


iol_type_spcf_t iping_id_t::mpf_type_spcf()noexcept{
	return iol_type_spcf_t::build(
		iol_type_spcf_t::make_attrb_pair( iol_type_attributes_t::type, iol_types_t::st_ping ) );
	}

iping_id_t::iping_id_t()noexcept
	: base_t( mpf_type_spcf()){}

/*void iping_id_t::deserialize(srlz::detail::rstream_base_handler * src){}
void iping_id_t::serialize(srlz::detail::wstream_base_handler * dest)const{}*/


/*==============================================================================
oping_id_t
================================================================================*/


oping_id_t::oping_id_t( )noexcept
	: base_t( iol_type_spcf_t::build(
		iol_type_spcf_t::make_attrb_pair( iol_type_attributes_t::type, iol_types_t::st_ping ),
		iol_type_spcf_t::make_attrb_pair( iol_type_attributes_t::length, iol_length_spcf_t::eof_marked_ ) ) ){}




