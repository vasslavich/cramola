#include "../../internal.h"
#include "../functor_package.h"


using namespace crm;
using namespace crm::detail;


size_t ofunctor_package_t::get_serialized_size()const noexcept {
	return srlz::object_trait_serialized_size(_fd) +
		srlz::object_trait_serialized_size(_bin);
	}

size_t ifunctor_package_t::get_serialized_size()const  noexcept {
	return srlz::object_trait_serialized_size(_fd) +
		srlz::object_trait_serialized_size(_value);
	}

detail::ifunctor_package_t crm::detail::transform(ofunctor_package_t&& op) {
	ifunctor_package_t r;
	r._fd = std::move(op._fd);
	r._value = std::move(op._bin);

	return r;
	}
