#include "../../internal.h"
#include "../binary_vector.h"

using namespace crm;
using namespace crm::detail;


obinary_vector_t::obinary_vector_t( binary_vector_t && bin  )
	: base_t( iol_type_spcf_t::build(
		iol_type_spcf_t::make_attrb_pair( iol_type_attributes_t::type, iol_types_t::st_binary_serialized ),
		iol_type_spcf_t::make_attrb_pair( iol_type_attributes_t::length, iol_length_spcf_t::prefixed ) ) )
	, _bin( std::move( bin ) ){}

size_t obinary_vector_t::get_serialized_size()const noexcept{
	return 0;
	}

const binary_vector_t& obinary_vector_t::blob()const & noexcept{
	return _bin;
	}

binary_vector_t obinary_vector_t::blob()&& noexcept{
	return std::move( _bin );
	}
