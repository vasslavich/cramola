#include "../../internal.h"
#include "../notification.h"


using namespace crm;
using namespace crm::detail;


static_assert(srlz::is_ctr_as_i_srlzd_v<inotification_t>, __FILE_LINE__);
static_assert(srlz::is_serializable_v<inotification_t>, __FILE_LINE__);

static_assert(srlz::detail::is_i_srlzd_base_v<onotification_t>, __FILE_LINE__);
static_assert(srlz::detail::is_i_srlzd_eof_v<onotification_t>, __FILE_LINE__);
static_assert(srlz::detail::is_i_srlzd_v<onotification_t>, __FILE_LINE__);
static_assert(srlz::is_ctr_as_i_srlzd_v<onotification_t>, __FILE_LINE__);
static_assert(srlz::is_serializable_v<onotification_t>, __FILE_LINE__);

static_assert(srlz::detail::is_i_srlzd_eof_v<inotification_t>, __FILE_LINE__);



onotification_t::onotification_t(onotification_t&& o)noexcept
	: base_type_t(std::move(o))
	, _keys(std::move(o._keys))
	, _ntf(std::move(o._ntf)) {}

onotification_t& onotification_t::operator=(onotification_t&& o)noexcept {
	if (this != std::addressof(o)) {
		base_type_t::operator=(std::move(o));

		_keys = std::move(o._keys);
		_ntf = std::move(o._ntf);
		}

	return (*this);
	}

iol_type_spcf_t onotification_t::mpf_type_spcf() noexcept{
	return iol_type_spcf_t::build(
		iol_type_spcf_t::make_attrb_pair( iol_type_attributes_t::type, iol_types_t::st_notification ),
		iol_type_spcf_t::make_attrb_pair( iol_type_attributes_t::length, iol_length_spcf_t::eof_marked_ ) );
	}

onotification_t::onotification_t( )noexcept
	: base_type_t( mpf_type_spcf() ){}

void onotification_t::set_ntf( dcf_exception_t && exc )noexcept{
	set_ntf( exc.dcpy() );
	}

void onotification_t::set_ntf( std::unique_ptr<dcf_exception_t> && ntf )noexcept{
	_ntf = std::move( ntf );
	}

void onotification_t::add( int i, std::string s )noexcept{
	_keys.emplace_back( notification_keyval_t{ std::move( s ), i } );
	}


/*==============================================================================
inotification_t
================================================================================*/


iol_type_spcf_t inotification_t::mpf_type_spcf()noexcept{
	return iol_type_spcf_t::build(
		iol_type_spcf_t::make_attrb_pair(
			iol_type_attributes_t::type, iol_types_t::st_notification ) );
	}

std::unique_ptr< inotification_t> inotification_t::make_internal_report(const address_descriptor_t & a,
	std::unique_ptr<crm::dcf_exception_t> && e)noexcept {

	CBL_VERIFY(!is_null(a.destination_id()));
	CBL_VERIFY(!is_null(a.destination_subscriber_id()));

	auto r = std::make_unique< inotification_t>();
	r->set_address(a);
	r->set_ntf(std::move(e));
	r->setf_report();

	return r;
	}

inotification_t::inotification_t()noexcept
	: base_t( mpf_type_spcf() ){}

inotification_t::inotification_t( std::unique_ptr<dcf_exception_t> && e )noexcept
	: base_t( mpf_type_spcf() )
	, _ntf( std::move( e ) ){}

void inotification_t::set_ntf( dcf_exception_t && exc )noexcept{
	set_ntf( exc.dcpy() );
	}

void inotification_t::set_ntf( std::unique_ptr<dcf_exception_t> && ntf )noexcept{
	_ntf = std::move( ntf );
	}

const std::list<notification_keyval_t> & inotification_t::keys()const noexcept{
	return _keys;
	}

std::optional<std::string> inotification_t::find_by_key( int i )const noexcept{
	auto it = std::find_if( _keys.cbegin(), _keys.cend(), [i]( auto & item ){  return item.i == i; } );
	if( it != _keys.cend() ){
		return std::make_optional( it->s );
		}
	else
		return std::nullopt;
	}

const std::unique_ptr<dcf_exception_t>& inotification_t::ntf()const noexcept{
	return _ntf;
	}

std::unique_ptr<dcf_exception_t> inotification_t::release_ntf()noexcept{
	return std::move( _ntf );
	}



