#include "../../internal.h"
#include "../tableval.h"

using namespace crm;
using namespace crm::detail;


static_assert(std::is_default_constructible_v<isendrecv_datagram_t>, __FILE_LINE__);
static_assert(srlz::detail::is_i_srlzd_eof_v<isendrecv_datagram_t>, __FILE_LINE__);
static_assert(srlz::detail::is_i_srlzd_v<isendrecv_datagram_t>, __FILE_LINE__);
static_assert(srlz::is_ctr_as_i_srlzd_v< isendrecv_datagram_t>, __FILE_LINE__);
static_assert(is_out_message_requirements_v< osendrecv_datagram_t>, __FILE_LINE__);
static_assert(srlz::is_serializable_v< osendrecv_datagram_t>, __FILE_LINE__);


iol_type_spcf_t osendrecv_datagram_t::mpf_type_spcf()noexcept{
	return iol_type_spcf_t::build(
		iol_type_spcf_t::make_attrb_pair( iol_type_attributes_t::type, iol_types_t::st_sendrecv_datagram ),
		iol_type_spcf_t::make_attrb_pair( iol_type_attributes_t::length, iol_length_spcf_t::eof_marked_ ) );
	}

osendrecv_datagram_t::osendrecv_datagram_t()noexcept
	: base_type_t(mpf_type_spcf()) {}

osendrecv_datagram_t::osendrecv_datagram_t(datagram_t&& value)
	: base_type_t(mpf_type_spcf())
	, _value(std::move(value)) {}

osendrecv_datagram_t::osendrecv_datagram_t(osendrecv_datagram_t&& o)noexcept
	: base_type_t(std::move(o))
	, _key(std::move(o._key))
	, _value(std::move(o._value)) {}

osendrecv_datagram_t& osendrecv_datagram_t::operator=(osendrecv_datagram_t&& o)noexcept {
	if (this != std::addressof(o)) {
		base_type_t::operator=(std::move(o));

		_key = std::move(o._key);
		_value = std::move(o._value);
		}

	return (*this);
	}

datagram_t& osendrecv_datagram_t::value()noexcept{
	return _value;
	}

const datagram_t& osendrecv_datagram_t::value()const noexcept{
	return _value;
	}

void osendrecv_datagram_t::set_key( const std::string & key ) noexcept{
	_key = key;
	}

const std::string& osendrecv_datagram_t::key()const noexcept{
	return _key;
	}


/*==============================================================================
isendrecv_datagram_t
================================================================================*/


iol_type_spcf_t isendrecv_datagram_t::mpf_type_spcf()noexcept{
	return iol_type_spcf_t::build(
		iol_type_spcf_t::make_attrb_pair(
			iol_type_attributes_t::type, iol_types_t::st_sendrecv_datagram ) );
	}

isendrecv_datagram_t::isendrecv_datagram_t(isendrecv_datagram_t&& o)noexcept
	: base_t(std::move(o))
	, _key(std::move(o._key))
	, _value(std::move(o._value)) {}

isendrecv_datagram_t& isendrecv_datagram_t::operator=(isendrecv_datagram_t&& o)noexcept {
	if (this != std::addressof(o)) {
		base_t::operator=(std::move(o));

		_key = std::move(o._key);
		_value = std::move(o._value);
		}

	return (*this);
	}

isendrecv_datagram_t::isendrecv_datagram_t()noexcept
	: base_t( mpf_type_spcf() ){}

const datagram_t& isendrecv_datagram_t::value()const noexcept{
	return _value;
	}

datagram_t isendrecv_datagram_t::value_release()noexcept{
	return std::move( _value );
	}

const std::string& isendrecv_datagram_t::key()const noexcept{
	return _key;
	}

datagram_t isendrecv_datagram_t::capture()noexcept{
	return std::move( _value );
	}

