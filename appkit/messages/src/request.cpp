#include "../../internal.h"
#include "../request.h"


using namespace crm;
using namespace crm::detail;


iol_type_spcf_t irequest_t::mpf_type_spcf(){
	return iol_type_spcf_t::build(
		iol_type_spcf_t::make_attrb_pair( iol_type_attributes_t::type, iol_types_t::st_request ) );
	}

irequest_t::irequest_t()noexcept
	: base_t( mpf_type_spcf() ){}

size_t irequest_t::get_serialized_size()const noexcept{
	return srlz::object_trait_serialized_size( _ucode )
		+ srlz::object_trait_serialized_size( _scode );
	}

const std::string& irequest_t::scode()const{
	return _scode;
	}

irequest_t::what_t irequest_t::ucode()const{
	return _ucode;
	}


/*==============================================================================
orequest_t
================================================================================*/


orequest_t::orequest_t()noexcept
	: base_t( iol_type_spcf_t::build(
		iol_type_spcf_t::make_attrb_pair( iol_type_attributes_t::type, iol_types_t::st_request )/*,
		iol_type_spcf_t::make_attrb_pair( iol_type_attributes_t::length, iol_length_spcf_t::eof_marked_ )*/ ) ){}

size_t orequest_t::get_serialized_size()const noexcept{
	return srlz::object_trait_serialized_size( _ucode )
		+ srlz::object_trait_serialized_size( _scode );
	}

void orequest_t::set_code( const std::string &code ){
	_scode = code;
	}

void orequest_t::set_code( std::string && code ){
	_scode = std::move( code );
	}

void orequest_t::set_code( const irequest_t::what_t code ){
	_ucode = code;
	}

const std::string& orequest_t::scode()const{
	return _scode;
	}

irequest_t::what_t orequest_t::ucode()const{
	return _ucode;
	}


