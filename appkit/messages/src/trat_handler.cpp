#include "../../internal.h"
#include "../trait_handler.h"

using namespace crm;
using namespace crm::detail;


message_handler::message_handler()noexcept {}

void message_handler::rethrow()const {
	if (_e) {
		if (auto pE = dynamic_cast<const dcf_exception_t*>(_e.get())) {
			pE->rethrow();
			}
		else {
			THROW_EXC_FWD(_e->msg());
			}
		}
	}

bool message_handler::has_canceled()const noexcept {
	return _st == async_operation_result_t::st_canceled;
	}

bool message_handler::has_result()const noexcept {
	return _m != nullptr;
	}

bool message_handler::has_exception()const noexcept {
	return _e != nullptr;
	}

const std::chrono::microseconds& message_handler::timeline()const noexcept {
	return _timeline;
	}

const std::unique_ptr<dcf_exception_t>& message_handler::exception()const& noexcept {
	return _e;
	}

std::unique_ptr<dcf_exception_t> message_handler::exception() && noexcept {
	return std::move(_e);
	}

async_operation_result_t message_handler::state()const noexcept {
	return _st;
	}

const std::unique_ptr<i_iol_ihandler_t>& message_handler::source_message()const& noexcept {
	return _m;
	}

std::unique_ptr<i_iol_ihandler_t> message_handler::source_message() && noexcept {
	return std::move(_m);
	}

void message_handler::commit_receive()noexcept {
	if (_m) {
		_m->reset_invoke_exception_guard().reset();
		}
	}
