#include "../../internal.h"
#include "../identity.h"
#include <appkit/utilities/utilities.h>
#include "../../typeframe.h"


using namespace crm;
using namespace crm::detail;


static_assert(srlz::is_serializable_v<identity_descriptor_t>, __FILE_LINE__);
static_assert(!srlz::detail::is_strob_serializable_v<identity_descriptor_t>, __FILE_LINE__);
static_assert(!srlz::detail::is_strob_flat_serializable_v<identity_descriptor_t>, __FILE_LINE__);
static_assert(srlz::detail::is_i_srlzd_prefixed_v<identity_descriptor_t>, __FILE_LINE__);
static_assert(std::is_convertible_v<decltype(srlz::object_trait_serialized_size(std::declval<identity_descriptor_t>())), size_t>, __FILE_LINE__);


const std::string identity_descriptor_t::null_name;
const global_object_identity_t identity_descriptor_t::null_id;
const identity_descriptor_t identity_descriptor_t::null;

static_assert(is_has_null_static_value_v< identity_descriptor_t>, __FILE_LINE__);


identity_descriptor_t::identity_descriptor_t()noexcept{}

identity_descriptor_t::identity_descriptor_t(identity_descriptor_id_type id,
	std::string_view name_)
	: _id(std::move(id))
	, _name(name_){
	
	if (_name.size() > max_name_length) {
		THROW_EXC_FWD(nullptr);
		}
	}

const identity_descriptor_id_type& identity_descriptor_t::id()const noexcept {
	return _id;
	}

const std::string& identity_descriptor_t::name()const noexcept {
	return _name;
	}

std::string identity_descriptor_t::to_str()const{
	return _id.to_str() + ":" + _name;
	}

std::wstring identity_descriptor_t::to_wstr()const{
	return cvt<wchar_t>( to_str() );
	}

bool identity_descriptor_t::is_null()const noexcept{
	return (*this) == null;
	}

std::size_t identity_descriptor_t::get_serialized_size()const noexcept {
	return srlz::object_trait_serialized_size(_id) +
		srlz::object_trait_serialized_size(_name, srlz::detail::option_max_serialized_size<max_name_length>{});
	}

bool crm::operator<( const identity_descriptor_t & l, const identity_descriptor_t  & r )noexcept{
	if( l.id() < r.id() )
		return true;
	else if( l.id() > r.id() )
		return false;
	else
		return l.name() < r.name();
	}

bool crm::operator>( const identity_descriptor_t & l, const identity_descriptor_t  & r )noexcept{
	return !(l < r) && !(l == r);
	}

bool crm::operator>=( const identity_descriptor_t & l, const identity_descriptor_t  & r )noexcept{
	return l > r || l == r;
	}

bool crm::operator==( const identity_descriptor_t & l, const identity_descriptor_t  & r )noexcept{
	return l.id() == r.id() && l.name() == r.name();
	}

bool crm::operator!=( const identity_descriptor_t & l, const identity_descriptor_t  & r )noexcept{
	return !(l == r);
	}

void crm::apply_at_hash( address_hash_t & h, const identity_descriptor_t & d )noexcept{
	apply_at_hash( h, d.id() );
	apply_at_hash( h, d.name() );
	}


std::ostream  & crm::operator <<( std::ostream & os, const identity_descriptor_t & uuid ){
	os << uuid.id() << ":" << uuid.name();
	return os;
	}

std::wostream  & crm::operator <<( std::wostream & os, const identity_descriptor_t & uuid ){
	os << uuid.id()<< L":" << cvt<wchar_t>( uuid.name() );
	return os;
	}


static_assert(srlz::is_serializable_v<identity_payload_entry_t>, __FILE_LINE__);
static_assert(std::is_aggregate_v< identity_payload_entry_t>, __FILE_LINE__);
static_assert(srlz::detail::is_aggregate_initializable_v<identity_payload_entry_t>, __FILE_LINE__);
static_assert(srlz::detail::__fields_count_hepler_t<identity_payload_entry_t>::count == 2, __FILE_LINE__);
static_assert(srlz::detail::fields_count_v<identity_payload_entry_t> == 2, __FILE_LINE__);
static_assert(std::is_class_v<identity_payload_t>, __FILE_LINE__);


/*==============================================================================
identity_payload_t
================================================================================*/


static const std::unique_ptr<identity_payload_entry_t> null_entry_ptr;

static_assert(srlz::is_serializable_v< identity_payload_t>, __FILE_LINE__);

std::unique_ptr<identity_payload_entry_t> identity_payload_t::find_by_key( 
	const std::string & key )const noexcept{
	
	for( const auto & item : collection ){
		if( item.key == key )
			return std::make_unique<std::decay_t<decltype(item)>>( item );
		}

	return {};
	}

void identity_payload_t::add( const std::string & key, binary_vector_t && data ){
	collection.emplace_back(identity_payload_entry_t{ key, std::move(data) });
	}

void identity_payload_t::add( identity_payload_entry_t && value ){
	collection.push_back( std::move( value ) );
	}

/*==============================================================================
identity_value_t
================================================================================*/

static_assert(srlz::is_serializable_v< identity_value_t>, __FILE_LINE__);


/*==============================================================================
iidentity_t
================================================================================*/


iol_type_spcf_t iidentity_t::mpf_type_spcf()noexcept{
	return iol_type_spcf_t::build(
		iol_type_spcf_t::make_attrb_pair( iol_type_attributes_t::type, iol_types_t::st_identity ) );
	}

iidentity_t::iidentity_t()noexcept
	: base_t( mpf_type_spcf() ){}

size_t iidentity_t::get_serialized_size()const noexcept{
	return srlz::object_trait_serialized_size( _identity )
		+ srlz::object_trait_serialized_size( _session );
	}

const session_description_t& iidentity_t::session()const{
	return _session;
	}

const identity_value_t& iidentity_t::identity()const{
	return _identity;
	}


/*==============================================================================
oidentity_t
================================================================================*/


oidentity_t::oidentity_t( )noexcept
	:base_t( iol_type_spcf_t::build(
		iol_type_spcf_t::make_attrb_pair( iol_type_attributes_t::type, iol_types_t::st_identity ),
		iol_type_spcf_t::make_attrb_pair( iol_type_attributes_t::length, iol_length_spcf_t::eof_marked_ ) ) ){}

oidentity_t::oidentity_t( identity_value_t && identity,
	const session_description_t & session_  )
	: base_t( iol_type_spcf_t::build(
		iol_type_spcf_t::make_attrb_pair( iol_type_attributes_t::type, iol_types_t::st_identity ),
		iol_type_spcf_t::make_attrb_pair( iol_type_attributes_t::length, iol_length_spcf_t::eof_marked_ ) ) )
	, _id( std::move( identity ) )
	, _session( session_ ){}

void oidentity_t::set_identity( identity_value_t && identity ){
	_id = std::move( identity );
	}

const identity_value_t& oidentity_t::identity()const{
	return _id;
	}

void oidentity_t::set_session( session_description_t && s ){
	_session = std::move( s );
	}

const session_description_t& oidentity_t::session()const{
	return _session;
	}

size_t oidentity_t::get_serialized_size()const noexcept{
	return srlz::object_trait_serialized_size( _id )
		+ srlz::object_trait_serialized_size( _session );
	}


message_spin_tag_type crm::make_message_spin_tag_by_value(uint64_t val)noexcept {
#ifdef CBL_MESSAGE_ADDRESS_TYPES_SHORT
	return sx_loc_64::make( val );
#else
	return sx_uuid_t{ uint64_t(0), val };
#endif
	}
