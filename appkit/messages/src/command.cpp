#include "../../internal.h"
#include "../command.h"

using namespace crm;
using namespace crm::detail;


static const std::string _cmd_identity_success( "[crm::mpf]in-identity-ok[crm::mpf]" );
const std::string& icommand_t::cmd_identity_success()noexcept{
	return _cmd_identity_success;
	}

static const std::string _cmd_initialize_data( "[crm::mpf]initialize-data[crm::mpf]" );
const std::string& icommand_t::cmd_initialize_data()noexcept{
	return _cmd_initialize_data;
	}

static const std::string _cmd_initialize_success( "[crm::mpf]initialize-ok[crm::mpf]" );
const std::string& icommand_t::cmd_initialize_success()noexcept{
	return _cmd_initialize_success;
	}


iol_type_spcf_t icommand_t::mpf_type_spcf()noexcept{
	return iol_type_spcf_t::build(
		iol_type_spcf_t::make_attrb_pair( iol_type_attributes_t::type, iol_types_t::st_command ) );
	}

icommand_t::icommand_t()noexcept
	: base_t( mpf_type_spcf() ){}


size_t icommand_t::get_serialized_size()const noexcept{
	return srlz::object_trait_serialized_size( _targetId )
		+ srlz::object_trait_serialized_size( _cmdId )
		+ srlz::object_trait_serialized_size( _cmd )
		+ srlz::object_trait_serialized_size( _tag );
	}

const global_object_identity_t& icommand_t::target_id()const noexcept{
	return _targetId;
	}

const global_command_identity_t& icommand_t::cmd_id()const noexcept{
	return _cmdId;
	}

const std::string& icommand_t::cmd()const noexcept{
	return _cmd;
	}

const std::string& icommand_t::tag()const noexcept{
	return _tag;
	}


/*===================================================================================
ocommand_t
=====================================================================================*/


ocommand_t::ocommand_t()noexcept
	: base_t( iol_type_spcf_t::build(
		iol_type_spcf_t::make_attrb_pair( iol_type_attributes_t::type, iol_types_t::st_command ),
		iol_type_spcf_t::make_attrb_pair( iol_type_attributes_t::length, iol_length_spcf_t::eof_marked_ ) ) ){}

size_t ocommand_t::get_serialized_size()const noexcept{
	return srlz::object_trait_serialized_size( _targetId )
		+ srlz::object_trait_serialized_size( _cmdId )
		+ srlz::object_trait_serialized_size( _cmd )
		+ srlz::object_trait_serialized_size( _tag );
	}

void ocommand_t::set_target_id( const global_object_identity_t & id ){
	_targetId = id;
	}

void ocommand_t::set_cmd( const std::string &cmd ){
	_cmd = cmd;
	}

void ocommand_t::set_cmd( std::string && cmd ){
	_cmd = std::move( cmd );
	}

void ocommand_t::set_tag( const std::string & tag ){
	_tag = tag;
	}

void ocommand_t::set_cmd_id( const global_command_identity_t& id ){
	_cmdId = id;
	}

