#include "../../internal.h"
#include "../disconnection.h"

using namespace crm;
using namespace crm::detail;


iol_type_spcf_t idisconnection_t::mpf_type_spcf()noexcept{
	return iol_type_spcf_t::build(
		iol_type_spcf_t::make_attrb_pair( iol_type_attributes_t::type, iol_types_t::st_disconnection ) );
	}

idisconnection_t::idisconnection_t( )noexcept
	: base_t( mpf_type_spcf() ){}

/*==============================================================================
odisconnection_t
================================================================================*/


odisconnection_t::odisconnection_t()noexcept
	: base_t( iol_type_spcf_t::build(
		iol_type_spcf_t::make_attrb_pair( iol_type_attributes_t::type, iol_types_t::st_disconnection ),
		iol_type_spcf_t::make_attrb_pair( iol_type_attributes_t::length, iol_length_spcf_t::eof_marked_ ) ) ){}
