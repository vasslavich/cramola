#include "../../internal.h"
#include "../connection.h"

using namespace crm;
using namespace crm::detail;


iol_type_spcf_t iconnection_t::mpf_type_spcf()noexcept{
	return iol_type_spcf_t::build(
		iol_type_spcf_t::make_attrb_pair( iol_type_attributes_t::type, iol_types_t::st_connection ) );
	}

iconnection_t::iconnection_t()noexcept
	: base_t(  mpf_type_spcf() ){}

size_t iconnection_t::get_serialized_size()const noexcept {
	/*return srlz::object_trait_serialized_size( _senderId )
		+ srlz::object_trait_serialized_size( _session );*/
	return 0;
	}

/*const session_description_t & iconnection_t::session()const{
	return _session;
	}

const identity_descriptor_t& iconnection_t::sender_id()const{
	return _senderId;
	}*/


/*==============================================================================
oconnection_t
================================================================================*/


oconnection_t::oconnection_t( const session_description_t & session_ )
	: base_t( iol_type_spcf_t::build(
		iol_type_spcf_t::make_attrb_pair( iol_type_attributes_t::type, iol_types_t::st_connection ),
		iol_type_spcf_t::make_attrb_pair( iol_type_attributes_t::length, iol_length_spcf_t::eof_marked_ ) ) ){
	
	set_session(session_);
	}

size_t oconnection_t::get_serialized_size()const noexcept{
	/*return srlz::object_trait_serialized_size( _senderId )
		+ srlz::object_trait_serialized_size( _session );*/
	return 0;
	}

/*const session_description_t & oconnection_t::session()const{
	return _session;
	}

const identity_descriptor_t& oconnection_t::sender_id()const{
	return _senderId;
	}*/




