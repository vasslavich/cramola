#include "../../internal.h"
#include "../message_quant.h"


using namespace crm;
using namespace crm::detail;


iol_type_spcf_t ipacket_t::mpf_type_spcf()noexcept{
	return iol_type_spcf_t::build(
		iol_type_spcf_t::make_attrb_pair( iol_type_attributes_t::type, iol_types_t::st_packet ) );
	}

ipacket_t::ipacket_t( _packet_t && pck )noexcept
	: base_t( mpf_type_spcf() )
	, _pck( std::move( pck ) ){

	set_address( _pck.address() );
	}

const _packet_t& ipacket_t::packet()const & noexcept{
	return _pck;
	}

_packet_t ipacket_t::packet()&& noexcept{
	return std::move( _pck );
	}

size_t ipacket_t:: get_serialized_size()const noexcept{
	return 0;
	}

