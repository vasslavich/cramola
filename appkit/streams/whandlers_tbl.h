#pragma once


#include <map>
#include <mutex>
#include "./i_streams.h"


namespace crm{
namespace detail{


class istreams_whandlers_table_t{
private:
	using wstream_make_f = std::function<std::unique_ptr<i_stream_whandler_t>(std::unique_ptr<i_stream_zyamba_identity_t>&&,
		std::unique_ptr<i_stream_zyamba_cover_data_t>&&)>;
	using whandler_push_f = std::function<void(const stream_source_descriptor_t & h, std::shared_ptr<i_stream_whandler_t>&&)>;

	typedef std::mutex lck_t;
	typedef std::unique_lock<lck_t> lckscp_t;

private:
	struct _impl_t{
		std::map<srlz::typemap_key_t, wstream_make_f> _streamCtrs;
		std::map<stream_source_descriptor_t, std::weak_ptr<i_stream_whandler_t>> _streams;
		mutable lck_t _lck;
		};

	std::unique_ptr<_impl_t> _pImpl;
	std::weak_ptr<distributed_ctx_t> _ctx;
	whandler_push_f _whpush;

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	utility::__xobject_counter_logger_t<istreams_whandlers_table_t> _xDbgCounter;
#endif

	std::shared_ptr<i_stream_whandler_t> __select_writer( std::unique_ptr<i_stream_zyamba_identity_t> && h,
		std::unique_ptr<i_stream_zyamba_cover_data_t>&& cd);

	void __return_writer( const stream_source_descriptor_t & h )noexcept;

	wstream_make_f __find_maker( const srlz::typemap_key_t & );

	template<typename TWStreamMaker,
		typename std::enable_if_t< is_write_stream_typemaker_v<TWStreamMaker>, int> = 0>
	void __add_wstream_type_maker(TWStreamMaker && makef) {

		if (auto c = _ctx.lock()) {
			auto key = makef.stream_typeid();

			auto itCtr = CHECK_PTR(_pImpl)->_streamCtrs.find(key);
			if (itCtr == CHECK_PTR(_pImpl)->_streamCtrs.end()) {

				if (!CHECK_PTR(_pImpl)->_streamCtrs.insert(std::make_pair(std::move(key), std::forward<TWStreamMaker>(makef))).second) {
					THROW_MPF_EXC_FWD(nullptr);
					}
				}
			else {
				THROW_MPF_EXC_FWD(nullptr);
				}
			}
		}

	//friend bool operator < ( const std::unique_ptr<i_stream_zyamba_header_t> & lval, const std::unique_ptr<i_stream_zyamba_header_t> &rval );
	//friend bool operator >= ( const std::unique_ptr<i_stream_zyamba_header_t> & lval, const std::unique_ptr<i_stream_zyamba_header_t> &rval );

public:
	istreams_whandlers_table_t( std::shared_ptr<distributed_ctx_t> && ctx, 
		whandler_push_f && whpush );

	istreams_whandlers_table_t( const istreams_whandlers_table_t & ) = delete;
	istreams_whandlers_table_t& operator=( const istreams_whandlers_table_t & ) = delete;

	istreams_whandlers_table_t( istreams_whandlers_table_t && o )noexcept;
	istreams_whandlers_table_t& operator=( istreams_whandlers_table_t && o )noexcept;

	wstream_make_f find_maker( const i_stream_zyamba_identity_t& );

	template<typename TWStreamMaker,
		typename std::enable_if_t< is_write_stream_typemaker_v<TWStreamMaker>, int> = 0>
	void add_wstream_type_maker(TWStreamMaker&& makef) {

		lckscp_t lck(CHECK_PTR(_pImpl)->_lck);
		__add_wstream_type_maker(std::forward<TWStreamMaker>(makef));
		}

	size_t streams_count()const noexcept;

	std::shared_ptr<i_stream_whandler_t> pop_stream_writer( std::unique_ptr<i_stream_zyamba_identity_t> && h,
		std::unique_ptr<i_stream_zyamba_cover_data_t>&& cd);

	void push_stream_writer( const stream_source_descriptor_t & d )noexcept;
	};
}
}

