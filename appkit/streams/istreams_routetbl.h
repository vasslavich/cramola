#pragma once


#include <map>
#include <set>
#include "../base_predecl.h"
#include "./i_stream_events.h"
#include "./i_streams.h"
#include "../xpeer/i_rt1_xpeer.h"
#include "../channel_terms/internal_terms.h"
#include "./streams_tpdrv.h"
#include "./messages.h"


namespace crm::detail{


class istreams_set_t {
private:
	typedef std::mutex lck_t;
	typedef std::unique_lock<lck_t> lckscp_t;

	std::weak_ptr<distributed_ctx_t> _ctx;
	std::multimap<stream_source_descriptor_t, std::weak_ptr<istream_t>> _tbl;
	mutable lck_t _lck;

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	mutable crm::utility::__xvalue_counter_logger_t<10> __sizeTraceer = __FILE_LINE__;
	crm::utility::__xobject_counter_logger_t<istreams_set_t> _xDbgCounter;

	void __size_trace()const noexcept;
#endif

	//size_t __count( const i_stream_header_properties_t & sprop )const;
	bool __insert( std::shared_ptr<istream_t> s );
	void __close( std::function<void( std::list<std::shared_ptr<istream_t>> && s )> && closedf )noexcept;
	void __close()noexcept;

public:
	istreams_set_t( std::weak_ptr<distributed_ctx_t> ctx );
	~istreams_set_t();

	std::shared_ptr<istream_t> find( const rdm_sessional_descriptor_t & sid )const noexcept;
	//size_t count( const i_stream_header_properties_t & sprop )const;
	//std::list<std::shared_ptr<istream_t>> find( const i_stream_header_properties_t & sprop )const;

	bool insert( std::shared_ptr<istream_t> s );
	std::list<std::shared_ptr<istream_t>> erase( const rdm_sessional_descriptor_t & sid )noexcept;

	void close( std::function<void( std::list<std::shared_ptr<istream_t>> && s )> && closedf )noexcept;
	void close()noexcept;
	};


class stream_route_itable_t : public std::enable_shared_from_this<stream_route_itable_t> {
public:
	using stream_key_type = local_object_id_t;

private:
	typedef stream_route_itable_t self_t;
	typedef std::mutex lck_t;
	typedef std::unique_lock<lck_t> lckscp_t;

	stream_types_driver_t _stpDrv;
	std::map<stream_key_type, std::shared_ptr<istreams_set_t>> _sl;
	mutable lck_t _slck;
	std::weak_ptr<distributed_ctx_t> _ctx;

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	mutable crm::utility::__xvalue_counter_logger_t<> __sizeTraceer = __FILE_LINE__;
	crm::utility::__xobject_counter_logger_t<stream_route_itable_t> _xDbgCounter;

	void __size_trace()const noexcept;
#endif

	std::shared_ptr<distributed_ctx_t> ctx()const noexcept;

	void ntf_hndl( std::unique_ptr<dcf_exception_t> && ntf )noexcept;

	void event_handler_invoke(__event_handler_point_f && eh, 
		std::unique_ptr<i_event_t> && value )const noexcept;

	void __close( const stream_key_type & ioDesc,
		const rdm_sessional_descriptor_t & sid,
		stream_close_reason_t r)noexcept;

	void __close_locked( const stream_key_type & ioDesc,
		const rdm_sessional_descriptor_t & sid,
		std::unique_ptr<dcf_exception_t> && exc )noexcept;

	void __push_dispatch( const stream_key_type & ioDesc,
		std::unique_ptr<i_istream_line_t> && h,
		xpeer_push_handler_pin && io,
		__event_handler_point_f && eh );

	void __push_header( const stream_key_type & ioDesc,
		std::unique_ptr<stream_iheader_t> && h,
		xpeer_push_handler_pin && io,
		__event_handler_point_f && eh );

	void __push_tail( const stream_key_type & ioDesc,
		std::unique_ptr<stream_itail_t> && h,
		xpeer_push_handler_pin && io );

	void __push_eof( const stream_key_type & ioDesc,
		std::unique_ptr<stream_iend_t> && h,
		xpeer_push_handler_pin && io );

	void __push_disconnection( const stream_key_type & ioDesc,
		std::unique_ptr<stream_idisconnection_t> && h,
		xpeer_push_handler_pin && io );

	void __handle_extern_exception( const xpeer_push_handler_pin & io,
		std::unique_ptr<i_istream_line_t> && hi,
		std::unique_ptr<dcf_exception_t> && exc ) noexcept;

	void __close( const stream_key_type & ioDesc )noexcept;
	void __close_async( std::shared_ptr<istream_t> && s,
		stream_close_reason_t r,
		std::unique_ptr<dcf_exception_t> && exc )const noexcept;

	std::shared_ptr<istreams_set_t> __get_stream_set( const stream_key_type ioDesc );

	void __make_stream_end( const std::shared_ptr<istream_t> &s,
		std::shared_ptr<i_stream_whandler_t> && w,
		std::unique_ptr<stream_iheader_t> && h,
		const xpeer_push_handler_pin & io );

	void __make_stream( const stream_key_type & ioDesc,
		std::unique_ptr<stream_iheader_t> && h,
		xpeer_push_handler_pin && io,
		__event_handler_point_f && eh );

	void __tail_stream( const stream_key_type & ioDesc, 
		std::unique_ptr<stream_itail_t> && s,
		xpeer_push_handler_pin && io );

	void __eof_response( const xpeer_push_handler_pin & io,
		const std::unique_ptr<stream_iend_t> & e );

	void __eof_stream( const stream_key_type & ioDesc,
		std::unique_ptr<stream_iend_t> && s,
		xpeer_push_handler_pin && io);

	void __disconnection_stream( const stream_key_type & ioDesc,
		std::unique_ptr<stream_idisconnection_t> && s,
		xpeer_push_handler_pin && io );

	void __response_exception( const xpeer_push_handler_pin & io,
		std::unique_ptr<i_istream_line_t> && hi,
		std::unique_ptr<dcf_exception_t> && exc )noexcept;

	template<typename M,
		typename std::enable_if_t <is_out_message_requirements_v<M>, int> = 0>
	static void push2handler(const xpeer_push_handler_pin & io, M && m) {
		if(io.p->peer_level() == rtl_level_t::l1) {
			if(auto pxio = dynamic_cast<xpeer_push_handler_l1*>(io.p.get())) {
				pxio->push(std::forward<M>(m));
				}
			else {
				FATAL_ERROR_FWD(nullptr);
				}
			}
		else if(io.p->peer_level() == rtl_level_t::l0) {
			if(auto pxio = dynamic_cast<xpeer_push_handler_l0*>(io.p.get())) {
				pxio->push(std::forward<M>(m));
				}
			else {
				FATAL_ERROR_FWD(nullptr);
				}
			}
		else {
			FATAL_ERROR_FWD(nullptr);
			}
		}

	template<typename O,
		typename std::enable_if_t<is_out_message_requirements_v<O>, int> = 0>
	void __response( const xpeer_push_handler_pin & io,
		O && value )noexcept {

		try {
			push2handler( io, std::forward<O>(value));
			}
		catch (const dcf_exception_t & exc) {
			ntf_hndl(exc.dcpy());
			}
		catch (const std::exception & exc1) {
			ntf_hndl(CREATE_PTR_EXC_FWD(exc1));
			}
		catch (...) {
			ntf_hndl(CREATE_PTR_EXC_FWD(nullptr));
			}
		}

	void close_async( std::shared_ptr<istream_t> s,
		stream_close_reason_t r,
		std::unique_ptr<dcf_exception_t> && exc )const noexcept;

	template<typename TAction, typename TMessage, typename ... TArgs>
	void invoke_extern(xpeer_push_handler_pin & io,
		std::unique_ptr<TMessage> && hi,
		typename TAction && f, 
		typename TArgs && ... args )noexcept {
		
		static_assert(std::is_base_of_v<i_istream_line_t, TMessage>, __FILE__);

		try {
			f( std::move(hi), std::forward<TArgs>( args )... );
			CBL_VERIFY(hi);
			}
		catch( const dcf_exception_t& exc1 ) {
			CBL_VERIFY(hi);
			__handle_extern_exception( io, message_cast<i_istream_line_t>(std::move(hi)), exc1.dcpy() );
			}
		catch( const std::exception & exc2 ) {
			CBL_VERIFY(hi);
			__handle_extern_exception( io, message_cast<i_istream_line_t>(std::move(hi)), CREATE_EXTERN_CODE_PTR_EXC_FWD( exc2 ) );
			}
		catch( ... ) {
			CBL_VERIFY(hi);
			__handle_extern_exception( io, message_cast<i_istream_line_t>(std::move(hi)), CREATE_EXTERN_CODE_PTR_EXC_FWD(nullptr) );
			}
		}

	void __close()noexcept;

public:
	stream_route_itable_t( std::weak_ptr<distributed_ctx_t> ctx );
	~stream_route_itable_t();

	void close()noexcept;

	void push( const stream_key_type & ioDesc,
		std::unique_ptr<i_istream_line_t> && h,
		xpeer_push_handler_pin && io,
		__event_handler_point_f && eh );

	void close( const stream_key_type & ioDesc)noexcept;
	};
}

