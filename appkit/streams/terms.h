#pragma once


#include "./i_stream_events.h"
#include "./i_streams.h"
#include "./predefined_stream_identity.h"
#include "./stream_reader_base.h"
#include "./xcontainer.h"
#include "./xfile.h"
#include "./handler_at_keys.h"
#include "./ostream.h"