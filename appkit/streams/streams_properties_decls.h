#pragma once


#include <type_traits>
#include "../typeframe/internal_terms.h"
#include "../channel_terms/base_terms.h"
#include "./streams_types_predecls.h"


namespace crm{


template<typename T, typename = std::void_t<>>
struct has_member_class_type_id : std::false_type {};

template<typename T, typename = std::void_t<>>
struct is_stream_identity_trait : std::false_type {};

template<typename T, typename = std::void_t<>>
struct is_stream_cover_data_trait : std::false_type {};

template<typename TWStreamMaker, typename = std::void_t<>>
struct is_write_stream_typemaker : std::false_type {};

template<typename TR, typename = std::void_t<>>
struct has_member_stream_typeid : std::false_type {};

template<typename T, typename = std::void_t<>>
struct has_member_header_params : std::false_type {};


template<typename TR, typename = std::void_t<>>
struct is_stream_reader_from_members : std::false_type {};

template<typename TR, typename = std::void_t<>>
struct is_stream_reader : std::false_type {};

template<typename S, typename = std::void_t<>>
struct is_stream_slot_based_by_interface : std::false_type{};

template<typename S, typename = std::void_t<>>
struct is_stream_slot_based_by_members : std::false_type {};

template<typename S, typename = std::void_t<>>
struct has_type_declaration_stream_params : std::false_type{};
}
