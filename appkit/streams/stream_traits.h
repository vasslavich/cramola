#pragma once


#include <type_traits>
#include <memory>
#include <functional>
#include "../base_predecl.h"
#include "./i_streams.h"
#include "../channel_terms/base_terms.h"
#include "../channel_terms/i_endpoint.h"


namespace crm::detail {


template<typename T, typename Enable = void>
struct trait_upload_ostream_t : public std::false_type {};


template<typename T, typename Enable = void>
struct is_xpeer_trait_t : public std::false_type {};


template<typename T>
struct is_xpeer_trait_methods_t {
	typedef typename std::decay_t<decltype(std::declval<T>().renewable_connection())> renewable_connection_result_t;
	typedef typename std::decay_t<decltype(std::declval<T>().remote_object_endpoint())> remote_object_endpoint_result_t;
	typedef typename std::decay_t<decltype(std::declval<T>().ctx())> ctx_result_t;
	};

template<typename T>
struct is_xpeer_trait_t<T,
	typename std::enable_if<
	(std::is_same<bool, typename is_xpeer_trait_methods_t<T>::renewable_connection_result_t>::value)
	&& (std::is_base_of<i_endpoint_object_t, typename is_xpeer_trait_methods_t<T>::remote_object_endpoint_result_t>::value)
	&& (std::is_same<std::shared_ptr<distributed_ctx_t>, typename is_xpeer_trait_methods_t<T>::ctx_result_t>::value)
>::type>
	: std::true_type {};


template<typename T>
struct trait_upload_ostream_t < T, std::void_t<
	typename T::stream_identity_type,
	typename T::stream_cover_data_type,
	typename T::stream_params_type,
	std::enable_if_t<has_member_stream_typeid_v<T>>,
	std::enable_if_t<is_stream_identity_trait_v<typename T::stream_identity_type>>,
	std::enable_if_t<is_stream_cover_data_trait_v<typename T::stream_cover_data_type>>,
	std::enable_if_t<std::is_same_v<typename T::stream_identity_type, std::decay_t<decltype(std::declval<const T>().make_identity())>>>,
	std::enable_if_t<std::is_same_v<typename T::stream_cover_data_type, std::decay_t<decltype(std::declval<T>().make_cover_data())>>>,
	std::enable_if_t<std::is_same_v<stream_source_descriptor_t, std::decay_t<decltype(std::declval<const T>().source_id())>>>
	>>
	: std::true_type{};

template<typename T>
constexpr bool trait_upload_ostream_v = trait_upload_ostream_t<T>::value;



	template<typename IBase>
	class stream_xtype_t final {
	private:
		srlz::typemap_key_t _typeID;
		std::vector<uint8_t> _objectBin;

	public:
		template<typename S>
		void srlz(S && dest)const {
			srlz::serialize(dest, _typeID);
			srlz::serialize(dest, _objectBin);
		}

		template<typename S>
		void dsrlz(S && src) {
			srlz::deserialize(src, _typeID);
			srlz::deserialize(src, _objectBin);
		}

		size_t get_serialized_size()const noexcept {
			return srlz::object_trait_serialized_size(_typeID)
				+ srlz::object_trait_serialized_size(_objectBin);
		}

		template<typename TObject,
			typename std::enable_if_t < srlz::is_serializable_v<TObject>, int> = 0 >
		static std::vector<uint8_t> serialize(const srlz::typemap_key_t &,
			typename TObject && v, const std::shared_ptr<distributed_ctx_t> &) {

			auto s(srlz::wstream_constructor_t::make());
			srlz::serialize(s, std::forward<TObject>(v));

			return s.release();
		}

		template<typename _TOut, typename _TIn,
			typename TOut = typename std::decay_t<_TOut>,
			typename TIn = typename std::decay_t<_TIn>,
			const typename std::enable_if_t<std::is_base_of_v<TOut, TIn> || std::is_base_of_v<TIn, TOut>, int> = 0>
		static std::unique_ptr<TOut> move_cast(std::unique_ptr<TIn> && pd)noexcept {
			auto pb = std::unique_ptr<TOut>(dynamic_cast<TOut*>(pd.get()));
			pd.release();
			return pb;
		}

	public:
		stream_xtype_t() noexcept{}

		template<typename TObject,
			typename std::enable_if<(
				std::is_base_of_v<IBase, std::decay_t<TObject>> && 
				srlz::is_serializable_v<TObject>
				), int>::type = 0>
		stream_xtype_t( TObject && v, const std::shared_ptr<distributed_ctx_t> & ctx)
			: _typeID(srlz::typemap_key_t::make<TObject>())
			, _objectBin(serialize(_typeID, std::forward<TObject>(v), ctx)) {}

		std::unique_ptr<IBase> deserialize(const std::shared_ptr<distributed_ctx_t> & ctx)const {

			auto pTypeCtr(CHECK_PTR(ctx)->typemap().find(_typeID));
			if (!pTypeCtr) {
				FATAL_ERROR_FWD(nullptr);
			}

			auto s = srlz::rstream_constructor_t::make(_objectBin.cbegin(), _objectBin.cend());

			if (auto pSM = std::dynamic_pointer_cast<srlz::i_type_ctr_i_srlzd_t<IBase>>(pTypeCtr)) {
				static_assert(std::is_base_of_v<IBase, std::decay_t<decltype(*(pSM->create())->pObj)>>, __FILE_LINE__);

				auto dcp = pSM->create();
				dcp->decode(s);			

				return move_cast<IBase, decltype(*dcp->pObj)>(std::move(dcp->pObj));

			} /*else if (auto pST = std::dynamic_pointer_cast<srlz::i_type_ctr_serialized_trait_t>(pTypeCtr)) {
				static_assert(std::is_base_of_v<srlz::serialized_base_r, IBase>, __FILE_LINE__);
				static_assert(std::is_base_of_v<srlz::serialized_base_r, std::decay_t<decltype(*(pST->create()))>>, __FILE_LINE__);

				auto pSrlzBase = pST->create();
				pSrlzBase->deserialize(s);

				return move_cast<IBase, decltype(*pSrlzBase)>(std::move(pSrlzBase));
			}*/ else {
				FATAL_ERROR_FWD(nullptr);
			}
		}

		const srlz::i_typeid_t& object_typeid()const {
			return _typeID;
		}

		const std::vector<uint8_t>& object_binary()const {
			return _objectBin;
		}
	};


	template<typename RemoteExceptionHandler>
	constexpr bool is_remove_exception_handler_v = std::is_invocable_r_v<bool, RemoteExceptionHandler, std::add_const_t<std::add_lvalue_reference_t<std::unique_ptr<dcf_exception_t>>>>;
}
