#pragma once


#include <filesystem>
#include "./i_stream_events.h"
#include "./i_streams.h"
#include "./header_desc_base.h"


namespace crm{


struct stream_file_descriptor_t final{
	std::wstring file_extension;
	std::wstring file_name;
	};


struct xstream_file_t{
	virtual ~xstream_file_t() = 0;

	using params_type = stream_file_descriptor_t;
	using stream_identity_type = typename stream_blob_identity_t<params_type>;

	static const srlz::typemap_key_t stream_blob_typeid;
	};

class rstream_file_adapter_t final :
	public i_stream_reader_t,
	public xstream_file_t{

private:
	std::filesystem::path _path;
	size_t _size = 0;
	stream_source_descriptor_t _sourceId;

	void initialize();

public:
	rstream_file_adapter_t()noexcept;

	rstream_file_adapter_t(const rstream_file_adapter_t&) = delete;
	rstream_file_adapter_t& operator=(const rstream_file_adapter_t&) = delete;

	rstream_file_adapter_t(rstream_file_adapter_t&&)noexcept = default;
	rstream_file_adapter_t& operator=(rstream_file_adapter_t&&)noexcept = default;

	template<typename _TPath,
		typename TXPath = typename std::decay_t<_TPath>,
		typename std::enable_if_t<(!std::is_same<rstream_file_adapter_t, TXPath>::value && std::is_constructible<std::filesystem::path, TXPath>::value)> * = 0>
		rstream_file_adapter_t(const std::weak_ptr<distributed_ctx_t>& ctx, _TPath && val )
		: _path( std::forward<_TPath>( val ) )
		, _sourceId(CHECK_PTR(ctx)->make_global_object_identity_128()){

		initialize();
		}

	const srlz::typemap_key_t& stream_typeid()const noexcept;
	params_type get_stream_params()const;

	/*stream_sequence_position_t size()const noexcept final;*/
	void read(const stream_sequence_position_t& offset, size_t blockSize, std::vector<uint8_t>& v)final;
	bool eof(const stream_sequence_position_t& offset)const noexcept final;
	};


template<typename _TPath,
	typename TXPath = typename std::decay_t<_TPath>,
	typename std::enable_if_t<(std::is_constructible<std::filesystem::path, TXPath>::value)> * = 0>
auto make_rstream_file_adapter(const std::weak_ptr<distributed_ctx_t>& ctx, _TPath && val ){
	return rstream_file_adapter_t( ctx, std::forward<TXPath>( val ) );
	}


class wstream_file_t final:
	public i_stream_writer_t,
	public xstream_file_t{
private:
	std::filesystem::path _path;
	std::filesystem::path _extension;
	std::filesystem::path _fileName;
	std::weak_ptr<distributed_ctx_t> _ctx;
	std::unique_ptr<i_stream_zyamba_identity_t> _si;
	std::unique_ptr<i_stream_zyamba_cover_data_t> _scd;
	bool _keepLocation = false;

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	utility::__xobject_counter_logger_t<wstream_file_t, 1> _xDbgCounter;
#endif

	stream_sequence_position_t next_offset()const noexcept final;
	stream_sequence_position_t write( const stream_sequence_position_t & offset, std::vector<uint8_t> && v )final;
	void close( detail::stream_close_reason_t rc )noexcept final;
	const srlz::typemap_key_t& stream_typeid()const noexcept final;

	void clear()noexcept;

public:
	~wstream_file_t();

	wstream_file_t( const wstream_file_t & ) = delete;
	wstream_file_t& operator=( const wstream_file_t & ) = delete;

	template<typename _TPath,
		typename std::enable_if_t<(!std::is_same<wstream_file_t, std::decay_t<_TPath>>::value && std::is_constructible_v<std::filesystem::path, _TPath>)> * = 0>
		wstream_file_t(std::weak_ptr<distributed_ctx_t> ctx,
			std::unique_ptr<i_stream_zyamba_identity_t>&& id,
			std::unique_ptr<i_stream_zyamba_cover_data_t>&& cd,
			_TPath&& val)
		: _path(std::forward<_TPath>(val))
		, _ctx(ctx)
		, _si(std::move(id))
		, _scd(std::move(cd)) {

		if( auto phBlb = dynamic_cast<const stream_identity_type*>(_si.get()) ){
			_extension = phBlb->params().file_extension;
			_fileName = phBlb->params().file_name;
			}
		else{
			TYPES_MISMATCHING_THROW_EXC_FWD(nullptr);
			}
		}

	/*const std::unique_ptr<i_stream_zyamba_identity_t>& stream_identity()const& noexcept final;
	const std::unique_ptr<i_stream_zyamba_cover_data_t>& stream_cover_data()const& noexcept final;

	std::unique_ptr<i_stream_zyamba_identity_t> stream_identity() && noexcept final;
	std::unique_ptr<i_stream_zyamba_cover_data_t> stream_cover_data() && noexcept final;*/

	const std::filesystem::path& path()const;
	void set_keep_location();

	static const srlz::typemap_key_t& class_type_id()noexcept {
		return stream_blob_typeid;
		}
	};
}


