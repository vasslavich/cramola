#pragma once


#include "./i_streams.h"
#include "./handler_at_keys.h"


namespace crm {
namespace detail {


template<typename TStreamWriter,
	typename = std::enable_if_t<is_stream_slot_based_by_interface_v<TStreamWriter>>>
struct custom_stream_maker_as_interface_t {
	using base_type = typename std::decay_t<TStreamWriter>;

	static srlz::typemap_key_t class_type_id()noexcept {
		return base_type::class_type_id();
		}

	std::weak_ptr<distributed_ctx_t> _c;
	custom_stream_maker_as_interface_t(std::weak_ptr<distributed_ctx_t> c)noexcept
		: _c(c) {}

	auto stream_typeid()const noexcept {
		return TStreamWriter::class_type_id();
		}

	auto operator()(std::unique_ptr<i_stream_zyamba_identity_t>&& h,
		std::unique_ptr<i_stream_zyamba_cover_data_t>&& cd) const noexcept {

		return wstream_handler_blob_t::make(_c.lock(),
			std::make_unique<TStreamWriter>(std::move(h), std::move(cd)));
		}
	};


template<typename _S,
	typename S = typename std::decay_t<_S>,
	typename = std::enable_if_t<is_stream_slot_based_by_members_v<S>>>
struct custom_stream_slot_to_inerface : i_stream_writer_t {
	using base_type = typename std::decay_t<S>;

	S _p;

	static srlz::typemap_key_t class_type_id()noexcept {
		return base_type::class_type_id();
		}

	custom_stream_slot_to_inerface(std::unique_ptr<i_stream_zyamba_identity_t>&& h,
		std::unique_ptr<i_stream_zyamba_cover_data_t>&& cd)noexcept
		: _p(std::move(h), std::move(cd)) {}

	template<typename _X, typename X = typename std::decay_t<_X> >
	std::enable_if_t<(
		is_stream_slot_based_by_members_v<X>&&
		std::is_same_v<S, X>
		), const X&> as()const& noexcept {
		return _p;
		}

	template<typename _X, typename X = typename std::decay_t<_X> >
	std::enable_if_t<(
		is_stream_slot_based_by_members_v<X>&&
		std::is_same_v<S, X>
		), X&&> as() && noexcept {
		return std::move(_p);
		}

	decltype(auto) stream_typeid()const noexcept final {
		return _p.stream_typeid();
		}

	decltype(auto) next_offset()const noexcept  final {
		return _p.next_offset();
		}

	decltype(auto) write(const stream_sequence_position_t& offset, std::vector<uint8_t>&& v) final {
		return _p.write(offset, std::move(v));
		}

	void close(detail::stream_close_reason_t rc)noexcept final {
		_p.close(rc);
		}

	/*decltype(auto) stream_identity()const& noexcept  final {
		return _p.stream_identity();
		}

	decltype(auto) stream_cover_data()const& noexcept final {
		return _p.stream_cover_data();
		}

	decltype(auto) stream_identity() && noexcept final {
		return _p.stream_identity();
		}

	decltype(auto) stream_cover_data() && noexcept final {
		return _p.stream_cover_data();
		}*/
	};

template<typename S,
	typename = std::enable_if_t<is_stream_slot_based_by_members_v<S>>>
	struct custom_stream_maker_as_members_t {
	std::weak_ptr<distributed_ctx_t> _c;

	custom_stream_maker_as_members_t(std::weak_ptr<distributed_ctx_t> c)noexcept
		: _c(c) {}

	auto stream_typeid()const noexcept {
		return S::class_type_id();
		}

	auto operator()(std::unique_ptr<i_stream_zyamba_identity_t>&& h,
		std::unique_ptr<i_stream_zyamba_cover_data_t>&& cd) const noexcept {

		return wstream_handler_blob_t::make(_c.lock(),
			std::make_unique<custom_stream_slot_to_inerface<S>>(std::move(h), std::move(cd)));
		}
	};

template<typename _TWriteSlot,
	typename TWriteSlot = std::decay_t<_TWriteSlot>>
decltype(auto) writer_as(const i_stream_writer_t& w) {

	/* custom stream object inherited from interface */
	if constexpr (is_stream_slot_based_by_interface_v<TWriteSlot>) {
		if constexpr (has_member_class_type_id_v<TWriteSlot>) {
			if (TWriteSlot::class_type_id() == w.stream_typeid()) {
				CBL_VERIFY(static_cast<const TWriteSlot*>(std::addressof(w)) 
					== dynamic_cast<const TWriteSlot*>(std::addressof(w)));

				return static_cast<const TWriteSlot&>(w);
				}
			else {
				THROW_EXC_FWD(nullptr);
				}
			}
		else {
			return dynamic_cast<const TWriteSlot&>(w);
			}
		}

	/* custom stream object from its members */
	else if constexpr (is_stream_slot_based_by_members_v<TWriteSlot>) {
		if constexpr (has_member_class_type_id_v<TWriteSlot>) {
			using wrapper_type = typename custom_stream_slot_to_inerface<TWriteSlot>;
			CBL_VERIFY(wrapper_type::class_type_id() == TWriteSlot::class_type_id());

			if (TWriteSlot::class_type_id() == w.stream_typeid()) {
				CBL_VERIFY(static_cast<const wrapper_type*>(std::addressof(w)) 
					== dynamic_cast<const wrapper_type*>(std::addressof(w)));
				
				return static_cast<const wrapper_type&>(w).as<TWriteSlot>();
				}
			else {
				THROW_EXC_FWD(nullptr);
				}
			}
		else {
			return dynamic_cast<const wrapper_type&>(w).as<TWriteSlot>();
			}
		}
	else {
		static_assert(fault_instantiaion_detect<TWriteSlot>::value, __FILE_LINE__);
		}
	}

template<typename _TWriteSlot,
	typename TWriteSlot = std::decay_t<_TWriteSlot>>
decltype(auto) writer_as(i_stream_writer_t&& w) {

	/* custom stream object inherited from interface */
	if constexpr (is_stream_slot_based_by_interface_v<TWriteSlot>) {
		if constexpr (has_member_class_type_id_v<TWriteSlot>) {
			if (TWriteSlot::class_type_id() == w.stream_typeid())
				return static_cast<TWriteSlot&&>(w);
			else {
				THROW_EXC_FWD(nullptr);
				}
			}
		else {
			return dynamic_cast<TWriteSlot&&>(w);
			}
		}

	/* custom stream object from its members */
	else if constexpr (is_stream_slot_based_by_members_v<TWriteSlot>) {
		using wrapper_type = custom_stream_slot_to_inerface<TWriteSlot>;

		if constexpr (has_member_class_type_id_v<TWriteSlot>) {
			if (TWriteSlot::class_type_id() == w.stream_typeid()) {
				return static_cast<wrapper_type&&>(w).as<TWriteSlot>();
				}
			else {
				THROW_EXC_FWD(nullptr);
				}
			}
		else {
			return dynamic_cast<wrapper_type&&>(w).as<TWriteSlot>();
			}
		}
	else {
		static_assert(fault_instantiaion_detect<TWriteSlot>::value, __FILE_LINE__);
		}
	}

template<typename _TWriteSlot,
	typename TWriteSlot = std::decay_t<_TWriteSlot>>
bool is_writer_as(const i_stream_writer_t& w)noexcept {

	/* custom stream object inherited from interface */
	if constexpr (is_stream_slot_based_by_interface_v<TWriteSlot>) {
		if constexpr (has_member_class_type_id_v<TWriteSlot>) {
			bool hasEq = TWriteSlot::class_type_id() == w.stream_typeid();
			if (hasEq) {
				CBL_VERIFY(dynamic_cast<const TWriteSlot*>(std::addressof(w)));
				CBL_VERIFY(static_cast<const TWriteSlot*>(std::addressof(w))
					== dynamic_cast<const TWriteSlot*>(std::addressof(w)));
				}

			return hasEq;
			}
		else {
			return nullptr != dynamic_cast<const TWriteSlot*>(std::addressof(w));
			}
		}

	/* custom stream object from its members */
	else if constexpr (is_stream_slot_based_by_members_v<TWriteSlot>) {
		using wrapper_type = custom_stream_slot_to_inerface<TWriteSlot>;

		if constexpr (has_member_class_type_id_v<TWriteSlot>) {
			bool hasEq = wrapper_type::class_type_id() == w.stream_typeid();
			if (hasEq) {
				CBL_VERIFY(dynamic_cast<const wrapper_type*>(std::addressof(w)));
				CBL_VERIFY(static_cast<const wrapper_type*>(std::addressof(w))
					== dynamic_cast<const wrapper_type*>(std::addressof(w)));
				}

			return hasEq;
			}
		else {
			return nullptr != dynamic_cast<const wrapper_type*>(std::addressof(w));
			}
		}
	else {
		return false;
		}
	}
}
}




