//#pragma once
//
//
//#include "./terms.h"
//#include "./i_streams.h"
//#include "../channel_terms/predecl.h"
//#include "../channel_terms/message_base_image.h"
//#include "../counters/internal_terms.h"
//
//
//namespace crm {
//
//
//class stream_reader_base_t : public i_stream_reader_t {
//	stream_source_descriptor_t _sourceId;
//	std::shared_ptr<i_stream_reader_adapter_t> _adpt;
//
//#ifdef CBL_OBJECTS_COUNTER_CHECKER
//	utility::__xobject_counter_logger_t<stream_reader_base_t, 1> _xDbgCounter;
//#endif
//
//public:
//	stream_reader_base_t();
//
//	stream_reader_base_t(const std::weak_ptr<distributed_ctx_t> & ctx,
//		std::shared_ptr<i_stream_reader_adapter_t> adpt);
//
//	template<typename XAdapter,
//		typename TXAdapter = std::decay_t<XAdapter>,
//		typename std::enable_if_t<(std::is_base_of<i_stream_reader_adapter_t, TXAdapter>::value)
//		&& (!std::is_base_of<stream_reader_base_t, TXAdapter>::value)> * = 0>
//		stream_reader_base_t(const std::weak_ptr<distributed_ctx_t> & ctx, XAdapter && adpt)
//		: stream_reader_base_t(ctx, std::make_shared<TXAdapter>(std::forward<XAdapter>(adpt))) {}
//
//	stream_sequence_position_t size()const noexcept final;
//	const stream_source_descriptor_t& source_id()const noexcept final;
//
//	stream_header_descriptor_t get_header_descriptor()const final;
//	void read(const stream_sequence_position_t & offset, size_t blockSize, std::vector<uint8_t> & v)final;
//	bool eof(const stream_sequence_position_t & offset)const noexcept final;
//	const srlz::typemap_key_t& stream_typeid()const noexcept final;
//	};
//}

