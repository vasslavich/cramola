#pragma once


#include <map>
#include "../notifications/internal_terms.h"
#include "../channel_terms/internal_terms.h"
#include "./i_stream_events.h"
#include "./i_streams.h"


namespace crm::detail{


class stream_types_driver_t {
public:
	typedef std::function<std::unique_ptr<i_stream_whandler_t>()> handler_ctr_t;

private:
	typedef std::mutex lck_t;
	typedef std::lock_guard<lck_t> lck_spc_t;

	std::map<srlz::typemap_key_t, handler_ctr_t>  _tpMap;
	lck_t _lck;

public:
	template<typename TTypeID,
		typename std::enable_if<std::is_base_of<srlz::i_typeid_t, TTypeID>::value, int>::type = 0>
		void add( const TTypeID & tid, handler_ctr_t && ctr ) {
		
		srlz::detail::_typemap_key_t ktid( tid );
		
		lck_spc_t lck( _lck );
		
		auto it = _tpMap.find( ktid );
		if( it == _tpMap.end() ) {
			if( !_tpMap.insert( std::make_pair( std::move( ktid ), std::move( ctr ) ) ).second ) {
				FATAL_ERROR_FWD(nullptr);
				}
			}
		else {
			FATAL_ERROR_FWD(nullptr);
			}
		}

	template<typename TTypeID,
		typename std::enable_if<std::is_base_of<srlz::i_typeid_t, TTypeID>::value, int>::type = 0>
		std::unique_ptr<i_stream_whandler_t> make( const TTypeID & id )const {

		srlz::detail::_typemap_key_t ktid( tid );

		lck_spc_t lck( _lck );

		auto it = _tpMap.find( ktid );
		if( it != _tpMap.end() ) {
			return (*it).second();
			}
		else {
			FATAL_ERROR_FWD(nullptr);
			}
		}
	};
}

