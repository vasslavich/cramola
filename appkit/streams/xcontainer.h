#pragma once


#include "./i_streams.h"
#include "./i_stream_events.h"
#include "./header_desc_base.h"
#include "./handler_at_keys.h"


namespace crm {


struct stream_container_params_t final {
	stream_sequence_position_t count_of_elements;
	};

template<typename TContainer>
struct xstream_container_t {
	virtual ~xstream_container_t() {}

	using params_type = stream_container_params_t;
	using stream_identity_type = typename stream_blob_identity_t<params_type>;

	static const srlz::typemap_key_t stream_blob_typeid;
	};

template<typename TContainer>
const srlz::typemap_key_t xstream_container_t<TContainer>::stream_blob_typeid = crm::srlz::typemap_key_t::make<xstream_container_t<TContainer>>();


namespace detail {


template<typename TContainer,
	typename = std::enable_if_t<srlz::detail::is_container_of_bytes_v<TContainer>>>
class xcontainter_stream_reader_adapter final :
	public i_stream_reader_t,
	public xstream_container_t<TContainer> {
	private:
		using xcontainer_t = typename TContainer;

		xcontainer_t _xcontaier;

	public:
		/*stream_sequence_position_t size()const noexcept {
			stream_sequence_position_t value;
			value.u64_2[0] = _xcontaier.size();

			return value;
			}*/

		stream_container_params_t get_stream_params()const {
			stream_container_params_t d;
			d.count_of_elements.at<size_t>(0) = _xcontaier.size();

			return d;
			}

		void read(const stream_sequence_position_t& offset, size_t blockSize, std::vector<uint8_t>& v) {
			const auto rsize((std::min)(blockSize, _xcontaier.size() - offset.u64_2[0]));
			v.resize(rsize);

			std::copy(_xcontaier.begin() + offset.u64_2[0], _xcontaier.begin() + offset.u64_2[0] + rsize, v.begin());
			}

		bool eof(const stream_sequence_position_t& offset)const noexcept {
			return offset.u64_2[0] >= _xcontaier.size();
			}

		const srlz::typemap_key_t& stream_typeid()const  noexcept {
			return stream_blob_typeid;
			}

	public:
		xcontainter_stream_reader_adapter()noexcept {}

		xcontainter_stream_reader_adapter(const xcontainter_stream_reader_adapter&) = delete;
		xcontainter_stream_reader_adapter& operator=(const xcontainter_stream_reader_adapter&) = delete;

		xcontainter_stream_reader_adapter(xcontainter_stream_reader_adapter&&)noexcept = default;
		xcontainter_stream_reader_adapter& operator=(xcontainter_stream_reader_adapter&&)noexcept = default;

		template<typename XContainer,
			typename TXContainer = typename std::decay_t<XContainer>,
			typename = std::enable_if_t<(
				std::is_constructible<xcontainer_t, TXContainer>::value &&
				srlz::detail::is_container_bytes_t<TXContainer>::value &&
				!std::is_base_of_v<xcontainter_stream_reader_adapter<TContainer>, TXContainer>
				)>>
			xcontainter_stream_reader_adapter(XContainer && xcontainer)
			: _xcontaier(std::forward<XContainer>(xcontainer)) {}
	};

//
//template<typename TContainer,
//	typename std::enable_if_t<srlz::detail::is_container_bytes_t<TContainer>::value> * = 0>
//	class rstream_container_adapter_t :
//	public i_stream_reader_t,
//	public i_stream_reader_adapter_t,
//	public xstream_container_t<TContainer>{
//
//	using xcontainer_t = typename TContainer;
//	using self_t = rstream_container_adapter_t<xcontainer_t>;
//
//	xcontainer_t _xcontaier;
//	stream_source_descriptor_t _sourceId;
//	stream_header_descriptor_t _headDesc;
//
//	const stream_source_descriptor_t& source_id()const noexcept final {
//		return _sourceId;
//		}
//
//	stream_sequence_position_t size()const noexcept final {
//		stream_sequence_position_t value;
//		value.u64_2[0] = _xcontaier.size();
//
//		return value;
//		}
//
//	stream_header_descriptor_t make_header_descriptor()const {
//		stream_container_descriptor_t d;
//		d.size = size();
//
//		return d.make_descriptor();
//		}
//
//	stream_header_descriptor_t get_header_descriptor()const final{
//		return _headDesc;
//		}
//
//	void read(const stream_sequence_position_t& offset, size_t blockSize, std::vector<uint8_t>& v)final {
//		const auto rsize((std::min)(blockSize, _xcontaier.size() - offset.u64_2[0]));
//		v.resize(rsize);
//
//		std::copy(_xcontaier.begin() + offset.u64_2[0], _xcontaier.begin() + offset.u64_2[0] + rsize, v.begin());
//		}
//
//	bool eof( const stream_sequence_position_t & offset )const noexcept final{
//		return offset.u64_2[0] >= _xcontaier.size();
//		}
//
//	const srlz::typemap_key_t& stream_typeid()const noexcept final{
//		return stream_blob_typeid;
//		}
//
//	public:
//		rstream_container_adapter_t()noexcept{}
//
//
//		rstream_container_adapter_t(const rstream_container_adapter_t&) = delete;
//		rstream_container_adapter_t& operator=(const rstream_container_adapter_t&) = delete;
//
//		rstream_container_adapter_t(rstream_container_adapter_t&&)noexcept = default;
//		rstream_container_adapter_t& operator=(rstream_container_adapter_t&&)noexcept = default;
//
//		template<typename XContainer,
//			typename TXContainer = typename std::decay_t<XContainer>,
//			typename std::enable_if<(std::is_constructible<xcontainer_t, TXContainer>::value && srlz::detail::is_container_bytes_t<TXContainer>::value && !std::is_same<self_t, TXContainer>::value), int>::type * = 0>
//			rstream_container_adapter_t(const std::weak_ptr<distributed_ctx_t>& ctx, XContainer && xcontainer )
//			: _xcontaier( std::forward<XContainer>( xcontainer ) )
//			, _sourceId(CHECK_PTR(ctx)->make_global_object_identity_128() )
//			, _headDesc(make_header_descriptor()){}
//	};
}

template<typename XContainer,
	typename TXContainer = typename std::decay_t<XContainer>,
	typename std::enable_if_t<srlz::detail::is_container_of_bytes_v<TXContainer>, int> = 0>
auto make_rstream_container_adapter(XContainer && c) {
	using TAdapter = typename detail::xcontainter_stream_reader_adapter<TXContainer>;

	return TAdapter(std::forward<XContainer>(c));
	}

template<typename TContainer,
	typename std::enable_if_t<srlz::detail::is_container_bytes_t<TContainer>::value>* = 0>
class wstream_container_t final :
	public i_stream_writer_t,
	public xstream_container_t<TContainer> {

	using container_type = typename std::decay_t<TContainer>;
	using container_value_type = typename container_type::value_type;
	using self_t = typename wstream_container_t<TContainer>;

	TContainer _xcontaier;
	std::unique_ptr<i_stream_zyamba_identity_t> _si;
	std::unique_ptr<i_stream_zyamba_cover_data_t> _scd;

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	utility::__xobject_counter_logger_t<self_t, 1> _xDbgCounter;
#endif

	stream_sequence_position_t next_offset()const noexcept final {
		stream_sequence_position_t off;
		off.u64_2[0] = _xcontaier.size();
		return std::move(off);
		}

	stream_sequence_position_t write(const stream_sequence_position_t& offset, std::vector<uint8_t>&& v)final {
		if (offset.u64_2[0] == _xcontaier.size()) {
			_xcontaier.insert(_xcontaier.end(), std::make_move_iterator(v.begin()), std::make_move_iterator(v.end()));
			return next_offset();
			}
		else {
			return next_offset();
			}
		}

	void close(detail::stream_close_reason_t /*rc*/)noexcept final {}

	const srlz::typemap_key_t& stream_typeid()const  noexcept final {
		return stream_blob_typeid;
		}

	public:
		wstream_container_t(std::unique_ptr<i_stream_zyamba_identity_t>&& id,
			std::unique_ptr<i_stream_zyamba_cover_data_t>&& cd)
			: _si(std::move(id))
			, _scd(std::move(cd)) {

			if(auto phBlb = dynamic_cast<const stream_identity_type*> (_si.get())) {
				const auto size = phBlb->params().count_of_elements;
				_xcontaier.reserve(size.u64_2[0]);
				}
			else {
				TYPES_MISMATCHING_THROW_EXC_FWD(nullptr);
				}
			}

		typename TContainer container_release() {
			return std::move(_xcontaier);
			}

		const typename TContainer& container()const {
			return _xcontaier;
			}

		/*const std::unique_ptr<i_stream_zyamba_identity_t>& stream_identity()const & noexcept final {
			return _si;
			}

		const std::unique_ptr<i_stream_zyamba_cover_data_t>& stream_cover_data()const & noexcept final {
			return _scd;
			}

		std::unique_ptr<i_stream_zyamba_identity_t> stream_identity() && noexcept final {
			return std::move(_si);
			}

		std::unique_ptr<i_stream_zyamba_cover_data_t> stream_cover_data() && noexcept final {
			return std::move(_scd);
			}*/

		const std::unique_ptr<i_stream_zyamba_identity_t>& identity()const& noexcept {
			return _si;
			}

		const std::unique_ptr<i_stream_zyamba_cover_data_t>& cover_data()const& noexcept {
			return _scd;
			}

		std::unique_ptr<i_stream_zyamba_identity_t> identity() && noexcept {
			return std::move(_si);
			}

		std::unique_ptr<i_stream_zyamba_cover_data_t> cover_data() && noexcept {
			return std::move(_scd);
			}

		static const srlz::typemap_key_t& class_type_id()noexcept {
			return stream_blob_typeid;
			}
	};


template<typename _XContainer,
	typename XContainer = typename std::decay_t<_XContainer>,
	typename std::enable_if_t<srlz::detail::is_container_of_bytes_v<XContainer>, int> = 0>
struct stream_from_container {

	template<typename _YContainer,
		typename YContainer = typename std::decay_t<_YContainer>,
		typename std::enable_if_t<std::is_same_v<YContainer, XContainer>, int> = 0>
	static auto make_reader(_YContainer&& c) {
		return make_rstream_container_adapter(std::forward<_YContainer>(c));
		}

	using writer_slot = typename wstream_container_t<XContainer>;
	};
}
