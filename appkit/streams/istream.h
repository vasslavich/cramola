#pragma once


#include <tuple>
#include <memory>
#include "./i_streams.h"
#include "../channel_terms/rdm_sessional_desc.h"
#include "../channel_terms/events_terms.h"


namespace crm::detail{


class istream_t final :
	public i_input_stream_t,
	public std::enable_shared_from_this<istream_t>{

private:
	typedef istream_t self_t;

	std::weak_ptr<distributed_ctx_t> _ctx;
	std::weak_ptr<i_stream_whandler_t> _wh;
	rdm_sessional_descriptor_t _session;
	__event_handler_point_f _eh;
	stream_source_descriptor_t _streamId;
	bool _eof = false;

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	utility::__xobject_counter_logger_t<self_t, 1> _xDbgCounter;
#endif

	void close() noexcept final;
	std::shared_ptr<i_stream_whandler_t> ctr(std::unique_ptr<i_stream_zyamba_identity_t>&&, std::unique_ptr<i_stream_zyamba_cover_data_t>&&);

public:
	istream_t( std::weak_ptr<distributed_ctx_t>,
		const rdm_sessional_descriptor_t & session,
		__event_handler_point_f && eh );

	static std::tuple<std::shared_ptr<istream_t>, std::shared_ptr<i_stream_whandler_t>> make( std::weak_ptr<distributed_ctx_t>,
		std::unique_ptr<i_stream_zyamba_identity_t> &&,
		std::unique_ptr< i_stream_zyamba_cover_data_t> &&,
		const rdm_sessional_descriptor_t & session,
		__event_handler_point_f && eh );
	~istream_t();


	bool is_possessing_of_writer()const noexcept;
	stream_sequence_position_t offset()const noexcept;

	/*const std::unique_ptr<i_stream_zyamba_cover_data_t>& cover_data()const& noexcept;
	std::unique_ptr<i_stream_zyamba_cover_data_t> cover_data()&& noexcept;*/

	const stream_source_descriptor_t& stream_source_id()const noexcept final;

	stream_sequence_position_t tail( const stream_sequence_position_t & offset,
		std::vector<uint8_t> && data );

	void eof()noexcept;
	void close( stream_close_reason_t r )noexcept;

	bool canceled()const noexcept;
	const rdm_sessional_descriptor_t& session()const noexcept;
	__event_handler_point_f get_event_handler()const noexcept;
	std::shared_ptr<i_stream_whandler_t> handler()const noexcept;
	};
}

