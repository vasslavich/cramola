#pragma once

#include "./i_streams.h"


namespace crm {

template<typename _TStreamParams,
	typename TStreamParams = std::decay_t<_TStreamParams>,
	typename = std::enable_if_t<srlz::is_serializable_v<TStreamParams>>>
class stream_blob_identity_t final : public i_stream_zyamba_identity_t {
public:
	using stream_params = typename TStreamParams;

	srlz::typemap_key_t _tid;
	stream_params _desc;
	stream_source_descriptor_t _sourceId;
	address_hash_t _hash;

	public:
		const stream_source_descriptor_t& stream_source_id()const noexcept final {
			return _sourceId;
			}

		const srlz::i_typeid_t& stream_typeid()const noexcept final {
			return _tid;
			}

		const stream_params& params()const& noexcept {
			return _desc;
			}

		stream_params params()&& noexcept {
			return std::move(_desc);
			}

		const address_hash_t& object_hash()const noexcept {
			return _hash;
			}

	private:
		void update_hash()noexcept {
			_hash = address_hash_t::null;

			apply_at_hash(_hash, _tid);
			apply_at_hash(_hash, _desc);
			apply_at_hash(_hash, _sourceId);
			}

	public:
		using hash_t = address_hash_t;

		template<typename Ts>
		void srlz(Ts&& dest)const {
			srlz::serialize(dest, _tid);
			srlz::serialize(dest, _desc);
			srlz::serialize(dest, _sourceId);
			srlz::serialize(dest, _hash);
			}

		template<typename Ts>
		void dsrlz(Ts&& src) {
			srlz::deserialize(src, _tid);
			srlz::deserialize(src, _desc);
			srlz::deserialize(src, _sourceId);
			srlz::deserialize(src, _hash);
			}

		size_t get_serialized_size()const noexcept {
			return srlz::object_trait_serialized_size(_tid)
				+ srlz::object_trait_serialized_size(_desc)
				+ srlz::object_trait_serialized_size(_sourceId)
				+ srlz::object_trait_serialized_size(_hash);
			}

		stream_blob_identity_t()noexcept {}

		template<typename _XStreamParams,
			typename XStreamParams = std::decay_t<_XStreamParams>,
			typename std::enable_if_t<(
				srlz::is_serializable_v<XStreamParams>
				), int> = 0>
			stream_blob_identity_t(const stream_source_descriptor_t& sourceId,
				const srlz::typemap_key_t& tid,
				_XStreamParams&& desc)noexcept
			: _tid(tid)
			, _desc(std::forward<_XStreamParams>(desc))
			, _sourceId(sourceId) {}
	};


template<typename _TCoverData,
	typename TCoverData = std::decay_t<_TCoverData>>
	std::optional<TCoverData> get_stream_cover_data(const i_stream_zyamba_cover_data_t& b)noexcept {
	if (auto pCd = dynamic_cast<const TCoverData*>(std::addressof(b))) {
		return std::make_optional(*pCd);
		}
	else {
		return {};
		}
	}

template<typename _TCoverData,
	typename TCoverData = std::decay_t<_TCoverData>>
	std::optional<TCoverData> get_stream_cover_data(i_stream_zyamba_cover_data_t&& b)noexcept {
	if (auto pCd = dynamic_cast<TCoverData*>(std::addressof(b))) {
		return std::make_optional(std::move(*pCd));
		}
	else {
		return {};
		}
	}

template<typename _TStreamParam,
	typename TStreamParam = std::decay_t<_TStreamParam>,
	typename TIdentityHolder = typename stream_blob_identity_t<TStreamParam>>
	std::optional<TStreamParam> get_stream_param(const i_stream_zyamba_identity_t& b)noexcept {

	if (auto pHold = dynamic_cast<const TIdentityHolder*>(std::addressof(b))) {
		return std::make_optional(pHold->params());
		}
	else {
		return {};
		}
	}

template<typename _TStreamParam,
	typename TStreamParam = std::decay_t<_TStreamParam>,
	typename TIdentityHolder = typename stream_blob_identity_t<TStreamParam>>
std::optional<TStreamParam> get_stream_param( i_stream_zyamba_identity_t&& b)noexcept {

	if (auto pHold = dynamic_cast<TIdentityHolder*>(std::addressof(b))) {
		return std::make_optional(std::move(*pHold).params());
		}
	else {
		return {};
		}
	}
}

