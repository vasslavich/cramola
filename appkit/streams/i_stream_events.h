#pragma once


#include "../channel_terms/i_events.h"
#include "./streams_types_predecls.h"


namespace crm{

struct i_input_stream_base_event_t : public i_userspace_event_t{
private:
	std::shared_ptr<i_stream_whandler_t> _handler;

public:
	i_input_stream_base_event_t()noexcept;

	template<typename TWHandler,
		typename std::enable_if_t<std::is_base_of<i_stream_whandler_t, TWHandler>::value, int> = 0>
		i_input_stream_base_event_t( std::shared_ptr<TWHandler> s )
		: _handler( std::move( s ) ){}
	
	template<typename _TWriteSlot, 
		typename TWriteSlot = std::decay_t<_TWriteSlot>>
	const TWriteSlot& writer_as()const & {
		if (_handler && _handler->writer()) {
			static_assert(std::is_lvalue_reference_v<
				decltype(detail::writer_as<TWriteSlot>(*_handler->writer()))>, __FILE_LINE__);
			return detail::writer_as<TWriteSlot>(*_handler->writer());
			}
		else {
			THROW_EXC_FWD(nullptr);
			}
		}

	template<typename _TWriteSlot,
		typename TWriteSlot = std::decay_t<_TWriteSlot>>
	TWriteSlot writer_as()&& {
		if (_handler && _handler->writer()) {
			static_assert(
				!std::is_reference_v<
				decltype(detail::writer_as<TWriteSlot>(std::move(*std::move(*_handler).writer())))>
					||
				std::is_rvalue_reference_v<
				decltype(detail::writer_as<TWriteSlot>(std::move(*std::move(*_handler).writer())))>, __FILE_LINE__);
			return detail::writer_as<TWriteSlot>(std::move(*std::move(*_handler).writer()));
			}
		else {
			THROW_EXC_FWD(nullptr);
			}
		}

	template<typename _TWriteSlot,
		typename TWriteSlot = std::decay_t<_TWriteSlot>>
	bool is_writer_as()const noexcept {
		if (_handler && _handler->writer()) {
			return detail::is_writer_as<TWriteSlot>(*_handler->writer());
			}
		else {
			return false;
			}
		}

	std::shared_ptr<i_stream_whandler_t> holder()const noexcept;
	};

struct i_input_stream_begin_t final : public i_input_stream_base_event_t{
	static const srlz::typemap_key_t class_type_id_;

	template<typename TWHandler,
		typename std::enable_if_t<std::is_base_of<i_stream_whandler_t, TWHandler>::value, int> = 0>
		i_input_stream_begin_t( std::shared_ptr<TWHandler> s)
		: i_input_stream_base_event_t( std::move( s ) ){}

	static const srlz::typemap_key_t& class_type_id()noexcept;
	const srlz::typemap_key_t& type_id()const noexcept;
	};

struct i_input_stream_ready_success : i_input_stream_base_event_t {
	template<typename TWHandler,
		typename std::enable_if_t<std::is_base_of<i_stream_whandler_t, TWHandler>::value, int> = 0>
		i_input_stream_ready_success(std::shared_ptr<TWHandler> s)
		: i_input_stream_base_event_t(std::move(s)) {}
	};

struct i_input_stream_ready_fault : i_input_stream_base_event_t {
	template<typename TWHandler,
		typename std::enable_if_t<std::is_base_of<i_stream_whandler_t, TWHandler>::value, int> = 0>
		i_input_stream_ready_fault(std::shared_ptr<TWHandler> s)
		: i_input_stream_base_event_t(std::move(s)) {}
	};

struct i_input_stream_eof_t final : public i_input_stream_ready_success {
	static const srlz::typemap_key_t class_type_id_;

	template<typename TWHandler,
		typename std::enable_if_t<std::is_base_of<i_stream_whandler_t, TWHandler>::value, int> = 0>
		i_input_stream_eof_t( std::shared_ptr<TWHandler> s )
		: i_input_stream_ready_success( std::move( s ) ){}

	static const srlz::typemap_key_t& class_type_id()noexcept;
	const srlz::typemap_key_t& type_id()const noexcept;
	};

struct i_input_stream_aborted_t final : public i_input_stream_ready_fault {
	static const srlz::typemap_key_t class_type_id_;

	template<typename TWHandler,
		typename std::enable_if_t<std::is_base_of<i_stream_whandler_t, TWHandler>::value, int> = 0>
		i_input_stream_aborted_t( std::shared_ptr<TWHandler> s )
		: i_input_stream_ready_fault( std::move( s ) ){}

	static const srlz::typemap_key_t& class_type_id()noexcept;
	const srlz::typemap_key_t& type_id()const noexcept;
	};

struct i_input_stream_exception_t final : public i_input_stream_ready_fault {
	static const srlz::typemap_key_t class_type_id_;

	std::unique_ptr<dcf_exception_t> exception;

	template<typename TWHandler,
		typename std::enable_if_t<std::is_base_of<i_stream_whandler_t, TWHandler>::value, int> = 0>
		i_input_stream_exception_t( std::shared_ptr<TWHandler> s, std::unique_ptr<dcf_exception_t> && exc )
		: i_input_stream_ready_fault( std::move( s ) )
		, exception( std::move( exc ) ){}

	static const srlz::typemap_key_t& class_type_id()noexcept;
	const srlz::typemap_key_t& type_id()const noexcept;
	};
}


