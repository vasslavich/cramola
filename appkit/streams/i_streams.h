#pragma once


#include <type_traits>
#include "../typeframe/internal_terms.h"
#include "../channel_terms/base_terms.h"
#include "../counters/xobject_counter.h"
#include "./streams_properties_using.h"
#include "./streams_types_predecls.h"


namespace crm {


struct stream_class_description {
	virtual ~stream_class_description() = 0;

	virtual const srlz::typemap_key_t& stream_typeid()const noexcept = 0;
	};

struct i_input_stream_t {
	virtual ~i_input_stream_t() {}

	virtual void close()noexcept = 0;
	virtual const stream_source_descriptor_t& stream_source_id()const noexcept = 0;
	};


void set_eof(stream_sequence_position_t& to);
bool is_eof(const stream_sequence_position_t&)noexcept;

template<typename Integer>
std::enable_if_t<std::is_integral_v<Integer>, Integer> 
stream_position_at(const stream_sequence_position_t& off)noexcept {
	return off.at<Integer>(0);
	}

template<typename Integer,
	typename = std::enable_if_t<std::is_integral_v<Integer>>>
stream_sequence_position_t make_stream_postion(Integer i)noexcept {
	stream_sequence_position_t pos;
	pos.at<Integer>(0) = i;
	return pos;
	}


/*template<
struct stream_session_t {
	std::string name;
	std::vector<std::string> keys;
	std::chrono::seconds timeoutFail = std::chrono::seconds(600);

	stream_session_t(std::string_view name_,
		std::vector<std::string> && keys_,
		const std::chrono::seconds & timeoutFail_);

	stream_session_t(std::string_view name_,
		const std::chrono::seconds& timeoutFail_);
	};*/

	//
	//struct i_header_equally_t : public hf_operators_t<i_header_equally_t> {
	//
	//	virtual ~i_header_equally_t() {}
	//
	//	virtual const stream_source_descriptor_t & stream_source_id()const noexcept = 0;
	//	virtual bool is_unique_requirement()const noexcept = 0;
	//	virtual std::string description()const noexcept = 0;
	//	};

	//bool operator == (const i_header_equally_t & lval, const i_header_equally_t &rval)noexcept;
	//bool operator != (const i_header_equally_t & lval, const i_header_equally_t &rval)noexcept;
	//bool operator < (const i_header_equally_t & lval, const i_header_equally_t &rval)noexcept;
	//bool operator > (const i_header_equally_t & lval, const i_header_equally_t &rval)noexcept;
	//bool operator >= (const i_header_equally_t & lval, const i_header_equally_t &rval)noexcept;


	/*struct i_stream_header_properties_t {
		virtual ~i_stream_header_properties_t() {}

		virtual const i_header_equally_t& equally_key()const = 0;
		virtual std::unique_ptr<i_stream_header_properties_t> copy()const = 0;
		};*/

struct i_stream_zyamba_identity_t {
	virtual ~i_stream_zyamba_identity_t() {}
	virtual const srlz::i_typeid_t& stream_typeid()const noexcept = 0;
	virtual const stream_source_descriptor_t& stream_source_id()const noexcept = 0;
	};

struct i_stream_zyamba_cover_data_t {
	virtual ~i_stream_zyamba_cover_data_t() {};
	virtual const srlz::i_typeid_t& stream_typeid()const noexcept = 0;
	};


struct stream_header_descriptor_t /*:
	public srlz::i_srlz_length_prefixed_t */ {

	std::vector<uint8_t> value;
	srlz::typemap_key_t header_typeid;

	//template<typename TWStream>
	//void srlz(TWStream &&dest)const {
	//	srlz::serialize(dest, value);
	//	srlz::serialize(dest, header_typeid);
	//	}

	//template<typename TRStream>
	//void dsrlz(TRStream &&src, size_t) {
	//	srlz::deserialize(src, value);
	//	srlz::deserialize(src, header_typeid);
	//	}

	//size_t get_serialized_size()const {
	//	return srlz::object_trait_serialized_size(value)
	//		+ srlz::object_trait_serialized_size(header_typeid);
	//	}
	};



struct i_stream_reader_t {
	virtual ~i_stream_reader_t() = 0;

	//virtual stream_sequence_position_t size()const noexcept = 0;
	virtual void read(const stream_sequence_position_t& offset, size_t blockSize, std::vector<uint8_t>& v) = 0;
	virtual bool eof(const stream_sequence_position_t& offset)const noexcept = 0;
	virtual const srlz::typemap_key_t& stream_typeid()const  noexcept = 0;

private:
#ifdef CBL_OBJECTS_COUNTER_CHECKER
	utility::__xobject_counter_logger_t<i_stream_reader_t, 1> _xDbgCounter;
#endif
	};





template<typename T>
struct is_stream_identity_trait< T, std::void_t<
	std::enable_if_t<srlz::is_serializable_v<T>>,
	std::enable_if_t<std::is_base_of_v<i_stream_zyamba_identity_t, T>>,
	typename T::stream_params,
	std::enable_if_t<detail::has_hashable_support_v<T>>,
	std::enable_if_t<std::is_nothrow_default_constructible_v<T>>
	>> : std::true_type{};








template<typename T>
struct is_stream_cover_data_trait < T, std::void_t<
	std::enable_if_t<srlz::is_serializable_v<T>>,
	std::enable_if_t<std::is_base_of_v<i_stream_zyamba_cover_data_t, T>>,
	std::enable_if_t<detail::has_hashable_support_v<T>>,
	std::enable_if_t<std::is_nothrow_default_constructible_v<T>>
	>> : std::true_type{};







template<typename TWStreamMaker>
struct is_write_stream_typemaker<TWStreamMaker, std::void_t<
	/*std::enable_if_t<std::is_nothrow_constructible_v<TWStreamMaker,
		std::unique_ptr<i_stream_zyamba_identity_t>&&, std::unique_ptr<i_stream_zyamba_cover_data_t>&&>>,*/

	std::enable_if_t<std::is_convertible_v<decltype(
		std::declval<TWStreamMaker>()(std::declval<std::unique_ptr<i_stream_zyamba_identity_t>>(), std::declval<std::unique_ptr<i_stream_zyamba_cover_data_t>>())
		), std::unique_ptr<i_stream_whandler_t>>>,

	std::enable_if_t<std::is_base_of_v<srlz::i_typeid_t, std::decay_t<decltype(std::declval<TWStreamMaker>().stream_typeid())>>>
	>> : std::true_type{};




template<typename TR>
struct has_member_class_type_id<TR, std::void_t<
	std::enable_if_t<std::is_nothrow_invocable_v<decltype(TR::class_type_id)>>,
	std::enable_if_t<std::is_base_of_v<srlz::i_typeid_t, std::decay_t<decltype(TR::class_type_id())>>>
	>> : std::true_type{};



template<typename TWStream>
struct is_stream_slot_based_by_interface < TWStream, std::void_t<
	std::enable_if_t<std::is_base_of_v<i_stream_writer_t, std::decay_t<TWStream>>>,
	std::enable_if_t<has_member_class_type_id_v<TWStream>>
	>> : std::true_type{};


template<typename S>
struct has_type_declaration_stream_params<S, std::void_t<
	typename std::decay_t<S>::params_type
	>> : std::true_type{
	
	using params_type = typename S::params_type;
	};

template<typename TWStream>
struct is_stream_slot_based_by_members < TWStream, std::void_t <
	std::enable_if_t<has_member_class_type_id_v<TWStream>>,

	std::enable_if_t < (
		std::is_base_of_v<stream_sequence_position_t, std::decay_t<decltype(std::declval<const TWStream&>().next_offset())>>
		/*&&
		noexcept(std::declval<const TWStream&>().next_offset())*/
		)>,

	std::enable_if_t<(
		std::is_base_of_v<stream_sequence_position_t,
		std::decay_t<decltype(std::declval<TWStream>().write(std::declval<const stream_sequence_position_t&>(), std::declval<std::vector<uint8_t>&&>()))>>
		)>,

	decltype(std::declval<TWStream>().close(std::declval<detail::stream_close_reason_t>()))
	
	//,
	
	/*std::enable_if_t<(
		noexcept(std::declval<TWStream>().close(std::declval<detail::stream_close_reason_t>()))
		)>,*/

	//std::enable_if_t<(
	//	std::is_base_of_v< i_stream_zyamba_identity_t,
	//	std::decay_t<decltype(*std::declval<const TWStream&>().stream_identity())>>
	//	/*&&

	//	noexcept(*std::declval<const TWStream&>().stream_identity())*/
	//	)>,

	//std::enable_if_t<(
	//	std::is_base_of_v< i_stream_zyamba_identity_t,
	//	std::decay_t<decltype(*std::declval<TWStream&&>().stream_identity())>>
	//	/*&&

	//	noexcept(*std::declval<TWStream&&>().stream_identity())*/
	//	)>,

	//std::enable_if_t<(
	//	std::is_base_of_v< i_stream_zyamba_cover_data_t,
	//	std::decay_t<decltype(*std::declval<const TWStream&>().stream_cover_data())>>
	//	/*&&

	//	noexcept(*std::declval<const TWStream&>().stream_cover_data())*/
	//	)>,

	//std::enable_if_t<(
	//	std::is_base_of_v< i_stream_zyamba_cover_data_t,
	//	std::decay_t<decltype(*std::declval<TWStream&&>().stream_cover_data())>>
	//	/*&&

	//	noexcept(*std::declval<typename TWStream&&>().stream_cover_data())*/
	//	)>
	>> : std::true_type{};










template<typename TR>
struct has_member_stream_typeid<TR, std::void_t<
	std::enable_if_t<std::is_same_v<std::decay_t<decltype(std::declval<const TR>().stream_typeid())>, srlz::typemap_key_t>>
	>> : std::true_type{};





template<typename T>
struct has_member_header_params<T, std::void_t<
	std::enable_if_t<srlz::is_serializable_v<decltype(std::declval<const T>().get_stream_params())>>
	>> : std::true_type{};








template<typename TR>
struct is_stream_reader_from_members<TR, std::void_t<
	decltype(std::declval<TR>().read(std::declval<const stream_sequence_position_t&>(), std::declval<size_t>(), std::declval<std::vector<uint8_t>&>())),
	/*std::enable_if_t<has_member_stream_header_params_v<TR>>,*/
	std::enable_if_t<std::is_convertible_v<decltype(std::declval<const TR>().eof(std::declval<const stream_sequence_position_t&>())), bool>>
	>> : std::true_type{};







template<typename TR>
struct is_stream_reader<TR,
	std::enable_if_t<is_stream_reader_form_interface_v<TR>>> : std::true_type {};

template<typename TR>
struct is_stream_reader<TR,
	std::enable_if_t<!is_stream_reader_form_interface_v<TR>&& is_stream_reader_from_members_v<TR>>> : std::true_type {};











namespace detail {

template<typename S, typename = std::void_t<>>
struct stream_typeid_maker {
	srlz::typemap_key_t make(const S&)const noexcept {
		return srlz::typemap_key_t::make<std::decay_t<S>>();
		}
	};

template<typename S>
struct stream_typeid_maker<S, std::void_t<
	std::enable_if_t<has_member_stream_typeid_v<S>>
	>> {
	srlz::typemap_key_t make(const S& s)const noexcept {
		return s.stream_typeid();
		}
	};
}


template<typename S>
auto make_stream_typeid(S&& s)noexcept {
	return detail::stream_typeid_maker<S>{}.make(std::forward<S>(s));
	}


struct i_stream_writer_t : stream_class_description {
	virtual ~i_stream_writer_t() = 0;

	virtual stream_sequence_position_t next_offset()const noexcept = 0;
	virtual stream_sequence_position_t write(const stream_sequence_position_t& offset, std::vector<uint8_t>&& v) = 0;
	virtual void close(detail::stream_close_reason_t rc)noexcept = 0;

	/*virtual const std::unique_ptr<i_stream_zyamba_identity_t>& stream_identity()const& noexcept = 0;
	virtual const std::unique_ptr<i_stream_zyamba_cover_data_t>& stream_cover_data()const& noexcept = 0;

	virtual std::unique_ptr<i_stream_zyamba_identity_t> stream_identity() && noexcept = 0;
	virtual std::unique_ptr<i_stream_zyamba_cover_data_t> stream_cover_data() && noexcept = 0;*/


private:
#ifdef CBL_OBJECTS_COUNTER_CHECKER
	utility::__xobject_counter_logger_t<i_stream_writer_t, 1> _xDbgCounter;
#endif
	};

struct i_stream_rhandler_t {
	virtual ~i_stream_rhandler_t() = 0;

	virtual void read(const stream_sequence_position_t& offset, std::vector<uint8_t>& v) = 0;
	virtual bool eof(const stream_sequence_position_t& offset)const noexcept = 0;
	virtual const stream_source_descriptor_t& source_id()const noexcept = 0;
	virtual const srlz::typemap_key_t& stream_typeid()const noexcept = 0;

private:
#ifdef CBL_OBJECTS_COUNTER_CHECKER
	utility::__xobject_counter_logger_t<i_stream_rhandler_t, 1> _xDbgCounter;
#endif
	};


struct i_stream_whandler_t {

	virtual bool is_possessing_of_writer()const noexcept = 0;
	virtual stream_sequence_position_t next_offset() = 0;
	virtual stream_sequence_position_t write(const stream_sequence_position_t& offset, std::vector<uint8_t>&& v) = 0;

	/*virtual const std::unique_ptr<i_stream_zyamba_identity_t>& stream_identity()const& noexcept = 0;
	virtual const std::unique_ptr<i_stream_zyamba_cover_data_t>& stream_cover_data()const& noexcept = 0;

	virtual std::unique_ptr<i_stream_zyamba_identity_t> stream_identity() && noexcept = 0;
	virtual std::unique_ptr<i_stream_zyamba_cover_data_t> stream_cover_data() && noexcept = 0;*/

	virtual const std::unique_ptr<i_stream_writer_t>& writer()const& noexcept = 0;
	virtual std::unique_ptr<i_stream_writer_t> writer() && noexcept = 0;

	virtual bool eof()const noexcept = 0;
	virtual void set_eof()noexcept = 0;
	virtual void close(detail::stream_close_reason_t rc)noexcept = 0;
	virtual bool canceled()const noexcept = 0;

private:
	std::weak_ptr<distributed_ctx_t> _ctx;
	std::shared_ptr<i_input_stream_t> _connector;
	std::atomic<bool> _clf = false;

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	utility::__xobject_counter_logger_t<i_stream_whandler_t, 1> _xDbgCounter;
#endif

public:
	i_stream_whandler_t(const i_stream_whandler_t& o) = delete;
	i_stream_whandler_t& operator=(const i_stream_whandler_t& o) = delete;

	i_stream_whandler_t(i_stream_whandler_t&&) noexcept;
	i_stream_whandler_t& operator=(i_stream_whandler_t&&) noexcept;

	i_stream_whandler_t(std::weak_ptr<distributed_ctx_t> ctx);
	virtual ~i_stream_whandler_t();

	void bind_stream(std::shared_ptr<i_input_stream_t> s)noexcept;
	void close()noexcept;
	};



//
//class stream_blob_header_equality_t final :	public i_header_equally_t{
//	stream_source_descriptor_t _sourceId;
//	std::vector<std::string> _keys;
//	hash_t _hash;
//
//public:
//	using hash_t = address_hash_t;
//
//	const stream_source_descriptor_t& stream_source_id()const noexcept;
//	const hash_t& object_hash()const noexcept;
//
//	bool equals_fields(const i_header_equally_t& r)const noexcept;
//	bool less_fields(const i_header_equally_t& r)const noexcept;
//	bool great_fields(const i_header_equally_t& r)const noexcept;
//	bool great_equals_fields(const i_header_equally_t& r)const noexcept;
//
//	bool is_unique_requirement()const noexcept final;
//	std::string description()const noexcept final;
//
//	template<typename TWStream>
//	void srlz(TWStream &&dest )const {
//		srlz::serialize( dest, _sourceId );
//		srlz::serialize( dest, _keys );
//		srlz::serialize( dest, _hash );
//		}
//
//	template<typename TRStream>
//	void dsrlz(TRStream && src ){
//		srlz::deserialize( src, _sourceId );
//		srlz::deserialize( src, _keys );
//		srlz::deserialize( src, _hash );
//		}
//
//	size_t get_serialized_size()const noexcept;
//
//private:
//	void update_hash()noexcept;
//
//public:
//	stream_blob_header_equality_t() noexcept;
//	stream_blob_header_equality_t(const stream_source_descriptor_t & sourceId, const std::vector<std::string> & keys);
//
//	const std::vector<std::string>& keys()const noexcept;
//	};


//class stream_blob_header_properties_t final :
//	public hf_operators_t<stream_blob_header_properties_t>,
//	public i_stream_header_properties_t {
//
//private:
//	stream_blob_header_equality_t _eqVal;
//
//public:
//	std::unique_ptr<i_stream_header_properties_t> copy()const noexcept final;
//
//	template<typename Ts>
//	void srlz( Ts &&dest )const{
//		srlz::serialize( dest, _eqVal );
//		}
//
//	template<typename Ts>
//	void dsrlz( Ts && src ){
//		srlz::deserialize( src, _eqVal );
//		}
//
//	size_t get_serialized_size()const noexcept;
//
//	using hash_t = address_hash_t;
//
//	const i_header_equally_t& equally_key()const noexcept final;
//	const address_hash_t& object_hash()const noexcept;
//	bool equals_fields(const stream_blob_header_properties_t& r)const  noexcept;
//	bool less_fields(const stream_blob_header_properties_t& r)const  noexcept;
//	bool great_fields(const stream_blob_header_properties_t& r)const  noexcept;
//	bool great_equals_fields(const stream_blob_header_properties_t& r)const  noexcept;
//
//	stream_blob_header_properties_t()noexcept;
//	stream_blob_header_properties_t(const stream_source_descriptor_t & sourceId, 
//		const std::vector<std::string> & keys);
//	};


/*	template<typename _TStreamDescriptor,
	typename TStreamDescriptor = std::decay_t<_TStreamDescriptor>,
	typename = std::enable_if_t<(
		srlz::is_serializable_v<TStreamDescriptor> &&
		srlz::detail::has_hashable_support_v<TStreamDescriptor>
		)>>
class stream_blob_header_t/*final:
	public i_stream_zyamba_header_t*/ /*{

public:
	using properties_type = TStreamDescriptor;

private:
	srlz::typemap_key_t _tid;
	stream_header_descriptor_t _desc;
	properties_type _prop;
	address_hash_t _hash;

public:
	const srlz::i_typeid_t& stream_typeid()const noexcept {
		return _tid;
		}

	const i_stream_header_properties_t& properties()const noexcept {
		return _prop;
		}

	const stream_header_descriptor_t& descriptor()const noexcept {
		return _desc;
		}

	const address_hash_t& object_hash()const noexcept {
		return _hash;
		}

	//std::unique_ptr<i_stream_zyamba_header_t> copy()const noexcept {
	//return std::make_unique<stream_blob_header_t>(*this);
	//	}

private:
	void update_hash()noexcept {
		_hash = address_hash_t::null;

		apply_at_hash(_hash, _prop.object_hash());
		apply_at_hash(_hash, _tid);
		apply_at_hash(_hash, _desc.header_typeid);
		}

public:
	using hash_t = address_hash_t;

	template<typename Ts>
	void srlz(Ts && dest)const {
		srlz::serialize(dest, _prop);
		srlz::serialize(dest, _tid);
		srlz::serialize(dest, _desc);
		}

	template<typename Ts>
	void dsrlz(Ts && src) {
		srlz::deserialize(src, _prop);
		srlz::deserialize(src, _tid);
		srlz::deserialize(src, _desc);
		}

	size_t get_serialized_size()const noexcept {
		return srlz::object_trait_serialized_size(_prop)
			+ srlz::object_trait_serialized_size(_tid)
			+ srlz::object_trait_serialized_size(_desc);
		}

	//void serialize(srlz::detail::wstream_base_handler& dest)const final;
	//void deserialize(srlz::detail::rstream_base_handler & src) final;

	stream_blob_header_t()noexcept {}

	template<typename _XStreamDescriptor,
		typename XStreamDescriptor = std::decay_t<_XStreamDescriptor>,
		typename std::enable_if_t < (
			std::is_base_of_v< properties_type, XStreamDescriptor> &&
			srlz::detail::has_hashable_support_v<XStreamDescriptor>
			), int> = 0>
		stream_blob_header_t(_XStreamDescriptor && prop,
			const srlz::typemap_key_t & tid,
			stream_header_descriptor_t && desc)noexcept
		: _tid(tid)
		, _desc(std::move(desc))
		, _prop(std::forward<_XStreamDescriptor>(prop)) {}
};
	*/


struct i_stream_reader_adapter_t {
	virtual ~i_stream_reader_adapter_t() {}

	virtual stream_header_descriptor_t get_header_descriptor()const = 0;
	virtual void read(const stream_sequence_position_t& off, size_t count, std::vector<uint8_t>& buf) = 0;
	virtual stream_sequence_position_t size()const noexcept = 0;
	virtual const srlz::typemap_key_t& stream_typeid()const noexcept = 0;
	virtual bool eof(const stream_sequence_position_t& offset)const noexcept = 0;

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	utility::__xobject_counter_logger_t<i_stream_reader_adapter_t, 1> _xDbgCounter;
#endif
	};


namespace detail {
struct i_stream_type_handler_t {
	using wstream_make_f = std::function<std::unique_ptr<i_stream_whandler_t>(std::unique_ptr<i_stream_zyamba_identity_t>&&,
		std::unique_ptr<i_stream_zyamba_cover_data_t>&&)>;
	using whandler_push_f = std::function<void(const stream_source_descriptor_t& h, std::shared_ptr<i_stream_whandler_t>&&)>;

	virtual ~i_stream_type_handler_t() {}

	virtual void add_wstream_type_maker(const srlz::i_typeid_t& tid, wstream_make_f&& makef) = 0;
	virtual std::shared_ptr<i_stream_whandler_t> pop_stream_writer(std::unique_ptr<i_stream_zyamba_identity_t>&& h) = 0;
	virtual void push_stream_writer(const stream_source_descriptor_t& streamIdentity)noexcept = 0;
	virtual size_t streams_count()const noexcept = 0;
	};
}
}

