#include "../../internal.h"
#include "../whandlers_tbl.h"


using namespace crm;
using namespace crm::detail;


/*bool crm::detail::operator < ( const istreams_whandlers_table_t::key_t & lval,
	const istreams_whandlers_table_t::key_t &rval )noexcept{

	return (*CHECK_PTR( lval.pval )) < (*CHECK_PTR( rval.pval ));
	}

bool crm::detail::operator >= ( const istreams_whandlers_table_t::key_t & lval,
	const istreams_whandlers_table_t::key_t &rval )noexcept{

	return (*CHECK_PTR( lval.pval )) >= (*CHECK_PTR( rval.pval ));
	}*/

istreams_whandlers_table_t::istreams_whandlers_table_t( std::shared_ptr<distributed_ctx_t> && ctx,
	whandler_push_f && whpush )
	: _pImpl( std::make_unique<_impl_t>() )
	, _ctx( std::move( ctx ) )
	, _whpush( std::move( whpush ) ){}

istreams_whandlers_table_t::istreams_whandlers_table_t( istreams_whandlers_table_t && o )noexcept
	: _pImpl( std::move( o._pImpl ) )
	, _ctx(std::move(o._ctx))
	, _whpush( std::move( o._whpush ) )

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	, _xDbgCounter( std::move( o._xDbgCounter ) )
#endif
	{}

istreams_whandlers_table_t& istreams_whandlers_table_t::operator=( istreams_whandlers_table_t && o )noexcept{
	_pImpl = std::move( o._pImpl );
	_ctx = std::move( o._ctx );
	_whpush = std::move( o._whpush );

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	_xDbgCounter = std::move( o._xDbgCounter );
#endif

	return (*this);
	}

istreams_whandlers_table_t::wstream_make_f istreams_whandlers_table_t::__find_maker( const srlz::typemap_key_t & tid ){

	auto itF = CHECK_PTR( _pImpl )->_streamCtrs.find( tid );
	if( itF == CHECK_PTR( _pImpl )->_streamCtrs.end() ){
		return nullptr;
		}
	else{
		return (*itF).second;
		}
	}

istreams_whandlers_table_t::wstream_make_f istreams_whandlers_table_t::find_maker( const i_stream_zyamba_identity_t& h ){
	auto tid = srlz::typemap_key_t( h.stream_typeid() );

	lckscp_t lck( CHECK_PTR( _pImpl )->_lck );
	return __find_maker( tid );
	}


std::shared_ptr<i_stream_whandler_t> istreams_whandlers_table_t::__select_writer( std::unique_ptr<i_stream_zyamba_identity_t> && h_,
	std::unique_ptr<i_stream_zyamba_cover_data_t>&& cd){

	auto key = h_->stream_source_id();

	auto itS = CHECK_PTR( _pImpl )->_streams.find( key );
	if( itS == CHECK_PTR( _pImpl )->_streams.end() ){

		if(auto makef = __find_maker(h_->stream_typeid())){
			if (std::shared_ptr<i_stream_whandler_t> obj = makef(std::move(h_), std::move(cd))) {

				if( !CHECK_PTR( _pImpl )->_streams.insert( std::make_pair( key, obj ) ).second )
					THROW_EXC_FWD(nullptr);

				return std::move( obj );
				}
			else{
				THROW_MPF_EXC_FWD(nullptr);
				}
			}
		else{
			TYPES_MISMATCHING_THROW_EXC_FWD(nullptr);
			}
		}
	else{
		if(auto r = itS->second.lock()) {
			return std::move(r);
			}
		else{
			CHECK_NO_EXCEPTION( CHECK_PTR( _pImpl )->_streams.erase( itS ) );
			ABANDONED_THROW_EXC_FWD(nullptr);
			}
		}
	}

std::shared_ptr<i_stream_whandler_t> istreams_whandlers_table_t::pop_stream_writer( std::unique_ptr<i_stream_zyamba_identity_t> && h_,
	std::unique_ptr<i_stream_zyamba_cover_data_t>&& cd){

	lckscp_t lck( CHECK_PTR( _pImpl )->_lck );
	return __select_writer( std::move(h_), std::move(cd) );
	}

void istreams_whandlers_table_t::__return_writer(const stream_source_descriptor_t & h_)noexcept{
	if(_whpush) {

		auto itS = CHECK_PTR(_pImpl)->_streams.find(h_);
		if(itS != CHECK_PTR(_pImpl)->_streams.end()) {
			auto pwh = itS->second.lock();
			auto erasef = (pwh ? (pwh->eof() || pwh->canceled()) : true);

			if(erasef) {
				CHECK_PTR(_pImpl)->_streams.erase(itS);

				if(auto c = _ctx.lock()) {

					c->launch_async_exthndl(__FILE_LINE__, _whpush, h_, std::move(pwh));
					CBL_VERIFY(_whpush);
					}
				}
			}
		}
	}

void istreams_whandlers_table_t::push_stream_writer( const stream_source_descriptor_t & h )noexcept{

	lckscp_t lck( CHECK_PTR( _pImpl )->_lck );
	return __return_writer( h );
	}

size_t istreams_whandlers_table_t::streams_count()const noexcept {
	lckscp_t lck(CHECK_PTR(_pImpl)->_lck);
	return _pImpl->_streams.size();
	}

