#include <fstream>
#include "../../internal.h"
#include "../xfile.h"


using namespace crm;
using namespace crm::detail;


static_assert(has_member_stream_typeid_v<rstream_file_adapter_t>, __FILE_LINE__);
static_assert(srlz::is_serializable_v< stream_file_descriptor_t>, __FILE_LINE__);


xstream_file_t::~xstream_file_t() {}
const srlz::typemap_key_t xstream_file_t::stream_blob_typeid = crm::srlz::typemap_key_t::make<xstream_file_t>();


stream_file_descriptor_t rstream_file_adapter_t::get_stream_params()const {
	stream_file_descriptor_t d;
	d.file_extension = _path.extension();
	d.file_name = _path.filename();

	return d;
	}

const srlz::typemap_key_t& rstream_file_adapter_t::stream_typeid()const noexcept {
	return stream_blob_typeid;
	}

/*const stream_source_descriptor_t& rstream_file_adapter_t::source_id()const noexcept {
	return _sourceId;
	}*/

void rstream_file_adapter_t::initialize() {
	_size = std::filesystem::file_size( _path );
	}

/*stream_sequence_position_t rstream_file_adapter_t::size()const noexcept {
	stream_sequence_position_t value;
	value.u64_2[0] = _size;

	return value;
	}*/

void rstream_file_adapter_t::read( const stream_sequence_position_t & offset, size_t blockSize, std::vector<uint8_t> & v ){
	if( offset.u64_2[0] > _size ) {
		EXTERN_CODE_THROW_EXC_FWD(nullptr);
		}
	else {
		const auto tailSize = _size - offset.u64_2[0];
		const auto rsize( (std::min)(blockSize, tailSize) );
		const auto untilEnd = rsize == tailSize;
	
		std::basic_ifstream<uint8_t> file( _path.string(), std::ios::binary );
		if( file ) {
			try {
				file.exceptions( std::ios_base::failbit | std::ios_base::badbit );
				file.unsetf( std::ios::skipws );

				/* �������� � ����� */
				file.seekg( offset.u64_2[0], std::ios::beg );

				if( untilEnd ) {

					/* ������ �� ����� ����� */
					v.clear();
					std::istreambuf_iterator<uint8_t> iter( file );
					std::copy( iter, (std::istreambuf_iterator<uint8_t>()), std::back_inserter( v ) );
					}
				else {

					/* ������ � ������ ������ */
					v.resize( rsize );
					file.read( v.data(), v.size() );
					if( file.gcount() != (std::streamsize)v.size() ) {
						EXTERN_CODE_THROW_EXC_FWD(nullptr);
						}
					}
				}
			catch( const std::exception & exc ) {
				EXTERN_CODE_THROW_EXC_FWD( exc );
				}
			}
		else {
			EXTERN_CODE_THROW_EXC_FWD(nullptr);
			}
		}
	}

bool rstream_file_adapter_t::eof( const stream_sequence_position_t & offset )const noexcept {
	return offset.u64_2[0] >= _size;
	}

/*const srlz::typemap_key_t& rstream_file_adapter_t::stream_typeid()const noexcept {
	return stream_blob_typeid;
	}*/

rstream_file_adapter_t::rstream_file_adapter_t() noexcept{}






void wstream_file_t::set_keep_location() {
	_keepLocation = true;
	}

wstream_file_t::~wstream_file_t() {
	clear();
	}

void wstream_file_t::clear()noexcept {
	try {
		if( !_keepLocation ) {
			auto ctx_( _ctx.lock() );
			if( ctx_ ) {
				ctx_->launch_async_exthndl( __FILE_LINE__, []( std::filesystem::path && pth ) { 
					std::filesystem::remove( pth );
					}, std::move( _path ) );
				}
			else {
				std::filesystem::remove( _path );
				}
			}
		}
	catch( const dcf_exception_t &exc0 ) {
		crm::file_logger_t::logging( exc0.msg(), 0x2, __CBL_FILEW__, __LINE__ );
		}
	catch( const std::exception & exc1 ) {
		crm::file_logger_t::logging( exc1.what(), 0x2, __CBL_FILEW__, __LINE__ );
		}
	catch( ... ) {
		crm::file_logger_t::logging( "", 0x2, __CBL_FILEW__, __LINE__ );
		}
	}

const std::filesystem::path& wstream_file_t::path()const {
	return _path;
	}

stream_sequence_position_t wstream_file_t::next_offset()const noexcept {
	stream_sequence_position_t off;
	if( std::filesystem::exists( _path ) )
		off.u64_2[0] = std::filesystem::file_size( _path );

	return std::move( off );
	}

stream_sequence_position_t wstream_file_t::write( const stream_sequence_position_t & offset, std::vector<uint8_t> && v ) {
	if( offset.u64_2[0] == std::filesystem::file_size( _path ) ) {

		if( !v.empty() ) {
			std::basic_ofstream<uint8_t> file( _path.string(), std::ios::binary | std::ios::app );
			if( file ) {

				try {
					file.exceptions( std::ios_base::badbit );
					file.unsetf( std::ios::skipws );

					file.write( (const uint8_t*)v.data(), v.size() );
					file.close();
					}
				catch( const std::exception & exc ) {
					EXTERN_CODE_THROW_EXC_FWD( exc );
					}
				}
			else {
				EXTERN_CODE_THROW_EXC_FWD(nullptr);
				}
			}

		return next_offset();
		}
	else {
		return next_offset();
		}
	}

void wstream_file_t::close( stream_close_reason_t /*rc*/ ) noexcept {}

const srlz::typemap_key_t& wstream_file_t::stream_typeid()const noexcept {
	return stream_blob_typeid;
	}

//const std::unique_ptr<i_stream_zyamba_identity_t>& wstream_file_t::stream_identity()const& noexcept {
//	return _si;
//	}
//
//const std::unique_ptr<i_stream_zyamba_cover_data_t>& wstream_file_t::stream_cover_data()const& noexcept {
//	return _scd;
//	}
//
//std::unique_ptr<i_stream_zyamba_identity_t> wstream_file_t::stream_identity() && noexcept {
//	return std::move(_si);
//	}
//
//std::unique_ptr<i_stream_zyamba_cover_data_t> wstream_file_t::stream_cover_data() && noexcept {
//	return std::move(_scd);
//	}



