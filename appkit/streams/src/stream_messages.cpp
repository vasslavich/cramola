#include "../../internal.h"
#include "../messages.h"


using namespace crm;
using namespace crm::detail;



static_assert(srlz::detail::is_i_srlzd_prefixed_v<stream_iheader_response_t>, __FILE_LINE__);
static_assert(srlz::is_ctr_as_i_srlzd_v< stream_iheader_response_t>, __FILE_LINE__);
static_assert(srlz::detail::is_i_srlzd_v<stream_iheader_response_t>, __FILE_LINE__);


static_assert(std::is_default_constructible_v<stream_oheader_t>, __FILE_LINE__);
static_assert(srlz::detail::is_i_srlzd_v<stream_oheader_t>, __FILE_LINE__);
static_assert(srlz::is_ctr_as_i_srlzd_v<stream_oheader_t>, __FILE_LINE__);


static_assert(std::is_default_constructible_v<stream_oheader_response_t>, __FILE_LINE__);
static_assert(srlz::detail::is_i_srlzd_prefixed_v<stream_oheader_response_t>, __FILE_LINE__);
static_assert(srlz::detail::is_i_srlzd_v<stream_oheader_response_t>, __FILE_LINE__);
static_assert(srlz::is_ctr_as_i_srlzd_v<stream_oheader_response_t>, __FILE_LINE__);

static_assert(srlz::detail::is_i_srlzd_prefixed_v<stream_xtype_t<i_stream_zyamba_identity_t>>, __FILE_LINE__);
static_assert(srlz::detail::is_i_srlzd_prefixed_v<stream_xtype_t<i_stream_zyamba_cover_data_t>>, __FILE_LINE__);

bool crm::detail::is_stream_based( const std::unique_ptr<i_iol_ihandler_t> & iom ) noexcept {
	return dynamic_cast<const i_istream_line_t*>(iom.get()) != nullptr;
	}




i_ostream_line_t::i_ostream_line_t() noexcept
	:base_type_t( iol_type_spcf_t(), global_object_identity_t::null ) {}

i_ostream_line_t::i_ostream_line_t( iol_type_spcf_t type, 
	const global_object_identity_t & id )noexcept
	: base_type_t( type, id ){}





i_istream_line_t::i_istream_line_t( const iol_type_spcf_t & type,
	const global_object_identity_t & id  )noexcept
	: base_type_t( type, id ) {}







iol_type_spcf_t stream_oheader_t::mpf_type_spcf()noexcept {
	return iol_type_spcf_t::build(
		iol_type_spcf_t::make_attrb_pair( iol_type_attributes_t::type, iol_types_t::st_stream_header ),
		iol_type_spcf_t::make_attrb_pair( iol_type_attributes_t::length, iol_length_spcf_t::prefixed ) );
	}

stream_oheader_t::stream_oheader_t()noexcept
	: base_type_t( mpf_type_spcf(),  global_object_identity_t::null ) {}

const rdm_sessional_descriptor_t& stream_oheader_t::sid()const noexcept {
	return _sid;
	}

size_t stream_oheader_t::get_serialized_size()const noexcept{
	return srlz::object_trait_serialized_size( _si )
		+ srlz::object_trait_serialized_size(_sd)
		+ srlz::object_trait_serialized_size( _sid );
	}










iol_type_spcf_t stream_iheader_t::mpf_type_spcf()noexcept {
	return iol_type_spcf_t::build(
		iol_type_spcf_t::make_attrb_pair( iol_type_attributes_t::type, iol_types_t::st_stream_header ),
		iol_type_spcf_t::make_attrb_pair( iol_type_attributes_t::length, iol_length_spcf_t::prefixed ) );
	}

stream_iheader_t::stream_iheader_t()noexcept
	: base_t( mpf_type_spcf() ) {}

const rdm_sessional_descriptor_t& stream_iheader_t::sid()const noexcept {
	return _sid;
	}

size_t stream_iheader_t::get_serialized_size()const noexcept {
	return srlz::object_trait_serialized_size( _si )
		+ srlz::object_trait_serialized_size(_sd)
		+ srlz::object_trait_serialized_size(_sid);
	}

const stream_xtype_t<i_stream_zyamba_identity_t>& stream_iheader_t::identity()const noexcept {
	return _si;
	}
const stream_xtype_t<i_stream_zyamba_cover_data_t>& stream_iheader_t::cover_data()const noexcept {
	return _sd;
	}








iol_type_spcf_t stream_oheader_response_t::mpf_type_spcf() noexcept {
	return iol_type_spcf_t::build(
		iol_type_spcf_t::make_attrb_pair( iol_type_attributes_t::type, iol_types_t::st_stream_header_response ),
		iol_type_spcf_t::make_attrb_pair( iol_type_attributes_t::length, iol_length_spcf_t::prefixed ) );
	}

const rdm_sessional_descriptor_t& stream_oheader_response_t::sid()const noexcept {
	return _sid;
	}

stream_oheader_response_t::stream_oheader_response_t()noexcept{}

stream_oheader_response_t::stream_oheader_response_t( const stream_sequence_position_t & posReady,
	const rdm_sessional_descriptor_t & sid )noexcept
	: base_type_t( mpf_type_spcf() )
	, _posReady(posReady)
	, _sid(sid){}

size_t stream_oheader_response_t::get_serialized_size()const noexcept{
	return srlz::object_trait_serialized_size( _posReady ) + srlz::object_trait_serialized_size( _sid );
	}










iol_type_spcf_t stream_iheader_response_t::mpf_type_spcf()noexcept {
	return iol_type_spcf_t::build(
		iol_type_spcf_t::make_attrb_pair( iol_type_attributes_t::type, iol_types_t::st_stream_header_response ),
		iol_type_spcf_t::make_attrb_pair( iol_type_attributes_t::length, iol_length_spcf_t::prefixed ) );
	}

const rdm_sessional_descriptor_t& stream_iheader_response_t::sid()const noexcept {
	return _sid;
	}

stream_iheader_response_t::stream_iheader_response_t()noexcept
	: base_t( mpf_type_spcf() ) {}

size_t stream_iheader_response_t::get_serialized_size()const noexcept{
	return srlz::object_trait_serialized_size( _posReady ) + srlz::object_trait_serialized_size( _sid );
	}

const stream_sequence_position_t& stream_iheader_response_t::ready_position()const noexcept {
	return _posReady;
	}








iol_type_spcf_t stream_otail_t::mpf_type_spcf() noexcept {
	return iol_type_spcf_t::build(
		iol_type_spcf_t::make_attrb_pair( iol_type_attributes_t::type, iol_types_t::st_stream_block_value ),
		iol_type_spcf_t::make_attrb_pair( iol_type_attributes_t::length, iol_length_spcf_t::prefixed ) );
	}

const rdm_sessional_descriptor_t& stream_otail_t::sid()const noexcept {
	return _sid;
	}

stream_otail_t::stream_otail_t( std::vector<uint8_t> && value, 
	const stream_sequence_position_t & pos,
	const rdm_sessional_descriptor_t & sid )noexcept
	:base_type_t( mpf_type_spcf() )
	, _value( std::move( value ) )
	, _pos( pos )
	, _sid( sid ){}

size_t stream_otail_t::get_serialized_size()const noexcept {
	return srlz::object_trait_serialized_size( _value )
		+ srlz::object_trait_serialized_size( _pos )
		+ srlz::object_trait_serialized_size( _sid );
	}














iol_type_spcf_t stream_itail_t::mpf_type_spcf() noexcept {
	return iol_type_spcf_t::build(
		iol_type_spcf_t::make_attrb_pair( iol_type_attributes_t::type, iol_types_t::st_stream_block_value ),
		iol_type_spcf_t::make_attrb_pair( iol_type_attributes_t::length, iol_length_spcf_t::prefixed ) );
	}

const rdm_sessional_descriptor_t& stream_itail_t::sid()const noexcept {
	return _sid;
	}

stream_itail_t::stream_itail_t()noexcept
	: base_t( mpf_type_spcf() ) {}

size_t stream_itail_t::get_serialized_size()const noexcept{
	return srlz::object_trait_serialized_size( _value )
		+ srlz::object_trait_serialized_size( _pos )
		+ srlz::object_trait_serialized_size( _sid );
	}

const std::vector<uint8_t>& stream_itail_t::value()const & noexcept {
	return _value;
	}

const stream_sequence_position_t& stream_itail_t::position()const noexcept {
	return _pos;
	}

std::vector<uint8_t> stream_itail_t::value()&& noexcept {
	return std::move( _value );
	}










iol_type_spcf_t stream_otail_next_t::mpf_type_spcf() noexcept {
	return iol_type_spcf_t::build(
		iol_type_spcf_t::make_attrb_pair( iol_type_attributes_t::type, iol_types_t::st_stream_block_request ),
		iol_type_spcf_t::make_attrb_pair( iol_type_attributes_t::length, iol_length_spcf_t::prefixed ) );
	}

const rdm_sessional_descriptor_t& stream_otail_next_t::sid()const noexcept {
	return _sid;
	}

stream_otail_next_t::stream_otail_next_t( const stream_sequence_position_t & nextPos ,
	const rdm_sessional_descriptor_t & sid )noexcept
	: base_type_t( mpf_type_spcf() )
	, _pos( nextPos )
	, _sid( sid ){}

size_t stream_otail_next_t::get_serialized_size()const noexcept {
	return srlz::object_trait_serialized_size( _pos ) + srlz::object_trait_serialized_size( _sid );
	}








iol_type_spcf_t stream_itail_next_t::mpf_type_spcf() noexcept {
	return iol_type_spcf_t::build(
		iol_type_spcf_t::make_attrb_pair( iol_type_attributes_t::type, iol_types_t::st_stream_block_request ),
		iol_type_spcf_t::make_attrb_pair( iol_type_attributes_t::length, iol_length_spcf_t::prefixed ) );
	}

const rdm_sessional_descriptor_t& stream_itail_next_t::sid()const noexcept {
	return _sid;
	}

stream_itail_next_t::stream_itail_next_t()noexcept
	: base_t( mpf_type_spcf() ) {}

size_t stream_itail_next_t::get_serialized_size()const noexcept{
	return srlz::object_trait_serialized_size( _pos ) + srlz::object_trait_serialized_size( _sid );
	}

const stream_sequence_position_t& stream_itail_next_t::position()const noexcept {
	return _pos;
	}






stream_oend_t::stream_oend_t()noexcept {}

iol_type_spcf_t stream_oend_t::mpf_type_spcf()noexcept {
	return iol_type_spcf_t::build(
		iol_type_spcf_t::make_attrb_pair( iol_type_attributes_t::type, iol_types_t::st_stream_end ),
		iol_type_spcf_t::make_attrb_pair( iol_type_attributes_t::length, iol_length_spcf_t::prefixed ) );
	}

stream_oend_t::stream_oend_t( const rdm_sessional_descriptor_t & sid,
	const global_object_identity_t & id )noexcept
	: base_type_t( mpf_type_spcf(), id )
	, _sid( sid ) {}

const rdm_sessional_descriptor_t& stream_oend_t::sid()const noexcept {
	return _sid;
	}

size_t stream_oend_t::get_serialized_size()const noexcept {
	return srlz::object_trait_serialized_size( _sid );
	}

//
//std::string stream_oend_t::type_name()const noexcept { 
//	return srlz::make_type<stream_iend_t>::name();
//	}
//type_identifier_t stream_oend_t::type_id()const noexcept { 
//	return srlz::make_type<stream_iend_t>::id(); 
//	}









iol_type_spcf_t stream_iend_t::mpf_type_spcf() noexcept {
	return iol_type_spcf_t::build(
		iol_type_spcf_t::make_attrb_pair( iol_type_attributes_t::type, iol_types_t::st_stream_end ),
		iol_type_spcf_t::make_attrb_pair( iol_type_attributes_t::length, iol_length_spcf_t::prefixed ) );
	}

const rdm_sessional_descriptor_t& stream_iend_t::sid()const noexcept {
	return _sid;
	}

stream_iend_t::stream_iend_t()noexcept
	: base_t( mpf_type_spcf() ) {}


size_t  stream_iend_t::get_serialized_size()const noexcept{
	return srlz::object_trait_serialized_size( _sid );
	}















iol_type_spcf_t stream_odisconnection_t::mpf_type_spcf() noexcept {
	return iol_type_spcf_t::build(
		iol_type_spcf_t::make_attrb_pair( iol_type_attributes_t::type, iol_types_t::st_stream_disconnection ),
		iol_type_spcf_t::make_attrb_pair( iol_type_attributes_t::length, iol_length_spcf_t::prefixed ) );
	}

const rdm_sessional_descriptor_t& stream_odisconnection_t::sid()const noexcept {
	return _sid;
	}

stream_odisconnection_t::stream_odisconnection_t( const rdm_sessional_descriptor_t & sid )noexcept
	: base_type_t( mpf_type_spcf() ) 
	, _sid( sid ){}

size_t stream_odisconnection_t::get_serialized_size()const noexcept {
	return srlz::object_trait_serialized_size( _sid );
	}
//
//std::string stream_odisconnection_t::type_name()const noexcept  {
//	return srlz::make_type<stream_idisconnection_t>::name(); 
//	}
//type_identifier_t stream_odisconnection_t::type_id()const noexcept  {
//	return srlz::make_type<stream_idisconnection_t>::id(); 
//	}

stream_odisconnection_t::stream_odisconnection_t()noexcept {}










iol_type_spcf_t stream_idisconnection_t::mpf_type_spcf()noexcept {
	return iol_type_spcf_t::build(
		iol_type_spcf_t::make_attrb_pair( iol_type_attributes_t::type, iol_types_t::st_stream_disconnection ),
		iol_type_spcf_t::make_attrb_pair( iol_type_attributes_t::length, iol_length_spcf_t::prefixed ) );
	}

const rdm_sessional_descriptor_t& stream_idisconnection_t::sid()const noexcept {
	return _sid;
	}

stream_idisconnection_t::stream_idisconnection_t()noexcept
	: base_t( mpf_type_spcf() ) {}


size_t  stream_idisconnection_t::get_serialized_size()const noexcept {
	return srlz::object_trait_serialized_size( _sid );
	}
