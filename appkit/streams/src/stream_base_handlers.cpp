#include "../../internal.h"
#include "../i_streams.h"


using namespace crm;
using namespace crm::detail;


i_stream_whandler_t::i_stream_whandler_t( i_stream_whandler_t && o )noexcept
	: _ctx( std::move( o._ctx ) )
	, _connector( std::move( o._connector ) )
	, _clf( o._clf.load() )
#ifdef CBL_OBJECTS_COUNTER_CHECKER
	,  _xDbgCounter(std::move(o._xDbgCounter))
#endif
	{}

i_stream_whandler_t& i_stream_whandler_t::operator=( i_stream_whandler_t && o )noexcept {
	_ctx = std::move( o._ctx );
	_connector = std::move( o._connector );
	_clf = o._clf.load();

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	_xDbgCounter = std::move( o._xDbgCounter);
#endif

	return (*this);
	}

i_stream_whandler_t::i_stream_whandler_t( std::weak_ptr<distributed_ctx_t> ctx )
	: _ctx( std::move( ctx ) ){}

i_stream_whandler_t::~i_stream_whandler_t(){
	CHECK_NO_EXCEPTION( close() );
	}

void i_stream_whandler_t::bind_stream( std::shared_ptr<i_input_stream_t> s )noexcept{
	std::atomic_exchange(&_connector, std::move(s));
	}

void i_stream_whandler_t::close()noexcept{
	bool clf = false;
	if( _clf.compare_exchange_strong( clf, true ) ){
		if (auto pConnector = std::atomic_load_explicit(&_connector, std::memory_order_acquire)) {
			auto streamId = pConnector->stream_source_id();
			if (auto c = _ctx.lock()) {
				CHECK_NO_EXCEPTION(c->push_stream_writer(streamId));
				}
			}
		}
	}

