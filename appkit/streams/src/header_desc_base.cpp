#include "../../internal.h"
#include "../header_desc_base.h"
#include "../../context/context.h"
#include "../../exceptions.h"


using namespace crm;
using namespace crm::detail;


static_assert(srlz::is_serializable_v< stream_header_descriptor_xbase_t>, __FILE_LINE__);

stream_header_descriptor_t stream_header_descriptor_xbase_t::make_descriptor()const{
	auto ws = srlz::wstream_constructor_t::make();
	srlz::serialize( ws, (*this) );

	stream_header_descriptor_t d;
	d.header_typeid = descriptor_typeid();
	d.value = ws.release();

	return d;
	}

void stream_header_descriptor_xbase_t::from_descriptor( const stream_header_descriptor_t & xdesc ){

	if( xdesc.header_typeid == descriptor_typeid() ){

		auto rs = srlz::rstream_constructor_t::make( xdesc.value.cbegin(), xdesc.value.cend() );
		srlz::deserialize( rs, (*this) );
		}
	else{
		TYPES_MISMATCHING_THROW_EXC_FWD(nullptr);
		}
	}

