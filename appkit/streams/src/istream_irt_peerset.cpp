#include "../../internal.h"
#include "../istream.h"
#include "../istreams_routetbl.h"


using namespace crm;
using namespace crm::detail;


#ifdef CBL_OBJECTS_COUNTER_CHECKER
void istreams_set_t::__size_trace()const noexcept{
	CHECK_NO_EXCEPTION(__sizeTraceer.CounterCheck( _tbl.size() ));
	}
#endif

istreams_set_t::istreams_set_t( std::weak_ptr<distributed_ctx_t> ctx )
	: _ctx(ctx) {}

std::shared_ptr<istream_t> istreams_set_t::find( const rdm_sessional_descriptor_t & sid )const noexcept {
	lckscp_t lck( _lck );

	for( const auto & p : _tbl ) {
		auto sp = p.second.lock();
		if( sp && sp->session() == sid ) {
			return sp;
			}
		}

	return nullptr;
	}

/*size_t istreams_set_t::count( const i_stream_header_properties_t & sprop )const {
	lckscp_t lck( _lck );
	return __count( sprop );
	}*/

/*size_t istreams_set_t::__count( const i_stream_header_properties_t & sprop )const {
	
	size_t icnt = 0;
	for( const auto & p : _tbl ) {
		auto && pval = p.first.pval;
		if( pval ) {
			if( sprop.equally_key() == pval->equally_key() )
				++icnt;
			}
		}

	return icnt;
	}*/

/*std::list<std::shared_ptr<istream_t>> istreams_set_t::find( const i_stream_header_properties_t & sprop )const {
	lckscp_t lck( _lck );

	std::list<std::shared_ptr<istream_t>> sl;
	for( const auto & p : _tbl ) {
		auto && pval = p.first.pval;
		if( pval ) {
			if( sprop.equally_key() == pval->equally_key() ){
				auto sp = p.second.lock();
				if( sp )
					sl.push_back( std::move( sp ) );
				}
			}
		}

	return std::move(sl);
	}*/

bool istreams_set_t::__insert( std::shared_ptr<istream_t> s ) {

	/* требование уникальности заголовка */
	/*if( prop->equally_key().is_unique_requirement() ) {

		if( __count( *CHECK_PTR( prop ) ) ) {
			return false;
			}
		}*/

	for( const auto & p : _tbl ) {
		auto sp = p.second.lock();
		if( sp && sp->session() == s->session() ) {
			return false;
			}
		}

	auto key = s->stream_source_id();
	_tbl.insert( std::make_pair( key, std::move( s ) ) );

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	__size_trace();
#endif

	return true;
	}

bool istreams_set_t::insert( std::shared_ptr<istream_t> s ) {
	lckscp_t lck( _lck );
	return __insert( std::move( s ) );
	}

std::list<std::shared_ptr<istream_t>> istreams_set_t::erase( const rdm_sessional_descriptor_t & sid ) noexcept {
	std::list<std::shared_ptr<istream_t>> r;

	lckscp_t lck( _lck );

	auto it = _tbl.begin();
	while( it != _tbl.end() ) {

		auto sp = it->second.lock();
		if( sp ){
			if( sp->session() == sid ){
				it = _tbl.erase( it );

			#ifdef CBL_OBJECTS_COUNTER_CHECKER
				__size_trace();
			#endif

				r.push_back(std::move(sp));
				}
			else{
				++it;
				}
			}
		else{
			it = _tbl.erase( it );
			}
		}

	return r;
	}

void istreams_set_t::__close(
	std::function<void( std::list<std::shared_ptr<istream_t>> && s )> && closedf )noexcept {
	
	std::list<std::shared_ptr<istream_t>> sl;

	auto it = _tbl.begin();
	for( ; it != _tbl.end(); ++it ) {
		auto sp = it->second.lock();
		if( sp )
			sl.push_back( std::move( sp ) );
		}

	if( closedf ) {
		if(auto c = _ctx.lock()) {
			c->launch_async( __FILE_LINE__, std::move( closedf ), std::move( sl ) );
			}
		}

	_tbl.clear();

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	CHECK_NO_EXCEPTION( __size_trace() );
#endif
	}

void istreams_set_t::close( std::function<void( std::list<std::shared_ptr<istream_t>> && s )> && closedf )noexcept {
	lckscp_t lck( _lck );
	__close( std::move( closedf ) );
	}

void istreams_set_t::__close()noexcept {
	__close( nullptr );
	}

void istreams_set_t::close() noexcept {
	lckscp_t lck( _lck );
	__close();
	}

istreams_set_t::~istreams_set_t() {
	CHECK_NO_EXCEPTION( __close() );
	}
