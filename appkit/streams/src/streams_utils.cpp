#include "../../internal.h"
#include "../i_streams.h"
#include "../messages.h"


using namespace crm;
using namespace crm::detail;


static_assert(srlz::detail::is_i_srlzd_prefixed_v< stream_oheader_t>, __FILE_LINE__);
static_assert(srlz::detail::is_i_srlzd_v<stream_oheader_t>, __FILE_LINE__);
static_assert(std::is_default_constructible_v<stream_oheader_t>, __FILE_LINE__);
static_assert(srlz::is_ctr_as_i_srlzd_v<stream_oheader_t>, __FILE_LINE__);


i_stream_end_t::~i_stream_end_t(){}
i_stream_rhandler_t::~i_stream_rhandler_t(){}
stream_class_description::~stream_class_description(){}
i_stream_reader_t::~i_stream_reader_t(){}
i_stream_writer_t::~i_stream_writer_t(){}


/*bool crm::operator == (const i_header_equally_t & lval, const i_header_equally_t &rval)noexcept{
	return hf_equals(lval, rval);
	}

bool crm::operator != (const i_header_equally_t & lval, const i_header_equally_t &rval)noexcept{
	return !(lval == rval);
	}

bool crm::operator < (const i_header_equally_t & lval, const i_header_equally_t &rval) noexcept{
	return hf_less(lval, rval);
	}

bool crm::operator > (const i_header_equally_t & lval, const i_header_equally_t &rval) noexcept{
	return hf_great(lval, rval);
	}

bool crm::operator >= (const i_header_equally_t & lval, const i_header_equally_t &rval)noexcept{
	return hf_great_equals(lval, rval);
	}


bool crm::operator == (const i_stream_zyamba_header_t & lval, const i_stream_zyamba_header_t &rval) noexcept{
	return lval.properties().equally_key() == rval.properties().equally_key();
	}

bool crm::operator != (const i_stream_zyamba_header_t & lval, const i_stream_zyamba_header_t &rval)noexcept{
	return !(lval == rval);
	}

bool crm::operator < (const i_stream_zyamba_header_t & lval, const i_stream_zyamba_header_t &rval)noexcept{
	return lval.properties().equally_key() < rval.properties().equally_key();
	}

bool crm::operator >= (const i_stream_zyamba_header_t & lval, const i_stream_zyamba_header_t &rval)noexcept{
	return lval.properties().equally_key() >= rval.properties().equally_key();
	}*/


void crm::set_eof(stream_sequence_position_t & to){
	to = stream_sequence_position_t::maxvalue;
	}

bool crm::is_eof(const stream_sequence_position_t & v)noexcept{
	return v == stream_sequence_position_t::maxvalue;
	}




/*stream_session_t::stream_session_t(std::string_view  name_,
	std::vector<std::string> && keys_,
	const std::chrono::seconds & timeoutFail_)
	: name(name_)
	, keys(std::move(keys_))
	, timeoutFail(timeoutFail_){}


stream_session_t::stream_session_t(std::string_view  name_,
	std::vector<std::string>&& keys_,
	const std::chrono::seconds& timeoutFail_)
	: name(name_)
	, keys(std::move(keys_))
	, timeoutFail(timeoutFail_) {}
*/




/*const stream_source_descriptor_t& stream_blob_header_equality_t::stream_source_id()const noexcept{
	return _sourceId;
	}

const stream_blob_header_equality_t::hash_t& stream_blob_header_equality_t::object_hash()const noexcept{
	return _hash;
	}*/

//bool stream_blob_header_equality_t::equals_fields(const i_header_equally_t& r)const noexcept{
//
//	const auto & r1 = dynamic_cast<const stream_blob_header_equality_t&>(r);
//
//	if (stream_source_id() != r1.stream_source_id())
//		return false;
//
//	if (_keys.size() != r1._keys.size())
//		return false;
//
//	for (size_t i = 0; i < _keys.size(); ++i){
//		if (_keys[i] != r1._keys[i])
//			return false;
//		}
//
//	return true;
//	}
//
//bool stream_blob_header_equality_t::less_fields(const i_header_equally_t& r)const  noexcept{
//
//	const auto & r1 = dynamic_cast<const stream_blob_header_equality_t&>(r);
//
//	if (stream_source_id() < r1.stream_source_id())
//		return true;
//	else if (stream_source_id() > r1.stream_source_id())
//		return false;
//	else if (_keys.size() < r1._keys.size())
//		return true;
//	else if (_keys.size() > r1._keys.size())
//		return false;
//	else{
//		size_t isLess = 0;
//		size_t isGreat = 0;
//
//		for (size_t i = 0; i < _keys.size(); ++i){
//			if (_keys[i] > r1._keys[i]){
//
//				++isGreat;
//				break;
//				}
//			else if (_keys[i] < r1._keys[i]){
//				++isLess;
//				}
//			}
//
//		return 0 < isLess && 0 == isGreat;
//		}
//	}
//
//bool stream_blob_header_equality_t::great_fields(const i_header_equally_t& r)const noexcept{
//
//	const auto & r1 = dynamic_cast<const stream_blob_header_equality_t&>(r);
//
//	if (stream_source_id() > r1.stream_source_id())
//		return true;
//	else if (stream_source_id() < r1.stream_source_id())
//		return false;
//	else if (_keys.size() > r1._keys.size())
//		return true;
//	else if (_keys.size() < r1._keys.size())
//		return false;
//	else{
//		size_t isGreat = 0;
//		size_t isLess = 0;
//
//		for (size_t i = 0; i < _keys.size(); ++i){
//			if (_keys[i] < r1._keys[i]){
//
//				++isLess;
//				break;
//				}
//			else if (_keys[i] > r1._keys[i]){
//				++isGreat;
//				}
//			}
//
//		return 0 < isGreat && 0 == isLess;
//		}
//	}
//
//bool stream_blob_header_equality_t::great_equals_fields(const i_header_equally_t& r)const noexcept{
//
//	const auto & r1 = dynamic_cast<const stream_blob_header_equality_t&>(r);
//	if (stream_source_id() > r1.stream_source_id())
//		return true;
//	else if (stream_source_id() < r1.stream_source_id())
//		return false;
//	else if (_keys.size() > r1._keys.size())
//		return true;
//	else if (_keys.size() < r1._keys.size())
//		return false;
//	else{
//		size_t isGreatEq = 0;
//		for (size_t i = 0; i < _keys.size(); ++i){
//			if (_keys[i] < r1._keys[i]){
//				break;
//				}
//			else if (_keys[i] >= r1._keys[i]){
//				++isGreatEq;
//				}
//			}
//
//		return isGreatEq == _keys.size();
//		}
//	}
//
//bool stream_blob_header_equality_t::is_unique_requirement()const noexcept{
//	return true;
//	}
//
//std::string stream_blob_header_equality_t::description()const noexcept{
//	std::ostringstream os;
//	for (const auto & k : _keys){
//		os << k << ":";
//		}
//
//	return os.str();
//	}
//
//size_t stream_blob_header_equality_t::get_serialized_size()const noexcept{
//	return srlz::object_trait_serialized_size(_sourceId)
//		+ srlz::object_trait_serialized_size(_keys)
//		+ srlz::object_trait_serialized_size(_hash);
//	}
//
//void stream_blob_header_equality_t::update_hash()noexcept{
//	_hash = hash_t::null;
//
//	apply_at_hash(_hash, _sourceId);
//	apply_at_hash(_hash, _keys);
//	}
//
//stream_blob_header_equality_t::stream_blob_header_equality_t() noexcept{}
//
//stream_blob_header_equality_t::stream_blob_header_equality_t(const stream_source_descriptor_t & sourceId, const std::vector<std::string> & keys)
//	: _sourceId(sourceId)
//	, _keys(keys){
//
//	update_hash();
//	}
//
//const std::vector<std::string>& stream_blob_header_equality_t::keys()const noexcept{
//	return _keys;
//	}







//
//std::unique_ptr<i_stream_header_properties_t> stream_blob_header_properties_t::copy()const noexcept{
//	return std::make_unique<stream_blob_header_properties_t>((*this));
//	}
//
//size_t stream_blob_header_properties_t::get_serialized_size()const noexcept{
//	return srlz::object_trait_serialized_size(_eqVal);
//	}


//
//const i_header_equally_t& stream_blob_header_properties_t::equally_key()const noexcept{
//	return _eqVal;
//	}
//
//const address_hash_t& stream_blob_header_properties_t::object_hash()const noexcept{
//	return _eqVal.object_hash();
//	}
//
//bool stream_blob_header_properties_t::equals_fields(const stream_blob_header_properties_t& r)const  noexcept{
//	return _eqVal == r._eqVal;
//	}
//
//bool stream_blob_header_properties_t::less_fields(const stream_blob_header_properties_t& r)const  noexcept{
//	return _eqVal < r._eqVal;
//	}
//
//bool stream_blob_header_properties_t::great_fields(const stream_blob_header_properties_t& r)const  noexcept{
//	return _eqVal > r._eqVal;
//	}
//
//bool stream_blob_header_properties_t::great_equals_fields(const stream_blob_header_properties_t& r)const  noexcept{
//	return _eqVal >= r._eqVal;
//	}
//
//stream_blob_header_properties_t::stream_blob_header_properties_t()noexcept{}
//
//stream_blob_header_properties_t::stream_blob_header_properties_t(const stream_source_descriptor_t & sourceId,
//	const std::vector<std::string> & keys)
//	: _eqVal(sourceId, keys){}

//
//
//
//const srlz::i_typeid_t& stream_blob_header_t::stream_typeid()const noexcept{
//	return _tid;
//	}
//
//const i_stream_header_properties_t& stream_blob_header_t::properties()const noexcept{
//
//	}
//
///*const stream_header_descriptor_t& stream_blob_header_t::value()const noexcept{
//
//	}*/
//
//const address_hash_t& stream_blob_header_t::object_hash()const noexcept{
//
//	}
//
///*std::unique_ptr<i_stream_zyamba_header_t> stream_blob_header_t::copy()const noexcept{
//	return std::make_unique<stream_blob_header_t>(*this);
//	}*/
//
//
//void stream_blob_header_t::update_hash()noexcept{
//
//	}
//
//size_t stream_blob_header_t::get_serialized_size()const noexcept{
//
//	}
//
//stream_blob_header_t::stream_blob_header_t()noexcept{}
//
//stream_blob_header_t::stream_blob_header_t(stream_blob_header_properties_t && prop,
//	const srlz::typemap_key_t & tid/*,
//	stream_header_descriptor_t && desc*/)noexcept
//	: _tid(tid)
//	/*, _desc(std::move(desc))*/
//	, _prop(std::move(prop)){}
//
///*void stream_blob_header_t::deserialize(srlz::detail::rstream_base_handler & src) {
//	dsrlz(src);
//	}
//
//void stream_blob_header_t::serialize(srlz::detail::wstream_base_handler& dest)const {
//	srlz(dest);
//	}
//*/
