#include "../../internal.h"
#include "../istreams_routetbl.h"
#include "../istream.h"
#include "../../utilities/scopedin/postactor.h"
#include "../../xpeer/xpeer_push_handler.h"


using namespace crm;
using namespace crm::detail;


/*bool crm::detail::operator < (const istreams_set_t::key_t & lval,
	const istreams_set_t::key_t &rval)noexcept {

	if(lval.pval && rval.pval) {
		return lval.pval->equally_key() < rval.pval->equally_key();
		}
	else {
		if(rval.pval)
			return true;
		else if(lval.pval)
			return false;
		else
			return true;
		}
	}

bool crm::detail::operator >= (const istreams_set_t::key_t & lval,
	const istreams_set_t::key_t &rval)noexcept {

	if(lval.pval && rval.pval) {
		return lval.pval->equally_key() >= rval.pval->equally_key();
		}
	else {
		if(lval.pval)
			return true;
		else if( rval.pval ) {
			return false;
			}
		else {
			return true;
			}
		}
	}*/


#ifdef CBL_OBJECTS_COUNTER_CHECKER
void stream_route_itable_t::__size_trace()const noexcept {
	CHECK_NO_EXCEPTION(__sizeTraceer.CounterCheck( _sl.size() ));
	}
#endif


std::shared_ptr<distributed_ctx_t> stream_route_itable_t::ctx()const noexcept {
	return _ctx.lock();
	}

stream_route_itable_t::stream_route_itable_t( std::weak_ptr<distributed_ctx_t> ctx )
	: _ctx( ctx ){}

void stream_route_itable_t::push( const stream_key_type & ioDesc,
	std::unique_ptr<i_istream_line_t> && h,
	xpeer_push_handler_pin && io,
	__event_handler_point_f && eh ) {

	__push_dispatch( ioDesc, std::move( h ), std::move( io ), std::move( eh ) );
	}

void stream_route_itable_t::__push_header( const stream_key_type & ioDesc,
	std::unique_ptr<stream_iheader_t> && h,
	xpeer_push_handler_pin && io,
	__event_handler_point_f && eh ) {
	
	__make_stream( ioDesc, std::move( h ), std::move(io), std::move(eh) );
	}

void stream_route_itable_t::__push_tail( const stream_key_type & ioDesc,
	std::unique_ptr<stream_itail_t> && h,
	xpeer_push_handler_pin && io) {

	__tail_stream( ioDesc, std::move( h ), std::move( io ) );
	}

void stream_route_itable_t::__push_eof( const stream_key_type & ioDesc,
	std::unique_ptr<stream_iend_t> && h,
	xpeer_push_handler_pin && io ) {

	__eof_stream( ioDesc, std::move( h ), std::move( io ) );
	}

void stream_route_itable_t::__push_disconnection( const stream_key_type & ioDesc,
	std::unique_ptr<stream_idisconnection_t> && h,
	xpeer_push_handler_pin && io ) {

	__disconnection_stream( ioDesc, std::move( h ), std::move( io ) );
	}

std::shared_ptr<istreams_set_t> stream_route_itable_t::__get_stream_set( const stream_key_type ioDesc ) {
	auto itSet = _sl.find( ioDesc );
	if( itSet == _sl.end() ) {
		auto insPr = _sl.insert( std::make_pair( ioDesc, std::make_shared<istreams_set_t>( _ctx ) ) );
		if( !insPr.second ) {
			THROW_MPF_EXC_FWD(nullptr);
			}
		else {
			itSet = insPr.first;
			}
		}

	return itSet->second;
	}

void stream_route_itable_t::__make_stream( const stream_key_type & ioDesc,
	std::unique_ptr<stream_iheader_t> && ph_,
	xpeer_push_handler_pin && io,
	__event_handler_point_f && eh ) {

	lckscp_t lck( _slck );

	auto xioTbl = __get_stream_set( ioDesc );
	invoke_extern( io, std::move(ph_),
		[this, xioTbl, &io, &eh]( std::unique_ptr<stream_iheader_t> && h_) {

		MPF_TEST_EMULATION_IN_CONTEXT_COND_TRCX(ctx(), 700,
			is_trait_at_emulation_context(*h_),
			trait_at_keytrace_set(*h_));

		if(auto ctx_ = _ctx.lock()){

			/* �������������� �������� */
			auto ih = h_->identity().deserialize( ctx_ );
			if( !ih )
				EXTERN_CODE_THROW_EXC_FWD(nullptr);

			auto cd = h_->cover_data().deserialize(ctx_);

			auto isIt = xioTbl->find( h_->sid() );
			if( !isIt ){

				/* ������ ������ */
				auto[stream, whandler] = istream_t::make( _ctx, std::move( ih ), std::move(cd), h_->sid(), std::move( eh ) );
				if( stream && whandler ){				
					
					/* � ������� ������� */
					if( xioTbl->insert( stream ) ){
						__make_stream_end( stream, std::move( whandler ), std::move(h_), io );
						}
					else{
						THROW_EXC_FWD(nullptr);
						}
					}
				else{
					REFUSE_OPERATION_THROW_EXC_FWD(nullptr);
					}
				}
			else{
				if( !isIt->canceled() ){
					auto wh = isIt->handler();
					if( wh ){
						__make_stream_end( isIt, std::move( wh ), std::move(h_), io );
						}
					else{
						REFUSE_OPERATION_THROW_EXC_FWD(nullptr);
						}
					}
				else{
					REFUSE_OPERATION_THROW_EXC_FWD(nullptr);
					}
				}
			}
		else {
			THROW_EXC_FWD(nullptr);
			}
		 } );


#ifdef CBL_OBJECTS_COUNTER_CHECKER
	__size_trace();
#endif
	}


void stream_route_itable_t::__make_stream_end(const std::shared_ptr<istream_t> & s,
	std::shared_ptr<i_stream_whandler_t> && w,
	std::unique_ptr<stream_iheader_t> && h,
	const xpeer_push_handler_pin & io) {

	MPF_TEST_EMULATION_IN_CONTEXT_COND_TRCX(ctx(), 701,
		is_trait_at_emulation_context(*h),
		trait_at_keytrace_set(*h));

	if(s->is_possessing_of_writer()) {

		auto off = s->offset();
		stream_oheader_response_t pNext(off, h->sid());
		pNext.capture_response_tail((*h));

		__response(io, std::move(pNext));
		event_handler_invoke(s->get_event_handler(), std::make_unique<i_input_stream_begin_t>(std::move(w)));
		}
	else {
		stream_sequence_position_t offpos;
		set_eof(offpos);

		stream_oheader_response_t pNext(offpos, h->sid());
		pNext.capture_response_tail((*h));

		__response(io, std::move(pNext));
		}
	}

void stream_route_itable_t::__tail_stream( const stream_key_type & ioDesc,
	std::unique_ptr<stream_itail_t> && pt_,
	xpeer_push_handler_pin && io ) {

	lckscp_t lck( _slck );

	auto xioTbl = __get_stream_set( ioDesc );
	invoke_extern( io, std::move(pt_),
		[this, &xioTbl, &io, &lck](std::unique_ptr<stream_itail_t> && t) {

		MPF_TEST_EMULATION_IN_CONTEXT_COND_TRCX(ctx(), 702,
			is_trait_at_emulation_context(*t),
			trait_at_keytrace_set(*t));

		auto isIt = xioTbl->find( t->sid() );
		lck.unlock();

		if( isIt ) {
			if( !isIt->canceled() ) {

				auto off = isIt->tail( t->position(), std::move(*t).value() );
				CBL_VERIFY(t && !is_null(t->sid()));

				stream_otail_next_t pNext( off, t->sid() );
				pNext.capture_response_tail( (*t) );

				__response( io, std::move( pNext ) );
				}
			else {
				REFUSE_OPERATION_THROW_EXC_FWD(nullptr);
				}
			}
		else {
			THROW_EXC_FWD(nullptr);
			}
		} );
	}

void stream_route_itable_t::__eof_response( const xpeer_push_handler_pin & io,
	const std::unique_ptr<stream_iend_t> & e ) {

	stream_oend_t pNext( e->sid() );
	CHECK_NO_EXCEPTION(pNext.capture_response_tail((*e)));

	MPF_TEST_EMULATION_IN_CONTEXT_COND_TRCX(ctx(), 703,
		is_trait_at_emulation_context(*e),
		trait_at_keytrace_set(*e));

	__response( io, std::move( pNext ) );
	}

void stream_route_itable_t::__eof_stream( const stream_key_type & ioDesc,
	std::unique_ptr<stream_iend_t> && pe_,
	xpeer_push_handler_pin && io) {

	lckscp_t lck( _slck );

	auto xioTbl = __get_stream_set( ioDesc );
	invoke_extern( io, std::move(pe_),
		[this, &xioTbl, &io, &lck](std::unique_ptr<stream_iend_t> && e) {

		MPF_TEST_EMULATION_IN_CONTEXT_COND_TRCX(ctx(), 704,
			is_trait_at_emulation_context(*e),
			trait_at_keytrace_set(*e));

		auto isItList = xioTbl->erase( e->sid() );
		lck.unlock();

		for( auto && isIt : isItList ) {
			if( !isIt->canceled() ) {

				isIt->eof();
				close_async( isIt, stream_close_reason_t::remote_object_eof, nullptr );
				}
			else {
				REFUSE_OPERATION_THROW_EXC_FWD(nullptr);
				}
			}

		__eof_response( io, e );
		} );
	}

void stream_route_itable_t::__disconnection_stream( const stream_key_type & ioDesc,
	std::unique_ptr<stream_idisconnection_t> && pd_,
	xpeer_push_handler_pin && io) {

	lckscp_t lck( _slck );

	auto xioTbl = __get_stream_set( ioDesc );
	invoke_extern( io, std::move(pd_),
		[this, &xioTbl, &io, &lck](std::unique_ptr<stream_idisconnection_t> && d) {

		MPF_TEST_EMULATION_IN_CONTEXT_COND_TRCX(ctx(), 705,
			is_trait_at_emulation_context(*d),
			trait_at_keytrace_set(*d));

		auto isItList = xioTbl->erase( d->sid() );
		lck.unlock();

		for(auto && isIt : isItList){
			close_async( isIt, stream_close_reason_t::remote_object_disconnection, nullptr );
			}

		d->reset_invoke_exception_guard().reset();
		} );
	}

void stream_route_itable_t::__push_dispatch( const stream_key_type & ioDesc,
	std::unique_ptr<i_istream_line_t> && o,
	xpeer_push_handler_pin && io,
	__event_handler_point_f && eh )  {

	post_scope_action_t faultCtx( [this, &io, eh, &o] {
		if(o) {
			__response_exception(io,
				std::move(o),
				CREATE_PTR_EXC_FWD(nullptr));
			}
		} );

	MPF_TEST_EMULATION_IN_CONTEXT_COND_TRCX(ctx(), 720,
		is_trait_at_emulation_context(*o),
		trait_at_keytrace_set(*o));
	
	if( auto h = message_cast<stream_iheader_t>(std::move(o)) ) {
		__push_header( ioDesc, std::move( h ), std::move( io ), std::move( eh ) );
		}
	else if( auto t = message_cast<stream_itail_t>(std::move(o)) ) {
		__push_tail( ioDesc, std::move( t ), std::move( io ) );
		}
	else if( auto e = message_cast<stream_iend_t>(std::move(o)) ) {
		__push_eof( ioDesc, std::move( e ), std::move( io ) );
		}
	else if( auto d = message_cast<stream_idisconnection_t>(std::move(o)) ) {
		__push_disconnection( ioDesc, std::move( d ), std::move( io ) );
		}
	else {
		FATAL_ERROR_FWD(nullptr);
		}

	faultCtx.reset();
	}

void stream_route_itable_t::__close( const stream_key_type & ioDesc )noexcept {

	lckscp_t lck( _slck );

	auto io( _sl.find( ioDesc ) );
	if( io != _sl.end() ) {

		std::weak_ptr<self_t> wptr( shared_from_this() );
		auto pctx = _ctx;

		auto closedf = [wptr, pctx]
			( std::list<std::shared_ptr<istream_t>> && s )noexcept{

			auto pctx_ = pctx.lock();
			if( pctx_ ) {
				for( auto && is : s ) {

					pctx_->launch_async( __FILE_LINE__, [wptr]( std::shared_ptr<istream_t> && is ) {
						auto sptr( wptr.lock() );
						if( sptr ) {
							sptr->close_async( std::move( is ), stream_close_reason_t::remote_object_disconnection, nullptr);
							}
						}, std::move( is ) );
					}
				}
			};

		io->second->close( std::move( closedf ) );
		_sl.erase( io );

#ifdef CBL_OBJECTS_COUNTER_CHECKER
		__size_trace();
#endif
		}
	}

void stream_route_itable_t::__close_async(std::shared_ptr<istream_t> && s_,
	stream_close_reason_t r, std::unique_ptr<dcf_exception_t> && exc)const noexcept {

	if(auto c = _ctx.lock()) {
		if(exc)
			r = stream_close_reason_t::exception;

		c->launch_async(__FILE_LINE__,

			[wptr = weak_from_this(), r](std::unique_ptr<dcf_exception_t> && exc, std::shared_ptr<istream_t> && s_) {

			auto sptr(wptr.lock());
			if(sptr) {

				auto bs = s_ ? s_->handler() : nullptr;
				auto eh = s_ ? s_->get_event_handler() : __event_handler_point_f{};
				if(eh) {

					switch(r) {

						case stream_close_reason_t::remote_object_eof:
							sptr->event_handler_invoke(std::move(eh), std::make_unique<i_input_stream_eof_t>(std::move(bs)));
							break;

						case stream_close_reason_t::on_destination_side:
						case stream_close_reason_t::io_closed:
						case stream_close_reason_t::remote_object_disconnection:
							sptr->event_handler_invoke(std::move(eh), std::make_unique<i_input_stream_aborted_t>(std::move(bs)));
							break;

						case stream_close_reason_t::exception:
							sptr->event_handler_invoke(std::move(eh), std::make_unique<i_input_stream_exception_t>(std::move(bs), std::move(exc)));
							break;

						default:
							FATAL_ERROR_FWD(nullptr);
						}
					}
				}
			}, std::move(exc), std::move(s_));
		}
	}

void stream_route_itable_t::close(const stream_key_type & ioDesc) noexcept{
	if(auto c = _ctx.lock()) {
		c->launch_async(__FILE_LINE__, [wptr = weak_from_this()](const stream_key_type & ioDesc) {
			auto sptr(wptr.lock());
			if(sptr) {
				sptr->__close(ioDesc);
				}
			}, ioDesc);
		}
	}

void stream_route_itable_t::close_async( std::shared_ptr<istream_t> s, 
	stream_close_reason_t r, std::unique_ptr<dcf_exception_t> && exc )const noexcept {

	if( s ){
		if(auto c = _ctx.lock()) {
			c->launch_async(__FILE_LINE__, 
				[wptr = weak_from_this()](std::shared_ptr<istream_t> && s, stream_close_reason_t r, std::unique_ptr<dcf_exception_t> && exc) {			

				if(auto sptr = wptr.lock()) {
					sptr->__close_async(std::move(s), r, std::move(exc));
					}
				}, std::move(s), r, std::move(exc));
			}
		}
	}

void stream_route_itable_t::__handle_extern_exception(const xpeer_push_handler_pin & io,
	std::unique_ptr<i_istream_line_t> && hi,
	std::unique_ptr<dcf_exception_t> && exc)noexcept {

	const auto clf = is_extern_code_exception( exc );
	post_scope_action_t removef( [this, keySid = hi->sid(), &io, clf, &exc] {
		if( clf ) {
			__close_locked(io.local_id(), keySid, std::move(exc));
			}
		} );

	__response_exception( io, std::move(hi), exc->dcpy() );
	}

void stream_route_itable_t::__close( const stream_key_type & ioDesc,
	const rdm_sessional_descriptor_t & sid, stream_close_reason_t r ) noexcept {

	lckscp_t lck( _slck );

	auto p = _sl.find( ioDesc );
	if( p != _sl.end() ){

		auto sl = p->second->erase( sid );
		for(auto && s : sl ) {
			close_async( std::move( s ), r, nullptr );
			}
		}
	}

void stream_route_itable_t::__close_locked( const stream_key_type & ioDesc,
	const rdm_sessional_descriptor_t & sid, std::unique_ptr<dcf_exception_t> && exc )noexcept {

	auto p = _sl.find( ioDesc );
	if( p != _sl.end() ) {

		auto sl = p->second->erase( sid );
		for( auto && s : sl ) {
			close_async( std::move( s ), stream_close_reason_t::exception, std::move( exc ));
			}
		}
	}

void stream_route_itable_t::__response_exception( const xpeer_push_handler_pin & io,
	std::unique_ptr<i_istream_line_t> && hi,
	std::unique_ptr<dcf_exception_t> && exc )noexcept {

	if(hi) {
		try {
			if(is_sendrecv_request(*hi)) {

				onotification_t pe{};
				pe.capture_response_tail((*hi));
				pe.set_ntf(std::move(exc));

				CBL_VERIFY(!is_null(pe.address().destination_subscriber_id()));
				CBL_VERIFY(pe.type().fl_report());

				push2handler(io, std::move(pe));
				}
			else if(hi->type().type() == iol_types_t::st_stream_disconnection) {
				/* do nothing */

				ntf_hndl(std::move(exc));
				}
			else {
				FATAL_ERROR_FWD((*exc));
				}
			}
		catch(const dcf_exception_t & exc) {
			ntf_hndl(exc.dcpy());
			}
		catch(const std::exception & exc1) {
			ntf_hndl(CREATE_PTR_EXC_FWD(exc1));
			}
		catch(...) {
			ntf_hndl(CREATE_PTR_EXC_FWD(nullptr));
			}
		}
	}

void stream_route_itable_t::ntf_hndl( std::unique_ptr<dcf_exception_t> && ntf ) noexcept {
	auto ctx_( _ctx.lock() );
	if( ctx_ ) {
		ctx_->exc_hndl( std::move( ntf ) );
		}
	}

void stream_route_itable_t::event_handler_invoke(__event_handler_point_f && eh,
	std::unique_ptr<i_event_t> && value )const noexcept {

	if( eh ) {
		if( auto ctx_ = _ctx.lock()) {
			if( dynamic_cast<const i_userspace_event_t*>(value.get()) ) {
				ctx_->launch_async_exthndl( __FILE_LINE__, std::move( eh ), std::move( value ) );
				}
			else if( dynamic_cast<const i_syspace_event_t*>(value.get()) ) {
				ctx_->launch_async( __FILE_LINE__, std::move( eh ), std::move( value ));
				}
			else {
				FATAL_ERROR_FWD(nullptr);
				}
			}
		}
	}

void stream_route_itable_t::__close()noexcept {

	for( auto && io : _sl ) {
		if( io.second )
			io.second->close();
		}

	_sl.clear();

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	CHECK_NO_EXCEPTION( __size_trace() );
#endif
	}

void stream_route_itable_t::close()noexcept {
	lckscp_t lck( _slck );
	CHECK_NO_EXCEPTION( __close() );
	}

stream_route_itable_t::~stream_route_itable_t() {
	CHECK_NO_EXCEPTION( __close() );
	}
