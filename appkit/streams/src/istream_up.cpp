#include "../../internal.h"
#include "../istream.h"


using namespace crm;
using namespace crm::detail;


istream_t::istream_t(std::weak_ptr<distributed_ctx_t> ctx_,
	const rdm_sessional_descriptor_t & session_,
	__event_handler_point_f && clh)
	: _ctx(ctx_)
	, _session(session_)
	, _eh(std::move(clh)) {}

std::shared_ptr<i_stream_whandler_t> istream_t::ctr(std::unique_ptr<i_stream_zyamba_identity_t> && ih_, std::unique_ptr<i_stream_zyamba_cover_data_t>&& cd_) {

	_streamId = ih_->stream_source_id();

	/* ������ ������ */
	auto sw = CHECK_PTR(_ctx)->pop_stream_writer(std::move(ih_), std::move(cd_));
	if(sw) {
		_wh = sw;

		sw->bind_stream(shared_from_this());

		return sw;
		}
	else {
		EXTERN_CODE_THROW_EXC_FWD(nullptr);
		}
	}

std::tuple<std::shared_ptr<istream_t>, std::shared_ptr<i_stream_whandler_t>> istream_t::make(std::weak_ptr<distributed_ctx_t> ctx_,
	std::unique_ptr<i_stream_zyamba_identity_t> && ih_,
	std::unique_ptr<i_stream_zyamba_cover_data_t> && cd_,
	const rdm_sessional_descriptor_t & session,
	__event_handler_point_f && clh) {

	auto obj = std::make_shared<istream_t>(ctx_, session, std::move(clh));
	auto sobj = obj->ctr(std::move(ih_), std::move(cd_));

	return std::make_pair(std::move(obj), std::move(sobj));
	}

istream_t::~istream_t() {
	CHECK_NO_EXCEPTION(close());
	}

stream_sequence_position_t istream_t::offset()const noexcept {
	if(auto p = _wh.lock())
		return p->next_offset();
	else {
		return stream_sequence_position_t{};
		}
	}

bool istream_t::is_possessing_of_writer()const noexcept {
	if(auto p = _wh.lock())
		return p->is_possessing_of_writer();
	else
		return false;
	}

/*istream_header_properties istream_t::header()const{
	return _wh.lock();
	}*/

const stream_source_descriptor_t& istream_t::stream_source_id()const noexcept {
	return _streamId;
	}

stream_sequence_position_t istream_t::tail(const stream_sequence_position_t & offset,
	std::vector<uint8_t> && data) {

	return CHECK_PTR(_wh)->write(offset, std::move(data));
	}

void istream_t::eof()noexcept {
	if(!_eof) {
		auto sw = _wh.lock();
		if(sw) {
			CHECK_NO_EXCEPTION(sw->set_eof());
			}
		_eof = true;
		}
	}

void istream_t::close()noexcept{
	close(stream_close_reason_t::on_destination_side);
	}

void istream_t::close(stream_close_reason_t /*r*/) noexcept{}

bool istream_t::canceled()const noexcept{
	auto w = _wh.lock();
	if(w) {
		return w->canceled();
		}
	else {
		return true;
		}
	}

const rdm_sessional_descriptor_t& istream_t::session()const noexcept {
	return _session;
	}

__event_handler_point_f istream_t::get_event_handler()const noexcept{
	return _eh;
	}

std::shared_ptr<i_stream_whandler_t> istream_t::handler()const noexcept{
	return _wh.lock();
	}


