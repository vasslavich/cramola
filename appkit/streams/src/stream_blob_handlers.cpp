#include "../../internal.h"
#include "../handler_at_keys.h"


using namespace crm;
using namespace crm::detail;


bool wstream_handler_blob_t::is_possessing_of_writer()const noexcept {
	return nullptr != _w;
	}

stream_sequence_position_t wstream_handler_blob_t::eof_value()noexcept {
	stream_sequence_position_t eofm;
	crm::set_eof( eofm );

	return eofm;
	}

stream_sequence_position_t wstream_handler_blob_t::next_offset(){
	CBL_VERIFY(!_isReleased);
	CBL_VERIFY(!_isMoved);
	
	return CHECK_PTR( _w )->next_offset();
	}

stream_sequence_position_t wstream_handler_blob_t::write( const stream_sequence_position_t & offset, std::vector<uint8_t> && v ){
	CBL_VERIFY(!_isReleased);
	CBL_VERIFY(!_isMoved);

	if( !eof() ){
		return CHECK_PTR( _w )->write( offset, std::move( v ) );
		}
	else{
		return eof_value();
		}
	}

void wstream_handler_blob_t::set_eof()noexcept {
	_eof = true;
	}

wstream_handler_blob_t::wstream_handler_blob_t( std::shared_ptr<distributed_ctx_t> ctx, 
	std::unique_ptr<i_stream_writer_t> && w )
	: i_stream_whandler_t( std::move( ctx ) )
	, _w( std::move( w ) ){
	
	CBL_VERIFY(_w);
	}

void wstream_handler_blob_t::close( stream_close_reason_t rc )noexcept {
	if(_w)
		_w->close(rc);
	}

//const std::unique_ptr<i_stream_zyamba_identity_t>& wstream_handler_blob_t::stream_identity()const& noexcept {
//	return _w->stream_identity();
//	}
//
//const std::unique_ptr<i_stream_zyamba_cover_data_t>& wstream_handler_blob_t::stream_cover_data()const& noexcept {
//	return _w->stream_cover_data();
//	}
//
//std::unique_ptr<i_stream_zyamba_identity_t> wstream_handler_blob_t::stream_identity() && noexcept {
//	return std::move(*_w).stream_identity();
//	}
//
//std::unique_ptr<i_stream_zyamba_cover_data_t> wstream_handler_blob_t::stream_cover_data() && noexcept {
//	return std::move(*_w).stream_cover_data();
//	}

wstream_handler_blob_t::~wstream_handler_blob_t() {
	close(stream_close_reason_t::io_closed);
	}

std::unique_ptr<i_stream_whandler_t> wstream_handler_blob_t::make( std::shared_ptr<distributed_ctx_t> ctx,
	std::unique_ptr<i_stream_writer_t> && w ){

	return std::make_unique<wstream_handler_blob_t>( std::move( ctx ), std::move( w ) );
	}

wstream_handler_blob_t::wstream_handler_blob_t( wstream_handler_blob_t && o )
	: i_stream_whandler_t( std::forward<i_stream_whandler_t>( o ))
	, _w( std::move( o._w ) )
	, _cancelf( o._cancelf.load() )
	, _eof( o._eof.load() )

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	, _xDbgCounter( std::move( o._xDbgCounter ) )
#endif
	{
	
#ifdef USE_DCF_CHECK
	o._isMoved = true;
#endif
	}

wstream_handler_blob_t& wstream_handler_blob_t::operator=( wstream_handler_blob_t && o ){
	i_stream_whandler_t::operator=( std::move( o ) );

	_w = std::move( o._w );
	_cancelf = o._cancelf.load();
	_eof = o._eof.load();

#ifdef USE_DCF_CHECK
	o._isMoved = true;
#endif

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	_xDbgCounter = std::move( o._xDbgCounter );
#endif
	return (*this);
	}

bool wstream_handler_blob_t::eof()const noexcept {
	return _eof;
	}

const std::unique_ptr<i_stream_writer_t>& wstream_handler_blob_t::writer()const & noexcept{
	return _w;
	}

std::unique_ptr<i_stream_writer_t> wstream_handler_blob_t::writer()&& noexcept{
	if(eof()) {
#ifdef USE_DCF_CHECK
		_isReleased = true;
#endif

		return std::move(_w);
		}
	else {
		return {};
		}
	}

bool wstream_handler_blob_t::canceled()const noexcept {
	return _cancelf.load( std::memory_order::memory_order_acquire );
	}

void wstream_handler_blob_t::cancel()noexcept {
	_cancelf.store( true, std::memory_order::memory_order_release );
	}

