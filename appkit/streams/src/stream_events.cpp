#include "../../internal.h"
#include "../i_stream_events.h"

using namespace crm;
using namespace crm::detail;

i_input_stream_base_event_t::i_input_stream_base_event_t()noexcept {}

std::shared_ptr<i_stream_whandler_t> i_input_stream_base_event_t::holder()const noexcept {
	return _handler;
	}



const srlz::typemap_key_t i_input_stream_begin_t::class_type_id_ = srlz::typemap_key_t::make<i_input_stream_begin_t>();
const srlz::typemap_key_t& i_input_stream_begin_t::class_type_id()noexcept {
	return class_type_id_;
	}

const srlz::typemap_key_t& i_input_stream_begin_t::type_id()const noexcept {
	return class_type_id();
	}



const srlz::typemap_key_t i_input_stream_eof_t::class_type_id_ = srlz::typemap_key_t::make<i_input_stream_eof_t>();
const srlz::typemap_key_t& i_input_stream_eof_t::class_type_id()noexcept {
	return class_type_id_;
	}

const srlz::typemap_key_t& i_input_stream_eof_t::type_id()const noexcept {
	return class_type_id();
	}


const srlz::typemap_key_t i_input_stream_aborted_t::class_type_id_ = srlz::typemap_key_t::make<i_input_stream_aborted_t>();
const srlz::typemap_key_t& i_input_stream_aborted_t::class_type_id()noexcept {
	return class_type_id_;
	}

const srlz::typemap_key_t& i_input_stream_aborted_t::type_id()const noexcept {
	return class_type_id();
	}


const srlz::typemap_key_t i_input_stream_exception_t::class_type_id_ = srlz::typemap_key_t::make<i_input_stream_exception_t>();
const srlz::typemap_key_t& i_input_stream_exception_t::class_type_id()noexcept {
	return class_type_id_;
	}

const srlz::typemap_key_t& i_input_stream_exception_t::type_id()const noexcept {
	return class_type_id();
	}
