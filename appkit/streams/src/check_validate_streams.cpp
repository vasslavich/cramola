#include "../../internal.h"
#include "../stream_traits.h"


using namespace crm;
using namespace crm::detail;


static_assert(std::is_base_of_v<i_stream_zyamba_identity_t, stream_blob_identity_t< stream_container_params_t>>, __FILE_LINE__);
static_assert(is_stream_identity_trait_v< stream_blob_identity_t< stream_container_params_t>>, __FILE_LINE__);
static_assert(is_stream_identity_trait_v< stream_blob_identity_t< stream_file_descriptor_t>>, __FILE_LINE__);

template<typename TStreamParams>
struct dummy_rstream_reader {
	void read(const stream_sequence_position_t& offset, size_t blockSize, std::vector<uint8_t>& v);
	bool eof(const stream_sequence_position_t& offset)const noexcept;
	typename TStreamParams get_stream_params()const;
	const srlz::typemap_key_t& stream_typeid()const  noexcept;
	};
static_assert(is_stream_reader_v< dummy_rstream_reader<stream_container_params_t>>, __FILE_LINE__);
static_assert(is_stream_reader_v< dummy_rstream_reader<stream_file_descriptor_t>>, __FILE_LINE__);


struct dummy_cover_data final : i_stream_zyamba_cover_data_t {
	address_hash_t object_hash()const noexcept;
	const srlz::i_typeid_t& stream_typeid()const noexcept;

	template<typename S>
	void srlz(S&& dest)const {}

	template<typename S>
	void dsrlz(S&& src) {}

	size_t get_serialized_size()const noexcept;
	};
static_assert(has_hashable_support_v<dummy_cover_data>, __FILE_LINE__);
static_assert(srlz::is_serializable_v<dummy_cover_data>, __FILE_LINE__);
static_assert(is_stream_cover_data_trait_v<dummy_cover_data>, __FILE_LINE__);


static bool exchndl1(const std::unique_ptr<dcf_exception_t> & exch);
static_assert(is_remove_exception_handler_v<decltype(exchndl1)>, __FILE__);

static_assert(has_member_stream_typeid_v<xcontainter_stream_reader_adapter<std::vector<uint8_t>>>, __FILE_LINE__);
static_assert(has_member_stream_typeid_v<rstream_file_adapter_t>, __FILE_LINE__);
static_assert(has_member_stream_header_params_v<xcontainter_stream_reader_adapter<std::vector<uint8_t>>>, __FILE_LINE__);
static_assert(has_member_stream_header_params_v<rstream_file_adapter_t>, __FILE_LINE__);
static_assert(srlz::is_serializable_v<stream_container_params_t>, __FILE_LINE__);
static_assert(is_stream_reader_v<xcontainter_stream_reader_adapter<std::vector<uint8_t>>>, __FILE_LINE__);

using dummy_upload_handler_type = decltype(make_rstream_handler(
	std::declval<std::weak_ptr<distributed_ctx_t>>(),
	std::declval<dummy_rstream_reader<stream_container_params_t>>(),
	std::declval<dummy_cover_data>(),
	std::declval<size_t>(),
	std::declval<std::chrono::hours>(),
	std::declval<std::wstring>()));
static_assert(trait_upload_ostream_v<dummy_upload_handler_type>, __FILE_LINE__);

static_assert(std::is_base_of_v<i_stream_writer_t, wstream_container_t<std::vector<uint8_t>>>, __FILE_LINE__);
static_assert(is_stream_slot_based_by_interface_v<wstream_container_t<std::vector<uint8_t>>>, __FILE_LINE__);
static_assert(is_stream_slot_based_by_interface_v<wstream_file_t>, __FILE_LINE__);
