#pragma once


#include "../typeframe/typeid.h"
#include "./i_stream_events.h"
#include "./i_streams.h"
#include "./predefined_stream_identity.h"
#include "../counters/internal_terms.h"


namespace crm{


template<typename _TReader,
	typename _TStreamCoverData,
	typename _TIdentityType,
	typename TReader = std::decay_t<_TReader>,
	typename TStreamCoverData = std::decay_t<_TStreamCoverData>,
	typename TIdentityType = std::decay_t<_TIdentityType>,
	typename = std::enable_if_t<(
		is_stream_reader_v<TReader> &&
		is_stream_identity_trait_v<TIdentityType> &&
		is_stream_cover_data_trait_v<TStreamCoverData> &&
		std::is_same_v<typename std::decay_t<decltype(std::declval<const _TReader>().get_stream_params())>, typename TIdentityType::stream_params> &&
		std::is_constructible_v<TIdentityType, stream_source_descriptor_t, srlz::typemap_key_t, typename TIdentityType::stream_params>
		)>>
class rstream_handler_bundle_t final : public i_stream_rhandler_t{
public:
	using reader_type = typename TReader;
	using stream_params_type = typename std::decay_t<decltype(std::declval<const reader_type>().get_stream_params())>;
	using stream_cover_data_type = typename TStreamCoverData;
	using stream_identity_type = typename TIdentityType;
	using self_type = rstream_handler_bundle_t<_TReader, _TStreamCoverData, _TIdentityType>;

	template<typename _XReader,
		typename _XStreamCoverData,
		typename TClockRep,
		typename TClockPeriod,
		typename XReader = std::decay_t<_XReader>,
		typename XStreamCoverData = std::decay_t<_XStreamCoverData>,
		typename std::enable_if_t<(
			is_stream_reader_v<XReader> &&
			std::is_base_of_v<reader_type, std::decay_t<XReader>> &&
			is_stream_cover_data_trait_v<XStreamCoverData> &&
			std::is_base_of_v<stream_cover_data_type, XStreamCoverData>
			), int>* = 0>
		rstream_handler_bundle_t(const std::weak_ptr<distributed_ctx_t>& ctx,
			_XReader && r,
			_XStreamCoverData&& cd,
			size_t blockSize,
			std::wstring_view name,
			const std::chrono::duration<TClockRep, TClockPeriod> & timeout)
		: _r(std::forward<_XReader>(r))
		, _cd(std::forward<_TStreamCoverData>(cd))
		, _name(name)
		, _blockSize(blockSize)
		, _timeout(timeout)
		, _sourceId(CHECK_PTR(ctx)->make_global_object_identity_128()) 
		, _ReaderTypeid(make_stream_typeid(_r)){}

private:
	reader_type _r;
	stream_cover_data_type _cd;
	std::wstring _name;
	size_t _blockSize = 1024;
	std::chrono::seconds _timeout;
	stream_source_descriptor_t _sourceId;
	const srlz::typemap_key_t _ReaderTypeid;

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	utility::__xobject_counter_logger_t<rstream_handler_bundle_t> _xDbgCounter;
#endif

public:
	rstream_handler_bundle_t( const rstream_handler_bundle_t& ) = delete;
	rstream_handler_bundle_t& operator=( const rstream_handler_bundle_t& ) = delete;

	rstream_handler_bundle_t(rstream_handler_bundle_t&& o)noexcept
		: _r(std::move(o._r))
		, _cd(std::move(o._cd))
		, _name(std::move(o._name))
		, _blockSize(std::move(o._blockSize))
		, _timeout(std::move(o._timeout))
		, _sourceId(std::move(o._sourceId))
		, _ReaderTypeid(std::move(o._ReaderTypeid))

#ifdef CBL_OBJECTS_COUNTER_CHECKER
		, _xDbgCounter(std::move(o._xDbgCounter))
#endif
		{}

	rstream_handler_bundle_t& operator=(rstream_handler_bundle_t&& o)noexcept {
		if (this != std::addressof(o)) {
			_r = std:move(o._r);
			_cd = std::move(o._cd);
			_name = std::move(o._name);
			_blockSize = std::move(o._blockSize);
			_timeout = std::move(o._timeout);
			_sourceId = std::move(o._sourceId);
			_ReaderTypeid = std::move(o._ReaderTypeid);

#ifdef CBL_OBJECTS_COUNTER_CHECKER
			_xDbgCounter = std::move(o._xDbgCounter);
#endif
			}

		return (*this);
		}

	const stream_source_descriptor_t& source_id()const noexcept final{
		return _sourceId;
		}

	void read( const stream_sequence_position_t & offset, std::vector<uint8_t> & v )final{
		_r.read( offset, _blockSize, v );
		}

	bool eof( const stream_sequence_position_t & offset )const noexcept final{
		return _r.eof( offset );
		}

	stream_identity_type make_identity()const{
		return stream_identity_type( _sourceId, stream_typeid(), _r.get_stream_params() );
		}

	const stream_cover_data_type& make_cover_data()const & noexcept {
		return _cd;
		}

	stream_cover_data_type make_cover_data()&& noexcept {
		return std::move(_cd);
		}

	const srlz::typemap_key_t& stream_typeid()const noexcept final {
		return _ReaderTypeid;
		}
	};


template<typename TReader, 
	typename TStreamCoverData,
	typename TClockRep,
	typename TClockPeriod,
	typename std::enable_if_t<(
		is_stream_reader_v<TReader> &&
		is_stream_cover_data_trait_v<TStreamCoverData>
		), int> = 0>
auto make_rstream_handler(const std::weak_ptr<distributed_ctx_t>& ctx, 
	TReader&& r,
	TStreamCoverData&& cd,
	size_t blockSize,
	const std::chrono::duration<TClockRep, TClockPeriod>& timeout,
	std::wstring_view name) {

	using reader_type = std::decay_t<TReader>;
	using cover_data_type = std::decay_t<TStreamCoverData>;
	using stream_params_type = typename std::decay_t<decltype(std::declval<const reader_type>().get_stream_params())>;
	using identity_type = typename stream_blob_identity_t<stream_params_type>;
	using handler_type = typename rstream_handler_bundle_t<reader_type, cover_data_type, identity_type>;

	return handler_type(ctx,
		std::forward<TReader>(r), 
		std::forward<TStreamCoverData>(cd),
		blockSize,
		name,
		timeout);
	}


class wstream_handler_blob_t final : public i_stream_whandler_t{
private:
	std::unique_ptr<i_stream_writer_t> _w;
	std::atomic<bool> _cancelf = false;
	std::atomic<bool> _eof = false;

#ifdef USE_DCF_CHECK
	std::atomic<bool> _isReleased = false;
	std::atomic<bool> _isMoved = false;
#endif

#ifdef CBL_OBJECTS_COUNTER_CHECKER
	utility::__xobject_counter_logger_t<wstream_handler_blob_t, 1> _xDbgCounter;
#endif

	static stream_sequence_position_t eof_value()noexcept;
	stream_sequence_position_t next_offset()final;
	stream_sequence_position_t write( const stream_sequence_position_t & offset, std::vector<uint8_t> && v )final;

	/*const std::unique_ptr<i_stream_zyamba_identity_t>& stream_identity()const& noexcept final;
	const std::unique_ptr<i_stream_zyamba_cover_data_t>& stream_cover_data()const& noexcept final;

	std::unique_ptr<i_stream_zyamba_identity_t> stream_identity() && noexcept final ;
	std::unique_ptr<i_stream_zyamba_cover_data_t> stream_cover_data() && noexcept final;*/

	void set_eof()noexcept final;
	void close( detail::stream_close_reason_t rc )noexcept final;

public:
	~wstream_handler_blob_t();
	wstream_handler_blob_t( std::shared_ptr<distributed_ctx_t> ctx,
		std::unique_ptr<i_stream_writer_t> && w );

	static std::unique_ptr<i_stream_whandler_t> make( std::shared_ptr<distributed_ctx_t> ctx,
		std::unique_ptr<i_stream_writer_t> && w );

	wstream_handler_blob_t( const wstream_handler_blob_t  & ) = delete;
	wstream_handler_blob_t& operator=( const wstream_handler_blob_t & ) = delete;

	wstream_handler_blob_t( wstream_handler_blob_t && o );
	wstream_handler_blob_t& operator=( wstream_handler_blob_t && o );

	bool is_possessing_of_writer()const noexcept final;
	bool eof()const noexcept final;
	const std::unique_ptr<i_stream_writer_t>& writer()const & noexcept final;
	std::unique_ptr<i_stream_writer_t> writer()&& noexcept final;
	bool canceled()const noexcept final;
	void cancel()noexcept;
	};
}
