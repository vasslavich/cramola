#pragma once


#include "./i_streams.h"


namespace crm{


struct stream_header_descriptor_xbase_t : srlz::serialized_base_rw {

	virtual const srlz::typemap_key_t& descriptor_typeid()const = 0;

	stream_header_descriptor_t make_descriptor()const;
	void from_descriptor( const stream_header_descriptor_t & xdesc );
	};
}
