#pragma once


#include <type_traits>
#include "../exceptions.h"
#include "../channel_terms/events_terms.h"
#include "../xpeer_invoke/xpeer_sndrcv_cycle.h"
#include "./stream_traits.h"
#include "./messages.h"


namespace crm{
namespace detail{


template<typename TypeRStream, 
	typename _TRStream = typename std::decay_t<TypeRStream>,
	typename std::enable_if<(
		std::is_base_of<i_stream_rhandler_t, _TRStream>::value && 
		trait_upload_ostream_v<_TRStream>
		), int>::type = 0>
class ostream_actor{
private:
	std::weak_ptr<distributed_ctx_t> _ctx;
	std::unique_ptr<_TRStream> _rh;
	std::string _name;

	std::vector<uint8_t> read( const stream_sequence_position_t & pos )const{
		if( _rh ){
			std::vector<uint8_t> buf( CHECK_PTR( _ctx )->policies().stream_block_size() );
			_rh->read( pos, buf );

			return std::move( buf );
			}
		else{
			return {};
			}
		}

	bool eof( const stream_sequence_position_t & offset )const noexcept{
		if( _rh ){
			return crm::is_eof( offset ) || _rh->eof( offset );
			}
		else{
			return true;
			}
		}

	auto make_identity()const -> typename decltype(_rh->make_identity()){
		using identity_type = typename std::decay_t<decltype(_rh->make_identity())>;
		static const identity_type default_;

		if( _rh )
			return _rh->make_identity();
		else
			return default_;
		}

	auto make_cover_data()const -> typename decltype(_rh->make_cover_data()){
		using cover_data_type = typename std::decay_t<decltype(_rh->make_cover_data())>;
		static const cover_data_type default_;

		if (_rh)
			return _rh->make_cover_data();
		else
			return default_;
		}

	auto tail_header( const rdm_sessional_descriptor_t & s){
		return stream_oheader_t( make_identity(), make_cover_data(), CHECK_PTR(_ctx ), s );
		}

	auto tail_next( const stream_sequence_position_t & pos, const rdm_sessional_descriptor_t & s ){
		return stream_otail_t( read( pos ), pos, s );
		}

	auto tail_eof( const rdm_sessional_descriptor_t & s ){
		return stream_oend_t( s );
		}

	template<typename TOutMessage, typename TInputMessage>
	auto bind_sndrcv(TOutMessage&& om, std::unique_ptr<TInputMessage> & im)const noexcept {
		im->reset_invoke_exception_guard().reset();
		return std::forward<TOutMessage>(om);
		}

	template<typename TOutMessage>
	auto bind_sndrcv(TOutMessage&& om)const noexcept {
		return std::forward<TOutMessage>(om);
		}

public:
	async_space_t space()const noexcept{
		return async_space_t::ext;
		}

	auto make_close(const rdm_sessional_descriptor_t& s) {
		return stream_odisconnection_t(s);
		}

	template<typename TContinueConsumer>
	void next( TContinueConsumer && cc,
		const rdm_sessional_descriptor_t & s,
		std::unique_ptr<i_iol_ihandler_t> && inm_ ){

		if( !inm_ ){
			std::forward<TContinueConsumer>(cc)( bind_sndrcv(tail_header(s)) );
			}
		else if( auto hr = message_cast<stream_iheader_response_t>(std::move(inm_)) ){
			if( !eof( hr->ready_position() ) ){
				std::forward<TContinueConsumer>(cc)(bind_sndrcv(tail_next( hr->ready_position(), s ), hr));
				}
			else{
				std::forward<TContinueConsumer>(cc)(bind_sndrcv(tail_eof( s ), hr));
				}
			}
		else if( auto hn = message_cast<stream_itail_next_t>(std::move(inm_)) ){
			if( !eof( hn->position() ) ){
				std::forward<TContinueConsumer>(cc)(bind_sndrcv(tail_next( hn->position(), s ), hn));
				}
			else{
				std::forward<TContinueConsumer>(cc)(bind_sndrcv(tail_eof( s ), hn));
				}
			}
		else if( auto he = message_cast<stream_iend_t>(std::move(inm_)) ){
			he->reset_invoke_exception_guard().reset();

			std::forward<TContinueConsumer>(cc)(continue_condition_eof{});
			}
		else{
			std::forward<TContinueConsumer>(cc)(CREATE_NOT_FOUND_PTR_EXC_FWD(L"undefined conditional operation"));
			}
		}

	const std::string& name()const noexcept{
		return _name;
		}

	std::unique_ptr<i_stream_rhandler_t> release_handler()noexcept{
		return std::move(_rh);
		}

	ostream_actor()noexcept{}

	template<typename XTypeRStream,
		typename _XTRStream = typename std::decay_t<XTypeRStream>/*,
		typename std::enable_if<(
			std::is_base_of<i_stream_rhandler_t, _XTRStream>::value && detail::trait_upload_ostream_t<_XTRStream>::value), int>::type = 0*/>
	ostream_actor( std::weak_ptr<distributed_ctx_t> ctx, XTypeRStream && r, std::string_view name_ )
		: _ctx(ctx)
		, _rh( std::make_unique<_XTRStream>( std::forward<XTypeRStream>( r ) ) )
		, _name(name_){}
	};
}

template<typename TIO,
	typename TRStream,
	typename TEndHndl,
	typename TExcHndl,
	typename _TRStream = typename std::decay_t<TRStream>,
	typename _TIO = typename std::decay_t<TIO>,
	typename _TActor = typename detail::ostream_actor<_TRStream>,
	typename std::enable_if<(
		std::is_base_of<i_stream_rhandler_t, _TRStream>::value &&
		detail::trait_upload_ostream_v<_TRStream> &&
		detail::is_xpeer_trait_t<_TIO>::value &&
		detail::is_remove_exception_handler_v<TExcHndl>
		), int>::type = 0>
	auto make_ostream_handler(async_space_t scx,
		std::weak_ptr<distributed_ctx_t> ctx_,
		TIO && io,
		TRStream && r,
		TExcHndl && excHndl,
		TEndHndl && eHndl,
		std::string_view name = ""){

	static_assert(detail::is_sendrecv_cyclic_end_invokable_v<TEndHndl>, __FILE_LINE__);
	static_assert(detail::is_remote_exception_handler_invokable_v<TExcHndl>, __FILE_LINE__);
	static_assert(detail::is_xpeer_trait_z_v<TIO>, __FILE_LINE__);
	static_assert(detail::is_sendrecv_cyclic_actor_invokable_v<decltype(_TActor(ctx_, std::forward<TRStream>(r), name)), TIO>, __FILE_LINE__);

	return make_handler_sndrcv_cyclic(name,
		scx,
		ctx_, 
		std::forward<TIO>( io ),
		_TActor( ctx_, std::forward<TRStream>( r ), name),
		std::forward<TEndHndl>( eHndl ),
		std::forward<TExcHndl>( excHndl ),
		sndrcv_timeline_t::make_once_call() );
	}

template<typename TIO,
	typename TRStream,
	typename TEndHndl,
	typename TExcHndl,
	typename _TRStream = typename std::decay_t<TRStream>,
	typename _TIO = typename std::decay_t<TIO>,
	typename _TActor = typename detail::ostream_actor<_TRStream>,
	typename std::enable_if<(
		std::is_base_of<i_stream_rhandler_t, _TRStream>::value && 
		detail::trait_upload_ostream_v<_TRStream> &&
		detail::is_xpeer_trait_t<_TIO>::value &&
		detail::is_remove_exception_handler_v<TExcHndl>
		), int>::type = 0>
	void make_ostream_launch(async_space_t scx,
		std::weak_ptr<distributed_ctx_t> ctx_,
		TIO && io,
		TRStream && r,
		TExcHndl && excHndl,
		TEndHndl && eHndl,
		std::string_view name = ""){

	static_assert(detail::is_sendrecv_cyclic_end_invokable_v<TEndHndl>, __FILE_LINE__);
	static_assert(detail::is_remote_exception_handler_invokable_v<TExcHndl>, __FILE_LINE__);
	static_assert(detail::is_xpeer_trait_z_v<TIO>, __FILE_LINE__);
	static_assert(detail::is_sendrecv_cyclic_actor_invokable_v<decltype(_TActor(ctx_, std::forward<TRStream>(r), name)), TIO>, __FILE_LINE__);

	make_async_sndrcv_cyclic(name, 
		scx,
		ctx_,
		std::forward<TIO>( io ),
		_TActor( ctx_, std::forward<TRStream>( r ), name ),
		std::forward<TEndHndl>( eHndl ),
		std::forward<TExcHndl>( excHndl ),
		sndrcv_timeline_t::make_once_call() );
	}
}

