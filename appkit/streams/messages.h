#pragma once


#include "./i_stream_events.h"
#include "./i_streams.h"
#include "./stream_traits.h"
#include "../channel_terms/message_base_image.h"
#include "../channel_terms/rdm_sessional_desc.h"


namespace crm::detail {


class i_ostream_line_t :
	public i_iol_ohandler_t{
private:
	typedef i_iol_ohandler_t base_type_t;

protected:
	i_ostream_line_t()noexcept;

	i_ostream_line_t(iol_type_spcf_t type,
		const global_object_identity_t & id = global_object_identity_t::null)noexcept;

public:
	virtual const rdm_sessional_descriptor_t& sid()const noexcept = 0;
	};

class i_istream_line_t :
	public i_iol_ihandler_t {
private:
	typedef i_iol_ihandler_t base_type_t;

protected:
	i_istream_line_t(const iol_type_spcf_t & type,
		const global_object_identity_t & id = global_object_identity_t::null)noexcept;

public:
	virtual const rdm_sessional_descriptor_t& sid()const noexcept = 0;
	};


class stream_iheader_t final :
	public i_istream_line_t {

private:
	stream_xtype_t<i_stream_zyamba_identity_t> _si;
	stream_xtype_t<i_stream_zyamba_cover_data_t> _sd;
	rdm_sessional_descriptor_t _sid;

public:
	typedef i_istream_line_t base_t;
	static iol_type_spcf_t mpf_type_spcf()noexcept;

	stream_iheader_t()noexcept;

	template<typename S>
	void srlz(S &&dest)const {
		srlz::serialize(dest, _si);
		srlz::serialize(dest, _sd);
		srlz::serialize(dest, _sid);
		}

	template<typename S>
	void dsrlz(S && src) {
		srlz::deserialize(src, _si);
		srlz::deserialize(src, _sd);
		srlz::deserialize(src, _sid);
		}

	size_t get_serialized_size()const noexcept;

	const stream_xtype_t<i_stream_zyamba_identity_t>& identity()const noexcept;
	const stream_xtype_t<i_stream_zyamba_cover_data_t>& cover_data()const noexcept;

	const rdm_sessional_descriptor_t& sid()const noexcept final;
	};


class stream_oheader_t final :
	public i_ostream_line_t {

private:
	stream_xtype_t<i_stream_zyamba_identity_t> _si;
	stream_xtype_t<i_stream_zyamba_cover_data_t> _sd;
	rdm_sessional_descriptor_t _sid;

public:
	typedef i_ostream_line_t base_type_t;
	static iol_type_spcf_t mpf_type_spcf()noexcept;

	stream_oheader_t()noexcept;

	template<typename TStreamIdentity,
		typename TStreamCoverData,
		typename std::enable_if_t<(
			is_stream_identity_trait_v<TStreamIdentity> &&
			is_stream_cover_data_trait_v<TStreamCoverData>
			), int> = 0>
	stream_oheader_t(TStreamIdentity && id,
		TStreamCoverData && cv,
		const std::shared_ptr<distributed_ctx_t> & ctx,
		const rdm_sessional_descriptor_t & sid,
		const global_object_identity_t & glid = global_object_identity_t::null)noexcept
		: base_type_t(mpf_type_spcf(), glid)
		, _si(std::forward<TStreamIdentity>(id), ctx)
		, _sd(std::forward<TStreamCoverData>(cv), ctx)
		, _sid(sid) {}

	const rdm_sessional_descriptor_t & sid()const noexcept final;

	template<typename S>
	void srlz(S && dest)const {
		srlz::serialize(dest, _si);
		srlz::serialize(dest, _sd);
		srlz::serialize(dest, _sid);
		}

	template<typename S>
	void dsrlz(S && src) {
		srlz::deserialize(src, _si);
		srlz::deserialize(src, _sd);
		srlz::deserialize(src, _sid);
		}

	size_t get_serialized_size()const noexcept;

	//std::string type_name()const noexcept final { return srlz::make_type<stream_iheader_t>::name(); }
	//type_identifier_t type_id()const noexcept final { return srlz::make_type<stream_iheader_t>::id(); }

	};

class stream_iheader_response_t final :
	public i_istream_line_t {

private:
	stream_sequence_position_t _posReady;
	rdm_sessional_descriptor_t _sid;

public:
	typedef i_istream_line_t base_t;
	static iol_type_spcf_t mpf_type_spcf()noexcept;

	stream_iheader_response_t()noexcept;

	template<typename S>
	void srlz(S && dest)const {
		srlz::serialize(dest, _posReady);
		srlz::serialize(dest, _sid);
		}

	template<typename S>
	void dsrlz(S && src) {
		srlz::deserialize(src, _posReady);
		srlz::deserialize(src, _sid);
		}

	size_t get_serialized_size()const noexcept;

	const stream_sequence_position_t& ready_position()const noexcept;
	const rdm_sessional_descriptor_t& sid()const noexcept final;
	};

class stream_oheader_response_t final :
	public i_ostream_line_t {

private:
	stream_sequence_position_t _posReady;
	rdm_sessional_descriptor_t _sid;

public:
	typedef i_ostream_line_t base_type_t;
	static iol_type_spcf_t mpf_type_spcf()noexcept;

	stream_oheader_response_t() noexcept;
	stream_oheader_response_t(const stream_sequence_position_t & posReady,
		const rdm_sessional_descriptor_t & sid)noexcept;

	const rdm_sessional_descriptor_t& sid()const noexcept final;

	template<typename S>
	void srlz(S && dest)const {
		srlz::serialize(dest, _posReady);
		srlz::serialize(dest, _sid);
		}

	template<typename S>
	void dsrlz(S && src) {
		srlz::deserialize(src, _posReady);
		srlz::deserialize(src, _sid);
		}

	size_t get_serialized_size()const noexcept;

	//std::string type_name()const noexcept final { return srlz::make_type<stream_iheader_response_t>::name(); }
	//type_identifier_t type_id()const noexcept final { return srlz::make_type<stream_iheader_response_t>::id(); }
	};


class stream_itail_t final :
	public i_istream_line_t {

private:
	std::vector<uint8_t> _value;
	stream_sequence_position_t _pos;
	rdm_sessional_descriptor_t _sid;

public:
	typedef i_istream_line_t base_t;
	static iol_type_spcf_t mpf_type_spcf()noexcept;

	stream_itail_t()noexcept;

	template<typename S>
	void srlz(S && dest)const {
		srlz::serialize(dest, _value);
		srlz::serialize(dest, _pos);
		srlz::serialize(dest, _sid);
		}

	template<typename S>
	void dsrlz(S && src) {
		srlz::deserialize(src, _value);
		srlz::deserialize(src, _pos);
		srlz::deserialize(src, _sid);
		}

	size_t get_serialized_size()const noexcept;

	const std::vector<uint8_t>& value()const & noexcept;
	const stream_sequence_position_t& position()const noexcept;
	const rdm_sessional_descriptor_t& sid()const noexcept final;
	std::vector<uint8_t> value() && noexcept;
	};


class stream_otail_t final :
	public i_ostream_line_t {

private:
	std::vector<uint8_t> _value;
	stream_sequence_position_t _pos;
	rdm_sessional_descriptor_t _sid;

public:
	typedef i_ostream_line_t base_type_t;
	static iol_type_spcf_t mpf_type_spcf()noexcept;

	stream_otail_t()noexcept {}
	stream_otail_t(std::vector<uint8_t> && value,
		const stream_sequence_position_t & pos,
		const rdm_sessional_descriptor_t & sid)noexcept;

	const rdm_sessional_descriptor_t& sid()const noexcept final;

	template<typename S>
	void srlz(S && dest)const {
		srlz::serialize(dest, _value);
		srlz::serialize(dest, _pos);
		srlz::serialize(dest, _sid);
		}

	template<typename S>
	void dsrlz(S && src) {
		srlz::deserialize(src, _value);
		srlz::deserialize(src, _pos);
		srlz::deserialize(src, _sid);
		}

	size_t get_serialized_size()const noexcept;

	//std::string type_name()const noexcept final { return srlz::make_type<stream_itail_t>::name(); }
	//type_identifier_t type_id()const noexcept final { return srlz::make_type<stream_itail_t>::id(); }
	};


class stream_itail_next_t final :
	public i_istream_line_t {

private:
	stream_sequence_position_t _pos;
	rdm_sessional_descriptor_t _sid;

public:
	typedef i_istream_line_t base_t;
	static iol_type_spcf_t mpf_type_spcf()noexcept;

	stream_itail_next_t()noexcept;

	template<typename S>
	void srlz(S && dest)const {
		srlz::serialize(dest, _pos);
		srlz::serialize(dest, _sid);
		}

	template<typename S>
	void dsrlz(S && src) {
		srlz::deserialize(src, _pos);
		srlz::deserialize(src, _sid);
		}

	size_t get_serialized_size()const noexcept;

	const stream_sequence_position_t& position()const noexcept;
	const rdm_sessional_descriptor_t & sid()const noexcept final;
	};


class stream_otail_next_t final :
	public i_ostream_line_t {

private:
	stream_sequence_position_t _pos;
	rdm_sessional_descriptor_t _sid;

public:
	typedef i_ostream_line_t base_type_t;
	static iol_type_spcf_t mpf_type_spcf()noexcept;

	stream_otail_next_t()noexcept {}
	stream_otail_next_t(const stream_sequence_position_t & nextPos,
		const rdm_sessional_descriptor_t & sid)noexcept;

	const rdm_sessional_descriptor_t& sid()const noexcept final;

	template<typename S>
	void srlz(S && dest)const {
		srlz::serialize(dest, _pos);
		srlz::serialize(dest, _sid);
		}

	template<typename S>
	void dsrlz(S && src) {
		srlz::deserialize(src, _pos);
		srlz::deserialize(src, _sid);
		}

	size_t get_serialized_size()const noexcept;

	//std::string type_name()const noexcept final { return srlz::make_type<stream_itail_next_t>::name(); }
	//type_identifier_t type_id()const noexcept final { return srlz::make_type<stream_itail_next_t>::id(); }
	};


class stream_iend_t final :
	public i_istream_line_t {

private:
	rdm_sessional_descriptor_t _sid;

public:
	typedef i_istream_line_t base_t;
	static iol_type_spcf_t mpf_type_spcf()noexcept;

	stream_iend_t()noexcept;

	template<typename S>
	void srlz(S &&dest)const {
		srlz::serialize(dest, _sid);
		}

	template<typename S>
	void dsrlz(S && src) {
		srlz::deserialize(src, _sid);
		}

	size_t get_serialized_size()const noexcept;
	const rdm_sessional_descriptor_t& sid()const noexcept final;
	};


class stream_oend_t final :
	public i_ostream_line_t {

private:
	rdm_sessional_descriptor_t _sid;

public:
	typedef i_ostream_line_t base_type_t;
	static iol_type_spcf_t mpf_type_spcf()noexcept;

	stream_oend_t()noexcept;
	stream_oend_t(const rdm_sessional_descriptor_t & sid,
		const global_object_identity_t & id = global_object_identity_t::null)noexcept;

	const rdm_sessional_descriptor_t& sid()const noexcept final;

	template<typename S>
	void srlz(S && dest)const {
		srlz::serialize(dest, _sid);
		}

	template<typename S>
	void dsrlz(S && src) {
		srlz::deserialize(src, _sid);
		}

	size_t get_serialized_size()const noexcept;

	//std::string type_name()const noexcept final;
	//type_identifier_t type_id()const noexcept final;
	};


class stream_idisconnection_t final :
	public i_istream_line_t {

private:
	rdm_sessional_descriptor_t _sid;

public:
	typedef i_istream_line_t base_t;
	static iol_type_spcf_t mpf_type_spcf()noexcept;

	stream_idisconnection_t()noexcept;

	template<typename S>
	void srlz(S && dest)const {
		srlz::serialize(dest, _sid);
		}

	template<typename S>
	void dsrlz(S && src) {
		srlz::deserialize(src, _sid);
		}

	size_t get_serialized_size()const noexcept;
	const rdm_sessional_descriptor_t& sid()const noexcept final;
	};


class stream_odisconnection_t final :
	public i_ostream_line_t {

private:
	rdm_sessional_descriptor_t _sid;

public:
	typedef i_ostream_line_t base_type_t;
	static iol_type_spcf_t mpf_type_spcf()noexcept;

	stream_odisconnection_t() noexcept;
	stream_odisconnection_t(const rdm_sessional_descriptor_t & sid)noexcept;

	const rdm_sessional_descriptor_t& sid()const noexcept final;

	template<typename S>
	void srlz(S && dest)const {
		srlz::serialize(dest, _sid);
		}

	template<typename S>
	void dsrlz(S && src) {
		srlz::deserialize(src, _sid);
		}

	size_t get_serialized_size()const noexcept;
	//std::string type_name()const noexcept;
	//type_identifier_t type_id()const noexcept;
	};

bool is_stream_based(const std::unique_ptr<i_iol_ihandler_t> & iom)noexcept;
}
