#pragma once

#include <type_traits>
#include "./streams_properties_decls.h"

namespace crm {

template<typename T>
constexpr bool is_stream_identity_trait_v = is_stream_identity_trait<std::decay_t<T>>::value;

template<typename T>
constexpr bool is_stream_cover_data_trait_v = is_stream_cover_data_trait<std::decay_t<T>>::value;

template<typename TWStream>
constexpr bool is_write_stream_typemaker_v = is_write_stream_typemaker<std::decay_t<TWStream>>::value;

template<typename S>
constexpr bool has_type_declaration_stream_params_v = has_type_declaration_stream_params<S>::value;

template<typename S>
constexpr bool is_stream_slot_v = (
	(is_stream_slot_based_by_interface_v<S> || is_stream_slot_based_by_members_v<S>) &&
	has_type_declaration_stream_params_v<S> &&
	srlz::is_serializable_v<typename has_type_declaration_stream_params<S>::params_type>
	);

template<typename TR>
constexpr bool has_member_stream_typeid_v = has_member_stream_typeid < std::decay_t< TR> > ::value;

template<typename TR>
constexpr bool has_member_class_type_id_v = has_member_class_type_id<std::decay_t<TR>>::value;

template<typename T>
constexpr bool has_member_stream_header_params_v = has_member_header_params<T>::value;

template<typename TR>
constexpr bool is_stream_reader_form_interface_v = 
std::is_base_of_v<i_stream_reader_t, std::decay_t<TR>> && 
has_member_stream_header_params_v<TR>;

template<typename TR>
constexpr bool is_stream_reader_from_members_v = is_stream_reader_from_members<TR>::value;

template<typename TR>
constexpr bool is_stream_reader_v = is_stream_reader<std::decay_t<TR>>::value;

template<typename S>
constexpr bool is_stream_slot_based_by_interface_v = is_stream_slot_based_by_interface<S>::value;

template<typename S>
constexpr bool is_stream_slot_based_by_members_v = is_stream_slot_based_by_members<S>::value;
}
