#pragma once


#include "../messages/functor_package.h"
#include "../functorapp/functor_result.h"


namespace crm {

//
//struct result_handler {
//	using message_type = detail::istrob_message_envelop_t<detail::functor_result>;
//
//	std::unique_ptr<i_iol_ihandler_t> _obj;
//	std::unique_ptr<crm::dcf_exception_t> _exc;
//	std::chrono::microseconds _mcs;
//
//	result_handler(std::unique_ptr<i_iol_ihandler_t>&& obj,
//		std::unique_ptr<dcf_exception_t>&& exc,
//		std::chrono::microseconds&& mcs)noexcept
//		: _obj(std::move(obj))
//		, _exc(std::move(exc))
//		, _mcs(std::move(mcs)) {
//
//		if (_obj) {
//			CHECK_NO_EXCEPTION(_obj->reset_invoke_exception_guard().reset());
//			}
//		}
//
//	const std::chrono::microseconds& timeline()const noexcept {
//		return _mcs;
//		}
//
//	template<typename ... Ts>
//	decltype(auto) as_values()&& {
//		if (_obj && !_exc) {
//			if (const auto& pTyped = message_cast<message_type>(_obj)) {
//				return pTyped->value<0>().as<Ts...>();
//				}
//			else {
//				THROW_EXC_FWD(nullptr);
//				}
//			}
//		else if (auto pExc = dynamic_cast<dcf_exception_t*>(_exc.get())) {
//			pExc->rethrow();
//			THROW_EXC_FWD(nullptr);
//			}
//		else {
//			THROW_EXC_FWD(nullptr);
//			}
//		}
//
//	bool has_values()const noexcept {
//		if (_obj && !_exc) {
//			return true;
//			}
//		else {
//			return false;
//			}
//		}
//
//	bool has_error()const noexcept {
//		return nullptr != _exc;
//		}
//
//	const std::unique_ptr<dcf_exception_t>& error()const& noexcept {
//		return _exc;
//		}
//
//	std::unique_ptr<dcf_exception_t> error() && noexcept {
//		return std::move(_exc);
//		}
//	};
}


