#include "../../internal.h"

using namespace crm;
using namespace crm::detail;


functor_result crm::rpc_get_result(message_handler&&m) {
	auto&& [r] = std::move(m).as<functor_result>();
	return std::move(r);
	}

const functor_result& crm::rpc_get_result(const message_handler& m) {
	static_assert(std::is_lvalue_reference_v<decltype(m.as<functor_result>())>, __FILE_LINE__);

	const auto&[r] = m.as<functor_result>();
	return r;
	}

