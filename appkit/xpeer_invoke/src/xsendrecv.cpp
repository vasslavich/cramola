#include "../../internal.h"
#include "../xpeer_asendrecv.h"


using namespace crm;
using namespace crm::detail;


#ifdef CBL_MPF_MESSAGE_TRACEWAY_KEYS_BY_ID
std::atomic<std::intmax_t> crm::detail::SendRecvOperationsCounter{ 0 };

size_t crm::detail::get_stat_counter_sendrecv_operations()noexcept {
	return crm::detail::SendRecvOperationsCounter.load(std::memory_order::memory_order_acquire);
	}
#else
size_t crm::detail::get_stat_counter_sendrecv_operations()noexcept { return 0; }
#endif



#ifdef CBL_SNDRCV_USE_TRACE_SPIN_TAG
std::atomic<std::uintmax_t> crm::detail::GlobalMessageSndRcvTraceCounter{ 0 };
#endif


static_assert(is_xpeer_trait_z_v<xpeer_invoke_functor_t<peer_lnk_l1_t<i_xpeer_rt1_t>>> , __FILE_LINE__);