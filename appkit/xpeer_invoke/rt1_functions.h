#pragma once


#include "./xpeer_traits.h"
#include "./xpeer_asendrecv.h"
#include "./xpeer_asendrecv_coro.h"
#include "../eval/async_decls.h"
#include "../messages/trait_handler.h"


namespace crm {
namespace detail {


template<typename TCallbackSendReceive>
constexpr bool is_sndrcv_callback_overload_v =
is_sndrcv_callback_v< TCallbackSendReceive> ||
is_sndrcv_result_invocable_v<TCallbackSendReceive>;

template<typename _TIO,
	typename TIO = std::decay_t<_TIO>,
	typename std::enable_if_t<is_xpeer_trait_z_v<TIO>, int> = 0>
	class sendrecv_t {

	template<typename T>
	struct fault_callback_trait : std::false_type {};

	template<typename T>
	struct fault_request_trait : std::false_type {};

	private:
		std::weak_ptr<distributed_ctx_t> _ctx;
		TIO _io;

	public:
		template<typename XIO,
			typename std::enable_if_t<is_xpeer_trait_z_v<XIO>, int> = 0>
			sendrecv_t(std::weak_ptr<distributed_ctx_t> ctx_, XIO&& io)
			: _ctx(std::move(ctx_))
			, _io(std::forward<XIO>(io)) {}

	private:
		template<typename _TMessageObject,
			typename _THandler,
			typename TMessageObject = std::decay_t<_TMessageObject>,
			typename THandler = std::decay_t<_THandler>,
			typename std::enable_if_t<(
				is_sndrcv_invocable_result_v<THandler, TMessageObject>&&

				std::is_nothrow_move_constructible_v<TMessageObject>&&
				std::is_nothrow_move_assignable_v<TMessageObject>
				), int> = 0>
			struct ainvoke_result_handler {
			using self_type = ainvoke_result_handler<_TMessageObject, _THandler>;

			virtual ~ainvoke_result_handler() {}

			std::weak_ptr<distributed_ctx_t> _c;
			THandler _h;

			ainvoke_result_handler(const self_type&) = delete;
			self_type& operator=(const self_type&) = delete;

			ainvoke_result_handler(self_type&& o)noexcept
				: _c(std::move(o._c))
				, _h(std::move(o._h)) {}

			self_type& operator=(self_type&& o)noexcept {
				if (this != std::addressof(o)) {
					_c = std::move(o._c);
					_h = std::move(o._h);
					}

				return (*this);
				}

			template<typename XHandler>
			ainvoke_result_handler(std::weak_ptr<distributed_ctx_t> c, XHandler&& h)
				: _c(c)
				, _h(std::forward<XHandler>(h)) {}

			void exchndl(THandler& h, std::unique_ptr<dcf_exception_t>&& exc, std::chrono::microseconds mcs_)noexcept {
				CBL_NO_EXCEPTION_BEGIN
					if constexpr (is_functional_check_empty_v<THandler>) {
						if (h) {
							h({}, std::move(exc), std::move(mcs_));
							}
						}
					else {
						h({}, std::move(exc), std::move(mcs_));
						}
				CBL_NO_EXCEPTION_END(_c.lock())
				}

			bool has_handler()noexcept {
				if constexpr (is_functional_check_empty_v<THandler>) {
					return !_h;
					}
				else {
					return true;
					}
				}

			void operator()(message_handler&& m_)noexcept {

				if (has_handler()) {
					THandler h{ std::move(_h) };

					try {
						if (m_.has_result()) {
							if (m_.has_result_as<TMessageObject>()) {
								h(std::get<0>(std::move(m_).as<TMessageObject>()), {}, m_.timeline());
								m_.commit_receive();
								}
							else {
								h({}, CREATE_SERIALIZE_PTR_EXC_FWD(L"receive message has wrong type"), m_.timeline());
								}
							}
						else if (m_.has_exception()) {
							h({}, std::move(m_).exception(), m_.timeline());
							}
						else if (m_.has_canceled()) {
							h({},
								m_.has_exception() ? std::move(m_).exception() : CREATE_CANCELETION_PTR_FWD(L""),
								m_.timeline());
							}
						else {
							h({},
								m_.has_exception() ? std::move(m_).exception() : CREATE_ABANDONED_PTR_EXC_FWD(L""),
								m_.timeline());
							}
						}
					catch (const dcf_exception_t& e1) {
						CHECK_NO_EXCEPTION(exchndl(h, e1.dcpy(), m_.timeline()));
						}
					catch (const std::exception& e0) {
						CHECK_NO_EXCEPTION(exchndl(h, CREATE_MPF_PTR_EXC_FWD(e0), m_.timeline()));
						}
					catch (...) {
						CHECK_NO_EXCEPTION(exchndl(h, CREATE_MPF_PTR_EXC_FWD(nullptr), m_.timeline()));
						}
					}
				}
			};

	public:

		/*
		The base case of a generic function with handler object, meet requirements of
		generic response callback signature is_sndrcv_callback_overload_v:
			1. Request message type constructor meet requirements of is_xpeer_invokable_as_sndrcv_request_v(
				functor returning an object meet requirements of is_out_message_requirements_v),
			2. Response callback meet requirements of is_sndrcv_callback_overload_v,
			3. Return a handler of operation.
		*/
		template<typename TRequestCtr,
			typename TResultHandler,
			typename std::enable_if_t<(
				/* Request message constructor should be following the requirements of is_out_message_requirements_v*/
				is_xpeer_invokable_as_sndrcv_request_v<TRequestCtr>&&
				/*! Requirements for signature of result invocable object: void(message_handler &&) */
				is_sndrcv_callback_overload_v<TResultHandler>
				), int> = 0>
			auto hinvoke_callback_trait(
				TRequestCtr&& sendCtr,
				TResultHandler&& clb,
				trace_tag&& tag,
				const crm::sndrcv_timeline_t& timeline,
				launch_as lm_ = crm::launch_as::now) {

			if constexpr (is_sndrcv_callback_v<TResultHandler>) {
				return make_handler_sndrcv_once(_ctx,
					_io,
					std::forward<TRequestCtr>(sendCtr),
					std::forward<TResultHandler>(clb),
					std::move(tag),
					timeline,
					lm_);
				}
			else if constexpr (is_sndrcv_result_invocable_v<TResultHandler>) {
				using result_object_type = typename is_sndrcv_result_handler_trait<TResultHandler>::response_object_type;
				return make_handler_sndrcv_once(_ctx,
					_io,
					std::forward<TRequestCtr>(sendCtr),
					ainvoke_result_handler<result_object_type, TResultHandler>(_ctx, std::forward<TResultHandler>(clb)),
					std::move(tag),
					timeline,
					lm_);
				}
			else {
				static_assert(fault_callback_trait<TResultHandler>::value, "fault meet callback requirements");
				}
			}


		/*
The base case of a generic function with handler object, meet requirements of:
	1. Request message type constructor meet requirements of is_invoke_request_type_v,
	2. Response callback meet requirements of is_sndrcv_result_invocable_v.
*/
		template<typename TRequestCtr,
			typename TResultHandler,
			typename std::enable_if_t<(
				/* Request message constructor should be following the requirements of is_invoke_request_type_v*/
				is_invoke_request_type_v<TRequestCtr>&&
				/*! Requirements for signature of result invocable object is_sndrcv_callback_overload_v */
				is_sndrcv_callback_overload_v<TResultHandler>
				), int> = 0>
			auto hinvoke_callback(
				TRequestCtr&& sendCtr,
				TResultHandler&& clb,
				trace_tag&& tag,
				const crm::sndrcv_timeline_t& timeline,
				launch_as lm_ = crm::launch_as::now) {

			if constexpr (is_xpeer_invokable_as_sndrcv_request_v<TRequestCtr>) {
				return hinvoke_callback_trait(std::forward<TRequestCtr>(sendCtr),
					std::forward<TResultHandler>(clb),
					std::move(tag),
					timeline,
					lm_);
				}
			else if constexpr (is_out_message_requirements_v<TRequestCtr>) {
				return hinvoke_callback_trait(
					[m_ = std::move(sendCtr)]()mutable {return std::move(m_);},
					std::forward<TResultHandler>(clb),
					std::move(tag),
					timeline,
					lm_);
				}
			else if constexpr (srlz::is_serializable_v<TRequestCtr>) {
				auto reqctr = [m_ = std::move(sendCtr)]()mutable{
					return ostrob_message_envelop_t<TRequestCtr>(std::move(m_));
					};
				static_assert(std::is_same_v< ostrob_message_envelop_t<TRequestCtr>, std::invoke_result_t<decltype(reqctr)>>, "-");
				static_assert(is_xpeer_invokable_as_sndrcv_request_v<decltype(reqctr)>, "");

				return hinvoke_callback_trait(std::move(reqctr),
					std::forward<TResultHandler>(clb),
					std::move(tag),
					timeline,
					lm_);
				}
			else {
				static_assert(fault_request_trait<TRequestCtr>::value, "fault meet the request requirements");
				}
			}


		/*
The base case of a generic asynchronuos function, meet requirements of
generic response callback signature is_sndrcv_callback_v:
	1. Request message type constructor meet requirements of is_xpeer_invokable_as_sndrcv_request_v(
		functor returning an object meet requirements of is_out_message_requirements_v),
	2. Response callback meet requirements of is_sndrcv_callback_v: void(message_handler &&),
		*/
		template<typename TRequestCtr,
			typename TResultHandler,
			typename std::enable_if_t<(
				/* Request message constructor should be following the requirements of is_out_message_requirements_v*/
				is_xpeer_invokable_as_sndrcv_request_v<TRequestCtr>&&

				/* Result handler should be following common requirements of is_sndrcv_callback_overload_v */
				is_sndrcv_callback_overload_v<TResultHandler>
				), int> = 0>
			void ainvoke_calback_trait(
				TRequestCtr&& sendCtr,
				TResultHandler&& clb,
				trace_tag&& tag,
				const crm::sndrcv_timeline_t& timeline,
				launch_as lm_ = launch_as::async) {

			if constexpr (is_sndrcv_callback_v<TResultHandler>) {
				make_async_sndrcv_once(_ctx,
					_io,
					std::forward<TRequestCtr>(sendCtr),
					std::forward<TResultHandler>(clb),
					std::move(tag),
					timeline,
					lm_);
				}
			else if constexpr (is_sndrcv_result_invocable_v<TResultHandler>) {
				using result_object_type = typename is_sndrcv_result_handler_trait<TResultHandler>::response_object_type;
				make_async_sndrcv_once(_ctx,
					_io,
					std::forward<TRequestCtr>(sendCtr),
					ainvoke_result_handler<result_object_type, TResultHandler>(_ctx, std::forward<TResultHandler>(clb)),
					std::move(tag),
					timeline,
					lm_);
				}
			else {
				static_assert(fault_callback_trait<TResultHandler>::value, "fault meet callback requirements");
				}
			}

		/*
The base case of a generic asynchronuos function, meet requirements of:
	1. Request message type constructor meet requirements of is_invoke_request_type_v,
	2. Response callback meet requirements of is_sndrcv_callback_overload_v.
		*/
		template<typename TRequestCtr,
			typename TResultHandler,
			typename std::enable_if_t<(
				/* Request message constructor should be following the requirements of is_invoke_request_type_v*/
				is_invoke_request_type_v<TRequestCtr>&&

				/* Result handler should be following common requirements of is_sndrcv_callback_overload_v */
				is_sndrcv_callback_overload_v<TResultHandler>
				), int> = 0>
			void ainvoke_callback(
				TRequestCtr&& sendCtr,
				TResultHandler&& clb,
				trace_tag&& tag,
				const crm::sndrcv_timeline_t& timeline,
				launch_as lm_ = launch_as::async) {

			if constexpr (is_xpeer_invokable_as_sndrcv_request_v<TRequestCtr>) {
				ainvoke_calback_trait(std::forward<TRequestCtr>(sendCtr),
					std::forward<TResultHandler>(clb),
					std::move(tag),
					timeline,
					lm_);
				}
			else if constexpr (is_out_message_requirements_v<TRequestCtr>) {
				ainvoke_calback_trait(
					[m_ = std::move(sendCtr)]()mutable{return std::move(m_); },
					std::forward<TResultHandler>(clb),
					std::move(tag),
					timeline,
					lm_);
				}
			else if constexpr (srlz::is_serializable_v<TRequestCtr> && !is_out_message_requirements_v<TRequestCtr>) {
				ainvoke_calback_trait(
					[m_ = std::move(sendCtr)]()mutable { return ostrob_message_envelop_t<TRequestCtr>(std::move(m_)); },
					std::forward<TResultHandler>(clb),
					std::move(tag),
					timeline,
					lm_);
				}
			else {
				static_assert(fault_request_trait<TRequestCtr>::value, "fault meet the request requirements");
				}
			}

		/*
Generic asynchronous function with coroutines requirements support:
	1. request message type meet requirements of is_invoke_request_type_v,
	2. return type has type of message_handler.
*/
		template<typename TRequestCtr,
			typename std::enable_if_t<(
				is_invoke_request_type_v<TRequestCtr>
				), int> = 0>
			auto coro_invoke(
				async_space_t scx,
				TRequestCtr&& sendCtr,
				trace_tag&& tag,
				const crm::sndrcv_timeline_t& timeline) {

			if constexpr (is_xpeer_invokable_as_sndrcv_request_v<TRequestCtr>) {
				return make_coro_sndrcv_once(scx,
					_ctx,
					_io,
					std::forward<TRequestCtr>(sendCtr),
					std::move(tag),
					timeline);
				}
			else if constexpr (is_out_message_requirements_v<TRequestCtr>) {
				return make_coro_sndrcv_once(scx,
					_ctx,
					_io,
					[m_ = std::move(sendCtr)]()mutable{ return std::move(m_); },
					std::move(tag),
					timeline);
				}
			else if constexpr (srlz::is_serializable_v<TRequestCtr>) {
				return make_coro_sndrcv_once(scx,
					_ctx,
					_io,
					[m_ = std::move(sendCtr)]()mutable { return ostrob_message_envelop_t<TRequestCtr>(std::move(m_)); },
					std::move(tag),
					timeline);
				}
			else {
				static_assert(fault_request_trait<TRequestCtr>::value, "fault meet the request requirements");
				}
			}

	private:
		template<typename T>
		struct fault_result_trait : std::false_type {};

	public:

		/*
Generic blocked function with:
	1. The request object is defined as a constructor for type that meet requirements of
		is_xpeer_invokable_as_sndrcv_request_v,
	2. return type has type of message_handler.
*/
		template<typename TRequestCtr,
			typename std::enable_if_t<(
				is_xpeer_invokable_as_sndrcv_request_v<TRequestCtr>
				), int> = 0>
		message_handler binvoke_trait(
				TRequestCtr&& sendCtr,
				trace_tag&& tag,
				const crm::sndrcv_timeline_t& timeline,
				launch_as lm_ = crm::launch_as::now) {

			class state_t {
			public:
				message_handler m;
				std::promise<void> resultWatcher;
				};

			auto st = std::make_shared<state_t>();
			auto asyncResult = st->resultWatcher.get_future().share();

			auto a1 = asyncResult;
			auto resultHndl = [st](message_handler&& m_)noexcept {
				CHECK_NO_EXCEPTION_BEGIN

					st->m = std::move(m_);

				if (st->m.has_result()) {
					CBL_MPF_KEYTRACE_SET((*st->m.source_message()), 269);
					}

				post_scope_action_t resultWatcherTrigger([&st] {
					st->resultWatcher.set_value();
					});

				CHECK_NO_EXCEPTION_END
				};
			static_assert(is_sndrcv_callback_no_throw_v<decltype(resultHndl)>, __FILE_LINE__);

			if (auto ctx_ = _ctx.lock()) {
				auto hndl = make_handler_sndrcv_once(_ctx,
					_io,
					std::forward<TRequestCtr>(sendCtr),
					std::move(resultHndl),
					std::move(tag),
					timeline,
					lm_);

				auto checkInterval_ = (timeline.is_timeouted() ?
					std::min(ctx_->policies().timeout_probe_async(), timeline.timeout()) :
					ctx_->policies().timeout_probe_async());
				auto endPoint = timeline.deadline_point();

				bool waitResult = false;
				do {
					waitResult = (std::future_status::ready == asyncResult.wait_for(checkInterval_));

					if (!waitResult & ctx_->stopped()) {
						THROW_EXC_FWD("IO context stopped");
						}
					} while (!waitResult && std::chrono::system_clock::now() < endPoint);

				if (waitResult)
					return std::move(st->m);
				else {
					return message_handler{ async_operation_result_t::st_timeouted, 
						{}, 
						CREATE_TIMEOUT_PTR_EXC_FWD(nullptr), 
						std::chrono::duration(std::chrono::system_clock::now() - timeline.start_point()) };
					}
				}
			else {
				THROW_EXC_FWD(nullptr);
				}
			}

		/*
Generic blocked function with:
	1. The request object is defined as a constructor for type that meet requirements of
		is_invoke_request_type_v,
	2. return type has type of message_handler.
*/
		template<typename TRequestMessage,
			typename _XMessage = typename std::decay_t<TRequestMessage>,
			typename std::enable_if_t<(
				/* The request object is defined as:
					1. not a constructor for type that meet requirements of is_invoke_request_type_v*/
				is_invoke_request_type_v<TRequestMessage>
				), int> = 0>
			message_handler binvoke(
				TRequestMessage&& m,
				trace_tag&& tag,
				const crm::sndrcv_timeline_t& timeline,
				launch_as lm_ = crm::launch_as::now) {

			if constexpr (is_xpeer_invokable_as_sndrcv_request_v<TRequestMessage>) {
				return binvoke_trait(
					std::forward<TRequestMessage>(m),
					std::move(tag),
					timeline,
					lm_);
				}
			else if constexpr (is_out_message_requirements_v<TRequestMessage>) {
				return binvoke_trait(
					[m_ = std::move(m)]()mutable { return std::move(m_); },
					std::move(tag),
					timeline,
					lm_);
				}
			else if constexpr (srlz::is_serializable_v<TRequestMessage>) {
				return binvoke_trait(
					[m_ = std::move(m)]()mutable{ return ostrob_message_envelop_t<TRequestMessage>(std::move(m_)); },
					std::move(tag),
					timeline,
					lm_);
				}
			else {
				static_assert(fault_request_trait<TRequestMessage>::value, "fault meet the request requirements");
				}
			}
	};
}

template<typename TIO,
	typename TRequest,
	typename TResultHandler,
	typename std::enable_if_t<(
		/*! Requirements for peer object type */
		detail::is_xpeer_trait_z_v<TIO> &&

		/* Request message constructor should be meet the requirements of is_out_message_requirements_v*/
		detail::is_invoke_request_type_v<TRequest> &&

		/*! Requirements for signature of result invocable object: void(message_handler &&) */
		detail::is_sndrcv_callback_overload_v<TResultHandler>
		), int> = 0>
	decltype(auto) hainvoke(std::weak_ptr<distributed_ctx_t> ctx,
		TIO&& xio,
		TRequest&& sendm,
		TResultHandler&& clb,
		trace_tag&& tag,
		const crm::sndrcv_timeline_t& timeline,
		launch_as lm_ = crm::launch_as::now) {

	using invokable_object = typename std::decay_t<decltype(detail::make_invokable_xpeer(std::forward<TIO>(xio)))>;

	detail::sendrecv_t<invokable_object> op(std::move(ctx), detail::make_invokable_xpeer(std::forward<TIO>(xio)));
	return op.hinvoke_callback(std::forward<TRequest>(sendm),
		std::forward<TResultHandler>(clb),
		std::move(tag),
		timeline,
		lm_);
	}


template<typename TIO,
	typename TRequest,
	typename TResultHandler,
	typename std::enable_if_t<(
		/*! Requirements for peer object type */
		detail::is_xpeer_trait_z_v<TIO>&&

		/* Request message constructor should be meet the requirements of is_out_message_requirements_v*/
		detail::is_invoke_request_type_v<TRequest>&&

		/*! Requirements for signature of result invocable object: void(message_handler &&) */
		detail::is_sndrcv_callback_overload_v<TResultHandler>
		), int> = 0>
	void ainvoke(std::weak_ptr<distributed_ctx_t> ctx,
		TIO && xio,
		TRequest&& sendm,
		TResultHandler && clb,
		trace_tag && tag,
		const crm::sndrcv_timeline_t & timeline,
		launch_as m = launch_as::async) {

	using invokable_object = typename std::decay_t<decltype(detail::make_invokable_xpeer(std::forward<TIO>(xio)))>;

	detail::sendrecv_t<invokable_object> op(std::move(ctx), detail::make_invokable_xpeer(std::forward<TIO>(xio)));
	return op.ainvoke_callback(std::forward<TRequest>(sendm),
		std::forward<TResultHandler>(clb),
		std::move(tag),
		timeline,
		m);
	}

template<typename TIO,
	typename TRequest,
	typename std::enable_if_t<(
		/*! Requirements for peer object type */
		detail::is_xpeer_trait_z_v<TIO>&&

		/* Request message constructor should be meet the requirements of is_out_message_requirements_v*/
		detail::is_invoke_request_type_v<TRequest>
		), int> = 0>
	auto coro_invoke(std::weak_ptr<distributed_ctx_t> ctx,
		TIO&& xio,
		TRequest && sendm,
		trace_tag&& tag,
		const crm::sndrcv_timeline_t& timeline,
		async_space_t scx = async_space_t::ext) {

	using invokable_object = typename std::decay_t<decltype(detail::make_invokable_xpeer(std::forward<TIO>(xio)))>;

	detail::sendrecv_t<invokable_object> op(std::move(ctx), detail::make_invokable_xpeer(std::forward<TIO>(xio)));
	return op.coro_invoke(scx,
		std::forward<TRequest>(sendm),
		std::move(tag),
		timeline);
	}

template<typename TIO,
	typename TRequest,
	typename std::enable_if_t<(
		/*! Requirements for peer object type */
		detail::is_xpeer_trait_z_v<TIO>&&

		/* Request message constructor should be meet the requirements of is_out_message_requirements_v*/
		detail::is_invoke_request_type_v<TRequest>
		), int> = 0>
	message_handler binvoke(std::weak_ptr<distributed_ctx_t> ctx,
		TIO && xio,
		TRequest&& sendm,
		trace_tag && tag,
		const crm::sndrcv_timeline_t & timeline,
		launch_as m = launch_as::async) {

	using invokable_object = typename std::decay_t<decltype(detail::make_invokable_xpeer(std::forward<TIO>(xio)))>;
	using invokable_point = typename detail::sendrecv_t<invokable_object>;

	invokable_point op(std::move(ctx), detail::make_invokable_xpeer(std::forward<TIO>(xio)));
	return op.binvoke(std::forward<TRequest>(sendm),
		std::move(tag),
		timeline,
		m );
	}
}


