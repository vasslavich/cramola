#pragma once


#include <type_traits>
#include "../internal_invariants.h"
#include "../xpeer/base_terms.h"


namespace crm {
namespace detail {


template<typename T, typename Enable = void>
struct is_xpeer_trait_z_t : public std::false_type {};

template<typename T>
struct is_xpeer_trait_z_t < T,
	std::void_t<
	typename std::enable_if_t<is_xpeer_push_trait_z_v<T>>,
	decltype(std::declval<T>().level()),
	decltype(std::declval<T>().closed()),
	decltype(std::declval<T>().renewable_connection()),
	decltype(std::declval<T>().remote_object_endpoint()),
	decltype(std::declval<T>().direction()),
	decltype(std::declval<T>().subscribe_hndl_id(async_space_t::undef, std::declval<const _address_hndl_t>(), subscribe_options{}, std::declval<subscribe_message_handler>())),
	decltype(std::declval<T>().unsubscribe_hndl_id(std::declval<const _address_hndl_t>())),
	decltype(std::declval<T>().subscribe_event(async_space_t::undef, std::declval<const _address_event_t>(), false, std::declval<__event_handler_f>())),
	decltype(std::declval<T>().unsubscribe_event(std::declval<const _address_event_t>())),
	decltype(std::declval<T>().connection_key()),
	typename std::enable_if_t<std::is_base_of_v<distributed_ctx_t, std::decay_t<decltype(*std::declval<T>().ctx())>>>
	>>
	: std::true_type {};


template<typename T>
constexpr bool is_xpeer_trait_z_v = is_xpeer_trait_z_t<std::decay_t<T>>::value;


template <typename T, typename = void>
struct is_xpeer_invokable_as_sndrcv_request_t : std::false_type {};

template <typename T>
struct is_xpeer_invokable_as_sndrcv_request_t < T,
	std::void_t<
		std::enable_if_t<
			//std::is_base_of_v<i_iol_ohandler_t, std::decay_t<std::invoke_result_t<T>>>

			is_out_message_requirements_v<std::invoke_result_t<T>>

			//std::is_invocable_r_v<
			//	std::unique_ptr<i_iol_ohandler_t>, 
			//	T>
			>
	>>
	: std::true_type{};

template<typename T>
static bool constexpr is_xpeer_invokable_as_sndrcv_request_v = is_xpeer_invokable_as_sndrcv_request_t<T>::value;


template<typename TRequest>
constexpr bool is_invoke_request_type_v = (
	is_xpeer_invokable_as_sndrcv_request_v<TRequest> ||
	is_out_message_requirements_v< TRequest> ||
	srlz::is_serializable_v< TRequest>);
}

template <typename T, typename = void>
struct is_sndrcv_callback_t : std::false_type {};

template <typename T>
struct is_sndrcv_callback_t<T,
	std::void_t<
	std::enable_if_t<(
		std::is_invocable_v<T, message_handler&&>
		)>>>
	: std::true_type {};

template<typename T>
bool constexpr is_sndrcv_callback_v = is_sndrcv_callback_t<T>::value;


template<typename TSendrecvCallback>
static constexpr bool is_sndrcv_callback_no_throw_v = std::is_nothrow_invocable_v<
	TSendrecvCallback,
	message_handler&&>;




template <typename T, typename R, typename = void>
struct is_sndrcv_invocable_result_t : std::false_type {};

template <typename T, typename R>
struct is_sndrcv_invocable_result_t<T, R,
	std::void_t<
	std::enable_if_t<(
		srlz::is_serializable_v<R> &&
		std::is_invocable_v<T, R &&, std::unique_ptr<dcf_exception_t> &&, std::chrono::microseconds>
		)>>>
	: std::true_type {};

template<typename T, typename R>
bool constexpr is_sndrcv_invocable_result_v = is_sndrcv_invocable_result_t<T, R>::value;





template<typename T, typename = void>
struct is_sndrcv_result_handler_trait : std::false_type{};


template<typename T>
struct is_sndrcv_result_handler_trait < T, std::void_t <
	std::enable_if_t<std::is_same_v<detail::function_params_trait<T>::argument_type<1>, std::unique_ptr<dcf_exception_t>>>,
	std::enable_if_t<std::is_constructible_v<std::chrono::microseconds, detail::function_params_trait<T>::argument_type<2>>>
	>> : std::true_type{
	
	using response_object_type = typename detail::function_params_trait<T>::argument_type<0>;
	};

template<typename T>
constexpr bool is_sndrcv_result_handler_trait_v = is_sndrcv_result_handler_trait<T>::value;

template<typename T>
constexpr bool is_sndrcv_result_invocable_v = (
	is_sndrcv_result_handler_trait_v<T> && 
		(
		srlz::is_serializable_v<is_sndrcv_result_handler_trait<T>::response_object_type>
		)
	);


template <typename T, typename = void>
struct is_xpeer_unique_object_t : std::false_type{};

template<typename T>
struct is_xpeer_unique_object_t<T,
	std::enable_if_t<(	
		!std::is_copy_assignable_v<std::decay_t<T>> || !std::is_copy_constructible_v<std::decay_t<T>>
		)>> : std::true_type{};

template<typename T>
constexpr bool is_xpeer_unique_object_v = is_xpeer_unique_object_t<T>::value;



template<typename T, typename = void>
struct is_xpeer_unique_compliant_t : std::false_type{};

template<typename T>
struct is_xpeer_unique_compliant_t<T,
	std::void_t<
	decltype(std::declval<std::decay_t<T>>().link()),
	std::enable_if_t<(
		detail::is_xpeer_trait_z_v<decltype(std::declval<std::decay_t<T>>().link())> && 
		is_xpeer_unique_object_v<T>)>
	>> : std::true_type{};

template<typename T>
constexpr bool is_xpeer_unique_compliant_v = is_xpeer_unique_compliant_t<T>::value;



template <typename T, typename = void>
struct is_xpeer_copyable_object_t : std::false_type{};

template<typename T>
struct is_xpeer_copyable_object_t<T,
	std::enable_if_t<(
		std::is_copy_assignable_v<std::decay_t<T>> && std::is_copy_constructible_v<std::decay_t<T>>
		)>> : std::true_type{};

template<typename T>
constexpr bool is_xpeer_copyable_object_v = is_xpeer_copyable_object_t<T>::value;
}

