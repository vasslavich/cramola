#pragma once


#include "./rt1_functions.h"
#include "../messages/functor_package.h"
#include "../functorapp/functor_result.h"
#include "../messages/functor_package.h"
#include "../xpeer_invoke/xpeer_traits.h"


namespace crm {


template<typename TIO,
	typename TResultClb,
	typename ... AL>
	auto rpc_ainvoke(TIO&& xio,
		launch_as m,
		trace_tag&& tag,
		function_descriptor&& fd,
		TResultClb && clb_,
		AL && ... al) -> typename std::enable_if_t<(
			srlz::detail::is_all_srlzd_v<AL...> &&
			detail::is_xpeer_trait_z_v<TIO>
			), void>{

	auto c = xio.ctx();
	auto obj = detail::make_ofunctor_package(std::move(fd), std::forward<AL>(al)...);

	auto clb = [c = std::move(clb_)](
		detail::functor_result&& rpack, std::unique_ptr<dcf_exception_t>&& e, std::chrono::microseconds mcs)mutable{

		using callback_trait = typename detail::function_params_trait<TResultClb>;
		using clb_args = typename callback_trait::result_handler_params_as_tuple;
		using clb_args_custom = typename detail::tuple_from_first_nth<clb_args, 
			callback_trait::result_handler_params_countof - 2>::type;

		if (!e) {
			clb_args_custom customArgsTpl;
			rpack.load_to_tuple(customArgsTpl);

			auto callbackAllArgs = std::tuple_cat(std::move(customArgsTpl), std::make_tuple(std::move(e), std::move(mcs)));
			std::apply(c, std::move(callbackAllArgs));
			}
		else {
			auto callbackAllArgs = std::tuple_cat(clb_args_custom{}, std::make_tuple(std::move(e), std::move(mcs)));
			std::apply(c, std::move(callbackAllArgs));
			}
		};

	return ainvoke(c,
		std::forward<TIO>(xio),
		std::move(obj),
		std::move(clb),
		std::move(tag),
		sndrcv_timeline_t::make_once_call(),
		m);
	}


namespace detail {


template<typename TIO,
	typename ... AL>
	auto rpc_coro_invoke(TIO&& xio,
		trace_tag&& tag,
		std::enable_if_t<(
			srlz::detail::is_all_srlzd_v<AL...> && 
			is_xpeer_trait_z_v<TIO>
			), function_descriptor>&& fd,
		AL&& ... al) {

	auto c = xio.ctx();
	auto obj = make_ofunctor_package(std::move(fd), std::forward<AL>(al)...);

	return coro_invoke(c,
		std::forward<TIO>(xio),
		std::move(obj),
		std::move(tag),
		sndrcv_timeline_t::make_once_call());
	}
}

template<typename TIO,
	typename ... AL>
	auto rpc_coro_invoke(TIO&& xio,
		trace_tag&& tag,
		std::enable_if_t<(
			srlz::detail::is_all_srlzd_v<AL...> && 
			detail::is_xpeer_trait_z_v<TIO>
			), function_descriptor> && fd,
		AL&& ... al) {
	
	return detail::rpc_coro_invoke(
		std::forward<TIO>(xio),
		std::move(tag),
		std::move(fd),
		std::forward<AL>(al)...);
	}

template<typename TIO,
	typename ... AL>
	auto rpc_binvoke(TIO&& xio,
		trace_tag&& tag,
		function_descriptor&& fd,
		AL&& ... al) -> typename std::enable_if_t<(
			srlz::detail::is_all_srlzd_v<AL...> &&
			detail::is_xpeer_trait_z_v<TIO>
			), message_handler> {

	auto c = xio.ctx();
	auto obj = detail::make_ofunctor_package(std::move(fd), std::forward<AL>(al)...);

	return binvoke(c,
		std::forward<TIO>(xio),
		std::move(obj),
		std::move(tag),
		sndrcv_timeline_t::make_once_call(),
		launch_as::async);
	}

detail::functor_result rpc_get_result(message_handler&&);
const detail::functor_result& rpc_get_result(const message_handler&);
}

