#pragma once


#include <type_traits>
#include "../internal_invariants.h"
#include "../utilities/utilities.h"
#include "../messages/terms.h"
#include "./xpeer_traits.h"
#include "../erremlt/utilities.h"
#include "../channel_terms/i_events.h"


namespace crm::detail{


template<typename _TIO,
	typename TIO = std::decay_t<_TIO>>
struct xpeer_invoke_functor_t{
	using object_type = typename TIO;

	TIO _io;

	template<typename _TIOy,
		typename std::enable_if_t<std::is_constructible_v<TIO, _TIOy>, int> = 0>
	xpeer_invoke_functor_t( _TIOy && io )
		: _io( std::forward<_TIOy>( io ) ){}

	template<typename THandler,
		typename std::enable_if_t<std::is_constructible_v< subscribe_message_handler, THandler>, int> = 0>
	[[nodiscard]]
	auto subscribe_hndl_id( async_space_t spc, const _address_hndl_t & addr, subscribe_options opt, THandler && handler ){
		return _io.subscribe_hndl_id( spc, addr, opt, std::forward<THandler>( handler ) );
		}

	void unsubscribe_hndl_id( const _address_hndl_t & addr )noexcept{
		_io.unsubscribe_hndl_id( addr );
		}

	template<typename THandler,
		typename std::enable_if_t<std::is_constructible_v<__event_handler_f, std::decay_t<THandler>>, int> = 0>
	[[nodiscard]]
	auto subscribe_event(async_space_t spc, const _address_event_t& addr, bool once, THandler&& handler) {
		return _io.subscribe_event(spc, addr, once, std::forward<THandler>(handler));
		}

	void unsubscribe_event( const _address_event_t & addr )noexcept{
		_io.unsubscribe_event( addr );
		}

	template<typename O,
		typename std::enable_if_t < is_out_message_requirements_v<O>, int> = 0>
	void push( O && o ){
		_io.push( std::forward<O>( o ) );
		}

	auto connection_key()const noexcept{
		return _io.connection_key();
		}

	auto ctx()const  noexcept {
		return _io.ctx();
		}

	rtl_level_t level()const noexcept{
		return _io.level();
		}

	bool closed()const noexcept{
		return _io.closed();
		}

	bool is_opened()const noexcept {
		return _io.is_opened();
		}

	bool renewable_connection()const noexcept{
		return _io.renewable_connection();
		}

	auto remote_object_endpoint()const noexcept{
		return _io.remote_object_endpoint();
		}

	rtl_table_t direction()const  noexcept {
		return _io.direction();
		}
	};


template<typename TUniqueXpeer,
	typename std::enable_if_t<(
		is_xpeer_unique_compliant_v<TUniqueXpeer> && 
		!std::is_rvalue_reference_v<TUniqueXpeer&&>
		), int> = 0>
auto make_invokable_xpeer( TUniqueXpeer && p ){
	
	return xpeer_invoke_functor_t<decltype(p.link())>( p.link() );
	}


template<typename TSharedXpeer,
	typename std::enable_if_t<(
		is_xpeer_copyable_object_v<TSharedXpeer> || 
		std::is_rvalue_reference_v<TSharedXpeer&&>
		), int> = 0>
auto make_invokable_xpeer( TSharedXpeer && p ){

	return std::forward<TSharedXpeer>(p);
	}
}

