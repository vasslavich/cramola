#include "../../internal.h"
#include "../rt1_functions.h"

using namespace crm;

namespace {
struct test_serializable_message {
	std::string s;
	std::vector<uint32_t> vb;
	};

static_assert(srlz::is_serializable_v<test_serializable_message>,
	__FILE_LINE__);
static_assert(detail::is_invoke_request_type_v<test_serializable_message>, __FILE_LINE__);


struct message_pack_callback_func {
	void operator()(test_serializable_message&&, std::unique_ptr<dcf_exception_t>&&, std::chrono::microseconds);
	};
static_assert(detail::is_sndrcv_callback_overload_v<message_pack_callback_func>, __FILE_LINE__);
static_assert(is_sndrcv_result_invocable_v<message_pack_callback_func>, __FILE_LINE__);



struct message_callback_func {
	void operator()(message_handler&&);
	};
static_assert(detail::is_sndrcv_callback_overload_v<message_callback_func>, __FILE_LINE__);
static_assert(is_sndrcv_callback_v<message_callback_func>, __FILE_LINE__);



static_assert(is_out_message_requirements_v<detail::ofunctor_package_t>,
	__FILE_LINE__);
static_assert(detail::is_invoke_request_type_v<detail::ofunctor_package_t>, __FILE_LINE__);



struct request_as_handler_ctr {
	auto operator()()->detail::ofunctor_package_t;
	};
static_assert(detail::is_invoke_request_type_v< request_as_handler_ctr>, __FILE_LINE__);
static_assert(detail::is_xpeer_invokable_as_sndrcv_request_v< request_as_handler_ctr>, __FILE_LINE__);


template<typename TIO, typename TCallback>
static constexpr bool check_hinvoke_v =
has_cancel_v<
	decltype(hainvoke(
		std::declval<std::weak_ptr<crm::distributed_ctx_t>>(),
		std::declval<TIO>(),
		std::declval<request_as_handler_ctr>(),
		std::declval<TCallback>(),
		std::declval<trace_tag>(),
		std::declval<sndrcv_timeline_t>()))
> &&

has_cancel_v<
	decltype(hainvoke(
		std::declval<std::weak_ptr<crm::distributed_ctx_t>>(),
		std::declval<TIO>(),
		std::declval<detail::ofunctor_package_t>(),
		std::declval<TCallback>(),
		std::declval<trace_tag>(),
		std::declval<sndrcv_timeline_t>()))
> &&

has_cancel_v<
	decltype(hainvoke(
		std::declval<std::weak_ptr<crm::distributed_ctx_t>>(),
		std::declval<TIO>(),
		std::declval<test_serializable_message>(),
		std::declval<TCallback>(),
		std::declval<trace_tag>(),
		std::declval<sndrcv_timeline_t>()))
>;


template<typename TIO>
static constexpr bool check_hinvoke_on_callbacks_v =
	check_hinvoke_v<TIO, message_callback_func> && 
	check_hinvoke_v<TIO, message_pack_callback_func>;

static_assert(check_hinvoke_on_callbacks_v< crm::opeer_t>, __FILE_LINE__);
static_assert(check_hinvoke_on_callbacks_v< crm::ipeer_t>, __FILE_LINE__);
static_assert(check_hinvoke_on_callbacks_v< crm::opeer_l0>, __FILE_LINE__);
static_assert(check_hinvoke_on_callbacks_v< crm::ipeer_l0>, __FILE_LINE__);


template<typename TIO, typename TCallback>
static constexpr bool check_ainvoke_v =
std::is_void_v<
	decltype(ainvoke(
		std::declval<std::weak_ptr<crm::distributed_ctx_t>>(),
		std::declval<TIO>(),
		std::declval<request_as_handler_ctr>(),
		std::declval<TCallback>(),
		std::declval<trace_tag>(),
		std::declval<sndrcv_timeline_t>()))
> &&

std::is_void_v<
	decltype(ainvoke(
		std::declval<std::weak_ptr<crm::distributed_ctx_t>>(),
		std::declval<TIO>(),
		std::declval<detail::ofunctor_package_t>(),
		std::declval<TCallback>(),
		std::declval<trace_tag>(),
		std::declval<sndrcv_timeline_t>()))
> &&

std::is_void_v<
	decltype(ainvoke(
		std::declval<std::weak_ptr<crm::distributed_ctx_t>>(),
		std::declval<TIO>(),
		std::declval<test_serializable_message>(),
		std::declval<TCallback>(),
		std::declval<trace_tag>(),
		std::declval<sndrcv_timeline_t>()))
>;

template<typename TIO>
static constexpr bool check_ainvoke_on_callbacks_v =
check_ainvoke_v<TIO, message_callback_func> &&
check_ainvoke_v<TIO, message_pack_callback_func>;



static_assert(check_ainvoke_on_callbacks_v<crm::opeer_t>, __FILE_LINE__);
static_assert(check_ainvoke_on_callbacks_v< crm::ipeer_t>, __FILE_LINE__);
static_assert(check_ainvoke_on_callbacks_v< crm::opeer_l0>, __FILE_LINE__);
static_assert(check_ainvoke_on_callbacks_v< crm::ipeer_l0>, __FILE_LINE__);







template<typename TIO>
static constexpr bool check_binvoke_v =
std::is_same_v < message_handler,
	std::decay_t<decltype(binvoke(
		std::declval<std::weak_ptr<crm::distributed_ctx_t>>(),
		std::declval<TIO>(),
		std::declval<request_as_handler_ctr>(),
		std::declval<trace_tag>(),
		std::declval<sndrcv_timeline_t>()))>> &&

std::is_same_v < message_handler,
	std::decay_t<decltype(binvoke(
		std::declval<std::weak_ptr<crm::distributed_ctx_t>>(),
		std::declval<TIO>(),
		std::declval<detail::ofunctor_package_t>(),
		std::declval<trace_tag>(),
		std::declval<sndrcv_timeline_t>()))>> &&

std::is_same_v<message_handler,
	std::decay_t<decltype(binvoke(
		std::declval<std::weak_ptr<crm::distributed_ctx_t>>(),
		std::declval<TIO>(),
		std::declval<test_serializable_message>(),
		std::declval<trace_tag>(),
		std::declval<sndrcv_timeline_t>()))>>;

static_assert(check_binvoke_v<crm::opeer_t>, __FILE_LINE__);
static_assert(check_binvoke_v<crm::ipeer_t>, __FILE_LINE__);
static_assert(check_binvoke_v<crm::opeer_l0>, __FILE_LINE__);
static_assert(check_binvoke_v<crm::ipeer_l0>, __FILE_LINE__);



template<typename TIO>
static constexpr bool check_coro_invoke_v =
	has_avaitable_requirements_v<decltype(coro_invoke(
		std::declval<std::weak_ptr<crm::distributed_ctx_t>>(),
		std::declval<TIO>(),
		std::declval<request_as_handler_ctr>(),
		std::declval<trace_tag>(),
		std::declval<sndrcv_timeline_t>())), message_handler> &&

	has_avaitable_requirements_v<decltype(coro_invoke(
		std::declval<std::weak_ptr<crm::distributed_ctx_t>>(),
		std::declval<TIO>(),
		std::declval<detail::ofunctor_package_t>(),
		std::declval<trace_tag>(),
		std::declval<sndrcv_timeline_t>())), message_handler> &&

	has_avaitable_requirements_v<decltype(coro_invoke(
		std::declval<std::weak_ptr<crm::distributed_ctx_t>>(),
		std::declval<TIO>(),
		std::declval<test_serializable_message>(),
		std::declval<trace_tag>(),
		std::declval<sndrcv_timeline_t>())), message_handler>;

static_assert(check_coro_invoke_v<crm::opeer_t>, __FILE_LINE__);
}
