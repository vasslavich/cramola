#include "../../internal.h"

using namespace crm;

namespace {

struct test_message_2 {
	std::wstring ws;
	double d;
	};

struct test_message_3 {
	std::wstring ws;
	double d;
	};

struct message_pack_callback_func {
	void operator()(test_message_3&&, std::unique_ptr<dcf_exception_t>&&, std::chrono::microseconds);
	};
static_assert(detail::is_sndrcv_callback_overload_v<message_pack_callback_func>, __FILE_LINE__);
static_assert(is_sndrcv_result_invocable_v<message_pack_callback_func>, __FILE_LINE__);



struct message_callback_func {
	void operator()(message_handler&&);
	};
static_assert(detail::is_sndrcv_callback_overload_v<message_callback_func>, __FILE_LINE__);
static_assert(is_sndrcv_callback_v<message_callback_func>, __FILE_LINE__);


template<typename IO>
constexpr bool check_rpc_ainvoke_callbacks_v =
std::is_void_v<decltype(rpc_ainvoke(
	std::declval<IO>(),
	std::declval<launch_as>(),
	std::declval<trace_tag>(),
	std::declval<function_descriptor>(),
	std::declval< message_pack_callback_func>(),
	std::declval<int>(),
	std::declval<test_message_2>()))> &&
	
	std::is_void_v<decltype(rpc_ainvoke(
		std::declval<IO>(),
		std::declval<launch_as>(),
		std::declval<trace_tag>(),
		std::declval<function_descriptor>(),
		std::declval< message_callback_func>(),
		std::declval<int>(),
		std::declval<test_message_2>()))>;

static_assert(check_rpc_ainvoke_callbacks_v<ipeer_l0>, __FILE_LINE__);
static_assert(check_rpc_ainvoke_callbacks_v<opeer_l0>, __FILE_LINE__);
static_assert(check_rpc_ainvoke_callbacks_v<ipeer_t>, __FILE_LINE__);
static_assert(check_rpc_ainvoke_callbacks_v<opeer_t>, __FILE_LINE__);




template<typename IO>
constexpr bool check_rpc_coro_v =
has_avaitable_requirements_v<decltype(rpc_coro_invoke(
	std::declval<IO>(),
	std::declval<trace_tag>(),
	std::declval<function_descriptor>(),
	std::declval<int>(),
	std::declval<test_message_2>())), message_handler>;


static_assert(check_rpc_coro_v<ipeer_l0>, __FILE_LINE__);
static_assert(check_rpc_coro_v<opeer_l0>, __FILE_LINE__);
static_assert(check_rpc_coro_v<ipeer_t>, __FILE_LINE__);
static_assert(check_rpc_coro_v<opeer_t>, __FILE_LINE__);



template<typename IO>
constexpr bool check_rpc_binvoke_v =
std::is_same_v<decltype(rpc_binvoke(
	std::declval<IO>(),
	std::declval<trace_tag>(),
	std::declval<function_descriptor>(),
	std::declval<int>(),
	std::declval<test_message_2>())), message_handler>;

static_assert(check_rpc_binvoke_v<ipeer_l0>, __FILE_LINE__);
static_assert(check_rpc_binvoke_v<opeer_l0>, __FILE_LINE__);
static_assert(check_rpc_binvoke_v<ipeer_t>, __FILE_LINE__);
static_assert(check_rpc_binvoke_v<opeer_t>, __FILE_LINE__);
}
