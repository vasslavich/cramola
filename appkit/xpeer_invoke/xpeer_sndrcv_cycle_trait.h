#pragma once


#include <type_traits>
#include "../frame/mpfdef.h"


namespace crm{
namespace mpf{
namespace detail{


template<typename _Tx,
	typename T = typename std::decay_t<_Tx>>
struct xpeer_sndrcv_cycle_methods_trait_t{
	using subscribe_handler_t = typename T::subscribe_handler_t;

	using subscribe_hndl_id_result_t = typename std::decay_t<decltype(std::declval<T>().subscribe_hndl_id(async_space_t::undef, std::declval<_address_hndl_t>(), subscribe_options{}, std::declval<subscribe_handler_t>())) > ;

	using unsubscribe_hndl_id_result_t = typename std::decay_t<decltype(std::declval<T>().unsubscribe_hndl_id( std::declval<_address_hndl_t>() )) >;

	using is_renewable_connection_result_t = typename std::decay_t<decltype(std::declval<T>().renewable_connection())>;

	using level_result_t = typename std::decay_t<decltype(std::declval<T>().level())>;
	};

template<typename T, typename Enable = void>
struct is_xpeer_sndrcv_cycle_trait_z_t : public std::false_type{};

template<typename T>
struct is_xpeer_sndrcv_cycle_trait_z_t<T,
	typename std::enable_if<
	(std::is_same_v<typename xpeer_sndrcv_cycle_methods_trait_t<T>::subscribe_handler_t, typename std::decay_t<T>::subscribe_handler_t>)
	&& (std::is_same_v<subscribe_invoker_result, typename xpeer_sndrcv_cycle_methods_trait_t<T>::subscribe_hndl_id_result_t>)
	&& (std::is_same_v<void, typename xpeer_sndrcv_cycle_methods_trait_t<T>::unsubscribe_hndl_id_result_t>)
	&& (std::is_same_v<bool, typename xpeer_sndrcv_cycle_methods_trait_t<T>::is_renewable_connection_result_t>)
	&& (std::is_same_v<rtl_level_t, typename xpeer_sndrcv_cycle_methods_trait_t<T>::level_result_t>)
>::type>
	: std::true_type{};

template<typename T>
constexpr bool is_xpeer_sndrcv_cycle_trait_v = is_xpeer_sndrcv_cycle_trait_z_t<T>::value;




template<typename T, typename Enable = void>
struct is_xpeer_sndrcv_cycle_actor_trait_z_t : public std::false_type{};


template<typename _Tx,
	typename T = typename std::decay_t<_Tx>>
struct xpeer_sndrcv_cycle_actor_methods_trait_t{
	using actor_conditional_next_result_t = typename std::decay_t<decltype(std::declval<T>().next(
		rdm_sessional_descriptor_t{},
		std::unique_ptr<i_iol_ihandler_t>{},
		std::unique_ptr<dcf_exception_t>{}
	))>;

	using actor_conditional_make_close_result_t = typename std::decay_t<decltype(std::declval<T>().make_close(
		rdm_sessional_descriptor_t{}
	))>;

	using actor_conditional_name_result_t = typename std::decay_t<decltype(std::declval<T>().name())>;
	};


template<typename T>
struct is_xpeer_sndrcv_cycle_actor_trait_z_t<T,
	typename std::enable_if<
	(std::is_same_v<std::unique_ptr<i_iol_ohandler_t>, typename xpeer_sndrcv_cycle_actor_methods_trait_t<T>::actor_conditional_next_result_t>)
	&& (std::is_same_v<std::unique_ptr<i_iol_ohandler_t>, typename xpeer_sndrcv_cycle_actor_methods_trait_t<T>::actor_conditional_make_close_result_t>)
	&& (std::is_same_v<std::string, typename xpeer_sndrcv_cycle_actor_methods_trait_t<T>::actor_conditional_name_result_t>)
>::type>
	: std::true_type{};


template<typename T>
constexpr bool is_xpeer_sndrcv_cycle_actor_trait_v = is_xpeer_sndrcv_cycle_actor_trait_z_t<T>::value;
}
}
}

