#pragma once


//#include "result_handler.h"


namespace crm {
namespace detail {


template<typename IO, typename CTR, typename CLB>
void start_alaunch(async_space_t scx,
	std::weak_ptr<distributed_ctx_t> ctx_,
	IO&& io_,
	CTR&& ctr_,
	CLB&& clb_,
	trace_tag&& tag_,
	const sndrcv_timeline_t& timeline_,
	launch_as m_,
	inovked_as as_) {

	using actor_type = typename std::decay_t<decltype(async_sndrcv_type_deduction_expression(
		std::forward<IO>(io_),
		std::forward<CTR>(ctr_),
		std::forward<CLB>(clb_)))>;

	actor_type::alaunch(scx,
		std::move(ctx_),
		std::forward<IO>(io_),
		std::forward<CTR>(ctr_),
		std::forward<CLB>(clb_),
		std::move(tag_),
		timeline_,
		m_,
		as_);
	}

template<typename _Xio,
	typename _XSendCtr,
	typename XTypeIO = typename std::decay_t<_Xio>,
	typename XTypeSendCtr = typename std::decay_t<_XSendCtr>,
	typename std::enable_if_t<(
		is_sendrecv_request_invokable_v<XTypeSendCtr, XTypeIO> &&
		is_xpeer_trait_z_v<XTypeIO>
		)> * = 0>
	auto make_coro_sndrcv_once_base(async_space_t scx,
		std::weak_ptr<distributed_ctx_t> ctx_,
		_Xio&& io,
		_XSendCtr&& ctr,
		trace_tag&& tag,
		const sndrcv_timeline_t& timeline) {

	static_assert(is_sendrecv_request_invokable_v<_XSendCtr, _Xio>, __FILE_LINE__);
	static_assert(is_xpeer_trait_z_v<_Xio>, __FILE_LINE__);

	using xio_type = typename std::decay_t<decltype(make_invokable_xpeer(std::declval<_Xio>()))>;

	struct Awaiter {
		std::weak_ptr<distributed_ctx_t> _ctx;
		xio_type _io;
		XTypeSendCtr _ctr;

#if defined (XPEER_SNDRCV_ACTOR_TRACE)
		trace_tag _tag;
#endif

		sndrcv_timeline_t _timeline;
		async_space_t _scx;
		message_handler message;

		Awaiter(async_space_t scx_,
			std::weak_ptr<distributed_ctx_t> ctx_,
			_Xio&& io_,
			_XSendCtr&& ctr_,

#if defined (XPEER_SNDRCV_ACTOR_TRACE)
			trace_tag&& tag_,
#else
			trace_tag&&,
#endif

			const sndrcv_timeline_t& timeline_)
			: _ctx(ctx_)
			, _io(make_invokable_xpeer(std::forward<_Xio>(io_)))
			, _ctr(std::forward<_XSendCtr>(ctr_))

#if defined (XPEER_SNDRCV_ACTOR_TRACE)
			, _tag(std::move(tag_))
#else

#endif
			, _timeline(timeline_)
			, _scx(scx_){}

		bool await_ready() { return false; }

		void await_suspend(std::experimental::coroutine_handle<> coro) {
			start_alaunch(
				_scx,
				std::move(_ctx),
				std::move(_io),
				std::move(_ctr),

				[this, coro](message_handler&& m_)noexcept {
				message = std::move(m_);
				coro.resume();
				},

#if defined (XPEER_SNDRCV_ACTOR_TRACE)
				std::move(_tag),
#else
				"",
#endif

				std::move(_timeline),
				launch_as::async,
				inovked_as::async);
			}

		auto await_resume() {
			return std::move(message);
			}
		};

	return Awaiter(scx,
		std::move(ctx_),
		std::forward<_Xio>(io),
		std::forward<_XSendCtr>(ctr),
		std::move(tag),
		timeline);
	}
}

template<typename _Xio,
	typename _XSendCtr,
	typename XTypeIO = typename std::decay_t<_Xio>,
	typename XTypeSendCtr = typename std::decay_t<_XSendCtr>,
	typename std::enable_if_t<(
		detail::is_sendrecv_request_invokable_v<XTypeSendCtr, XTypeIO> &&
		detail::is_xpeer_trait_z_v<XTypeIO>
		)> * = 0>
	auto make_coro_sndrcv_once(async_space_t scx,
		std::weak_ptr<distributed_ctx_t> ctx_,
		_Xio&& io,
		_XSendCtr&& ctr,
		trace_tag&& tag,
		const sndrcv_timeline_t& timeline) {

	return detail::make_coro_sndrcv_once_base(
		scx,
		std::move(ctx_),
		std::forward<_Xio>(io),
		std::forward<_XSendCtr>(ctr),
		std::move(tag),
		timeline);
	}
}

