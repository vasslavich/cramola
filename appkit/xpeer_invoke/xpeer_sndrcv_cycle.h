#pragma once


#include <functional>
#include <chrono>
#include <atomic>
#include <memory>
#include "../internal_invariants.h"
#include "../utilities/utilities.h"
#include "../messages/terms.h"
#include "./xpeer_traits.h"
#include "./xpeer_invoke_trait.h"
#include "../erremlt/utilities.h"
#include "../utilities/check_validate/checker_seqcall.h"
#include "../streams/i_streams.h"
#include "../time/i_timers.h"


#if defined(CBL_USE_OBJECTS_MONITOR) || defined(CBL_MPF_MESSAGE_TRACEWAY_KEYS_BY_ID)
#define CBL_SNDRCV_CYCLIC_USE_TRACE_SPIN_TAG
#endif


namespace crm{
namespace detail{


extern std::atomic<std::intmax_t> SendRecvOperationsCounter;
size_t get_stat_counter_sendrecv_operations()noexcept;


#ifdef CBL_SNDRCV_CYCLIC_USE_TRACE_SPIN_TAG
extern std::atomic<std::uintmax_t> GlobalMessageSndRcvTraceCounter;
#endif


template<typename TSendrecvCyclicEnd>
struct is_sendrecv_cyclic_end_invokable : std::is_invocable<
	TSendrecvCyclicEnd,
	async_operation_result_t,
	std::unique_ptr<i_stream_end_t>,
	std::unique_ptr<i_stream_rhandler_t>,
	std::unique_ptr<dcf_exception_t>> {};

template<typename TSendrecvCyclicEnd>
static constexpr bool is_sendrecv_cyclic_end_invokable_v = is_sendrecv_cyclic_end_invokable<TSendrecvCyclicEnd>::value;


template<typename TRemoteExcHndl>
struct is_remote_exception_handler_invokable : std::is_nothrow_invocable_r<bool,
	TRemoteExcHndl,
	std::unique_ptr<crm::dcf_exception_t>> {};

template<typename TRemoteExcHndl>
static constexpr bool is_remote_exception_handler_invokable_v = is_remote_exception_handler_invokable<TRemoteExcHndl>::value;



template<typename TSendrecvCyclicActor, typename Tio, typename = std::void_t<>>
struct is_sendrecv_cyclic_actor_invokable : std::false_type {};

template<typename TSendrecvCyclicActor, typename Tio>
struct is_sendrecv_cyclic_actor_invokable < TSendrecvCyclicActor, Tio, std::void_t<
	decltype(std::declval<TSendrecvCyclicActor>().make_close(std::declval<const rdm_sessional_descriptor_t>())),
	typename std::enable_if_t<std::is_same_v<
		std::decay_t<decltype(std::declval<TSendrecvCyclicActor>().release_handler())>, 
		std::unique_ptr<i_stream_rhandler_t>
		>>/*,
	typename std::enable_if_t<std::is_same_v<
		std::decay_t<decltype(
			std::declval<TSendrecvCyclicActor>().next(
				std::declval<rdm_sessional_descriptor_t>(),
				std::declval<std::unique_ptr<i_iol_ihandler_t>>()
			))>,
		std::pair<std::unique_ptr<i_iol_ohandler_t>, bool>
		>>*//*,
	typename std::enable_if_t<noexcept(std::declval<TSendrecvCyclicActor>().make_close(std::declval<rdm_sessional_descriptor_t>()))>,
	typename std::enable_if_t<noexcept(std::declval<TSendrecvCyclicActor>().release_handler())>*/
	>> : std::true_type{};

template<typename TSendrecvCyclicActor, typename Tio>
static constexpr bool is_sendrecv_cyclic_actor_invokable_v = is_sendrecv_cyclic_actor_invokable<TSendrecvCyclicActor, Tio>::value;


template<typename TSendrecvCyclicEnd>
static constexpr bool is_sendrecv_cyclic_end_no_throw_invokable_v = std::is_nothrow_invocable_v<
	TSendrecvCyclicEnd,
	async_operation_result_t,
	std::unique_ptr<i_stream_end_t>&&,
	std::unique_ptr<i_stream_rhandler_t>&&,
	std::unique_ptr<dcf_exception_t>&&>;


struct continue_condition_eof {};

#if defined (CBL_MPF_ENABLE_TIMETRACE) || defined(CBL_MPF_USE_REVERSE_EXCEPTION_TRACE) || defined(CBL_TRACELEVEL_TRACE_TAG_ON)
#define XPEER_SNDRCV_CYCLIC_ACTOR_TRACE
#endif

template<typename _Tio,
	typename _TActor,
	typename _TEndHndl,
	typename _TRemoteExceptionHandler,
	typename TypeIO = typename std::decay_t<_Tio>,
	typename TypeActor = typename std::decay_t<_TActor>,
	typename TEndHndl = typename std::decay_t<_TEndHndl>,
	typename TRemoteExceptionHandler = typename std::decay_t<_TRemoteExceptionHandler>,
	typename std::enable_if_t<(
		is_sendrecv_cyclic_actor_invokable_v<TypeActor, TypeIO> &&
		is_sendrecv_cyclic_end_invokable_v<TEndHndl> &&
		is_remote_exception_handler_invokable_v< TRemoteExceptionHandler> &&
		is_xpeer_trait_z_v<TypeIO>
		), int> = 0,
	const bool CallbackNoThrowInvokable = is_sendrecv_cyclic_end_no_throw_invokable_v<TEndHndl>>
	class sndrcv_cyclic_actor_t : public std::enable_shared_from_this<sndrcv_cyclic_actor_t< _Tio, _TActor, _TEndHndl, _TRemoteExceptionHandler>> {
	public:
		using self_t = typename sndrcv_cyclic_actor_t<_Tio, _TActor, _TEndHndl, _TRemoteExceptionHandler>;
		using io_t = typename TypeIO;
		using actor_t = typename TypeActor;
		using remote_exception_handler_t = typename TRemoteExceptionHandler;

		using end_hndl_t = std::function<void(async_operation_result_t st,
			std::unique_ptr<i_stream_end_t> && h,
			std::unique_ptr<i_stream_rhandler_t> && rh,
			std::unique_ptr<dcf_exception_t> && exc)>;

		using end_hndl_handler = crm::utility::invoke_functor_shared<void,
			async_operation_result_t,
			std::unique_ptr<i_stream_end_t> &&,
			std::unique_ptr<i_stream_rhandler_t> &&,
			std::unique_ptr<dcf_exception_t> &&>;

		struct continue_condition {
			std::weak_ptr<self_t> _wpthis;
			
			continue_condition(std::weak_ptr<self_t> wpthis_)noexcept : _wpthis(wpthis_){}

			continue_condition(const continue_condition&) = delete;
			continue_condition(continue_condition&&) = delete;

			void operator()(continue_condition_eof eofm)const {
				if (auto spthis = _wpthis.lock()) {
					spthis->_continue_condition_hndl(eofm);
					}
				}

			template<typename Message,
				typename std::enable_if_t < is_out_message_requirements_v<Message>, int> = 0>
			void operator()(Message&& m) const{
				if (auto spthis = _wpthis.lock()) {
					spthis->_continue_condition_hndl(std::forward<Message>(m));
					}
				}

			template<typename Exception,
				typename std::enable_if_t<std::is_base_of_v<dcf_exception_t, std::decay_t<Exception>>, int> = 0>
			void operator()(std::unique_ptr<Exception> && m) const {
				if (auto spthis = _wpthis.lock()) {
					spthis->_continue_condition_hndl(std::move(m));
					}
				}
			};

		struct end_hndl_wrapper : end_hndl_handler {
			end_hndl_wrapper()noexcept {}

			template<typename _XHandler,
				typename std::enable_if_t<!std::is_base_of_v< end_hndl_handler, std::decay_t<_XHandler>>, int> = 0>
				end_hndl_wrapper(_XHandler && f)noexcept
				: end_hndl_handler(std::forward< _XHandler>(f)) {}

			void operator()(std::unique_ptr<dcf_exception_t> && e)noexcept {
				invoke(async_operation_result_t::st_exception, nullptr, nullptr, std::move(e));
				}

			void operator()(async_operation_result_t st,
				std::unique_ptr<i_stream_end_t> && h,
				std::unique_ptr<i_stream_rhandler_t> && rh,
				std::unique_ptr<dcf_exception_t> && exc) {

				invoke(st, std::move(h), std::move(rh), std::move(exc));
				}
			};

		struct end_hndl_guard {
			using tail_exception_guard = typename functor_invoke_guard_t<end_hndl_wrapper, make_fault_result_t>;

			tail_exception_guard hndl;

#ifdef CBL_OBJECTS_COUNTER_CHECKER
			utility::__xobject_counter_logger_t<end_hndl_guard, 10> _xDbgCounter;
#endif

			template<typename THandler,
				typename std::enable_if_t<(
					!std::is_base_of_v<end_hndl_guard, std::decay_t<THandler>>
					), int> = 0>
				end_hndl_guard(THandler && h,
					std::weak_ptr<distributed_ctx_t> ctx_,
					trace_tag && tag_,
					async_space_t scx)noexcept
				: hndl(std::move(h),
					std::move(ctx_),
					std::move(tag_),
					scx) {}

			void operator()(std::unique_ptr<dcf_exception_t> && e)noexcept {
				hndl.invoke_async(std::move(e), nullptr);
				}

			void operator()(async_operation_result_t st,
				std::unique_ptr<i_stream_end_t> && h,
				std::unique_ptr<i_stream_rhandler_t> && rh,
				std::unique_ptr<dcf_exception_t> && exc) {

				hndl.invoke_async(st, std::move(h), std::move(rh), std::move(exc));
				}

			operator bool()const noexcept {
				return hndl;
				}
			};

		struct handler_t {
			friend class self_t;

		private:
			std::shared_ptr<self_t> _p;

		public:
			handler_t()noexcept {}

		private:
			handler_t(std::shared_ptr<self_t> p)noexcept
				: _p(std::move(p)) {}

		public:
			void cancel()noexcept {
				if(_p) {
					_p->cancel();
					_p.reset();
					}
				}
			};

	private:
		class cycle_flow_recovery_t {
		private:
			std::unique_ptr<callable_functor_t> _recoveryf;
			std::atomic<bool> _invokedf{ false };

#ifdef CBL_OBJECTS_COUNTER_CHECKER
			utility::__xobject_counter_logger_t<cycle_flow_recovery_t, 1> _xDbgCounter;
#endif

		public:
			template<typename TAction, typename ... TArgs>
			cycle_flow_recovery_t(typename TAction && action, TArgs && ... args)noexcept {
				_recoveryf = make_callable_functor_pack<TAction, TArgs...>(nullptr,
					std::forward<TAction>(action),
					std::forward<TArgs>(args)...);
				}

			~cycle_flow_recovery_t() {
				CHECK_NO_EXCEPTION(invoke());
				}

			void invoke()noexcept {
				bool inv = false;
				if(_invokedf.compare_exchange_strong(inv, true)) {
					auto rf = std::move(_recoveryf);
					if(rf) {
						CHECK_NO_EXCEPTION(rf->execute());
						}
					}
				}

			cycle_flow_recovery_t()noexcept {}

			cycle_flow_recovery_t(const cycle_flow_recovery_t &) = delete;
			cycle_flow_recovery_t& operator=(const cycle_flow_recovery_t &) = delete;

			cycle_flow_recovery_t(cycle_flow_recovery_t && o)noexcept
				: _recoveryf(std::move(o._recoveryf))
#ifdef CBL_OBJECTS_COUNTER_CHECKER
				, _xDbgCounter(std::move(o._xDbgCounter))
#endif
				{

				o.reset();
				}

			cycle_flow_recovery_t& operator=(cycle_flow_recovery_t && o) = delete;

			void reset()noexcept {
				bool inv = false;
				if(_invokedf.compare_exchange_strong(inv, true)) {
					_recoveryf.reset();
					}
				}
			};

		struct timeline_trace {
			using millisec_time_line = typename decltype(std::declval<std::chrono::milliseconds>().count());
			static const int time_line_factor_sec = 1000;

			static millisec_time_line curr_time_line()noexcept {
				return std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
				}

			std::atomic<millisec_time_line> _lastEmit{ curr_time_line() };

			void mark_snd()noexcept {
				auto curr = curr_time_line();
				auto prev = _lastEmit.exchange(curr);

				CBL_VERIFY(curr >= prev);

#ifdef CBL_USE_OBJECTS_MONITOR
				auto diffMs = curr - prev;

				if((diffMs / time_line_factor_sec) > (60 * 3)) {
					if (auto c = ctx()) {
						c->exc_hndl(CREATE_MPF_PTR_EXC_FWD("long timed send-recive cycled operation{" + trace_name() + "}"));
						}
					}
#endif
				}
			};

		std::weak_ptr<distributed_ctx_t> _ctx;
#if defined (XPEER_SNDRCV_CYCLIC_ACTOR_TRACE)
		trace_tag _tag;
#endif

		io_t _io;
		rdm_sessional_descriptor_t _session;
		subscriber_node_id_t _subscribeId;
		subscriber_event_id_t _eventSubscripeId;
		actor_t _actor;
		async_sequential_push_handler _sh;
		_address_hndl_t _addrTargetType;
		_address_event_t _addrDisconnectionHandler;
		end_hndl_guard _ehndl;
		std::unique_ptr<i_stream_end_t> _send;
		remote_exception_handler_t _remoteExcHndl;
		std::chrono::seconds _timeoutReconnect = std::chrono::seconds(5);
		std::atomic<bool> _cancelf{ false };
		std::atomic<bool> _closedf = false;
		sndrcv_timeline_t _timeline;
		std::atomic<bool> _invokedf{ false };
		std::unique_ptr<i_xtimer_base_t> _expiredTimer;
		std::atomic < std::uintmax_t> _spinTagState{ 0 };
		timeline_trace _timelineTracer;


		enum class bind_point_t {
			extern_,
			internal_
			};

		bind_point_t _mode;
		async_space_t _scx{ async_space_t::undef };

#ifdef CBL_MPF_OBJECTS_COUNTER_CHECKER_SNDRCV_OPERATIONS
		crm::utility::__xobject_counter_logger_t<self_t> _xDbgCounter;
#endif


#ifdef CBL_SNDRCV_CYCLIC_USE_TRACE_SPIN_TAG
#define CBL_SNDRCV_KEYTRACE_UNSET(val)							CBL_MPF_KEYTRACE_UNSET(val)
#define CBL_SNDRCV_KEYTRACE_SET(x1,x2)							CBL_MPF_KEYTRACE_SET((x1), (x2))
#define CBL_SNDRCV_VERIFY(val)									CBL_VERIFY(val)
#define CBL_SNDRCV_TRACE_ID_INC(x1, x2)							CBL_MPF_TRACE_ID_INC((x1), (x2))
#define CBL_SNDRCV_TRACE_ID_DEC(val)							CBL_MPF_TRACE_ID_DEC(val)
#define CBL_SNDRCV_OBJECT_MONITOR_INC(x1,x2,x3)					CBL_OBJECT_MONITOR_INC((x1), (x2), (x3))
#define CBL_SNDRCV_OBJECT_MONITOR_DEC(x1,x2)					CBL_OBJECT_MONITOR_DEC((x1), (x2))
#define CBL_SNDRCV_TRACE_ID_HAS(val)							CBL_MPF_TRACE_ID_HAS(val)

		sx_locid_t _MessageSndRcvTraceId;
#else
#define CBL_SNDRCV_KEYTRACE_UNSET(val)
#define CBL_SNDRCV_KEYTRACE_SET(x1, x2)
#define CBL_SNDRCV_VERIFY(val)
#define CBL_SNDRCV_TRACE_ID_INC(x1, x2)
#define CBL_SNDRCV_TRACE_ID_DEC(val)
#define CBL_SNDRCV_OBJECT_MONITOR_INC(x1,x2,x3)
#define CBL_SNDRCV_OBJECT_MONITOR_DEC(x1,x2)
#define CBL_SNDRCV_TRACE_ID_HAS(val)
#endif


		std::string trace_name()const {
#ifdef XPEER_SNDRCV_CYCLIC_ACTOR_TRACE
			return _tag.name;
#else
			return {};
#endif
			}

		const std::string& name()const noexcept {
			return _session.sid().name();
			}

#ifdef CBL_MPF_STREAMED_OBJECTS_TRACE
#define CBL_MPF_STREAMED_OBJECTS_TRACE_PRINT(msg) crm::file_logger_t::logging(CBL_MPF_PREFIX((name() + ":" + (msg))), CBL_MPF_STREAMED_OBJECTS_TRACE_TAG);
#else
#define CBL_MPF_STREAMED_OBJECTS_TRACE_PRINT(msg)
#endif

		void ntf_hndl(std::unique_ptr<dcf_exception_t> && exc)noexcept {
			if(exc) {
				CBL_MPF_STREAMED_OBJECTS_TRACE_PRINT(cvt(exc->msg()));

				if(auto c = _ctx.lock()) {
					c->exc_hndl(std::move(exc));
					}
				}
			}

		bool check_remote_exception(const std::unique_ptr<dcf_exception_t> & exc_)const noexcept {
			if (exc_) {
				CBL_MPF_STREAMED_OBJECTS_TRACE_PRINT(cvt(exc_->msg()));

#ifdef CBL_SNDRCV_TRACE_COLLECT
				if (exc_) {
					exceptions_trace_collector::instance().add((*exc_));
					}
#endif

				/* ���� ����������� �� ���������� ������� ������ ����������� */
				if (dynamic_cast<const mpf_exception_t*>(exc_.get())) {
					return true;
					}
				else if (dynamic_cast<const abandoned_exception_t*>(exc_.get())) {
					return true;
					}
				else if (dynamic_cast<const mpf_test_exception_t*>(exc_.get())) {
					return true;
					}
				else if (
					/* ������ ������������ */
					dynamic_cast<const srlz::serialize_exception_t*>(exc_.get())
					/* �����-���� �������������� �� ����� ������������� ��������� */
					|| dynamic_cast<const types_mismatching_exception_t*>(exc_.get())) {

					return false;
					}
				else if (
					/* ���� ���������� � ���������������� ���� */
					dynamic_cast<const extern_code_exception_t*>(exc_.get())) {

					return _remoteExcHndl(exc_);
					}
				else {
					return _remoteExcHndl(exc_);
					}
				}
			else {
				return true;
				}
			}

		bool check_send_exception(const std::unique_ptr<dcf_exception_t> & exc)const noexcept {
			if(exc) {
				CBL_MPF_STREAMED_OBJECTS_TRACE_PRINT(cvt(exc->msg()));

#ifdef CBL_SNDRCV_TRACE_COLLECT
				if(exc) {
					exceptions_trace_collector::instance().add((*exc));
					}
#endif

				if(dynamic_cast<const mpf_test_exception_t*>(exc.get())) {
					return true;
					}
				else if(
					/* ���� ���������� � ���������������� ���� */
					dynamic_cast<const extern_code_exception_t*>(exc.get())
					/* ������ ������������ */
					|| dynamic_cast<const srlz::serialize_exception_t*>(exc.get())) {

					return false;
					}
				else {
					return true;
					}
				}
			else {
				return true;
				}
			}

		message_spin_tag_type shifft_spin_tag()noexcept {
			return make_message_spin_tag_by_value( ++_spinTagState);
			}

		message_spin_tag_type current_spin_tag()const noexcept {
			return make_message_spin_tag_by_value( _spinTagState);
			}

		void push_recovery()noexcept {
			if(auto ctx_ = ctx()) {
				if(ctx_->policies().timeout_reconnect_attempt().count()) {

					std::function<void(async_operation_result_t)> f;
					if(bind_point_t::internal_ == _mode) {
						f = [sptr = shared_from_this()](async_operation_result_t){
							sptr->_sh.push(__FILE_LINE__, [sptr] {
								sptr->recovery_launch();
								});
							};
						}
					else {
						f = [wptr = weak_from_this()](async_operation_result_t){
							if(auto s = wptr.lock()) {
								s->_sh.push(__FILE_LINE__, [s] {
									s->recovery_launch();
									});
								}
							};
						}

					ctx_->register_timer_deffered_action_sys(trace_name(), std::move(f), ctx_->policies().timeout_reconnect_attempt());
					}
				else {
					if(bind_point_t::internal_ == _mode) {
						_sh.push(__FILE_LINE__, [sptr = shared_from_this()]{
							sptr->recovery_launch();
							});
						}
					else {
						_sh.push(__FILE_LINE__, [wptr = weak_from_this()]{
							if(auto s = wptr.lock()) {
								s->recovery_launch();
								}
							});
						}
					}
				}
			}

		std::shared_ptr<cycle_flow_recovery_t> make_recovery() noexcept {

			if(!canceled()) {
				if(bind_point_t::internal_ == _mode) {
					return std::make_unique<cycle_flow_recovery_t>([sptr = shared_from_this()]{
						if(!sptr->canceled()) {
							sptr->push_recovery();
							}
						});
					}
				else {
					return std::make_unique<cycle_flow_recovery_t>([wptr = weak_from_this()]{
						if(auto sptr = wptr.lock()) {
							if(!sptr->canceled()) {
								sptr->push_recovery();
								}
							}
						});
					}
				}
			else {
				return std::unique_ptr<cycle_flow_recovery_t>();
				}
			}

		bool is_timeouted()const noexcept {
			return timeline().is_timeouted();
			}

		const sndrcv_timeline_t& timeline()const noexcept {
			return _timeline;
			}

		std::shared_ptr<distributed_ctx_t> ctx()const noexcept {
			return _ctx.lock();
			}

		std::unique_ptr<dcf_exception_t> capture_exception()noexcept {
			return std::move(_sExc);
			}

		void set_state(async_operation_result_t st)noexcept {
			_state = st;
			}

		async_operation_result_t state()const noexcept {
			return _state;
			}

		bool xio_renewable_connection()const noexcept {
			return _io.renewable_connection();
			}

		bool xio_closed()const noexcept {
			return _io.closed();
			}

		bool ready()const {
			return _invokedf.load(std::memory_order::memory_order_acquire);
			}

		bool canceled()const {
			return _cancelf.load(std::memory_order::memory_order_acquire);
			}

		/*! �������� ���-������ */
		void check_response(std::unique_ptr<i_iol_ihandler_t> && iObj_)noexcept {
			CBL_VERIFY(iObj_);

			try {
				if(!ready()) {

					/* ��������� ����� ���������� ������ �����������,
					��������������� �� ����� ����� ��������� �������� ������-�����, ��������� � ���,
					��� �������� ������ �������� ���������� */
					if(iObj_->type().type() == iol_types_t::st_disconnection) {
						CBL_MPF_STREAMED_OBJECTS_TRACE_PRINT("check_response:disconnect");

						rcv_invoke();
						}

					/* ���������� ��� �������� ���������, ��������������� �� �������� �������, ����������
					�������� ����������� ��������� ������� */
					else if(iObj_->type().type() == iol_types_t::st_notification) {
						CBL_MPF_STREAMED_OBJECTS_TRACE_PRINT("check_response:notification");

						if(auto iexcm = message_cast<inotification_t>(std::move(iObj_))) {

							auto e1_ = iexcm->release_ntf();

							if(e1_ && check_remote_exception(std::move(e1_))) {
								rcv_invoke();
								}
							else {
								end_invoke(std::move(e1_));
								}
							}
						else {
							end_invoke(CREATE_TERMINATE_OPERATION_PTR_EXC_FWD(nullptr));
							}
						}

					/* ���� ��������� ���������� ��������� ������-�����: ��������� ���������� ���� �������� */
					else {
						snd_request_invoke(std::move(iObj_));
						}
					}
				}
			catch(const dcf_exception_t & e0) {
				end_invoke(e0.dcpy());
				}
			catch(const std::exception & e1) {
				end_invoke(CREATE_MPF_PTR_EXC_FWD(e1));
				}
			catch(...) {
				end_invoke(CREATE_MPF_PTR_EXC_FWD(nullptr));
				}
			}

#ifdef CBL_USE_SEQUENTIAL_CALL_CHECKER
		detail::checker_seqcall_t _checkerCycleRecoveryGuarder;
#endif

		std::weak_ptr<cycle_flow_recovery_t> _rcv;
		void rcv_reset(std::shared_ptr<cycle_flow_recovery_t> obj)noexcept {
			CBL_MPF_STREAMED_OBJECTS_TRACE_PRINT("rcv_reset");

#ifdef CBL_USE_SEQUENTIAL_CALL_CHECKER
			auto scp = _checkerCycleRecoveryGuarder.inc_scope();
#endif

			decltype(_rcv) tr = obj;
			std::swap(tr, _rcv);

			if(auto str = tr.lock()) {
				CHECK_NO_EXCEPTION(str->reset());
				}
			}

		void rcv_invoke()noexcept {
			CBL_MPF_STREAMED_OBJECTS_TRACE_PRINT("rcv_invoke");

#ifdef CBL_USE_SEQUENTIAL_CALL_CHECKER
			auto scp = _checkerCycleRecoveryGuarder.inc_scope();
#endif

			decltype(_rcv) tr;
			std::swap(tr, _rcv);

			if(auto str = tr.lock()) {
				CHECK_NO_EXCEPTION(str->invoke());
				}
			}

		/* ��������� ��������� �������� */
		void bind_io_hndl(std::unique_ptr<dcf_exception_t>&& exc, std::unique_ptr<i_iol_ihandler_t>&& rcvObj)noexcept {
			if (!exc) {
				if (rcvObj) {
					check_response(std::move(rcvObj));
					}
				else {
					end_invoke(async_operation_result_t::st_exception);
					}
				}
			else {
				CBL_MPF_STREAMED_OBJECTS_TRACE_PRINT("__subscriber_invoke:err");

				if (check_remote_exception(std::move(exc))) {
					rcv_invoke();
					}
				else {
					end_invoke(std::move(exc));
					}
				}
			}

		decltype(auto) make_cyclic_bind_io_hndl_extern(std::shared_ptr<cycle_flow_recovery_t> rcv)noexcept {
			return[wthis = weak_from_this(), rcv]
			(std::unique_ptr<dcf_exception_t>&& e, std::unique_ptr<i_iol_ihandler_t>&& r, invoke_context_trait)noexcept{

				if (auto s = wthis.lock()) {
					s->bind_io_hndl(std::move(e), std::move(r));
					}
				else {
#ifdef CBL_SNDRCV_TRACE_COLLECT
					if (e) {
						exceptions_trace_collector::instance().add((*e));
						}
#endif
					}
				};
			}

		decltype(auto) make_cyclic_bind_io_hndl_internal(std::shared_ptr<cycle_flow_recovery_t> rcv)noexcept {
			return[sptr = shared_from_this(), rcv]
			(std::unique_ptr<dcf_exception_t>&& e, std::unique_ptr<i_iol_ihandler_t>&& r, invoke_context_trait) noexcept{
				sptr->bind_io_hndl(std::move(e), std::move(r));
				};
			}

		decltype(auto) make_cyclic_disconnection_event_hndl(std::weak_ptr<cycle_flow_recovery_t> wrcv)noexcept{

			/* ���������� ���������� ��������� ������� */
			return[wptr = weak_from_this(), wrcv](std::unique_ptr<i_event_t>&& /*args*/, subscribe_invoke)noexcept {
				if (auto sptr = wptr.lock()) {
					sptr->bind_io_hndl(CREATE_MPF_PTR_EXC_FWD(nullptr), nullptr);
					}

				if (auto rcv = wrcv.lock()) {
					rcv->invoke();
					}
				};
			}

		void launch_timered_ctx()noexcept {
			if(auto c = _ctx.lock()) {
				auto xt = c->create_timer_oncecall(_scx);

				/* ready - ����� ������� ����� �������� �������� */
				xt->start([wptr = weak_from_this()](const async_operation_result_t result){
					if(auto s = wptr.lock()) {

						/* ���� �������� �������� ������, ��������� �������� ������� */
						if(result == async_operation_result_t::st_ready) {
							s->end_invoke(CREATE_TIMEOUT_PTR_EXC_FWD(nullptr));
							}

						else if(result == async_operation_result_t::st_canceled) {
							s->end_invoke(CREATE_REFUSE_OPERATION_PTR_EXC_FWD(nullptr));
							}

						/* ���� ������ */
						else {
							s->end_invoke(CREATE_PTR_EXC_FWD(nullptr));
							}
						}
					}, timeline().timeout());

				_expiredTimer = std::move(xt);
				}
			else {
				end_invoke(CREATE_PTR_EXC_FWD(nullptr));
				}
			}

#ifdef CBL_USE_SEQUENTIAL_CALL_CHECKER
		checker_seqcall_t _checkerBindIO;

#endif

		void bind_io(std::shared_ptr<cycle_flow_recovery_t> rcv) {
			try {
				CBL_VERIFY(!is_null(_subscribeId));
				CBL_VERIFY(!is_null(_eventSubscripeId));

				CBL_MPF_STREAMED_OBJECTS_TRACE_PRINT("bind_io");

#ifdef CBL_USE_SEQUENTIAL_CALL_CHECKER
				auto checkerBindTail = _checkerBindIO.inc_scope();
#endif

				/* �������� �� ������� ��������� ��������� ����
				----------------------------------------------------------------------------------------------------*/
				_addrTargetType = _address_hndl_t::make(_subscribeId, _io.level(), _scx);
				CBL_VERIFY(_addrTargetType.invoke_abandoned());

				subscribe_options opt{ assign_options::overwrite_if_exits, false };

				if (bind_point_t::extern_ == _mode) {
					static_assert(is_callback_subscribe_message_invokable_v<decltype(make_cyclic_bind_io_hndl_extern(rcv))>, __FILE_LINE__);

					if (detail::subscribe_invoker_result::enum_type::success != _io.subscribe_hndl_id(_scx, _addrTargetType, opt,
						subscribe_message_handler("sndrcv-cyclic-bind_io-1", make_cyclic_bind_io_hndl_extern(rcv), inovked_as::async))) {
						end_invoke(CREATE_MPF_PTR_EXC_FWD(L"subscribe to response receive fault-1"));
						}
					}
				else {
					static_assert(is_callback_subscribe_message_invokable_v<decltype(make_cyclic_bind_io_hndl_internal(rcv))>, __FILE_LINE__);

					if (detail::subscribe_invoker_result::enum_type::success != _io.subscribe_hndl_id(_scx, _addrTargetType, opt,
						subscribe_message_handler("sndrcv-cyclic-bind_io-2", make_cyclic_bind_io_hndl_internal(rcv), inovked_as::async))) {
						end_invoke(CREATE_MPF_PTR_EXC_FWD(L"subscribe to response receive fault-2"));
						}
					}

				static_assert(is_event_callback_invokable_v<decltype(make_cyclic_disconnection_event_hndl(rcv))>, __FILE_LINE__);

				_addrDisconnectionHandler = _address_event_t::make(_eventSubscripeId, CHECK_PTR(ctx())->domain_id(), event_type_t::st_disconnection, _io.level());
				if (!_io.subscribe_event(_scx, _addrDisconnectionHandler, true, make_cyclic_disconnection_event_hndl(rcv))) {
					end_invoke(CREATE_MPF_PTR_EXC_FWD(L"subscribe to peer events fault"));
					}
				}
			catch (const dcf_exception_t & e0) {
				rcv->invoke();
				}
			catch (const std::exception & e1) {
				end_invoke( CREATE_MPF_PTR_EXC_FWD(e1));
				}
			catch (...) {
				end_invoke( CREATE_MPF_PTR_EXC_FWD(nullptr));
				}
			}

		void end_invoke(async_operation_result_t st, bool dctr = false)noexcept {
			__end_invoke(st, nullptr, dctr);
			}

		void end_invoke(std::unique_ptr<dcf_exception_t> && exc, bool dctr = false)noexcept {
			__end_invoke(async_operation_result_t::st_exception, std::move(exc), dctr);
			}

		void __end_invoke(async_operation_result_t st,
			std::unique_ptr<dcf_exception_t> && exc,
			bool dctr)noexcept {

			bool endf = false;
			if(_invokedf.compare_exchange_strong(endf, true)) {

				CBL_MPF_STREAMED_OBJECTS_TRACE_PRINT(std::string("__end_invoke:") + cvt2string(st));
				CBL_VERIFY(_ehndl);

				cancel();

				if(!dctr) {
					_sh.push(__FILE_LINE__, [wp = weak_from_this()]{
						if(auto s = wp.lock()) {
							s->rcv_reset(nullptr);
							}
						});
					}

				if(auto ctx_ = ctx()) {
					auto excTail = [this, &ctx_, st](std::unique_ptr<dcf_exception_t> && e)noexcept {
						if(_ehndl) {
							CHECK_NO_EXCEPTION(ctx_->launch_async(__FILE_LINE__,
								std::move(_ehndl), st, nullptr, nullptr, std::move(e)));
							}
						};

					try {
						if(dctr) {

							ctx_->launch_async(__FILE_LINE__, [](io_t && io, auto && mobj) {
								io.push(std::move(mobj));
								}, std::move(_io), _actor.make_close(_session));

							CBL_VERIFY(_ehndl);
							ctx_->launch_async(__FILE_LINE__,
								std::move(_ehndl), st, std::move(_send), _actor.release_handler(), std::move(exc));
							}
						else {
							_sh.push(__FILE_LINE__, [sptr = shared_from_this()]{
								sptr->_io.push(sptr->_actor.make_close(sptr->_session));
								});

							_sh.push(__FILE_LINE__, [sptr = shared_from_this(), st](decltype(_ehndl) && h, std::unique_ptr<dcf_exception_t> && e){
								if(h) {
									h(st, std::move(sptr->_send), sptr->_actor.release_handler(), std::move(e));
									}
								}, std::move(_ehndl), std::move(exc));
							}
						}
					catch(const dcf_exception_t & e0) {
						excTail(e0.dcpy());
						}
					catch(const std::exception & e1) {
						excTail(CREATE_PTR_EXC_FWD(e1));
						}
					catch(...) {
						excTail(CREATE_PTR_EXC_FWD(nullptr));
						}

					CBL_VERIFY(!_ehndl);
					}
				}
			}

		void launch_cycle_0_invoke(std::shared_ptr<cycle_flow_recovery_t> rcv) {
			bind_io(rcv);

			/* ������ ��������� � ��������� */
			if(is_timeouted()) {

				_sh.push(__FILE_LINE__, [wptr = weak_from_this()]{
					if(auto s = wptr.lock()) {

						/* �������� ������� */
						s->snd_request_invoke(nullptr);

						/* ������ ������� �������� */
						s->launch_timered_ctx();
						}
					});
				}

			/* ������ ��������� ��������������� �������� */
			else {
				_sh.push(__FILE_LINE__, [wptr = weak_from_this()]{
					if(auto s = wptr.lock()) {

						/* �������� ������� */
						s->snd_request_invoke(nullptr);
						}
					});
				}
			}

		bool _isLaunched{ false };
		void launch_cycle_0() {

			CBL_VERIFY(!_isLaunched);
			_isLaunched = true;

			_sh.push(__FILE_LINE__, [wptr = weak_from_this()](decltype(make_recovery()) && rcv){
				if(auto s = wptr.lock()) {
					s->launch_cycle_0_invoke(std::move(rcv));
					}
				}, make_recovery());
			}

		void recovery_launch() {
			if(!canceled()) {
				CBL_MPF_STREAMED_OBJECTS_TRACE_PRINT("recovery_launch:0");

				bind_io(make_recovery());
				snd_request_invoke(nullptr);
				}
			else {
				CBL_MPF_STREAMED_OBJECTS_TRACE_PRINT("recovery_launch:1");

				rcv_reset(nullptr);
				}
			}

		void _continue_condition_hndl(continue_condition_eof){
			end_invoke(async_operation_result_t::st_ready);
			}

		/* ��������� ��������� ���������� ��������� � ����� ��������� �������� */
		template<typename Message,
			typename std::enable_if_t<is_out_message_requirements_v<Message>, int> = 0>
		decltype(auto) make_tail_1()noexcept {

			return [wp = weak_from_this()](Message && rqm)noexcept{
				if (auto s = wp.lock()) {
					auto exchndl = [&s](std::unique_ptr<dcf_exception_t>&& exc)noexcept {
						if (s->check_send_exception(exc)) {
							s->ntf_hndl(std::move(exc));
							s->rcv_invoke();
							}
						else {
							s->end_invoke(std::move(exc));
							}
						};

					try {
						MPF_TEST_EMULATION_IN_CONTEXT_COND_TRCX(s->ctx(), 721,
							is_trait_at_emulation_context(rqm),
							trait_at_keytrace_set(rqm));

						/* ���������� ���������� ������ �� "����" �������
						-------------------------------------------------------------*/
						rqm.set_source_subscriber_id(s->_addrTargetType.id());
						rqm.set_spin_tag(s->shifft_spin_tag());

						/* ��������
						-------------------------------------------------------------*/
						s->_io.push(std::forward<Message>(rqm));
						s->_timelineTracer.mark_snd();
						}
					catch (const dcf_exception_t & exc0) {
						exchndl(exc0.dcpy());
						}
					catch (const std::exception & exc1) {
						exchndl(CREATE_EXTERN_CODE_PTR_EXC_FWD(exc1));
						}
					catch (...) {
						exchndl(CREATE_EXTERN_CODE_PTR_EXC_FWD(nullptr));
						}
					}
				};
			}

		template<typename Message,
			typename std::enable_if_t < is_out_message_requirements_v<Message>, int> = 0>
		void _continue_condition_hndl(Message&& m){
			CBL_VERIFY(!m.type().fl_report());

			/* �����-���������� */
			if (auto prevTail = m.reset_invoke_exception_guard(inbound_message_with_tail_exception_guard(
				[wptr = weak_from_this(), opn = name()](std::unique_ptr<dcf_exception_t>&& exc)noexcept {

				CHECK_NO_EXCEPTION_BEGIN

				std::wostringstream what;
				what << L"sndrcv-guard(initial)";
				what << L":name=" << cvt<wchar_t>(opn);

				if (!exc) {
					exc = CREATE_MPF_PTR_EXC_FWD(what.str());
					exc->set_code(RQo_B);
					}
				else {
					exc->add_message_line(what.str());
					}

				if (auto s = wptr.lock()) {
					if (s->check_send_exception(exc)) {
						s->_sh.push(__FILE_LINE__, [wptr] {
							if (auto s = wptr.lock()) {
								s->rcv_invoke();
								}
							});
						}
					else {
						s->end_invoke(std::move(exc));
						}
					}

				CHECK_NO_EXCEPTION_END
				},
					ctx(),
					__FILE_LINE__,
					_scx))) {

				prevTail.reset();
				}

			/* ������ ��������� �������� */
			_sh.push(__FILE_LINE__, make_tail_1<std::decay_t<Message>>(), std::forward<Message>(m));
			}

		template<typename Exception,
			typename std::enable_if_t<std::is_base_of_v<dcf_exception_t, std::decay_t<Exception>>, int> = 0>
		void _continue_condition_hndl(std::unique_ptr<Exception>&& e){
			end_invoke(std::move(e));
			}

		void snd_request_invoke(std::unique_ptr<i_iol_ihandler_t> && irsm)noexcept {

			bool doIt{ false };
			if(!xio_closed()) {
				doIt = true;
				}
			else {
				if(xio_renewable_connection()) {
					doIt = true;
					}
				}

			if(doIt) {
				try {
					/* �������� ��������� */
					auto requestStage = [wptr = weak_from_this()](std::unique_ptr<i_iol_ihandler_t> && irsm)noexcept{

						if(auto s = wptr.lock()) {
							if(!irsm || irsm->spin_tag() == s->current_spin_tag()) {

								auto exchndl = [&s](std::unique_ptr<dcf_exception_t> && exc)noexcept {
									if(s->check_send_exception(exc)) {
										s->ntf_hndl(std::move(exc));
										s->rcv_invoke();
										}
									else {
										s->end_invoke(std::move(exc));
										}
									};

								try {
									MPF_TEST_EMULATION_IN_CONTEXT_COND_TRCX(s->ctx(), 720, 
										(irsm && is_trait_at_emulation_context(*irsm)),
										trait_at_keytrace_set(*irsm));

									s->_actor.next(continue_condition(wptr), s->_session, std::move(irsm));
									}
								catch(const dcf_exception_t & exc0) {
									exchndl(exc0.dcpy());
									}
								catch(const std::exception & exc1) {
									exchndl(CREATE_EXTERN_CODE_PTR_EXC_FWD(exc1));
									}
								catch(...) {
									exchndl(CREATE_EXTERN_CODE_PTR_EXC_FWD(nullptr));
									}
								}
							else {
								s->rcv_invoke();
								}
							}
						};

					/* ������ �������� �������� ��������� */
					_sh.push(__FILE_LINE__, std::move(requestStage), std::move(irsm));
					}
				catch(const dcf_exception_t & exc0) {
					end_invoke(exc0.dcpy());
					}
				catch(const std::exception & exc1) {
					end_invoke(CREATE_EXTERN_CODE_PTR_EXC_FWD(exc1));
					}
				catch(...) {
					end_invoke(CREATE_EXTERN_CODE_PTR_EXC_FWD(nullptr));
					}
				}
			else {
				end_invoke(CREATE_MPF_PTR_EXC_FWD(nullptr));
				}
			}

	public:
		template<typename _Xio,
			typename _XActor,
			typename _XEndHndl,
			typename _TRemoteExceptionHandler,
			typename XTypeIO = typename std::decay_t<_Xio>,
			typename XTypeActor = typename std::decay_t<_XActor>,
			typename XEndHndl = typename std::decay_t<_XEndHndl>,
			typename TRemoteExceptionHandler = typename std::decay_t<_TRemoteExceptionHandler>,
			typename std::enable_if_t<(
				is_sendrecv_cyclic_actor_invokable_v<XTypeActor, TypeIO> &&
				is_sendrecv_cyclic_end_invokable_v<XEndHndl> &&
				is_remote_exception_handler_invokable_v<TRemoteExceptionHandler> &&
				is_xpeer_trait_z_v<TypeIO>
				), int> = 0>
			sndrcv_cyclic_actor_t(
#if defined (XPEER_SNDRCV_CYCLIC_ACTOR_TRACE)
				trace_tag && tag,
#else
				trace_tag&&,
#endif
				
				async_space_t scx_,
				std::weak_ptr<distributed_ctx_t> ctx_,
				_Xio && io,
				_XActor && actor,
				_XEndHndl && endh,
				_TRemoteExceptionHandler&& reh,
				const sndrcv_timeline_t & tml,
				bind_point_t m)
			: _ctx(ctx_)

#if defined (XPEER_SNDRCV_CYCLIC_ACTOR_TRACE)
			, _tag(std::move(tag))
#else

#endif

			, _io(std::forward<_Xio>(io))
			, _session(_io.remote_object_endpoint(), session_description_t(CHECK_PTR(_ctx)->make_local_object_identity(), actor.name()))
			, _subscribeId(CHECK_PTR(ctx_)->make_local_object_identity())
			, _eventSubscripeId(CHECK_PTR(ctx_)->make_event_identity())
			, _actor(std::forward<_XActor>(actor))
			, _sh(CHECK_PTR(ctx_)->get_sequential_handler(_actor.space()))
			, _ehndl(std::forward<_XEndHndl>(endh), ctx_, __FILE_LINE__, scx_)
			, _remoteExcHndl(std::forward<_TRemoteExceptionHandler>(reh))
			, _timeline(tml)
			, _mode{ m }
			, _scx(scx_)

#if defined( CBL_MPF_OBJECTS_COUNTER_CHECKER_SNDRCV_OPERATIONS) && defined(XPEER_SNDRCV_CYCLIC_ACTOR_TRACE)
			, _xDbgCounter{ _tag.name }
#endif

			{
			if(!_ehndl) {
				THROW_MPF_EXC_FWD(nullptr);
				}

			CBL_VERIFY(!is_null(_subscribeId));
			CBL_VERIFY(!is_null(_eventSubscripeId));
			}

		void unregister()noexcept {
			/* ���������� �� �������� ��������� */
			CHECK_NO_EXCEPTION(_io.unsubscribe_hndl_id(_addrTargetType));
			}

	public:
		sndrcv_cyclic_actor_t(const sndrcv_cyclic_actor_t &) = delete;
		sndrcv_cyclic_actor_t& operator=(const sndrcv_cyclic_actor_t &) = delete;

		~sndrcv_cyclic_actor_t() {
			CHECK_NO_EXCEPTION(_close(true));
			CHECK_NO_EXCEPTION(unregister());
			}

		void close()noexcept {
			_close(false);
			}

	private:
		void _close(bool dctr)noexcept {

			/* ������ �������� */
			cancel();

			/* ����� ������� ��������� ������ */
			end_invoke(async_operation_result_t::st_abandoned, dctr);

			bool clf = false;
			if(_closedf.compare_exchange_strong(clf, true)) {

				/* ���������� ������ */
				if(_expiredTimer) {
					_expiredTimer->stop();
					_expiredTimer.reset();
					}

				if (dctr) {
					if (auto c = ctx()) {
						c->launch_async(__FILE_LINE__,
							[](decltype(_io) && io, decltype(_addrTargetType) && addr, decltype(_addrDisconnectionHandler) && evAddr) {

							io.unsubscribe_hndl_id(addr);
							io.unsubscribe_event(evAddr);

							}, std::move(_io), std::move(_addrTargetType), std::move(_addrDisconnectionHandler));
						}
					}
				else {
					if (auto c = ctx()) {
						c->launch_async(__FILE_LINE__,
							[wptr = weak_from_this()](const decltype(_addrTargetType)& addr, const decltype(_addrDisconnectionHandler)& evAddr) {

							if (auto s = wptr.lock()) {
								s->_io.unsubscribe_hndl_id(addr);
								s->_io.unsubscribe_event(evAddr);
								}
							}, _addrTargetType, _addrDisconnectionHandler);

						CBL_VERIFY(!is_null(_addrTargetType.id()));
						CBL_VERIFY(!is_null(_addrDisconnectionHandler));
						}
					}
				}
			}

	public:
		template<typename _Xio,
			typename _XActor,
			typename _XEndHndl,
			typename _TRemoteExceptionHandler,
			typename XTypeIO = typename std::decay_t<_Xio>,
			typename XTypeActor = typename std::decay_t<_XActor>,
			typename XEndHndl = typename std::decay_t<_XEndHndl>,
			typename XRemoteExceptionHandler = typename std::decay_t<_TRemoteExceptionHandler>,
			typename std::enable_if_t<(
				is_sendrecv_cyclic_actor_invokable_v<XTypeActor, TypeIO> &&
				is_sendrecv_cyclic_end_invokable_v<XEndHndl> &&
				is_remote_exception_handler_invokable_v< TRemoteExceptionHandler> &&
				is_xpeer_trait_z_v<TypeIO>
				), int> = 0>
			static handler_t hlaunch(trace_tag && tag, 
				async_space_t scx,
				std::weak_ptr<distributed_ctx_t> ctx_,
				_Xio && io,
				_XActor && actor,
				_XEndHndl && endh,
				_TRemoteExceptionHandler&& reh,
				sndrcv_timeline_t tml) {

			auto obj = std::make_shared<self_t>(std::move(tag),
				scx,
				ctx_,
				std::forward<_Xio>(io),
				std::forward<_XActor>(actor),
				std::forward<_XEndHndl>(endh),
				std::forward<_TRemoteExceptionHandler>(reh),
				std::move(tml),
				bind_point_t::extern_);
			obj->launch_cycle_0();

			return handler_t(std::move(obj));
			}

		template<typename _Xio,
			typename _XActor,
			typename _XEndHndl,
			typename _TRemoteExceptionHandler,
			typename XTypeIO = typename std::decay_t<_Xio>,
			typename XTypeActor = typename std::decay_t<_XActor>,
			typename XEndHndl = typename std::decay_t<_XEndHndl>,
			typename XRemoteExceptionHandler = typename std::decay_t<_TRemoteExceptionHandler>,
			typename std::enable_if_t<(
				is_sendrecv_cyclic_actor_invokable_v<XTypeActor, TypeIO> &&
				is_sendrecv_cyclic_end_invokable_v<XEndHndl> &&
				is_remote_exception_handler_invokable_v< TRemoteExceptionHandler> &&
				is_xpeer_trait_z_v<TypeIO>
				), int> = 0>
			static void alaunch(trace_tag && tag, 
				async_space_t scx,
				std::weak_ptr<distributed_ctx_t> ctx_,
				_Xio && io,
				_XActor && actor,
				_XEndHndl && endh,
				_TRemoteExceptionHandler&& reh,
				const sndrcv_timeline_t & tml) {

			auto obj = std::make_shared<self_t>(std::move(tag),
				scx,
				ctx_,
				std::forward<_Xio>(io),
				std::forward<_XActor>(actor),
				std::forward<_XEndHndl>(endh),
				std::forward<_TRemoteExceptionHandler>(reh),
				std::move(tml),
				bind_point_t::internal_);
			obj->launch_cycle_0();
			}

		void cancel()noexcept {
			_cancelf.store(true, std::memory_order::memory_order_release);
			}
	};


	template<typename _Xio>
	using invokable_peer_type = typename std::decay_t<decltype(make_invokable_xpeer(std::declval<_Xio>()))>;

	template<typename _Xio,
		typename _XActor,
		typename _XExceptionHndl,
		typename _XEndHndl>
		struct async_sndrcv_cyclic_type_deduction_t {

		static_assert(is_sendrecv_cyclic_actor_invokable_v<_XActor, invokable_peer_type<_Xio>>, __FILE_LINE__);
		using xio_type = typename invokable_peer_type<_Xio>;

		static_assert(is_sendrecv_cyclic_end_invokable_v<_XEndHndl>, __FILE_LINE__);
		static_assert(is_remote_exception_handler_invokable_v< _XExceptionHndl>, __FILE_LINE__);
		static_assert(is_xpeer_trait_z_v<xio_type>, __FILE_LINE__);

		using actor_type = typename sndrcv_cyclic_actor_t<_Xio, _XActor, _XEndHndl, _XExceptionHndl>;
		};

	template<typename _Xio,
		typename _XActor,
		typename _XExceptionHndl,
		typename _XEndHndl>
		auto async_sndrcv_cyclic_type_deduction_expression(_Xio&& , _XActor&& , _XExceptionHndl&&, _XEndHndl &&)
		-> typename async_sndrcv_cyclic_type_deduction_t<_Xio, _XActor, _XExceptionHndl, _XEndHndl>::actor_type;


template<typename _Xio,
	typename _XActor,
	typename _XExceptionHndl,
	typename _XEndHndl>
using sendrecv_actor_type= typename std::decay_t<decltype(async_sndrcv_cyclic_type_deduction_expression(
	std::declval<typename invokable_peer_type<_Xio>>(),
	std::declval<_XActor>(),
	std::declval<_XExceptionHndl>(),
	std::declval<_XEndHndl>()))>;

template<typename _Xio,
	typename _XActor, 
	typename _XExceptionHndl,
	typename _XEndHndl>
void make_async_sndrcv_cyclic_base(trace_tag&& tag, 
	async_space_t scx,
	std::weak_ptr<distributed_ctx_t> ctx_,
	_Xio && io,
	_XActor && actor,
	_XEndHndl && endh,
	_XExceptionHndl&& remoteExcH,
	const sndrcv_timeline_t & tml ){

	static_assert(is_sendrecv_cyclic_actor_invokable_v<_XActor, invokable_peer_type<_Xio>>, __FILE_LINE__);
	using xio_type = typename invokable_peer_type<_Xio>;

	static_assert(is_sendrecv_cyclic_end_invokable_v<_XEndHndl>, __FILE_LINE__);
	static_assert(is_remote_exception_handler_invokable_v< _XExceptionHndl>, __FILE_LINE__);
	static_assert(is_xpeer_trait_z_v<xio_type>, __FILE_LINE__);
	using actor_type = sendrecv_actor_type<_Xio, _XActor, _XExceptionHndl, _XEndHndl>;

	actor_type::alaunch(std::move(tag),
		scx,
		std::move( ctx_ ),
		make_invokable_xpeer( std::forward<_Xio>( io ) ),
		std::forward<_XActor>( actor ), 
		std::forward<_XEndHndl>( endh ),
		std::forward<_XExceptionHndl>( remoteExcH ), 
		tml );
	}

template<typename _Xio,
	typename _XActor,
	typename _XEndHndl,
	typename _XExceptionHndl>
auto make_handler_sndrcv_cyclic_base(trace_tag&& tag, 
	async_space_t scx,
	std::weak_ptr<distributed_ctx_t> ctx_,
	_Xio && io,
	_XActor && actor,
	_XEndHndl && endh,
	_XExceptionHndl && remoteExcH,
	const sndrcv_timeline_t & tml ){

	static_assert(is_sendrecv_cyclic_actor_invokable_v<_XActor, invokable_peer_type<_Xio>>, __FILE_LINE__);
	using xio_type = invokable_peer_type<_Xio>;

	static_assert(is_sendrecv_cyclic_end_invokable_v<_XEndHndl>, __FILE_LINE__);
	static_assert(is_remote_exception_handler_invokable_v< _XExceptionHndl>, __FILE_LINE__);
	static_assert(is_xpeer_trait_z_v<xio_type>, __FILE_LINE__);
	using actor_type = sendrecv_actor_type<_Xio, _XActor, _XExceptionHndl, _XEndHndl>;

	return actor_type::hlaunch(std::move(tag),
		scx,
		std::move( ctx_ ),
		make_invokable_xpeer( std::forward<_Xio>( io ) ),
		std::forward<_XActor>( actor ),
		std::forward<_XEndHndl>( endh ),
		std::forward<_XExceptionHndl>( remoteExcH ),
		tml );
	}
}


template<typename _Xio,
	typename _XActor,
	typename _XExceptionHndl,
	typename _XEndHndl>
	void make_async_sndrcv_cyclic(trace_tag&& tag, 
		async_space_t scx,
		std::weak_ptr<distributed_ctx_t> ctx_,
		_Xio && io,
		_XActor && actor,
		_XEndHndl && endh,
		_XExceptionHndl && remoteExcH,
		const sndrcv_timeline_t & tml) {

	detail::make_async_sndrcv_cyclic_base(std::move(tag),
		scx,
		std::move(ctx_),
		std::forward<_Xio>(io),
		std::forward<_XActor>(actor),
		std::forward<_XEndHndl>(endh),
		std::forward<_XExceptionHndl>(remoteExcH),
		tml );
	}


template<typename _Xio,
	typename _XActor,
	typename _XEndHndl,
	typename _XExceptionHndl>
	auto make_handler_sndrcv_cyclic(trace_tag&& tag, 
		async_space_t scx,
		std::weak_ptr<distributed_ctx_t> ctx_,
		_Xio && io,
		_XActor && actor,
		_XEndHndl && endh,
		_XExceptionHndl && remoteExcH,
		const sndrcv_timeline_t & tml) {

	return detail::make_handler_sndrcv_cyclic_base(std::move(tag),
		scx,
		async_space_t::ext,
		std::move(ctx_),
		make_invokable_xpeer(std::forward<_Xio>(io)),
		std::forward<_XActor>(actor),
		std::forward<_XEndHndl>(endh),
		std::forward<_XExceptionHndl>(remoteExcH),
		tml);
	}
}



