#pragma once


#include <functional>
#include <chrono>
#include <atomic>
#include <memory>
#include <type_traits>
#include "../internal_invariants.h"
#include "../utilities/utilities.h"
#include "../messages/terms.h"
#include "./xpeer_traits.h"
#include "./xpeer_invoke_trait.h"
#include "../erremlt/utilities.h"


#if defined(CBL_USE_OBJECTS_MONITOR) || defined(CBL_MPF_MESSAGE_TRACEWAY_KEYS_BY_ID)
#define CBL_SNDRCV_USE_TRACE_SPIN_TAG
#endif


namespace crm{
namespace detail{


extern std::atomic<std::intmax_t> SendRecvOperationsCounter;
size_t get_stat_counter_sendrecv_operations()noexcept;


#ifdef CBL_SNDRCV_USE_TRACE_SPIN_TAG
extern std::atomic<std::uintmax_t> GlobalMessageSndRcvTraceCounter;
#endif



template<typename TSendrecvRequest, typename Tio, typename = std::void_t<>>
struct is_sendrecv_request_invokable : std::false_type {};

template<typename TSendrecvRequest, typename Tio>
struct is_sendrecv_request_invokable < TSendrecvRequest, Tio,
	std::void_t<decltype(std::declval<Tio>().push(std::declval<std::invoke_result_t<TSendrecvRequest>>()))>> : std::true_type{};

template<typename TSendrecvRequest, typename Tio>
static constexpr bool is_sendrecv_request_invokable_v = is_sendrecv_request_invokable<TSendrecvRequest, Tio>::value;


#if defined (CBL_MPF_ENABLE_TIMETRACE) || defined(CBL_MPF_USE_REVERSE_EXCEPTION_TRACE) || defined(CBL_TRACELEVEL_TRACE_TAG_ON)
#define XPEER_SNDRCV_ACTOR_TRACE
#endif


template<typename _Tio,
	typename _TSendCtr,
	typename _TCallback,
	typename TypeIO = typename std::decay_t<_Tio>,
	typename TypeSendCtr = typename std::decay_t<_TSendCtr>,
	typename TCallback = typename std::decay_t<_TCallback>,
	typename std::enable_if_t<(
		is_sendrecv_request_invokable_v<TypeSendCtr, TypeIO> &&
		is_sndrcv_callback_v<TCallback> &&
		is_xpeer_trait_z_v<TypeIO>
		), int> = 0,
	const bool CallbackNoThrowInvokable = is_sndrcv_callback_no_throw_v<TCallback>
>
class sndrcv_once_actor_t final :
	public i_async_expression_handler_t,
	public i_terminatable_timeout_t,
	public std::enable_shared_from_this<sndrcv_once_actor_t< _Tio, _TSendCtr, _TCallback>> {
public:
	using io_t = typename TypeIO;
	using send_message_ctr_t = typename TypeSendCtr;
	using self_t = typename sndrcv_once_actor_t<_Tio, _TSendCtr, _TCallback>;
	using echo_callback_t = typename TCallback;
	using request_message_type = std::decay_t<decltype(std::declval<send_message_ctr_t>()())>;

private:
	std::weak_ptr<distributed_ctx_t> _ctx;
#if defined (XPEER_SNDRCV_ACTOR_TRACE)
	trace_tag _tag;
#endif

	io_t _io;
	subscriber_node_id_t _subscribeId;
	subscriber_event_id_t _eventSubscripeId;
	send_message_ctr_t _ctr;
	echo_callback_t _clb;
	_address_hndl_t _addrTargetType;
	_address_event_t _addrDisconnectionHandler;
	std::atomic<bool> _cancelf{ false };
	std::atomic<bool> _closedf = false;
	sndrcv_timeline_t _timeline;
	std::atomic<bool> _invokedf{ false };
	std::chrono::system_clock::time_point _timepointStart;
	std::unique_ptr<i_xtimer_base_t> _expiredTimer;


#ifdef CBL_SNDRCV_USE_TRACE_SPIN_TAG
	#define CBL_SNDRCV_KEYTRACE_UNSET(val)							CBL_MPF_KEYTRACE_UNSET(val)
	#define CBL_SNDRCV_KEYTRACE_SET(x1,x2)							CBL_MPF_KEYTRACE_SET((x1), (x2))
	#define CBL_SNDRCV_VERIFY(val)									CBL_VERIFY(val)
	#define CBL_SNDRCV_TRACE_ID_INC(x1, x2)							CBL_MPF_TRACE_ID_INC((x1), (x2))
	#define CBL_SNDRCV_TRACE_ID_DEC(val)							CBL_MPF_TRACE_ID_DEC(val)
	#define CBL_SNDRCV_OBJECT_MONITOR_INC(x1,x2,x3)					CBL_OBJECT_MONITOR_INC((x1), (x2), (x3))
	#define CBL_SNDRCV_OBJECT_MONITOR_DEC(x1,x2)					CBL_OBJECT_MONITOR_DEC((x1), (x2))
	#define CBL_SNDRCV_TRACE_ID_HAS(val)							CBL_MPF_TRACE_ID_HAS(val)

	sx_locid_t _MessageSndRcvTraceId;
#else
	#define CBL_SNDRCV_KEYTRACE_UNSET(val)
	#define CBL_SNDRCV_KEYTRACE_SET(x1, x2)
	#define CBL_SNDRCV_VERIFY(val)
	#define CBL_SNDRCV_TRACE_ID_INC(x1, x2)
	#define CBL_SNDRCV_TRACE_ID_DEC(val)
	#define CBL_SNDRCV_OBJECT_MONITOR_INC(x1,x2,x3)
	#define CBL_SNDRCV_OBJECT_MONITOR_DEC(x1,x2)
	#define CBL_SNDRCV_TRACE_ID_HAS(val)
#endif

	enum class invoke_handler_as {
		async,
		instack
		};

	enum class bind_point_t {
		extern_,
		internal_
		};

	bind_point_t _mode;
	inovked_as _clbInvokeAs;
	async_space_t _scx{ async_space_t::undef };

#ifdef CBL_MPF_OBJECTS_COUNTER_CHECKER_SNDRCV_OPERATIONS
	crm::utility::__xobject_counter_logger_t<self_t, 1> _xDbgCounter;
#endif

	std::string trace_name()const {
#ifdef XPEER_SNDRCV_ACTOR_TRACE
		return _tag.name;
#else
		return {};
#endif
		}

	template<typename TF, const std::enable_if_t<crm::is_bool_cast_v<TF>, bool> IsBoolCast = true>
	static constexpr bool callback_check_nullptr(const TF& f)noexcept {
		static_assert(crm::is_bool_cast_v<TF>, __FILE_LINE__);
		if (f) {
			return false;
			}
		else {
			return true;
			}
		}

	template<typename TF, const std::enable_if_t<!crm::is_bool_cast_v<TF>, bool> IsBoolCast = false>
	static constexpr bool callback_check_nullptr(const TF& )noexcept {
		static_assert(!crm::is_bool_cast_v<TF>, __FILE_LINE__);
		return false;
		}

	bool is_timeouted()const noexcept {
		return timeline().is_timeouted();
		}

	const sndrcv_timeline_t& timeline()const noexcept {
		return _timeline;
		}

	std::shared_ptr<distributed_ctx_t> ctx()const noexcept {
		return _ctx.lock();
		}

	bool ready()const noexcept {
		return _invokedf.load(std::memory_order::memory_order_acquire);
		}

	bool canceled()const noexcept {
		return _cancelf.load(std::memory_order::memory_order_acquire);
		}

	/*! ���������� � ������������� ����������� */
	void invoke_rejected(crm::async_operation_result_t rst)noexcept {
		__invoke(invoke_handler_as::async, rst, nullptr, nullptr, invoke_context_trait::sync);
		}

	/*! ���������� � ������������� ����������� */
	void invoke_dctr(crm::async_operation_result_t rst)noexcept {
		__invoke(invoke_handler_as::async, rst, nullptr, nullptr, invoke_context_trait::sync);
		}

	/*! ���������� � ����������� */
	void invoke_exception(std::unique_ptr<crm::dcf_exception_t> && exc,
		invoke_context_trait ic = invoke_context_trait::sync,
		invoke_handler_as as_ = invoke_handler_as::async)noexcept {

		__invoke(as_, 
			crm::async_operation_result_t::st_exception,
			nullptr,
			std::move(exc),
			ic );
		}

	/*! ���������� � ����������� */
	void invoke_exception(std::unique_ptr<mpf_exception_t> & exc, 
		invoke_context_trait ic = invoke_context_trait::sync, 
		invoke_handler_as as_ = invoke_handler_as::async)noexcept {

		invoke_exception(std::move(exc), ic, as_);
		}

	/*! ���������� � ����������� */
	void invoke_exception(const crm::dcf_exception_t & exc, invoke_context_trait ic = invoke_context_trait::sync)noexcept {
		invoke_exception(exc.dcpy(), ic);
		}

	/*! ���������� � ����������� */
	void invoke_exception(crm::dcf_exception_t && exc, invoke_context_trait ic = invoke_context_trait::sync) noexcept {
		invoke_exception(exc.dcpy(), ic);
		}

	/*! ���������� � ����������� �� �������� ������� */
	void invoke_exception(std::unique_ptr<i_iol_ihandler_t> && iObj_, 
		invoke_context_trait ic = invoke_context_trait::sync, 
		invoke_handler_as as_ = invoke_handler_as::async)noexcept {

		auto iexc = message_cast<inotification_t>(std::move(iObj_));
		if (iexc) {
			invoke_exception(iexc->release_ntf(), ic, as_);
			}
		else {
			FATAL_ERROR_FWD(nullptr);
			}
		}

	/*! �������� ���-������ */
	void check_response(std::unique_ptr<i_iol_ihandler_t>&& iObj, invoke_context_trait ic)noexcept {
		/*-------------------------------------------------------------------------
		���������� ����������
		---------------------------------------------------------------------------*/
		if (!ready()) {
			/* ������� ��������� - ��������, � ��������, �� ������ ����� ������������� */
			if (!iObj) {
				invoke_exception(CREATE_MPF_PTR_EXC_FWD(nullptr), ic, invoke_handler_as::async);
				}

			/* ��������� ����� ���������� ������ �����������,
			��������������� �� ����� ����� ��������� �������� ������-�����, ��������� � ���,
			��� �������� ������ �������� ���������� */
			else if (iObj->type().type() == iol_types_t::st_disconnection) {
				invoke_exception(CREATE_MPF_PTR_EXC_FWD(nullptr), ic, invoke_handler_as::async);
				}

			/* ���������� ��� �������� ���������, ��������������� �� �������� �������, ����������
			�������� ����������� ��������� ������� */
			else if (iObj->type().type() == iol_types_t::st_notification) {
				CBL_VERIFY(iObj);
				invoke_exception(std::move(iObj), ic, invoke_handler_as::async);
				}

			/* ���� ��������� ���������� ��������� ������-�����: ��������� ���������� ���� �������� */
			else {
#ifdef CBL_MPF_MESSAGE_TRACEWAY_KEYS_BY_ID
				if (_MessageSndRcvTraceId == iObj->spin_tag()) {
					__invoke(invoke_handler_as::instack,
						crm::async_operation_result_t::st_ready,
						std::move(iObj),
						nullptr,
						ic);
					}
				else {
					invoke_exception(CREATE_MPF_PTR_EXC_FWD(nullptr), ic, invoke_handler_as::async);
					FATAL_ERROR_FWD(nullptr);
					}

#else

				__invoke(invoke_handler_as::instack,
					crm::async_operation_result_t::st_ready,
					std::move(iObj),
					nullptr,
					ic);
#endif
				}
			}
		}

	bool launch_timered_ctx_cyclic(crm::async_operation_result_t result) {
		bool next = false;
		try {
			/* ���� �������� �������� ������, ��������� �������� ������� */
			if (result == crm::async_operation_result_t::st_ready) {
				/*next = cycle_shifft();*/
				invoke_rejected(crm::async_operation_result_t::st_canceled);
				}

			else if (result == crm::async_operation_result_t::st_canceled) {
				invoke_rejected(crm::async_operation_result_t::st_canceled);
				}

			/* ���� ������ */
			else {
				invoke_exception(CREATE_MPF_PTR_EXC_FWD(nullptr));
				}
			}
		catch (const dcf_exception_t & exc0) {
			invoke_exception(exc0.dcpy());
			}
		catch (const std::exception & exc1) {
			invoke_exception(CREATE_MPF_PTR_EXC_FWD(exc1));
			}
		catch (...) {
			invoke_exception(CREATE_PTR_EXC_FWD(nullptr));
			}

		/* ����������� ��� ��� */
		return next;
		}

	bool launch_timered_ctx_oncecall(crm::async_operation_result_t result) {
		try {

			/* ���� �������� �������� ������, ��������� �������� ������� */
			if (result == crm::async_operation_result_t::st_ready) {
				invoke_rejected(crm::async_operation_result_t::st_timeouted);
				}

			else if (result == crm::async_operation_result_t::st_canceled) {
				invoke_rejected(crm::async_operation_result_t::st_canceled);
				}

			/* ���� ������ */
			else {
				invoke_exception(CREATE_MPF_PTR_EXC_FWD(nullptr));
				}
			}
		catch (const dcf_exception_t & exc0) {
			invoke_exception(exc0.dcpy());
			}
		catch (const std::exception & exc1) {
			invoke_exception(CREATE_MPF_PTR_EXC_FWD(exc1));
			}
		catch (...) {
			invoke_exception(CREATE_PTR_EXC_FWD(nullptr));
			}

		return false;
		}

	void launch_timered_ctx()noexcept {
		if (auto c = _ctx.lock()) {

			CBL_VERIFY(!_expiredTimer);
			if (timeline().is_cyclic()) {

				auto xt = c->create_timer(_scx);

				/* ������ ������� ��������, ready - ����� ������� ����� �������� �������� */
				xt->start([wptr = weak_from_this()](crm::async_operation_result_t r) {
					if (auto s = wptr.lock()) {
						return s->launch_timered_ctx_cyclic(r);
						}
					else {
						return false;
						}
					}, timeline().interval(), false);

				_expiredTimer = std::move(xt);
				}
			else if (timeline().is_once_call()) {

				auto xt = c->create_timer_oncecall(_scx);

				/* ������ ������� ��������, ready - ����� ������� ����� �������� �������� */
				xt->start([wptr = weak_from_this()](crm::async_operation_result_t) {
					if (auto s = wptr.lock()) {
						s->close();
						}
					}, timeline().timeout());

				_expiredTimer = std::move(xt);
				}
			else {
				invoke_exception(CREATE_PTR_EXC_FWD("bad timeline format"));
				}
			}
		}

	void bind_io_hndl(std::unique_ptr<dcf_exception_t>&& exc,
		std::unique_ptr<i_iol_ihandler_t>&& rcvObj, 
		invoke_context_trait ic)noexcept {

		CBL_SNDRCV_VERIFY(!rcvObj || rcvObj->spin_tag() == _MessageSndRcvTraceId);

		CBL_SNDRCV_KEYTRACE_SET(_MessageSndRcvTraceId, 251);

		if (!exc) {
			if (rcvObj) {
				check_response(std::move(rcvObj), ic);
				}
			else {
				invoke_exception(CREATE_MPF_PTR_EXC_FWD(nullptr), ic, invoke_handler_as::async);
				}
			}
		else {
			invoke_exception(std::move(exc), ic, invoke_handler_as::async);
			}
		}

	decltype(auto) make_bind_io_hndl_extern()noexcept {
		return[wthis = weak_from_this()]
		(std::unique_ptr<dcf_exception_t>&& e, std::unique_ptr<i_iol_ihandler_t>&& r, invoke_context_trait ic)noexcept {

			if (auto s = wthis.lock()) {
				s->bind_io_hndl(std::move(e), std::move(r), ic);
				}
			else {
#ifdef CBL_SNDRCV_TRACE_COLLECT
				if (e) {
					exceptions_trace_collector::instance().add((*e));
					}
#endif
				}
			};
		}

	decltype(auto) make_bind_io_hndl_internal()noexcept {
		return[sptr = shared_from_this()]
		(std::unique_ptr<dcf_exception_t> && e, std::unique_ptr<i_iol_ihandler_t> && r, invoke_context_trait ic)noexcept{
			CBL_SNDRCV_KEYTRACE_SET(sptr->_MessageSndRcvTraceId, 79);
			sptr->bind_io_hndl(std::move(e), std::move(r), ic);
			};
		}

	decltype(auto) make_disconnection_event_hndl()noexcept {

		/* ���������� ���������� ��������� ������� */
		return[wptr = weak_from_this()](std::unique_ptr<i_event_t> && /*args*/, subscribe_invoke)noexcept {
			if (auto sptr = wptr.lock()) {

#ifdef CBL_MPF_TRACELEVEL_EVENT_CONNECTION
				sptr->invoke_exception(CREATE_MPF_PTR_EXC_FWD("remote object has been closed:" + sptr->_io.connection_key().to_string()));
#else
				sptr->invoke_exception(CREATE_MPF_PTR_EXC_FWD(L"remote object has been closed"));
#endif

				}
			};
		}

	void bind_io() {

		/* �������� �� ������� ��������� ��������� ����
		----------------------------------------------------------------------------------------------------*/
		CBL_VERIFY(!is_null(_subscribeId));
		CBL_VERIFY(!is_null(_eventSubscripeId));

		_addrTargetType = _address_hndl_t::make(_subscribeId, _io.level(), _scx);
		CBL_VERIFY(_addrTargetType.invoke_abandoned());

		subscribe_options opt{assign_options::insert_if_not_exists, true};

		if (bind_point_t::extern_ == _mode) {
			if (detail::subscribe_invoker_result::enum_type::success != _io.subscribe_hndl_id(_scx, _addrTargetType, opt,
				subscribe_message_handler("sndrcv-bind_io-1", make_bind_io_hndl_extern(), _clbInvokeAs))) {
				invoke_exception(CREATE_MPF_PTR_EXC_FWD(L"subscribe to response receive fault-1"));
				}
			}
		else {
			if (detail::subscribe_invoker_result::enum_type::success != _io.subscribe_hndl_id(_scx, _addrTargetType, opt,
				subscribe_message_handler("sndrcv-bind_io-2", make_bind_io_hndl_internal(), _clbInvokeAs))) {
				invoke_exception(CREATE_MPF_PTR_EXC_FWD(L"subscribe to response receive fault-2"));
				}
			}

		static_assert(is_event_callback_invokable_v<decltype(make_disconnection_event_hndl())>, __FILE_LINE__);

		_addrDisconnectionHandler = _address_event_t::make(_eventSubscripeId, CHECK_PTR(ctx())->domain_id(), event_type_t::st_disconnection, _io.level());
		if (!_io.subscribe_event(_scx, _addrDisconnectionHandler, true, make_disconnection_event_hndl())) {
			invoke_exception(CREATE_MPF_PTR_EXC_FWD(L"subscribe to peer events fault"));
			}

		CBL_SNDRCV_KEYTRACE_SET(_MessageSndRcvTraceId, 11);
		}

	void launch(launch_as m) {
		
		setup();
		CBL_VERIFY(weak_from_this().use_count() <= 1);

		auto body__ = [&] {

			/* �������� � �������� �����/������ */
			bind_io();

			/* ������ ��������� � ��������� */
			if(is_timeouted()) {

				/* ������ ��������� ������� */
				_timepointStart = std::chrono::system_clock::now();

				/* �������� ������� */
				snd_request_invoke([w = weak_from_this()]{
					if(auto s = w.lock()) {

						/* ������ ������� �������� */
						s->launch_timered_ctx();
						}
					}, m);
				}

			/* ������ ��������� ��������������� �������� */
			else {
				/* ������ ��������� ������� */
				_timepointStart = std::chrono::system_clock::now();

				/* �������� ������� */
				snd_request_invoke(nullptr, m);
				}
			};

		if(launch_as::async == m) {
			try {
				body__();
				}
			catch(const dcf_exception_t & exc0) {
				invoke_exception(exc0.dcpy());
				}
			catch(const std::exception & exc1) {
				invoke_exception(CREATE_MPF_PTR_EXC_FWD(exc1));
				}
			catch(...) {
				invoke_exception(CREATE_MPF_PTR_EXC_FWD(nullptr));
				}
			}
		else {
			body__();
			}
		}

	void snd_message_stage(request_message_type&& rq, std::function<void()>&& tail0) {
		MPF_TEST_EMULATION_IN_CONTEXT_COND_TRCX(ctx(), 400,
			is_trait_at_emulation_context(rq),
			trait_at_keytrace_set(rq));

		/* ���������� ���������� ������ �� "����" �������
		-------------------------------------------------------------*/
		rq.set_source_subscriber_id(_addrTargetType.id());

#ifdef  CBL_MPF_MESSAGE_TRACEWAY_KEYS_BY_ID
		CBL_SNDRCV_VERIFY(!is_null(_MessageSndRcvTraceId));
		rq.set_spin_tag(_MessageSndRcvTraceId);
#endif

		CBL_MPF_KEYTRACE_SET(rq, 12);

		/* ��������
		-------------------------------------------------------------*/
		CBL_VERIFY_SNDRCV_KEY_SPIN_TAG(rq);
		_io.push(std::move(rq));

		/* �������������� �������� */
		if (tail0) {
			tail0();
			}
		}

	auto make_request_stage() {

		/* ������� ������ */
		auto sendObj = _ctr();

		CBL_SNDRCV_KEYTRACE_SET(_MessageSndRcvTraceId, 13);

		MPF_TEST_EMULATION_IN_CONTEXT_COND_TRCX(ctx(), 401,
			is_trait_at_emulation_context(sendObj),
			trait_at_keytrace_set(sendObj));

		/* �����-���������� */
		if (sendObj.reset_invoke_exception_guard(inbound_message_with_tail_exception_guard([wptr = weak_from_this()](std::unique_ptr<dcf_exception_t>&& exc)noexcept {

			CHECK_NO_EXCEPTION_BEGIN

				if (auto s = wptr.lock()) {
					CBL_SNDRCV_KEYTRACE_SET(s->_MessageSndRcvTraceId, 501);

					std::wostringstream what;
					what << L"sndrcv-guard(initial)";
					what << L":name=" << cvt<wchar_t>(s->trace_name());

					if (!exc) {
						exc = CREATE_MPF_PTR_EXC_FWD(nullptr);
						}
					else {
						exc->add_message_line(what.str());
						}

					exc->set_code(RQo_B);

#ifdef CBL_MPF_TRACELEVEL_EVENT_SNDRCV_GUARD
					GLOBAL_STOUT_FWDRITE_STR(L"sndrcv-guard(initial):" + exc->msg());
#endif

					s->invoke_exception(std::move(exc));
					}

			CHECK_NO_EXCEPTION_END
			},
			ctx(),
			__FILE_LINE__,
			_scx))) {

			THROW_EXC_FWD(nullptr);
			}
		else {
			return sendObj;
			}
		}

	void snd_request_invoke(std::function<void()>&& tail0, launch_as m) {

		if (m == launch_as::async) {
			/* ��������� ��������� ���������� ��������� � ����� ��������� �������� */
			auto tail1 = utility::bind_once_call({ "sendrecv::snd_request_invoke" },
				[wptr = weak_from_this()](request_message_type&& rq, std::function<void()>&& tail0){
				if (auto s = wptr.lock()) {
					try {
						s->snd_message_stage(std::move(rq), std::move(tail0));
						}
					catch (const dcf_exception_t & exc0) {
						s->invoke_exception(exc0.dcpy());
						}
					catch (const std::exception & exc1) {
						s->invoke_exception(CREATE_MPF_PTR_EXC_FWD(exc1));
						}
					catch (...) {
						s->invoke_exception(CREATE_MPF_PTR_EXC_FWD(nullptr));
						}
					}
				}, std::placeholders::_1, std::move(tail0));

			/* �������� ��������� */
			auto requestStage = utility::bind_once_call({ "sendrecv::requestStage" },
				[wptr = weak_from_this()](decltype(tail1) && tail1_){
				if (auto s = wptr.lock()) {

					try {
						/* ������ ��������� �������� */
						if (auto c = s->ctx()) {
							c->launch_async(__FILE_LINE__, std::move(tail1_), s->make_request_stage());
							}
						}
					catch (const dcf_exception_t & exc0) {
						s->invoke_exception(exc0.dcpy());
						}
					catch (const std::exception & exc1) {
						s->invoke_exception(CREATE_MPF_PTR_EXC_FWD(exc1));
						}
					catch (...) {
						s->invoke_exception(CREATE_MPF_PTR_EXC_FWD(nullptr));
						}
					}
				}, std::move(tail1));

			/* ������ �������� �������� ��������� */
			if (auto c = ctx()) {
				c->launch_async(_scx, __FILE_LINE__, std::move(requestStage));
				}
			}
		else {
			snd_message_stage(make_request_stage(), std::move(tail0));
			}
		}


	/*! ��� - ����� ������� */
	void __invoke(invoke_handler_as f,
		async_operation_result_t result,
		std::unique_ptr<i_iol_ihandler_t> && obj,
		std::unique_ptr<dcf_exception_t> && ntf,
		invoke_context_trait /*ic*/)noexcept {

		CBL_SNDRCV_KEYTRACE_SET(_MessageSndRcvTraceId, 260);

		bool hndlf = false;
		if (_invokedf.compare_exchange_strong(hndlf, true)) {

#ifdef CBL_MPF_MESSAGE_TRACEWAY_KEYS_BY_ID
			--SendRecvOperationsCounter;
#endif

			if (auto c = _ctx.lock()) {
				auto mcs = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now() - _timepointStart);

#ifdef CBL_MPF_ENABLE_TIMETRACE
				if (obj)
					c->log_time(obj->source_id(), obj->destination_id(), "sndrcv_once_actor_t::invoke:" + trace_name(), mcs);
#endif

#ifdef CBL_MPF_USE_REVERSE_EXCEPTION_TRACE
				if (ntf) {
					auto rekey = cvt<wchar_t>(trace_name()) + L":" + ntf->msg();
					CHECK_NO_EXCEPTION(c->add_reverse_exception_trace(rekey));
					}
#endif

#ifdef CBL_SNDRCV_TRACE_COLLECT
				if (ntf) {
					exceptions_trace_collector::instance().add((*ntf));
					}
#endif

				CBL_VERIFY(!callback_check_nullptr(_clb));

				if (f == invoke_handler_as::instack && CallbackNoThrowInvokable) {
					CBL_SNDRCV_KEYTRACE_SET(_MessageSndRcvTraceId, 261);

					static_assert(std::is_nothrow_move_constructible_v<decltype(_clb)>, __FILE_LINE__);

					auto tailf = std::move(_clb);
					tailf(message_handler(result, std::move(obj), std::move(ntf), std::move(mcs)));

					CBL_SNDRCV_KEYTRACE_SET(_MessageSndRcvTraceId, 262);
					}
				else {
					CBL_SNDRCV_KEYTRACE_SET(_MessageSndRcvTraceId, 263);

					c->launch_async(_scx, __FILE_LINE__,
						[result
						
#ifdef CBL_SNDRCV_USE_TRACE_SPIN_TAG
						, keytrace_tag = _MessageSndRcvTraceId
#endif
						](decltype(_clb) && clb_,
							std::unique_ptr<i_iol_ihandler_t> && obj,
							std::unique_ptr<crm::dcf_exception_t> && ntf_,
							std::chrono::microseconds && mcs_) {

							CBL_MPF_KEYTRACE_SET(keytrace_tag, 265);
							
							clb_(message_handler(result, std::move(obj), std::move(ntf_), std::move(mcs_)));
							
							CBL_MPF_KEYTRACE_SET(keytrace_tag, 266);

						}, std::move(_clb), std::move(obj), std::move(ntf), std::move(mcs));

					c->launch_async(async_space_t::sys, __FILE_LINE__, [wptr = weak_from_this()]{
						if (auto sptr = wptr.lock()) {
							sptr->close();
							}
						});
					}
				}
			}
		}

	/*! �������� ��������� ��������� ������� */
	bool cycle_shifft() {

		bool next = false;
		if (!ready()) {

			/* �������� �� �������� */
			if (std::chrono::system_clock::now() >= _timeline.deadline_point()) {

				/* ���������� �� �������� */
				invoke_rejected(crm::async_operation_result_t::st_timeouted);
				}

			/* �������� ������� */
			else {
				try {
					snd_request_invoke(nullptr);
					next = timeline().is_cyclic();
					}
				catch (const dcf_exception_t & exc0) {
					invoke_exception(exc0.dcpy());
					}
				catch (const std::exception & exc1) {
					invoke_exception(CREATE_MPF_PTR_EXC_FWD(exc1));
					}
				catch (...) {
					invoke_exception(CREATE_MPF_PTR_EXC_FWD(nullptr));
					}
				}
			}

		return next;
		}

	bool invoked()const noexcept {
		return _invokedf.load(std::memory_order::memory_order_acquire);
		}

	void close(bool dctr)noexcept {
		CHECK_NO_EXCEPTION_BEGIN

			cancel();

		bool clf = false;
		if (_closedf.compare_exchange_strong(clf, true)) {

			/* ����� ������� ��������� ������ */
			if (!invoked()) {
				invoke_dctr(async_operation_result_t::st_abandoned);
				}

			/* ���������� ������ */
			if (_expiredTimer) {
				_expiredTimer->stop();
				_expiredTimer.reset();
				}

			/* ���������� �� �������� ��������� */
			if (_clbInvokeAs == inovked_as::async) {
				_io.unsubscribe_hndl_id(_addrTargetType);
				_io.unsubscribe_event(_addrDisconnectionHandler);
				}
			else {
				if (dctr) {
					if (auto c = ctx()) {
						c->launch_async(__FILE_LINE__,
							[](decltype(_io) && io, decltype(_addrTargetType) && addr, decltype(_addrDisconnectionHandler) && evAddr) {

							io.unsubscribe_hndl_id(addr);
							io.unsubscribe_event(evAddr);

							}, std::move(_io), std::move(_addrTargetType), std::move(_addrDisconnectionHandler));
						}
					}
				else {
					if (auto c = ctx()) {
						c->launch_async(__FILE_LINE__,
							[wptr = weak_from_this()](const decltype(_addrTargetType) & addr, const decltype(_addrDisconnectionHandler) & evAddr) {

							if (auto s = wptr.lock()) {
								s->_io.unsubscribe_hndl_id(addr);
								s->_io.unsubscribe_event(evAddr);
								}
							}, _addrTargetType, _addrDisconnectionHandler);

						CBL_VERIFY(!is_null(_addrTargetType.id()));
						CBL_VERIFY(!is_null(_addrDisconnectionHandler));
						}
					}
				}
			}

		CHECK_NO_EXCEPTION_END
		}

	void ctr_trace_id(int ktcc = 0)noexcept {
		ktcc;

		CHECK_NO_EXCEPTION_BEGIN

#ifdef CBL_SNDRCV_USE_TRACE_SPIN_TAG
			_MessageSndRcvTraceId = sx_locid_t::make(_subscribeId.value() ^ _subscribeId.key(),
				++detail::GlobalMessageSndRcvTraceCounter);
		CBL_VERIFY_SNDRCV_KEY_SPIN_TAG(_MessageSndRcvTraceId);
#endif

		CBL_SNDRCV_KEYTRACE_SET(_MessageSndRcvTraceId, ktcc);
		CBL_SNDRCV_TRACE_ID_INC(_MessageSndRcvTraceId, trace_name());

		CHECK_NO_EXCEPTION_END
		}

public:
	template<typename _Xio,
		typename _XSendCtr,
		typename _XCallback,
		typename XTypeIO = typename std::decay_t<_Xio>,
		typename XTypeSendCtr = typename std::decay_t<_XSendCtr>,
		typename XCallback = typename std::decay_t<_XCallback>,
		typename std::enable_if_t<(
			is_sendrecv_request_invokable_v<XTypeSendCtr, XTypeIO> &&
			is_sndrcv_callback_v<XCallback> &&
			is_xpeer_trait_z_v<XTypeIO>
			), int>* = 0>
		sndrcv_once_actor_t(async_space_t scx_,
			std::weak_ptr<distributed_ctx_t> ctx_,
			_Xio && io,
			_XSendCtr && ctr,
			_XCallback && clb,
			
#if defined (XPEER_SNDRCV_ACTOR_TRACE)
			trace_tag&& tag,
#else
			trace_tag&&,
#endif

			const sndrcv_timeline_t & timeline_,
			bind_point_t mode_,
			inovked_as as_ = inovked_as::async,
			int ktcc = 0)
		: _ctx(ctx_)
#if defined (XPEER_SNDRCV_ACTOR_TRACE)
		, _tag(std::move(tag))
#else

#endif
		, _io(std::forward<_Xio>(io))
		, _subscribeId(CHECK_PTR(ctx_)->make_local_object_identity())
		, _eventSubscripeId(CHECK_PTR(ctx_)->make_event_identity())
		, _ctr(std::forward<_XSendCtr>(ctr))
		, _clb(std::forward<_XCallback>(clb))
		, _timeline(timeline_)
		, _mode(mode_)
		, _clbInvokeAs{as_}
		, _scx(scx_)
		
#if defined( CBL_MPF_OBJECTS_COUNTER_CHECKER_SNDRCV_OPERATIONS) && defined(XPEER_SNDRCV_ACTOR_TRACE)
	, _xDbgCounter{ _tag.name }
#endif
	{

#ifdef CBL_MPF_MESSAGE_TRACEWAY_KEYS_BY_ID
		++SendRecvOperationsCounter;
#endif

		if (callback_check_nullptr(_clb))
			THROW_MPF_EXC_FWD(nullptr);

		CBL_VERIFY(!is_null(_subscribeId));
		CBL_VERIFY(!is_null(_eventSubscripeId));
		CBL_VERIFY(_clbInvokeAs == inovked_as::async || (_clbInvokeAs == inovked_as::sync && CallbackNoThrowInvokable));

		ctr_trace_id(ktcc);
		}

	void setup() noexcept{
		CBL_SNDRCV_OBJECT_MONITOR_INC(*CHECK_PTR(_ctx), _MessageSndRcvTraceId, shared_from_this());
		}

	std::string name()const noexcept final {
		return trace_name() +
			
#ifdef CBL_SNDRCV_USE_TRACE_SPIN_TAG
			":" + _MessageSndRcvTraceId.to_str() + 
#endif
			(_invokedf.load() ? ":invoked" : ":wait");
		}

	void do_terminate()noexcept final {
		CBL_NO_EXCEPTION_BEGIN

		std::unique_ptr<dcf_exception_t> pExc;

#ifdef CBL_SNDRCV_USE_TRACE_SPIN_TAG
		pExc = CREATE_TIMEOUT_PTR_EXC_FWD(_MessageSndRcvTraceId.to_str() + ":" + trace_name());
#else
		pExc = CREATE_TIMEOUT_PTR_EXC_FWD(trace_name());
#endif

		CHECK_NO_EXCEPTION(invoke_exception(std::move(pExc), invoke_context_trait::sync, invoke_handler_as::async));

#if defined( CBL_MPF_TRACELEVEL_EVENT_SNDRCV_GUARD) && defined(CBL_SNDRCV_USE_TRACE_SPIN_TAG)
		GLOBAL_STOUT_FWDRITE_STR("sndrcv force terminated: " + _MessageSndRcvTraceId.to_str() + ":" + trace_name());
#endif

		CBL_NO_EXCEPTION_END(_ctx.lock())
		}

	std::chrono::system_clock::duration timeout_interval()const noexcept final {
		if (timeline().is_timeouted()) {
			return timeline().timeout();
			}
		else {
			return std::chrono::system_clock::duration(0);
			}
		}

	std::chrono::system_clock::time_point time_point_deadline()const noexcept final {
		if (timeline().is_timeouted()) {
			return timeline().deadline_point();
			}
		else {
			return {};
			}
		}

	std::chrono::system_clock::time_point time_point_start()const noexcept {
		return _timepointStart;
		}

	std::chrono::system_clock::duration default_timeline()const noexcept {
		if (auto ctx = _ctx.lock()) {
			return ctx->default_timeline();
			}
		else {
			return std::chrono::seconds(3600);
			}
		}

public:
	struct handler_t {
		friend class self_t;

	private:
		std::shared_ptr<self_t> _p;

	public:
		handler_t()noexcept {}

	private:
		handler_t(std::shared_ptr<self_t> p)noexcept
			: _p(std::move(p)) {
			}

	public:
		void cancel()noexcept {
			if (_p) {
				_p->cancel();
				_p->close();
				_p.reset();
				}
			}
		};

	sndrcv_once_actor_t(const sndrcv_once_actor_t &) = delete;
	sndrcv_once_actor_t& operator=(const sndrcv_once_actor_t &) = delete;

	~sndrcv_once_actor_t() {
		CHECK_NO_EXCEPTION(close(true));

		CBL_SNDRCV_KEYTRACE_UNSET(_MessageSndRcvTraceId);
		CBL_SNDRCV_TRACE_ID_DEC(_MessageSndRcvTraceId);

		if (auto pctx = _ctx.lock()) {
			CBL_SNDRCV_OBJECT_MONITOR_DEC(*pctx, _MessageSndRcvTraceId);
			CBL_SNDRCV_VERIFY(CBL_SNDRCV_TRACE_ID_HAS(_MessageSndRcvTraceId) == 0);
			}
		}

	template<typename _Xio,
		typename _XSendCtr,
		typename _XCallback,
		typename XTypeIO = typename std::decay_t<_Xio>,
		typename XTypeSendCtr = typename std::decay_t<_XSendCtr>,
		typename XCallback = typename std::decay_t<_XCallback>,
		typename std::enable_if_t<(
			is_sendrecv_request_invokable_v<XTypeSendCtr, XTypeIO> &&
			is_sndrcv_callback_v<XCallback> &&
			is_xpeer_trait_z_v<XTypeIO>
			)>* = 0>
		static handler_t hlaunch(async_space_t scx,
			std::weak_ptr<distributed_ctx_t> ctx_,
			_Xio && io,
			_XSendCtr && ctr,
			_XCallback && clb,
			trace_tag && tag,
			const sndrcv_timeline_t & timeline,
			launch_as m,
			inovked_as as_ = inovked_as::async,
			int ktcc = 0) {

		auto obj = std::make_shared<self_t>(scx,
			ctx_,
			std::forward<_Xio>(io),
			std::forward<_XSendCtr>(ctr),
			std::forward<_XCallback>(clb),
			std::move(tag),
			timeline,
			bind_point_t::extern_,
			as_,
			ktcc);
		obj->launch(m);

		return obj;
		}

	template<typename _Xio,
		typename _XSendCtr,
		typename _XCallback,
		typename XTypeIO = typename std::decay_t<_Xio>,
		typename XTypeSendCtr = typename std::decay_t<_XSendCtr>,
		typename XCallback = typename std::decay_t<_XCallback>,
		typename std::enable_if_t<(
			is_sendrecv_request_invokable_v<XTypeSendCtr, XTypeIO> &&
			is_sndrcv_callback_v<XCallback> &&
			is_xpeer_trait_z_v<XTypeIO>
			)>* = 0>
		static void alaunch(async_space_t scx,
			std::weak_ptr<distributed_ctx_t> ctx_,
			_Xio&& io,
			_XSendCtr&& ctr,
			_XCallback&& clb,
			trace_tag&& tag,
			const sndrcv_timeline_t& timeline,
			launch_as m,
			inovked_as as_ = inovked_as::async,
			int ktcc = 0) {

		auto obj = std::make_shared<self_t>(scx,
			ctx_,
			std::forward<_Xio>(io),
			std::forward<_XSendCtr>(ctr),
			std::forward<_XCallback>(clb),
			std::move(tag),
			timeline,
			bind_point_t::internal_,
			as_,
			ktcc);
		obj->launch(m);
		}

	void cancel()noexcept {
		_cancelf.store(true, std::memory_order::memory_order_release);
		}

	void close()noexcept {
		close(false);
		}
	};



template<typename _Xio,
	typename _XSendCtr,
	typename _XCallback,
	typename std::enable_if_t<(
		is_xpeer_trait_z_v<_Xio>&&
		is_sndrcv_callback_v<_XCallback>&&
		is_sendrecv_request_invokable_v<_XSendCtr, _Xio>
		), int> = 0>
	struct async_sndrcv_type_deduction_t {

	using actor_type = typename sndrcv_once_actor_t<_Xio, _XSendCtr, _XCallback>;
	};

template<typename _Xio,
	typename _XSendCtr,
	typename _XCallback,
	typename std::enable_if_t<(
		is_xpeer_trait_z_v<_Xio>&&
		is_sndrcv_callback_v<_XCallback>&&
		is_sendrecv_request_invokable_v<_XSendCtr, _Xio>
		), int> = 0>
	auto async_sndrcv_type_deduction_expression(_Xio&& xio, _XSendCtr&& ctr, _XCallback&& c)
	-> typename async_sndrcv_type_deduction_t<_Xio, _XSendCtr, _XCallback>::actor_type;


template<typename _Xio,
	typename _XSendCtr,
	typename _XCallback,
	typename std::enable_if_t<(
		is_xpeer_trait_z_v<_Xio>&&
		is_sndrcv_callback_v<_XCallback>&&
		is_sendrecv_request_invokable_v<_XSendCtr, _Xio>
		), int> = 0>
void make_async_sndrcv_once_base(async_space_t scx,
	std::weak_ptr<distributed_ctx_t> ctx_,
	_Xio && io,
	_XSendCtr && ctr,
	_XCallback && clb,
	trace_tag&& tag,
	const sndrcv_timeline_t & timeline,
	launch_as m,
	inovked_as as_ = inovked_as::async ) {

	using xio_type = std::decay_t<decltype(make_invokable_xpeer(std::forward<_Xio>(io)))>;
	using actor_type = typename std::decay_t<decltype(async_sndrcv_type_deduction_expression(
		std::declval<xio_type>(),
		std::forward<_XSendCtr>(ctr),
		std::forward<_XCallback>(clb)))>;

	static_assert(is_sendrecv_request_invokable_v<_XSendCtr, xio_type>, __FILE_LINE__);
	static_assert(is_sndrcv_callback_v<_XCallback>, __FILE_LINE__);
	static_assert(is_xpeer_trait_z_v<xio_type>, __FILE_LINE__);

	actor_type::alaunch(
		scx,
		std::move(ctx_),
		make_invokable_xpeer(std::forward<_Xio>(io)),
		std::forward<_XSendCtr>(ctr),
		std::forward<_XCallback>(clb),
		std::move(tag),
		timeline,
		m,
		as_ );
	}

template<typename _Xio,
	typename _XSendCtr,
	typename _XCallback,
	typename std::enable_if_t<(
		is_xpeer_trait_z_v<_Xio>&&
		is_sndrcv_callback_v<_XCallback>&&
		is_sendrecv_request_invokable_v<_XSendCtr, _Xio>
		), int> = 0 >
auto make_handler_sndrcv_once_base(async_space_t scx,
	std::weak_ptr<distributed_ctx_t> ctx_,
	_Xio && io,
	_XSendCtr && ctr,
	_XCallback && clb,
	trace_tag&& tag,
	const sndrcv_timeline_t & timeline,
	launch_as m,
	inovked_as as_ = inovked_as::async) {

	using xio_type = std::decay_t<decltype(make_invokable_xpeer(std::forward<_Xio>(io)))>;
	using actor_type = typename std::decay_t<decltype(async_sndrcv_type_deduction_expression(
		std::declval<xio_type>(),
		std::forward<_XSendCtr>(ctr),
		std::forward<_XCallback>(clb)))>;

	static_assert(is_sendrecv_request_invokable_v<_XSendCtr, xio_type>, __FILE_LINE__);
	static_assert(is_sndrcv_callback_v<_XCallback>, __FILE_LINE__);
	static_assert(is_xpeer_trait_z_v<xio_type>, __FILE_LINE__);
	
	return actor_type::hlaunch(
		scx,
		std::move(ctx_),
		make_invokable_xpeer(std::forward<_Xio>(io)),
		std::forward<_XSendCtr>(ctr),
		std::forward<_XCallback>(clb),
		std::move(tag),
		timeline,
		m,
		as_);
	}


template<typename _Xio,
	typename _XSendCtr,
	typename _XCallback,
	typename std::enable_if_t<(
		is_xpeer_trait_z_v<_Xio>&&
		is_sndrcv_callback_v<_XCallback>&&
		is_sendrecv_request_invokable_v<_XSendCtr, _Xio>
		), int> = 0>
	void make_async_sndrcv_once_sys(std::weak_ptr<distributed_ctx_t> ctx_,
		_Xio && io,
		_XSendCtr && ctr,
		_XCallback && clb,
		trace_tag&& tag,
		const sndrcv_timeline_t & timeline,
		launch_as m,
		inovked_as as_ = inovked_as::async) {

	detail::make_async_sndrcv_once_base(
		async_space_t::sys,
		std::move(ctx_),
		std::forward<_Xio>(io),
		std::forward<_XSendCtr>(ctr),
		std::forward<_XCallback>(clb),
		std::move(tag),
		timeline,
		m,
		as_ );
	}
}


template<typename _Xio,
	typename _XSendCtr,
	typename _XCallback,
	typename std::enable_if_t<(
		detail::is_xpeer_trait_z_v<_Xio>&&
		is_sndrcv_callback_v<_XCallback>&&
		detail::is_sendrecv_request_invokable_v<_XSendCtr, _Xio>
		), int> = 0>
void make_async_sndrcv_once(std::weak_ptr<distributed_ctx_t> ctx_, 
	_Xio && io,
	_XSendCtr && ctr,
	_XCallback && clb,
	trace_tag&& tag,
	const sndrcv_timeline_t & timeline,
	launch_as m,
	detail::inovked_as as_ = detail::inovked_as::async) {

	detail::make_async_sndrcv_once_base(
		async_space_t::ext,
		std::move(ctx_),
		std::forward<_Xio>(io),
		std::forward<_XSendCtr>(ctr),
		std::forward<_XCallback>(clb),
		std::move(tag),
		timeline,
		m,
		as_ );
	}


template<typename _Xio,
	typename _XSendCtr,
	typename _XCallback,
	typename std::enable_if_t<(
		detail::is_xpeer_trait_z_v<_Xio> &&
		is_sndrcv_callback_v<_XCallback> &&
		detail::is_sendrecv_request_invokable_v<_XSendCtr, _Xio>
	), int> = 0>
	auto make_handler_sndrcv_once(std::weak_ptr<distributed_ctx_t> ctx_, 
		_Xio && io,
		_XSendCtr && ctr,
		_XCallback && clb,
		trace_tag&& tag,
		const sndrcv_timeline_t & timeline,
		launch_as m,
		detail::inovked_as as_ = detail::inovked_as::async) {

	return detail::make_handler_sndrcv_once_base(
		async_space_t::ext,
		std::move(ctx_),
		std::forward<_Xio>(io),
		std::forward<_XSendCtr>(ctr),
		std::forward<_XCallback>(clb),
		std::move(tag),
		timeline,
		m,
		as_ );
	}
}

