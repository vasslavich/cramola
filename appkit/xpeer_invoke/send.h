#pragma once

#include "../xpeer/terms.h"
#include "./xpeer_traits.h"

namespace crm{


template<typename IO, 
	typename O,
	typename std::enable_if_t <(
		is_xpeer_push_trait_z_v<IO> &&
		is_out_message_requirements_v<O>
		), int> = 0>
void send(IO & io, O&& obj){
	io.push(std::forward<O>(obj));
	}

template<typename IO, 
	typename O,
	typename std::enable_if_t <(
		is_xpeer_push_trait_z_v<IO>&&
		srlz::is_serializable_v<O> &&
		!is_out_message_requirements_v<O>
		), int> = 0>
void send(IO& io, O&& obj){
	send(io, detail::ostrob_message_envelop_t(std::forward<O>(obj)));
	}
}
